/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GLOBALNEGRESPONSE</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getGLOBALNEGRESPONSE()
 * @model extendedMetaData="name='GLOBAL-NEG-RESPONSE' kind='elementOnly'"
 * @generated
 */
public interface GLOBALNEGRESPONSE extends RESPONSE {
} // GLOBALNEGRESPONSE
