/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UNITREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.UNITREFS#getUNITREF <em>UNITREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getUNITREFS()
 * @model extendedMetaData="name='UNIT-REFS' kind='elementOnly'"
 * @generated
 */
public interface UNITREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>UNITREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNITREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITREFS_UNITREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='UNIT-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getUNITREF();

} // UNITREFS
