/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLETYPE</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLETYPE()
 * @model extendedMetaData="name='VEHICLE-TYPE' kind='elementOnly'"
 * @generated
 */
public interface VEHICLETYPE extends INFOCOMPONENT {
} // VEHICLETYPE
