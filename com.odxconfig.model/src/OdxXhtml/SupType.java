/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sup Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSupType()
 * @model extendedMetaData="name='sup_._type' kind='mixed'"
 * @generated
 */
public interface SupType extends Inline {
} // SupType
