/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPARAMREF</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPARAMREF#getVALUE <em>VALUE</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAMREF#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAMREF#getPROTOCOLSNREF <em>PROTOCOLSNREF</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAMREF#getDOCREF <em>DOCREF</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAMREF#getDOCTYPE <em>DOCTYPE</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAMREF#getIDREF <em>IDREF</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAMREF#getREVISION <em>REVISION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREF()
 * @model extendedMetaData="name='COMPARAM-REF' kind='elementOnly'"
 * @generated
 */
public interface COMPARAMREF extends EObject {
	/**
	 * Returns the value of the '<em><b>VALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VALUE</em>' attribute.
	 * @see #setVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREF_VALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMREF#getVALUE <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VALUE</em>' attribute.
	 * @see #getVALUE()
	 * @generated
	 */
	void setVALUE(String value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREF_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMREF#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>PROTOCOLSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROTOCOLSNREF</em>' containment reference.
	 * @see #setPROTOCOLSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREF_PROTOCOLSNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PROTOCOL-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getPROTOCOLSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMREF#getPROTOCOLSNREF <em>PROTOCOLSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROTOCOLSNREF</em>' containment reference.
	 * @see #getPROTOCOLSNREF()
	 * @generated
	 */
	void setPROTOCOLSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>DOCREF</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCREF</em>' attribute.
	 * @see #setDOCREF(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREF_DOCREF()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='DOCREF' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDOCREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMREF#getDOCREF <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOCREF</em>' attribute.
	 * @see #getDOCREF()
	 * @generated
	 */
	void setDOCREF(String value);

	/**
	 * Returns the value of the '<em><b>DOCTYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.DOCTYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCTYPE</em>' attribute.
	 * @see OdxXhtml.DOCTYPE
	 * @see #isSetDOCTYPE()
	 * @see #unsetDOCTYPE()
	 * @see #setDOCTYPE(DOCTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREF_DOCTYPE()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='DOCTYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	DOCTYPE getDOCTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMREF#getDOCTYPE <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOCTYPE</em>' attribute.
	 * @see OdxXhtml.DOCTYPE
	 * @see #isSetDOCTYPE()
	 * @see #unsetDOCTYPE()
	 * @see #getDOCTYPE()
	 * @generated
	 */
	void setDOCTYPE(DOCTYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.COMPARAMREF#getDOCTYPE <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDOCTYPE()
	 * @see #getDOCTYPE()
	 * @see #setDOCTYPE(DOCTYPE)
	 * @generated
	 */
	void unsetDOCTYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.COMPARAMREF#getDOCTYPE <em>DOCTYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>DOCTYPE</em>' attribute is set.
	 * @see #unsetDOCTYPE()
	 * @see #getDOCTYPE()
	 * @see #setDOCTYPE(DOCTYPE)
	 * @generated
	 */
	boolean isSetDOCTYPE();

	/**
	 * Returns the value of the '<em><b>IDREF</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IDREF</em>' attribute.
	 * @see #setIDREF(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREF_IDREF()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='ID-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	String getIDREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMREF#getIDREF <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IDREF</em>' attribute.
	 * @see #getIDREF()
	 * @generated
	 */
	void setIDREF(String value);

	/**
	 * Returns the value of the '<em><b>REVISION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REVISION</em>' attribute.
	 * @see #setREVISION(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREF_REVISION()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='REVISION' namespace='##targetNamespace'"
	 * @generated
	 */
	String getREVISION();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMREF#getREVISION <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REVISION</em>' attribute.
	 * @see #getREVISION()
	 * @generated
	 */
	void setREVISION(String value);

} // COMPARAMREF
