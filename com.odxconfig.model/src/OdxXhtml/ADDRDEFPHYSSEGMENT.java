/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ADDRDEFPHYSSEGMENT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ADDRDEFPHYSSEGMENT#getENDADDRESS <em>ENDADDRESS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getADDRDEFPHYSSEGMENT()
 * @model extendedMetaData="name='ADDRDEF-PHYS-SEGMENT' kind='elementOnly'"
 * @generated
 */
public interface ADDRDEFPHYSSEGMENT extends PHYSSEGMENT {
	/**
	 * Returns the value of the '<em><b>ENDADDRESS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENDADDRESS</em>' attribute.
	 * @see #setENDADDRESS(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getADDRDEFPHYSSEGMENT_ENDADDRESS()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary" required="true"
	 *        extendedMetaData="kind='element' name='END-ADDRESS' namespace='##targetNamespace'"
	 * @generated
	 */
	byte[] getENDADDRESS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ADDRDEFPHYSSEGMENT#getENDADDRESS <em>ENDADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ENDADDRESS</em>' attribute.
	 * @see #getENDADDRESS()
	 * @generated
	 */
	void setENDADDRESS(byte[] value);

} // ADDRDEFPHYSSEGMENT
