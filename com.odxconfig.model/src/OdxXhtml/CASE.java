/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CASE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.CASE#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.CASE#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.CASE#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.CASE#getSTRUCTUREREF <em>STRUCTUREREF</em>}</li>
 *   <li>{@link OdxXhtml.CASE#getLOWERLIMIT <em>LOWERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.CASE#getUPPERLIMIT <em>UPPERLIMIT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCASE()
 * @model extendedMetaData="name='CASE' kind='elementOnly'"
 * @generated
 */
public interface CASE extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCASE_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.CASE#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getCASE_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.CASE#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getCASE_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.CASE#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>STRUCTUREREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STRUCTUREREF</em>' containment reference.
	 * @see #setSTRUCTUREREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getCASE_STRUCTUREREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='STRUCTURE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getSTRUCTUREREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.CASE#getSTRUCTUREREF <em>STRUCTUREREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>STRUCTUREREF</em>' containment reference.
	 * @see #getSTRUCTUREREF()
	 * @generated
	 */
	void setSTRUCTUREREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>LOWERLIMIT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LOWERLIMIT</em>' containment reference.
	 * @see #setLOWERLIMIT(LIMIT)
	 * @see OdxXhtml.OdxXhtmlPackage#getCASE_LOWERLIMIT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='LOWER-LIMIT' namespace='##targetNamespace'"
	 * @generated
	 */
	LIMIT getLOWERLIMIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.CASE#getLOWERLIMIT <em>LOWERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LOWERLIMIT</em>' containment reference.
	 * @see #getLOWERLIMIT()
	 * @generated
	 */
	void setLOWERLIMIT(LIMIT value);

	/**
	 * Returns the value of the '<em><b>UPPERLIMIT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UPPERLIMIT</em>' containment reference.
	 * @see #setUPPERLIMIT(LIMIT)
	 * @see OdxXhtml.OdxXhtmlPackage#getCASE_UPPERLIMIT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='UPPER-LIMIT' namespace='##targetNamespace'"
	 * @generated
	 */
	LIMIT getUPPERLIMIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.CASE#getUPPERLIMIT <em>UPPERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UPPERLIMIT</em>' containment reference.
	 * @see #getUPPERLIMIT()
	 * @generated
	 */
	void setUPPERLIMIT(LIMIT value);

} // CASE
