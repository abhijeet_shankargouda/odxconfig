/**
 */
package OdxXhtml;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DOCREVISION</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DOCREVISION#getTEAMMEMBERREF <em>TEAMMEMBERREF</em>}</li>
 *   <li>{@link OdxXhtml.DOCREVISION#getREVISIONLABEL <em>REVISIONLABEL</em>}</li>
 *   <li>{@link OdxXhtml.DOCREVISION#getSTATE <em>STATE</em>}</li>
 *   <li>{@link OdxXhtml.DOCREVISION#getDATE <em>DATE</em>}</li>
 *   <li>{@link OdxXhtml.DOCREVISION#getTOOL <em>TOOL</em>}</li>
 *   <li>{@link OdxXhtml.DOCREVISION#getCOMPANYREVISIONINFOS <em>COMPANYREVISIONINFOS</em>}</li>
 *   <li>{@link OdxXhtml.DOCREVISION#getMODIFICATIONS <em>MODIFICATIONS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISION()
 * @model extendedMetaData="name='DOC-REVISION' kind='elementOnly'"
 * @generated
 */
public interface DOCREVISION extends EObject {
	/**
	 * Returns the value of the '<em><b>TEAMMEMBERREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TEAMMEMBERREF</em>' containment reference.
	 * @see #setTEAMMEMBERREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISION_TEAMMEMBERREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TEAM-MEMBER-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getTEAMMEMBERREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DOCREVISION#getTEAMMEMBERREF <em>TEAMMEMBERREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TEAMMEMBERREF</em>' containment reference.
	 * @see #getTEAMMEMBERREF()
	 * @generated
	 */
	void setTEAMMEMBERREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>REVISIONLABEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REVISIONLABEL</em>' attribute.
	 * @see #setREVISIONLABEL(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISION_REVISIONLABEL()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='REVISION-LABEL' namespace='##targetNamespace'"
	 * @generated
	 */
	String getREVISIONLABEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.DOCREVISION#getREVISIONLABEL <em>REVISIONLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REVISIONLABEL</em>' attribute.
	 * @see #getREVISIONLABEL()
	 * @generated
	 */
	void setREVISIONLABEL(String value);

	/**
	 * Returns the value of the '<em><b>STATE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STATE</em>' attribute.
	 * @see #setSTATE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISION_STATE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='STATE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSTATE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DOCREVISION#getSTATE <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>STATE</em>' attribute.
	 * @see #getSTATE()
	 * @generated
	 */
	void setSTATE(String value);

	/**
	 * Returns the value of the '<em><b>DATE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATE</em>' attribute.
	 * @see #setDATE(XMLGregorianCalendar)
	 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISION_DATE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.DateTime" required="true"
	 *        extendedMetaData="kind='element' name='DATE' namespace='##targetNamespace'"
	 * @generated
	 */
	XMLGregorianCalendar getDATE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DOCREVISION#getDATE <em>DATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATE</em>' attribute.
	 * @see #getDATE()
	 * @generated
	 */
	void setDATE(XMLGregorianCalendar value);

	/**
	 * Returns the value of the '<em><b>TOOL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TOOL</em>' attribute.
	 * @see #setTOOL(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISION_TOOL()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='TOOL' namespace='##targetNamespace'"
	 * @generated
	 */
	String getTOOL();

	/**
	 * Sets the value of the '{@link OdxXhtml.DOCREVISION#getTOOL <em>TOOL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TOOL</em>' attribute.
	 * @see #getTOOL()
	 * @generated
	 */
	void setTOOL(String value);

	/**
	 * Returns the value of the '<em><b>COMPANYREVISIONINFOS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYREVISIONINFOS</em>' containment reference.
	 * @see #setCOMPANYREVISIONINFOS(COMPANYREVISIONINFOS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISION_COMPANYREVISIONINFOS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPANY-REVISION-INFOS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPANYREVISIONINFOS getCOMPANYREVISIONINFOS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DOCREVISION#getCOMPANYREVISIONINFOS <em>COMPANYREVISIONINFOS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYREVISIONINFOS</em>' containment reference.
	 * @see #getCOMPANYREVISIONINFOS()
	 * @generated
	 */
	void setCOMPANYREVISIONINFOS(COMPANYREVISIONINFOS value);

	/**
	 * Returns the value of the '<em><b>MODIFICATIONS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MODIFICATIONS</em>' containment reference.
	 * @see #setMODIFICATIONS(MODIFICATIONS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISION_MODIFICATIONS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MODIFICATIONS' namespace='##targetNamespace'"
	 * @generated
	 */
	MODIFICATIONS getMODIFICATIONS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DOCREVISION#getMODIFICATIONS <em>MODIFICATIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MODIFICATIONS</em>' containment reference.
	 * @see #getMODIFICATIONS()
	 * @generated
	 */
	void setMODIFICATIONS(MODIFICATIONS value);

} // DOCREVISION
