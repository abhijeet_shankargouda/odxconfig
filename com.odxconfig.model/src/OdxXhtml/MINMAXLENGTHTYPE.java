/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MINMAXLENGTHTYPE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MINMAXLENGTHTYPE#getMAXLENGTH <em>MAXLENGTH</em>}</li>
 *   <li>{@link OdxXhtml.MINMAXLENGTHTYPE#getMINLENGTH <em>MINLENGTH</em>}</li>
 *   <li>{@link OdxXhtml.MINMAXLENGTHTYPE#getTERMINATION <em>TERMINATION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMINMAXLENGTHTYPE()
 * @model extendedMetaData="name='MIN-MAX-LENGTH-TYPE' kind='elementOnly'"
 * @generated
 */
public interface MINMAXLENGTHTYPE extends DIAGCODEDTYPE {
	/**
	 * Returns the value of the '<em><b>MAXLENGTH</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MAXLENGTH</em>' attribute.
	 * @see #isSetMAXLENGTH()
	 * @see #unsetMAXLENGTH()
	 * @see #setMAXLENGTH(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getMINMAXLENGTHTYPE_MAXLENGTH()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='element' name='MAX-LENGTH' namespace='##targetNamespace'"
	 * @generated
	 */
	long getMAXLENGTH();

	/**
	 * Sets the value of the '{@link OdxXhtml.MINMAXLENGTHTYPE#getMAXLENGTH <em>MAXLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MAXLENGTH</em>' attribute.
	 * @see #isSetMAXLENGTH()
	 * @see #unsetMAXLENGTH()
	 * @see #getMAXLENGTH()
	 * @generated
	 */
	void setMAXLENGTH(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MINMAXLENGTHTYPE#getMAXLENGTH <em>MAXLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMAXLENGTH()
	 * @see #getMAXLENGTH()
	 * @see #setMAXLENGTH(long)
	 * @generated
	 */
	void unsetMAXLENGTH();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MINMAXLENGTHTYPE#getMAXLENGTH <em>MAXLENGTH</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MAXLENGTH</em>' attribute is set.
	 * @see #unsetMAXLENGTH()
	 * @see #getMAXLENGTH()
	 * @see #setMAXLENGTH(long)
	 * @generated
	 */
	boolean isSetMAXLENGTH();

	/**
	 * Returns the value of the '<em><b>MINLENGTH</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MINLENGTH</em>' attribute.
	 * @see #isSetMINLENGTH()
	 * @see #unsetMINLENGTH()
	 * @see #setMINLENGTH(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getMINMAXLENGTHTYPE_MINLENGTH()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='MIN-LENGTH' namespace='##targetNamespace'"
	 * @generated
	 */
	long getMINLENGTH();

	/**
	 * Sets the value of the '{@link OdxXhtml.MINMAXLENGTHTYPE#getMINLENGTH <em>MINLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MINLENGTH</em>' attribute.
	 * @see #isSetMINLENGTH()
	 * @see #unsetMINLENGTH()
	 * @see #getMINLENGTH()
	 * @generated
	 */
	void setMINLENGTH(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MINMAXLENGTHTYPE#getMINLENGTH <em>MINLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMINLENGTH()
	 * @see #getMINLENGTH()
	 * @see #setMINLENGTH(long)
	 * @generated
	 */
	void unsetMINLENGTH();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MINMAXLENGTHTYPE#getMINLENGTH <em>MINLENGTH</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MINLENGTH</em>' attribute is set.
	 * @see #unsetMINLENGTH()
	 * @see #getMINLENGTH()
	 * @see #setMINLENGTH(long)
	 * @generated
	 */
	boolean isSetMINLENGTH();

	/**
	 * Returns the value of the '<em><b>TERMINATION</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.TERMINATION}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TERMINATION</em>' attribute.
	 * @see OdxXhtml.TERMINATION
	 * @see #isSetTERMINATION()
	 * @see #unsetTERMINATION()
	 * @see #setTERMINATION(TERMINATION)
	 * @see OdxXhtml.OdxXhtmlPackage#getMINMAXLENGTHTYPE_TERMINATION()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='TERMINATION' namespace='##targetNamespace'"
	 * @generated
	 */
	TERMINATION getTERMINATION();

	/**
	 * Sets the value of the '{@link OdxXhtml.MINMAXLENGTHTYPE#getTERMINATION <em>TERMINATION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TERMINATION</em>' attribute.
	 * @see OdxXhtml.TERMINATION
	 * @see #isSetTERMINATION()
	 * @see #unsetTERMINATION()
	 * @see #getTERMINATION()
	 * @generated
	 */
	void setTERMINATION(TERMINATION value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MINMAXLENGTHTYPE#getTERMINATION <em>TERMINATION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTERMINATION()
	 * @see #getTERMINATION()
	 * @see #setTERMINATION(TERMINATION)
	 * @generated
	 */
	void unsetTERMINATION();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MINMAXLENGTHTYPE#getTERMINATION <em>TERMINATION</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TERMINATION</em>' attribute is set.
	 * @see #unsetTERMINATION()
	 * @see #getTERMINATION()
	 * @see #setTERMINATION(TERMINATION)
	 * @generated
	 */
	boolean isSetTERMINATION();

} // MINMAXLENGTHTYPE
