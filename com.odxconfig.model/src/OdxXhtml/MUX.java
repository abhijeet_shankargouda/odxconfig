/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUX</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MUX#getBYTEPOSITION <em>BYTEPOSITION</em>}</li>
 *   <li>{@link OdxXhtml.MUX#getSWITCHKEY <em>SWITCHKEY</em>}</li>
 *   <li>{@link OdxXhtml.MUX#getDEFAULTCASE <em>DEFAULTCASE</em>}</li>
 *   <li>{@link OdxXhtml.MUX#getCASES <em>CASES</em>}</li>
 *   <li>{@link OdxXhtml.MUX#isISVISIBLE <em>ISVISIBLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMUX()
 * @model extendedMetaData="name='MUX' kind='elementOnly'"
 * @generated
 */
public interface MUX extends COMPLEXDOP {
	/**
	 * Returns the value of the '<em><b>BYTEPOSITION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BYTEPOSITION</em>' attribute.
	 * @see #isSetBYTEPOSITION()
	 * @see #unsetBYTEPOSITION()
	 * @see #setBYTEPOSITION(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getMUX_BYTEPOSITION()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='BYTE-POSITION' namespace='##targetNamespace'"
	 * @generated
	 */
	long getBYTEPOSITION();

	/**
	 * Sets the value of the '{@link OdxXhtml.MUX#getBYTEPOSITION <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BYTEPOSITION</em>' attribute.
	 * @see #isSetBYTEPOSITION()
	 * @see #unsetBYTEPOSITION()
	 * @see #getBYTEPOSITION()
	 * @generated
	 */
	void setBYTEPOSITION(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MUX#getBYTEPOSITION <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBYTEPOSITION()
	 * @see #getBYTEPOSITION()
	 * @see #setBYTEPOSITION(long)
	 * @generated
	 */
	void unsetBYTEPOSITION();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MUX#getBYTEPOSITION <em>BYTEPOSITION</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BYTEPOSITION</em>' attribute is set.
	 * @see #unsetBYTEPOSITION()
	 * @see #getBYTEPOSITION()
	 * @see #setBYTEPOSITION(long)
	 * @generated
	 */
	boolean isSetBYTEPOSITION();

	/**
	 * Returns the value of the '<em><b>SWITCHKEY</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SWITCHKEY</em>' containment reference.
	 * @see #setSWITCHKEY(SWITCHKEY)
	 * @see OdxXhtml.OdxXhtmlPackage#getMUX_SWITCHKEY()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SWITCH-KEY' namespace='##targetNamespace'"
	 * @generated
	 */
	SWITCHKEY getSWITCHKEY();

	/**
	 * Sets the value of the '{@link OdxXhtml.MUX#getSWITCHKEY <em>SWITCHKEY</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SWITCHKEY</em>' containment reference.
	 * @see #getSWITCHKEY()
	 * @generated
	 */
	void setSWITCHKEY(SWITCHKEY value);

	/**
	 * Returns the value of the '<em><b>DEFAULTCASE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DEFAULTCASE</em>' containment reference.
	 * @see #setDEFAULTCASE(DEFAULTCASE)
	 * @see OdxXhtml.OdxXhtmlPackage#getMUX_DEFAULTCASE()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DEFAULT-CASE' namespace='##targetNamespace'"
	 * @generated
	 */
	DEFAULTCASE getDEFAULTCASE();

	/**
	 * Sets the value of the '{@link OdxXhtml.MUX#getDEFAULTCASE <em>DEFAULTCASE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DEFAULTCASE</em>' containment reference.
	 * @see #getDEFAULTCASE()
	 * @generated
	 */
	void setDEFAULTCASE(DEFAULTCASE value);

	/**
	 * Returns the value of the '<em><b>CASES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CASES</em>' containment reference.
	 * @see #setCASES(CASES)
	 * @see OdxXhtml.OdxXhtmlPackage#getMUX_CASES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='CASES' namespace='##targetNamespace'"
	 * @generated
	 */
	CASES getCASES();

	/**
	 * Sets the value of the '{@link OdxXhtml.MUX#getCASES <em>CASES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CASES</em>' containment reference.
	 * @see #getCASES()
	 * @generated
	 */
	void setCASES(CASES value);

	/**
	 * Returns the value of the '<em><b>ISVISIBLE</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISVISIBLE</em>' attribute.
	 * @see #isSetISVISIBLE()
	 * @see #unsetISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getMUX_ISVISIBLE()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-VISIBLE' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISVISIBLE();

	/**
	 * Sets the value of the '{@link OdxXhtml.MUX#isISVISIBLE <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISVISIBLE</em>' attribute.
	 * @see #isSetISVISIBLE()
	 * @see #unsetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @generated
	 */
	void setISVISIBLE(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MUX#isISVISIBLE <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @generated
	 */
	void unsetISVISIBLE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MUX#isISVISIBLE <em>ISVISIBLE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISVISIBLE</em>' attribute is set.
	 * @see #unsetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @generated
	 */
	boolean isSetISVISIBLE();

} // MUX
