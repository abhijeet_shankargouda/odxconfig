/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPUPHYSTOINTERNAL</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPUPHYSTOINTERNAL#getPROGCODE <em>PROGCODE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUPHYSTOINTERNAL()
 * @model extendedMetaData="name='COMPU-PHYS-TO-INTERNAL' kind='elementOnly'"
 * @generated
 */
public interface COMPUPHYSTOINTERNAL extends EObject {
	/**
	 * Returns the value of the '<em><b>PROGCODE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROGCODE</em>' containment reference.
	 * @see #setPROGCODE(PROGCODE)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUPHYSTOINTERNAL_PROGCODE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PROG-CODE' namespace='##targetNamespace'"
	 * @generated
	 */
	PROGCODE getPROGCODE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUPHYSTOINTERNAL#getPROGCODE <em>PROGCODE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROGCODE</em>' containment reference.
	 * @see #getPROGCODE()
	 * @generated
	 */
	void setPROGCODE(PROGCODE value);

} // COMPUPHYSTOINTERNAL
