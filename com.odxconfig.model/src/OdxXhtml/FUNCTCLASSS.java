/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FUNCTCLASSS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FUNCTCLASSS#getFUNCTCLASS <em>FUNCTCLASS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTCLASSS()
 * @model extendedMetaData="name='FUNCT-CLASSS' kind='elementOnly'"
 * @generated
 */
public interface FUNCTCLASSS extends EObject {
	/**
	 * Returns the value of the '<em><b>FUNCTCLASS</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.FUNCTCLASS}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FUNCTCLASS</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTCLASSS_FUNCTCLASS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FUNCT-CLASS' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FUNCTCLASS> getFUNCTCLASS();

} // FUNCTCLASSS
