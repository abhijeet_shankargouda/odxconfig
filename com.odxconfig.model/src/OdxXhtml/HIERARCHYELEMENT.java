/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HIERARCHYELEMENT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.HIERARCHYELEMENT#getCOMPARAMREFS <em>COMPARAMREFS</em>}</li>
 *   <li>{@link OdxXhtml.HIERARCHYELEMENT#getIMPORTREFS <em>IMPORTREFS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getHIERARCHYELEMENT()
 * @model extendedMetaData="name='HIERARCHY-ELEMENT' kind='elementOnly'"
 * @generated
 */
public interface HIERARCHYELEMENT extends DIAGLAYER {
	/**
	 * Returns the value of the '<em><b>COMPARAMREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPARAMREFS</em>' containment reference.
	 * @see #setCOMPARAMREFS(COMPARAMREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getHIERARCHYELEMENT_COMPARAMREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPARAM-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPARAMREFS getCOMPARAMREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.HIERARCHYELEMENT#getCOMPARAMREFS <em>COMPARAMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPARAMREFS</em>' containment reference.
	 * @see #getCOMPARAMREFS()
	 * @generated
	 */
	void setCOMPARAMREFS(COMPARAMREFS value);

	/**
	 * Returns the value of the '<em><b>IMPORTREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IMPORTREFS</em>' containment reference.
	 * @see #setIMPORTREFS(IMPORTREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getHIERARCHYELEMENT_IMPORTREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='IMPORT-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	IMPORTREFS getIMPORTREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.HIERARCHYELEMENT#getIMPORTREFS <em>IMPORTREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IMPORTREFS</em>' containment reference.
	 * @see #getIMPORTREFS()
	 * @generated
	 */
	void setIMPORTREFS(IMPORTREFS value);

} // HIERARCHYELEMENT
