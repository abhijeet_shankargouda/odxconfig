/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYREVISIONINFO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYREVISIONINFO#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYREVISIONINFO#getREVISIONLABEL <em>REVISIONLABEL</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYREVISIONINFO#getSTATE <em>STATE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFO()
 * @model extendedMetaData="name='COMPANY-REVISION-INFO' kind='elementOnly'"
 * @generated
 */
public interface COMPANYREVISIONINFO extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPANYDATAREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYDATAREF</em>' containment reference.
	 * @see #setCOMPANYDATAREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFO_COMPANYDATAREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPANY-DATA-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getCOMPANYDATAREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYREVISIONINFO#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYDATAREF</em>' containment reference.
	 * @see #getCOMPANYDATAREF()
	 * @generated
	 */
	void setCOMPANYDATAREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>REVISIONLABEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REVISIONLABEL</em>' attribute.
	 * @see #setREVISIONLABEL(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFO_REVISIONLABEL()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='REVISION-LABEL' namespace='##targetNamespace'"
	 * @generated
	 */
	String getREVISIONLABEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYREVISIONINFO#getREVISIONLABEL <em>REVISIONLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REVISIONLABEL</em>' attribute.
	 * @see #getREVISIONLABEL()
	 * @generated
	 */
	void setREVISIONLABEL(String value);

	/**
	 * Returns the value of the '<em><b>STATE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STATE</em>' attribute.
	 * @see #setSTATE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFO_STATE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='STATE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSTATE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYREVISIONINFO#getSTATE <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>STATE</em>' attribute.
	 * @see #getSTATE()
	 * @generated
	 */
	void setSTATE(String value);

} // COMPANYREVISIONINFO
