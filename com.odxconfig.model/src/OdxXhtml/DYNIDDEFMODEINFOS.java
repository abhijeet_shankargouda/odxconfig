/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DYNIDDEFMODEINFOS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFOS#getDYNIDDEFMODEINFO <em>DYNIDDEFMODEINFO</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFOS()
 * @model extendedMetaData="name='DYN-ID-DEF-MODE-INFOS' kind='elementOnly'"
 * @generated
 */
public interface DYNIDDEFMODEINFOS extends EObject {
	/**
	 * Returns the value of the '<em><b>DYNIDDEFMODEINFO</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DYNIDDEFMODEINFO}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DYNIDDEFMODEINFO</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFOS_DYNIDDEFMODEINFO()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DYN-ID-DEF-MODE-INFO' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DYNIDDEFMODEINFO> getDYNIDDEFMODEINFO();

} // DYNIDDEFMODEINFOS
