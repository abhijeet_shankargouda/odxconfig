/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PARENTREF</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PARENTREF#getNOTINHERITEDDIAGCOMMS <em>NOTINHERITEDDIAGCOMMS</em>}</li>
 *   <li>{@link OdxXhtml.PARENTREF#getNOTINHERITEDVARIABLES <em>NOTINHERITEDVARIABLES</em>}</li>
 *   <li>{@link OdxXhtml.PARENTREF#getDOCREF <em>DOCREF</em>}</li>
 *   <li>{@link OdxXhtml.PARENTREF#getDOCTYPE <em>DOCTYPE</em>}</li>
 *   <li>{@link OdxXhtml.PARENTREF#getIDREF <em>IDREF</em>}</li>
 *   <li>{@link OdxXhtml.PARENTREF#getREVISION <em>REVISION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPARENTREF()
 * @model extendedMetaData="name='PARENT-REF' kind='elementOnly'"
 * @generated
 */
public interface PARENTREF extends EObject {
	/**
	 * Returns the value of the '<em><b>NOTINHERITEDDIAGCOMMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NOTINHERITEDDIAGCOMMS</em>' containment reference.
	 * @see #setNOTINHERITEDDIAGCOMMS(NOTINHERITEDDIAGCOMMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getPARENTREF_NOTINHERITEDDIAGCOMMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='NOT-INHERITED-DIAG-COMMS' namespace='##targetNamespace'"
	 * @generated
	 */
	NOTINHERITEDDIAGCOMMS getNOTINHERITEDDIAGCOMMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.PARENTREF#getNOTINHERITEDDIAGCOMMS <em>NOTINHERITEDDIAGCOMMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NOTINHERITEDDIAGCOMMS</em>' containment reference.
	 * @see #getNOTINHERITEDDIAGCOMMS()
	 * @generated
	 */
	void setNOTINHERITEDDIAGCOMMS(NOTINHERITEDDIAGCOMMS value);

	/**
	 * Returns the value of the '<em><b>NOTINHERITEDVARIABLES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NOTINHERITEDVARIABLES</em>' containment reference.
	 * @see #setNOTINHERITEDVARIABLES(NOTINHERITEDVARIABLES)
	 * @see OdxXhtml.OdxXhtmlPackage#getPARENTREF_NOTINHERITEDVARIABLES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='NOT-INHERITED-VARIABLES' namespace='##targetNamespace'"
	 * @generated
	 */
	NOTINHERITEDVARIABLES getNOTINHERITEDVARIABLES();

	/**
	 * Sets the value of the '{@link OdxXhtml.PARENTREF#getNOTINHERITEDVARIABLES <em>NOTINHERITEDVARIABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NOTINHERITEDVARIABLES</em>' containment reference.
	 * @see #getNOTINHERITEDVARIABLES()
	 * @generated
	 */
	void setNOTINHERITEDVARIABLES(NOTINHERITEDVARIABLES value);

	/**
	 * Returns the value of the '<em><b>DOCREF</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCREF</em>' attribute.
	 * @see #setDOCREF(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPARENTREF_DOCREF()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='DOCREF' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDOCREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.PARENTREF#getDOCREF <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOCREF</em>' attribute.
	 * @see #getDOCREF()
	 * @generated
	 */
	void setDOCREF(String value);

	/**
	 * Returns the value of the '<em><b>DOCTYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.DOCTYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCTYPE</em>' attribute.
	 * @see OdxXhtml.DOCTYPE
	 * @see #isSetDOCTYPE()
	 * @see #unsetDOCTYPE()
	 * @see #setDOCTYPE(DOCTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getPARENTREF_DOCTYPE()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='DOCTYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	DOCTYPE getDOCTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PARENTREF#getDOCTYPE <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOCTYPE</em>' attribute.
	 * @see OdxXhtml.DOCTYPE
	 * @see #isSetDOCTYPE()
	 * @see #unsetDOCTYPE()
	 * @see #getDOCTYPE()
	 * @generated
	 */
	void setDOCTYPE(DOCTYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PARENTREF#getDOCTYPE <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDOCTYPE()
	 * @see #getDOCTYPE()
	 * @see #setDOCTYPE(DOCTYPE)
	 * @generated
	 */
	void unsetDOCTYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PARENTREF#getDOCTYPE <em>DOCTYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>DOCTYPE</em>' attribute is set.
	 * @see #unsetDOCTYPE()
	 * @see #getDOCTYPE()
	 * @see #setDOCTYPE(DOCTYPE)
	 * @generated
	 */
	boolean isSetDOCTYPE();

	/**
	 * Returns the value of the '<em><b>IDREF</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IDREF</em>' attribute.
	 * @see #setIDREF(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPARENTREF_IDREF()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='ID-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	String getIDREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.PARENTREF#getIDREF <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IDREF</em>' attribute.
	 * @see #getIDREF()
	 * @generated
	 */
	void setIDREF(String value);

	/**
	 * Returns the value of the '<em><b>REVISION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REVISION</em>' attribute.
	 * @see #setREVISION(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPARENTREF_REVISION()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='REVISION' namespace='##targetNamespace'"
	 * @generated
	 */
	String getREVISION();

	/**
	 * Sets the value of the '{@link OdxXhtml.PARENTREF#getREVISION <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REVISION</em>' attribute.
	 * @see #getREVISION()
	 * @generated
	 */
	void setREVISION(String value);

} // PARENTREF
