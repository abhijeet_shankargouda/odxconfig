/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RELATEDDOCS1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.RELATEDDOCS1#getRELATEDDOC <em>RELATEDDOC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDOCS1()
 * @model extendedMetaData="name='RELATED-DOCS' kind='elementOnly'"
 * @generated
 */
public interface RELATEDDOCS1 extends EObject {
	/**
	 * Returns the value of the '<em><b>RELATEDDOC</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.RELATEDDOC1}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RELATEDDOC</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDOCS1_RELATEDDOC()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='RELATED-DOC' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<RELATEDDOC1> getRELATEDDOC();

} // RELATEDDOCS1
