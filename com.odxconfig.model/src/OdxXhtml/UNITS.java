/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UNITS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.UNITS#getUNIT <em>UNIT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getUNITS()
 * @model extendedMetaData="name='UNITS' kind='elementOnly'"
 * @generated
 */
public interface UNITS extends EObject {
	/**
	 * Returns the value of the '<em><b>UNIT</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.UNIT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNIT</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITS_UNIT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='UNIT' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<UNIT> getUNIT();

} // UNITS
