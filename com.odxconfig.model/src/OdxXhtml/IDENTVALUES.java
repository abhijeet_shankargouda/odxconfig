/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IDENTVALUES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.IDENTVALUES#getIDENTVALUE <em>IDENTVALUE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getIDENTVALUES()
 * @model extendedMetaData="name='IDENT-VALUES' kind='elementOnly'"
 * @generated
 */
public interface IDENTVALUES extends EObject {
	/**
	 * Returns the value of the '<em><b>IDENTVALUE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.IDENTVALUE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IDENTVALUE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getIDENTVALUES_IDENTVALUE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='IDENT-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<IDENTVALUE> getIDENTVALUE();

} // IDENTVALUES
