/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FLASHCLASSS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FLASHCLASSS#getFLASHCLASS <em>FLASHCLASS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFLASHCLASSS()
 * @model extendedMetaData="name='FLASH-CLASSS' kind='elementOnly'"
 * @generated
 */
public interface FLASHCLASSS extends EObject {
	/**
	 * Returns the value of the '<em><b>FLASHCLASS</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.FLASHCLASS}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FLASHCLASS</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHCLASSS_FLASHCLASS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FLASH-CLASS' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FLASHCLASS> getFLASHCLASS();

} // FLASHCLASSS
