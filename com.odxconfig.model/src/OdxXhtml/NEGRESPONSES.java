/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NEGRESPONSES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.NEGRESPONSES#getNEGRESPONSE <em>NEGRESPONSE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getNEGRESPONSES()
 * @model extendedMetaData="name='NEG-RESPONSES' kind='elementOnly'"
 * @generated
 */
public interface NEGRESPONSES extends EObject {
	/**
	 * Returns the value of the '<em><b>NEGRESPONSE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.NEGRESPONSE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NEGRESPONSE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getNEGRESPONSES_NEGRESPONSE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='NEG-RESPONSE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<NEGRESPONSE> getNEGRESPONSE();

} // NEGRESPONSES
