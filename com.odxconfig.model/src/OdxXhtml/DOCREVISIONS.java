/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DOCREVISIONS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DOCREVISIONS#getDOCREVISION <em>DOCREVISION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISIONS()
 * @model extendedMetaData="name='DOC-REVISIONS' kind='elementOnly'"
 * @generated
 */
public interface DOCREVISIONS extends EObject {
	/**
	 * Returns the value of the '<em><b>DOCREVISION</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DOCREVISION}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCREVISION</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISIONS_DOCREVISION()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DOC-REVISION' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DOCREVISION> getDOCREVISION();

} // DOCREVISIONS
