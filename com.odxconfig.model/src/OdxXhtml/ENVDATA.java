/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ENVDATA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ENVDATA#getALLVALUE <em>ALLVALUE</em>}</li>
 *   <li>{@link OdxXhtml.ENVDATA#getDTCVALUES <em>DTCVALUES</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getENVDATA()
 * @model extendedMetaData="name='ENV-DATA' kind='elementOnly'"
 * @generated
 */
public interface ENVDATA extends BASICSTRUCTURE {
	/**
	 * Returns the value of the '<em><b>ALLVALUE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ALLVALUE</em>' containment reference.
	 * @see #setALLVALUE(ALLVALUE)
	 * @see OdxXhtml.OdxXhtmlPackage#getENVDATA_ALLVALUE()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ALL-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	ALLVALUE getALLVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.ENVDATA#getALLVALUE <em>ALLVALUE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ALLVALUE</em>' containment reference.
	 * @see #getALLVALUE()
	 * @generated
	 */
	void setALLVALUE(ALLVALUE value);

	/**
	 * Returns the value of the '<em><b>DTCVALUES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DTCVALUES</em>' containment reference.
	 * @see #setDTCVALUES(DTCVALUES)
	 * @see OdxXhtml.OdxXhtmlPackage#getENVDATA_DTCVALUES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DTC-VALUES' namespace='##targetNamespace'"
	 * @generated
	 */
	DTCVALUES getDTCVALUES();

	/**
	 * Sets the value of the '{@link OdxXhtml.ENVDATA#getDTCVALUES <em>DTCVALUES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DTCVALUES</em>' containment reference.
	 * @see #getDTCVALUES()
	 * @generated
	 */
	void setDTCVALUES(DTCVALUES value);

} // ENVDATA
