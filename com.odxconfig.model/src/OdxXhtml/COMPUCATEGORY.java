/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>COMPUCATEGORY</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUCATEGORY()
 * @model extendedMetaData="name='COMPU-CATEGORY'"
 * @generated
 */
public enum COMPUCATEGORY implements Enumerator {
	/**
	 * The '<em><b>IDENTICAL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IDENTICAL_VALUE
	 * @generated
	 * @ordered
	 */
	IDENTICAL(0, "IDENTICAL", "IDENTICAL"),

	/**
	 * The '<em><b>LINEAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LINEAR_VALUE
	 * @generated
	 * @ordered
	 */
	LINEAR(1, "LINEAR", "LINEAR"),

	/**
	 * The '<em><b>SCALELINEAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCALELINEAR_VALUE
	 * @generated
	 * @ordered
	 */
	SCALELINEAR(2, "SCALELINEAR", "SCALE-LINEAR"),

	/**
	 * The '<em><b>TEXTTABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEXTTABLE_VALUE
	 * @generated
	 * @ordered
	 */
	TEXTTABLE(3, "TEXTTABLE", "TEXTTABLE"),

	/**
	 * The '<em><b>COMPUCODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPUCODE_VALUE
	 * @generated
	 * @ordered
	 */
	COMPUCODE(4, "COMPUCODE", "COMPUCODE"),

	/**
	 * The '<em><b>TABINTP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TABINTP_VALUE
	 * @generated
	 * @ordered
	 */
	TABINTP(5, "TABINTP", "TAB-INTP"),

	/**
	 * The '<em><b>RATFUNC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RATFUNC_VALUE
	 * @generated
	 * @ordered
	 */
	RATFUNC(6, "RATFUNC", "RAT-FUNC"),

	/**
	 * The '<em><b>SCALERATFUNC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCALERATFUNC_VALUE
	 * @generated
	 * @ordered
	 */
	SCALERATFUNC(7, "SCALERATFUNC", "SCALE-RAT-FUNC");

	/**
	 * The '<em><b>IDENTICAL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IDENTICAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IDENTICAL_VALUE = 0;

	/**
	 * The '<em><b>LINEAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LINEAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LINEAR_VALUE = 1;

	/**
	 * The '<em><b>SCALELINEAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCALELINEAR
	 * @model literal="SCALE-LINEAR"
	 * @generated
	 * @ordered
	 */
	public static final int SCALELINEAR_VALUE = 2;

	/**
	 * The '<em><b>TEXTTABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEXTTABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TEXTTABLE_VALUE = 3;

	/**
	 * The '<em><b>COMPUCODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPUCODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int COMPUCODE_VALUE = 4;

	/**
	 * The '<em><b>TABINTP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TABINTP
	 * @model literal="TAB-INTP"
	 * @generated
	 * @ordered
	 */
	public static final int TABINTP_VALUE = 5;

	/**
	 * The '<em><b>RATFUNC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RATFUNC
	 * @model literal="RAT-FUNC"
	 * @generated
	 * @ordered
	 */
	public static final int RATFUNC_VALUE = 6;

	/**
	 * The '<em><b>SCALERATFUNC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SCALERATFUNC
	 * @model literal="SCALE-RAT-FUNC"
	 * @generated
	 * @ordered
	 */
	public static final int SCALERATFUNC_VALUE = 7;

	/**
	 * An array of all the '<em><b>COMPUCATEGORY</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final COMPUCATEGORY[] VALUES_ARRAY =
		new COMPUCATEGORY[] {
			IDENTICAL,
			LINEAR,
			SCALELINEAR,
			TEXTTABLE,
			COMPUCODE,
			TABINTP,
			RATFUNC,
			SCALERATFUNC,
		};

	/**
	 * A public read-only list of all the '<em><b>COMPUCATEGORY</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<COMPUCATEGORY> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>COMPUCATEGORY</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static COMPUCATEGORY get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			COMPUCATEGORY result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>COMPUCATEGORY</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static COMPUCATEGORY getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			COMPUCATEGORY result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>COMPUCATEGORY</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static COMPUCATEGORY get(int value) {
		switch (value) {
			case IDENTICAL_VALUE: return IDENTICAL;
			case LINEAR_VALUE: return LINEAR;
			case SCALELINEAR_VALUE: return SCALELINEAR;
			case TEXTTABLE_VALUE: return TEXTTABLE;
			case COMPUCODE_VALUE: return COMPUCODE;
			case TABINTP_VALUE: return TABINTP;
			case RATFUNC_VALUE: return RATFUNC;
			case SCALERATFUNC_VALUE: return SCALERATFUNC;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private COMPUCATEGORY(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //COMPUCATEGORY
