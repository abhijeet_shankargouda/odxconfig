/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GATEWAYLOGICALLINKREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.GATEWAYLOGICALLINKREFS#getGATEWAYLOGICALLINKREF <em>GATEWAYLOGICALLINKREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getGATEWAYLOGICALLINKREFS()
 * @model extendedMetaData="name='GATEWAY-LOGICAL-LINK-REFS' kind='elementOnly'"
 * @generated
 */
public interface GATEWAYLOGICALLINKREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>GATEWAYLOGICALLINKREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>GATEWAYLOGICALLINKREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getGATEWAYLOGICALLINKREFS_GATEWAYLOGICALLINKREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='GATEWAY-LOGICAL-LINK-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getGATEWAYLOGICALLINKREF();

} // GATEWAYLOGICALLINKREFS
