/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGDATADICTIONARYSPEC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getDTCDOPS <em>DTCDOPS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getENVDATADESCS <em>ENVDATADESCS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getDATAOBJECTPROPS <em>DATAOBJECTPROPS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getSTRUCTURES <em>STRUCTURES</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getSTATICFIELDS <em>STATICFIELDS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getDYNAMICLENGTHFIELDS <em>DYNAMICLENGTHFIELDS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getDYNAMICENDMARKERFIELDS <em>DYNAMICENDMARKERFIELDS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getENDOFPDUFIELDS <em>ENDOFPDUFIELDS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getMUXS <em>MUXS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getENVDATAS <em>ENVDATAS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getUNITSPEC <em>UNITSPEC</em>}</li>
 *   <li>{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getTABLES <em>TABLES</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC()
 * @model extendedMetaData="name='DIAG-DATA-DICTIONARY-SPEC' kind='elementOnly'"
 * @generated
 */
public interface DIAGDATADICTIONARYSPEC extends EObject {
	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA value);

	/**
	 * Returns the value of the '<em><b>DTCDOPS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DTCDOPS</em>' containment reference.
	 * @see #setDTCDOPS(DTCDOPS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_DTCDOPS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DTC-DOPS' namespace='##targetNamespace'"
	 * @generated
	 */
	DTCDOPS getDTCDOPS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getDTCDOPS <em>DTCDOPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DTCDOPS</em>' containment reference.
	 * @see #getDTCDOPS()
	 * @generated
	 */
	void setDTCDOPS(DTCDOPS value);

	/**
	 * Returns the value of the '<em><b>ENVDATADESCS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENVDATADESCS</em>' containment reference.
	 * @see #setENVDATADESCS(ENVDATADESCS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_ENVDATADESCS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ENV-DATA-DESCS' namespace='##targetNamespace'"
	 * @generated
	 */
	ENVDATADESCS getENVDATADESCS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getENVDATADESCS <em>ENVDATADESCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ENVDATADESCS</em>' containment reference.
	 * @see #getENVDATADESCS()
	 * @generated
	 */
	void setENVDATADESCS(ENVDATADESCS value);

	/**
	 * Returns the value of the '<em><b>DATAOBJECTPROPS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATAOBJECTPROPS</em>' containment reference.
	 * @see #setDATAOBJECTPROPS(DATAOBJECTPROPS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_DATAOBJECTPROPS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DATA-OBJECT-PROPS' namespace='##targetNamespace'"
	 * @generated
	 */
	DATAOBJECTPROPS getDATAOBJECTPROPS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getDATAOBJECTPROPS <em>DATAOBJECTPROPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATAOBJECTPROPS</em>' containment reference.
	 * @see #getDATAOBJECTPROPS()
	 * @generated
	 */
	void setDATAOBJECTPROPS(DATAOBJECTPROPS value);

	/**
	 * Returns the value of the '<em><b>STRUCTURES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STRUCTURES</em>' containment reference.
	 * @see #setSTRUCTURES(STRUCTURES)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_STRUCTURES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='STRUCTURES' namespace='##targetNamespace'"
	 * @generated
	 */
	STRUCTURES getSTRUCTURES();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getSTRUCTURES <em>STRUCTURES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>STRUCTURES</em>' containment reference.
	 * @see #getSTRUCTURES()
	 * @generated
	 */
	void setSTRUCTURES(STRUCTURES value);

	/**
	 * Returns the value of the '<em><b>STATICFIELDS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STATICFIELDS</em>' containment reference.
	 * @see #setSTATICFIELDS(STATICFIELDS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_STATICFIELDS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='STATIC-FIELDS' namespace='##targetNamespace'"
	 * @generated
	 */
	STATICFIELDS getSTATICFIELDS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getSTATICFIELDS <em>STATICFIELDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>STATICFIELDS</em>' containment reference.
	 * @see #getSTATICFIELDS()
	 * @generated
	 */
	void setSTATICFIELDS(STATICFIELDS value);

	/**
	 * Returns the value of the '<em><b>DYNAMICLENGTHFIELDS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DYNAMICLENGTHFIELDS</em>' containment reference.
	 * @see #setDYNAMICLENGTHFIELDS(DYNAMICLENGTHFIELDS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_DYNAMICLENGTHFIELDS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DYNAMIC-LENGTH-FIELDS' namespace='##targetNamespace'"
	 * @generated
	 */
	DYNAMICLENGTHFIELDS getDYNAMICLENGTHFIELDS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getDYNAMICLENGTHFIELDS <em>DYNAMICLENGTHFIELDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DYNAMICLENGTHFIELDS</em>' containment reference.
	 * @see #getDYNAMICLENGTHFIELDS()
	 * @generated
	 */
	void setDYNAMICLENGTHFIELDS(DYNAMICLENGTHFIELDS value);

	/**
	 * Returns the value of the '<em><b>DYNAMICENDMARKERFIELDS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DYNAMICENDMARKERFIELDS</em>' containment reference.
	 * @see #setDYNAMICENDMARKERFIELDS(DYNAMICENDMARKERFIELDS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_DYNAMICENDMARKERFIELDS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DYNAMIC-ENDMARKER-FIELDS' namespace='##targetNamespace'"
	 * @generated
	 */
	DYNAMICENDMARKERFIELDS getDYNAMICENDMARKERFIELDS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getDYNAMICENDMARKERFIELDS <em>DYNAMICENDMARKERFIELDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DYNAMICENDMARKERFIELDS</em>' containment reference.
	 * @see #getDYNAMICENDMARKERFIELDS()
	 * @generated
	 */
	void setDYNAMICENDMARKERFIELDS(DYNAMICENDMARKERFIELDS value);

	/**
	 * Returns the value of the '<em><b>ENDOFPDUFIELDS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENDOFPDUFIELDS</em>' containment reference.
	 * @see #setENDOFPDUFIELDS(ENDOFPDUFIELDS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_ENDOFPDUFIELDS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='END-OF-PDU-FIELDS' namespace='##targetNamespace'"
	 * @generated
	 */
	ENDOFPDUFIELDS getENDOFPDUFIELDS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getENDOFPDUFIELDS <em>ENDOFPDUFIELDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ENDOFPDUFIELDS</em>' containment reference.
	 * @see #getENDOFPDUFIELDS()
	 * @generated
	 */
	void setENDOFPDUFIELDS(ENDOFPDUFIELDS value);

	/**
	 * Returns the value of the '<em><b>MUXS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MUXS</em>' containment reference.
	 * @see #setMUXS(MUXS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_MUXS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MUXS' namespace='##targetNamespace'"
	 * @generated
	 */
	MUXS getMUXS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getMUXS <em>MUXS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MUXS</em>' containment reference.
	 * @see #getMUXS()
	 * @generated
	 */
	void setMUXS(MUXS value);

	/**
	 * Returns the value of the '<em><b>ENVDATAS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENVDATAS</em>' containment reference.
	 * @see #setENVDATAS(ENVDATAS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_ENVDATAS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ENV-DATAS' namespace='##targetNamespace'"
	 * @generated
	 */
	ENVDATAS getENVDATAS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getENVDATAS <em>ENVDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ENVDATAS</em>' containment reference.
	 * @see #getENVDATAS()
	 * @generated
	 */
	void setENVDATAS(ENVDATAS value);

	/**
	 * Returns the value of the '<em><b>UNITSPEC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNITSPEC</em>' containment reference.
	 * @see #setUNITSPEC(UNITSPEC)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_UNITSPEC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UNIT-SPEC' namespace='##targetNamespace'"
	 * @generated
	 */
	UNITSPEC getUNITSPEC();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getUNITSPEC <em>UNITSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UNITSPEC</em>' containment reference.
	 * @see #getUNITSPEC()
	 * @generated
	 */
	void setUNITSPEC(UNITSPEC value);

	/**
	 * Returns the value of the '<em><b>TABLES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TABLES</em>' containment reference.
	 * @see #setTABLES(TABLES)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGDATADICTIONARYSPEC_TABLES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TABLES' namespace='##targetNamespace'"
	 * @generated
	 */
	TABLES getTABLES();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGDATADICTIONARYSPEC#getTABLES <em>TABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TABLES</em>' containment reference.
	 * @see #getTABLES()
	 * @generated
	 */
	void setTABLES(TABLES value);

} // DIAGDATADICTIONARYSPEC
