/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ABLOCKS Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ABLOCKSType#getABLOCK <em>ABLOCK</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getABLOCKSType()
 * @model extendedMetaData="name='ABLOCKS_._type' kind='elementOnly'"
 * @generated
 */
public interface ABLOCKSType extends EObject {
	/**
	 * Returns the value of the '<em><b>ABLOCK</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ABLOCK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ABLOCK</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getABLOCKSType_ABLOCK()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ABLOCK' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ABLOCK> getABLOCK();

} // ABLOCKSType
