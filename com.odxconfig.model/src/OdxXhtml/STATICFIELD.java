/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>STATICFIELD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.STATICFIELD#getFIXEDNUMBEROFITEMS <em>FIXEDNUMBEROFITEMS</em>}</li>
 *   <li>{@link OdxXhtml.STATICFIELD#getITEMBYTESIZE <em>ITEMBYTESIZE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSTATICFIELD()
 * @model extendedMetaData="name='STATIC-FIELD' kind='elementOnly'"
 * @generated
 */
public interface STATICFIELD extends FIELD {
	/**
	 * Returns the value of the '<em><b>FIXEDNUMBEROFITEMS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FIXEDNUMBEROFITEMS</em>' attribute.
	 * @see #isSetFIXEDNUMBEROFITEMS()
	 * @see #unsetFIXEDNUMBEROFITEMS()
	 * @see #setFIXEDNUMBEROFITEMS(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getSTATICFIELD_FIXEDNUMBEROFITEMS()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='FIXED-NUMBER-OF-ITEMS' namespace='##targetNamespace'"
	 * @generated
	 */
	long getFIXEDNUMBEROFITEMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.STATICFIELD#getFIXEDNUMBEROFITEMS <em>FIXEDNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FIXEDNUMBEROFITEMS</em>' attribute.
	 * @see #isSetFIXEDNUMBEROFITEMS()
	 * @see #unsetFIXEDNUMBEROFITEMS()
	 * @see #getFIXEDNUMBEROFITEMS()
	 * @generated
	 */
	void setFIXEDNUMBEROFITEMS(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.STATICFIELD#getFIXEDNUMBEROFITEMS <em>FIXEDNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFIXEDNUMBEROFITEMS()
	 * @see #getFIXEDNUMBEROFITEMS()
	 * @see #setFIXEDNUMBEROFITEMS(long)
	 * @generated
	 */
	void unsetFIXEDNUMBEROFITEMS();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.STATICFIELD#getFIXEDNUMBEROFITEMS <em>FIXEDNUMBEROFITEMS</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>FIXEDNUMBEROFITEMS</em>' attribute is set.
	 * @see #unsetFIXEDNUMBEROFITEMS()
	 * @see #getFIXEDNUMBEROFITEMS()
	 * @see #setFIXEDNUMBEROFITEMS(long)
	 * @generated
	 */
	boolean isSetFIXEDNUMBEROFITEMS();

	/**
	 * Returns the value of the '<em><b>ITEMBYTESIZE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ITEMBYTESIZE</em>' attribute.
	 * @see #isSetITEMBYTESIZE()
	 * @see #unsetITEMBYTESIZE()
	 * @see #setITEMBYTESIZE(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getSTATICFIELD_ITEMBYTESIZE()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='ITEM-BYTE-SIZE' namespace='##targetNamespace'"
	 * @generated
	 */
	long getITEMBYTESIZE();

	/**
	 * Sets the value of the '{@link OdxXhtml.STATICFIELD#getITEMBYTESIZE <em>ITEMBYTESIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ITEMBYTESIZE</em>' attribute.
	 * @see #isSetITEMBYTESIZE()
	 * @see #unsetITEMBYTESIZE()
	 * @see #getITEMBYTESIZE()
	 * @generated
	 */
	void setITEMBYTESIZE(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.STATICFIELD#getITEMBYTESIZE <em>ITEMBYTESIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetITEMBYTESIZE()
	 * @see #getITEMBYTESIZE()
	 * @see #setITEMBYTESIZE(long)
	 * @generated
	 */
	void unsetITEMBYTESIZE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.STATICFIELD#getITEMBYTESIZE <em>ITEMBYTESIZE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ITEMBYTESIZE</em>' attribute is set.
	 * @see #unsetITEMBYTESIZE()
	 * @see #getITEMBYTESIZE()
	 * @see #setITEMBYTESIZE(long)
	 * @generated
	 */
	boolean isSetITEMBYTESIZE();

} // STATICFIELD
