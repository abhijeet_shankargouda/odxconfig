/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CODEDCONST</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.CODEDCONST#getCODEDVALUE <em>CODEDVALUE</em>}</li>
 *   <li>{@link OdxXhtml.CODEDCONST#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCODEDCONST()
 * @model extendedMetaData="name='CODED-CONST' kind='elementOnly'"
 * @generated
 */
public interface CODEDCONST extends POSITIONABLEPARAM {
	/**
	 * Returns the value of the '<em><b>CODEDVALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CODEDVALUE</em>' attribute.
	 * @see #setCODEDVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCODEDCONST_CODEDVALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='CODED-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCODEDVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.CODEDCONST#getCODEDVALUE <em>CODEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CODEDVALUE</em>' attribute.
	 * @see #getCODEDVALUE()
	 * @generated
	 */
	void setCODEDVALUE(String value);

	/**
	 * Returns the value of the '<em><b>DIAGCODEDTYPE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCODEDTYPE</em>' containment reference.
	 * @see #setDIAGCODEDTYPE(DIAGCODEDTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getCODEDCONST_DIAGCODEDTYPE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-CODED-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGCODEDTYPE getDIAGCODEDTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.CODEDCONST#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCODEDTYPE</em>' containment reference.
	 * @see #getDIAGCODEDTYPE()
	 * @generated
	 */
	void setDIAGCODEDTYPE(DIAGCODEDTYPE value);

} // CODEDCONST
