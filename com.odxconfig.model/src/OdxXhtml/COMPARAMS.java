/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPARAMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPARAMS#getCOMPARAM <em>COMPARAM</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMS()
 * @model extendedMetaData="name='COMPARAMS' kind='elementOnly'"
 * @generated
 */
public interface COMPARAMS extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPARAM</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.COMPARAM}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPARAM</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMS_COMPARAM()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPARAM' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<COMPARAM> getCOMPARAM();

} // COMPARAMS
