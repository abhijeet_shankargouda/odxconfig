/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CASES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.CASES#getCASE <em>CASE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCASES()
 * @model extendedMetaData="name='CASES' kind='elementOnly'"
 * @generated
 */
public interface CASES extends EObject {
	/**
	 * Returns the value of the '<em><b>CASE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.CASE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CASE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCASES_CASE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='CASE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<CASE> getCASE();

} // CASES
