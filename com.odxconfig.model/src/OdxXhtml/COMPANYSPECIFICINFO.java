/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYSPECIFICINFO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYSPECIFICINFO#getRELATEDDOCS <em>RELATEDDOCS</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYSPECIFICINFO#getSDGS <em>SDGS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYSPECIFICINFO()
 * @model extendedMetaData="name='COMPANY-SPECIFIC-INFO' kind='elementOnly'"
 * @generated
 */
public interface COMPANYSPECIFICINFO extends EObject {
	/**
	 * Returns the value of the '<em><b>RELATEDDOCS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RELATEDDOCS</em>' containment reference.
	 * @see #setRELATEDDOCS(RELATEDDOCS)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYSPECIFICINFO_RELATEDDOCS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RELATED-DOCS' namespace='##targetNamespace'"
	 * @generated
	 */
	RELATEDDOCS getRELATEDDOCS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYSPECIFICINFO#getRELATEDDOCS <em>RELATEDDOCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RELATEDDOCS</em>' containment reference.
	 * @see #getRELATEDDOCS()
	 * @generated
	 */
	void setRELATEDDOCS(RELATEDDOCS value);

	/**
	 * Returns the value of the '<em><b>SDGS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGS</em>' containment reference.
	 * @see #setSDGS(SDGS)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYSPECIFICINFO_SDGS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDGS' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGS getSDGS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYSPECIFICINFO#getSDGS <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGS</em>' containment reference.
	 * @see #getSDGS()
	 * @generated
	 */
	void setSDGS(SDGS value);

} // COMPANYSPECIFICINFO
