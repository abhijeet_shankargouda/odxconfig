/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SECURITY</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SECURITY#getSECURITYMETHOD <em>SECURITYMETHOD</em>}</li>
 *   <li>{@link OdxXhtml.SECURITY#getFWSIGNATURE <em>FWSIGNATURE</em>}</li>
 *   <li>{@link OdxXhtml.SECURITY#getFWCHECKSUM <em>FWCHECKSUM</em>}</li>
 *   <li>{@link OdxXhtml.SECURITY#getVALIDITYFOR <em>VALIDITYFOR</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSECURITY()
 * @model extendedMetaData="name='SECURITY' kind='elementOnly'"
 * @generated
 */
public interface SECURITY extends EObject {
	/**
	 * Returns the value of the '<em><b>SECURITYMETHOD</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SECURITYMETHOD</em>' containment reference.
	 * @see #setSECURITYMETHOD(SECURITYMETHOD)
	 * @see OdxXhtml.OdxXhtmlPackage#getSECURITY_SECURITYMETHOD()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SECURITY-METHOD' namespace='##targetNamespace'"
	 * @generated
	 */
	SECURITYMETHOD getSECURITYMETHOD();

	/**
	 * Sets the value of the '{@link OdxXhtml.SECURITY#getSECURITYMETHOD <em>SECURITYMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SECURITYMETHOD</em>' containment reference.
	 * @see #getSECURITYMETHOD()
	 * @generated
	 */
	void setSECURITYMETHOD(SECURITYMETHOD value);

	/**
	 * Returns the value of the '<em><b>FWSIGNATURE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FWSIGNATURE</em>' containment reference.
	 * @see #setFWSIGNATURE(FWSIGNATURE)
	 * @see OdxXhtml.OdxXhtmlPackage#getSECURITY_FWSIGNATURE()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FW-SIGNATURE' namespace='##targetNamespace'"
	 * @generated
	 */
	FWSIGNATURE getFWSIGNATURE();

	/**
	 * Sets the value of the '{@link OdxXhtml.SECURITY#getFWSIGNATURE <em>FWSIGNATURE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FWSIGNATURE</em>' containment reference.
	 * @see #getFWSIGNATURE()
	 * @generated
	 */
	void setFWSIGNATURE(FWSIGNATURE value);

	/**
	 * Returns the value of the '<em><b>FWCHECKSUM</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FWCHECKSUM</em>' containment reference.
	 * @see #setFWCHECKSUM(FWCHECKSUM)
	 * @see OdxXhtml.OdxXhtmlPackage#getSECURITY_FWCHECKSUM()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FW-CHECKSUM' namespace='##targetNamespace'"
	 * @generated
	 */
	FWCHECKSUM getFWCHECKSUM();

	/**
	 * Sets the value of the '{@link OdxXhtml.SECURITY#getFWCHECKSUM <em>FWCHECKSUM</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FWCHECKSUM</em>' containment reference.
	 * @see #getFWCHECKSUM()
	 * @generated
	 */
	void setFWCHECKSUM(FWCHECKSUM value);

	/**
	 * Returns the value of the '<em><b>VALIDITYFOR</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VALIDITYFOR</em>' containment reference.
	 * @see #setVALIDITYFOR(VALIDITYFOR)
	 * @see OdxXhtml.OdxXhtmlPackage#getSECURITY_VALIDITYFOR()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='VALIDITY-FOR' namespace='##targetNamespace'"
	 * @generated
	 */
	VALIDITYFOR getVALIDITYFOR();

	/**
	 * Sets the value of the '{@link OdxXhtml.SECURITY#getVALIDITYFOR <em>VALIDITYFOR</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VALIDITYFOR</em>' containment reference.
	 * @see #getVALIDITYFOR()
	 * @generated
	 */
	void setVALIDITYFOR(VALIDITYFOR value);

} // SECURITY
