/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DYNIDDEFMODEINFO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFO#getDEFMODE <em>DEFMODE</em>}</li>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFO#getCLEARDYNDEFMESSAGEREF <em>CLEARDYNDEFMESSAGEREF</em>}</li>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFO#getCLEARDYNDEFMESSAGESNREF <em>CLEARDYNDEFMESSAGESNREF</em>}</li>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFO#getREADDYNDEFMESSAGEREF <em>READDYNDEFMESSAGEREF</em>}</li>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFO#getREADDYNDEFMESSAGESNREF <em>READDYNDEFMESSAGESNREF</em>}</li>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFO#getDYNDEFMESSAGEREF <em>DYNDEFMESSAGEREF</em>}</li>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFO#getDYNDEFMESSAGESNREF <em>DYNDEFMESSAGESNREF</em>}</li>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFO#getSUPPORTEDDYNIDS <em>SUPPORTEDDYNIDS</em>}</li>
 *   <li>{@link OdxXhtml.DYNIDDEFMODEINFO#getSELECTIONTABLEREFS <em>SELECTIONTABLEREFS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO()
 * @model extendedMetaData="name='DYN-ID-DEF-MODE-INFO' kind='elementOnly'"
 * @generated
 */
public interface DYNIDDEFMODEINFO extends EObject {
	/**
	 * Returns the value of the '<em><b>DEFMODE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DEFMODE</em>' attribute.
	 * @see #setDEFMODE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO_DEFMODE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='DEF-MODE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDEFMODE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNIDDEFMODEINFO#getDEFMODE <em>DEFMODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DEFMODE</em>' attribute.
	 * @see #getDEFMODE()
	 * @generated
	 */
	void setDEFMODE(String value);

	/**
	 * Returns the value of the '<em><b>CLEARDYNDEFMESSAGEREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CLEARDYNDEFMESSAGEREF</em>' containment reference.
	 * @see #setCLEARDYNDEFMESSAGEREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO_CLEARDYNDEFMESSAGEREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='CLEAR-DYN-DEF-MESSAGE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getCLEARDYNDEFMESSAGEREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNIDDEFMODEINFO#getCLEARDYNDEFMESSAGEREF <em>CLEARDYNDEFMESSAGEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CLEARDYNDEFMESSAGEREF</em>' containment reference.
	 * @see #getCLEARDYNDEFMESSAGEREF()
	 * @generated
	 */
	void setCLEARDYNDEFMESSAGEREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>CLEARDYNDEFMESSAGESNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CLEARDYNDEFMESSAGESNREF</em>' containment reference.
	 * @see #setCLEARDYNDEFMESSAGESNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO_CLEARDYNDEFMESSAGESNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='CLEAR-DYN-DEF-MESSAGE-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getCLEARDYNDEFMESSAGESNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNIDDEFMODEINFO#getCLEARDYNDEFMESSAGESNREF <em>CLEARDYNDEFMESSAGESNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CLEARDYNDEFMESSAGESNREF</em>' containment reference.
	 * @see #getCLEARDYNDEFMESSAGESNREF()
	 * @generated
	 */
	void setCLEARDYNDEFMESSAGESNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>READDYNDEFMESSAGEREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>READDYNDEFMESSAGEREF</em>' containment reference.
	 * @see #setREADDYNDEFMESSAGEREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO_READDYNDEFMESSAGEREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='READ-DYN-DEF-MESSAGE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getREADDYNDEFMESSAGEREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNIDDEFMODEINFO#getREADDYNDEFMESSAGEREF <em>READDYNDEFMESSAGEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>READDYNDEFMESSAGEREF</em>' containment reference.
	 * @see #getREADDYNDEFMESSAGEREF()
	 * @generated
	 */
	void setREADDYNDEFMESSAGEREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>READDYNDEFMESSAGESNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>READDYNDEFMESSAGESNREF</em>' containment reference.
	 * @see #setREADDYNDEFMESSAGESNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO_READDYNDEFMESSAGESNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='READ-DYN-DEF-MESSAGE-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getREADDYNDEFMESSAGESNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNIDDEFMODEINFO#getREADDYNDEFMESSAGESNREF <em>READDYNDEFMESSAGESNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>READDYNDEFMESSAGESNREF</em>' containment reference.
	 * @see #getREADDYNDEFMESSAGESNREF()
	 * @generated
	 */
	void setREADDYNDEFMESSAGESNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>DYNDEFMESSAGEREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DYNDEFMESSAGEREF</em>' containment reference.
	 * @see #setDYNDEFMESSAGEREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO_DYNDEFMESSAGEREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DYN-DEF-MESSAGE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDYNDEFMESSAGEREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNIDDEFMODEINFO#getDYNDEFMESSAGEREF <em>DYNDEFMESSAGEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DYNDEFMESSAGEREF</em>' containment reference.
	 * @see #getDYNDEFMESSAGEREF()
	 * @generated
	 */
	void setDYNDEFMESSAGEREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>DYNDEFMESSAGESNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DYNDEFMESSAGESNREF</em>' containment reference.
	 * @see #setDYNDEFMESSAGESNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO_DYNDEFMESSAGESNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DYN-DEF-MESSAGE-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDYNDEFMESSAGESNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNIDDEFMODEINFO#getDYNDEFMESSAGESNREF <em>DYNDEFMESSAGESNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DYNDEFMESSAGESNREF</em>' containment reference.
	 * @see #getDYNDEFMESSAGESNREF()
	 * @generated
	 */
	void setDYNDEFMESSAGESNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>SUPPORTEDDYNIDS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SUPPORTEDDYNIDS</em>' containment reference.
	 * @see #setSUPPORTEDDYNIDS(SUPPORTEDDYNIDS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO_SUPPORTEDDYNIDS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SUPPORTED-DYN-IDS' namespace='##targetNamespace'"
	 * @generated
	 */
	SUPPORTEDDYNIDS getSUPPORTEDDYNIDS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNIDDEFMODEINFO#getSUPPORTEDDYNIDS <em>SUPPORTEDDYNIDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SUPPORTEDDYNIDS</em>' containment reference.
	 * @see #getSUPPORTEDDYNIDS()
	 * @generated
	 */
	void setSUPPORTEDDYNIDS(SUPPORTEDDYNIDS value);

	/**
	 * Returns the value of the '<em><b>SELECTIONTABLEREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SELECTIONTABLEREFS</em>' containment reference.
	 * @see #setSELECTIONTABLEREFS(SELECTIONTABLEREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNIDDEFMODEINFO_SELECTIONTABLEREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SELECTION-TABLE-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	SELECTIONTABLEREFS getSELECTIONTABLEREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNIDDEFMODEINFO#getSELECTIONTABLEREFS <em>SELECTIONTABLEREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SELECTIONTABLEREFS</em>' containment reference.
	 * @see #getSELECTIONTABLEREFS()
	 * @generated
	 */
	void setSELECTIONTABLEREFS(SELECTIONTABLEREFS value);

} // DYNIDDEFMODEINFO
