/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROTOCOLREF</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOLREF()
 * @model extendedMetaData="name='PROTOCOL-REF' kind='elementOnly'"
 * @generated
 */
public interface PROTOCOLREF extends PARENTREF {
} // PROTOCOLREF
