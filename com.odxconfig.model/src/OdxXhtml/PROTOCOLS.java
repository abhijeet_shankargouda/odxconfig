/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROTOCOLS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PROTOCOLS#getPROTOCOL <em>PROTOCOL</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOLS()
 * @model extendedMetaData="name='PROTOCOLS' kind='elementOnly'"
 * @generated
 */
public interface PROTOCOLS extends EObject {
	/**
	 * Returns the value of the '<em><b>PROTOCOL</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.PROTOCOL}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROTOCOL</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOLS_PROTOCOL()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PROTOCOL' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PROTOCOL> getPROTOCOL();

} // PROTOCOLS
