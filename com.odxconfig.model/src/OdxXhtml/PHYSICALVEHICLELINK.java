/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PHYSICALVEHICLELINK</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PHYSICALVEHICLELINK#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALVEHICLELINK#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALVEHICLELINK#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALVEHICLELINK#getVEHICLECONNECTORPINREFS <em>VEHICLECONNECTORPINREFS</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALVEHICLELINK#getLINKCOMPARAMREFS <em>LINKCOMPARAMREFS</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALVEHICLELINK#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALVEHICLELINK#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALVEHICLELINK#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINK()
 * @model extendedMetaData="name='PHYSICAL-VEHICLE-LINK' kind='elementOnly'"
 * @generated
 */
public interface PHYSICALVEHICLELINK extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINK_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINK_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINK_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>VEHICLECONNECTORPINREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VEHICLECONNECTORPINREFS</em>' containment reference.
	 * @see #setVEHICLECONNECTORPINREFS(VEHICLECONNECTORPINREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINK_VEHICLECONNECTORPINREFS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='VEHICLE-CONNECTOR-PIN-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	VEHICLECONNECTORPINREFS getVEHICLECONNECTORPINREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getVEHICLECONNECTORPINREFS <em>VEHICLECONNECTORPINREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VEHICLECONNECTORPINREFS</em>' containment reference.
	 * @see #getVEHICLECONNECTORPINREFS()
	 * @generated
	 */
	void setVEHICLECONNECTORPINREFS(VEHICLECONNECTORPINREFS value);

	/**
	 * Returns the value of the '<em><b>LINKCOMPARAMREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LINKCOMPARAMREFS</em>' containment reference.
	 * @see #setLINKCOMPARAMREFS(LINKCOMPARAMREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINK_LINKCOMPARAMREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LINK-COMPARAM-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	LINKCOMPARAMREFS getLINKCOMPARAMREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getLINKCOMPARAMREFS <em>LINKCOMPARAMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LINKCOMPARAMREFS</em>' containment reference.
	 * @see #getLINKCOMPARAMREFS()
	 * @generated
	 */
	void setLINKCOMPARAMREFS(LINKCOMPARAMREFS value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINK_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINK_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

	/**
	 * Returns the value of the '<em><b>TYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.PHYSICALLINKTYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TYPE</em>' attribute.
	 * @see OdxXhtml.PHYSICALLINKTYPE
	 * @see #isSetTYPE()
	 * @see #unsetTYPE()
	 * @see #setTYPE(PHYSICALLINKTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINK_TYPE()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	PHYSICALLINKTYPE getTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TYPE</em>' attribute.
	 * @see OdxXhtml.PHYSICALLINKTYPE
	 * @see #isSetTYPE()
	 * @see #unsetTYPE()
	 * @see #getTYPE()
	 * @generated
	 */
	void setTYPE(PHYSICALLINKTYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTYPE()
	 * @see #getTYPE()
	 * @see #setTYPE(PHYSICALLINKTYPE)
	 * @generated
	 */
	void unsetTYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALVEHICLELINK#getTYPE <em>TYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TYPE</em>' attribute is set.
	 * @see #unsetTYPE()
	 * @see #getTYPE()
	 * @see #setTYPE(PHYSICALLINKTYPE)
	 * @generated
	 */
	boolean isSetTYPE();

} // PHYSICALVEHICLELINK
