/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EXTERNFLASHDATA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.EXTERNFLASHDATA#getDATAFILE <em>DATAFILE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getEXTERNFLASHDATA()
 * @model extendedMetaData="name='EXTERN-FLASHDATA' kind='elementOnly'"
 * @generated
 */
public interface EXTERNFLASHDATA extends FLASHDATA {
	/**
	 * Returns the value of the '<em><b>DATAFILE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATAFILE</em>' containment reference.
	 * @see #setDATAFILE(DATAFILE)
	 * @see OdxXhtml.OdxXhtmlPackage#getEXTERNFLASHDATA_DATAFILE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATAFILE' namespace='##targetNamespace'"
	 * @generated
	 */
	DATAFILE getDATAFILE();

	/**
	 * Sets the value of the '{@link OdxXhtml.EXTERNFLASHDATA#getDATAFILE <em>DATAFILE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATAFILE</em>' containment reference.
	 * @see #getDATAFILE()
	 * @generated
	 */
	void setDATAFILE(DATAFILE value);

} // EXTERNFLASHDATA
