/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MULTIPLEECUJOBSPEC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOBSPEC#getMULTIPLEECUJOBS <em>MULTIPLEECUJOBS</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOBSPEC#getDIAGDATADICTIONARYSPEC <em>DIAGDATADICTIONARYSPEC</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOBSPEC#getFUNCTCLASSS <em>FUNCTCLASSS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOBSPEC()
 * @model extendedMetaData="name='MULTIPLE-ECU-JOB-SPEC' kind='elementOnly'"
 * @generated
 */
public interface MULTIPLEECUJOBSPEC extends ODXCATEGORY {
	/**
	 * Returns the value of the '<em><b>MULTIPLEECUJOBS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MULTIPLEECUJOBS</em>' containment reference.
	 * @see #setMULTIPLEECUJOBS(MULTIPLEECUJOBS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOBSPEC_MULTIPLEECUJOBS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MULTIPLE-ECU-JOBS' namespace='##targetNamespace'"
	 * @generated
	 */
	MULTIPLEECUJOBS getMULTIPLEECUJOBS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOBSPEC#getMULTIPLEECUJOBS <em>MULTIPLEECUJOBS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MULTIPLEECUJOBS</em>' containment reference.
	 * @see #getMULTIPLEECUJOBS()
	 * @generated
	 */
	void setMULTIPLEECUJOBS(MULTIPLEECUJOBS value);

	/**
	 * Returns the value of the '<em><b>DIAGDATADICTIONARYSPEC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGDATADICTIONARYSPEC</em>' containment reference.
	 * @see #setDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOBSPEC_DIAGDATADICTIONARYSPEC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-DATA-DICTIONARY-SPEC' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGDATADICTIONARYSPEC getDIAGDATADICTIONARYSPEC();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOBSPEC#getDIAGDATADICTIONARYSPEC <em>DIAGDATADICTIONARYSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGDATADICTIONARYSPEC</em>' containment reference.
	 * @see #getDIAGDATADICTIONARYSPEC()
	 * @generated
	 */
	void setDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC value);

	/**
	 * Returns the value of the '<em><b>FUNCTCLASSS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FUNCTCLASSS</em>' containment reference.
	 * @see #setFUNCTCLASSS(FUNCTCLASSS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOBSPEC_FUNCTCLASSS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FUNCT-CLASSS' namespace='##targetNamespace'"
	 * @generated
	 */
	FUNCTCLASSS getFUNCTCLASSS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOBSPEC#getFUNCTCLASSS <em>FUNCTCLASSS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FUNCTCLASSS</em>' containment reference.
	 * @see #getFUNCTCLASSS()
	 * @generated
	 */
	void setFUNCTCLASSS(FUNCTCLASSS value);

} // MULTIPLEECUJOBSPEC
