/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PHYSICALVEHICLELINKS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PHYSICALVEHICLELINKS#getPHYSICALVEHICLELINK <em>PHYSICALVEHICLELINK</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINKS()
 * @model extendedMetaData="name='PHYSICAL-VEHICLE-LINKS' kind='elementOnly'"
 * @generated
 */
public interface PHYSICALVEHICLELINKS extends EObject {
	/**
	 * Returns the value of the '<em><b>PHYSICALVEHICLELINK</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.PHYSICALVEHICLELINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALVEHICLELINK</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALVEHICLELINKS_PHYSICALVEHICLELINK()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PHYSICAL-VEHICLE-LINK' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PHYSICALVEHICLELINK> getPHYSICALVEHICLELINK();

} // PHYSICALVEHICLELINKS
