/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ENCRYPTCOMPRESSMETHODTYPE</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getENCRYPTCOMPRESSMETHODTYPE()
 * @model extendedMetaData="name='ENCRYPT-COMPRESS-METHOD-TYPE'"
 * @generated
 */
public enum ENCRYPTCOMPRESSMETHODTYPE implements Enumerator {
	/**
	 * The '<em><b>AUINT32</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT32_VALUE
	 * @generated
	 * @ordered
	 */
	AUINT32(0, "AUINT32", "A_UINT32"),

	/**
	 * The '<em><b>ABYTEFIELD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABYTEFIELD_VALUE
	 * @generated
	 * @ordered
	 */
	ABYTEFIELD(1, "ABYTEFIELD", "A_BYTEFIELD");

	/**
	 * The '<em><b>AUINT32</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT32
	 * @model literal="A_UINT32"
	 * @generated
	 * @ordered
	 */
	public static final int AUINT32_VALUE = 0;

	/**
	 * The '<em><b>ABYTEFIELD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABYTEFIELD
	 * @model literal="A_BYTEFIELD"
	 * @generated
	 * @ordered
	 */
	public static final int ABYTEFIELD_VALUE = 1;

	/**
	 * An array of all the '<em><b>ENCRYPTCOMPRESSMETHODTYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ENCRYPTCOMPRESSMETHODTYPE[] VALUES_ARRAY =
		new ENCRYPTCOMPRESSMETHODTYPE[] {
			AUINT32,
			ABYTEFIELD,
		};

	/**
	 * A public read-only list of all the '<em><b>ENCRYPTCOMPRESSMETHODTYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ENCRYPTCOMPRESSMETHODTYPE> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ENCRYPTCOMPRESSMETHODTYPE</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENCRYPTCOMPRESSMETHODTYPE get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENCRYPTCOMPRESSMETHODTYPE result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENCRYPTCOMPRESSMETHODTYPE</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENCRYPTCOMPRESSMETHODTYPE getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENCRYPTCOMPRESSMETHODTYPE result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENCRYPTCOMPRESSMETHODTYPE</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENCRYPTCOMPRESSMETHODTYPE get(int value) {
		switch (value) {
			case AUINT32_VALUE: return AUINT32;
			case ABYTEFIELD_VALUE: return ABYTEFIELD;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ENCRYPTCOMPRESSMETHODTYPE(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ENCRYPTCOMPRESSMETHODTYPE
