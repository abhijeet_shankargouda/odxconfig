/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGLAYERREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGLAYERREFS#getDIAGLAYERREF <em>DIAGLAYERREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYERREFS()
 * @model extendedMetaData="name='DIAG-LAYER-REFS' kind='elementOnly'"
 * @generated
 */
public interface DIAGLAYERREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>DIAGLAYERREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGLAYERREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYERREFS_DIAGLAYERREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-LAYER-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getDIAGLAYERREF();

} // DIAGLAYERREFS
