/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LAYERREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.LAYERREFS#getLAYERREF <em>LAYERREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLAYERREFS()
 * @model extendedMetaData="name='LAYER-REFS' kind='elementOnly'"
 * @generated
 */
public interface LAYERREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>LAYERREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LAYERREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getLAYERREFS_LAYERREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='LAYER-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getLAYERREF();

} // LAYERREFS
