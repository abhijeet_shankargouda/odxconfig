/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NOTINHERITEDDIAGCOMMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.NOTINHERITEDDIAGCOMMS#getNOTINHERITEDDIAGCOMM <em>NOTINHERITEDDIAGCOMM</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getNOTINHERITEDDIAGCOMMS()
 * @model extendedMetaData="name='NOT-INHERITED-DIAG-COMMS' kind='elementOnly'"
 * @generated
 */
public interface NOTINHERITEDDIAGCOMMS extends EObject {
	/**
	 * Returns the value of the '<em><b>NOTINHERITEDDIAGCOMM</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.NOTINHERITEDDIAGCOMM}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NOTINHERITEDDIAGCOMM</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getNOTINHERITEDDIAGCOMMS_NOTINHERITEDDIAGCOMM()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='NOT-INHERITED-DIAG-COMM' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<NOTINHERITEDDIAGCOMM> getNOTINHERITEDDIAGCOMM();

} // NOTINHERITEDDIAGCOMMS
