/**
 */
package OdxXhtml;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FILE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FILE#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.FILE#getCREATIONDATE <em>CREATIONDATE</em>}</li>
 *   <li>{@link OdxXhtml.FILE#getCREATOR <em>CREATOR</em>}</li>
 *   <li>{@link OdxXhtml.FILE#getMIMETYPE <em>MIMETYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFILE()
 * @model extendedMetaData="name='FILE' kind='simple'"
 * @generated
 */
public interface FILE extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getFILE_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link OdxXhtml.FILE#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>CREATIONDATE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATIONDATE</em>' attribute.
	 * @see #setCREATIONDATE(XMLGregorianCalendar)
	 * @see OdxXhtml.OdxXhtmlPackage#getFILE_CREATIONDATE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Date"
	 *        extendedMetaData="kind='attribute' name='CREATION-DATE' namespace='##targetNamespace'"
	 * @generated
	 */
	XMLGregorianCalendar getCREATIONDATE();

	/**
	 * Sets the value of the '{@link OdxXhtml.FILE#getCREATIONDATE <em>CREATIONDATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATIONDATE</em>' attribute.
	 * @see #getCREATIONDATE()
	 * @generated
	 */
	void setCREATIONDATE(XMLGregorianCalendar value);

	/**
	 * Returns the value of the '<em><b>CREATOR</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CREATOR</em>' attribute.
	 * @see #setCREATOR(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getFILE_CREATOR()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='CREATOR' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCREATOR();

	/**
	 * Sets the value of the '{@link OdxXhtml.FILE#getCREATOR <em>CREATOR</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CREATOR</em>' attribute.
	 * @see #getCREATOR()
	 * @generated
	 */
	void setCREATOR(String value);

	/**
	 * Returns the value of the '<em><b>MIMETYPE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MIMETYPE</em>' attribute.
	 * @see #setMIMETYPE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getFILE_MIMETYPE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='MIME-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getMIMETYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.FILE#getMIMETYPE <em>MIMETYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MIMETYPE</em>' attribute.
	 * @see #getMIMETYPE()
	 * @generated
	 */
	void setMIMETYPE(String value);

} // FILE
