/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FLASHCLASSREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FLASHCLASSREFS#getFLASHCLASSREF <em>FLASHCLASSREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFLASHCLASSREFS()
 * @model extendedMetaData="name='FLASH-CLASS-REFS' kind='elementOnly'"
 * @generated
 */
public interface FLASHCLASSREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>FLASHCLASSREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FLASHCLASSREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHCLASSREFS_FLASHCLASSREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FLASH-CLASS-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getFLASHCLASSREF();

} // FLASHCLASSREFS
