/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>STATICFIELDS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.STATICFIELDS#getSTATICFIELD <em>STATICFIELD</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSTATICFIELDS()
 * @model extendedMetaData="name='STATIC-FIELDS' kind='elementOnly'"
 * @generated
 */
public interface STATICFIELDS extends EObject {
	/**
	 * Returns the value of the '<em><b>STATICFIELD</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.STATICFIELD}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STATICFIELD</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSTATICFIELDS_STATICFIELD()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='STATIC-FIELD' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<STATICFIELD> getSTATICFIELD();

} // STATICFIELDS
