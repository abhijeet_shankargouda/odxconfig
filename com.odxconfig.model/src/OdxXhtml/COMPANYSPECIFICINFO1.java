/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYSPECIFICINFO1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYSPECIFICINFO1#getRELATEDDOCS <em>RELATEDDOCS</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYSPECIFICINFO1#getSDGS <em>SDGS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYSPECIFICINFO1()
 * @model extendedMetaData="name='COMPANY-SPECIFIC-INFO' kind='elementOnly'"
 * @generated
 */
public interface COMPANYSPECIFICINFO1 extends EObject {
	/**
	 * Returns the value of the '<em><b>RELATEDDOCS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RELATEDDOCS</em>' containment reference.
	 * @see #setRELATEDDOCS(RELATEDDOCS1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYSPECIFICINFO1_RELATEDDOCS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RELATED-DOCS' namespace='##targetNamespace'"
	 * @generated
	 */
	RELATEDDOCS1 getRELATEDDOCS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYSPECIFICINFO1#getRELATEDDOCS <em>RELATEDDOCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RELATEDDOCS</em>' containment reference.
	 * @see #getRELATEDDOCS()
	 * @generated
	 */
	void setRELATEDDOCS(RELATEDDOCS1 value);

	/**
	 * Returns the value of the '<em><b>SDGS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGS</em>' containment reference.
	 * @see #setSDGS(SDGS1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYSPECIFICINFO1_SDGS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDGS' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGS1 getSDGS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYSPECIFICINFO1#getSDGS <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGS</em>' containment reference.
	 * @see #getSDGS()
	 * @generated
	 */
	void setSDGS(SDGS1 value);

} // COMPANYSPECIFICINFO1
