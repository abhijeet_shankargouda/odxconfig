/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LINKCOMPARAMREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.LINKCOMPARAMREFS#getLINKCOMPARAMREF <em>LINKCOMPARAMREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLINKCOMPARAMREFS()
 * @model extendedMetaData="name='LINK-COMPARAM-REFS' kind='elementOnly'"
 * @generated
 */
public interface LINKCOMPARAMREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>LINKCOMPARAMREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.LINKCOMPARAMREF}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LINKCOMPARAMREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getLINKCOMPARAMREFS_LINKCOMPARAMREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='LINK-COMPARAM-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<LINKCOMPARAMREF> getLINKCOMPARAMREF();

} // LINKCOMPARAMREFS
