/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROTOCOLSNREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PROTOCOLSNREFS#getPROTOCOLSNREF <em>PROTOCOLSNREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOLSNREFS()
 * @model extendedMetaData="name='PROTOCOL-SNREFS' kind='elementOnly'"
 * @generated
 */
public interface PROTOCOLSNREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>PROTOCOLSNREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SNREF}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROTOCOLSNREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOLSNREFS_PROTOCOLSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PROTOCOL-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SNREF> getPROTOCOLSNREF();

} // PROTOCOLSNREFS
