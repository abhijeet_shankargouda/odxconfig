/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EXPECTEDIDENTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.EXPECTEDIDENTS#getEXPECTEDIDENT <em>EXPECTEDIDENT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getEXPECTEDIDENTS()
 * @model extendedMetaData="name='EXPECTED-IDENTS' kind='elementOnly'"
 * @generated
 */
public interface EXPECTEDIDENTS extends EObject {
	/**
	 * Returns the value of the '<em><b>EXPECTEDIDENT</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.EXPECTEDIDENT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EXPECTEDIDENT</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getEXPECTEDIDENTS_EXPECTEDIDENT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='EXPECTED-IDENT' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<EXPECTEDIDENT> getEXPECTEDIDENT();

} // EXPECTEDIDENTS
