/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CATALOG</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.CATALOG#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.CATALOG#getCOMPANYDATAS <em>COMPANYDATAS</em>}</li>
 *   <li>{@link OdxXhtml.CATALOG#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.CATALOG#getABLOCKS <em>ABLOCKS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCATALOG()
 * @model extendedMetaData="name='CATALOG' kind='elementOnly'"
 * @generated
 */
public interface CATALOG extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCATALOG_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.CATALOG#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>COMPANYDATAS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYDATAS</em>' containment reference.
	 * @see #setCOMPANYDATAS(COMPANYDATASType)
	 * @see OdxXhtml.OdxXhtmlPackage#getCATALOG_COMPANYDATAS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPANY-DATAS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPANYDATASType getCOMPANYDATAS();

	/**
	 * Sets the value of the '{@link OdxXhtml.CATALOG#getCOMPANYDATAS <em>COMPANYDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYDATAS</em>' containment reference.
	 * @see #getCOMPANYDATAS()
	 * @generated
	 */
	void setCOMPANYDATAS(COMPANYDATASType value);

	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCATALOG_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA1 getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.CATALOG#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA1 value);

	/**
	 * Returns the value of the '<em><b>ABLOCKS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ABLOCKS</em>' containment reference.
	 * @see #setABLOCKS(ABLOCKSType)
	 * @see OdxXhtml.OdxXhtmlPackage#getCATALOG_ABLOCKS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ABLOCKS' namespace='##targetNamespace'"
	 * @generated
	 */
	ABLOCKSType getABLOCKS();

	/**
	 * Sets the value of the '{@link OdxXhtml.CATALOG#getABLOCKS <em>ABLOCKS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ABLOCKS</em>' containment reference.
	 * @see #getABLOCKS()
	 * @generated
	 */
	void setABLOCKS(ABLOCKSType value);

} // CATALOG
