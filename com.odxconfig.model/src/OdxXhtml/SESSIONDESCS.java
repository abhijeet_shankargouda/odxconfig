/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SESSIONDESCS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SESSIONDESCS#getSESSIONDESC <em>SESSIONDESC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESCS()
 * @model extendedMetaData="name='SESSION-DESCS' kind='elementOnly'"
 * @generated
 */
public interface SESSIONDESCS extends EObject {
	/**
	 * Returns the value of the '<em><b>SESSIONDESC</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SESSIONDESC}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SESSIONDESC</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESCS_SESSIONDESC()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SESSION-DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SESSIONDESC> getSESSIONDESC();

} // SESSIONDESCS
