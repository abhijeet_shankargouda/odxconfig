/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NEGOFFSET</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.NEGOFFSET#getNEGATIVEOFFSET <em>NEGATIVEOFFSET</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getNEGOFFSET()
 * @model extendedMetaData="name='NEG-OFFSET' kind='elementOnly'"
 * @generated
 */
public interface NEGOFFSET extends TARGETADDROFFSET {
	/**
	 * Returns the value of the '<em><b>NEGATIVEOFFSET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NEGATIVEOFFSET</em>' attribute.
	 * @see #setNEGATIVEOFFSET(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getNEGOFFSET_NEGATIVEOFFSET()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary" required="true"
	 *        extendedMetaData="kind='element' name='NEGATIVE-OFFSET' namespace='##targetNamespace'"
	 * @generated
	 */
	byte[] getNEGATIVEOFFSET();

	/**
	 * Sets the value of the '{@link OdxXhtml.NEGOFFSET#getNEGATIVEOFFSET <em>NEGATIVEOFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NEGATIVEOFFSET</em>' attribute.
	 * @see #getNEGATIVEOFFSET()
	 * @generated
	 */
	void setNEGATIVEOFFSET(byte[] value);

} // NEGOFFSET
