/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SDG</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SDG#getSDGCAPTION <em>SDGCAPTION</em>}</li>
 *   <li>{@link OdxXhtml.SDG#getGroup <em>Group</em>}</li>
 *   <li>{@link OdxXhtml.SDG#getSDG <em>SDG</em>}</li>
 *   <li>{@link OdxXhtml.SDG#getSD <em>SD</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSDG()
 * @model extendedMetaData="name='SDG' kind='elementOnly'"
 * @generated
 */
public interface SDG extends SPECIALDATA {
	/**
	 * Returns the value of the '<em><b>SDGCAPTION</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGCAPTION</em>' containment reference.
	 * @see #setSDGCAPTION(SDGCAPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getSDG_SDGCAPTION()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDG-CAPTION' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGCAPTION getSDGCAPTION();

	/**
	 * Sets the value of the '{@link OdxXhtml.SDG#getSDGCAPTION <em>SDGCAPTION</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGCAPTION</em>' containment reference.
	 * @see #getSDGCAPTION()
	 * @generated
	 */
	void setSDGCAPTION(SDGCAPTION value);

	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSDG_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:1'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>SDG</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SDG}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDG</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSDG_SDG()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SDG' namespace='##targetNamespace' group='group:1'"
	 * @generated
	 */
	EList<SDG> getSDG();

	/**
	 * Returns the value of the '<em><b>SD</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SD}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SD</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSDG_SD()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SD' namespace='##targetNamespace' group='group:1'"
	 * @generated
	 */
	EList<SD> getSD();

} // SDG
