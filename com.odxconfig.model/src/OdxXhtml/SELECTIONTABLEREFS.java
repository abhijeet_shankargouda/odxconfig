/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SELECTIONTABLEREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SELECTIONTABLEREFS#getGroup <em>Group</em>}</li>
 *   <li>{@link OdxXhtml.SELECTIONTABLEREFS#getSELECTIONTABLEREF <em>SELECTIONTABLEREF</em>}</li>
 *   <li>{@link OdxXhtml.SELECTIONTABLEREFS#getSELECTIONTABLESNREF <em>SELECTIONTABLESNREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSELECTIONTABLEREFS()
 * @model extendedMetaData="name='SELECTION-TABLE-REFS' kind='elementOnly'"
 * @generated
 */
public interface SELECTIONTABLEREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSELECTIONTABLEREFS_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>SELECTIONTABLEREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SELECTIONTABLEREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSELECTIONTABLEREFS_SELECTIONTABLEREF()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SELECTION-TABLE-REF' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ODXLINK> getSELECTIONTABLEREF();

	/**
	 * Returns the value of the '<em><b>SELECTIONTABLESNREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SNREF}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SELECTIONTABLESNREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSELECTIONTABLEREFS_SELECTIONTABLESNREF()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SELECTION-TABLE-SNREF' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SNREF> getSELECTIONTABLESNREF();

} // SELECTIONTABLEREFS
