/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PHYSICALDIMENSION</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getLENGTHEXP <em>LENGTHEXP</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getMASSEXP <em>MASSEXP</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getTIMEEXP <em>TIMEEXP</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getCURRENTEXP <em>CURRENTEXP</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getTEMPERATUREEXP <em>TEMPERATUREEXP</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getMOLARAMOUNTEXP <em>MOLARAMOUNTEXP</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getLUMINOUSINTENSITYEXP <em>LUMINOUSINTENSITYEXP</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSION#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION()
 * @model extendedMetaData="name='PHYSICAL-DIMENSION' kind='elementOnly'"
 * @generated
 */
public interface PHYSICALDIMENSION extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>LENGTHEXP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LENGTHEXP</em>' attribute.
	 * @see #isSetLENGTHEXP()
	 * @see #unsetLENGTHEXP()
	 * @see #setLENGTHEXP(int)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_LENGTHEXP()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='element' name='LENGTH-EXP' namespace='##targetNamespace'"
	 * @generated
	 */
	int getLENGTHEXP();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getLENGTHEXP <em>LENGTHEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LENGTHEXP</em>' attribute.
	 * @see #isSetLENGTHEXP()
	 * @see #unsetLENGTHEXP()
	 * @see #getLENGTHEXP()
	 * @generated
	 */
	void setLENGTHEXP(int value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getLENGTHEXP <em>LENGTHEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLENGTHEXP()
	 * @see #getLENGTHEXP()
	 * @see #setLENGTHEXP(int)
	 * @generated
	 */
	void unsetLENGTHEXP();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getLENGTHEXP <em>LENGTHEXP</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>LENGTHEXP</em>' attribute is set.
	 * @see #unsetLENGTHEXP()
	 * @see #getLENGTHEXP()
	 * @see #setLENGTHEXP(int)
	 * @generated
	 */
	boolean isSetLENGTHEXP();

	/**
	 * Returns the value of the '<em><b>MASSEXP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MASSEXP</em>' attribute.
	 * @see #isSetMASSEXP()
	 * @see #unsetMASSEXP()
	 * @see #setMASSEXP(int)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_MASSEXP()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='element' name='MASS-EXP' namespace='##targetNamespace'"
	 * @generated
	 */
	int getMASSEXP();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getMASSEXP <em>MASSEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MASSEXP</em>' attribute.
	 * @see #isSetMASSEXP()
	 * @see #unsetMASSEXP()
	 * @see #getMASSEXP()
	 * @generated
	 */
	void setMASSEXP(int value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getMASSEXP <em>MASSEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMASSEXP()
	 * @see #getMASSEXP()
	 * @see #setMASSEXP(int)
	 * @generated
	 */
	void unsetMASSEXP();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getMASSEXP <em>MASSEXP</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MASSEXP</em>' attribute is set.
	 * @see #unsetMASSEXP()
	 * @see #getMASSEXP()
	 * @see #setMASSEXP(int)
	 * @generated
	 */
	boolean isSetMASSEXP();

	/**
	 * Returns the value of the '<em><b>TIMEEXP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TIMEEXP</em>' attribute.
	 * @see #isSetTIMEEXP()
	 * @see #unsetTIMEEXP()
	 * @see #setTIMEEXP(int)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_TIMEEXP()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='element' name='TIME-EXP' namespace='##targetNamespace'"
	 * @generated
	 */
	int getTIMEEXP();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getTIMEEXP <em>TIMEEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TIMEEXP</em>' attribute.
	 * @see #isSetTIMEEXP()
	 * @see #unsetTIMEEXP()
	 * @see #getTIMEEXP()
	 * @generated
	 */
	void setTIMEEXP(int value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getTIMEEXP <em>TIMEEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTIMEEXP()
	 * @see #getTIMEEXP()
	 * @see #setTIMEEXP(int)
	 * @generated
	 */
	void unsetTIMEEXP();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getTIMEEXP <em>TIMEEXP</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TIMEEXP</em>' attribute is set.
	 * @see #unsetTIMEEXP()
	 * @see #getTIMEEXP()
	 * @see #setTIMEEXP(int)
	 * @generated
	 */
	boolean isSetTIMEEXP();

	/**
	 * Returns the value of the '<em><b>CURRENTEXP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CURRENTEXP</em>' attribute.
	 * @see #isSetCURRENTEXP()
	 * @see #unsetCURRENTEXP()
	 * @see #setCURRENTEXP(int)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_CURRENTEXP()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='element' name='CURRENT-EXP' namespace='##targetNamespace'"
	 * @generated
	 */
	int getCURRENTEXP();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getCURRENTEXP <em>CURRENTEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CURRENTEXP</em>' attribute.
	 * @see #isSetCURRENTEXP()
	 * @see #unsetCURRENTEXP()
	 * @see #getCURRENTEXP()
	 * @generated
	 */
	void setCURRENTEXP(int value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getCURRENTEXP <em>CURRENTEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCURRENTEXP()
	 * @see #getCURRENTEXP()
	 * @see #setCURRENTEXP(int)
	 * @generated
	 */
	void unsetCURRENTEXP();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getCURRENTEXP <em>CURRENTEXP</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>CURRENTEXP</em>' attribute is set.
	 * @see #unsetCURRENTEXP()
	 * @see #getCURRENTEXP()
	 * @see #setCURRENTEXP(int)
	 * @generated
	 */
	boolean isSetCURRENTEXP();

	/**
	 * Returns the value of the '<em><b>TEMPERATUREEXP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TEMPERATUREEXP</em>' attribute.
	 * @see #isSetTEMPERATUREEXP()
	 * @see #unsetTEMPERATUREEXP()
	 * @see #setTEMPERATUREEXP(int)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_TEMPERATUREEXP()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='element' name='TEMPERATURE-EXP' namespace='##targetNamespace'"
	 * @generated
	 */
	int getTEMPERATUREEXP();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getTEMPERATUREEXP <em>TEMPERATUREEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TEMPERATUREEXP</em>' attribute.
	 * @see #isSetTEMPERATUREEXP()
	 * @see #unsetTEMPERATUREEXP()
	 * @see #getTEMPERATUREEXP()
	 * @generated
	 */
	void setTEMPERATUREEXP(int value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getTEMPERATUREEXP <em>TEMPERATUREEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTEMPERATUREEXP()
	 * @see #getTEMPERATUREEXP()
	 * @see #setTEMPERATUREEXP(int)
	 * @generated
	 */
	void unsetTEMPERATUREEXP();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getTEMPERATUREEXP <em>TEMPERATUREEXP</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TEMPERATUREEXP</em>' attribute is set.
	 * @see #unsetTEMPERATUREEXP()
	 * @see #getTEMPERATUREEXP()
	 * @see #setTEMPERATUREEXP(int)
	 * @generated
	 */
	boolean isSetTEMPERATUREEXP();

	/**
	 * Returns the value of the '<em><b>MOLARAMOUNTEXP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MOLARAMOUNTEXP</em>' attribute.
	 * @see #isSetMOLARAMOUNTEXP()
	 * @see #unsetMOLARAMOUNTEXP()
	 * @see #setMOLARAMOUNTEXP(int)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_MOLARAMOUNTEXP()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='element' name='MOLAR-AMOUNT-EXP' namespace='##targetNamespace'"
	 * @generated
	 */
	int getMOLARAMOUNTEXP();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getMOLARAMOUNTEXP <em>MOLARAMOUNTEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MOLARAMOUNTEXP</em>' attribute.
	 * @see #isSetMOLARAMOUNTEXP()
	 * @see #unsetMOLARAMOUNTEXP()
	 * @see #getMOLARAMOUNTEXP()
	 * @generated
	 */
	void setMOLARAMOUNTEXP(int value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getMOLARAMOUNTEXP <em>MOLARAMOUNTEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMOLARAMOUNTEXP()
	 * @see #getMOLARAMOUNTEXP()
	 * @see #setMOLARAMOUNTEXP(int)
	 * @generated
	 */
	void unsetMOLARAMOUNTEXP();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getMOLARAMOUNTEXP <em>MOLARAMOUNTEXP</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MOLARAMOUNTEXP</em>' attribute is set.
	 * @see #unsetMOLARAMOUNTEXP()
	 * @see #getMOLARAMOUNTEXP()
	 * @see #setMOLARAMOUNTEXP(int)
	 * @generated
	 */
	boolean isSetMOLARAMOUNTEXP();

	/**
	 * Returns the value of the '<em><b>LUMINOUSINTENSITYEXP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LUMINOUSINTENSITYEXP</em>' attribute.
	 * @see #isSetLUMINOUSINTENSITYEXP()
	 * @see #unsetLUMINOUSINTENSITYEXP()
	 * @see #setLUMINOUSINTENSITYEXP(int)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_LUMINOUSINTENSITYEXP()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 *        extendedMetaData="kind='element' name='LUMINOUS-INTENSITY-EXP' namespace='##targetNamespace'"
	 * @generated
	 */
	int getLUMINOUSINTENSITYEXP();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getLUMINOUSINTENSITYEXP <em>LUMINOUSINTENSITYEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LUMINOUSINTENSITYEXP</em>' attribute.
	 * @see #isSetLUMINOUSINTENSITYEXP()
	 * @see #unsetLUMINOUSINTENSITYEXP()
	 * @see #getLUMINOUSINTENSITYEXP()
	 * @generated
	 */
	void setLUMINOUSINTENSITYEXP(int value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getLUMINOUSINTENSITYEXP <em>LUMINOUSINTENSITYEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLUMINOUSINTENSITYEXP()
	 * @see #getLUMINOUSINTENSITYEXP()
	 * @see #setLUMINOUSINTENSITYEXP(int)
	 * @generated
	 */
	void unsetLUMINOUSINTENSITYEXP();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getLUMINOUSINTENSITYEXP <em>LUMINOUSINTENSITYEXP</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>LUMINOUSINTENSITYEXP</em>' attribute is set.
	 * @see #unsetLUMINOUSINTENSITYEXP()
	 * @see #getLUMINOUSINTENSITYEXP()
	 * @see #setLUMINOUSINTENSITYEXP(int)
	 * @generated
	 */
	boolean isSetLUMINOUSINTENSITYEXP();

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSION_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALDIMENSION#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // PHYSICALDIMENSION
