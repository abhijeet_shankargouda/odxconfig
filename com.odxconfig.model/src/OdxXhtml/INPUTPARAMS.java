/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INPUTPARAMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.INPUTPARAMS#getINPUTPARAM <em>INPUTPARAM</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAMS()
 * @model extendedMetaData="name='INPUT-PARAMS' kind='elementOnly'"
 * @generated
 */
public interface INPUTPARAMS extends EObject {
	/**
	 * Returns the value of the '<em><b>INPUTPARAM</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.INPUTPARAM}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INPUTPARAM</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAMS_INPUTPARAM()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='INPUT-PARAM' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<INPUTPARAM> getINPUTPARAM();

} // INPUTPARAMS
