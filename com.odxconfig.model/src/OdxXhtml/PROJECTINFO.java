/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROJECTINFO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PROJECTINFO#getECUFAMILYDESC <em>ECUFAMILYDESC</em>}</li>
 *   <li>{@link OdxXhtml.PROJECTINFO#getMODIFICATIONLETTER <em>MODIFICATIONLETTER</em>}</li>
 *   <li>{@link OdxXhtml.PROJECTINFO#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.PROJECTINFO#getPROJECTIDENTS <em>PROJECTIDENTS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTINFO()
 * @model extendedMetaData="name='PROJECT-INFO' kind='elementOnly'"
 * @generated
 */
public interface PROJECTINFO extends EObject {
	/**
	 * Returns the value of the '<em><b>ECUFAMILYDESC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUFAMILYDESC</em>' attribute.
	 * @see #setECUFAMILYDESC(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTINFO_ECUFAMILYDESC()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='ECU-FAMILY-DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	String getECUFAMILYDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROJECTINFO#getECUFAMILYDESC <em>ECUFAMILYDESC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ECUFAMILYDESC</em>' attribute.
	 * @see #getECUFAMILYDESC()
	 * @generated
	 */
	void setECUFAMILYDESC(String value);

	/**
	 * Returns the value of the '<em><b>MODIFICATIONLETTER</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MODIFICATIONLETTER</em>' attribute.
	 * @see #setMODIFICATIONLETTER(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTINFO_MODIFICATIONLETTER()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='MODIFICATION-LETTER' namespace='##targetNamespace'"
	 * @generated
	 */
	String getMODIFICATIONLETTER();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROJECTINFO#getMODIFICATIONLETTER <em>MODIFICATIONLETTER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MODIFICATIONLETTER</em>' attribute.
	 * @see #getMODIFICATIONLETTER()
	 * @generated
	 */
	void setMODIFICATIONLETTER(String value);

	/**
	 * Returns the value of the '<em><b>COMPANYDATAREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYDATAREF</em>' containment reference.
	 * @see #setCOMPANYDATAREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTINFO_COMPANYDATAREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPANY-DATA-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getCOMPANYDATAREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROJECTINFO#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYDATAREF</em>' containment reference.
	 * @see #getCOMPANYDATAREF()
	 * @generated
	 */
	void setCOMPANYDATAREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>PROJECTIDENTS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROJECTIDENTS</em>' containment reference.
	 * @see #setPROJECTIDENTS(PROJECTIDENTS)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTINFO_PROJECTIDENTS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PROJECT-IDENTS' namespace='##targetNamespace'"
	 * @generated
	 */
	PROJECTIDENTS getPROJECTIDENTS();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROJECTINFO#getPROJECTIDENTS <em>PROJECTIDENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROJECTIDENTS</em>' containment reference.
	 * @see #getPROJECTIDENTS()
	 * @generated
	 */
	void setPROJECTIDENTS(PROJECTIDENTS value);

} // PROJECTINFO
