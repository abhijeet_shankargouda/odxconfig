/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DocumentRoot#getMixed <em>Mixed</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getB <em>B</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getBr <em>Br</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getCATALOG <em>CATALOG</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getI <em>I</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getLi <em>Li</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getOl <em>Ol</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getSub <em>Sub</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getSup <em>Sup</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getUl <em>Ul</em>}</li>
 *   <li>{@link OdxXhtml.DocumentRoot#getODX <em>ODX</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends EObject {
	/**
	 * Returns the value of the '<em><b>Mixed</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mixed</em>' attribute list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_Mixed()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='elementWildcard' name=':mixed'"
	 * @generated
	 */
	FeatureMap getMixed();

	/**
	 * Returns the value of the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XMLNS Prefix Map</em>' map.
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_XMLNSPrefixMap()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;" transient="true"
	 *        extendedMetaData="kind='attribute' name='xmlns:prefix'"
	 * @generated
	 */
	EMap<String, String> getXMLNSPrefixMap();

	/**
	 * Returns the value of the '<em><b>XSI Schema Location</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link java.lang.String},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XSI Schema Location</em>' map.
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_XSISchemaLocation()
	 * @model mapType="org.eclipse.emf.ecore.EStringToStringMapEntry&lt;org.eclipse.emf.ecore.EString, org.eclipse.emf.ecore.EString&gt;" transient="true"
	 *        extendedMetaData="kind='attribute' name='xsi:schemaLocation'"
	 * @generated
	 */
	EMap<String, String> getXSISchemaLocation();

	/**
	 * Returns the value of the '<em><b>B</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>B</em>' containment reference.
	 * @see #setB(BType)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_B()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='b' namespace='##targetNamespace'"
	 * @generated
	 */
	BType getB();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getB <em>B</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>B</em>' containment reference.
	 * @see #getB()
	 * @generated
	 */
	void setB(BType value);

	/**
	 * Returns the value of the '<em><b>Br</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Br</em>' containment reference.
	 * @see #setBr(EObject)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_Br()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='br' namespace='##targetNamespace'"
	 * @generated
	 */
	EObject getBr();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getBr <em>Br</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Br</em>' containment reference.
	 * @see #getBr()
	 * @generated
	 */
	void setBr(EObject value);

	/**
	 * Returns the value of the '<em><b>CATALOG</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CATALOG</em>' containment reference.
	 * @see #setCATALOG(CATALOG)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_CATALOG()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='CATALOG' namespace='##targetNamespace'"
	 * @generated
	 */
	CATALOG getCATALOG();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getCATALOG <em>CATALOG</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CATALOG</em>' containment reference.
	 * @see #getCATALOG()
	 * @generated
	 */
	void setCATALOG(CATALOG value);

	/**
	 * Returns the value of the '<em><b>I</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>I</em>' containment reference.
	 * @see #setI(IType)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_I()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='i' namespace='##targetNamespace'"
	 * @generated
	 */
	IType getI();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getI <em>I</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>I</em>' containment reference.
	 * @see #getI()
	 * @generated
	 */
	void setI(IType value);

	/**
	 * Returns the value of the '<em><b>Li</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Li</em>' containment reference.
	 * @see #setLi(LiType)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_Li()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='li' namespace='##targetNamespace'"
	 * @generated
	 */
	LiType getLi();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getLi <em>Li</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Li</em>' containment reference.
	 * @see #getLi()
	 * @generated
	 */
	void setLi(LiType value);

	/**
	 * Returns the value of the '<em><b>Ol</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ol</em>' containment reference.
	 * @see #setOl(OlType)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_Ol()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ol' namespace='##targetNamespace'"
	 * @generated
	 */
	OlType getOl();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getOl <em>Ol</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ol</em>' containment reference.
	 * @see #getOl()
	 * @generated
	 */
	void setOl(OlType value);

	/**
	 * Returns the value of the '<em><b>Sub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub</em>' containment reference.
	 * @see #setSub(SubType)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_Sub()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='sub' namespace='##targetNamespace'"
	 * @generated
	 */
	SubType getSub();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getSub <em>Sub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub</em>' containment reference.
	 * @see #getSub()
	 * @generated
	 */
	void setSub(SubType value);

	/**
	 * Returns the value of the '<em><b>Sup</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sup</em>' containment reference.
	 * @see #setSup(SupType)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_Sup()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='sup' namespace='##targetNamespace'"
	 * @generated
	 */
	SupType getSup();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getSup <em>Sup</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sup</em>' containment reference.
	 * @see #getSup()
	 * @generated
	 */
	void setSup(SupType value);

	/**
	 * Returns the value of the '<em><b>Ul</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ul</em>' containment reference.
	 * @see #setUl(UlType)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_Ul()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ul' namespace='##targetNamespace'"
	 * @generated
	 */
	UlType getUl();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getUl <em>Ul</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ul</em>' containment reference.
	 * @see #getUl()
	 * @generated
	 */
	void setUl(UlType value);

	/**
	 * Returns the value of the '<em><b>ODX</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ODX</em>' containment reference.
	 * @see #setODX(ODX)
	 * @see OdxXhtml.OdxXhtmlPackage#getDocumentRoot_ODX()
	 * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ODX' namespace='##targetNamespace'"
	 * @generated
	 */
	ODX getODX();

	/**
	 * Sets the value of the '{@link OdxXhtml.DocumentRoot#getODX <em>ODX</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ODX</em>' containment reference.
	 * @see #getODX()
	 * @generated
	 */
	void setODX(ODX value);

} // DocumentRoot
