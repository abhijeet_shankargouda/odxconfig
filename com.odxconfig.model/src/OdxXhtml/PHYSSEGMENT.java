/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PHYSSEGMENT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PHYSSEGMENT#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.PHYSSEGMENT#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.PHYSSEGMENT#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.PHYSSEGMENT#getFILLBYTE <em>FILLBYTE</em>}</li>
 *   <li>{@link OdxXhtml.PHYSSEGMENT#getBLOCKSIZE <em>BLOCKSIZE</em>}</li>
 *   <li>{@link OdxXhtml.PHYSSEGMENT#getSTARTADDRESS <em>STARTADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.PHYSSEGMENT#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.PHYSSEGMENT#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENT()
 * @model extendedMetaData="name='PHYS-SEGMENT' kind='elementOnly'"
 * @generated
 */
public interface PHYSSEGMENT extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENT_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSSEGMENT#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENT_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSSEGMENT#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENT_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSSEGMENT#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>FILLBYTE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FILLBYTE</em>' attribute.
	 * @see #setFILLBYTE(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENT_FILLBYTE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary"
	 *        extendedMetaData="kind='element' name='FILLBYTE' namespace='##targetNamespace'"
	 * @generated
	 */
	byte[] getFILLBYTE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSSEGMENT#getFILLBYTE <em>FILLBYTE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FILLBYTE</em>' attribute.
	 * @see #getFILLBYTE()
	 * @generated
	 */
	void setFILLBYTE(byte[] value);

	/**
	 * Returns the value of the '<em><b>BLOCKSIZE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BLOCKSIZE</em>' attribute.
	 * @see #isSetBLOCKSIZE()
	 * @see #unsetBLOCKSIZE()
	 * @see #setBLOCKSIZE(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENT_BLOCKSIZE()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='element' name='BLOCK-SIZE' namespace='##targetNamespace'"
	 * @generated
	 */
	long getBLOCKSIZE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSSEGMENT#getBLOCKSIZE <em>BLOCKSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BLOCKSIZE</em>' attribute.
	 * @see #isSetBLOCKSIZE()
	 * @see #unsetBLOCKSIZE()
	 * @see #getBLOCKSIZE()
	 * @generated
	 */
	void setBLOCKSIZE(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSSEGMENT#getBLOCKSIZE <em>BLOCKSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBLOCKSIZE()
	 * @see #getBLOCKSIZE()
	 * @see #setBLOCKSIZE(long)
	 * @generated
	 */
	void unsetBLOCKSIZE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSSEGMENT#getBLOCKSIZE <em>BLOCKSIZE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BLOCKSIZE</em>' attribute is set.
	 * @see #unsetBLOCKSIZE()
	 * @see #getBLOCKSIZE()
	 * @see #setBLOCKSIZE(long)
	 * @generated
	 */
	boolean isSetBLOCKSIZE();

	/**
	 * Returns the value of the '<em><b>STARTADDRESS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STARTADDRESS</em>' attribute.
	 * @see #setSTARTADDRESS(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENT_STARTADDRESS()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary" required="true"
	 *        extendedMetaData="kind='element' name='START-ADDRESS' namespace='##targetNamespace'"
	 * @generated
	 */
	byte[] getSTARTADDRESS();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSSEGMENT#getSTARTADDRESS <em>STARTADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>STARTADDRESS</em>' attribute.
	 * @see #getSTARTADDRESS()
	 * @generated
	 */
	void setSTARTADDRESS(byte[] value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENT_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSSEGMENT#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENT_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSSEGMENT#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // PHYSSEGMENT
