/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUMEMCONNECTOR</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getFLASHCLASSS <em>FLASHCLASSS</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getSESSIONDESCS <em>SESSIONDESCS</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getIDENTDESCS <em>IDENTDESCS</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getECUMEMREF <em>ECUMEMREF</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getLAYERREFS <em>LAYERREFS</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTOR#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR()
 * @model extendedMetaData="name='ECU-MEM-CONNECTOR' kind='elementOnly'"
 * @generated
 */
public interface ECUMEMCONNECTOR extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA value);

	/**
	 * Returns the value of the '<em><b>FLASHCLASSS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FLASHCLASSS</em>' containment reference.
	 * @see #setFLASHCLASSS(FLASHCLASSS)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_FLASHCLASSS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FLASH-CLASSS' namespace='##targetNamespace'"
	 * @generated
	 */
	FLASHCLASSS getFLASHCLASSS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getFLASHCLASSS <em>FLASHCLASSS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FLASHCLASSS</em>' containment reference.
	 * @see #getFLASHCLASSS()
	 * @generated
	 */
	void setFLASHCLASSS(FLASHCLASSS value);

	/**
	 * Returns the value of the '<em><b>SESSIONDESCS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SESSIONDESCS</em>' containment reference.
	 * @see #setSESSIONDESCS(SESSIONDESCS)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_SESSIONDESCS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SESSION-DESCS' namespace='##targetNamespace'"
	 * @generated
	 */
	SESSIONDESCS getSESSIONDESCS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getSESSIONDESCS <em>SESSIONDESCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SESSIONDESCS</em>' containment reference.
	 * @see #getSESSIONDESCS()
	 * @generated
	 */
	void setSESSIONDESCS(SESSIONDESCS value);

	/**
	 * Returns the value of the '<em><b>IDENTDESCS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IDENTDESCS</em>' containment reference.
	 * @see #setIDENTDESCS(IDENTDESCS)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_IDENTDESCS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='IDENT-DESCS' namespace='##targetNamespace'"
	 * @generated
	 */
	IDENTDESCS getIDENTDESCS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getIDENTDESCS <em>IDENTDESCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IDENTDESCS</em>' containment reference.
	 * @see #getIDENTDESCS()
	 * @generated
	 */
	void setIDENTDESCS(IDENTDESCS value);

	/**
	 * Returns the value of the '<em><b>ECUMEMREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUMEMREF</em>' containment reference.
	 * @see #setECUMEMREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_ECUMEMREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ECU-MEM-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getECUMEMREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getECUMEMREF <em>ECUMEMREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ECUMEMREF</em>' containment reference.
	 * @see #getECUMEMREF()
	 * @generated
	 */
	void setECUMEMREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>LAYERREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LAYERREFS</em>' containment reference.
	 * @see #setLAYERREFS(LAYERREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_LAYERREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LAYER-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	LAYERREFS getLAYERREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getLAYERREFS <em>LAYERREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LAYERREFS</em>' containment reference.
	 * @see #getLAYERREFS()
	 * @generated
	 */
	void setLAYERREFS(LAYERREFS value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTOR_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEMCONNECTOR#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // ECUMEMCONNECTOR
