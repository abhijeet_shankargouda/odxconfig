/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGLAYERCONTAINER</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGLAYERCONTAINER#getPROTOCOLS <em>PROTOCOLS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYERCONTAINER#getFUNCTIONALGROUPS <em>FUNCTIONALGROUPS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYERCONTAINER#getECUSHAREDDATAS <em>ECUSHAREDDATAS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYERCONTAINER#getBASEVARIANTS <em>BASEVARIANTS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYERCONTAINER#getECUVARIANTS <em>ECUVARIANTS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYERCONTAINER()
 * @model extendedMetaData="name='DIAG-LAYER-CONTAINER' kind='elementOnly'"
 * @generated
 */
public interface DIAGLAYERCONTAINER extends ODXCATEGORY {
	/**
	 * Returns the value of the '<em><b>PROTOCOLS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROTOCOLS</em>' containment reference.
	 * @see #setPROTOCOLS(PROTOCOLS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYERCONTAINER_PROTOCOLS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PROTOCOLS' namespace='##targetNamespace'"
	 * @generated
	 */
	PROTOCOLS getPROTOCOLS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYERCONTAINER#getPROTOCOLS <em>PROTOCOLS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROTOCOLS</em>' containment reference.
	 * @see #getPROTOCOLS()
	 * @generated
	 */
	void setPROTOCOLS(PROTOCOLS value);

	/**
	 * Returns the value of the '<em><b>FUNCTIONALGROUPS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FUNCTIONALGROUPS</em>' containment reference.
	 * @see #setFUNCTIONALGROUPS(FUNCTIONALGROUPS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYERCONTAINER_FUNCTIONALGROUPS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FUNCTIONAL-GROUPS' namespace='##targetNamespace'"
	 * @generated
	 */
	FUNCTIONALGROUPS getFUNCTIONALGROUPS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYERCONTAINER#getFUNCTIONALGROUPS <em>FUNCTIONALGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FUNCTIONALGROUPS</em>' containment reference.
	 * @see #getFUNCTIONALGROUPS()
	 * @generated
	 */
	void setFUNCTIONALGROUPS(FUNCTIONALGROUPS value);

	/**
	 * Returns the value of the '<em><b>ECUSHAREDDATAS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUSHAREDDATAS</em>' containment reference.
	 * @see #setECUSHAREDDATAS(ECUSHAREDDATAS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYERCONTAINER_ECUSHAREDDATAS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ECU-SHARED-DATAS' namespace='##targetNamespace'"
	 * @generated
	 */
	ECUSHAREDDATAS getECUSHAREDDATAS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYERCONTAINER#getECUSHAREDDATAS <em>ECUSHAREDDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ECUSHAREDDATAS</em>' containment reference.
	 * @see #getECUSHAREDDATAS()
	 * @generated
	 */
	void setECUSHAREDDATAS(ECUSHAREDDATAS value);

	/**
	 * Returns the value of the '<em><b>BASEVARIANTS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BASEVARIANTS</em>' containment reference.
	 * @see #setBASEVARIANTS(BASEVARIANTS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYERCONTAINER_BASEVARIANTS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='BASE-VARIANTS' namespace='##targetNamespace'"
	 * @generated
	 */
	BASEVARIANTS getBASEVARIANTS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYERCONTAINER#getBASEVARIANTS <em>BASEVARIANTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BASEVARIANTS</em>' containment reference.
	 * @see #getBASEVARIANTS()
	 * @generated
	 */
	void setBASEVARIANTS(BASEVARIANTS value);

	/**
	 * Returns the value of the '<em><b>ECUVARIANTS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUVARIANTS</em>' containment reference.
	 * @see #setECUVARIANTS(ECUVARIANTS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYERCONTAINER_ECUVARIANTS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ECU-VARIANTS' namespace='##targetNamespace'"
	 * @generated
	 */
	ECUVARIANTS getECUVARIANTS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYERCONTAINER#getECUVARIANTS <em>ECUVARIANTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ECUVARIANTS</em>' containment reference.
	 * @see #getECUVARIANTS()
	 * @generated
	 */
	void setECUVARIANTS(ECUVARIANTS value);

} // DIAGLAYERCONTAINER
