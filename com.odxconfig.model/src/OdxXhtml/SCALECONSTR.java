/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SCALECONSTR</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SCALECONSTR#getSHORTLABEL <em>SHORTLABEL</em>}</li>
 *   <li>{@link OdxXhtml.SCALECONSTR#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.SCALECONSTR#getLOWERLIMIT <em>LOWERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.SCALECONSTR#getUPPERLIMIT <em>UPPERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.SCALECONSTR#getVALIDITY <em>VALIDITY</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSCALECONSTR()
 * @model extendedMetaData="name='SCALE-CONSTR' kind='elementOnly'"
 * @generated
 */
public interface SCALECONSTR extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTLABEL</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTLABEL</em>' containment reference.
	 * @see #setSHORTLABEL(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getSCALECONSTR_SHORTLABEL()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SHORT-LABEL' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getSHORTLABEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.SCALECONSTR#getSHORTLABEL <em>SHORTLABEL</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTLABEL</em>' containment reference.
	 * @see #getSHORTLABEL()
	 * @generated
	 */
	void setSHORTLABEL(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getSCALECONSTR_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.SCALECONSTR#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>LOWERLIMIT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LOWERLIMIT</em>' containment reference.
	 * @see #setLOWERLIMIT(LIMIT)
	 * @see OdxXhtml.OdxXhtmlPackage#getSCALECONSTR_LOWERLIMIT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='LOWER-LIMIT' namespace='##targetNamespace'"
	 * @generated
	 */
	LIMIT getLOWERLIMIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.SCALECONSTR#getLOWERLIMIT <em>LOWERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LOWERLIMIT</em>' containment reference.
	 * @see #getLOWERLIMIT()
	 * @generated
	 */
	void setLOWERLIMIT(LIMIT value);

	/**
	 * Returns the value of the '<em><b>UPPERLIMIT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UPPERLIMIT</em>' containment reference.
	 * @see #setUPPERLIMIT(LIMIT)
	 * @see OdxXhtml.OdxXhtmlPackage#getSCALECONSTR_UPPERLIMIT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='UPPER-LIMIT' namespace='##targetNamespace'"
	 * @generated
	 */
	LIMIT getUPPERLIMIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.SCALECONSTR#getUPPERLIMIT <em>UPPERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UPPERLIMIT</em>' containment reference.
	 * @see #getUPPERLIMIT()
	 * @generated
	 */
	void setUPPERLIMIT(LIMIT value);

	/**
	 * Returns the value of the '<em><b>VALIDITY</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.VALIDTYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VALIDITY</em>' attribute.
	 * @see OdxXhtml.VALIDTYPE
	 * @see #isSetVALIDITY()
	 * @see #unsetVALIDITY()
	 * @see #setVALIDITY(VALIDTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getSCALECONSTR_VALIDITY()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='VALIDITY' namespace='##targetNamespace'"
	 * @generated
	 */
	VALIDTYPE getVALIDITY();

	/**
	 * Sets the value of the '{@link OdxXhtml.SCALECONSTR#getVALIDITY <em>VALIDITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VALIDITY</em>' attribute.
	 * @see OdxXhtml.VALIDTYPE
	 * @see #isSetVALIDITY()
	 * @see #unsetVALIDITY()
	 * @see #getVALIDITY()
	 * @generated
	 */
	void setVALIDITY(VALIDTYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.SCALECONSTR#getVALIDITY <em>VALIDITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVALIDITY()
	 * @see #getVALIDITY()
	 * @see #setVALIDITY(VALIDTYPE)
	 * @generated
	 */
	void unsetVALIDITY();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.SCALECONSTR#getVALIDITY <em>VALIDITY</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>VALIDITY</em>' attribute is set.
	 * @see #unsetVALIDITY()
	 * @see #getVALIDITY()
	 * @see #setVALIDITY(VALIDTYPE)
	 * @generated
	 */
	boolean isSetVALIDITY();

} // SCALECONSTR
