/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGCODEDTYPE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGCODEDTYPE#getBASEDATATYPE <em>BASEDATATYPE</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCODEDTYPE#getBASETYPEENCODING <em>BASETYPEENCODING</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCODEDTYPE#isISHIGHLOWBYTEORDER <em>ISHIGHLOWBYTEORDER</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCODEDTYPE()
 * @model extendedMetaData="name='DIAG-CODED-TYPE' kind='empty'"
 * @generated
 */
public interface DIAGCODEDTYPE extends EObject {
	/**
	 * Returns the value of the '<em><b>BASEDATATYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.DATATYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BASEDATATYPE</em>' attribute.
	 * @see OdxXhtml.DATATYPE
	 * @see #isSetBASEDATATYPE()
	 * @see #unsetBASEDATATYPE()
	 * @see #setBASEDATATYPE(DATATYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCODEDTYPE_BASEDATATYPE()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='BASE-DATA-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	DATATYPE getBASEDATATYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCODEDTYPE#getBASEDATATYPE <em>BASEDATATYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BASEDATATYPE</em>' attribute.
	 * @see OdxXhtml.DATATYPE
	 * @see #isSetBASEDATATYPE()
	 * @see #unsetBASEDATATYPE()
	 * @see #getBASEDATATYPE()
	 * @generated
	 */
	void setBASEDATATYPE(DATATYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGCODEDTYPE#getBASEDATATYPE <em>BASEDATATYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBASEDATATYPE()
	 * @see #getBASEDATATYPE()
	 * @see #setBASEDATATYPE(DATATYPE)
	 * @generated
	 */
	void unsetBASEDATATYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGCODEDTYPE#getBASEDATATYPE <em>BASEDATATYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BASEDATATYPE</em>' attribute is set.
	 * @see #unsetBASEDATATYPE()
	 * @see #getBASEDATATYPE()
	 * @see #setBASEDATATYPE(DATATYPE)
	 * @generated
	 */
	boolean isSetBASEDATATYPE();

	/**
	 * Returns the value of the '<em><b>BASETYPEENCODING</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.ENCODING}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BASETYPEENCODING</em>' attribute.
	 * @see OdxXhtml.ENCODING
	 * @see #isSetBASETYPEENCODING()
	 * @see #unsetBASETYPEENCODING()
	 * @see #setBASETYPEENCODING(ENCODING)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCODEDTYPE_BASETYPEENCODING()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='BASE-TYPE-ENCODING' namespace='##targetNamespace'"
	 * @generated
	 */
	ENCODING getBASETYPEENCODING();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCODEDTYPE#getBASETYPEENCODING <em>BASETYPEENCODING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BASETYPEENCODING</em>' attribute.
	 * @see OdxXhtml.ENCODING
	 * @see #isSetBASETYPEENCODING()
	 * @see #unsetBASETYPEENCODING()
	 * @see #getBASETYPEENCODING()
	 * @generated
	 */
	void setBASETYPEENCODING(ENCODING value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGCODEDTYPE#getBASETYPEENCODING <em>BASETYPEENCODING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBASETYPEENCODING()
	 * @see #getBASETYPEENCODING()
	 * @see #setBASETYPEENCODING(ENCODING)
	 * @generated
	 */
	void unsetBASETYPEENCODING();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGCODEDTYPE#getBASETYPEENCODING <em>BASETYPEENCODING</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BASETYPEENCODING</em>' attribute is set.
	 * @see #unsetBASETYPEENCODING()
	 * @see #getBASETYPEENCODING()
	 * @see #setBASETYPEENCODING(ENCODING)
	 * @generated
	 */
	boolean isSetBASETYPEENCODING();

	/**
	 * Returns the value of the '<em><b>ISHIGHLOWBYTEORDER</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISHIGHLOWBYTEORDER</em>' attribute.
	 * @see #isSetISHIGHLOWBYTEORDER()
	 * @see #unsetISHIGHLOWBYTEORDER()
	 * @see #setISHIGHLOWBYTEORDER(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCODEDTYPE_ISHIGHLOWBYTEORDER()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-HIGHLOW-BYTE-ORDER' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISHIGHLOWBYTEORDER();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCODEDTYPE#isISHIGHLOWBYTEORDER <em>ISHIGHLOWBYTEORDER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISHIGHLOWBYTEORDER</em>' attribute.
	 * @see #isSetISHIGHLOWBYTEORDER()
	 * @see #unsetISHIGHLOWBYTEORDER()
	 * @see #isISHIGHLOWBYTEORDER()
	 * @generated
	 */
	void setISHIGHLOWBYTEORDER(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGCODEDTYPE#isISHIGHLOWBYTEORDER <em>ISHIGHLOWBYTEORDER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISHIGHLOWBYTEORDER()
	 * @see #isISHIGHLOWBYTEORDER()
	 * @see #setISHIGHLOWBYTEORDER(boolean)
	 * @generated
	 */
	void unsetISHIGHLOWBYTEORDER();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGCODEDTYPE#isISHIGHLOWBYTEORDER <em>ISHIGHLOWBYTEORDER</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISHIGHLOWBYTEORDER</em>' attribute is set.
	 * @see #unsetISHIGHLOWBYTEORDER()
	 * @see #isISHIGHLOWBYTEORDER()
	 * @see #setISHIGHLOWBYTEORDER(boolean)
	 * @generated
	 */
	boolean isSetISHIGHLOWBYTEORDER();

} // DIAGCODEDTYPE
