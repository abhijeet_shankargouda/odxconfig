/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IType</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getIType()
 * @model extendedMetaData="name='i_._type' kind='mixed'"
 * @generated
 */
public interface IType extends Inline {
} // IType
