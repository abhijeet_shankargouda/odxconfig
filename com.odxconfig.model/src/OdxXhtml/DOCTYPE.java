/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DOCTYPE</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getDOCTYPE()
 * @model extendedMetaData="name='DOCTYPE'"
 * @generated
 */
public enum DOCTYPE implements Enumerator {
	/**
	 * The '<em><b>FLASH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FLASH_VALUE
	 * @generated
	 * @ordered
	 */
	FLASH(0, "FLASH", "FLASH"),

	/**
	 * The '<em><b>CONTAINER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTAINER_VALUE
	 * @generated
	 * @ordered
	 */
	CONTAINER(1, "CONTAINER", "CONTAINER"),

	/**
	 * The '<em><b>LAYER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LAYER_VALUE
	 * @generated
	 * @ordered
	 */
	LAYER(2, "LAYER", "LAYER"),

	/**
	 * The '<em><b>MULTIPLEECUJOBSPEC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MULTIPLEECUJOBSPEC_VALUE
	 * @generated
	 * @ordered
	 */
	MULTIPLEECUJOBSPEC(3, "MULTIPLEECUJOBSPEC", "MULTIPLE-ECU-JOB-SPEC"),

	/**
	 * The '<em><b>COMPARAMSPEC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPARAMSPEC_VALUE
	 * @generated
	 * @ordered
	 */
	COMPARAMSPEC(4, "COMPARAMSPEC", "COMPARAM-SPEC");

	/**
	 * The '<em><b>FLASH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FLASH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FLASH_VALUE = 0;

	/**
	 * The '<em><b>CONTAINER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTAINER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CONTAINER_VALUE = 1;

	/**
	 * The '<em><b>LAYER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LAYER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LAYER_VALUE = 2;

	/**
	 * The '<em><b>MULTIPLEECUJOBSPEC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MULTIPLEECUJOBSPEC
	 * @model literal="MULTIPLE-ECU-JOB-SPEC"
	 * @generated
	 * @ordered
	 */
	public static final int MULTIPLEECUJOBSPEC_VALUE = 3;

	/**
	 * The '<em><b>COMPARAMSPEC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPARAMSPEC
	 * @model literal="COMPARAM-SPEC"
	 * @generated
	 * @ordered
	 */
	public static final int COMPARAMSPEC_VALUE = 4;

	/**
	 * An array of all the '<em><b>DOCTYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DOCTYPE[] VALUES_ARRAY =
		new DOCTYPE[] {
			FLASH,
			CONTAINER,
			LAYER,
			MULTIPLEECUJOBSPEC,
			COMPARAMSPEC,
		};

	/**
	 * A public read-only list of all the '<em><b>DOCTYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DOCTYPE> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>DOCTYPE</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DOCTYPE get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DOCTYPE result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DOCTYPE</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DOCTYPE getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DOCTYPE result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DOCTYPE</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DOCTYPE get(int value) {
		switch (value) {
			case FLASH_VALUE: return FLASH;
			case CONTAINER_VALUE: return CONTAINER;
			case LAYER_VALUE: return LAYER;
			case MULTIPLEECUJOBSPEC_VALUE: return MULTIPLEECUJOBSPEC;
			case COMPARAMSPEC_VALUE: return COMPARAMSPEC;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DOCTYPE(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DOCTYPE
