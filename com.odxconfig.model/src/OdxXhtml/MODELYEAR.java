/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MODELYEAR</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMODELYEAR()
 * @model extendedMetaData="name='MODEL-YEAR' kind='elementOnly'"
 * @generated
 */
public interface MODELYEAR extends INFOCOMPONENT {
} // MODELYEAR
