/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SUPPORTEDDYNIDS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SUPPORTEDDYNIDS#getSUPPORTEDDYNID <em>SUPPORTEDDYNID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSUPPORTEDDYNIDS()
 * @model extendedMetaData="name='SUPPORTED-DYN-IDS' kind='elementOnly'"
 * @generated
 */
public interface SUPPORTEDDYNIDS extends EObject {
	/**
	 * Returns the value of the '<em><b>SUPPORTEDDYNID</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SUPPORTEDDYNID}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SUPPORTEDDYNID</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSUPPORTEDDYNIDS_SUPPORTEDDYNID()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SUPPORTED-DYN-ID' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SUPPORTEDDYNID> getSUPPORTEDDYNID();

} // SUPPORTEDDYNIDS
