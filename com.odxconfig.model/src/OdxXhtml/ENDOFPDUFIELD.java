/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ENDOFPDUFIELD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ENDOFPDUFIELD#getMAXNUMBEROFITEMS <em>MAXNUMBEROFITEMS</em>}</li>
 *   <li>{@link OdxXhtml.ENDOFPDUFIELD#getMINNUMBEROFITEMS <em>MINNUMBEROFITEMS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getENDOFPDUFIELD()
 * @model extendedMetaData="name='END-OF-PDU-FIELD' kind='elementOnly'"
 * @generated
 */
public interface ENDOFPDUFIELD extends FIELD {
	/**
	 * Returns the value of the '<em><b>MAXNUMBEROFITEMS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MAXNUMBEROFITEMS</em>' attribute.
	 * @see #isSetMAXNUMBEROFITEMS()
	 * @see #unsetMAXNUMBEROFITEMS()
	 * @see #setMAXNUMBEROFITEMS(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getENDOFPDUFIELD_MAXNUMBEROFITEMS()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='element' name='MAX-NUMBER-OF-ITEMS' namespace='##targetNamespace'"
	 * @generated
	 */
	long getMAXNUMBEROFITEMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ENDOFPDUFIELD#getMAXNUMBEROFITEMS <em>MAXNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MAXNUMBEROFITEMS</em>' attribute.
	 * @see #isSetMAXNUMBEROFITEMS()
	 * @see #unsetMAXNUMBEROFITEMS()
	 * @see #getMAXNUMBEROFITEMS()
	 * @generated
	 */
	void setMAXNUMBEROFITEMS(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.ENDOFPDUFIELD#getMAXNUMBEROFITEMS <em>MAXNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMAXNUMBEROFITEMS()
	 * @see #getMAXNUMBEROFITEMS()
	 * @see #setMAXNUMBEROFITEMS(long)
	 * @generated
	 */
	void unsetMAXNUMBEROFITEMS();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.ENDOFPDUFIELD#getMAXNUMBEROFITEMS <em>MAXNUMBEROFITEMS</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MAXNUMBEROFITEMS</em>' attribute is set.
	 * @see #unsetMAXNUMBEROFITEMS()
	 * @see #getMAXNUMBEROFITEMS()
	 * @see #setMAXNUMBEROFITEMS(long)
	 * @generated
	 */
	boolean isSetMAXNUMBEROFITEMS();

	/**
	 * Returns the value of the '<em><b>MINNUMBEROFITEMS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MINNUMBEROFITEMS</em>' attribute.
	 * @see #isSetMINNUMBEROFITEMS()
	 * @see #unsetMINNUMBEROFITEMS()
	 * @see #setMINNUMBEROFITEMS(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getENDOFPDUFIELD_MINNUMBEROFITEMS()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='element' name='MIN-NUMBER-OF-ITEMS' namespace='##targetNamespace'"
	 * @generated
	 */
	long getMINNUMBEROFITEMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ENDOFPDUFIELD#getMINNUMBEROFITEMS <em>MINNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MINNUMBEROFITEMS</em>' attribute.
	 * @see #isSetMINNUMBEROFITEMS()
	 * @see #unsetMINNUMBEROFITEMS()
	 * @see #getMINNUMBEROFITEMS()
	 * @generated
	 */
	void setMINNUMBEROFITEMS(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.ENDOFPDUFIELD#getMINNUMBEROFITEMS <em>MINNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMINNUMBEROFITEMS()
	 * @see #getMINNUMBEROFITEMS()
	 * @see #setMINNUMBEROFITEMS(long)
	 * @generated
	 */
	void unsetMINNUMBEROFITEMS();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.ENDOFPDUFIELD#getMINNUMBEROFITEMS <em>MINNUMBEROFITEMS</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MINNUMBEROFITEMS</em>' attribute is set.
	 * @see #unsetMINNUMBEROFITEMS()
	 * @see #getMINNUMBEROFITEMS()
	 * @see #setMINNUMBEROFITEMS(long)
	 * @generated
	 */
	boolean isSetMINNUMBEROFITEMS();

} // ENDOFPDUFIELD
