/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INTERNFLASHDATA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.INTERNFLASHDATA#getDATA <em>DATA</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getINTERNFLASHDATA()
 * @model extendedMetaData="name='INTERN-FLASHDATA' kind='elementOnly'"
 * @generated
 */
public interface INTERNFLASHDATA extends FLASHDATA {
	/**
	 * Returns the value of the '<em><b>DATA</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATA</em>' attribute.
	 * @see #setDATA(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getINTERNFLASHDATA_DATA()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.INTERNFLASHDATA#getDATA <em>DATA</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATA</em>' attribute.
	 * @see #getDATA()
	 * @generated
	 */
	void setDATA(String value);

} // INTERNFLASHDATA
