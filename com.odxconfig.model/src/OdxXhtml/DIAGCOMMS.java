/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGCOMMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGCOMMS#getDIAGCOMMPROXY <em>DIAGCOMMPROXY</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMMS#getDIAGSERVICE <em>DIAGSERVICE</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMMS#getSINGLEECUJOB <em>SINGLEECUJOB</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMMS#getDIAGCOMMREF <em>DIAGCOMMREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMMS()
 * @model extendedMetaData="name='DIAG-COMMS' kind='elementOnly'"
 * @generated
 */
public interface DIAGCOMMS extends EObject {
	/**
	 * Returns the value of the '<em><b>DIAGCOMMPROXY</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMPROXY</em>' attribute list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMMS_DIAGCOMMPROXY()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='DIAGCOMMPROXY:0'"
	 * @generated
	 */
	FeatureMap getDIAGCOMMPROXY();

	/**
	 * Returns the value of the '<em><b>DIAGSERVICE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DIAGSERVICE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGSERVICE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMMS_DIAGSERVICE()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DIAG-SERVICE' namespace='##targetNamespace' group='DIAGCOMMPROXY:0'"
	 * @generated
	 */
	EList<DIAGSERVICE> getDIAGSERVICE();

	/**
	 * Returns the value of the '<em><b>SINGLEECUJOB</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SINGLEECUJOB}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SINGLEECUJOB</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMMS_SINGLEECUJOB()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SINGLE-ECU-JOB' namespace='##targetNamespace' group='DIAGCOMMPROXY:0'"
	 * @generated
	 */
	EList<SINGLEECUJOB> getSINGLEECUJOB();

	/**
	 * Returns the value of the '<em><b>DIAGCOMMREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMMS_DIAGCOMMREF()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMM-REF' namespace='##targetNamespace' group='DIAGCOMMPROXY:0'"
	 * @generated
	 */
	EList<ODXLINK> getDIAGCOMMREF();

} // DIAGCOMMS
