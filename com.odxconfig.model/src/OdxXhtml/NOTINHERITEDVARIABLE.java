/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NOTINHERITEDVARIABLE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.NOTINHERITEDVARIABLE#getDIAGVARIABLESNREF <em>DIAGVARIABLESNREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getNOTINHERITEDVARIABLE()
 * @model extendedMetaData="name='NOT-INHERITED-VARIABLE' kind='elementOnly'"
 * @generated
 */
public interface NOTINHERITEDVARIABLE extends EObject {
	/**
	 * Returns the value of the '<em><b>DIAGVARIABLESNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGVARIABLESNREF</em>' containment reference.
	 * @see #setDIAGVARIABLESNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getNOTINHERITEDVARIABLE_DIAGVARIABLESNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-VARIABLE-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDIAGVARIABLESNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.NOTINHERITEDVARIABLE#getDIAGVARIABLESNREF <em>DIAGVARIABLESNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGVARIABLESNREF</em>' containment reference.
	 * @see #getDIAGVARIABLESNREF()
	 * @generated
	 */
	void setDIAGVARIABLESNREF(SNREF value);

} // NOTINHERITEDVARIABLE
