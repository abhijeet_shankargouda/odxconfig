/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IDENTDESCS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.IDENTDESCS#getIDENTDESC <em>IDENTDESC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getIDENTDESCS()
 * @model extendedMetaData="name='IDENT-DESCS' kind='elementOnly'"
 * @generated
 */
public interface IDENTDESCS extends EObject {
	/**
	 * Returns the value of the '<em><b>IDENTDESC</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.IDENTDESC}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IDENTDESC</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getIDENTDESCS_IDENTDESC()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='IDENT-DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<IDENTDESC> getIDENTDESC();

} // IDENTDESCS
