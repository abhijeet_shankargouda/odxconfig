/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PHYSICALDIMENSIONS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PHYSICALDIMENSIONS#getPHYSICALDIMENSION <em>PHYSICALDIMENSION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSIONS()
 * @model extendedMetaData="name='PHYSICAL-DIMENSIONS' kind='elementOnly'"
 * @generated
 */
public interface PHYSICALDIMENSIONS extends EObject {
	/**
	 * Returns the value of the '<em><b>PHYSICALDIMENSION</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.PHYSICALDIMENSION}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALDIMENSION</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDIMENSIONS_PHYSICALDIMENSION()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PHYSICAL-DIMENSION' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PHYSICALDIMENSION> getPHYSICALDIMENSION();

} // PHYSICALDIMENSIONS
