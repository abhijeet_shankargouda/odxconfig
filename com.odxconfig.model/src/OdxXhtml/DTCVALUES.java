/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DTCVALUES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DTCVALUES#getDTCVALUE <em>DTCVALUE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDTCVALUES()
 * @model extendedMetaData="name='DTC-VALUES' kind='elementOnly'"
 * @generated
 */
public interface DTCVALUES extends EObject {
	/**
	 * Returns the value of the '<em><b>DTCVALUE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DTCVALUE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DTCVALUE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCVALUES_DTCVALUE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DTC-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DTCVALUE> getDTCVALUE();

} // DTCVALUES
