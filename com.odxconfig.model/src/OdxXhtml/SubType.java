/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSubType()
 * @model extendedMetaData="name='sub_._type' kind='mixed'"
 * @generated
 */
public interface SubType extends Inline {
} // SubType
