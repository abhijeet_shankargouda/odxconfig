/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NEGRESPONSEREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.NEGRESPONSEREFS#getNEGRESPONSEREF <em>NEGRESPONSEREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getNEGRESPONSEREFS()
 * @model extendedMetaData="name='NEG-RESPONSE-REFS' kind='elementOnly'"
 * @generated
 */
public interface NEGRESPONSEREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>NEGRESPONSEREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NEGRESPONSEREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getNEGRESPONSEREFS_NEGRESPONSEREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='NEG-RESPONSE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getNEGRESPONSEREF();

} // NEGRESPONSEREFS
