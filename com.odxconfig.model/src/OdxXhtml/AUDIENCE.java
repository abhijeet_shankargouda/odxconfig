/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AUDIENCE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.AUDIENCE#isISAFTERMARKET <em>ISAFTERMARKET</em>}</li>
 *   <li>{@link OdxXhtml.AUDIENCE#isISAFTERSALES <em>ISAFTERSALES</em>}</li>
 *   <li>{@link OdxXhtml.AUDIENCE#isISDEVELOPMENT <em>ISDEVELOPMENT</em>}</li>
 *   <li>{@link OdxXhtml.AUDIENCE#isISMANUFACTURING <em>ISMANUFACTURING</em>}</li>
 *   <li>{@link OdxXhtml.AUDIENCE#isISSUPPLIER <em>ISSUPPLIER</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getAUDIENCE()
 * @model extendedMetaData="name='AUDIENCE' kind='empty'"
 * @generated
 */
public interface AUDIENCE extends EObject {
	/**
	 * Returns the value of the '<em><b>ISAFTERMARKET</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISAFTERMARKET</em>' attribute.
	 * @see #isSetISAFTERMARKET()
	 * @see #unsetISAFTERMARKET()
	 * @see #setISAFTERMARKET(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getAUDIENCE_ISAFTERMARKET()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-AFTERMARKET' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISAFTERMARKET();

	/**
	 * Sets the value of the '{@link OdxXhtml.AUDIENCE#isISAFTERMARKET <em>ISAFTERMARKET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISAFTERMARKET</em>' attribute.
	 * @see #isSetISAFTERMARKET()
	 * @see #unsetISAFTERMARKET()
	 * @see #isISAFTERMARKET()
	 * @generated
	 */
	void setISAFTERMARKET(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.AUDIENCE#isISAFTERMARKET <em>ISAFTERMARKET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISAFTERMARKET()
	 * @see #isISAFTERMARKET()
	 * @see #setISAFTERMARKET(boolean)
	 * @generated
	 */
	void unsetISAFTERMARKET();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.AUDIENCE#isISAFTERMARKET <em>ISAFTERMARKET</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISAFTERMARKET</em>' attribute is set.
	 * @see #unsetISAFTERMARKET()
	 * @see #isISAFTERMARKET()
	 * @see #setISAFTERMARKET(boolean)
	 * @generated
	 */
	boolean isSetISAFTERMARKET();

	/**
	 * Returns the value of the '<em><b>ISAFTERSALES</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISAFTERSALES</em>' attribute.
	 * @see #isSetISAFTERSALES()
	 * @see #unsetISAFTERSALES()
	 * @see #setISAFTERSALES(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getAUDIENCE_ISAFTERSALES()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-AFTERSALES' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISAFTERSALES();

	/**
	 * Sets the value of the '{@link OdxXhtml.AUDIENCE#isISAFTERSALES <em>ISAFTERSALES</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISAFTERSALES</em>' attribute.
	 * @see #isSetISAFTERSALES()
	 * @see #unsetISAFTERSALES()
	 * @see #isISAFTERSALES()
	 * @generated
	 */
	void setISAFTERSALES(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.AUDIENCE#isISAFTERSALES <em>ISAFTERSALES</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISAFTERSALES()
	 * @see #isISAFTERSALES()
	 * @see #setISAFTERSALES(boolean)
	 * @generated
	 */
	void unsetISAFTERSALES();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.AUDIENCE#isISAFTERSALES <em>ISAFTERSALES</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISAFTERSALES</em>' attribute is set.
	 * @see #unsetISAFTERSALES()
	 * @see #isISAFTERSALES()
	 * @see #setISAFTERSALES(boolean)
	 * @generated
	 */
	boolean isSetISAFTERSALES();

	/**
	 * Returns the value of the '<em><b>ISDEVELOPMENT</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISDEVELOPMENT</em>' attribute.
	 * @see #isSetISDEVELOPMENT()
	 * @see #unsetISDEVELOPMENT()
	 * @see #setISDEVELOPMENT(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getAUDIENCE_ISDEVELOPMENT()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-DEVELOPMENT' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISDEVELOPMENT();

	/**
	 * Sets the value of the '{@link OdxXhtml.AUDIENCE#isISDEVELOPMENT <em>ISDEVELOPMENT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISDEVELOPMENT</em>' attribute.
	 * @see #isSetISDEVELOPMENT()
	 * @see #unsetISDEVELOPMENT()
	 * @see #isISDEVELOPMENT()
	 * @generated
	 */
	void setISDEVELOPMENT(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.AUDIENCE#isISDEVELOPMENT <em>ISDEVELOPMENT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISDEVELOPMENT()
	 * @see #isISDEVELOPMENT()
	 * @see #setISDEVELOPMENT(boolean)
	 * @generated
	 */
	void unsetISDEVELOPMENT();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.AUDIENCE#isISDEVELOPMENT <em>ISDEVELOPMENT</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISDEVELOPMENT</em>' attribute is set.
	 * @see #unsetISDEVELOPMENT()
	 * @see #isISDEVELOPMENT()
	 * @see #setISDEVELOPMENT(boolean)
	 * @generated
	 */
	boolean isSetISDEVELOPMENT();

	/**
	 * Returns the value of the '<em><b>ISMANUFACTURING</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISMANUFACTURING</em>' attribute.
	 * @see #isSetISMANUFACTURING()
	 * @see #unsetISMANUFACTURING()
	 * @see #setISMANUFACTURING(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getAUDIENCE_ISMANUFACTURING()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-MANUFACTURING' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISMANUFACTURING();

	/**
	 * Sets the value of the '{@link OdxXhtml.AUDIENCE#isISMANUFACTURING <em>ISMANUFACTURING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISMANUFACTURING</em>' attribute.
	 * @see #isSetISMANUFACTURING()
	 * @see #unsetISMANUFACTURING()
	 * @see #isISMANUFACTURING()
	 * @generated
	 */
	void setISMANUFACTURING(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.AUDIENCE#isISMANUFACTURING <em>ISMANUFACTURING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISMANUFACTURING()
	 * @see #isISMANUFACTURING()
	 * @see #setISMANUFACTURING(boolean)
	 * @generated
	 */
	void unsetISMANUFACTURING();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.AUDIENCE#isISMANUFACTURING <em>ISMANUFACTURING</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISMANUFACTURING</em>' attribute is set.
	 * @see #unsetISMANUFACTURING()
	 * @see #isISMANUFACTURING()
	 * @see #setISMANUFACTURING(boolean)
	 * @generated
	 */
	boolean isSetISMANUFACTURING();

	/**
	 * Returns the value of the '<em><b>ISSUPPLIER</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISSUPPLIER</em>' attribute.
	 * @see #isSetISSUPPLIER()
	 * @see #unsetISSUPPLIER()
	 * @see #setISSUPPLIER(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getAUDIENCE_ISSUPPLIER()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-SUPPLIER' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISSUPPLIER();

	/**
	 * Sets the value of the '{@link OdxXhtml.AUDIENCE#isISSUPPLIER <em>ISSUPPLIER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISSUPPLIER</em>' attribute.
	 * @see #isSetISSUPPLIER()
	 * @see #unsetISSUPPLIER()
	 * @see #isISSUPPLIER()
	 * @generated
	 */
	void setISSUPPLIER(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.AUDIENCE#isISSUPPLIER <em>ISSUPPLIER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISSUPPLIER()
	 * @see #isISSUPPLIER()
	 * @see #setISSUPPLIER(boolean)
	 * @generated
	 */
	void unsetISSUPPLIER();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.AUDIENCE#isISSUPPLIER <em>ISSUPPLIER</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISSUPPLIER</em>' attribute is set.
	 * @see #unsetISSUPPLIER()
	 * @see #isISSUPPLIER()
	 * @see #setISSUPPLIER(boolean)
	 * @generated
	 */
	boolean isSetISSUPPLIER();

} // AUDIENCE
