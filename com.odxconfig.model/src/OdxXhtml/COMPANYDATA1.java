/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYDATA1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYDATA1#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA1#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA1#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA1#getROLES <em>ROLES</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA1#getTEAMMEMBERS <em>TEAMMEMBERS</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA1#getCOMPANYSPECIFICINFO <em>COMPANYSPECIFICINFO</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA1#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA1#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA1()
 * @model extendedMetaData="name='COMPANY-DATA' kind='elementOnly'"
 * @generated
 */
public interface COMPANYDATA1 extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA1_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA1#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA1_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT1 getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA1#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT1 value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA1_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION1 getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA1#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION1 value);

	/**
	 * Returns the value of the '<em><b>ROLES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ROLES</em>' containment reference.
	 * @see #setROLES(ROLES1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA1_ROLES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ROLES' namespace='##targetNamespace'"
	 * @generated
	 */
	ROLES1 getROLES();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA1#getROLES <em>ROLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ROLES</em>' containment reference.
	 * @see #getROLES()
	 * @generated
	 */
	void setROLES(ROLES1 value);

	/**
	 * Returns the value of the '<em><b>TEAMMEMBERS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TEAMMEMBERS</em>' containment reference.
	 * @see #setTEAMMEMBERS(TEAMMEMBERS1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA1_TEAMMEMBERS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TEAM-MEMBERS' namespace='##targetNamespace'"
	 * @generated
	 */
	TEAMMEMBERS1 getTEAMMEMBERS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA1#getTEAMMEMBERS <em>TEAMMEMBERS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TEAMMEMBERS</em>' containment reference.
	 * @see #getTEAMMEMBERS()
	 * @generated
	 */
	void setTEAMMEMBERS(TEAMMEMBERS1 value);

	/**
	 * Returns the value of the '<em><b>COMPANYSPECIFICINFO</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYSPECIFICINFO</em>' containment reference.
	 * @see #setCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA1_COMPANYSPECIFICINFO()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPANY-SPECIFIC-INFO' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPANYSPECIFICINFO1 getCOMPANYSPECIFICINFO();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA1#getCOMPANYSPECIFICINFO <em>COMPANYSPECIFICINFO</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYSPECIFICINFO</em>' containment reference.
	 * @see #getCOMPANYSPECIFICINFO()
	 * @generated
	 */
	void setCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO1 value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA1_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA1#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA1_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA1#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // COMPANYDATA1
