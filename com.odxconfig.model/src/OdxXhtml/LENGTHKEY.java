/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LENGTHKEY</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.LENGTHKEY#getDOPREF <em>DOPREF</em>}</li>
 *   <li>{@link OdxXhtml.LENGTHKEY#getDOPSNREF <em>DOPSNREF</em>}</li>
 *   <li>{@link OdxXhtml.LENGTHKEY#getID <em>ID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLENGTHKEY()
 * @model extendedMetaData="name='LENGTH-KEY' kind='elementOnly'"
 * @generated
 */
public interface LENGTHKEY extends POSITIONABLEPARAM {
	/**
	 * Returns the value of the '<em><b>DOPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPREF</em>' containment reference.
	 * @see #setDOPREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getLENGTHKEY_DOPREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DOP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDOPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.LENGTHKEY#getDOPREF <em>DOPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPREF</em>' containment reference.
	 * @see #getDOPREF()
	 * @generated
	 */
	void setDOPREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>DOPSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPSNREF</em>' containment reference.
	 * @see #setDOPSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getLENGTHKEY_DOPSNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DOP-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDOPSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.LENGTHKEY#getDOPSNREF <em>DOPSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPSNREF</em>' containment reference.
	 * @see #getDOPSNREF()
	 * @generated
	 */
	void setDOPSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getLENGTHKEY_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.LENGTHKEY#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

} // LENGTHKEY
