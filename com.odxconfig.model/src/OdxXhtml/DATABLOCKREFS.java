/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DATABLOCKREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DATABLOCKREFS#getDATABLOCKREF <em>DATABLOCKREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCKREFS()
 * @model extendedMetaData="name='DATABLOCK-REFS' kind='elementOnly'"
 * @generated
 */
public interface DATABLOCKREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>DATABLOCKREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATABLOCKREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCKREFS_DATABLOCKREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATABLOCK-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getDATABLOCKREF();

} // DATABLOCKREFS
