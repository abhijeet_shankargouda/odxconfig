/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TEAMMEMBERS1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.TEAMMEMBERS1#getTEAMMEMBER <em>TEAMMEMBER</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBERS1()
 * @model extendedMetaData="name='TEAM-MEMBERS' kind='elementOnly'"
 * @generated
 */
public interface TEAMMEMBERS1 extends EObject {
	/**
	 * Returns the value of the '<em><b>TEAMMEMBER</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.TEAMMEMBER1}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TEAMMEMBER</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBERS1_TEAMMEMBER()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='TEAM-MEMBER' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TEAMMEMBER1> getTEAMMEMBER();

} // TEAMMEMBERS1
