/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SESSIONS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SESSIONS#getSESSION <em>SESSION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONS()
 * @model extendedMetaData="name='SESSIONS' kind='elementOnly'"
 * @generated
 */
public interface SESSIONS extends EObject {
	/**
	 * Returns the value of the '<em><b>SESSION</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SESSION}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SESSION</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONS_SESSION()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SESSION' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SESSION> getSESSION();

} // SESSIONS
