/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DESCRIPTION</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DESCRIPTION#getP <em>P</em>}</li>
 *   <li>{@link OdxXhtml.DESCRIPTION#getTI <em>TI</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDESCRIPTION()
 * @model extendedMetaData="name='DESCRIPTION' kind='elementOnly'"
 * @generated
 */
public interface DESCRIPTION extends EObject {
	/**
	 * Returns the value of the '<em><b>P</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.P}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>P</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDESCRIPTION_P()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='p' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<P> getP();

	/**
	 * Returns the value of the '<em><b>TI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TI</em>' attribute.
	 * @see #setTI(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDESCRIPTION_TI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='TI' namespace='##targetNamespace'"
	 * @generated
	 */
	String getTI();

	/**
	 * Sets the value of the '{@link OdxXhtml.DESCRIPTION#getTI <em>TI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TI</em>' attribute.
	 * @see #getTI()
	 * @generated
	 */
	void setTI(String value);

} // DESCRIPTION
