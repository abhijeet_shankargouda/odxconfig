/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MATCHINGPARAMETERS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MATCHINGPARAMETERS#getMATCHINGPARAMETER <em>MATCHINGPARAMETER</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGPARAMETERS()
 * @model extendedMetaData="name='MATCHING-PARAMETERS' kind='elementOnly'"
 * @generated
 */
public interface MATCHINGPARAMETERS extends EObject {
	/**
	 * Returns the value of the '<em><b>MATCHINGPARAMETER</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.MATCHINGPARAMETER}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MATCHINGPARAMETER</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGPARAMETERS_MATCHINGPARAMETER()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MATCHING-PARAMETER' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MATCHINGPARAMETER> getMATCHINGPARAMETER();

} // MATCHINGPARAMETERS
