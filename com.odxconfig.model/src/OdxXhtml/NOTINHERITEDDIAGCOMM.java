/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NOTINHERITEDDIAGCOMM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.NOTINHERITEDDIAGCOMM#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getNOTINHERITEDDIAGCOMM()
 * @model extendedMetaData="name='NOT-INHERITED-DIAG-COMM' kind='elementOnly'"
 * @generated
 */
public interface NOTINHERITEDDIAGCOMM extends EObject {
	/**
	 * Returns the value of the '<em><b>DIAGCOMMSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #setDIAGCOMMSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getNOTINHERITEDDIAGCOMM_DIAGCOMMSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMM-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDIAGCOMMSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.NOTINHERITEDDIAGCOMM#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 */
	void setDIAGCOMMSNREF(SNREF value);

} // NOTINHERITEDDIAGCOMM
