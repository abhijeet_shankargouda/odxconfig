/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DTCDOPS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DTCDOPS#getDTCDOP <em>DTCDOP</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDTCDOPS()
 * @model extendedMetaData="name='DTC-DOPS' kind='elementOnly'"
 * @generated
 */
public interface DTCDOPS extends EObject {
	/**
	 * Returns the value of the '<em><b>DTCDOP</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DTCDOP}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DTCDOP</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCDOPS_DTCDOP()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DTC-DOP' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DTCDOP> getDTCDOP();

} // DTCDOPS
