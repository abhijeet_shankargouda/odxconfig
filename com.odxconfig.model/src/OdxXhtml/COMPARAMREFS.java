/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPARAMREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPARAMREFS#getCOMPARAMREF <em>COMPARAMREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREFS()
 * @model extendedMetaData="name='COMPARAM-REFS' kind='elementOnly'"
 * @generated
 */
public interface COMPARAMREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPARAMREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.COMPARAMREF}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPARAMREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMREFS_COMPARAMREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPARAM-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<COMPARAMREF> getCOMPARAMREF();

} // COMPARAMREFS
