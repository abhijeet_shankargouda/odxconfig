/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UNITSPEC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.UNITSPEC#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.UNITSPEC#getUNITGROUPS <em>UNITGROUPS</em>}</li>
 *   <li>{@link OdxXhtml.UNITSPEC#getUNITS <em>UNITS</em>}</li>
 *   <li>{@link OdxXhtml.UNITSPEC#getPHYSICALDIMENSIONS <em>PHYSICALDIMENSIONS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getUNITSPEC()
 * @model extendedMetaData="name='UNIT-SPEC' kind='elementOnly'"
 * @generated
 */
public interface UNITSPEC extends EObject {
	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITSPEC_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITSPEC#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA value);

	/**
	 * Returns the value of the '<em><b>UNITGROUPS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNITGROUPS</em>' containment reference.
	 * @see #setUNITGROUPS(UNITGROUPS)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITSPEC_UNITGROUPS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UNIT-GROUPS' namespace='##targetNamespace'"
	 * @generated
	 */
	UNITGROUPS getUNITGROUPS();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITSPEC#getUNITGROUPS <em>UNITGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UNITGROUPS</em>' containment reference.
	 * @see #getUNITGROUPS()
	 * @generated
	 */
	void setUNITGROUPS(UNITGROUPS value);

	/**
	 * Returns the value of the '<em><b>UNITS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNITS</em>' containment reference.
	 * @see #setUNITS(UNITS)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITSPEC_UNITS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UNITS' namespace='##targetNamespace'"
	 * @generated
	 */
	UNITS getUNITS();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITSPEC#getUNITS <em>UNITS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UNITS</em>' containment reference.
	 * @see #getUNITS()
	 * @generated
	 */
	void setUNITS(UNITS value);

	/**
	 * Returns the value of the '<em><b>PHYSICALDIMENSIONS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALDIMENSIONS</em>' containment reference.
	 * @see #setPHYSICALDIMENSIONS(PHYSICALDIMENSIONS)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITSPEC_PHYSICALDIMENSIONS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PHYSICAL-DIMENSIONS' namespace='##targetNamespace'"
	 * @generated
	 */
	PHYSICALDIMENSIONS getPHYSICALDIMENSIONS();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITSPEC#getPHYSICALDIMENSIONS <em>PHYSICALDIMENSIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSICALDIMENSIONS</em>' containment reference.
	 * @see #getPHYSICALDIMENSIONS()
	 * @generated
	 */
	void setPHYSICALDIMENSIONS(PHYSICALDIMENSIONS value);

} // UNITSPEC
