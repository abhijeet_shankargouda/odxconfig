/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUPROXYREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUPROXYREFS#getECUPROXYREF <em>ECUPROXYREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUPROXYREFS()
 * @model extendedMetaData="name='ECU-PROXY-REFS' kind='elementOnly'"
 * @generated
 */
public interface ECUPROXYREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>ECUPROXYREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUPROXYREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getECUPROXYREFS_ECUPROXYREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ECU-PROXY-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getECUPROXYREF();

} // ECUPROXYREFS
