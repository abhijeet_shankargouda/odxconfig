/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUVARIANTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUVARIANTS#getECUVARIANT <em>ECUVARIANT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUVARIANTS()
 * @model extendedMetaData="name='ECU-VARIANTS' kind='elementOnly'"
 * @generated
 */
public interface ECUVARIANTS extends EObject {
	/**
	 * Returns the value of the '<em><b>ECUVARIANT</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ECUVARIANT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUVARIANT</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getECUVARIANTS_ECUVARIANT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ECU-VARIANT' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ECUVARIANT> getECUVARIANT();

} // ECUVARIANTS
