/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LENGTHDESCRIPTOR</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLENGTHDESCRIPTOR()
 * @model extendedMetaData="name='LENGTH-DESCRIPTOR' kind='empty'"
 * @generated
 */
public interface LENGTHDESCRIPTOR extends EObject {
} // LENGTHDESCRIPTOR
