/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RELATEDDIAGCOMMREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.RELATEDDIAGCOMMREFS#getRELATEDDIAGCOMMREF <em>RELATEDDIAGCOMMREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDIAGCOMMREFS()
 * @model extendedMetaData="name='RELATED-DIAG-COMM-REFS' kind='elementOnly'"
 * @generated
 */
public interface RELATEDDIAGCOMMREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>RELATEDDIAGCOMMREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.RELATEDDIAGCOMMREF}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RELATEDDIAGCOMMREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDIAGCOMMREFS_RELATEDDIAGCOMMREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='RELATED-DIAG-COMM-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<RELATEDDIAGCOMMREF> getRELATEDDIAGCOMMREF();

} // RELATEDDIAGCOMMREFS
