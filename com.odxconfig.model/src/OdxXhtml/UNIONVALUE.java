/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UNIONVALUE</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getUNIONVALUE()
 * @model extendedMetaData="name='UNION-VALUE' kind='empty'"
 * @generated
 */
public interface UNIONVALUE extends EObject {
} // UNIONVALUE
