/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MATCHINGPARAMETER</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MATCHINGPARAMETER#getEXPECTEDVALUE <em>EXPECTEDVALUE</em>}</li>
 *   <li>{@link OdxXhtml.MATCHINGPARAMETER#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.MATCHINGPARAMETER#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGPARAMETER()
 * @model extendedMetaData="name='MATCHING-PARAMETER' kind='elementOnly'"
 * @generated
 */
public interface MATCHINGPARAMETER extends EObject {
	/**
	 * Returns the value of the '<em><b>EXPECTEDVALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EXPECTEDVALUE</em>' attribute.
	 * @see #setEXPECTEDVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGPARAMETER_EXPECTEDVALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='EXPECTED-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getEXPECTEDVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.MATCHINGPARAMETER#getEXPECTEDVALUE <em>EXPECTEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EXPECTEDVALUE</em>' attribute.
	 * @see #getEXPECTEDVALUE()
	 * @generated
	 */
	void setEXPECTEDVALUE(String value);

	/**
	 * Returns the value of the '<em><b>DIAGCOMMSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #setDIAGCOMMSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGPARAMETER_DIAGCOMMSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMM-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDIAGCOMMSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.MATCHINGPARAMETER#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 */
	void setDIAGCOMMSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>OUTPARAMIFSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OUTPARAMIFSNREF</em>' containment reference.
	 * @see #setOUTPARAMIFSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGPARAMETER_OUTPARAMIFSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OUT-PARAM-IF-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getOUTPARAMIFSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.MATCHINGPARAMETER#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OUTPARAMIFSNREF</em>' containment reference.
	 * @see #getOUTPARAMIFSNREF()
	 * @generated
	 */
	void setOUTPARAMIFSNREF(SNREF value);

} // MATCHINGPARAMETER
