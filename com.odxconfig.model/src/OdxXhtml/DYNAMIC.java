/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DYNAMIC</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMIC()
 * @model extendedMetaData="name='DYNAMIC' kind='elementOnly'"
 * @generated
 */
public interface DYNAMIC extends POSITIONABLEPARAM {
} // DYNAMIC
