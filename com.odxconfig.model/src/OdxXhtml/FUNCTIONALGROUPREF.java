/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FUNCTIONALGROUPREF</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTIONALGROUPREF()
 * @model extendedMetaData="name='FUNCTIONAL-GROUP-REF' kind='elementOnly'"
 * @generated
 */
public interface FUNCTIONALGROUPREF extends PARENTREF {
} // FUNCTIONALGROUPREF
