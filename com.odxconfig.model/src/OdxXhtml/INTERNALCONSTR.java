/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INTERNALCONSTR</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.INTERNALCONSTR#getLOWERLIMIT <em>LOWERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.INTERNALCONSTR#getUPPERLIMIT <em>UPPERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.INTERNALCONSTR#getSCALECONSTRS <em>SCALECONSTRS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getINTERNALCONSTR()
 * @model extendedMetaData="name='INTERNAL-CONSTR' kind='elementOnly'"
 * @generated
 */
public interface INTERNALCONSTR extends EObject {
	/**
	 * Returns the value of the '<em><b>LOWERLIMIT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LOWERLIMIT</em>' containment reference.
	 * @see #setLOWERLIMIT(LIMIT)
	 * @see OdxXhtml.OdxXhtmlPackage#getINTERNALCONSTR_LOWERLIMIT()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LOWER-LIMIT' namespace='##targetNamespace'"
	 * @generated
	 */
	LIMIT getLOWERLIMIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.INTERNALCONSTR#getLOWERLIMIT <em>LOWERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LOWERLIMIT</em>' containment reference.
	 * @see #getLOWERLIMIT()
	 * @generated
	 */
	void setLOWERLIMIT(LIMIT value);

	/**
	 * Returns the value of the '<em><b>UPPERLIMIT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UPPERLIMIT</em>' containment reference.
	 * @see #setUPPERLIMIT(LIMIT)
	 * @see OdxXhtml.OdxXhtmlPackage#getINTERNALCONSTR_UPPERLIMIT()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UPPER-LIMIT' namespace='##targetNamespace'"
	 * @generated
	 */
	LIMIT getUPPERLIMIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.INTERNALCONSTR#getUPPERLIMIT <em>UPPERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UPPERLIMIT</em>' containment reference.
	 * @see #getUPPERLIMIT()
	 * @generated
	 */
	void setUPPERLIMIT(LIMIT value);

	/**
	 * Returns the value of the '<em><b>SCALECONSTRS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SCALECONSTRS</em>' containment reference.
	 * @see #setSCALECONSTRS(SCALECONSTRS)
	 * @see OdxXhtml.OdxXhtmlPackage#getINTERNALCONSTR_SCALECONSTRS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SCALE-CONSTRS' namespace='##targetNamespace'"
	 * @generated
	 */
	SCALECONSTRS getSCALECONSTRS();

	/**
	 * Sets the value of the '{@link OdxXhtml.INTERNALCONSTR#getSCALECONSTRS <em>SCALECONSTRS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SCALECONSTRS</em>' containment reference.
	 * @see #getSCALECONSTRS()
	 * @generated
	 */
	void setSCALECONSTRS(SCALECONSTRS value);

} // INTERNALCONSTR
