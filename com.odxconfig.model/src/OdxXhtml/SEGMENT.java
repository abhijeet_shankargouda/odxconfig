/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SEGMENT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SEGMENT#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.SEGMENT#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.SEGMENT#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.SEGMENT#getSOURCESTARTADDRESS <em>SOURCESTARTADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.SEGMENT#getCOMPRESSEDSIZE <em>COMPRESSEDSIZE</em>}</li>
 *   <li>{@link OdxXhtml.SEGMENT#getUNCOMPRESSEDSIZE <em>UNCOMPRESSEDSIZE</em>}</li>
 *   <li>{@link OdxXhtml.SEGMENT#getSOURCEENDADDRESS <em>SOURCEENDADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.SEGMENT#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.SEGMENT#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT()
 * @model extendedMetaData="name='SEGMENT' kind='elementOnly'"
 * @generated
 */
public interface SEGMENT extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.SEGMENT#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.SEGMENT#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.SEGMENT#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>SOURCESTARTADDRESS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SOURCESTARTADDRESS</em>' attribute.
	 * @see #setSOURCESTARTADDRESS(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT_SOURCESTARTADDRESS()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary" required="true"
	 *        extendedMetaData="kind='element' name='SOURCE-START-ADDRESS' namespace='##targetNamespace'"
	 * @generated
	 */
	byte[] getSOURCESTARTADDRESS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SEGMENT#getSOURCESTARTADDRESS <em>SOURCESTARTADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SOURCESTARTADDRESS</em>' attribute.
	 * @see #getSOURCESTARTADDRESS()
	 * @generated
	 */
	void setSOURCESTARTADDRESS(byte[] value);

	/**
	 * Returns the value of the '<em><b>COMPRESSEDSIZE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPRESSEDSIZE</em>' attribute.
	 * @see #isSetCOMPRESSEDSIZE()
	 * @see #unsetCOMPRESSEDSIZE()
	 * @see #setCOMPRESSEDSIZE(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT_COMPRESSEDSIZE()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='element' name='COMPRESSED-SIZE' namespace='##targetNamespace'"
	 * @generated
	 */
	long getCOMPRESSEDSIZE();

	/**
	 * Sets the value of the '{@link OdxXhtml.SEGMENT#getCOMPRESSEDSIZE <em>COMPRESSEDSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPRESSEDSIZE</em>' attribute.
	 * @see #isSetCOMPRESSEDSIZE()
	 * @see #unsetCOMPRESSEDSIZE()
	 * @see #getCOMPRESSEDSIZE()
	 * @generated
	 */
	void setCOMPRESSEDSIZE(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.SEGMENT#getCOMPRESSEDSIZE <em>COMPRESSEDSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCOMPRESSEDSIZE()
	 * @see #getCOMPRESSEDSIZE()
	 * @see #setCOMPRESSEDSIZE(long)
	 * @generated
	 */
	void unsetCOMPRESSEDSIZE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.SEGMENT#getCOMPRESSEDSIZE <em>COMPRESSEDSIZE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>COMPRESSEDSIZE</em>' attribute is set.
	 * @see #unsetCOMPRESSEDSIZE()
	 * @see #getCOMPRESSEDSIZE()
	 * @see #setCOMPRESSEDSIZE(long)
	 * @generated
	 */
	boolean isSetCOMPRESSEDSIZE();

	/**
	 * Returns the value of the '<em><b>UNCOMPRESSEDSIZE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNCOMPRESSEDSIZE</em>' containment reference.
	 * @see #setUNCOMPRESSEDSIZE(UNCOMPRESSEDSIZE)
	 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT_UNCOMPRESSEDSIZE()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UNCOMPRESSED-SIZE' namespace='##targetNamespace'"
	 * @generated
	 */
	UNCOMPRESSEDSIZE getUNCOMPRESSEDSIZE();

	/**
	 * Sets the value of the '{@link OdxXhtml.SEGMENT#getUNCOMPRESSEDSIZE <em>UNCOMPRESSEDSIZE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UNCOMPRESSEDSIZE</em>' containment reference.
	 * @see #getUNCOMPRESSEDSIZE()
	 * @generated
	 */
	void setUNCOMPRESSEDSIZE(UNCOMPRESSEDSIZE value);

	/**
	 * Returns the value of the '<em><b>SOURCEENDADDRESS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SOURCEENDADDRESS</em>' containment reference.
	 * @see #setSOURCEENDADDRESS(SOURCEENDADDRESS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT_SOURCEENDADDRESS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SOURCE-END-ADDRESS' namespace='##targetNamespace'"
	 * @generated
	 */
	SOURCEENDADDRESS getSOURCEENDADDRESS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SEGMENT#getSOURCEENDADDRESS <em>SOURCEENDADDRESS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SOURCEENDADDRESS</em>' containment reference.
	 * @see #getSOURCEENDADDRESS()
	 * @generated
	 */
	void setSOURCEENDADDRESS(SOURCEENDADDRESS value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.SEGMENT#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSEGMENT_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.SEGMENT#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // SEGMENT
