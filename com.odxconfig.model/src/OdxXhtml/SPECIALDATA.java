/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SPECIALDATA</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSPECIALDATA()
 * @model extendedMetaData="name='SPECIAL-DATA' kind='empty'"
 * @generated
 */
public interface SPECIALDATA extends EObject {
} // SPECIALDATA
