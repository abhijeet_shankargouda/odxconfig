/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MODIFICATIONS1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MODIFICATIONS1#getMODIFICATION <em>MODIFICATION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMODIFICATIONS1()
 * @model extendedMetaData="name='MODIFICATIONS' kind='elementOnly'"
 * @generated
 */
public interface MODIFICATIONS1 extends EObject {
	/**
	 * Returns the value of the '<em><b>MODIFICATION</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.MODIFICATION1}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MODIFICATION</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getMODIFICATIONS1_MODIFICATION()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MODIFICATION' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MODIFICATION1> getMODIFICATION();

} // MODIFICATIONS1
