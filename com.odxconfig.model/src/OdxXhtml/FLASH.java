/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FLASH</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FLASH#getECUMEMS <em>ECUMEMS</em>}</li>
 *   <li>{@link OdxXhtml.FLASH#getECUMEMCONNECTORS <em>ECUMEMCONNECTORS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFLASH()
 * @model extendedMetaData="name='FLASH' kind='elementOnly'"
 * @generated
 */
public interface FLASH extends ODXCATEGORY {
	/**
	 * Returns the value of the '<em><b>ECUMEMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUMEMS</em>' containment reference.
	 * @see #setECUMEMS(ECUMEMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASH_ECUMEMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ECU-MEMS' namespace='##targetNamespace'"
	 * @generated
	 */
	ECUMEMS getECUMEMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.FLASH#getECUMEMS <em>ECUMEMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ECUMEMS</em>' containment reference.
	 * @see #getECUMEMS()
	 * @generated
	 */
	void setECUMEMS(ECUMEMS value);

	/**
	 * Returns the value of the '<em><b>ECUMEMCONNECTORS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUMEMCONNECTORS</em>' containment reference.
	 * @see #setECUMEMCONNECTORS(ECUMEMCONNECTORS)
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASH_ECUMEMCONNECTORS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ECU-MEM-CONNECTORS' namespace='##targetNamespace'"
	 * @generated
	 */
	ECUMEMCONNECTORS getECUMEMCONNECTORS();

	/**
	 * Sets the value of the '{@link OdxXhtml.FLASH#getECUMEMCONNECTORS <em>ECUMEMCONNECTORS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ECUMEMCONNECTORS</em>' containment reference.
	 * @see #getECUMEMCONNECTORS()
	 * @generated
	 */
	void setECUMEMCONNECTORS(ECUMEMCONNECTORS value);

} // FLASH
