/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BASICSTRUCTURE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.BASICSTRUCTURE#getBYTESIZE <em>BYTESIZE</em>}</li>
 *   <li>{@link OdxXhtml.BASICSTRUCTURE#getPARAMS <em>PARAMS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getBASICSTRUCTURE()
 * @model extendedMetaData="name='BASIC-STRUCTURE' kind='elementOnly'"
 * @generated
 */
public interface BASICSTRUCTURE extends COMPLEXDOP {
	/**
	 * Returns the value of the '<em><b>BYTESIZE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BYTESIZE</em>' attribute.
	 * @see #isSetBYTESIZE()
	 * @see #unsetBYTESIZE()
	 * @see #setBYTESIZE(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getBASICSTRUCTURE_BYTESIZE()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='element' name='BYTE-SIZE' namespace='##targetNamespace'"
	 * @generated
	 */
	long getBYTESIZE();

	/**
	 * Sets the value of the '{@link OdxXhtml.BASICSTRUCTURE#getBYTESIZE <em>BYTESIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BYTESIZE</em>' attribute.
	 * @see #isSetBYTESIZE()
	 * @see #unsetBYTESIZE()
	 * @see #getBYTESIZE()
	 * @generated
	 */
	void setBYTESIZE(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.BASICSTRUCTURE#getBYTESIZE <em>BYTESIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBYTESIZE()
	 * @see #getBYTESIZE()
	 * @see #setBYTESIZE(long)
	 * @generated
	 */
	void unsetBYTESIZE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.BASICSTRUCTURE#getBYTESIZE <em>BYTESIZE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BYTESIZE</em>' attribute is set.
	 * @see #unsetBYTESIZE()
	 * @see #getBYTESIZE()
	 * @see #setBYTESIZE(long)
	 * @generated
	 */
	boolean isSetBYTESIZE();

	/**
	 * Returns the value of the '<em><b>PARAMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PARAMS</em>' containment reference.
	 * @see #setPARAMS(PARAMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getBASICSTRUCTURE_PARAMS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PARAMS' namespace='##targetNamespace'"
	 * @generated
	 */
	PARAMS getPARAMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.BASICSTRUCTURE#getPARAMS <em>PARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PARAMS</em>' containment reference.
	 * @see #getPARAMS()
	 * @generated
	 */
	void setPARAMS(PARAMS value);

} // BASICSTRUCTURE
