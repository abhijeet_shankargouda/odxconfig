/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYDATAS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYDATAS#getCOMPANYDATA <em>COMPANYDATA</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATAS()
 * @model extendedMetaData="name='COMPANY-DATAS' kind='elementOnly'"
 * @generated
 */
public interface COMPANYDATAS extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPANYDATA</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.COMPANYDATA}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYDATA</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATAS_COMPANYDATA()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPANY-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<COMPANYDATA> getCOMPANYDATA();

} // COMPANYDATAS
