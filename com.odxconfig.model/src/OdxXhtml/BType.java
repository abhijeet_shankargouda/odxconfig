/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BType</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getBType()
 * @model extendedMetaData="name='b_._type' kind='mixed'"
 * @generated
 */
public interface BType extends Inline {
} // BType
