/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ADDRDEFFILTER</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ADDRDEFFILTER#getFILTEREND <em>FILTEREND</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getADDRDEFFILTER()
 * @model extendedMetaData="name='ADDRDEF-FILTER' kind='elementOnly'"
 * @generated
 */
public interface ADDRDEFFILTER extends FILTER {
	/**
	 * Returns the value of the '<em><b>FILTEREND</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FILTEREND</em>' attribute.
	 * @see #setFILTEREND(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getADDRDEFFILTER_FILTEREND()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary" required="true"
	 *        extendedMetaData="kind='element' name='FILTER-END' namespace='##targetNamespace'"
	 * @generated
	 */
	byte[] getFILTEREND();

	/**
	 * Sets the value of the '{@link OdxXhtml.ADDRDEFFILTER#getFILTEREND <em>FILTEREND</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FILTEREND</em>' attribute.
	 * @see #getFILTEREND()
	 * @generated
	 */
	void setFILTEREND(byte[] value);

} // ADDRDEFFILTER
