/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUSHAREDDATAREF</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUSHAREDDATAREF()
 * @model extendedMetaData="name='ECU-SHARED-DATA-REF' kind='elementOnly'"
 * @generated
 */
public interface ECUSHAREDDATAREF extends PARENTREF {
} // ECUSHAREDDATAREF
