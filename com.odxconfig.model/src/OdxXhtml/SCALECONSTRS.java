/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SCALECONSTRS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SCALECONSTRS#getSCALECONSTR <em>SCALECONSTR</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSCALECONSTRS()
 * @model extendedMetaData="name='SCALE-CONSTRS' kind='elementOnly'"
 * @generated
 */
public interface SCALECONSTRS extends EObject {
	/**
	 * Returns the value of the '<em><b>SCALECONSTR</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SCALECONSTR}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SCALECONSTR</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSCALECONSTRS_SCALECONSTR()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SCALE-CONSTR' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SCALECONSTR> getSCALECONSTR();

} // SCALECONSTRS
