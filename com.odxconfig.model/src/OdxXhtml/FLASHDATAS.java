/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FLASHDATAS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FLASHDATAS#getFLASHDATA <em>FLASHDATA</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATAS()
 * @model extendedMetaData="name='FLASHDATAS' kind='elementOnly'"
 * @generated
 */
public interface FLASHDATAS extends EObject {
	/**
	 * Returns the value of the '<em><b>FLASHDATA</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.FLASHDATA}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FLASHDATA</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATAS_FLASHDATA()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FLASHDATA' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FLASHDATA> getFLASHDATA();

} // FLASHDATAS
