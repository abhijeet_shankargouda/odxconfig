/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DIAGCLASSTYPE</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCLASSTYPE()
 * @model extendedMetaData="name='DIAG-CLASS-TYPE'"
 * @generated
 */
public enum DIAGCLASSTYPE implements Enumerator {
	/**
	 * The '<em><b>STARTCOMM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STARTCOMM_VALUE
	 * @generated
	 * @ordered
	 */
	STARTCOMM(0, "STARTCOMM", "STARTCOMM"),

	/**
	 * The '<em><b>STOPCOMM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STOPCOMM_VALUE
	 * @generated
	 * @ordered
	 */
	STOPCOMM(1, "STOPCOMM", "STOPCOMM"),

	/**
	 * The '<em><b>VARIANTIDENTIFICATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VARIANTIDENTIFICATION_VALUE
	 * @generated
	 * @ordered
	 */
	VARIANTIDENTIFICATION(2, "VARIANTIDENTIFICATION", "VARIANTIDENTIFICATION"),

	/**
	 * The '<em><b>READDYNDEFINEDMESSAGE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #READDYNDEFINEDMESSAGE_VALUE
	 * @generated
	 * @ordered
	 */
	READDYNDEFINEDMESSAGE(3, "READDYNDEFINEDMESSAGE", "READ-DYN-DEFINED-MESSAGE"),

	/**
	 * The '<em><b>DYNDEFMESSAGE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DYNDEFMESSAGE_VALUE
	 * @generated
	 * @ordered
	 */
	DYNDEFMESSAGE(4, "DYNDEFMESSAGE", "DYN-DEF-MESSAGE"),

	/**
	 * The '<em><b>CLEARDYNDEFMESSAGE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLEARDYNDEFMESSAGE_VALUE
	 * @generated
	 * @ordered
	 */
	CLEARDYNDEFMESSAGE(5, "CLEARDYNDEFMESSAGE", "CLEAR-DYN-DEF-MESSAGE");

	/**
	 * The '<em><b>STARTCOMM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STARTCOMM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STARTCOMM_VALUE = 0;

	/**
	 * The '<em><b>STOPCOMM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STOPCOMM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STOPCOMM_VALUE = 1;

	/**
	 * The '<em><b>VARIANTIDENTIFICATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VARIANTIDENTIFICATION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VARIANTIDENTIFICATION_VALUE = 2;

	/**
	 * The '<em><b>READDYNDEFINEDMESSAGE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #READDYNDEFINEDMESSAGE
	 * @model literal="READ-DYN-DEFINED-MESSAGE"
	 * @generated
	 * @ordered
	 */
	public static final int READDYNDEFINEDMESSAGE_VALUE = 3;

	/**
	 * The '<em><b>DYNDEFMESSAGE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DYNDEFMESSAGE
	 * @model literal="DYN-DEF-MESSAGE"
	 * @generated
	 * @ordered
	 */
	public static final int DYNDEFMESSAGE_VALUE = 4;

	/**
	 * The '<em><b>CLEARDYNDEFMESSAGE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLEARDYNDEFMESSAGE
	 * @model literal="CLEAR-DYN-DEF-MESSAGE"
	 * @generated
	 * @ordered
	 */
	public static final int CLEARDYNDEFMESSAGE_VALUE = 5;

	/**
	 * An array of all the '<em><b>DIAGCLASSTYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DIAGCLASSTYPE[] VALUES_ARRAY =
		new DIAGCLASSTYPE[] {
			STARTCOMM,
			STOPCOMM,
			VARIANTIDENTIFICATION,
			READDYNDEFINEDMESSAGE,
			DYNDEFMESSAGE,
			CLEARDYNDEFMESSAGE,
		};

	/**
	 * A public read-only list of all the '<em><b>DIAGCLASSTYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DIAGCLASSTYPE> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>DIAGCLASSTYPE</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DIAGCLASSTYPE get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DIAGCLASSTYPE result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DIAGCLASSTYPE</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DIAGCLASSTYPE getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DIAGCLASSTYPE result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DIAGCLASSTYPE</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DIAGCLASSTYPE get(int value) {
		switch (value) {
			case STARTCOMM_VALUE: return STARTCOMM;
			case STOPCOMM_VALUE: return STOPCOMM;
			case VARIANTIDENTIFICATION_VALUE: return VARIANTIDENTIFICATION;
			case READDYNDEFINEDMESSAGE_VALUE: return READDYNDEFINEDMESSAGE;
			case DYNDEFMESSAGE_VALUE: return DYNDEFMESSAGE;
			case CLEARDYNDEFMESSAGE_VALUE: return CLEARDYNDEFMESSAGE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DIAGCLASSTYPE(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DIAGCLASSTYPE
