/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UNITGROUP</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.UNITGROUP#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.UNITGROUP#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.UNITGROUP#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.UNITGROUP#getCATEGORY <em>CATEGORY</em>}</li>
 *   <li>{@link OdxXhtml.UNITGROUP#getUNITREFS <em>UNITREFS</em>}</li>
 *   <li>{@link OdxXhtml.UNITGROUP#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getUNITGROUP()
 * @model extendedMetaData="name='UNIT-GROUP' kind='elementOnly'"
 * @generated
 */
public interface UNITGROUP extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITGROUP_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITGROUP#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITGROUP_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITGROUP#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITGROUP_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITGROUP#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>CATEGORY</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.UNITGROUPCATEGORY}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CATEGORY</em>' attribute.
	 * @see OdxXhtml.UNITGROUPCATEGORY
	 * @see #isSetCATEGORY()
	 * @see #unsetCATEGORY()
	 * @see #setCATEGORY(UNITGROUPCATEGORY)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITGROUP_CATEGORY()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='element' name='CATEGORY' namespace='##targetNamespace'"
	 * @generated
	 */
	UNITGROUPCATEGORY getCATEGORY();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITGROUP#getCATEGORY <em>CATEGORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CATEGORY</em>' attribute.
	 * @see OdxXhtml.UNITGROUPCATEGORY
	 * @see #isSetCATEGORY()
	 * @see #unsetCATEGORY()
	 * @see #getCATEGORY()
	 * @generated
	 */
	void setCATEGORY(UNITGROUPCATEGORY value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.UNITGROUP#getCATEGORY <em>CATEGORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCATEGORY()
	 * @see #getCATEGORY()
	 * @see #setCATEGORY(UNITGROUPCATEGORY)
	 * @generated
	 */
	void unsetCATEGORY();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.UNITGROUP#getCATEGORY <em>CATEGORY</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>CATEGORY</em>' attribute is set.
	 * @see #unsetCATEGORY()
	 * @see #getCATEGORY()
	 * @see #setCATEGORY(UNITGROUPCATEGORY)
	 * @generated
	 */
	boolean isSetCATEGORY();

	/**
	 * Returns the value of the '<em><b>UNITREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNITREFS</em>' containment reference.
	 * @see #setUNITREFS(UNITREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITGROUP_UNITREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UNIT-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	UNITREFS getUNITREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITGROUP#getUNITREFS <em>UNITREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UNITREFS</em>' containment reference.
	 * @see #getUNITREFS()
	 * @generated
	 */
	void setUNITREFS(UNITREFS value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITGROUP_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNITGROUP#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // UNITGROUP
