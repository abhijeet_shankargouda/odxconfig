/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYREVISIONINFOS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYREVISIONINFOS#getCOMPANYREVISIONINFO <em>COMPANYREVISIONINFO</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFOS()
 * @model extendedMetaData="name='COMPANY-REVISION-INFOS' kind='elementOnly'"
 * @generated
 */
public interface COMPANYREVISIONINFOS extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPANYREVISIONINFO</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.COMPANYREVISIONINFO}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYREVISIONINFO</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFOS_COMPANYREVISIONINFO()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPANY-REVISION-INFO' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<COMPANYREVISIONINFO> getCOMPANYREVISIONINFO();

} // COMPANYREVISIONINFOS
