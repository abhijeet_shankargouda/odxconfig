/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SYSTEM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SYSTEM#getDOPREF <em>DOPREF</em>}</li>
 *   <li>{@link OdxXhtml.SYSTEM#getDOPSNREF <em>DOPSNREF</em>}</li>
 *   <li>{@link OdxXhtml.SYSTEM#getSYSPARAM <em>SYSPARAM</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSYSTEM()
 * @model extendedMetaData="name='SYSTEM' kind='elementOnly'"
 * @generated
 */
public interface SYSTEM extends POSITIONABLEPARAM {
	/**
	 * Returns the value of the '<em><b>DOPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPREF</em>' containment reference.
	 * @see #setDOPREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getSYSTEM_DOPREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DOP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDOPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.SYSTEM#getDOPREF <em>DOPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPREF</em>' containment reference.
	 * @see #getDOPREF()
	 * @generated
	 */
	void setDOPREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>DOPSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPSNREF</em>' containment reference.
	 * @see #setDOPSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getSYSTEM_DOPSNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DOP-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDOPSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.SYSTEM#getDOPSNREF <em>DOPSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPSNREF</em>' containment reference.
	 * @see #getDOPSNREF()
	 * @generated
	 */
	void setDOPSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>SYSPARAM</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SYSPARAM</em>' attribute.
	 * @see #setSYSPARAM(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSYSTEM_SYSPARAM()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='SYSPARAM' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSYSPARAM();

	/**
	 * Sets the value of the '{@link OdxXhtml.SYSTEM#getSYSPARAM <em>SYSPARAM</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SYSPARAM</em>' attribute.
	 * @see #getSYSPARAM()
	 * @generated
	 */
	void setSYSPARAM(String value);

} // SYSTEM
