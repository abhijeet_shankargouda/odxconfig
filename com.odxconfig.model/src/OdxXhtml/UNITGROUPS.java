/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UNITGROUPS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.UNITGROUPS#getUNITGROUP <em>UNITGROUP</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getUNITGROUPS()
 * @model extendedMetaData="name='UNIT-GROUPS' kind='elementOnly'"
 * @generated
 */
public interface UNITGROUPS extends EObject {
	/**
	 * Returns the value of the '<em><b>UNITGROUP</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.UNITGROUP}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNITGROUP</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getUNITGROUPS_UNITGROUP()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='UNIT-GROUP' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<UNITGROUP> getUNITGROUP();

} // UNITGROUPS
