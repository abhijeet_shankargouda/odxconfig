/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPUINTERNALTOPHYS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPUINTERNALTOPHYS#getCOMPUSCALES <em>COMPUSCALES</em>}</li>
 *   <li>{@link OdxXhtml.COMPUINTERNALTOPHYS#getPROGCODE <em>PROGCODE</em>}</li>
 *   <li>{@link OdxXhtml.COMPUINTERNALTOPHYS#getCOMPUDEFAULTVALUE <em>COMPUDEFAULTVALUE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUINTERNALTOPHYS()
 * @model extendedMetaData="name='COMPU-INTERNAL-TO-PHYS' kind='elementOnly'"
 * @generated
 */
public interface COMPUINTERNALTOPHYS extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPUSCALES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUSCALES</em>' containment reference.
	 * @see #setCOMPUSCALES(COMPUSCALES)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUINTERNALTOPHYS_COMPUSCALES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPU-SCALES' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUSCALES getCOMPUSCALES();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUINTERNALTOPHYS#getCOMPUSCALES <em>COMPUSCALES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUSCALES</em>' containment reference.
	 * @see #getCOMPUSCALES()
	 * @generated
	 */
	void setCOMPUSCALES(COMPUSCALES value);

	/**
	 * Returns the value of the '<em><b>PROGCODE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROGCODE</em>' containment reference.
	 * @see #setPROGCODE(PROGCODE)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUINTERNALTOPHYS_PROGCODE()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PROG-CODE' namespace='##targetNamespace'"
	 * @generated
	 */
	PROGCODE getPROGCODE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUINTERNALTOPHYS#getPROGCODE <em>PROGCODE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROGCODE</em>' containment reference.
	 * @see #getPROGCODE()
	 * @generated
	 */
	void setPROGCODE(PROGCODE value);

	/**
	 * Returns the value of the '<em><b>COMPUDEFAULTVALUE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUDEFAULTVALUE</em>' containment reference.
	 * @see #setCOMPUDEFAULTVALUE(COMPUDEFAULTVALUE)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUINTERNALTOPHYS_COMPUDEFAULTVALUE()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPU-DEFAULT-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUDEFAULTVALUE getCOMPUDEFAULTVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUINTERNALTOPHYS#getCOMPUDEFAULTVALUE <em>COMPUDEFAULTVALUE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUDEFAULTVALUE</em>' containment reference.
	 * @see #getCOMPUDEFAULTVALUE()
	 * @generated
	 */
	void setCOMPUDEFAULTVALUE(COMPUDEFAULTVALUE value);

} // COMPUINTERNALTOPHYS
