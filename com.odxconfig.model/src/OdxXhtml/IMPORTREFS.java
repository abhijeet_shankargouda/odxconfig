/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IMPORTREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.IMPORTREFS#getIMPORTREF <em>IMPORTREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getIMPORTREFS()
 * @model extendedMetaData="name='IMPORT-REFS' kind='elementOnly'"
 * @generated
 */
public interface IMPORTREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>IMPORTREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IMPORTREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getIMPORTREFS_IMPORTREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='IMPORT-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getIMPORTREF();

} // IMPORTREFS
