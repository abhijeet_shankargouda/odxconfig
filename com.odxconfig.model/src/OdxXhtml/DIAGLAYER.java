/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGLAYER</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGLAYER#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getCOMPANYDATAS <em>COMPANYDATAS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getFUNCTCLASSS <em>FUNCTCLASSS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getDIAGDATADICTIONARYSPEC <em>DIAGDATADICTIONARYSPEC</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getDIAGCOMMS <em>DIAGCOMMS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getREQUESTS <em>REQUESTS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getPOSRESPONSES <em>POSRESPONSES</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getNEGRESPONSES <em>NEGRESPONSES</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getGLOBALNEGRESPONSES <em>GLOBALNEGRESPONSES</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.DIAGLAYER#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER()
 * @model extendedMetaData="name='DIAG-LAYER' kind='elementOnly'"
 * @generated
 */
public interface DIAGLAYER extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA value);

	/**
	 * Returns the value of the '<em><b>COMPANYDATAS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYDATAS</em>' containment reference.
	 * @see #setCOMPANYDATAS(COMPANYDATAS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_COMPANYDATAS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPANY-DATAS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPANYDATAS getCOMPANYDATAS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getCOMPANYDATAS <em>COMPANYDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYDATAS</em>' containment reference.
	 * @see #getCOMPANYDATAS()
	 * @generated
	 */
	void setCOMPANYDATAS(COMPANYDATAS value);

	/**
	 * Returns the value of the '<em><b>FUNCTCLASSS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FUNCTCLASSS</em>' containment reference.
	 * @see #setFUNCTCLASSS(FUNCTCLASSS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_FUNCTCLASSS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FUNCT-CLASSS' namespace='##targetNamespace'"
	 * @generated
	 */
	FUNCTCLASSS getFUNCTCLASSS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getFUNCTCLASSS <em>FUNCTCLASSS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FUNCTCLASSS</em>' containment reference.
	 * @see #getFUNCTCLASSS()
	 * @generated
	 */
	void setFUNCTCLASSS(FUNCTCLASSS value);

	/**
	 * Returns the value of the '<em><b>DIAGDATADICTIONARYSPEC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGDATADICTIONARYSPEC</em>' containment reference.
	 * @see #setDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_DIAGDATADICTIONARYSPEC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-DATA-DICTIONARY-SPEC' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGDATADICTIONARYSPEC getDIAGDATADICTIONARYSPEC();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getDIAGDATADICTIONARYSPEC <em>DIAGDATADICTIONARYSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGDATADICTIONARYSPEC</em>' containment reference.
	 * @see #getDIAGDATADICTIONARYSPEC()
	 * @generated
	 */
	void setDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC value);

	/**
	 * Returns the value of the '<em><b>DIAGCOMMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMS</em>' containment reference.
	 * @see #setDIAGCOMMS(DIAGCOMMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_DIAGCOMMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMMS' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGCOMMS getDIAGCOMMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getDIAGCOMMS <em>DIAGCOMMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCOMMS</em>' containment reference.
	 * @see #getDIAGCOMMS()
	 * @generated
	 */
	void setDIAGCOMMS(DIAGCOMMS value);

	/**
	 * Returns the value of the '<em><b>REQUESTS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REQUESTS</em>' containment reference.
	 * @see #setREQUESTS(REQUESTS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_REQUESTS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='REQUESTS' namespace='##targetNamespace'"
	 * @generated
	 */
	REQUESTS getREQUESTS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getREQUESTS <em>REQUESTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REQUESTS</em>' containment reference.
	 * @see #getREQUESTS()
	 * @generated
	 */
	void setREQUESTS(REQUESTS value);

	/**
	 * Returns the value of the '<em><b>POSRESPONSES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>POSRESPONSES</em>' containment reference.
	 * @see #setPOSRESPONSES(POSRESPONSES)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_POSRESPONSES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='POS-RESPONSES' namespace='##targetNamespace'"
	 * @generated
	 */
	POSRESPONSES getPOSRESPONSES();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getPOSRESPONSES <em>POSRESPONSES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>POSRESPONSES</em>' containment reference.
	 * @see #getPOSRESPONSES()
	 * @generated
	 */
	void setPOSRESPONSES(POSRESPONSES value);

	/**
	 * Returns the value of the '<em><b>NEGRESPONSES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NEGRESPONSES</em>' containment reference.
	 * @see #setNEGRESPONSES(NEGRESPONSES)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_NEGRESPONSES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='NEG-RESPONSES' namespace='##targetNamespace'"
	 * @generated
	 */
	NEGRESPONSES getNEGRESPONSES();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getNEGRESPONSES <em>NEGRESPONSES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NEGRESPONSES</em>' containment reference.
	 * @see #getNEGRESPONSES()
	 * @generated
	 */
	void setNEGRESPONSES(NEGRESPONSES value);

	/**
	 * Returns the value of the '<em><b>GLOBALNEGRESPONSES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>GLOBALNEGRESPONSES</em>' containment reference.
	 * @see #setGLOBALNEGRESPONSES(GLOBALNEGRESPONSES)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_GLOBALNEGRESPONSES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='GLOBAL-NEG-RESPONSES' namespace='##targetNamespace'"
	 * @generated
	 */
	GLOBALNEGRESPONSES getGLOBALNEGRESPONSES();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getGLOBALNEGRESPONSES <em>GLOBALNEGRESPONSES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>GLOBALNEGRESPONSES</em>' containment reference.
	 * @see #getGLOBALNEGRESPONSES()
	 * @generated
	 */
	void setGLOBALNEGRESPONSES(GLOBALNEGRESPONSES value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGLAYER_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGLAYER#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // DIAGLAYER
