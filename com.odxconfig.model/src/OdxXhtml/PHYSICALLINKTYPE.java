/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>PHYSICALLINKTYPE</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALLINKTYPE()
 * @model extendedMetaData="name='PHYSICAL-LINK-TYPE'"
 * @generated
 */
public enum PHYSICALLINKTYPE implements Enumerator {
	/**
	 * The '<em><b>ISO118982DWCAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO118982DWCAN_VALUE
	 * @generated
	 * @ordered
	 */
	ISO118982DWCAN(0, "ISO118982DWCAN", "ISO_11898_2_DWCAN"),

	/**
	 * The '<em><b>ISO118983DWFTCAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO118983DWFTCAN_VALUE
	 * @generated
	 * @ordered
	 */
	ISO118983DWFTCAN(1, "ISO118983DWFTCAN", "ISO_11898_3_DWFTCAN"),

	/**
	 * The '<em><b>ISO119921DWCAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO119921DWCAN_VALUE
	 * @generated
	 * @ordered
	 */
	ISO119921DWCAN(2, "ISO119921DWCAN", "ISO_11992_1_DWCAN"),

	/**
	 * The '<em><b>ISO91412UART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO91412UART_VALUE
	 * @generated
	 * @ordered
	 */
	ISO91412UART(3, "ISO91412UART", "ISO_9141_2_UART"),

	/**
	 * The '<em><b>ISO142301UART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO142301UART_VALUE
	 * @generated
	 * @ordered
	 */
	ISO142301UART(4, "ISO142301UART", "ISO_14230_1_UART"),

	/**
	 * The '<em><b>ISO11898RAW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO11898RAW_VALUE
	 * @generated
	 * @ordered
	 */
	ISO11898RAW(5, "ISO11898RAW", "ISO_11898_RAW"),

	/**
	 * The '<em><b>SAEJ1850VPW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ1850VPW_VALUE
	 * @generated
	 * @ordered
	 */
	SAEJ1850VPW(6, "SAEJ1850VPW", "SAE_J1850_VPW"),

	/**
	 * The '<em><b>SAEJ1850PWM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ1850PWM_VALUE
	 * @generated
	 * @ordered
	 */
	SAEJ1850PWM(7, "SAEJ1850PWM", "SAE_J1850_PWM"),

	/**
	 * The '<em><b>SAEJ2610UART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ2610UART_VALUE
	 * @generated
	 * @ordered
	 */
	SAEJ2610UART(8, "SAEJ2610UART", "SAE_J2610_UART"),

	/**
	 * The '<em><b>SAEJ1708UART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ1708UART_VALUE
	 * @generated
	 * @ordered
	 */
	SAEJ1708UART(9, "SAEJ1708UART", "SAE_J1708_UART"),

	/**
	 * The '<em><b>SAEJ193911DWCAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ193911DWCAN_VALUE
	 * @generated
	 * @ordered
	 */
	SAEJ193911DWCAN(10, "SAEJ193911DWCAN", "SAE_J1939_11_DWCAN"),

	/**
	 * The '<em><b>GMW3089SWCAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GMW3089SWCAN_VALUE
	 * @generated
	 * @ordered
	 */
	GMW3089SWCAN(11, "GMW3089SWCAN", "GMW_3089_SWCAN"),

	/**
	 * The '<em><b>XDE5024UART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XDE5024UART_VALUE
	 * @generated
	 * @ordered
	 */
	XDE5024UART(12, "XDE5024UART", "XDE_5024_UART"),

	/**
	 * The '<em><b>CCDUART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CCDUART_VALUE
	 * @generated
	 * @ordered
	 */
	CCDUART(13, "CCDUART", "CCD_UART");

	/**
	 * The '<em><b>ISO118982DWCAN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO118982DWCAN
	 * @model literal="ISO_11898_2_DWCAN"
	 * @generated
	 * @ordered
	 */
	public static final int ISO118982DWCAN_VALUE = 0;

	/**
	 * The '<em><b>ISO118983DWFTCAN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO118983DWFTCAN
	 * @model literal="ISO_11898_3_DWFTCAN"
	 * @generated
	 * @ordered
	 */
	public static final int ISO118983DWFTCAN_VALUE = 1;

	/**
	 * The '<em><b>ISO119921DWCAN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO119921DWCAN
	 * @model literal="ISO_11992_1_DWCAN"
	 * @generated
	 * @ordered
	 */
	public static final int ISO119921DWCAN_VALUE = 2;

	/**
	 * The '<em><b>ISO91412UART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO91412UART
	 * @model literal="ISO_9141_2_UART"
	 * @generated
	 * @ordered
	 */
	public static final int ISO91412UART_VALUE = 3;

	/**
	 * The '<em><b>ISO142301UART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO142301UART
	 * @model literal="ISO_14230_1_UART"
	 * @generated
	 * @ordered
	 */
	public static final int ISO142301UART_VALUE = 4;

	/**
	 * The '<em><b>ISO11898RAW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO11898RAW
	 * @model literal="ISO_11898_RAW"
	 * @generated
	 * @ordered
	 */
	public static final int ISO11898RAW_VALUE = 5;

	/**
	 * The '<em><b>SAEJ1850VPW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ1850VPW
	 * @model literal="SAE_J1850_VPW"
	 * @generated
	 * @ordered
	 */
	public static final int SAEJ1850VPW_VALUE = 6;

	/**
	 * The '<em><b>SAEJ1850PWM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ1850PWM
	 * @model literal="SAE_J1850_PWM"
	 * @generated
	 * @ordered
	 */
	public static final int SAEJ1850PWM_VALUE = 7;

	/**
	 * The '<em><b>SAEJ2610UART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ2610UART
	 * @model literal="SAE_J2610_UART"
	 * @generated
	 * @ordered
	 */
	public static final int SAEJ2610UART_VALUE = 8;

	/**
	 * The '<em><b>SAEJ1708UART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ1708UART
	 * @model literal="SAE_J1708_UART"
	 * @generated
	 * @ordered
	 */
	public static final int SAEJ1708UART_VALUE = 9;

	/**
	 * The '<em><b>SAEJ193911DWCAN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAEJ193911DWCAN
	 * @model literal="SAE_J1939_11_DWCAN"
	 * @generated
	 * @ordered
	 */
	public static final int SAEJ193911DWCAN_VALUE = 10;

	/**
	 * The '<em><b>GMW3089SWCAN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GMW3089SWCAN
	 * @model literal="GMW_3089_SWCAN"
	 * @generated
	 * @ordered
	 */
	public static final int GMW3089SWCAN_VALUE = 11;

	/**
	 * The '<em><b>XDE5024UART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XDE5024UART
	 * @model literal="XDE_5024_UART"
	 * @generated
	 * @ordered
	 */
	public static final int XDE5024UART_VALUE = 12;

	/**
	 * The '<em><b>CCDUART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CCDUART
	 * @model literal="CCD_UART"
	 * @generated
	 * @ordered
	 */
	public static final int CCDUART_VALUE = 13;

	/**
	 * An array of all the '<em><b>PHYSICALLINKTYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final PHYSICALLINKTYPE[] VALUES_ARRAY =
		new PHYSICALLINKTYPE[] {
			ISO118982DWCAN,
			ISO118983DWFTCAN,
			ISO119921DWCAN,
			ISO91412UART,
			ISO142301UART,
			ISO11898RAW,
			SAEJ1850VPW,
			SAEJ1850PWM,
			SAEJ2610UART,
			SAEJ1708UART,
			SAEJ193911DWCAN,
			GMW3089SWCAN,
			XDE5024UART,
			CCDUART,
		};

	/**
	 * A public read-only list of all the '<em><b>PHYSICALLINKTYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<PHYSICALLINKTYPE> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>PHYSICALLINKTYPE</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static PHYSICALLINKTYPE get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			PHYSICALLINKTYPE result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>PHYSICALLINKTYPE</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static PHYSICALLINKTYPE getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			PHYSICALLINKTYPE result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>PHYSICALLINKTYPE</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static PHYSICALLINKTYPE get(int value) {
		switch (value) {
			case ISO118982DWCAN_VALUE: return ISO118982DWCAN;
			case ISO118983DWFTCAN_VALUE: return ISO118983DWFTCAN;
			case ISO119921DWCAN_VALUE: return ISO119921DWCAN;
			case ISO91412UART_VALUE: return ISO91412UART;
			case ISO142301UART_VALUE: return ISO142301UART;
			case ISO11898RAW_VALUE: return ISO11898RAW;
			case SAEJ1850VPW_VALUE: return SAEJ1850VPW;
			case SAEJ1850PWM_VALUE: return SAEJ1850PWM;
			case SAEJ2610UART_VALUE: return SAEJ2610UART;
			case SAEJ1708UART_VALUE: return SAEJ1708UART;
			case SAEJ193911DWCAN_VALUE: return SAEJ193911DWCAN;
			case GMW3089SWCAN_VALUE: return GMW3089SWCAN;
			case XDE5024UART_VALUE: return XDE5024UART;
			case CCDUART_VALUE: return CCDUART;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private PHYSICALLINKTYPE(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //PHYSICALLINKTYPE
