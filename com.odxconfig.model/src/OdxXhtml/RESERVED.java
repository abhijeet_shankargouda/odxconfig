/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RESERVED</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.RESERVED#getBITLENGTH <em>BITLENGTH</em>}</li>
 *   <li>{@link OdxXhtml.RESERVED#getCODEDVALUE <em>CODEDVALUE</em>}</li>
 *   <li>{@link OdxXhtml.RESERVED#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getRESERVED()
 * @model extendedMetaData="name='RESERVED' kind='elementOnly'"
 * @generated
 */
public interface RESERVED extends POSITIONABLEPARAM {
	/**
	 * Returns the value of the '<em><b>BITLENGTH</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BITLENGTH</em>' attribute.
	 * @see #isSetBITLENGTH()
	 * @see #unsetBITLENGTH()
	 * @see #setBITLENGTH(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getRESERVED_BITLENGTH()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='BIT-LENGTH' namespace='##targetNamespace'"
	 * @generated
	 */
	long getBITLENGTH();

	/**
	 * Sets the value of the '{@link OdxXhtml.RESERVED#getBITLENGTH <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BITLENGTH</em>' attribute.
	 * @see #isSetBITLENGTH()
	 * @see #unsetBITLENGTH()
	 * @see #getBITLENGTH()
	 * @generated
	 */
	void setBITLENGTH(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.RESERVED#getBITLENGTH <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBITLENGTH()
	 * @see #getBITLENGTH()
	 * @see #setBITLENGTH(long)
	 * @generated
	 */
	void unsetBITLENGTH();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.RESERVED#getBITLENGTH <em>BITLENGTH</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BITLENGTH</em>' attribute is set.
	 * @see #unsetBITLENGTH()
	 * @see #getBITLENGTH()
	 * @see #setBITLENGTH(long)
	 * @generated
	 */
	boolean isSetBITLENGTH();

	/**
	 * Returns the value of the '<em><b>CODEDVALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CODEDVALUE</em>' attribute.
	 * @see #setCODEDVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getRESERVED_CODEDVALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='CODED-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCODEDVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.RESERVED#getCODEDVALUE <em>CODEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CODEDVALUE</em>' attribute.
	 * @see #getCODEDVALUE()
	 * @generated
	 */
	void setCODEDVALUE(String value);

	/**
	 * Returns the value of the '<em><b>DIAGCODEDTYPE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCODEDTYPE</em>' containment reference.
	 * @see #setDIAGCODEDTYPE(DIAGCODEDTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getRESERVED_DIAGCODEDTYPE()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-CODED-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGCODEDTYPE getDIAGCODEDTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.RESERVED#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCODEDTYPE</em>' containment reference.
	 * @see #getDIAGCODEDTYPE()
	 * @generated
	 */
	void setDIAGCODEDTYPE(DIAGCODEDTYPE value);

} // RESERVED
