/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPUCONST</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPUCONST#getV <em>V</em>}</li>
 *   <li>{@link OdxXhtml.COMPUCONST#getVT <em>VT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUCONST()
 * @model extendedMetaData="name='COMPU-CONST' kind='elementOnly'"
 * @generated
 */
public interface COMPUCONST extends EObject {
	/**
	 * Returns the value of the '<em><b>V</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>V</em>' containment reference.
	 * @see #setV(V)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUCONST_V()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='V' namespace='##targetNamespace'"
	 * @generated
	 */
	V getV();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUCONST#getV <em>V</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>V</em>' containment reference.
	 * @see #getV()
	 * @generated
	 */
	void setV(V value);

	/**
	 * Returns the value of the '<em><b>VT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VT</em>' containment reference.
	 * @see #setVT(VT)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUCONST_VT()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='VT' namespace='##targetNamespace'"
	 * @generated
	 */
	VT getVT();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUCONST#getVT <em>VT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VT</em>' containment reference.
	 * @see #getVT()
	 * @generated
	 */
	void setVT(VT value);

} // COMPUCONST
