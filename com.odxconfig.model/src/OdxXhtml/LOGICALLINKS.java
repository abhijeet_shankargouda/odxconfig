/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LOGICALLINKS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.LOGICALLINKS#getLOGICALLINK <em>LOGICALLINK</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINKS()
 * @model extendedMetaData="name='LOGICAL-LINKS' kind='elementOnly'"
 * @generated
 */
public interface LOGICALLINKS extends EObject {
	/**
	 * Returns the value of the '<em><b>LOGICALLINK</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.LOGICALLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LOGICALLINK</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINKS_LOGICALLINK()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='LOGICAL-LINK' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<LOGICALLINK> getLOGICALLINK();

} // LOGICALLINKS
