/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROTOCOL</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PROTOCOL#getCOMPARAMSPECREF <em>COMPARAMSPECREF</em>}</li>
 *   <li>{@link OdxXhtml.PROTOCOL#getACCESSLEVELS <em>ACCESSLEVELS</em>}</li>
 *   <li>{@link OdxXhtml.PROTOCOL#getAUTMETHODS <em>AUTMETHODS</em>}</li>
 *   <li>{@link OdxXhtml.PROTOCOL#getPARENTREFS <em>PARENTREFS</em>}</li>
 *   <li>{@link OdxXhtml.PROTOCOL#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOL()
 * @model extendedMetaData="name='PROTOCOL' kind='elementOnly'"
 * @generated
 */
public interface PROTOCOL extends HIERARCHYELEMENT {
	/**
	 * Returns the value of the '<em><b>COMPARAMSPECREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPARAMSPECREF</em>' containment reference.
	 * @see #setCOMPARAMSPECREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOL_COMPARAMSPECREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPARAM-SPEC-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getCOMPARAMSPECREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROTOCOL#getCOMPARAMSPECREF <em>COMPARAMSPECREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPARAMSPECREF</em>' containment reference.
	 * @see #getCOMPARAMSPECREF()
	 * @generated
	 */
	void setCOMPARAMSPECREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>ACCESSLEVELS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ACCESSLEVELS</em>' containment reference.
	 * @see #setACCESSLEVELS(ACCESSLEVELS)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOL_ACCESSLEVELS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ACCESS-LEVELS' namespace='##targetNamespace'"
	 * @generated
	 */
	ACCESSLEVELS getACCESSLEVELS();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROTOCOL#getACCESSLEVELS <em>ACCESSLEVELS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ACCESSLEVELS</em>' containment reference.
	 * @see #getACCESSLEVELS()
	 * @generated
	 */
	void setACCESSLEVELS(ACCESSLEVELS value);

	/**
	 * Returns the value of the '<em><b>AUTMETHODS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUTMETHODS</em>' containment reference.
	 * @see #setAUTMETHODS(AUTMETHODS)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOL_AUTMETHODS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AUT-METHODS' namespace='##targetNamespace'"
	 * @generated
	 */
	AUTMETHODS getAUTMETHODS();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROTOCOL#getAUTMETHODS <em>AUTMETHODS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AUTMETHODS</em>' containment reference.
	 * @see #getAUTMETHODS()
	 * @generated
	 */
	void setAUTMETHODS(AUTMETHODS value);

	/**
	 * Returns the value of the '<em><b>PARENTREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PARENTREFS</em>' containment reference.
	 * @see #setPARENTREFS(PARENTREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOL_PARENTREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PARENT-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	PARENTREFS getPARENTREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROTOCOL#getPARENTREFS <em>PARENTREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PARENTREFS</em>' containment reference.
	 * @see #getPARENTREFS()
	 * @generated
	 */
	void setPARENTREFS(PARENTREFS value);

	/**
	 * Returns the value of the '<em><b>TYPE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TYPE</em>' attribute.
	 * @see #setTYPE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROTOCOL_TYPE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROTOCOL#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TYPE</em>' attribute.
	 * @see #getTYPE()
	 * @generated
	 */
	void setTYPE(String value);

} // PROTOCOL
