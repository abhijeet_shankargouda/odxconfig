/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLEMODEL</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEMODEL()
 * @model extendedMetaData="name='VEHICLE-MODEL' kind='elementOnly'"
 * @generated
 */
public interface VEHICLEMODEL extends INFOCOMPONENT {
} // VEHICLEMODEL
