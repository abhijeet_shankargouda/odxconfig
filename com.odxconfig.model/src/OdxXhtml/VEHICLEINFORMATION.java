/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLEINFORMATION</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATION#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATION#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATION#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATION#getINFOCOMPONENTREFS <em>INFOCOMPONENTREFS</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATION#getVEHICLECONNECTORS <em>VEHICLECONNECTORS</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATION#getLOGICALLINKS <em>LOGICALLINKS</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATION#getECUGROUPS <em>ECUGROUPS</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATION#getPHYSICALVEHICLELINKS <em>PHYSICALVEHICLELINKS</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATION#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION()
 * @model extendedMetaData="name='VEHICLE-INFORMATION' kind='elementOnly'"
 * @generated
 */
public interface VEHICLEINFORMATION extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFORMATION#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFORMATION#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFORMATION#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>INFOCOMPONENTREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INFOCOMPONENTREFS</em>' containment reference.
	 * @see #setINFOCOMPONENTREFS(INFOCOMPONENTREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION_INFOCOMPONENTREFS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='INFO-COMPONENT-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	INFOCOMPONENTREFS getINFOCOMPONENTREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFORMATION#getINFOCOMPONENTREFS <em>INFOCOMPONENTREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>INFOCOMPONENTREFS</em>' containment reference.
	 * @see #getINFOCOMPONENTREFS()
	 * @generated
	 */
	void setINFOCOMPONENTREFS(INFOCOMPONENTREFS value);

	/**
	 * Returns the value of the '<em><b>VEHICLECONNECTORS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VEHICLECONNECTORS</em>' containment reference.
	 * @see #setVEHICLECONNECTORS(VEHICLECONNECTORS)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION_VEHICLECONNECTORS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='VEHICLE-CONNECTORS' namespace='##targetNamespace'"
	 * @generated
	 */
	VEHICLECONNECTORS getVEHICLECONNECTORS();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFORMATION#getVEHICLECONNECTORS <em>VEHICLECONNECTORS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VEHICLECONNECTORS</em>' containment reference.
	 * @see #getVEHICLECONNECTORS()
	 * @generated
	 */
	void setVEHICLECONNECTORS(VEHICLECONNECTORS value);

	/**
	 * Returns the value of the '<em><b>LOGICALLINKS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LOGICALLINKS</em>' containment reference.
	 * @see #setLOGICALLINKS(LOGICALLINKS)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION_LOGICALLINKS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LOGICAL-LINKS' namespace='##targetNamespace'"
	 * @generated
	 */
	LOGICALLINKS getLOGICALLINKS();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFORMATION#getLOGICALLINKS <em>LOGICALLINKS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LOGICALLINKS</em>' containment reference.
	 * @see #getLOGICALLINKS()
	 * @generated
	 */
	void setLOGICALLINKS(LOGICALLINKS value);

	/**
	 * Returns the value of the '<em><b>ECUGROUPS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUGROUPS</em>' containment reference.
	 * @see #setECUGROUPS(ECUGROUPS)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION_ECUGROUPS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ECU-GROUPS' namespace='##targetNamespace'"
	 * @generated
	 */
	ECUGROUPS getECUGROUPS();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFORMATION#getECUGROUPS <em>ECUGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ECUGROUPS</em>' containment reference.
	 * @see #getECUGROUPS()
	 * @generated
	 */
	void setECUGROUPS(ECUGROUPS value);

	/**
	 * Returns the value of the '<em><b>PHYSICALVEHICLELINKS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALVEHICLELINKS</em>' containment reference.
	 * @see #setPHYSICALVEHICLELINKS(PHYSICALVEHICLELINKS)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION_PHYSICALVEHICLELINKS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PHYSICAL-VEHICLE-LINKS' namespace='##targetNamespace'"
	 * @generated
	 */
	PHYSICALVEHICLELINKS getPHYSICALVEHICLELINKS();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFORMATION#getPHYSICALVEHICLELINKS <em>PHYSICALVEHICLELINKS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSICALVEHICLELINKS</em>' containment reference.
	 * @see #getPHYSICALVEHICLELINKS()
	 * @generated
	 */
	void setPHYSICALVEHICLELINKS(PHYSICALVEHICLELINKS value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATION_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFORMATION#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // VEHICLEINFORMATION
