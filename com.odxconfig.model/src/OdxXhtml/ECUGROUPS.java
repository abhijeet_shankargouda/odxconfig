/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUGROUPS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUGROUPS#getECUGROUP <em>ECUGROUP</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUGROUPS()
 * @model extendedMetaData="name='ECU-GROUPS' kind='elementOnly'"
 * @generated
 */
public interface ECUGROUPS extends EObject {
	/**
	 * Returns the value of the '<em><b>ECUGROUP</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ECUGROUP}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUGROUP</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getECUGROUPS_ECUGROUP()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ECU-GROUP' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ECUGROUP> getECUGROUP();

} // ECUGROUPS
