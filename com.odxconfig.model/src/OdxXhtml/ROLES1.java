/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ROLES1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ROLES1#getROLE <em>ROLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getROLES1()
 * @model extendedMetaData="name='ROLES' kind='elementOnly'"
 * @generated
 */
public interface ROLES1 extends EObject {
	/**
	 * Returns the value of the '<em><b>ROLE</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ROLE</em>' attribute list.
	 * @see OdxXhtml.OdxXhtmlPackage#getROLES1_ROLE()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='ROLE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<String> getROLE();

} // ROLES1
