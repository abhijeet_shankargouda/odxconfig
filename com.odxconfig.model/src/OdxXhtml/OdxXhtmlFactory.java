/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage
 * @generated
 */
public interface OdxXhtmlFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OdxXhtmlFactory eINSTANCE = OdxXhtml.impl.OdxXhtmlFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>ABLOCK</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ABLOCK</em>'.
	 * @generated
	 */
	ABLOCK createABLOCK();

	/**
	 * Returns a new object of class '<em>ABLOCKS Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ABLOCKS Type</em>'.
	 * @generated
	 */
	ABLOCKSType createABLOCKSType();

	/**
	 * Returns a new object of class '<em>ACCESSLEVEL</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ACCESSLEVEL</em>'.
	 * @generated
	 */
	ACCESSLEVEL createACCESSLEVEL();

	/**
	 * Returns a new object of class '<em>ACCESSLEVELS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ACCESSLEVELS</em>'.
	 * @generated
	 */
	ACCESSLEVELS createACCESSLEVELS();

	/**
	 * Returns a new object of class '<em>ADDRDEFFILTER</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ADDRDEFFILTER</em>'.
	 * @generated
	 */
	ADDRDEFFILTER createADDRDEFFILTER();

	/**
	 * Returns a new object of class '<em>ADDRDEFPHYSSEGMENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ADDRDEFPHYSSEGMENT</em>'.
	 * @generated
	 */
	ADDRDEFPHYSSEGMENT createADDRDEFPHYSSEGMENT();

	/**
	 * Returns a new object of class '<em>ADMINDATA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ADMINDATA</em>'.
	 * @generated
	 */
	ADMINDATA createADMINDATA();

	/**
	 * Returns a new object of class '<em>ADMINDATA1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ADMINDATA1</em>'.
	 * @generated
	 */
	ADMINDATA1 createADMINDATA1();

	/**
	 * Returns a new object of class '<em>ALLVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ALLVALUE</em>'.
	 * @generated
	 */
	ALLVALUE createALLVALUE();

	/**
	 * Returns a new object of class '<em>AUDIENCE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AUDIENCE</em>'.
	 * @generated
	 */
	AUDIENCE createAUDIENCE();

	/**
	 * Returns a new object of class '<em>AUTMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AUTMETHOD</em>'.
	 * @generated
	 */
	AUTMETHOD createAUTMETHOD();

	/**
	 * Returns a new object of class '<em>AUTMETHODS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AUTMETHODS</em>'.
	 * @generated
	 */
	AUTMETHODS createAUTMETHODS();

	/**
	 * Returns a new object of class '<em>BASEVARIANT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BASEVARIANT</em>'.
	 * @generated
	 */
	BASEVARIANT createBASEVARIANT();

	/**
	 * Returns a new object of class '<em>BASEVARIANTREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BASEVARIANTREF</em>'.
	 * @generated
	 */
	BASEVARIANTREF createBASEVARIANTREF();

	/**
	 * Returns a new object of class '<em>BASEVARIANTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BASEVARIANTS</em>'.
	 * @generated
	 */
	BASEVARIANTS createBASEVARIANTS();

	/**
	 * Returns a new object of class '<em>BASICSTRUCTURE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BASICSTRUCTURE</em>'.
	 * @generated
	 */
	BASICSTRUCTURE createBASICSTRUCTURE();

	/**
	 * Returns a new object of class '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Block</em>'.
	 * @generated
	 */
	Block createBlock();

	/**
	 * Returns a new object of class '<em>BType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BType</em>'.
	 * @generated
	 */
	BType createBType();

	/**
	 * Returns a new object of class '<em>CASE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CASE</em>'.
	 * @generated
	 */
	CASE createCASE();

	/**
	 * Returns a new object of class '<em>CASES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CASES</em>'.
	 * @generated
	 */
	CASES createCASES();

	/**
	 * Returns a new object of class '<em>CATALOG</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CATALOG</em>'.
	 * @generated
	 */
	CATALOG createCATALOG();

	/**
	 * Returns a new object of class '<em>CHECKSUM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CHECKSUM</em>'.
	 * @generated
	 */
	CHECKSUM createCHECKSUM();

	/**
	 * Returns a new object of class '<em>CHECKSUMRESULT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CHECKSUMRESULT</em>'.
	 * @generated
	 */
	CHECKSUMRESULT createCHECKSUMRESULT();

	/**
	 * Returns a new object of class '<em>CHECKSUMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CHECKSUMS</em>'.
	 * @generated
	 */
	CHECKSUMS createCHECKSUMS();

	/**
	 * Returns a new object of class '<em>CODEDCONST</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CODEDCONST</em>'.
	 * @generated
	 */
	CODEDCONST createCODEDCONST();

	/**
	 * Returns a new object of class '<em>COMMRELATION</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMMRELATION</em>'.
	 * @generated
	 */
	COMMRELATION createCOMMRELATION();

	/**
	 * Returns a new object of class '<em>COMMRELATIONS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMMRELATIONS</em>'.
	 * @generated
	 */
	COMMRELATIONS createCOMMRELATIONS();

	/**
	 * Returns a new object of class '<em>COMPANYDATA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYDATA</em>'.
	 * @generated
	 */
	COMPANYDATA createCOMPANYDATA();

	/**
	 * Returns a new object of class '<em>COMPANYDATA1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYDATA1</em>'.
	 * @generated
	 */
	COMPANYDATA1 createCOMPANYDATA1();

	/**
	 * Returns a new object of class '<em>COMPANYDATAS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYDATAS</em>'.
	 * @generated
	 */
	COMPANYDATAS createCOMPANYDATAS();

	/**
	 * Returns a new object of class '<em>COMPANYDATAS Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYDATAS Type</em>'.
	 * @generated
	 */
	COMPANYDATASType createCOMPANYDATASType();

	/**
	 * Returns a new object of class '<em>COMPANYDOCINFO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYDOCINFO</em>'.
	 * @generated
	 */
	COMPANYDOCINFO createCOMPANYDOCINFO();

	/**
	 * Returns a new object of class '<em>COMPANYDOCINFO1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYDOCINFO1</em>'.
	 * @generated
	 */
	COMPANYDOCINFO1 createCOMPANYDOCINFO1();

	/**
	 * Returns a new object of class '<em>COMPANYDOCINFOS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYDOCINFOS</em>'.
	 * @generated
	 */
	COMPANYDOCINFOS createCOMPANYDOCINFOS();

	/**
	 * Returns a new object of class '<em>COMPANYDOCINFOS1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYDOCINFOS1</em>'.
	 * @generated
	 */
	COMPANYDOCINFOS1 createCOMPANYDOCINFOS1();

	/**
	 * Returns a new object of class '<em>COMPANYREVISIONINFO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYREVISIONINFO</em>'.
	 * @generated
	 */
	COMPANYREVISIONINFO createCOMPANYREVISIONINFO();

	/**
	 * Returns a new object of class '<em>COMPANYREVISIONINFO1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYREVISIONINFO1</em>'.
	 * @generated
	 */
	COMPANYREVISIONINFO1 createCOMPANYREVISIONINFO1();

	/**
	 * Returns a new object of class '<em>COMPANYREVISIONINFOS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYREVISIONINFOS</em>'.
	 * @generated
	 */
	COMPANYREVISIONINFOS createCOMPANYREVISIONINFOS();

	/**
	 * Returns a new object of class '<em>COMPANYREVISIONINFOS1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYREVISIONINFOS1</em>'.
	 * @generated
	 */
	COMPANYREVISIONINFOS1 createCOMPANYREVISIONINFOS1();

	/**
	 * Returns a new object of class '<em>COMPANYSPECIFICINFO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYSPECIFICINFO</em>'.
	 * @generated
	 */
	COMPANYSPECIFICINFO createCOMPANYSPECIFICINFO();

	/**
	 * Returns a new object of class '<em>COMPANYSPECIFICINFO1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPANYSPECIFICINFO1</em>'.
	 * @generated
	 */
	COMPANYSPECIFICINFO1 createCOMPANYSPECIFICINFO1();

	/**
	 * Returns a new object of class '<em>COMPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPARAM</em>'.
	 * @generated
	 */
	COMPARAM createCOMPARAM();

	/**
	 * Returns a new object of class '<em>COMPARAMREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPARAMREF</em>'.
	 * @generated
	 */
	COMPARAMREF createCOMPARAMREF();

	/**
	 * Returns a new object of class '<em>COMPARAMREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPARAMREFS</em>'.
	 * @generated
	 */
	COMPARAMREFS createCOMPARAMREFS();

	/**
	 * Returns a new object of class '<em>COMPARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPARAMS</em>'.
	 * @generated
	 */
	COMPARAMS createCOMPARAMS();

	/**
	 * Returns a new object of class '<em>COMPARAMSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPARAMSPEC</em>'.
	 * @generated
	 */
	COMPARAMSPEC createCOMPARAMSPEC();

	/**
	 * Returns a new object of class '<em>COMPLEXDOP</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPLEXDOP</em>'.
	 * @generated
	 */
	COMPLEXDOP createCOMPLEXDOP();

	/**
	 * Returns a new object of class '<em>COMPUCONST</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUCONST</em>'.
	 * @generated
	 */
	COMPUCONST createCOMPUCONST();

	/**
	 * Returns a new object of class '<em>COMPUDEFAULTVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUDEFAULTVALUE</em>'.
	 * @generated
	 */
	COMPUDEFAULTVALUE createCOMPUDEFAULTVALUE();

	/**
	 * Returns a new object of class '<em>COMPUDENOMINATOR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUDENOMINATOR</em>'.
	 * @generated
	 */
	COMPUDENOMINATOR createCOMPUDENOMINATOR();

	/**
	 * Returns a new object of class '<em>COMPUINTERNALTOPHYS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUINTERNALTOPHYS</em>'.
	 * @generated
	 */
	COMPUINTERNALTOPHYS createCOMPUINTERNALTOPHYS();

	/**
	 * Returns a new object of class '<em>COMPUINVERSEVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUINVERSEVALUE</em>'.
	 * @generated
	 */
	COMPUINVERSEVALUE createCOMPUINVERSEVALUE();

	/**
	 * Returns a new object of class '<em>COMPUMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUMETHOD</em>'.
	 * @generated
	 */
	COMPUMETHOD createCOMPUMETHOD();

	/**
	 * Returns a new object of class '<em>COMPUNUMERATOR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUNUMERATOR</em>'.
	 * @generated
	 */
	COMPUNUMERATOR createCOMPUNUMERATOR();

	/**
	 * Returns a new object of class '<em>COMPUPHYSTOINTERNAL</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUPHYSTOINTERNAL</em>'.
	 * @generated
	 */
	COMPUPHYSTOINTERNAL createCOMPUPHYSTOINTERNAL();

	/**
	 * Returns a new object of class '<em>COMPURATIONALCOEFFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPURATIONALCOEFFS</em>'.
	 * @generated
	 */
	COMPURATIONALCOEFFS createCOMPURATIONALCOEFFS();

	/**
	 * Returns a new object of class '<em>COMPUSCALE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUSCALE</em>'.
	 * @generated
	 */
	COMPUSCALE createCOMPUSCALE();

	/**
	 * Returns a new object of class '<em>COMPUSCALES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>COMPUSCALES</em>'.
	 * @generated
	 */
	COMPUSCALES createCOMPUSCALES();

	/**
	 * Returns a new object of class '<em>DATABLOCK</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DATABLOCK</em>'.
	 * @generated
	 */
	DATABLOCK createDATABLOCK();

	/**
	 * Returns a new object of class '<em>DATABLOCKREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DATABLOCKREFS</em>'.
	 * @generated
	 */
	DATABLOCKREFS createDATABLOCKREFS();

	/**
	 * Returns a new object of class '<em>DATABLOCKS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DATABLOCKS</em>'.
	 * @generated
	 */
	DATABLOCKS createDATABLOCKS();

	/**
	 * Returns a new object of class '<em>DATAFILE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DATAFILE</em>'.
	 * @generated
	 */
	DATAFILE createDATAFILE();

	/**
	 * Returns a new object of class '<em>DATAFORMAT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DATAFORMAT</em>'.
	 * @generated
	 */
	DATAFORMAT createDATAFORMAT();

	/**
	 * Returns a new object of class '<em>DATAOBJECTPROP</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DATAOBJECTPROP</em>'.
	 * @generated
	 */
	DATAOBJECTPROP createDATAOBJECTPROP();

	/**
	 * Returns a new object of class '<em>DATAOBJECTPROPS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DATAOBJECTPROPS</em>'.
	 * @generated
	 */
	DATAOBJECTPROPS createDATAOBJECTPROPS();

	/**
	 * Returns a new object of class '<em>DEFAULTCASE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DEFAULTCASE</em>'.
	 * @generated
	 */
	DEFAULTCASE createDEFAULTCASE();

	/**
	 * Returns a new object of class '<em>DESCRIPTION</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DESCRIPTION</em>'.
	 * @generated
	 */
	DESCRIPTION createDESCRIPTION();

	/**
	 * Returns a new object of class '<em>DESCRIPTION1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DESCRIPTION1</em>'.
	 * @generated
	 */
	DESCRIPTION1 createDESCRIPTION1();

	/**
	 * Returns a new object of class '<em>DETERMINENUMBEROFITEMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DETERMINENUMBEROFITEMS</em>'.
	 * @generated
	 */
	DETERMINENUMBEROFITEMS createDETERMINENUMBEROFITEMS();

	/**
	 * Returns a new object of class '<em>DIAGCODEDTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGCODEDTYPE</em>'.
	 * @generated
	 */
	DIAGCODEDTYPE createDIAGCODEDTYPE();

	/**
	 * Returns a new object of class '<em>DIAGCOMM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGCOMM</em>'.
	 * @generated
	 */
	DIAGCOMM createDIAGCOMM();

	/**
	 * Returns a new object of class '<em>DIAGCOMMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGCOMMS</em>'.
	 * @generated
	 */
	DIAGCOMMS createDIAGCOMMS();

	/**
	 * Returns a new object of class '<em>DIAGDATADICTIONARYSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGDATADICTIONARYSPEC</em>'.
	 * @generated
	 */
	DIAGDATADICTIONARYSPEC createDIAGDATADICTIONARYSPEC();

	/**
	 * Returns a new object of class '<em>DIAGLAYER</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGLAYER</em>'.
	 * @generated
	 */
	DIAGLAYER createDIAGLAYER();

	/**
	 * Returns a new object of class '<em>DIAGLAYERCONTAINER</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGLAYERCONTAINER</em>'.
	 * @generated
	 */
	DIAGLAYERCONTAINER createDIAGLAYERCONTAINER();

	/**
	 * Returns a new object of class '<em>DIAGLAYERREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGLAYERREFS</em>'.
	 * @generated
	 */
	DIAGLAYERREFS createDIAGLAYERREFS();

	/**
	 * Returns a new object of class '<em>DIAGSERVICE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGSERVICE</em>'.
	 * @generated
	 */
	DIAGSERVICE createDIAGSERVICE();

	/**
	 * Returns a new object of class '<em>DIAGVARIABLE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGVARIABLE</em>'.
	 * @generated
	 */
	DIAGVARIABLE createDIAGVARIABLE();

	/**
	 * Returns a new object of class '<em>DIAGVARIABLES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DIAGVARIABLES</em>'.
	 * @generated
	 */
	DIAGVARIABLES createDIAGVARIABLES();

	/**
	 * Returns a new object of class '<em>DOCREVISION</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DOCREVISION</em>'.
	 * @generated
	 */
	DOCREVISION createDOCREVISION();

	/**
	 * Returns a new object of class '<em>DOCREVISION1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DOCREVISION1</em>'.
	 * @generated
	 */
	DOCREVISION1 createDOCREVISION1();

	/**
	 * Returns a new object of class '<em>DOCREVISIONS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DOCREVISIONS</em>'.
	 * @generated
	 */
	DOCREVISIONS createDOCREVISIONS();

	/**
	 * Returns a new object of class '<em>DOCREVISIONS1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DOCREVISIONS1</em>'.
	 * @generated
	 */
	DOCREVISIONS1 createDOCREVISIONS1();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	DocumentRoot createDocumentRoot();

	/**
	 * Returns a new object of class '<em>DOPBASE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DOPBASE</em>'.
	 * @generated
	 */
	DOPBASE createDOPBASE();

	/**
	 * Returns a new object of class '<em>DTC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DTC</em>'.
	 * @generated
	 */
	DTC createDTC();

	/**
	 * Returns a new object of class '<em>DTCDOP</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DTCDOP</em>'.
	 * @generated
	 */
	DTCDOP createDTCDOP();

	/**
	 * Returns a new object of class '<em>DTCDOPS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DTCDOPS</em>'.
	 * @generated
	 */
	DTCDOPS createDTCDOPS();

	/**
	 * Returns a new object of class '<em>DTCS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DTCS</em>'.
	 * @generated
	 */
	DTCS createDTCS();

	/**
	 * Returns a new object of class '<em>DTCVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DTCVALUE</em>'.
	 * @generated
	 */
	DTCVALUE createDTCVALUE();

	/**
	 * Returns a new object of class '<em>DTCVALUES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DTCVALUES</em>'.
	 * @generated
	 */
	DTCVALUES createDTCVALUES();

	/**
	 * Returns a new object of class '<em>DYNAMIC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DYNAMIC</em>'.
	 * @generated
	 */
	DYNAMIC createDYNAMIC();

	/**
	 * Returns a new object of class '<em>DYNAMICENDMARKERFIELD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DYNAMICENDMARKERFIELD</em>'.
	 * @generated
	 */
	DYNAMICENDMARKERFIELD createDYNAMICENDMARKERFIELD();

	/**
	 * Returns a new object of class '<em>DYNAMICENDMARKERFIELDS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DYNAMICENDMARKERFIELDS</em>'.
	 * @generated
	 */
	DYNAMICENDMARKERFIELDS createDYNAMICENDMARKERFIELDS();

	/**
	 * Returns a new object of class '<em>DYNAMICLENGTHFIELD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DYNAMICLENGTHFIELD</em>'.
	 * @generated
	 */
	DYNAMICLENGTHFIELD createDYNAMICLENGTHFIELD();

	/**
	 * Returns a new object of class '<em>DYNAMICLENGTHFIELDS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DYNAMICLENGTHFIELDS</em>'.
	 * @generated
	 */
	DYNAMICLENGTHFIELDS createDYNAMICLENGTHFIELDS();

	/**
	 * Returns a new object of class '<em>DYNDEFINEDSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DYNDEFINEDSPEC</em>'.
	 * @generated
	 */
	DYNDEFINEDSPEC createDYNDEFINEDSPEC();

	/**
	 * Returns a new object of class '<em>DYNENDDOPREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DYNENDDOPREF</em>'.
	 * @generated
	 */
	DYNENDDOPREF createDYNENDDOPREF();

	/**
	 * Returns a new object of class '<em>DYNIDDEFMODEINFO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DYNIDDEFMODEINFO</em>'.
	 * @generated
	 */
	DYNIDDEFMODEINFO createDYNIDDEFMODEINFO();

	/**
	 * Returns a new object of class '<em>DYNIDDEFMODEINFOS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DYNIDDEFMODEINFOS</em>'.
	 * @generated
	 */
	DYNIDDEFMODEINFOS createDYNIDDEFMODEINFOS();

	/**
	 * Returns a new object of class '<em>ECUGROUP</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUGROUP</em>'.
	 * @generated
	 */
	ECUGROUP createECUGROUP();

	/**
	 * Returns a new object of class '<em>ECUGROUPS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUGROUPS</em>'.
	 * @generated
	 */
	ECUGROUPS createECUGROUPS();

	/**
	 * Returns a new object of class '<em>ECUMEM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUMEM</em>'.
	 * @generated
	 */
	ECUMEM createECUMEM();

	/**
	 * Returns a new object of class '<em>ECUMEMCONNECTOR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUMEMCONNECTOR</em>'.
	 * @generated
	 */
	ECUMEMCONNECTOR createECUMEMCONNECTOR();

	/**
	 * Returns a new object of class '<em>ECUMEMCONNECTORS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUMEMCONNECTORS</em>'.
	 * @generated
	 */
	ECUMEMCONNECTORS createECUMEMCONNECTORS();

	/**
	 * Returns a new object of class '<em>ECUMEMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUMEMS</em>'.
	 * @generated
	 */
	ECUMEMS createECUMEMS();

	/**
	 * Returns a new object of class '<em>ECUPROXY</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUPROXY</em>'.
	 * @generated
	 */
	ECUPROXY createECUPROXY();

	/**
	 * Returns a new object of class '<em>ECUPROXYREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUPROXYREFS</em>'.
	 * @generated
	 */
	ECUPROXYREFS createECUPROXYREFS();

	/**
	 * Returns a new object of class '<em>ECUSHAREDDATA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUSHAREDDATA</em>'.
	 * @generated
	 */
	ECUSHAREDDATA createECUSHAREDDATA();

	/**
	 * Returns a new object of class '<em>ECUSHAREDDATAREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUSHAREDDATAREF</em>'.
	 * @generated
	 */
	ECUSHAREDDATAREF createECUSHAREDDATAREF();

	/**
	 * Returns a new object of class '<em>ECUSHAREDDATAS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUSHAREDDATAS</em>'.
	 * @generated
	 */
	ECUSHAREDDATAS createECUSHAREDDATAS();

	/**
	 * Returns a new object of class '<em>ECUVARIANT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUVARIANT</em>'.
	 * @generated
	 */
	ECUVARIANT createECUVARIANT();

	/**
	 * Returns a new object of class '<em>ECUVARIANTPATTERN</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUVARIANTPATTERN</em>'.
	 * @generated
	 */
	ECUVARIANTPATTERN createECUVARIANTPATTERN();

	/**
	 * Returns a new object of class '<em>ECUVARIANTPATTERNS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUVARIANTPATTERNS</em>'.
	 * @generated
	 */
	ECUVARIANTPATTERNS createECUVARIANTPATTERNS();

	/**
	 * Returns a new object of class '<em>ECUVARIANTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ECUVARIANTS</em>'.
	 * @generated
	 */
	ECUVARIANTS createECUVARIANTS();

	/**
	 * Returns a new object of class '<em>ENCRYPTCOMPRESSMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ENCRYPTCOMPRESSMETHOD</em>'.
	 * @generated
	 */
	ENCRYPTCOMPRESSMETHOD createENCRYPTCOMPRESSMETHOD();

	/**
	 * Returns a new object of class '<em>ENDOFPDUFIELD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ENDOFPDUFIELD</em>'.
	 * @generated
	 */
	ENDOFPDUFIELD createENDOFPDUFIELD();

	/**
	 * Returns a new object of class '<em>ENDOFPDUFIELDS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ENDOFPDUFIELDS</em>'.
	 * @generated
	 */
	ENDOFPDUFIELDS createENDOFPDUFIELDS();

	/**
	 * Returns a new object of class '<em>ENVDATA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ENVDATA</em>'.
	 * @generated
	 */
	ENVDATA createENVDATA();

	/**
	 * Returns a new object of class '<em>ENVDATADESC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ENVDATADESC</em>'.
	 * @generated
	 */
	ENVDATADESC createENVDATADESC();

	/**
	 * Returns a new object of class '<em>ENVDATADESCS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ENVDATADESCS</em>'.
	 * @generated
	 */
	ENVDATADESCS createENVDATADESCS();

	/**
	 * Returns a new object of class '<em>ENVDATAS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ENVDATAS</em>'.
	 * @generated
	 */
	ENVDATAS createENVDATAS();

	/**
	 * Returns a new object of class '<em>EXPECTEDIDENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EXPECTEDIDENT</em>'.
	 * @generated
	 */
	EXPECTEDIDENT createEXPECTEDIDENT();

	/**
	 * Returns a new object of class '<em>EXPECTEDIDENTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EXPECTEDIDENTS</em>'.
	 * @generated
	 */
	EXPECTEDIDENTS createEXPECTEDIDENTS();

	/**
	 * Returns a new object of class '<em>EXTERNALACCESSMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EXTERNALACCESSMETHOD</em>'.
	 * @generated
	 */
	EXTERNALACCESSMETHOD createEXTERNALACCESSMETHOD();

	/**
	 * Returns a new object of class '<em>EXTERNFLASHDATA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EXTERNFLASHDATA</em>'.
	 * @generated
	 */
	EXTERNFLASHDATA createEXTERNFLASHDATA();

	/**
	 * Returns a new object of class '<em>FIELD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FIELD</em>'.
	 * @generated
	 */
	FIELD createFIELD();

	/**
	 * Returns a new object of class '<em>FILE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FILE</em>'.
	 * @generated
	 */
	FILE createFILE();

	/**
	 * Returns a new object of class '<em>FILES Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FILES Type</em>'.
	 * @generated
	 */
	FILESType createFILESType();

	/**
	 * Returns a new object of class '<em>FILTER</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FILTER</em>'.
	 * @generated
	 */
	FILTER createFILTER();

	/**
	 * Returns a new object of class '<em>FILTERS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FILTERS</em>'.
	 * @generated
	 */
	FILTERS createFILTERS();

	/**
	 * Returns a new object of class '<em>FLASH</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FLASH</em>'.
	 * @generated
	 */
	FLASH createFLASH();

	/**
	 * Returns a new object of class '<em>FLASHCLASS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FLASHCLASS</em>'.
	 * @generated
	 */
	FLASHCLASS createFLASHCLASS();

	/**
	 * Returns a new object of class '<em>FLASHCLASSREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FLASHCLASSREFS</em>'.
	 * @generated
	 */
	FLASHCLASSREFS createFLASHCLASSREFS();

	/**
	 * Returns a new object of class '<em>FLASHCLASSS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FLASHCLASSS</em>'.
	 * @generated
	 */
	FLASHCLASSS createFLASHCLASSS();

	/**
	 * Returns a new object of class '<em>FLASHDATA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FLASHDATA</em>'.
	 * @generated
	 */
	FLASHDATA createFLASHDATA();

	/**
	 * Returns a new object of class '<em>FLASHDATAS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FLASHDATAS</em>'.
	 * @generated
	 */
	FLASHDATAS createFLASHDATAS();

	/**
	 * Returns a new object of class '<em>Flow</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Flow</em>'.
	 * @generated
	 */
	Flow createFlow();

	/**
	 * Returns a new object of class '<em>FUNCTCLASS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FUNCTCLASS</em>'.
	 * @generated
	 */
	FUNCTCLASS createFUNCTCLASS();

	/**
	 * Returns a new object of class '<em>FUNCTCLASSREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FUNCTCLASSREFS</em>'.
	 * @generated
	 */
	FUNCTCLASSREFS createFUNCTCLASSREFS();

	/**
	 * Returns a new object of class '<em>FUNCTCLASSS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FUNCTCLASSS</em>'.
	 * @generated
	 */
	FUNCTCLASSS createFUNCTCLASSS();

	/**
	 * Returns a new object of class '<em>FUNCTIONALGROUP</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FUNCTIONALGROUP</em>'.
	 * @generated
	 */
	FUNCTIONALGROUP createFUNCTIONALGROUP();

	/**
	 * Returns a new object of class '<em>FUNCTIONALGROUPREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FUNCTIONALGROUPREF</em>'.
	 * @generated
	 */
	FUNCTIONALGROUPREF createFUNCTIONALGROUPREF();

	/**
	 * Returns a new object of class '<em>FUNCTIONALGROUPS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FUNCTIONALGROUPS</em>'.
	 * @generated
	 */
	FUNCTIONALGROUPS createFUNCTIONALGROUPS();

	/**
	 * Returns a new object of class '<em>FWCHECKSUM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FWCHECKSUM</em>'.
	 * @generated
	 */
	FWCHECKSUM createFWCHECKSUM();

	/**
	 * Returns a new object of class '<em>FWSIGNATURE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FWSIGNATURE</em>'.
	 * @generated
	 */
	FWSIGNATURE createFWSIGNATURE();

	/**
	 * Returns a new object of class '<em>GATEWAYLOGICALLINK</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>GATEWAYLOGICALLINK</em>'.
	 * @generated
	 */
	GATEWAYLOGICALLINK createGATEWAYLOGICALLINK();

	/**
	 * Returns a new object of class '<em>GATEWAYLOGICALLINKREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>GATEWAYLOGICALLINKREFS</em>'.
	 * @generated
	 */
	GATEWAYLOGICALLINKREFS createGATEWAYLOGICALLINKREFS();

	/**
	 * Returns a new object of class '<em>GLOBALNEGRESPONSE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>GLOBALNEGRESPONSE</em>'.
	 * @generated
	 */
	GLOBALNEGRESPONSE createGLOBALNEGRESPONSE();

	/**
	 * Returns a new object of class '<em>GLOBALNEGRESPONSES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>GLOBALNEGRESPONSES</em>'.
	 * @generated
	 */
	GLOBALNEGRESPONSES createGLOBALNEGRESPONSES();

	/**
	 * Returns a new object of class '<em>HIERARCHYELEMENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HIERARCHYELEMENT</em>'.
	 * @generated
	 */
	HIERARCHYELEMENT createHIERARCHYELEMENT();

	/**
	 * Returns a new object of class '<em>IDENTDESC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IDENTDESC</em>'.
	 * @generated
	 */
	IDENTDESC createIDENTDESC();

	/**
	 * Returns a new object of class '<em>IDENTDESCS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IDENTDESCS</em>'.
	 * @generated
	 */
	IDENTDESCS createIDENTDESCS();

	/**
	 * Returns a new object of class '<em>IDENTVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IDENTVALUE</em>'.
	 * @generated
	 */
	IDENTVALUE createIDENTVALUE();

	/**
	 * Returns a new object of class '<em>IDENTVALUES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IDENTVALUES</em>'.
	 * @generated
	 */
	IDENTVALUES createIDENTVALUES();

	/**
	 * Returns a new object of class '<em>IMPORTREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IMPORTREFS</em>'.
	 * @generated
	 */
	IMPORTREFS createIMPORTREFS();

	/**
	 * Returns a new object of class '<em>INFOCOMPONENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>INFOCOMPONENT</em>'.
	 * @generated
	 */
	INFOCOMPONENT createINFOCOMPONENT();

	/**
	 * Returns a new object of class '<em>INFOCOMPONENTREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>INFOCOMPONENTREFS</em>'.
	 * @generated
	 */
	INFOCOMPONENTREFS createINFOCOMPONENTREFS();

	/**
	 * Returns a new object of class '<em>INFOCOMPONENTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>INFOCOMPONENTS</em>'.
	 * @generated
	 */
	INFOCOMPONENTS createINFOCOMPONENTS();

	/**
	 * Returns a new object of class '<em>Inline</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inline</em>'.
	 * @generated
	 */
	Inline createInline();

	/**
	 * Returns a new object of class '<em>INPUTPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>INPUTPARAM</em>'.
	 * @generated
	 */
	INPUTPARAM createINPUTPARAM();

	/**
	 * Returns a new object of class '<em>INPUTPARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>INPUTPARAMS</em>'.
	 * @generated
	 */
	INPUTPARAMS createINPUTPARAMS();

	/**
	 * Returns a new object of class '<em>INTERNALCONSTR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>INTERNALCONSTR</em>'.
	 * @generated
	 */
	INTERNALCONSTR createINTERNALCONSTR();

	/**
	 * Returns a new object of class '<em>INTERNFLASHDATA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>INTERNFLASHDATA</em>'.
	 * @generated
	 */
	INTERNFLASHDATA createINTERNFLASHDATA();

	/**
	 * Returns a new object of class '<em>IType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IType</em>'.
	 * @generated
	 */
	IType createIType();

	/**
	 * Returns a new object of class '<em>LAYERREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LAYERREFS</em>'.
	 * @generated
	 */
	LAYERREFS createLAYERREFS();

	/**
	 * Returns a new object of class '<em>LEADINGLENGTHINFOTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LEADINGLENGTHINFOTYPE</em>'.
	 * @generated
	 */
	LEADINGLENGTHINFOTYPE createLEADINGLENGTHINFOTYPE();

	/**
	 * Returns a new object of class '<em>LENGTHDESCRIPTOR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LENGTHDESCRIPTOR</em>'.
	 * @generated
	 */
	LENGTHDESCRIPTOR createLENGTHDESCRIPTOR();

	/**
	 * Returns a new object of class '<em>LENGTHKEY</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LENGTHKEY</em>'.
	 * @generated
	 */
	LENGTHKEY createLENGTHKEY();

	/**
	 * Returns a new object of class '<em>LIMIT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LIMIT</em>'.
	 * @generated
	 */
	LIMIT createLIMIT();

	/**
	 * Returns a new object of class '<em>LINKCOMPARAMREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LINKCOMPARAMREF</em>'.
	 * @generated
	 */
	LINKCOMPARAMREF createLINKCOMPARAMREF();

	/**
	 * Returns a new object of class '<em>LINKCOMPARAMREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LINKCOMPARAMREFS</em>'.
	 * @generated
	 */
	LINKCOMPARAMREFS createLINKCOMPARAMREFS();

	/**
	 * Returns a new object of class '<em>Li Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Li Type</em>'.
	 * @generated
	 */
	LiType createLiType();

	/**
	 * Returns a new object of class '<em>LOGICALLINK</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LOGICALLINK</em>'.
	 * @generated
	 */
	LOGICALLINK createLOGICALLINK();

	/**
	 * Returns a new object of class '<em>LOGICALLINKREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LOGICALLINKREFS</em>'.
	 * @generated
	 */
	LOGICALLINKREFS createLOGICALLINKREFS();

	/**
	 * Returns a new object of class '<em>LOGICALLINKS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LOGICALLINKS</em>'.
	 * @generated
	 */
	LOGICALLINKS createLOGICALLINKS();

	/**
	 * Returns a new object of class '<em>MATCHINGCOMPONENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MATCHINGCOMPONENT</em>'.
	 * @generated
	 */
	MATCHINGCOMPONENT createMATCHINGCOMPONENT();

	/**
	 * Returns a new object of class '<em>MATCHINGCOMPONENTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MATCHINGCOMPONENTS</em>'.
	 * @generated
	 */
	MATCHINGCOMPONENTS createMATCHINGCOMPONENTS();

	/**
	 * Returns a new object of class '<em>MATCHINGPARAMETER</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MATCHINGPARAMETER</em>'.
	 * @generated
	 */
	MATCHINGPARAMETER createMATCHINGPARAMETER();

	/**
	 * Returns a new object of class '<em>MATCHINGPARAMETERS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MATCHINGPARAMETERS</em>'.
	 * @generated
	 */
	MATCHINGPARAMETERS createMATCHINGPARAMETERS();

	/**
	 * Returns a new object of class '<em>MATCHINGREQUESTPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MATCHINGREQUESTPARAM</em>'.
	 * @generated
	 */
	MATCHINGREQUESTPARAM createMATCHINGREQUESTPARAM();

	/**
	 * Returns a new object of class '<em>MEM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MEM</em>'.
	 * @generated
	 */
	MEM createMEM();

	/**
	 * Returns a new object of class '<em>MEMBERLOGICALLINK</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MEMBERLOGICALLINK</em>'.
	 * @generated
	 */
	MEMBERLOGICALLINK createMEMBERLOGICALLINK();

	/**
	 * Returns a new object of class '<em>MINMAXLENGTHTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MINMAXLENGTHTYPE</em>'.
	 * @generated
	 */
	MINMAXLENGTHTYPE createMINMAXLENGTHTYPE();

	/**
	 * Returns a new object of class '<em>MODELYEAR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MODELYEAR</em>'.
	 * @generated
	 */
	MODELYEAR createMODELYEAR();

	/**
	 * Returns a new object of class '<em>MODIFICATION</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MODIFICATION</em>'.
	 * @generated
	 */
	MODIFICATION createMODIFICATION();

	/**
	 * Returns a new object of class '<em>MODIFICATION1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MODIFICATION1</em>'.
	 * @generated
	 */
	MODIFICATION1 createMODIFICATION1();

	/**
	 * Returns a new object of class '<em>MODIFICATIONS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MODIFICATIONS</em>'.
	 * @generated
	 */
	MODIFICATIONS createMODIFICATIONS();

	/**
	 * Returns a new object of class '<em>MODIFICATIONS1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MODIFICATIONS1</em>'.
	 * @generated
	 */
	MODIFICATIONS1 createMODIFICATIONS1();

	/**
	 * Returns a new object of class '<em>MULTIPLEECUJOB</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MULTIPLEECUJOB</em>'.
	 * @generated
	 */
	MULTIPLEECUJOB createMULTIPLEECUJOB();

	/**
	 * Returns a new object of class '<em>MULTIPLEECUJOBS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MULTIPLEECUJOBS</em>'.
	 * @generated
	 */
	MULTIPLEECUJOBS createMULTIPLEECUJOBS();

	/**
	 * Returns a new object of class '<em>MULTIPLEECUJOBSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MULTIPLEECUJOBSPEC</em>'.
	 * @generated
	 */
	MULTIPLEECUJOBSPEC createMULTIPLEECUJOBSPEC();

	/**
	 * Returns a new object of class '<em>MUX</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUX</em>'.
	 * @generated
	 */
	MUX createMUX();

	/**
	 * Returns a new object of class '<em>MUXS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MUXS</em>'.
	 * @generated
	 */
	MUXS createMUXS();

	/**
	 * Returns a new object of class '<em>NEGOFFSET</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NEGOFFSET</em>'.
	 * @generated
	 */
	NEGOFFSET createNEGOFFSET();

	/**
	 * Returns a new object of class '<em>NEGOUTPUTPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NEGOUTPUTPARAM</em>'.
	 * @generated
	 */
	NEGOUTPUTPARAM createNEGOUTPUTPARAM();

	/**
	 * Returns a new object of class '<em>NEGOUTPUTPARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NEGOUTPUTPARAMS</em>'.
	 * @generated
	 */
	NEGOUTPUTPARAMS createNEGOUTPUTPARAMS();

	/**
	 * Returns a new object of class '<em>NEGRESPONSE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NEGRESPONSE</em>'.
	 * @generated
	 */
	NEGRESPONSE createNEGRESPONSE();

	/**
	 * Returns a new object of class '<em>NEGRESPONSEREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NEGRESPONSEREFS</em>'.
	 * @generated
	 */
	NEGRESPONSEREFS createNEGRESPONSEREFS();

	/**
	 * Returns a new object of class '<em>NEGRESPONSES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NEGRESPONSES</em>'.
	 * @generated
	 */
	NEGRESPONSES createNEGRESPONSES();

	/**
	 * Returns a new object of class '<em>NOTINHERITEDDIAGCOMM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NOTINHERITEDDIAGCOMM</em>'.
	 * @generated
	 */
	NOTINHERITEDDIAGCOMM createNOTINHERITEDDIAGCOMM();

	/**
	 * Returns a new object of class '<em>NOTINHERITEDDIAGCOMMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NOTINHERITEDDIAGCOMMS</em>'.
	 * @generated
	 */
	NOTINHERITEDDIAGCOMMS createNOTINHERITEDDIAGCOMMS();

	/**
	 * Returns a new object of class '<em>NOTINHERITEDVARIABLE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NOTINHERITEDVARIABLE</em>'.
	 * @generated
	 */
	NOTINHERITEDVARIABLE createNOTINHERITEDVARIABLE();

	/**
	 * Returns a new object of class '<em>NOTINHERITEDVARIABLES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>NOTINHERITEDVARIABLES</em>'.
	 * @generated
	 */
	NOTINHERITEDVARIABLES createNOTINHERITEDVARIABLES();

	/**
	 * Returns a new object of class '<em>ODX</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ODX</em>'.
	 * @generated
	 */
	ODX createODX();

	/**
	 * Returns a new object of class '<em>ODXCATEGORY</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ODXCATEGORY</em>'.
	 * @generated
	 */
	ODXCATEGORY createODXCATEGORY();

	/**
	 * Returns a new object of class '<em>ODXLINK</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ODXLINK</em>'.
	 * @generated
	 */
	ODXLINK createODXLINK();

	/**
	 * Returns a new object of class '<em>ODXLINK1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ODXLINK1</em>'.
	 * @generated
	 */
	ODXLINK1 createODXLINK1();

	/**
	 * Returns a new object of class '<em>OEM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OEM</em>'.
	 * @generated
	 */
	OEM createOEM();

	/**
	 * Returns a new object of class '<em>Ol Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ol Type</em>'.
	 * @generated
	 */
	OlType createOlType();

	/**
	 * Returns a new object of class '<em>OUTPUTPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OUTPUTPARAM</em>'.
	 * @generated
	 */
	OUTPUTPARAM createOUTPUTPARAM();

	/**
	 * Returns a new object of class '<em>OUTPUTPARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OUTPUTPARAMS</em>'.
	 * @generated
	 */
	OUTPUTPARAMS createOUTPUTPARAMS();

	/**
	 * Returns a new object of class '<em>OWNIDENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OWNIDENT</em>'.
	 * @generated
	 */
	OWNIDENT createOWNIDENT();

	/**
	 * Returns a new object of class '<em>OWNIDENTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OWNIDENTS</em>'.
	 * @generated
	 */
	OWNIDENTS createOWNIDENTS();

	/**
	 * Returns a new object of class '<em>P</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>P</em>'.
	 * @generated
	 */
	P createP();

	/**
	 * Returns a new object of class '<em>PARAM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PARAM</em>'.
	 * @generated
	 */
	PARAM createPARAM();

	/**
	 * Returns a new object of class '<em>PARAMLENGTHINFOTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PARAMLENGTHINFOTYPE</em>'.
	 * @generated
	 */
	PARAMLENGTHINFOTYPE createPARAMLENGTHINFOTYPE();

	/**
	 * Returns a new object of class '<em>PARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PARAMS</em>'.
	 * @generated
	 */
	PARAMS createPARAMS();

	/**
	 * Returns a new object of class '<em>PARENTREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PARENTREF</em>'.
	 * @generated
	 */
	PARENTREF createPARENTREF();

	/**
	 * Returns a new object of class '<em>PARENTREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PARENTREFS</em>'.
	 * @generated
	 */
	PARENTREFS createPARENTREFS();

	/**
	 * Returns a new object of class '<em>PHYSCONST</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PHYSCONST</em>'.
	 * @generated
	 */
	PHYSCONST createPHYSCONST();

	/**
	 * Returns a new object of class '<em>PHYSICALDIMENSION</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PHYSICALDIMENSION</em>'.
	 * @generated
	 */
	PHYSICALDIMENSION createPHYSICALDIMENSION();

	/**
	 * Returns a new object of class '<em>PHYSICALDIMENSIONS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PHYSICALDIMENSIONS</em>'.
	 * @generated
	 */
	PHYSICALDIMENSIONS createPHYSICALDIMENSIONS();

	/**
	 * Returns a new object of class '<em>PHYSICALTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PHYSICALTYPE</em>'.
	 * @generated
	 */
	PHYSICALTYPE createPHYSICALTYPE();

	/**
	 * Returns a new object of class '<em>PHYSICALVEHICLELINK</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PHYSICALVEHICLELINK</em>'.
	 * @generated
	 */
	PHYSICALVEHICLELINK createPHYSICALVEHICLELINK();

	/**
	 * Returns a new object of class '<em>PHYSICALVEHICLELINKS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PHYSICALVEHICLELINKS</em>'.
	 * @generated
	 */
	PHYSICALVEHICLELINKS createPHYSICALVEHICLELINKS();

	/**
	 * Returns a new object of class '<em>PHYSMEM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PHYSMEM</em>'.
	 * @generated
	 */
	PHYSMEM createPHYSMEM();

	/**
	 * Returns a new object of class '<em>PHYSSEGMENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PHYSSEGMENT</em>'.
	 * @generated
	 */
	PHYSSEGMENT createPHYSSEGMENT();

	/**
	 * Returns a new object of class '<em>PHYSSEGMENTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PHYSSEGMENTS</em>'.
	 * @generated
	 */
	PHYSSEGMENTS createPHYSSEGMENTS();

	/**
	 * Returns a new object of class '<em>POSITIONABLEPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>POSITIONABLEPARAM</em>'.
	 * @generated
	 */
	POSITIONABLEPARAM createPOSITIONABLEPARAM();

	/**
	 * Returns a new object of class '<em>POSOFFSET</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>POSOFFSET</em>'.
	 * @generated
	 */
	POSOFFSET createPOSOFFSET();

	/**
	 * Returns a new object of class '<em>POSRESPONSE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>POSRESPONSE</em>'.
	 * @generated
	 */
	POSRESPONSE createPOSRESPONSE();

	/**
	 * Returns a new object of class '<em>POSRESPONSEREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>POSRESPONSEREFS</em>'.
	 * @generated
	 */
	POSRESPONSEREFS createPOSRESPONSEREFS();

	/**
	 * Returns a new object of class '<em>POSRESPONSES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>POSRESPONSES</em>'.
	 * @generated
	 */
	POSRESPONSES createPOSRESPONSES();

	/**
	 * Returns a new object of class '<em>PROGCODE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROGCODE</em>'.
	 * @generated
	 */
	PROGCODE createPROGCODE();

	/**
	 * Returns a new object of class '<em>PROGCODES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROGCODES</em>'.
	 * @generated
	 */
	PROGCODES createPROGCODES();

	/**
	 * Returns a new object of class '<em>PROJECTIDENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROJECTIDENT</em>'.
	 * @generated
	 */
	PROJECTIDENT createPROJECTIDENT();

	/**
	 * Returns a new object of class '<em>PROJECTIDENTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROJECTIDENTS</em>'.
	 * @generated
	 */
	PROJECTIDENTS createPROJECTIDENTS();

	/**
	 * Returns a new object of class '<em>PROJECTINFO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROJECTINFO</em>'.
	 * @generated
	 */
	PROJECTINFO createPROJECTINFO();

	/**
	 * Returns a new object of class '<em>PROJECTINFOS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROJECTINFOS</em>'.
	 * @generated
	 */
	PROJECTINFOS createPROJECTINFOS();

	/**
	 * Returns a new object of class '<em>PROTOCOL</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROTOCOL</em>'.
	 * @generated
	 */
	PROTOCOL createPROTOCOL();

	/**
	 * Returns a new object of class '<em>PROTOCOLREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROTOCOLREF</em>'.
	 * @generated
	 */
	PROTOCOLREF createPROTOCOLREF();

	/**
	 * Returns a new object of class '<em>PROTOCOLS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROTOCOLS</em>'.
	 * @generated
	 */
	PROTOCOLS createPROTOCOLS();

	/**
	 * Returns a new object of class '<em>PROTOCOLSNREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>PROTOCOLSNREFS</em>'.
	 * @generated
	 */
	PROTOCOLSNREFS createPROTOCOLSNREFS();

	/**
	 * Returns a new object of class '<em>RELATEDDIAGCOMMREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RELATEDDIAGCOMMREF</em>'.
	 * @generated
	 */
	RELATEDDIAGCOMMREF createRELATEDDIAGCOMMREF();

	/**
	 * Returns a new object of class '<em>RELATEDDIAGCOMMREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RELATEDDIAGCOMMREFS</em>'.
	 * @generated
	 */
	RELATEDDIAGCOMMREFS createRELATEDDIAGCOMMREFS();

	/**
	 * Returns a new object of class '<em>RELATEDDOC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RELATEDDOC</em>'.
	 * @generated
	 */
	RELATEDDOC createRELATEDDOC();

	/**
	 * Returns a new object of class '<em>RELATEDDOC1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RELATEDDOC1</em>'.
	 * @generated
	 */
	RELATEDDOC1 createRELATEDDOC1();

	/**
	 * Returns a new object of class '<em>RELATEDDOCS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RELATEDDOCS</em>'.
	 * @generated
	 */
	RELATEDDOCS createRELATEDDOCS();

	/**
	 * Returns a new object of class '<em>RELATEDDOCS1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RELATEDDOCS1</em>'.
	 * @generated
	 */
	RELATEDDOCS1 createRELATEDDOCS1();

	/**
	 * Returns a new object of class '<em>REQUEST</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>REQUEST</em>'.
	 * @generated
	 */
	REQUEST createREQUEST();

	/**
	 * Returns a new object of class '<em>REQUESTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>REQUESTS</em>'.
	 * @generated
	 */
	REQUESTS createREQUESTS();

	/**
	 * Returns a new object of class '<em>RESERVED</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RESERVED</em>'.
	 * @generated
	 */
	RESERVED createRESERVED();

	/**
	 * Returns a new object of class '<em>RESPONSE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RESPONSE</em>'.
	 * @generated
	 */
	RESPONSE createRESPONSE();

	/**
	 * Returns a new object of class '<em>ROLES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ROLES</em>'.
	 * @generated
	 */
	ROLES createROLES();

	/**
	 * Returns a new object of class '<em>ROLES1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>ROLES1</em>'.
	 * @generated
	 */
	ROLES1 createROLES1();

	/**
	 * Returns a new object of class '<em>SCALECONSTR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SCALECONSTR</em>'.
	 * @generated
	 */
	SCALECONSTR createSCALECONSTR();

	/**
	 * Returns a new object of class '<em>SCALECONSTRS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SCALECONSTRS</em>'.
	 * @generated
	 */
	SCALECONSTRS createSCALECONSTRS();

	/**
	 * Returns a new object of class '<em>SD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SD</em>'.
	 * @generated
	 */
	SD createSD();

	/**
	 * Returns a new object of class '<em>SD1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SD1</em>'.
	 * @generated
	 */
	SD1 createSD1();

	/**
	 * Returns a new object of class '<em>SDG</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SDG</em>'.
	 * @generated
	 */
	SDG createSDG();

	/**
	 * Returns a new object of class '<em>SDG1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SDG1</em>'.
	 * @generated
	 */
	SDG1 createSDG1();

	/**
	 * Returns a new object of class '<em>SDGCAPTION</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SDGCAPTION</em>'.
	 * @generated
	 */
	SDGCAPTION createSDGCAPTION();

	/**
	 * Returns a new object of class '<em>SDGCAPTION1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SDGCAPTION1</em>'.
	 * @generated
	 */
	SDGCAPTION1 createSDGCAPTION1();

	/**
	 * Returns a new object of class '<em>SDGS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SDGS</em>'.
	 * @generated
	 */
	SDGS createSDGS();

	/**
	 * Returns a new object of class '<em>SDGS1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SDGS1</em>'.
	 * @generated
	 */
	SDGS1 createSDGS1();

	/**
	 * Returns a new object of class '<em>SECURITY</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SECURITY</em>'.
	 * @generated
	 */
	SECURITY createSECURITY();

	/**
	 * Returns a new object of class '<em>SECURITYMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SECURITYMETHOD</em>'.
	 * @generated
	 */
	SECURITYMETHOD createSECURITYMETHOD();

	/**
	 * Returns a new object of class '<em>SECURITYS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SECURITYS</em>'.
	 * @generated
	 */
	SECURITYS createSECURITYS();

	/**
	 * Returns a new object of class '<em>SEGMENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SEGMENT</em>'.
	 * @generated
	 */
	SEGMENT createSEGMENT();

	/**
	 * Returns a new object of class '<em>SEGMENTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SEGMENTS</em>'.
	 * @generated
	 */
	SEGMENTS createSEGMENTS();

	/**
	 * Returns a new object of class '<em>SELECTIONTABLEREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SELECTIONTABLEREFS</em>'.
	 * @generated
	 */
	SELECTIONTABLEREFS createSELECTIONTABLEREFS();

	/**
	 * Returns a new object of class '<em>SESSION</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SESSION</em>'.
	 * @generated
	 */
	SESSION createSESSION();

	/**
	 * Returns a new object of class '<em>SESSIONDESC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SESSIONDESC</em>'.
	 * @generated
	 */
	SESSIONDESC createSESSIONDESC();

	/**
	 * Returns a new object of class '<em>SESSIONDESCS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SESSIONDESCS</em>'.
	 * @generated
	 */
	SESSIONDESCS createSESSIONDESCS();

	/**
	 * Returns a new object of class '<em>SESSIONS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SESSIONS</em>'.
	 * @generated
	 */
	SESSIONS createSESSIONS();

	/**
	 * Returns a new object of class '<em>SINGLEECUJOB</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SINGLEECUJOB</em>'.
	 * @generated
	 */
	SINGLEECUJOB createSINGLEECUJOB();

	/**
	 * Returns a new object of class '<em>SIZEDEFFILTER</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SIZEDEFFILTER</em>'.
	 * @generated
	 */
	SIZEDEFFILTER createSIZEDEFFILTER();

	/**
	 * Returns a new object of class '<em>SIZEDEFPHYSSEGMENT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SIZEDEFPHYSSEGMENT</em>'.
	 * @generated
	 */
	SIZEDEFPHYSSEGMENT createSIZEDEFPHYSSEGMENT();

	/**
	 * Returns a new object of class '<em>SNREF</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SNREF</em>'.
	 * @generated
	 */
	SNREF createSNREF();

	/**
	 * Returns a new object of class '<em>SOURCEENDADDRESS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SOURCEENDADDRESS</em>'.
	 * @generated
	 */
	SOURCEENDADDRESS createSOURCEENDADDRESS();

	/**
	 * Returns a new object of class '<em>SPECIALDATA</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SPECIALDATA</em>'.
	 * @generated
	 */
	SPECIALDATA createSPECIALDATA();

	/**
	 * Returns a new object of class '<em>SPECIALDATA1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SPECIALDATA1</em>'.
	 * @generated
	 */
	SPECIALDATA1 createSPECIALDATA1();

	/**
	 * Returns a new object of class '<em>STANDARDLENGTHTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>STANDARDLENGTHTYPE</em>'.
	 * @generated
	 */
	STANDARDLENGTHTYPE createSTANDARDLENGTHTYPE();

	/**
	 * Returns a new object of class '<em>STATICFIELD</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>STATICFIELD</em>'.
	 * @generated
	 */
	STATICFIELD createSTATICFIELD();

	/**
	 * Returns a new object of class '<em>STATICFIELDS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>STATICFIELDS</em>'.
	 * @generated
	 */
	STATICFIELDS createSTATICFIELDS();

	/**
	 * Returns a new object of class '<em>STRUCTURE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>STRUCTURE</em>'.
	 * @generated
	 */
	STRUCTURE createSTRUCTURE();

	/**
	 * Returns a new object of class '<em>STRUCTURES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>STRUCTURES</em>'.
	 * @generated
	 */
	STRUCTURES createSTRUCTURES();

	/**
	 * Returns a new object of class '<em>Sub Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sub Type</em>'.
	 * @generated
	 */
	SubType createSubType();

	/**
	 * Returns a new object of class '<em>SUPPORTEDDYNID</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SUPPORTEDDYNID</em>'.
	 * @generated
	 */
	SUPPORTEDDYNID createSUPPORTEDDYNID();

	/**
	 * Returns a new object of class '<em>SUPPORTEDDYNIDS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SUPPORTEDDYNIDS</em>'.
	 * @generated
	 */
	SUPPORTEDDYNIDS createSUPPORTEDDYNIDS();

	/**
	 * Returns a new object of class '<em>Sup Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sup Type</em>'.
	 * @generated
	 */
	SupType createSupType();

	/**
	 * Returns a new object of class '<em>SWITCHKEY</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SWITCHKEY</em>'.
	 * @generated
	 */
	SWITCHKEY createSWITCHKEY();

	/**
	 * Returns a new object of class '<em>SWVARIABLE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SWVARIABLE</em>'.
	 * @generated
	 */
	SWVARIABLE createSWVARIABLE();

	/**
	 * Returns a new object of class '<em>SWVARIABLES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SWVARIABLES</em>'.
	 * @generated
	 */
	SWVARIABLES createSWVARIABLES();

	/**
	 * Returns a new object of class '<em>SYSTEM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SYSTEM</em>'.
	 * @generated
	 */
	SYSTEM createSYSTEM();

	/**
	 * Returns a new object of class '<em>TABLE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TABLE</em>'.
	 * @generated
	 */
	TABLE createTABLE();

	/**
	 * Returns a new object of class '<em>TABLEENTRY</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TABLEENTRY</em>'.
	 * @generated
	 */
	TABLEENTRY createTABLEENTRY();

	/**
	 * Returns a new object of class '<em>TABLEKEY</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TABLEKEY</em>'.
	 * @generated
	 */
	TABLEKEY createTABLEKEY();

	/**
	 * Returns a new object of class '<em>TABLEROW</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TABLEROW</em>'.
	 * @generated
	 */
	TABLEROW createTABLEROW();

	/**
	 * Returns a new object of class '<em>TABLES</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TABLES</em>'.
	 * @generated
	 */
	TABLES createTABLES();

	/**
	 * Returns a new object of class '<em>TABLESTRUCT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TABLESTRUCT</em>'.
	 * @generated
	 */
	TABLESTRUCT createTABLESTRUCT();

	/**
	 * Returns a new object of class '<em>TARGETADDROFFSET</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TARGETADDROFFSET</em>'.
	 * @generated
	 */
	TARGETADDROFFSET createTARGETADDROFFSET();

	/**
	 * Returns a new object of class '<em>TEAMMEMBER</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TEAMMEMBER</em>'.
	 * @generated
	 */
	TEAMMEMBER createTEAMMEMBER();

	/**
	 * Returns a new object of class '<em>TEAMMEMBER1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TEAMMEMBER1</em>'.
	 * @generated
	 */
	TEAMMEMBER1 createTEAMMEMBER1();

	/**
	 * Returns a new object of class '<em>TEAMMEMBERS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TEAMMEMBERS</em>'.
	 * @generated
	 */
	TEAMMEMBERS createTEAMMEMBERS();

	/**
	 * Returns a new object of class '<em>TEAMMEMBERS1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TEAMMEMBERS1</em>'.
	 * @generated
	 */
	TEAMMEMBERS1 createTEAMMEMBERS1();

	/**
	 * Returns a new object of class '<em>TEXT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TEXT</em>'.
	 * @generated
	 */
	TEXT createTEXT();

	/**
	 * Returns a new object of class '<em>TEXT1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TEXT1</em>'.
	 * @generated
	 */
	TEXT1 createTEXT1();

	/**
	 * Returns a new object of class '<em>Ul Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ul Type</em>'.
	 * @generated
	 */
	UlType createUlType();

	/**
	 * Returns a new object of class '<em>UNCOMPRESSEDSIZE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UNCOMPRESSEDSIZE</em>'.
	 * @generated
	 */
	UNCOMPRESSEDSIZE createUNCOMPRESSEDSIZE();

	/**
	 * Returns a new object of class '<em>UNIONVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UNIONVALUE</em>'.
	 * @generated
	 */
	UNIONVALUE createUNIONVALUE();

	/**
	 * Returns a new object of class '<em>UNIT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UNIT</em>'.
	 * @generated
	 */
	UNIT createUNIT();

	/**
	 * Returns a new object of class '<em>UNITGROUP</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UNITGROUP</em>'.
	 * @generated
	 */
	UNITGROUP createUNITGROUP();

	/**
	 * Returns a new object of class '<em>UNITGROUPS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UNITGROUPS</em>'.
	 * @generated
	 */
	UNITGROUPS createUNITGROUPS();

	/**
	 * Returns a new object of class '<em>UNITREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UNITREFS</em>'.
	 * @generated
	 */
	UNITREFS createUNITREFS();

	/**
	 * Returns a new object of class '<em>UNITS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UNITS</em>'.
	 * @generated
	 */
	UNITS createUNITS();

	/**
	 * Returns a new object of class '<em>UNITSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UNITSPEC</em>'.
	 * @generated
	 */
	UNITSPEC createUNITSPEC();

	/**
	 * Returns a new object of class '<em>V</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>V</em>'.
	 * @generated
	 */
	V createV();

	/**
	 * Returns a new object of class '<em>VALIDITYFOR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VALIDITYFOR</em>'.
	 * @generated
	 */
	VALIDITYFOR createVALIDITYFOR();

	/**
	 * Returns a new object of class '<em>VALUE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VALUE</em>'.
	 * @generated
	 */
	VALUE createVALUE();

	/**
	 * Returns a new object of class '<em>VARIABLEGROUP</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VARIABLEGROUP</em>'.
	 * @generated
	 */
	VARIABLEGROUP createVARIABLEGROUP();

	/**
	 * Returns a new object of class '<em>VARIABLEGROUPS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VARIABLEGROUPS</em>'.
	 * @generated
	 */
	VARIABLEGROUPS createVARIABLEGROUPS();

	/**
	 * Returns a new object of class '<em>VEHICLECONNECTOR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLECONNECTOR</em>'.
	 * @generated
	 */
	VEHICLECONNECTOR createVEHICLECONNECTOR();

	/**
	 * Returns a new object of class '<em>VEHICLECONNECTORPIN</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLECONNECTORPIN</em>'.
	 * @generated
	 */
	VEHICLECONNECTORPIN createVEHICLECONNECTORPIN();

	/**
	 * Returns a new object of class '<em>VEHICLECONNECTORPINREFS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLECONNECTORPINREFS</em>'.
	 * @generated
	 */
	VEHICLECONNECTORPINREFS createVEHICLECONNECTORPINREFS();

	/**
	 * Returns a new object of class '<em>VEHICLECONNECTORPINS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLECONNECTORPINS</em>'.
	 * @generated
	 */
	VEHICLECONNECTORPINS createVEHICLECONNECTORPINS();

	/**
	 * Returns a new object of class '<em>VEHICLECONNECTORS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLECONNECTORS</em>'.
	 * @generated
	 */
	VEHICLECONNECTORS createVEHICLECONNECTORS();

	/**
	 * Returns a new object of class '<em>VEHICLEINFORMATION</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLEINFORMATION</em>'.
	 * @generated
	 */
	VEHICLEINFORMATION createVEHICLEINFORMATION();

	/**
	 * Returns a new object of class '<em>VEHICLEINFORMATIONS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLEINFORMATIONS</em>'.
	 * @generated
	 */
	VEHICLEINFORMATIONS createVEHICLEINFORMATIONS();

	/**
	 * Returns a new object of class '<em>VEHICLEINFOSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLEINFOSPEC</em>'.
	 * @generated
	 */
	VEHICLEINFOSPEC createVEHICLEINFOSPEC();

	/**
	 * Returns a new object of class '<em>VEHICLEMODEL</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLEMODEL</em>'.
	 * @generated
	 */
	VEHICLEMODEL createVEHICLEMODEL();

	/**
	 * Returns a new object of class '<em>VEHICLETYPE</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VEHICLETYPE</em>'.
	 * @generated
	 */
	VEHICLETYPE createVEHICLETYPE();

	/**
	 * Returns a new object of class '<em>VT</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VT</em>'.
	 * @generated
	 */
	VT createVT();

	/**
	 * Returns a new object of class '<em>XDOC</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XDOC</em>'.
	 * @generated
	 */
	XDOC createXDOC();

	/**
	 * Returns a new object of class '<em>XDOC1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XDOC1</em>'.
	 * @generated
	 */
	XDOC1 createXDOC1();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OdxXhtmlPackage getOdxXhtmlPackage();

} //OdxXhtmlFactory
