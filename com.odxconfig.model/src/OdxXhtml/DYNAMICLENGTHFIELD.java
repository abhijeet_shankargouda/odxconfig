/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DYNAMICLENGTHFIELD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DYNAMICLENGTHFIELD#getOFFSET <em>OFFSET</em>}</li>
 *   <li>{@link OdxXhtml.DYNAMICLENGTHFIELD#getDETERMINENUMBEROFITEMS <em>DETERMINENUMBEROFITEMS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMICLENGTHFIELD()
 * @model extendedMetaData="name='DYNAMIC-LENGTH-FIELD' kind='elementOnly'"
 * @generated
 */
public interface DYNAMICLENGTHFIELD extends FIELD {
	/**
	 * Returns the value of the '<em><b>OFFSET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OFFSET</em>' attribute.
	 * @see #isSetOFFSET()
	 * @see #unsetOFFSET()
	 * @see #setOFFSET(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMICLENGTHFIELD_OFFSET()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='OFFSET' namespace='##targetNamespace'"
	 * @generated
	 */
	long getOFFSET();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNAMICLENGTHFIELD#getOFFSET <em>OFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OFFSET</em>' attribute.
	 * @see #isSetOFFSET()
	 * @see #unsetOFFSET()
	 * @see #getOFFSET()
	 * @generated
	 */
	void setOFFSET(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DYNAMICLENGTHFIELD#getOFFSET <em>OFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOFFSET()
	 * @see #getOFFSET()
	 * @see #setOFFSET(long)
	 * @generated
	 */
	void unsetOFFSET();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DYNAMICLENGTHFIELD#getOFFSET <em>OFFSET</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>OFFSET</em>' attribute is set.
	 * @see #unsetOFFSET()
	 * @see #getOFFSET()
	 * @see #setOFFSET(long)
	 * @generated
	 */
	boolean isSetOFFSET();

	/**
	 * Returns the value of the '<em><b>DETERMINENUMBEROFITEMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DETERMINENUMBEROFITEMS</em>' containment reference.
	 * @see #setDETERMINENUMBEROFITEMS(DETERMINENUMBEROFITEMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMICLENGTHFIELD_DETERMINENUMBEROFITEMS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DETERMINE-NUMBER-OF-ITEMS' namespace='##targetNamespace'"
	 * @generated
	 */
	DETERMINENUMBEROFITEMS getDETERMINENUMBEROFITEMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNAMICLENGTHFIELD#getDETERMINENUMBEROFITEMS <em>DETERMINENUMBEROFITEMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DETERMINENUMBEROFITEMS</em>' containment reference.
	 * @see #getDETERMINENUMBEROFITEMS()
	 * @generated
	 */
	void setDETERMINENUMBEROFITEMS(DETERMINENUMBEROFITEMS value);

} // DYNAMICLENGTHFIELD
