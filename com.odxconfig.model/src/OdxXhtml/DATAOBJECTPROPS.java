/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DATAOBJECTPROPS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DATAOBJECTPROPS#getDATAOBJECTPROP <em>DATAOBJECTPROP</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDATAOBJECTPROPS()
 * @model extendedMetaData="name='DATA-OBJECT-PROPS' kind='elementOnly'"
 * @generated
 */
public interface DATAOBJECTPROPS extends EObject {
	/**
	 * Returns the value of the '<em><b>DATAOBJECTPROP</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DATAOBJECTPROP}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATAOBJECTPROP</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAOBJECTPROPS_DATAOBJECTPROP()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATA-OBJECT-PROP' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DATAOBJECTPROP> getDATAOBJECTPROP();

} // DATAOBJECTPROPS
