/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPLEXDOP</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPLEXDOP()
 * @model extendedMetaData="name='COMPLEX-DOP' kind='elementOnly'"
 * @generated
 */
public interface COMPLEXDOP extends DOPBASE {
} // COMPLEXDOP
