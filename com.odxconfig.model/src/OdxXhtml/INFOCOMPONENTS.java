/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INFOCOMPONENTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.INFOCOMPONENTS#getINFOCOMPONENT <em>INFOCOMPONENT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getINFOCOMPONENTS()
 * @model extendedMetaData="name='INFO-COMPONENTS' kind='elementOnly'"
 * @generated
 */
public interface INFOCOMPONENTS extends EObject {
	/**
	 * Returns the value of the '<em><b>INFOCOMPONENT</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.INFOCOMPONENT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INFOCOMPONENT</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getINFOCOMPONENTS_INFOCOMPONENT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='INFO-COMPONENT' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<INFOCOMPONENT> getINFOCOMPONENT();

} // INFOCOMPONENTS
