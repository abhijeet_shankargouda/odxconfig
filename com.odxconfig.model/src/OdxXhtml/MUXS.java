/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUXS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MUXS#getMUX <em>MUX</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMUXS()
 * @model extendedMetaData="name='MUXS' kind='elementOnly'"
 * @generated
 */
public interface MUXS extends EObject {
	/**
	 * Returns the value of the '<em><b>MUX</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.MUX}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MUX</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getMUXS_MUX()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MUX' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MUX> getMUX();

} // MUXS
