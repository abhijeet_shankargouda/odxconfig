/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYDOCINFOS1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYDOCINFOS1#getCOMPANYDOCINFO <em>COMPANYDOCINFO</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDOCINFOS1()
 * @model extendedMetaData="name='COMPANY-DOC-INFOS' kind='elementOnly'"
 * @generated
 */
public interface COMPANYDOCINFOS1 extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPANYDOCINFO</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.COMPANYDOCINFO1}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYDOCINFO</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDOCINFOS1_COMPANYDOCINFO()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPANY-DOC-INFO' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<COMPANYDOCINFO1> getCOMPANYDOCINFO();

} // COMPANYDOCINFOS1
