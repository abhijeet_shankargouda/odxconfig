/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DATAFORMATSELECTION</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getDATAFORMATSELECTION()
 * @model extendedMetaData="name='DATAFORMAT-SELECTION'"
 * @generated
 */
public enum DATAFORMATSELECTION implements Enumerator {
	/**
	 * The '<em><b>INTELHEX</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTELHEX_VALUE
	 * @generated
	 * @ordered
	 */
	INTELHEX(0, "INTELHEX", "INTEL-HEX"),

	/**
	 * The '<em><b>MOTOROLAS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOTOROLAS_VALUE
	 * @generated
	 * @ordered
	 */
	MOTOROLAS(1, "MOTOROLAS", "MOTOROLA-S"),

	/**
	 * The '<em><b>BINARY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BINARY_VALUE
	 * @generated
	 * @ordered
	 */
	BINARY(2, "BINARY", "BINARY");

	/**
	 * The '<em><b>INTELHEX</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTELHEX
	 * @model literal="INTEL-HEX"
	 * @generated
	 * @ordered
	 */
	public static final int INTELHEX_VALUE = 0;

	/**
	 * The '<em><b>MOTOROLAS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOTOROLAS
	 * @model literal="MOTOROLA-S"
	 * @generated
	 * @ordered
	 */
	public static final int MOTOROLAS_VALUE = 1;

	/**
	 * The '<em><b>BINARY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BINARY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BINARY_VALUE = 2;

	/**
	 * An array of all the '<em><b>DATAFORMATSELECTION</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DATAFORMATSELECTION[] VALUES_ARRAY =
		new DATAFORMATSELECTION[] {
			INTELHEX,
			MOTOROLAS,
			BINARY,
		};

	/**
	 * A public read-only list of all the '<em><b>DATAFORMATSELECTION</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DATAFORMATSELECTION> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>DATAFORMATSELECTION</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DATAFORMATSELECTION get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DATAFORMATSELECTION result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DATAFORMATSELECTION</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DATAFORMATSELECTION getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DATAFORMATSELECTION result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DATAFORMATSELECTION</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DATAFORMATSELECTION get(int value) {
		switch (value) {
			case INTELHEX_VALUE: return INTELHEX;
			case MOTOROLAS_VALUE: return MOTOROLAS;
			case BINARY_VALUE: return BINARY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DATAFORMATSELECTION(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DATAFORMATSELECTION
