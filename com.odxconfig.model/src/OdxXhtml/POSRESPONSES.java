/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>POSRESPONSES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.POSRESPONSES#getPOSRESPONSE <em>POSRESPONSE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPOSRESPONSES()
 * @model extendedMetaData="name='POS-RESPONSES' kind='elementOnly'"
 * @generated
 */
public interface POSRESPONSES extends EObject {
	/**
	 * Returns the value of the '<em><b>POSRESPONSE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.POSRESPONSE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>POSRESPONSE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPOSRESPONSES_POSRESPONSE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='POS-RESPONSE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<POSRESPONSE> getPOSRESPONSE();

} // POSRESPONSES
