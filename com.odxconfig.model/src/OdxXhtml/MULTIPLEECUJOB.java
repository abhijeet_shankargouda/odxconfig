/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MULTIPLEECUJOB</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getFUNCTCLASSREFS <em>FUNCTCLASSREFS</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getPROGCODES <em>PROGCODES</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getINPUTPARAMS <em>INPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getOUTPUTPARAMS <em>OUTPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getNEGOUTPUTPARAMS <em>NEGOUTPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getDIAGLAYERREFS <em>DIAGLAYERREFS</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getAUDIENCE <em>AUDIENCE</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#isISEXECUTABLE <em>ISEXECUTABLE</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}</li>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOB#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB()
 * @model extendedMetaData="name='MULTIPLE-ECU-JOB' kind='elementOnly'"
 * @generated
 */
public interface MULTIPLEECUJOB extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA value);

	/**
	 * Returns the value of the '<em><b>SDGS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGS</em>' containment reference.
	 * @see #setSDGS(SDGS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_SDGS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDGS' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGS getSDGS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getSDGS <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGS</em>' containment reference.
	 * @see #getSDGS()
	 * @generated
	 */
	void setSDGS(SDGS value);

	/**
	 * Returns the value of the '<em><b>FUNCTCLASSREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FUNCTCLASSREFS</em>' containment reference.
	 * @see #setFUNCTCLASSREFS(FUNCTCLASSREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_FUNCTCLASSREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FUNCT-CLASS-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	FUNCTCLASSREFS getFUNCTCLASSREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getFUNCTCLASSREFS <em>FUNCTCLASSREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FUNCTCLASSREFS</em>' containment reference.
	 * @see #getFUNCTCLASSREFS()
	 * @generated
	 */
	void setFUNCTCLASSREFS(FUNCTCLASSREFS value);

	/**
	 * Returns the value of the '<em><b>PROGCODES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROGCODES</em>' containment reference.
	 * @see #setPROGCODES(PROGCODES)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_PROGCODES()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PROG-CODES' namespace='##targetNamespace'"
	 * @generated
	 */
	PROGCODES getPROGCODES();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getPROGCODES <em>PROGCODES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROGCODES</em>' containment reference.
	 * @see #getPROGCODES()
	 * @generated
	 */
	void setPROGCODES(PROGCODES value);

	/**
	 * Returns the value of the '<em><b>INPUTPARAMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INPUTPARAMS</em>' containment reference.
	 * @see #setINPUTPARAMS(INPUTPARAMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_INPUTPARAMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='INPUT-PARAMS' namespace='##targetNamespace'"
	 * @generated
	 */
	INPUTPARAMS getINPUTPARAMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getINPUTPARAMS <em>INPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>INPUTPARAMS</em>' containment reference.
	 * @see #getINPUTPARAMS()
	 * @generated
	 */
	void setINPUTPARAMS(INPUTPARAMS value);

	/**
	 * Returns the value of the '<em><b>OUTPUTPARAMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OUTPUTPARAMS</em>' containment reference.
	 * @see #setOUTPUTPARAMS(OUTPUTPARAMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_OUTPUTPARAMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OUTPUT-PARAMS' namespace='##targetNamespace'"
	 * @generated
	 */
	OUTPUTPARAMS getOUTPUTPARAMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getOUTPUTPARAMS <em>OUTPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OUTPUTPARAMS</em>' containment reference.
	 * @see #getOUTPUTPARAMS()
	 * @generated
	 */
	void setOUTPUTPARAMS(OUTPUTPARAMS value);

	/**
	 * Returns the value of the '<em><b>NEGOUTPUTPARAMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NEGOUTPUTPARAMS</em>' containment reference.
	 * @see #setNEGOUTPUTPARAMS(NEGOUTPUTPARAMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_NEGOUTPUTPARAMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='NEG-OUTPUT-PARAMS' namespace='##targetNamespace'"
	 * @generated
	 */
	NEGOUTPUTPARAMS getNEGOUTPUTPARAMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getNEGOUTPUTPARAMS <em>NEGOUTPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NEGOUTPUTPARAMS</em>' containment reference.
	 * @see #getNEGOUTPUTPARAMS()
	 * @generated
	 */
	void setNEGOUTPUTPARAMS(NEGOUTPUTPARAMS value);

	/**
	 * Returns the value of the '<em><b>DIAGLAYERREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGLAYERREFS</em>' containment reference.
	 * @see #setDIAGLAYERREFS(DIAGLAYERREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_DIAGLAYERREFS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-LAYER-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGLAYERREFS getDIAGLAYERREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getDIAGLAYERREFS <em>DIAGLAYERREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGLAYERREFS</em>' containment reference.
	 * @see #getDIAGLAYERREFS()
	 * @generated
	 */
	void setDIAGLAYERREFS(DIAGLAYERREFS value);

	/**
	 * Returns the value of the '<em><b>AUDIENCE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUDIENCE</em>' containment reference.
	 * @see #setAUDIENCE(AUDIENCE)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_AUDIENCE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='AUDIENCE' namespace='##targetNamespace'"
	 * @generated
	 */
	AUDIENCE getAUDIENCE();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getAUDIENCE <em>AUDIENCE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AUDIENCE</em>' containment reference.
	 * @see #getAUDIENCE()
	 * @generated
	 */
	void setAUDIENCE(AUDIENCE value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>ISEXECUTABLE</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISEXECUTABLE</em>' attribute.
	 * @see #isSetISEXECUTABLE()
	 * @see #unsetISEXECUTABLE()
	 * @see #setISEXECUTABLE(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_ISEXECUTABLE()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-EXECUTABLE' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISEXECUTABLE();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#isISEXECUTABLE <em>ISEXECUTABLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISEXECUTABLE</em>' attribute.
	 * @see #isSetISEXECUTABLE()
	 * @see #unsetISEXECUTABLE()
	 * @see #isISEXECUTABLE()
	 * @generated
	 */
	void setISEXECUTABLE(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#isISEXECUTABLE <em>ISEXECUTABLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISEXECUTABLE()
	 * @see #isISEXECUTABLE()
	 * @see #setISEXECUTABLE(boolean)
	 * @generated
	 */
	void unsetISEXECUTABLE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MULTIPLEECUJOB#isISEXECUTABLE <em>ISEXECUTABLE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISEXECUTABLE</em>' attribute is set.
	 * @see #unsetISEXECUTABLE()
	 * @see #isISEXECUTABLE()
	 * @see #setISEXECUTABLE(boolean)
	 * @generated
	 */
	boolean isSetISEXECUTABLE();

	/**
	 * Returns the value of the '<em><b>ISREDUCEDRESULTENABLED</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISREDUCEDRESULTENABLED</em>' attribute.
	 * @see #isSetISREDUCEDRESULTENABLED()
	 * @see #unsetISREDUCEDRESULTENABLED()
	 * @see #setISREDUCEDRESULTENABLED(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_ISREDUCEDRESULTENABLED()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-REDUCED-RESULT-ENABLED' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISREDUCEDRESULTENABLED();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISREDUCEDRESULTENABLED</em>' attribute.
	 * @see #isSetISREDUCEDRESULTENABLED()
	 * @see #unsetISREDUCEDRESULTENABLED()
	 * @see #isISREDUCEDRESULTENABLED()
	 * @generated
	 */
	void setISREDUCEDRESULTENABLED(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISREDUCEDRESULTENABLED()
	 * @see #isISREDUCEDRESULTENABLED()
	 * @see #setISREDUCEDRESULTENABLED(boolean)
	 * @generated
	 */
	void unsetISREDUCEDRESULTENABLED();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MULTIPLEECUJOB#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISREDUCEDRESULTENABLED</em>' attribute is set.
	 * @see #unsetISREDUCEDRESULTENABLED()
	 * @see #isISREDUCEDRESULTENABLED()
	 * @see #setISREDUCEDRESULTENABLED(boolean)
	 * @generated
	 */
	boolean isSetISREDUCEDRESULTENABLED();

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

	/**
	 * Returns the value of the '<em><b>SECURITYACCESSLEVEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SECURITYACCESSLEVEL</em>' attribute.
	 * @see #isSetSECURITYACCESSLEVEL()
	 * @see #unsetSECURITYACCESSLEVEL()
	 * @see #setSECURITYACCESSLEVEL(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_SECURITYACCESSLEVEL()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='attribute' name='SECURITY-ACCESS-LEVEL' namespace='##targetNamespace'"
	 * @generated
	 */
	long getSECURITYACCESSLEVEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SECURITYACCESSLEVEL</em>' attribute.
	 * @see #isSetSECURITYACCESSLEVEL()
	 * @see #unsetSECURITYACCESSLEVEL()
	 * @see #getSECURITYACCESSLEVEL()
	 * @generated
	 */
	void setSECURITYACCESSLEVEL(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSECURITYACCESSLEVEL()
	 * @see #getSECURITYACCESSLEVEL()
	 * @see #setSECURITYACCESSLEVEL(long)
	 * @generated
	 */
	void unsetSECURITYACCESSLEVEL();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>SECURITYACCESSLEVEL</em>' attribute is set.
	 * @see #unsetSECURITYACCESSLEVEL()
	 * @see #getSECURITYACCESSLEVEL()
	 * @see #setSECURITYACCESSLEVEL(long)
	 * @generated
	 */
	boolean isSetSECURITYACCESSLEVEL();

	/**
	 * Returns the value of the '<em><b>SEMANTIC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SEMANTIC</em>' attribute.
	 * @see #setSEMANTIC(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOB_SEMANTIC()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='SEMANTIC' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSEMANTIC();

	/**
	 * Sets the value of the '{@link OdxXhtml.MULTIPLEECUJOB#getSEMANTIC <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SEMANTIC</em>' attribute.
	 * @see #getSEMANTIC()
	 * @generated
	 */
	void setSEMANTIC(String value);

} // MULTIPLEECUJOB
