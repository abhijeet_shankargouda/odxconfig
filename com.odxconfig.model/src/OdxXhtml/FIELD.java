/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FIELD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FIELD#getBASICSTRUCTUREREF <em>BASICSTRUCTUREREF</em>}</li>
 *   <li>{@link OdxXhtml.FIELD#getENVDATADESCREF <em>ENVDATADESCREF</em>}</li>
 *   <li>{@link OdxXhtml.FIELD#isISVISIBLE <em>ISVISIBLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFIELD()
 * @model extendedMetaData="name='FIELD' kind='elementOnly'"
 * @generated
 */
public interface FIELD extends COMPLEXDOP {
	/**
	 * Returns the value of the '<em><b>BASICSTRUCTUREREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BASICSTRUCTUREREF</em>' containment reference.
	 * @see #setBASICSTRUCTUREREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getFIELD_BASICSTRUCTUREREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='BASIC-STRUCTURE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getBASICSTRUCTUREREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.FIELD#getBASICSTRUCTUREREF <em>BASICSTRUCTUREREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BASICSTRUCTUREREF</em>' containment reference.
	 * @see #getBASICSTRUCTUREREF()
	 * @generated
	 */
	void setBASICSTRUCTUREREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>ENVDATADESCREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENVDATADESCREF</em>' containment reference.
	 * @see #setENVDATADESCREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getFIELD_ENVDATADESCREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ENV-DATA-DESC-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getENVDATADESCREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.FIELD#getENVDATADESCREF <em>ENVDATADESCREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ENVDATADESCREF</em>' containment reference.
	 * @see #getENVDATADESCREF()
	 * @generated
	 */
	void setENVDATADESCREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>ISVISIBLE</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISVISIBLE</em>' attribute.
	 * @see #isSetISVISIBLE()
	 * @see #unsetISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getFIELD_ISVISIBLE()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-VISIBLE' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISVISIBLE();

	/**
	 * Sets the value of the '{@link OdxXhtml.FIELD#isISVISIBLE <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISVISIBLE</em>' attribute.
	 * @see #isSetISVISIBLE()
	 * @see #unsetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @generated
	 */
	void setISVISIBLE(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.FIELD#isISVISIBLE <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @generated
	 */
	void unsetISVISIBLE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.FIELD#isISVISIBLE <em>ISVISIBLE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISVISIBLE</em>' attribute is set.
	 * @see #unsetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @generated
	 */
	boolean isSetISVISIBLE();

} // FIELD
