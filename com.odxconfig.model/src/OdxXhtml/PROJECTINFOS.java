/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROJECTINFOS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PROJECTINFOS#getPROJECTINFO <em>PROJECTINFO</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTINFOS()
 * @model extendedMetaData="name='PROJECT-INFOS' kind='elementOnly'"
 * @generated
 */
public interface PROJECTINFOS extends EObject {
	/**
	 * Returns the value of the '<em><b>PROJECTINFO</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.PROJECTINFO}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROJECTINFO</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTINFOS_PROJECTINFO()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PROJECT-INFO' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PROJECTINFO> getPROJECTINFO();

} // PROJECTINFOS
