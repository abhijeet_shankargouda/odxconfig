/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SIZEDEFPHYSSEGMENT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SIZEDEFPHYSSEGMENT#getSIZE <em>SIZE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSIZEDEFPHYSSEGMENT()
 * @model extendedMetaData="name='SIZEDEF-PHYS-SEGMENT' kind='elementOnly'"
 * @generated
 */
public interface SIZEDEFPHYSSEGMENT extends PHYSSEGMENT {
	/**
	 * Returns the value of the '<em><b>SIZE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SIZE</em>' attribute.
	 * @see #isSetSIZE()
	 * @see #unsetSIZE()
	 * @see #setSIZE(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getSIZEDEFPHYSSEGMENT_SIZE()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='SIZE' namespace='##targetNamespace'"
	 * @generated
	 */
	long getSIZE();

	/**
	 * Sets the value of the '{@link OdxXhtml.SIZEDEFPHYSSEGMENT#getSIZE <em>SIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SIZE</em>' attribute.
	 * @see #isSetSIZE()
	 * @see #unsetSIZE()
	 * @see #getSIZE()
	 * @generated
	 */
	void setSIZE(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.SIZEDEFPHYSSEGMENT#getSIZE <em>SIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSIZE()
	 * @see #getSIZE()
	 * @see #setSIZE(long)
	 * @generated
	 */
	void unsetSIZE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.SIZEDEFPHYSSEGMENT#getSIZE <em>SIZE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>SIZE</em>' attribute is set.
	 * @see #unsetSIZE()
	 * @see #getSIZE()
	 * @see #setSIZE(long)
	 * @generated
	 */
	boolean isSetSIZE();

} // SIZEDEFPHYSSEGMENT
