/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLECONNECTORPINS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORPINS#getVEHICLECONNECTORPIN <em>VEHICLECONNECTORPIN</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPINS()
 * @model extendedMetaData="name='VEHICLE-CONNECTOR-PINS' kind='elementOnly'"
 * @generated
 */
public interface VEHICLECONNECTORPINS extends EObject {
	/**
	 * Returns the value of the '<em><b>VEHICLECONNECTORPIN</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.VEHICLECONNECTORPIN}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VEHICLECONNECTORPIN</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPINS_VEHICLECONNECTORPIN()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='VEHICLE-CONNECTOR-PIN' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<VEHICLECONNECTORPIN> getVEHICLECONNECTORPIN();

} // VEHICLECONNECTORPINS
