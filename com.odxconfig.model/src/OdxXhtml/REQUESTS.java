/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>REQUESTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.REQUESTS#getREQUEST <em>REQUEST</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getREQUESTS()
 * @model extendedMetaData="name='REQUESTS' kind='elementOnly'"
 * @generated
 */
public interface REQUESTS extends EObject {
	/**
	 * Returns the value of the '<em><b>REQUEST</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.REQUEST}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REQUEST</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getREQUESTS_REQUEST()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='REQUEST' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<REQUEST> getREQUEST();

} // REQUESTS
