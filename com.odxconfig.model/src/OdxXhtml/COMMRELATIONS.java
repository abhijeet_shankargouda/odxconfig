/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMMRELATIONS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMMRELATIONS#getCOMMRELATION <em>COMMRELATION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATIONS()
 * @model extendedMetaData="name='COMM-RELATIONS' kind='elementOnly'"
 * @generated
 */
public interface COMMRELATIONS extends EObject {
	/**
	 * Returns the value of the '<em><b>COMMRELATION</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.COMMRELATION}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMMRELATION</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATIONS_COMMRELATION()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMM-RELATION' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<COMMRELATION> getCOMMRELATION();

} // COMMRELATIONS
