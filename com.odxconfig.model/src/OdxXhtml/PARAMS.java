/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PARAMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PARAMS#getPARAM <em>PARAM</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPARAMS()
 * @model extendedMetaData="name='PARAMS' kind='elementOnly'"
 * @generated
 */
public interface PARAMS extends EObject {
	/**
	 * Returns the value of the '<em><b>PARAM</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.PARAM}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PARAM</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPARAMS_PARAM()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PARAM' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PARAM> getPARAM();

} // PARAMS
