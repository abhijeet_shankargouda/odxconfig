/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SWVARIABLES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SWVARIABLES#getSWVARIABLE <em>SWVARIABLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSWVARIABLES()
 * @model extendedMetaData="name='SW-VARIABLES' kind='elementOnly'"
 * @generated
 */
public interface SWVARIABLES extends EObject {
	/**
	 * Returns the value of the '<em><b>SWVARIABLE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SWVARIABLE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SWVARIABLE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSWVARIABLES_SWVARIABLE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SW-VARIABLE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SWVARIABLE> getSWVARIABLE();

} // SWVARIABLES
