/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGCOMM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGCOMM#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getCOMPARAMREFS <em>COMPARAMREFS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getFUNCTCLASSREFS <em>FUNCTCLASSREFS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getAUDIENCE <em>AUDIENCE</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getPROTOCOLSNREFS <em>PROTOCOLSNREFS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getRELATEDDIAGCOMMREFS <em>RELATEDDIAGCOMMREFS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getDIAGNOSTICCLASS <em>DIAGNOSTICCLASS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#isISEXECUTABLE <em>ISEXECUTABLE</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#isISFINAL <em>ISFINAL</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#isISMANDATORY <em>ISMANDATORY</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}</li>
 *   <li>{@link OdxXhtml.DIAGCOMM#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM()
 * @model extendedMetaData="name='DIAG-COMM' kind='elementOnly'"
 * @generated
 */
public interface DIAGCOMM extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA value);

	/**
	 * Returns the value of the '<em><b>SDGS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGS</em>' containment reference.
	 * @see #setSDGS(SDGS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_SDGS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDGS' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGS getSDGS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getSDGS <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGS</em>' containment reference.
	 * @see #getSDGS()
	 * @generated
	 */
	void setSDGS(SDGS value);

	/**
	 * Returns the value of the '<em><b>COMPARAMREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPARAMREFS</em>' containment reference.
	 * @see #setCOMPARAMREFS(COMPARAMREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_COMPARAMREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPARAM-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPARAMREFS getCOMPARAMREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getCOMPARAMREFS <em>COMPARAMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPARAMREFS</em>' containment reference.
	 * @see #getCOMPARAMREFS()
	 * @generated
	 */
	void setCOMPARAMREFS(COMPARAMREFS value);

	/**
	 * Returns the value of the '<em><b>FUNCTCLASSREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FUNCTCLASSREFS</em>' containment reference.
	 * @see #setFUNCTCLASSREFS(FUNCTCLASSREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_FUNCTCLASSREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FUNCT-CLASS-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	FUNCTCLASSREFS getFUNCTCLASSREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getFUNCTCLASSREFS <em>FUNCTCLASSREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FUNCTCLASSREFS</em>' containment reference.
	 * @see #getFUNCTCLASSREFS()
	 * @generated
	 */
	void setFUNCTCLASSREFS(FUNCTCLASSREFS value);

	/**
	 * Returns the value of the '<em><b>AUDIENCE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUDIENCE</em>' containment reference.
	 * @see #setAUDIENCE(AUDIENCE)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_AUDIENCE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='AUDIENCE' namespace='##targetNamespace'"
	 * @generated
	 */
	AUDIENCE getAUDIENCE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getAUDIENCE <em>AUDIENCE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AUDIENCE</em>' containment reference.
	 * @see #getAUDIENCE()
	 * @generated
	 */
	void setAUDIENCE(AUDIENCE value);

	/**
	 * Returns the value of the '<em><b>PROTOCOLSNREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROTOCOLSNREFS</em>' containment reference.
	 * @see #setPROTOCOLSNREFS(PROTOCOLSNREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_PROTOCOLSNREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PROTOCOL-SNREFS' namespace='##targetNamespace'"
	 * @generated
	 */
	PROTOCOLSNREFS getPROTOCOLSNREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getPROTOCOLSNREFS <em>PROTOCOLSNREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROTOCOLSNREFS</em>' containment reference.
	 * @see #getPROTOCOLSNREFS()
	 * @generated
	 */
	void setPROTOCOLSNREFS(PROTOCOLSNREFS value);

	/**
	 * Returns the value of the '<em><b>RELATEDDIAGCOMMREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RELATEDDIAGCOMMREFS</em>' containment reference.
	 * @see #setRELATEDDIAGCOMMREFS(RELATEDDIAGCOMMREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_RELATEDDIAGCOMMREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='RELATED-DIAG-COMM-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	RELATEDDIAGCOMMREFS getRELATEDDIAGCOMMREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getRELATEDDIAGCOMMREFS <em>RELATEDDIAGCOMMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RELATEDDIAGCOMMREFS</em>' containment reference.
	 * @see #getRELATEDDIAGCOMMREFS()
	 * @generated
	 */
	void setRELATEDDIAGCOMMREFS(RELATEDDIAGCOMMREFS value);

	/**
	 * Returns the value of the '<em><b>DIAGNOSTICCLASS</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.DIAGCLASSTYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGNOSTICCLASS</em>' attribute.
	 * @see OdxXhtml.DIAGCLASSTYPE
	 * @see #isSetDIAGNOSTICCLASS()
	 * @see #unsetDIAGNOSTICCLASS()
	 * @see #setDIAGNOSTICCLASS(DIAGCLASSTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_DIAGNOSTICCLASS()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='DIAGNOSTIC-CLASS' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGCLASSTYPE getDIAGNOSTICCLASS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getDIAGNOSTICCLASS <em>DIAGNOSTICCLASS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGNOSTICCLASS</em>' attribute.
	 * @see OdxXhtml.DIAGCLASSTYPE
	 * @see #isSetDIAGNOSTICCLASS()
	 * @see #unsetDIAGNOSTICCLASS()
	 * @see #getDIAGNOSTICCLASS()
	 * @generated
	 */
	void setDIAGNOSTICCLASS(DIAGCLASSTYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGCOMM#getDIAGNOSTICCLASS <em>DIAGNOSTICCLASS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDIAGNOSTICCLASS()
	 * @see #getDIAGNOSTICCLASS()
	 * @see #setDIAGNOSTICCLASS(DIAGCLASSTYPE)
	 * @generated
	 */
	void unsetDIAGNOSTICCLASS();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGCOMM#getDIAGNOSTICCLASS <em>DIAGNOSTICCLASS</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>DIAGNOSTICCLASS</em>' attribute is set.
	 * @see #unsetDIAGNOSTICCLASS()
	 * @see #getDIAGNOSTICCLASS()
	 * @see #setDIAGNOSTICCLASS(DIAGCLASSTYPE)
	 * @generated
	 */
	boolean isSetDIAGNOSTICCLASS();

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>ISEXECUTABLE</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISEXECUTABLE</em>' attribute.
	 * @see #isSetISEXECUTABLE()
	 * @see #unsetISEXECUTABLE()
	 * @see #setISEXECUTABLE(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_ISEXECUTABLE()
	 * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-EXECUTABLE' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISEXECUTABLE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#isISEXECUTABLE <em>ISEXECUTABLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISEXECUTABLE</em>' attribute.
	 * @see #isSetISEXECUTABLE()
	 * @see #unsetISEXECUTABLE()
	 * @see #isISEXECUTABLE()
	 * @generated
	 */
	void setISEXECUTABLE(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGCOMM#isISEXECUTABLE <em>ISEXECUTABLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISEXECUTABLE()
	 * @see #isISEXECUTABLE()
	 * @see #setISEXECUTABLE(boolean)
	 * @generated
	 */
	void unsetISEXECUTABLE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGCOMM#isISEXECUTABLE <em>ISEXECUTABLE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISEXECUTABLE</em>' attribute is set.
	 * @see #unsetISEXECUTABLE()
	 * @see #isISEXECUTABLE()
	 * @see #setISEXECUTABLE(boolean)
	 * @generated
	 */
	boolean isSetISEXECUTABLE();

	/**
	 * Returns the value of the '<em><b>ISFINAL</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISFINAL</em>' attribute.
	 * @see #isSetISFINAL()
	 * @see #unsetISFINAL()
	 * @see #setISFINAL(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_ISFINAL()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-FINAL' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISFINAL();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#isISFINAL <em>ISFINAL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISFINAL</em>' attribute.
	 * @see #isSetISFINAL()
	 * @see #unsetISFINAL()
	 * @see #isISFINAL()
	 * @generated
	 */
	void setISFINAL(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGCOMM#isISFINAL <em>ISFINAL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISFINAL()
	 * @see #isISFINAL()
	 * @see #setISFINAL(boolean)
	 * @generated
	 */
	void unsetISFINAL();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGCOMM#isISFINAL <em>ISFINAL</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISFINAL</em>' attribute is set.
	 * @see #unsetISFINAL()
	 * @see #isISFINAL()
	 * @see #setISFINAL(boolean)
	 * @generated
	 */
	boolean isSetISFINAL();

	/**
	 * Returns the value of the '<em><b>ISMANDATORY</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISMANDATORY</em>' attribute.
	 * @see #isSetISMANDATORY()
	 * @see #unsetISMANDATORY()
	 * @see #setISMANDATORY(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_ISMANDATORY()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-MANDATORY' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISMANDATORY();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#isISMANDATORY <em>ISMANDATORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISMANDATORY</em>' attribute.
	 * @see #isSetISMANDATORY()
	 * @see #unsetISMANDATORY()
	 * @see #isISMANDATORY()
	 * @generated
	 */
	void setISMANDATORY(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGCOMM#isISMANDATORY <em>ISMANDATORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISMANDATORY()
	 * @see #isISMANDATORY()
	 * @see #setISMANDATORY(boolean)
	 * @generated
	 */
	void unsetISMANDATORY();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGCOMM#isISMANDATORY <em>ISMANDATORY</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISMANDATORY</em>' attribute is set.
	 * @see #unsetISMANDATORY()
	 * @see #isISMANDATORY()
	 * @see #setISMANDATORY(boolean)
	 * @generated
	 */
	boolean isSetISMANDATORY();

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

	/**
	 * Returns the value of the '<em><b>SECURITYACCESSLEVEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SECURITYACCESSLEVEL</em>' attribute.
	 * @see #isSetSECURITYACCESSLEVEL()
	 * @see #unsetSECURITYACCESSLEVEL()
	 * @see #setSECURITYACCESSLEVEL(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_SECURITYACCESSLEVEL()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='attribute' name='SECURITY-ACCESS-LEVEL' namespace='##targetNamespace'"
	 * @generated
	 */
	long getSECURITYACCESSLEVEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SECURITYACCESSLEVEL</em>' attribute.
	 * @see #isSetSECURITYACCESSLEVEL()
	 * @see #unsetSECURITYACCESSLEVEL()
	 * @see #getSECURITYACCESSLEVEL()
	 * @generated
	 */
	void setSECURITYACCESSLEVEL(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGCOMM#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSECURITYACCESSLEVEL()
	 * @see #getSECURITYACCESSLEVEL()
	 * @see #setSECURITYACCESSLEVEL(long)
	 * @generated
	 */
	void unsetSECURITYACCESSLEVEL();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGCOMM#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>SECURITYACCESSLEVEL</em>' attribute is set.
	 * @see #unsetSECURITYACCESSLEVEL()
	 * @see #getSECURITYACCESSLEVEL()
	 * @see #setSECURITYACCESSLEVEL(long)
	 * @generated
	 */
	boolean isSetSECURITYACCESSLEVEL();

	/**
	 * Returns the value of the '<em><b>SEMANTIC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SEMANTIC</em>' attribute.
	 * @see #setSEMANTIC(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGCOMM_SEMANTIC()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='SEMANTIC' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSEMANTIC();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGCOMM#getSEMANTIC <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SEMANTIC</em>' attribute.
	 * @see #getSEMANTIC()
	 * @generated
	 */
	void setSEMANTIC(String value);

} // DIAGCOMM
