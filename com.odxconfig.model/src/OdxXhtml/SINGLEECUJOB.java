/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SINGLEECUJOB</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SINGLEECUJOB#getPROGCODES <em>PROGCODES</em>}</li>
 *   <li>{@link OdxXhtml.SINGLEECUJOB#getINPUTPARAMS <em>INPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.SINGLEECUJOB#getOUTPUTPARAMS <em>OUTPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.SINGLEECUJOB#getNEGOUTPUTPARAMS <em>NEGOUTPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.SINGLEECUJOB#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSINGLEECUJOB()
 * @model extendedMetaData="name='SINGLE-ECU-JOB' kind='elementOnly'"
 * @generated
 */
public interface SINGLEECUJOB extends DIAGCOMM {
	/**
	 * Returns the value of the '<em><b>PROGCODES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROGCODES</em>' containment reference.
	 * @see #setPROGCODES(PROGCODES)
	 * @see OdxXhtml.OdxXhtmlPackage#getSINGLEECUJOB_PROGCODES()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PROG-CODES' namespace='##targetNamespace'"
	 * @generated
	 */
	PROGCODES getPROGCODES();

	/**
	 * Sets the value of the '{@link OdxXhtml.SINGLEECUJOB#getPROGCODES <em>PROGCODES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROGCODES</em>' containment reference.
	 * @see #getPROGCODES()
	 * @generated
	 */
	void setPROGCODES(PROGCODES value);

	/**
	 * Returns the value of the '<em><b>INPUTPARAMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INPUTPARAMS</em>' containment reference.
	 * @see #setINPUTPARAMS(INPUTPARAMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSINGLEECUJOB_INPUTPARAMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='INPUT-PARAMS' namespace='##targetNamespace'"
	 * @generated
	 */
	INPUTPARAMS getINPUTPARAMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SINGLEECUJOB#getINPUTPARAMS <em>INPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>INPUTPARAMS</em>' containment reference.
	 * @see #getINPUTPARAMS()
	 * @generated
	 */
	void setINPUTPARAMS(INPUTPARAMS value);

	/**
	 * Returns the value of the '<em><b>OUTPUTPARAMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OUTPUTPARAMS</em>' containment reference.
	 * @see #setOUTPUTPARAMS(OUTPUTPARAMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSINGLEECUJOB_OUTPUTPARAMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OUTPUT-PARAMS' namespace='##targetNamespace'"
	 * @generated
	 */
	OUTPUTPARAMS getOUTPUTPARAMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SINGLEECUJOB#getOUTPUTPARAMS <em>OUTPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OUTPUTPARAMS</em>' containment reference.
	 * @see #getOUTPUTPARAMS()
	 * @generated
	 */
	void setOUTPUTPARAMS(OUTPUTPARAMS value);

	/**
	 * Returns the value of the '<em><b>NEGOUTPUTPARAMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NEGOUTPUTPARAMS</em>' containment reference.
	 * @see #setNEGOUTPUTPARAMS(NEGOUTPUTPARAMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSINGLEECUJOB_NEGOUTPUTPARAMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='NEG-OUTPUT-PARAMS' namespace='##targetNamespace'"
	 * @generated
	 */
	NEGOUTPUTPARAMS getNEGOUTPUTPARAMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SINGLEECUJOB#getNEGOUTPUTPARAMS <em>NEGOUTPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NEGOUTPUTPARAMS</em>' containment reference.
	 * @see #getNEGOUTPUTPARAMS()
	 * @generated
	 */
	void setNEGOUTPUTPARAMS(NEGOUTPUTPARAMS value);

	/**
	 * Returns the value of the '<em><b>ISREDUCEDRESULTENABLED</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISREDUCEDRESULTENABLED</em>' attribute.
	 * @see #isSetISREDUCEDRESULTENABLED()
	 * @see #unsetISREDUCEDRESULTENABLED()
	 * @see #setISREDUCEDRESULTENABLED(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getSINGLEECUJOB_ISREDUCEDRESULTENABLED()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-REDUCED-RESULT-ENABLED' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISREDUCEDRESULTENABLED();

	/**
	 * Sets the value of the '{@link OdxXhtml.SINGLEECUJOB#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISREDUCEDRESULTENABLED</em>' attribute.
	 * @see #isSetISREDUCEDRESULTENABLED()
	 * @see #unsetISREDUCEDRESULTENABLED()
	 * @see #isISREDUCEDRESULTENABLED()
	 * @generated
	 */
	void setISREDUCEDRESULTENABLED(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.SINGLEECUJOB#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISREDUCEDRESULTENABLED()
	 * @see #isISREDUCEDRESULTENABLED()
	 * @see #setISREDUCEDRESULTENABLED(boolean)
	 * @generated
	 */
	void unsetISREDUCEDRESULTENABLED();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.SINGLEECUJOB#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISREDUCEDRESULTENABLED</em>' attribute is set.
	 * @see #unsetISREDUCEDRESULTENABLED()
	 * @see #isISREDUCEDRESULTENABLED()
	 * @see #setISREDUCEDRESULTENABLED(boolean)
	 * @generated
	 */
	boolean isSetISREDUCEDRESULTENABLED();

} // SINGLEECUJOB
