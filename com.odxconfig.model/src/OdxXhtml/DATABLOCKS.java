/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DATABLOCKS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DATABLOCKS#getDATABLOCK <em>DATABLOCK</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCKS()
 * @model extendedMetaData="name='DATABLOCKS' kind='elementOnly'"
 * @generated
 */
public interface DATABLOCKS extends EObject {
	/**
	 * Returns the value of the '<em><b>DATABLOCK</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DATABLOCK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATABLOCK</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCKS_DATABLOCK()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATABLOCK' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DATABLOCK> getDATABLOCK();

} // DATABLOCKS
