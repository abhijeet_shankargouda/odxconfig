/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGVARIABLES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGVARIABLES#getDIAGVARIABLEPROXY <em>DIAGVARIABLEPROXY</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLES#getDIAGVARIABLEREF <em>DIAGVARIABLEREF</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLES#getDIAGVARIABLE <em>DIAGVARIABLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLES()
 * @model extendedMetaData="name='DIAG-VARIABLES' kind='elementOnly'"
 * @generated
 */
public interface DIAGVARIABLES extends EObject {
	/**
	 * Returns the value of the '<em><b>DIAGVARIABLEPROXY</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGVARIABLEPROXY</em>' attribute list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLES_DIAGVARIABLEPROXY()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='DIAGVARIABLEPROXY:0'"
	 * @generated
	 */
	FeatureMap getDIAGVARIABLEPROXY();

	/**
	 * Returns the value of the '<em><b>DIAGVARIABLEREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGVARIABLEREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLES_DIAGVARIABLEREF()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DIAG-VARIABLE-REF' namespace='##targetNamespace' group='DIAGVARIABLEPROXY:0'"
	 * @generated
	 */
	EList<ODXLINK> getDIAGVARIABLEREF();

	/**
	 * Returns the value of the '<em><b>DIAGVARIABLE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DIAGVARIABLE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGVARIABLE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLES_DIAGVARIABLE()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DIAG-VARIABLE' namespace='##targetNamespace' group='DIAGVARIABLEPROXY:0'"
	 * @generated
	 */
	EList<DIAGVARIABLE> getDIAGVARIABLE();

} // DIAGVARIABLES
