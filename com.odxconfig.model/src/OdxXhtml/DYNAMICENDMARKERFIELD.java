/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DYNAMICENDMARKERFIELD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DYNAMICENDMARKERFIELD#getDATAOBJECTPROPREF <em>DATAOBJECTPROPREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMICENDMARKERFIELD()
 * @model extendedMetaData="name='DYNAMIC-ENDMARKER-FIELD' kind='elementOnly'"
 * @generated
 */
public interface DYNAMICENDMARKERFIELD extends FIELD {
	/**
	 * Returns the value of the '<em><b>DATAOBJECTPROPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATAOBJECTPROPREF</em>' containment reference.
	 * @see #setDATAOBJECTPROPREF(DYNENDDOPREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMICENDMARKERFIELD_DATAOBJECTPROPREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATA-OBJECT-PROP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	DYNENDDOPREF getDATAOBJECTPROPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNAMICENDMARKERFIELD#getDATAOBJECTPROPREF <em>DATAOBJECTPROPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATAOBJECTPROPREF</em>' containment reference.
	 * @see #getDATAOBJECTPROPREF()
	 * @generated
	 */
	void setDATAOBJECTPROPREF(DYNENDDOPREF value);

} // DYNAMICENDMARKERFIELD
