/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ENVDATADESCS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ENVDATADESCS#getENVDATADESC <em>ENVDATADESC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getENVDATADESCS()
 * @model extendedMetaData="name='ENV-DATA-DESCS' kind='elementOnly'"
 * @generated
 */
public interface ENVDATADESCS extends EObject {
	/**
	 * Returns the value of the '<em><b>ENVDATADESC</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ENVDATADESC}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENVDATADESC</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getENVDATADESCS_ENVDATADESC()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ENV-DATA-DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ENVDATADESC> getENVDATADESC();

} // ENVDATADESCS
