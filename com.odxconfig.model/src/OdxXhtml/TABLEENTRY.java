/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TABLEENTRY</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.TABLEENTRY#getTARGET <em>TARGET</em>}</li>
 *   <li>{@link OdxXhtml.TABLEENTRY#getTABLEROWREF <em>TABLEROWREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getTABLEENTRY()
 * @model extendedMetaData="name='TABLE-ENTRY' kind='elementOnly'"
 * @generated
 */
public interface TABLEENTRY extends PARAM {
	/**
	 * Returns the value of the '<em><b>TARGET</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.ROWFRAGMENT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TARGET</em>' attribute.
	 * @see OdxXhtml.ROWFRAGMENT
	 * @see #isSetTARGET()
	 * @see #unsetTARGET()
	 * @see #setTARGET(ROWFRAGMENT)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLEENTRY_TARGET()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='element' name='TARGET' namespace='##targetNamespace'"
	 * @generated
	 */
	ROWFRAGMENT getTARGET();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLEENTRY#getTARGET <em>TARGET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TARGET</em>' attribute.
	 * @see OdxXhtml.ROWFRAGMENT
	 * @see #isSetTARGET()
	 * @see #unsetTARGET()
	 * @see #getTARGET()
	 * @generated
	 */
	void setTARGET(ROWFRAGMENT value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.TABLEENTRY#getTARGET <em>TARGET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTARGET()
	 * @see #getTARGET()
	 * @see #setTARGET(ROWFRAGMENT)
	 * @generated
	 */
	void unsetTARGET();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.TABLEENTRY#getTARGET <em>TARGET</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TARGET</em>' attribute is set.
	 * @see #unsetTARGET()
	 * @see #getTARGET()
	 * @see #setTARGET(ROWFRAGMENT)
	 * @generated
	 */
	boolean isSetTARGET();

	/**
	 * Returns the value of the '<em><b>TABLEROWREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TABLEROWREF</em>' containment reference.
	 * @see #setTABLEROWREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLEENTRY_TABLEROWREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='TABLE-ROW-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getTABLEROWREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLEENTRY#getTABLEROWREF <em>TABLEROWREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TABLEROWREF</em>' containment reference.
	 * @see #getTABLEROWREF()
	 * @generated
	 */
	void setTABLEROWREF(ODXLINK value);

} // TABLEENTRY
