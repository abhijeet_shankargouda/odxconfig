/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MEM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MEM#getSESSIONS <em>SESSIONS</em>}</li>
 *   <li>{@link OdxXhtml.MEM#getDATABLOCKS <em>DATABLOCKS</em>}</li>
 *   <li>{@link OdxXhtml.MEM#getFLASHDATAS <em>FLASHDATAS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMEM()
 * @model extendedMetaData="name='MEM' kind='elementOnly'"
 * @generated
 */
public interface MEM extends EObject {
	/**
	 * Returns the value of the '<em><b>SESSIONS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SESSIONS</em>' containment reference.
	 * @see #setSESSIONS(SESSIONS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMEM_SESSIONS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SESSIONS' namespace='##targetNamespace'"
	 * @generated
	 */
	SESSIONS getSESSIONS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MEM#getSESSIONS <em>SESSIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SESSIONS</em>' containment reference.
	 * @see #getSESSIONS()
	 * @generated
	 */
	void setSESSIONS(SESSIONS value);

	/**
	 * Returns the value of the '<em><b>DATABLOCKS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATABLOCKS</em>' containment reference.
	 * @see #setDATABLOCKS(DATABLOCKS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMEM_DATABLOCKS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATABLOCKS' namespace='##targetNamespace'"
	 * @generated
	 */
	DATABLOCKS getDATABLOCKS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MEM#getDATABLOCKS <em>DATABLOCKS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATABLOCKS</em>' containment reference.
	 * @see #getDATABLOCKS()
	 * @generated
	 */
	void setDATABLOCKS(DATABLOCKS value);

	/**
	 * Returns the value of the '<em><b>FLASHDATAS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FLASHDATAS</em>' containment reference.
	 * @see #setFLASHDATAS(FLASHDATAS)
	 * @see OdxXhtml.OdxXhtmlPackage#getMEM_FLASHDATAS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FLASHDATAS' namespace='##targetNamespace'"
	 * @generated
	 */
	FLASHDATAS getFLASHDATAS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MEM#getFLASHDATAS <em>FLASHDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FLASHDATAS</em>' containment reference.
	 * @see #getFLASHDATAS()
	 * @generated
	 */
	void setFLASHDATAS(FLASHDATAS value);

} // MEM
