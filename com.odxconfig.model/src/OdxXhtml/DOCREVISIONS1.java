/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DOCREVISIONS1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DOCREVISIONS1#getDOCREVISION <em>DOCREVISION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISIONS1()
 * @model extendedMetaData="name='DOC-REVISIONS' kind='elementOnly'"
 * @generated
 */
public interface DOCREVISIONS1 extends EObject {
	/**
	 * Returns the value of the '<em><b>DOCREVISION</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DOCREVISION1}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCREVISION</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDOCREVISIONS1_DOCREVISION()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DOC-REVISION' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DOCREVISION1> getDOCREVISION();

} // DOCREVISIONS1
