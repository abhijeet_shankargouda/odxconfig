/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ROWFRAGMENT</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getROWFRAGMENT()
 * @model extendedMetaData="name='ROW-FRAGMENT'"
 * @generated
 */
public enum ROWFRAGMENT implements Enumerator {
	/**
	 * The '<em><b>KEY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #KEY_VALUE
	 * @generated
	 * @ordered
	 */
	KEY(0, "KEY", "KEY"),

	/**
	 * The '<em><b>STRUCT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STRUCT_VALUE
	 * @generated
	 * @ordered
	 */
	STRUCT(1, "STRUCT", "STRUCT"),

	/**
	 * The '<em><b>KEYANDSTRUCT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #KEYANDSTRUCT_VALUE
	 * @generated
	 * @ordered
	 */
	KEYANDSTRUCT(2, "KEYANDSTRUCT", "KEY-AND-STRUCT");

	/**
	 * The '<em><b>KEY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #KEY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KEY_VALUE = 0;

	/**
	 * The '<em><b>STRUCT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STRUCT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STRUCT_VALUE = 1;

	/**
	 * The '<em><b>KEYANDSTRUCT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #KEYANDSTRUCT
	 * @model literal="KEY-AND-STRUCT"
	 * @generated
	 * @ordered
	 */
	public static final int KEYANDSTRUCT_VALUE = 2;

	/**
	 * An array of all the '<em><b>ROWFRAGMENT</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ROWFRAGMENT[] VALUES_ARRAY =
		new ROWFRAGMENT[] {
			KEY,
			STRUCT,
			KEYANDSTRUCT,
		};

	/**
	 * A public read-only list of all the '<em><b>ROWFRAGMENT</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ROWFRAGMENT> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ROWFRAGMENT</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ROWFRAGMENT get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ROWFRAGMENT result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ROWFRAGMENT</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ROWFRAGMENT getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ROWFRAGMENT result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ROWFRAGMENT</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ROWFRAGMENT get(int value) {
		switch (value) {
			case KEY_VALUE: return KEY;
			case STRUCT_VALUE: return STRUCT;
			case KEYANDSTRUCT_VALUE: return KEYANDSTRUCT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ROWFRAGMENT(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ROWFRAGMENT
