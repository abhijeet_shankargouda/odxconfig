/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPURATIONALCOEFFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPURATIONALCOEFFS#getCOMPUNUMERATOR <em>COMPUNUMERATOR</em>}</li>
 *   <li>{@link OdxXhtml.COMPURATIONALCOEFFS#getCOMPUDENOMINATOR <em>COMPUDENOMINATOR</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPURATIONALCOEFFS()
 * @model extendedMetaData="name='COMPU-RATIONAL-COEFFS' kind='elementOnly'"
 * @generated
 */
public interface COMPURATIONALCOEFFS extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPUNUMERATOR</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUNUMERATOR</em>' containment reference.
	 * @see #setCOMPUNUMERATOR(COMPUNUMERATOR)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPURATIONALCOEFFS_COMPUNUMERATOR()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPU-NUMERATOR' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUNUMERATOR getCOMPUNUMERATOR();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPURATIONALCOEFFS#getCOMPUNUMERATOR <em>COMPUNUMERATOR</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUNUMERATOR</em>' containment reference.
	 * @see #getCOMPUNUMERATOR()
	 * @generated
	 */
	void setCOMPUNUMERATOR(COMPUNUMERATOR value);

	/**
	 * Returns the value of the '<em><b>COMPUDENOMINATOR</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUDENOMINATOR</em>' containment reference.
	 * @see #setCOMPUDENOMINATOR(COMPUDENOMINATOR)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPURATIONALCOEFFS_COMPUDENOMINATOR()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPU-DENOMINATOR' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUDENOMINATOR getCOMPUDENOMINATOR();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPURATIONALCOEFFS#getCOMPUDENOMINATOR <em>COMPUDENOMINATOR</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUDENOMINATOR</em>' containment reference.
	 * @see #getCOMPUDENOMINATOR()
	 * @generated
	 */
	void setCOMPUDENOMINATOR(COMPUDENOMINATOR value);

} // COMPURATIONALCOEFFS
