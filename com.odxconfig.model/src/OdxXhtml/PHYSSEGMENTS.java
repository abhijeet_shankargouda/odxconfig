/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PHYSSEGMENTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PHYSSEGMENTS#getPHYSSEGMENT <em>PHYSSEGMENT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENTS()
 * @model extendedMetaData="name='PHYS-SEGMENTS' kind='elementOnly'"
 * @generated
 */
public interface PHYSSEGMENTS extends EObject {
	/**
	 * Returns the value of the '<em><b>PHYSSEGMENT</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.PHYSSEGMENT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSSEGMENT</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSSEGMENTS_PHYSSEGMENT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PHYS-SEGMENT' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PHYSSEGMENT> getPHYSSEGMENT();

} // PHYSSEGMENTS
