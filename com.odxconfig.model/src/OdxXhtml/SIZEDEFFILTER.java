/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SIZEDEFFILTER</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SIZEDEFFILTER#getFILTERSIZE <em>FILTERSIZE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSIZEDEFFILTER()
 * @model extendedMetaData="name='SIZEDEF-FILTER' kind='elementOnly'"
 * @generated
 */
public interface SIZEDEFFILTER extends FILTER {
	/**
	 * Returns the value of the '<em><b>FILTERSIZE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FILTERSIZE</em>' attribute.
	 * @see #isSetFILTERSIZE()
	 * @see #unsetFILTERSIZE()
	 * @see #setFILTERSIZE(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getSIZEDEFFILTER_FILTERSIZE()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='FILTER-SIZE' namespace='##targetNamespace'"
	 * @generated
	 */
	long getFILTERSIZE();

	/**
	 * Sets the value of the '{@link OdxXhtml.SIZEDEFFILTER#getFILTERSIZE <em>FILTERSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FILTERSIZE</em>' attribute.
	 * @see #isSetFILTERSIZE()
	 * @see #unsetFILTERSIZE()
	 * @see #getFILTERSIZE()
	 * @generated
	 */
	void setFILTERSIZE(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.SIZEDEFFILTER#getFILTERSIZE <em>FILTERSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFILTERSIZE()
	 * @see #getFILTERSIZE()
	 * @see #setFILTERSIZE(long)
	 * @generated
	 */
	void unsetFILTERSIZE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.SIZEDEFFILTER#getFILTERSIZE <em>FILTERSIZE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>FILTERSIZE</em>' attribute is set.
	 * @see #unsetFILTERSIZE()
	 * @see #getFILTERSIZE()
	 * @see #setFILTERSIZE(long)
	 * @generated
	 */
	boolean isSetFILTERSIZE();

} // SIZEDEFFILTER
