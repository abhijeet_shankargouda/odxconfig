/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LOGICALLINKREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.LOGICALLINKREFS#getLOGICALLINKREF <em>LOGICALLINKREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINKREFS()
 * @model extendedMetaData="name='LOGICAL-LINK-REFS' kind='elementOnly'"
 * @generated
 */
public interface LOGICALLINKREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>LOGICALLINKREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LOGICALLINKREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINKREFS_LOGICALLINKREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='LOGICAL-LINK-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getLOGICALLINKREF();

} // LOGICALLINKREFS
