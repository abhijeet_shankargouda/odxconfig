/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SESSIONDESC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SESSIONDESC#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getPARTNUMBER <em>PARTNUMBER</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getPRIORITY <em>PRIORITY</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getSESSIONSNREF <em>SESSIONSNREF</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getFLASHCLASSREFS <em>FLASHCLASSREFS</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getDIRECTION <em>DIRECTION</em>}</li>
 *   <li>{@link OdxXhtml.SESSIONDESC#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC()
 * @model extendedMetaData="name='SESSION-DESC' kind='elementOnly'"
 * @generated
 */
public interface SESSIONDESC extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>PARTNUMBER</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PARTNUMBER</em>' attribute.
	 * @see #setPARTNUMBER(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_PARTNUMBER()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='PARTNUMBER' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPARTNUMBER();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getPARTNUMBER <em>PARTNUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PARTNUMBER</em>' attribute.
	 * @see #getPARTNUMBER()
	 * @generated
	 */
	void setPARTNUMBER(String value);

	/**
	 * Returns the value of the '<em><b>PRIORITY</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PRIORITY</em>' attribute.
	 * @see #isSetPRIORITY()
	 * @see #unsetPRIORITY()
	 * @see #setPRIORITY(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_PRIORITY()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='element' name='PRIORITY' namespace='##targetNamespace'"
	 * @generated
	 */
	long getPRIORITY();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getPRIORITY <em>PRIORITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PRIORITY</em>' attribute.
	 * @see #isSetPRIORITY()
	 * @see #unsetPRIORITY()
	 * @see #getPRIORITY()
	 * @generated
	 */
	void setPRIORITY(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.SESSIONDESC#getPRIORITY <em>PRIORITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPRIORITY()
	 * @see #getPRIORITY()
	 * @see #setPRIORITY(long)
	 * @generated
	 */
	void unsetPRIORITY();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.SESSIONDESC#getPRIORITY <em>PRIORITY</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>PRIORITY</em>' attribute is set.
	 * @see #unsetPRIORITY()
	 * @see #getPRIORITY()
	 * @see #setPRIORITY(long)
	 * @generated
	 */
	boolean isSetPRIORITY();

	/**
	 * Returns the value of the '<em><b>SESSIONSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SESSIONSNREF</em>' containment reference.
	 * @see #setSESSIONSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_SESSIONSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SESSION-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getSESSIONSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getSESSIONSNREF <em>SESSIONSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SESSIONSNREF</em>' containment reference.
	 * @see #getSESSIONSNREF()
	 * @generated
	 */
	void setSESSIONSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>DIAGCOMMSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #setDIAGCOMMSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_DIAGCOMMSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMM-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDIAGCOMMSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 */
	void setDIAGCOMMSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>FLASHCLASSREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FLASHCLASSREFS</em>' containment reference.
	 * @see #setFLASHCLASSREFS(FLASHCLASSREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_FLASHCLASSREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FLASH-CLASS-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	FLASHCLASSREFS getFLASHCLASSREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getFLASHCLASSREFS <em>FLASHCLASSREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FLASHCLASSREFS</em>' containment reference.
	 * @see #getFLASHCLASSREFS()
	 * @generated
	 */
	void setFLASHCLASSREFS(FLASHCLASSREFS value);

	/**
	 * Returns the value of the '<em><b>SDGS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGS</em>' containment reference.
	 * @see #setSDGS(SDGS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_SDGS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDGS' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGS getSDGS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getSDGS <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGS</em>' containment reference.
	 * @see #getSDGS()
	 * @generated
	 */
	void setSDGS(SDGS value);

	/**
	 * Returns the value of the '<em><b>DIRECTION</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.DIRECTION}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIRECTION</em>' attribute.
	 * @see OdxXhtml.DIRECTION
	 * @see #isSetDIRECTION()
	 * @see #unsetDIRECTION()
	 * @see #setDIRECTION(DIRECTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_DIRECTION()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='DIRECTION' namespace='##targetNamespace'"
	 * @generated
	 */
	DIRECTION getDIRECTION();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getDIRECTION <em>DIRECTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIRECTION</em>' attribute.
	 * @see OdxXhtml.DIRECTION
	 * @see #isSetDIRECTION()
	 * @see #unsetDIRECTION()
	 * @see #getDIRECTION()
	 * @generated
	 */
	void setDIRECTION(DIRECTION value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.SESSIONDESC#getDIRECTION <em>DIRECTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDIRECTION()
	 * @see #getDIRECTION()
	 * @see #setDIRECTION(DIRECTION)
	 * @generated
	 */
	void unsetDIRECTION();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.SESSIONDESC#getDIRECTION <em>DIRECTION</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>DIRECTION</em>' attribute is set.
	 * @see #unsetDIRECTION()
	 * @see #getDIRECTION()
	 * @see #setDIRECTION(DIRECTION)
	 * @generated
	 */
	boolean isSetDIRECTION();

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSIONDESC_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSIONDESC#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // SESSIONDESC
