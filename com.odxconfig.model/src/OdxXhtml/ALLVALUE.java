/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ALLVALUE</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getALLVALUE()
 * @model extendedMetaData="name='ALL-VALUE' kind='empty'"
 * @generated
 */
public interface ALLVALUE extends EObject {
} // ALLVALUE
