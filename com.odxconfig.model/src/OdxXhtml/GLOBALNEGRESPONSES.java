/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GLOBALNEGRESPONSES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.GLOBALNEGRESPONSES#getGLOBALNEGRESPONSE <em>GLOBALNEGRESPONSE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getGLOBALNEGRESPONSES()
 * @model extendedMetaData="name='GLOBAL-NEG-RESPONSES' kind='elementOnly'"
 * @generated
 */
public interface GLOBALNEGRESPONSES extends EObject {
	/**
	 * Returns the value of the '<em><b>GLOBALNEGRESPONSE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.GLOBALNEGRESPONSE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>GLOBALNEGRESPONSE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getGLOBALNEGRESPONSES_GLOBALNEGRESPONSE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='GLOBAL-NEG-RESPONSE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<GLOBALNEGRESPONSE> getGLOBALNEGRESPONSE();

} // GLOBALNEGRESPONSES
