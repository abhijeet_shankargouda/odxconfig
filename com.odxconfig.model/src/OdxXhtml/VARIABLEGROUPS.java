/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VARIABLEGROUPS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VARIABLEGROUPS#getVARIABLEGROUP <em>VARIABLEGROUP</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVARIABLEGROUPS()
 * @model extendedMetaData="name='VARIABLE-GROUPS' kind='elementOnly'"
 * @generated
 */
public interface VARIABLEGROUPS extends EObject {
	/**
	 * Returns the value of the '<em><b>VARIABLEGROUP</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.VARIABLEGROUP}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VARIABLEGROUP</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getVARIABLEGROUPS_VARIABLEGROUP()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='VARIABLE-GROUP' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<VARIABLEGROUP> getVARIABLEGROUP();

} // VARIABLEGROUPS
