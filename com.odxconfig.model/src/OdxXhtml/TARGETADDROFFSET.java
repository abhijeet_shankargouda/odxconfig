/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TARGETADDROFFSET</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getTARGETADDROFFSET()
 * @model extendedMetaData="name='TARGET-ADDR-OFFSET' kind='empty'"
 * @generated
 */
public interface TARGETADDROFFSET extends EObject {
} // TARGETADDROFFSET
