/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OEM</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getOEM()
 * @model extendedMetaData="name='OEM' kind='elementOnly'"
 * @generated
 */
public interface OEM extends INFOCOMPONENT {
} // OEM
