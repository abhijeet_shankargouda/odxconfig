/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MULTIPLEECUJOBS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MULTIPLEECUJOBS#getMULTIPLEECUJOB <em>MULTIPLEECUJOB</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOBS()
 * @model extendedMetaData="name='MULTIPLE-ECU-JOBS' kind='elementOnly'"
 * @generated
 */
public interface MULTIPLEECUJOBS extends EObject {
	/**
	 * Returns the value of the '<em><b>MULTIPLEECUJOB</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.MULTIPLEECUJOB}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MULTIPLEECUJOB</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getMULTIPLEECUJOBS_MULTIPLEECUJOB()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MULTIPLE-ECU-JOB' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MULTIPLEECUJOB> getMULTIPLEECUJOB();

} // MULTIPLEECUJOBS
