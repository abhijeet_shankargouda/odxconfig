/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPARAMSPEC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPARAMSPEC#getCOMPARAMS <em>COMPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAMSPEC#getDATAOBJECTPROPS <em>DATAOBJECTPROPS</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAMSPEC#getUNITSPEC <em>UNITSPEC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMSPEC()
 * @model extendedMetaData="name='COMPARAM-SPEC' kind='elementOnly'"
 * @generated
 */
public interface COMPARAMSPEC extends ODXCATEGORY {
	/**
	 * Returns the value of the '<em><b>COMPARAMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPARAMS</em>' containment reference.
	 * @see #setCOMPARAMS(COMPARAMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMSPEC_COMPARAMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPARAMS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPARAMS getCOMPARAMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMSPEC#getCOMPARAMS <em>COMPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPARAMS</em>' containment reference.
	 * @see #getCOMPARAMS()
	 * @generated
	 */
	void setCOMPARAMS(COMPARAMS value);

	/**
	 * Returns the value of the '<em><b>DATAOBJECTPROPS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATAOBJECTPROPS</em>' containment reference.
	 * @see #setDATAOBJECTPROPS(DATAOBJECTPROPS)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMSPEC_DATAOBJECTPROPS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DATA-OBJECT-PROPS' namespace='##targetNamespace'"
	 * @generated
	 */
	DATAOBJECTPROPS getDATAOBJECTPROPS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMSPEC#getDATAOBJECTPROPS <em>DATAOBJECTPROPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATAOBJECTPROPS</em>' containment reference.
	 * @see #getDATAOBJECTPROPS()
	 * @generated
	 */
	void setDATAOBJECTPROPS(DATAOBJECTPROPS value);

	/**
	 * Returns the value of the '<em><b>UNITSPEC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNITSPEC</em>' containment reference.
	 * @see #setUNITSPEC(UNITSPEC)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAMSPEC_UNITSPEC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UNIT-SPEC' namespace='##targetNamespace'"
	 * @generated
	 */
	UNITSPEC getUNITSPEC();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAMSPEC#getUNITSPEC <em>UNITSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UNITSPEC</em>' containment reference.
	 * @see #getUNITSPEC()
	 * @generated
	 */
	void setUNITSPEC(UNITSPEC value);

} // COMPARAMSPEC
