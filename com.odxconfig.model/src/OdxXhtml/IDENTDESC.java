/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IDENTDESC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.IDENTDESC#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.IDENTDESC#getIDENTIFSNREF <em>IDENTIFSNREF</em>}</li>
 *   <li>{@link OdxXhtml.IDENTDESC#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getIDENTDESC()
 * @model extendedMetaData="name='IDENT-DESC' kind='elementOnly'"
 * @generated
 */
public interface IDENTDESC extends EObject {
	/**
	 * Returns the value of the '<em><b>DIAGCOMMSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #setDIAGCOMMSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getIDENTDESC_DIAGCOMMSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMM-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDIAGCOMMSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.IDENTDESC#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 */
	void setDIAGCOMMSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>IDENTIFSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IDENTIFSNREF</em>' containment reference.
	 * @see #setIDENTIFSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getIDENTDESC_IDENTIFSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='IDENT-IF-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getIDENTIFSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.IDENTDESC#getIDENTIFSNREF <em>IDENTIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IDENTIFSNREF</em>' containment reference.
	 * @see #getIDENTIFSNREF()
	 * @generated
	 */
	void setIDENTIFSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>OUTPARAMIFSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OUTPARAMIFSNREF</em>' containment reference.
	 * @see #setOUTPARAMIFSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getIDENTDESC_OUTPARAMIFSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OUT-PARAM-IF-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getOUTPARAMIFSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.IDENTDESC#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OUTPARAMIFSNREF</em>' containment reference.
	 * @see #getOUTPARAMIFSNREF()
	 * @generated
	 */
	void setOUTPARAMIFSNREF(SNREF value);

} // IDENTDESC
