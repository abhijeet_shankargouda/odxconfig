/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLECONNECTOR</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VEHICLECONNECTOR#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTOR#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTOR#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTOR#getVEHICLECONNECTORPINS <em>VEHICLECONNECTORPINS</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTOR#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTOR()
 * @model extendedMetaData="name='VEHICLE-CONNECTOR' kind='elementOnly'"
 * @generated
 */
public interface VEHICLECONNECTOR extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTOR_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTOR#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTOR_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTOR#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTOR_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTOR#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>VEHICLECONNECTORPINS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VEHICLECONNECTORPINS</em>' containment reference.
	 * @see #setVEHICLECONNECTORPINS(VEHICLECONNECTORPINS)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTOR_VEHICLECONNECTORPINS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='VEHICLE-CONNECTOR-PINS' namespace='##targetNamespace'"
	 * @generated
	 */
	VEHICLECONNECTORPINS getVEHICLECONNECTORPINS();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTOR#getVEHICLECONNECTORPINS <em>VEHICLECONNECTORPINS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VEHICLECONNECTORPINS</em>' containment reference.
	 * @see #getVEHICLECONNECTORPINS()
	 * @generated
	 */
	void setVEHICLECONNECTORPINS(VEHICLECONNECTORPINS value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTOR_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTOR#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // VEHICLECONNECTOR
