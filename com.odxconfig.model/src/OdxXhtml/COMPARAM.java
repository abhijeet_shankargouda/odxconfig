/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPARAM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPARAM#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getPHYSICALDEFAULTVALUE <em>PHYSICALDEFAULTVALUE</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getDATAOBJECTPROPREF <em>DATAOBJECTPROPREF</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getCPTYPE <em>CPTYPE</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getDISPLAYLEVEL <em>DISPLAYLEVEL</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getPARAMCLASS <em>PARAMCLASS</em>}</li>
 *   <li>{@link OdxXhtml.COMPARAM#getPDUAPIINDEX <em>PDUAPIINDEX</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM()
 * @model extendedMetaData="name='COMPARAM' kind='elementOnly'"
 * @generated
 */
public interface COMPARAM extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>PHYSICALDEFAULTVALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALDEFAULTVALUE</em>' attribute.
	 * @see #setPHYSICALDEFAULTVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_PHYSICALDEFAULTVALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='PHYSICAL-DEFAULT-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPHYSICALDEFAULTVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getPHYSICALDEFAULTVALUE <em>PHYSICALDEFAULTVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSICALDEFAULTVALUE</em>' attribute.
	 * @see #getPHYSICALDEFAULTVALUE()
	 * @generated
	 */
	void setPHYSICALDEFAULTVALUE(String value);

	/**
	 * Returns the value of the '<em><b>DATAOBJECTPROPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATAOBJECTPROPREF</em>' containment reference.
	 * @see #setDATAOBJECTPROPREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_DATAOBJECTPROPREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATA-OBJECT-PROP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDATAOBJECTPROPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getDATAOBJECTPROPREF <em>DATAOBJECTPROPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATAOBJECTPROPREF</em>' containment reference.
	 * @see #getDATAOBJECTPROPREF()
	 * @generated
	 */
	void setDATAOBJECTPROPREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>CPTYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.STANDARDISATIONLEVEL}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CPTYPE</em>' attribute.
	 * @see OdxXhtml.STANDARDISATIONLEVEL
	 * @see #isSetCPTYPE()
	 * @see #unsetCPTYPE()
	 * @see #setCPTYPE(STANDARDISATIONLEVEL)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_CPTYPE()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='CPTYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	STANDARDISATIONLEVEL getCPTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getCPTYPE <em>CPTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CPTYPE</em>' attribute.
	 * @see OdxXhtml.STANDARDISATIONLEVEL
	 * @see #isSetCPTYPE()
	 * @see #unsetCPTYPE()
	 * @see #getCPTYPE()
	 * @generated
	 */
	void setCPTYPE(STANDARDISATIONLEVEL value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.COMPARAM#getCPTYPE <em>CPTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCPTYPE()
	 * @see #getCPTYPE()
	 * @see #setCPTYPE(STANDARDISATIONLEVEL)
	 * @generated
	 */
	void unsetCPTYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.COMPARAM#getCPTYPE <em>CPTYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>CPTYPE</em>' attribute is set.
	 * @see #unsetCPTYPE()
	 * @see #getCPTYPE()
	 * @see #setCPTYPE(STANDARDISATIONLEVEL)
	 * @generated
	 */
	boolean isSetCPTYPE();

	/**
	 * Returns the value of the '<em><b>DISPLAYLEVEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DISPLAYLEVEL</em>' attribute.
	 * @see #isSetDISPLAYLEVEL()
	 * @see #unsetDISPLAYLEVEL()
	 * @see #setDISPLAYLEVEL(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_DISPLAYLEVEL()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='attribute' name='DISPLAY-LEVEL' namespace='##targetNamespace'"
	 * @generated
	 */
	long getDISPLAYLEVEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getDISPLAYLEVEL <em>DISPLAYLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DISPLAYLEVEL</em>' attribute.
	 * @see #isSetDISPLAYLEVEL()
	 * @see #unsetDISPLAYLEVEL()
	 * @see #getDISPLAYLEVEL()
	 * @generated
	 */
	void setDISPLAYLEVEL(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.COMPARAM#getDISPLAYLEVEL <em>DISPLAYLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDISPLAYLEVEL()
	 * @see #getDISPLAYLEVEL()
	 * @see #setDISPLAYLEVEL(long)
	 * @generated
	 */
	void unsetDISPLAYLEVEL();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.COMPARAM#getDISPLAYLEVEL <em>DISPLAYLEVEL</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>DISPLAYLEVEL</em>' attribute is set.
	 * @see #unsetDISPLAYLEVEL()
	 * @see #getDISPLAYLEVEL()
	 * @see #setDISPLAYLEVEL(long)
	 * @generated
	 */
	boolean isSetDISPLAYLEVEL();

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

	/**
	 * Returns the value of the '<em><b>PARAMCLASS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PARAMCLASS</em>' attribute.
	 * @see #setPARAMCLASS(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_PARAMCLASS()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='PARAM-CLASS' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPARAMCLASS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getPARAMCLASS <em>PARAMCLASS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PARAMCLASS</em>' attribute.
	 * @see #getPARAMCLASS()
	 * @generated
	 */
	void setPARAMCLASS(String value);

	/**
	 * Returns the value of the '<em><b>PDUAPIINDEX</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PDUAPIINDEX</em>' attribute.
	 * @see #isSetPDUAPIINDEX()
	 * @see #unsetPDUAPIINDEX()
	 * @see #setPDUAPIINDEX(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPARAM_PDUAPIINDEX()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='attribute' name='PDU-API-INDEX' namespace='##targetNamespace'"
	 * @generated
	 */
	long getPDUAPIINDEX();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPARAM#getPDUAPIINDEX <em>PDUAPIINDEX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PDUAPIINDEX</em>' attribute.
	 * @see #isSetPDUAPIINDEX()
	 * @see #unsetPDUAPIINDEX()
	 * @see #getPDUAPIINDEX()
	 * @generated
	 */
	void setPDUAPIINDEX(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.COMPARAM#getPDUAPIINDEX <em>PDUAPIINDEX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPDUAPIINDEX()
	 * @see #getPDUAPIINDEX()
	 * @see #setPDUAPIINDEX(long)
	 * @generated
	 */
	void unsetPDUAPIINDEX();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.COMPARAM#getPDUAPIINDEX <em>PDUAPIINDEX</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>PDUAPIINDEX</em>' attribute is set.
	 * @see #unsetPDUAPIINDEX()
	 * @see #getPDUAPIINDEX()
	 * @see #setPDUAPIINDEX(long)
	 * @generated
	 */
	boolean isSetPDUAPIINDEX();

} // COMPARAM
