/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPUSCALES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPUSCALES#getCOMPUSCALE <em>COMPUSCALE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALES()
 * @model extendedMetaData="name='COMPU-SCALES' kind='elementOnly'"
 * @generated
 */
public interface COMPUSCALES extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPUSCALE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.COMPUSCALE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUSCALE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALES_COMPUSCALE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPU-SCALE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<COMPUSCALE> getCOMPUSCALE();

} // COMPUSCALES
