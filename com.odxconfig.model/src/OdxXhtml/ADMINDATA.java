/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ADMINDATA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ADMINDATA#getLANGUAGE <em>LANGUAGE</em>}</li>
 *   <li>{@link OdxXhtml.ADMINDATA#getCOMPANYDOCINFOS <em>COMPANYDOCINFOS</em>}</li>
 *   <li>{@link OdxXhtml.ADMINDATA#getDOCREVISIONS <em>DOCREVISIONS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getADMINDATA()
 * @model extendedMetaData="name='ADMIN-DATA' kind='elementOnly'"
 * @generated
 */
public interface ADMINDATA extends EObject {
	/**
	 * Returns the value of the '<em><b>LANGUAGE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LANGUAGE</em>' attribute.
	 * @see #setLANGUAGE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getADMINDATA_LANGUAGE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='LANGUAGE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getLANGUAGE();

	/**
	 * Sets the value of the '{@link OdxXhtml.ADMINDATA#getLANGUAGE <em>LANGUAGE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LANGUAGE</em>' attribute.
	 * @see #getLANGUAGE()
	 * @generated
	 */
	void setLANGUAGE(String value);

	/**
	 * Returns the value of the '<em><b>COMPANYDOCINFOS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYDOCINFOS</em>' containment reference.
	 * @see #setCOMPANYDOCINFOS(COMPANYDOCINFOS)
	 * @see OdxXhtml.OdxXhtmlPackage#getADMINDATA_COMPANYDOCINFOS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPANY-DOC-INFOS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPANYDOCINFOS getCOMPANYDOCINFOS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ADMINDATA#getCOMPANYDOCINFOS <em>COMPANYDOCINFOS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYDOCINFOS</em>' containment reference.
	 * @see #getCOMPANYDOCINFOS()
	 * @generated
	 */
	void setCOMPANYDOCINFOS(COMPANYDOCINFOS value);

	/**
	 * Returns the value of the '<em><b>DOCREVISIONS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCREVISIONS</em>' containment reference.
	 * @see #setDOCREVISIONS(DOCREVISIONS)
	 * @see OdxXhtml.OdxXhtmlPackage#getADMINDATA_DOCREVISIONS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DOC-REVISIONS' namespace='##targetNamespace'"
	 * @generated
	 */
	DOCREVISIONS getDOCREVISIONS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ADMINDATA#getDOCREVISIONS <em>DOCREVISIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOCREVISIONS</em>' containment reference.
	 * @see #getDOCREVISIONS()
	 * @generated
	 */
	void setDOCREVISIONS(DOCREVISIONS value);

} // ADMINDATA
