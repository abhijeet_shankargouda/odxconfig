/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LOGICALLINK</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.LOGICALLINK#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getGATEWAYLOGICALLINKREFS <em>GATEWAYLOGICALLINKREFS</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getPHYSICALVEHICLELINKREF <em>PHYSICALVEHICLELINKREF</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getPROTOCOLREF <em>PROTOCOLREF</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getFUNCTIONALGROUPREF <em>FUNCTIONALGROUPREF</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getBASEVARIANTREF <em>BASEVARIANTREF</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getECUPROXYREFS <em>ECUPROXYREFS</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getLINKCOMPARAMREFS <em>LINKCOMPARAMREFS</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.LOGICALLINK#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK()
 * @model extendedMetaData="name='LOGICAL-LINK' kind='elementOnly'"
 * @generated
 */
public interface LOGICALLINK extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>GATEWAYLOGICALLINKREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>GATEWAYLOGICALLINKREFS</em>' containment reference.
	 * @see #setGATEWAYLOGICALLINKREFS(GATEWAYLOGICALLINKREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_GATEWAYLOGICALLINKREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='GATEWAY-LOGICAL-LINK-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	GATEWAYLOGICALLINKREFS getGATEWAYLOGICALLINKREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getGATEWAYLOGICALLINKREFS <em>GATEWAYLOGICALLINKREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>GATEWAYLOGICALLINKREFS</em>' containment reference.
	 * @see #getGATEWAYLOGICALLINKREFS()
	 * @generated
	 */
	void setGATEWAYLOGICALLINKREFS(GATEWAYLOGICALLINKREFS value);

	/**
	 * Returns the value of the '<em><b>PHYSICALVEHICLELINKREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALVEHICLELINKREF</em>' containment reference.
	 * @see #setPHYSICALVEHICLELINKREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_PHYSICALVEHICLELINKREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PHYSICAL-VEHICLE-LINK-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getPHYSICALVEHICLELINKREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getPHYSICALVEHICLELINKREF <em>PHYSICALVEHICLELINKREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSICALVEHICLELINKREF</em>' containment reference.
	 * @see #getPHYSICALVEHICLELINKREF()
	 * @generated
	 */
	void setPHYSICALVEHICLELINKREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>PROTOCOLREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROTOCOLREF</em>' containment reference.
	 * @see #setPROTOCOLREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_PROTOCOLREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PROTOCOL-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getPROTOCOLREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getPROTOCOLREF <em>PROTOCOLREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROTOCOLREF</em>' containment reference.
	 * @see #getPROTOCOLREF()
	 * @generated
	 */
	void setPROTOCOLREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>FUNCTIONALGROUPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FUNCTIONALGROUPREF</em>' containment reference.
	 * @see #setFUNCTIONALGROUPREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_FUNCTIONALGROUPREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FUNCTIONAL-GROUP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getFUNCTIONALGROUPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getFUNCTIONALGROUPREF <em>FUNCTIONALGROUPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FUNCTIONALGROUPREF</em>' containment reference.
	 * @see #getFUNCTIONALGROUPREF()
	 * @generated
	 */
	void setFUNCTIONALGROUPREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>BASEVARIANTREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BASEVARIANTREF</em>' containment reference.
	 * @see #setBASEVARIANTREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_BASEVARIANTREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='BASE-VARIANT-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getBASEVARIANTREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getBASEVARIANTREF <em>BASEVARIANTREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BASEVARIANTREF</em>' containment reference.
	 * @see #getBASEVARIANTREF()
	 * @generated
	 */
	void setBASEVARIANTREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>ECUPROXYREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUPROXYREFS</em>' containment reference.
	 * @see #setECUPROXYREFS(ECUPROXYREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_ECUPROXYREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ECU-PROXY-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	ECUPROXYREFS getECUPROXYREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getECUPROXYREFS <em>ECUPROXYREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ECUPROXYREFS</em>' containment reference.
	 * @see #getECUPROXYREFS()
	 * @generated
	 */
	void setECUPROXYREFS(ECUPROXYREFS value);

	/**
	 * Returns the value of the '<em><b>LINKCOMPARAMREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LINKCOMPARAMREFS</em>' containment reference.
	 * @see #setLINKCOMPARAMREFS(LINKCOMPARAMREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_LINKCOMPARAMREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LINK-COMPARAM-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	LINKCOMPARAMREFS getLINKCOMPARAMREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getLINKCOMPARAMREFS <em>LINKCOMPARAMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LINKCOMPARAMREFS</em>' containment reference.
	 * @see #getLINKCOMPARAMREFS()
	 * @generated
	 */
	void setLINKCOMPARAMREFS(LINKCOMPARAMREFS value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getLOGICALLINK_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.LOGICALLINK#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // LOGICALLINK
