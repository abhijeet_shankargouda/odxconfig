/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INFOCOMPONENTREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.INFOCOMPONENTREFS#getINFOCOMPONENTREF <em>INFOCOMPONENTREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getINFOCOMPONENTREFS()
 * @model extendedMetaData="name='INFO-COMPONENT-REFS' kind='elementOnly'"
 * @generated
 */
public interface INFOCOMPONENTREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>INFOCOMPONENTREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INFOCOMPONENTREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getINFOCOMPONENTREFS_INFOCOMPONENTREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='INFO-COMPONENT-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getINFOCOMPONENTREF();

} // INFOCOMPONENTREFS
