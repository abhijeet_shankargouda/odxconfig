/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MATCHINGCOMPONENT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MATCHINGCOMPONENT#getEXPECTEDVALUE <em>EXPECTEDVALUE</em>}</li>
 *   <li>{@link OdxXhtml.MATCHINGCOMPONENT#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}</li>
 *   <li>{@link OdxXhtml.MATCHINGCOMPONENT#getMULTIPLEECUJOBREF <em>MULTIPLEECUJOBREF</em>}</li>
 *   <li>{@link OdxXhtml.MATCHINGCOMPONENT#getDIAGCOMMREF <em>DIAGCOMMREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGCOMPONENT()
 * @model extendedMetaData="name='MATCHING-COMPONENT' kind='elementOnly'"
 * @generated
 */
public interface MATCHINGCOMPONENT extends EObject {
	/**
	 * Returns the value of the '<em><b>EXPECTEDVALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EXPECTEDVALUE</em>' attribute.
	 * @see #setEXPECTEDVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGCOMPONENT_EXPECTEDVALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='EXPECTED-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getEXPECTEDVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.MATCHINGCOMPONENT#getEXPECTEDVALUE <em>EXPECTEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EXPECTEDVALUE</em>' attribute.
	 * @see #getEXPECTEDVALUE()
	 * @generated
	 */
	void setEXPECTEDVALUE(String value);

	/**
	 * Returns the value of the '<em><b>OUTPARAMIFSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OUTPARAMIFSNREF</em>' containment reference.
	 * @see #setOUTPARAMIFSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGCOMPONENT_OUTPARAMIFSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OUT-PARAM-IF-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getOUTPARAMIFSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.MATCHINGCOMPONENT#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OUTPARAMIFSNREF</em>' containment reference.
	 * @see #getOUTPARAMIFSNREF()
	 * @generated
	 */
	void setOUTPARAMIFSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>MULTIPLEECUJOBREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MULTIPLEECUJOBREF</em>' containment reference.
	 * @see #setMULTIPLEECUJOBREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGCOMPONENT_MULTIPLEECUJOBREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MULTIPLE-ECU-JOB-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getMULTIPLEECUJOBREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.MATCHINGCOMPONENT#getMULTIPLEECUJOBREF <em>MULTIPLEECUJOBREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MULTIPLEECUJOBREF</em>' containment reference.
	 * @see #getMULTIPLEECUJOBREF()
	 * @generated
	 */
	void setMULTIPLEECUJOBREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>DIAGCOMMREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMREF</em>' containment reference.
	 * @see #setDIAGCOMMREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGCOMPONENT_DIAGCOMMREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMM-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDIAGCOMMREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.MATCHINGCOMPONENT#getDIAGCOMMREF <em>DIAGCOMMREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCOMMREF</em>' containment reference.
	 * @see #getDIAGCOMMREF()
	 * @generated
	 */
	void setDIAGCOMMREF(ODXLINK value);

} // MATCHINGCOMPONENT
