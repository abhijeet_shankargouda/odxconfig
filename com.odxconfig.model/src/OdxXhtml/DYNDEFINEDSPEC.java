/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DYNDEFINEDSPEC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DYNDEFINEDSPEC#getDYNIDDEFMODEINFOS <em>DYNIDDEFMODEINFOS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDYNDEFINEDSPEC()
 * @model extendedMetaData="name='DYN-DEFINED-SPEC' kind='elementOnly'"
 * @generated
 */
public interface DYNDEFINEDSPEC extends EObject {
	/**
	 * Returns the value of the '<em><b>DYNIDDEFMODEINFOS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DYNIDDEFMODEINFOS</em>' containment reference.
	 * @see #setDYNIDDEFMODEINFOS(DYNIDDEFMODEINFOS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNDEFINEDSPEC_DYNIDDEFMODEINFOS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DYN-ID-DEF-MODE-INFOS' namespace='##targetNamespace'"
	 * @generated
	 */
	DYNIDDEFMODEINFOS getDYNIDDEFMODEINFOS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DYNDEFINEDSPEC#getDYNIDDEFMODEINFOS <em>DYNIDDEFMODEINFOS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DYNIDDEFMODEINFOS</em>' containment reference.
	 * @see #getDYNIDDEFMODEINFOS()
	 * @generated
	 */
	void setDYNIDDEFMODEINFOS(DYNIDDEFMODEINFOS value);

} // DYNDEFINEDSPEC
