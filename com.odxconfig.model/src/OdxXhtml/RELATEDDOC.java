/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RELATEDDOC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.RELATEDDOC#getXDOC <em>XDOC</em>}</li>
 *   <li>{@link OdxXhtml.RELATEDDOC#getDESC <em>DESC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDOC()
 * @model extendedMetaData="name='RELATED-DOC' kind='elementOnly'"
 * @generated
 */
public interface RELATEDDOC extends EObject {
	/**
	 * Returns the value of the '<em><b>XDOC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XDOC</em>' containment reference.
	 * @see #setXDOC(XDOC)
	 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDOC_XDOC()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='XDOC' namespace='##targetNamespace'"
	 * @generated
	 */
	XDOC getXDOC();

	/**
	 * Sets the value of the '{@link OdxXhtml.RELATEDDOC#getXDOC <em>XDOC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>XDOC</em>' containment reference.
	 * @see #getXDOC()
	 * @generated
	 */
	void setXDOC(XDOC value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDOC_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.RELATEDDOC#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

} // RELATEDDOC
