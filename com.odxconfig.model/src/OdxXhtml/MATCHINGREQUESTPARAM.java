/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MATCHINGREQUESTPARAM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MATCHINGREQUESTPARAM#getREQUESTBYTEPOS <em>REQUESTBYTEPOS</em>}</li>
 *   <li>{@link OdxXhtml.MATCHINGREQUESTPARAM#getBYTELENGTH <em>BYTELENGTH</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGREQUESTPARAM()
 * @model extendedMetaData="name='MATCHING-REQUEST-PARAM' kind='elementOnly'"
 * @generated
 */
public interface MATCHINGREQUESTPARAM extends POSITIONABLEPARAM {
	/**
	 * Returns the value of the '<em><b>REQUESTBYTEPOS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REQUESTBYTEPOS</em>' attribute.
	 * @see #isSetREQUESTBYTEPOS()
	 * @see #unsetREQUESTBYTEPOS()
	 * @see #setREQUESTBYTEPOS(int)
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGREQUESTPARAM_REQUESTBYTEPOS()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int" required="true"
	 *        extendedMetaData="kind='element' name='REQUEST-BYTE-POS' namespace='##targetNamespace'"
	 * @generated
	 */
	int getREQUESTBYTEPOS();

	/**
	 * Sets the value of the '{@link OdxXhtml.MATCHINGREQUESTPARAM#getREQUESTBYTEPOS <em>REQUESTBYTEPOS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REQUESTBYTEPOS</em>' attribute.
	 * @see #isSetREQUESTBYTEPOS()
	 * @see #unsetREQUESTBYTEPOS()
	 * @see #getREQUESTBYTEPOS()
	 * @generated
	 */
	void setREQUESTBYTEPOS(int value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MATCHINGREQUESTPARAM#getREQUESTBYTEPOS <em>REQUESTBYTEPOS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetREQUESTBYTEPOS()
	 * @see #getREQUESTBYTEPOS()
	 * @see #setREQUESTBYTEPOS(int)
	 * @generated
	 */
	void unsetREQUESTBYTEPOS();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MATCHINGREQUESTPARAM#getREQUESTBYTEPOS <em>REQUESTBYTEPOS</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>REQUESTBYTEPOS</em>' attribute is set.
	 * @see #unsetREQUESTBYTEPOS()
	 * @see #getREQUESTBYTEPOS()
	 * @see #setREQUESTBYTEPOS(int)
	 * @generated
	 */
	boolean isSetREQUESTBYTEPOS();

	/**
	 * Returns the value of the '<em><b>BYTELENGTH</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BYTELENGTH</em>' attribute.
	 * @see #isSetBYTELENGTH()
	 * @see #unsetBYTELENGTH()
	 * @see #setBYTELENGTH(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGREQUESTPARAM_BYTELENGTH()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='BYTE-LENGTH' namespace='##targetNamespace'"
	 * @generated
	 */
	long getBYTELENGTH();

	/**
	 * Sets the value of the '{@link OdxXhtml.MATCHINGREQUESTPARAM#getBYTELENGTH <em>BYTELENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BYTELENGTH</em>' attribute.
	 * @see #isSetBYTELENGTH()
	 * @see #unsetBYTELENGTH()
	 * @see #getBYTELENGTH()
	 * @generated
	 */
	void setBYTELENGTH(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.MATCHINGREQUESTPARAM#getBYTELENGTH <em>BYTELENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBYTELENGTH()
	 * @see #getBYTELENGTH()
	 * @see #setBYTELENGTH(long)
	 * @generated
	 */
	void unsetBYTELENGTH();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.MATCHINGREQUESTPARAM#getBYTELENGTH <em>BYTELENGTH</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BYTELENGTH</em>' attribute is set.
	 * @see #unsetBYTELENGTH()
	 * @see #getBYTELENGTH()
	 * @see #setBYTELENGTH(long)
	 * @generated
	 */
	boolean isSetBYTELENGTH();

} // MATCHINGREQUESTPARAM
