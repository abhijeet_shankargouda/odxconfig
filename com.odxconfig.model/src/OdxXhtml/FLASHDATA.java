/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FLASHDATA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FLASHDATA#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.FLASHDATA#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.FLASHDATA#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.FLASHDATA#getDATAFORMAT <em>DATAFORMAT</em>}</li>
 *   <li>{@link OdxXhtml.FLASHDATA#getENCRYPTCOMPRESSMETHOD <em>ENCRYPTCOMPRESSMETHOD</em>}</li>
 *   <li>{@link OdxXhtml.FLASHDATA#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.FLASHDATA#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATA()
 * @model extendedMetaData="name='FLASHDATA' kind='elementOnly'"
 * @generated
 */
public interface FLASHDATA extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATA_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.FLASHDATA#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATA_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.FLASHDATA#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATA_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.FLASHDATA#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>DATAFORMAT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATAFORMAT</em>' containment reference.
	 * @see #setDATAFORMAT(DATAFORMAT)
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATA_DATAFORMAT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATAFORMAT' namespace='##targetNamespace'"
	 * @generated
	 */
	DATAFORMAT getDATAFORMAT();

	/**
	 * Sets the value of the '{@link OdxXhtml.FLASHDATA#getDATAFORMAT <em>DATAFORMAT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATAFORMAT</em>' containment reference.
	 * @see #getDATAFORMAT()
	 * @generated
	 */
	void setDATAFORMAT(DATAFORMAT value);

	/**
	 * Returns the value of the '<em><b>ENCRYPTCOMPRESSMETHOD</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENCRYPTCOMPRESSMETHOD</em>' containment reference.
	 * @see #setENCRYPTCOMPRESSMETHOD(ENCRYPTCOMPRESSMETHOD)
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATA_ENCRYPTCOMPRESSMETHOD()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ENCRYPT-COMPRESS-METHOD' namespace='##targetNamespace'"
	 * @generated
	 */
	ENCRYPTCOMPRESSMETHOD getENCRYPTCOMPRESSMETHOD();

	/**
	 * Sets the value of the '{@link OdxXhtml.FLASHDATA#getENCRYPTCOMPRESSMETHOD <em>ENCRYPTCOMPRESSMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ENCRYPTCOMPRESSMETHOD</em>' containment reference.
	 * @see #getENCRYPTCOMPRESSMETHOD()
	 * @generated
	 */
	void setENCRYPTCOMPRESSMETHOD(ENCRYPTCOMPRESSMETHOD value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATA_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.FLASHDATA#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getFLASHDATA_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.FLASHDATA#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // FLASHDATA
