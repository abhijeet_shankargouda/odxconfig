/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MATCHINGCOMPONENTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MATCHINGCOMPONENTS#getMATCHINGCOMPONENT <em>MATCHINGCOMPONENT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGCOMPONENTS()
 * @model extendedMetaData="name='MATCHING-COMPONENTS' kind='elementOnly'"
 * @generated
 */
public interface MATCHINGCOMPONENTS extends EObject {
	/**
	 * Returns the value of the '<em><b>MATCHINGCOMPONENT</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.MATCHINGCOMPONENT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MATCHINGCOMPONENT</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getMATCHINGCOMPONENTS_MATCHINGCOMPONENT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MATCHING-COMPONENT' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<MATCHINGCOMPONENT> getMATCHINGCOMPONENT();

} // MATCHINGCOMPONENTS
