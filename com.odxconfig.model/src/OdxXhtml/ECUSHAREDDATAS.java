/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUSHAREDDATAS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUSHAREDDATAS#getECUSHAREDDATA <em>ECUSHAREDDATA</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUSHAREDDATAS()
 * @model extendedMetaData="name='ECU-SHARED-DATAS' kind='elementOnly'"
 * @generated
 */
public interface ECUSHAREDDATAS extends EObject {
	/**
	 * Returns the value of the '<em><b>ECUSHAREDDATA</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ECUSHAREDDATA}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUSHAREDDATA</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getECUSHAREDDATAS_ECUSHAREDDATA()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ECU-SHARED-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ECUSHAREDDATA> getECUSHAREDDATA();

} // ECUSHAREDDATAS
