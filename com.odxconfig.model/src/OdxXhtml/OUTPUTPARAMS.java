/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OUTPUTPARAMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.OUTPUTPARAMS#getOUTPUTPARAM <em>OUTPUTPARAM</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAMS()
 * @model extendedMetaData="name='OUTPUT-PARAMS' kind='elementOnly'"
 * @generated
 */
public interface OUTPUTPARAMS extends EObject {
	/**
	 * Returns the value of the '<em><b>OUTPUTPARAM</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.OUTPUTPARAM}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OUTPUTPARAM</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAMS_OUTPUTPARAM()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OUTPUT-PARAM' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OUTPUTPARAM> getOUTPUTPARAM();

} // OUTPUTPARAMS
