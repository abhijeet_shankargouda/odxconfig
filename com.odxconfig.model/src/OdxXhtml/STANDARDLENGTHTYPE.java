/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>STANDARDLENGTHTYPE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.STANDARDLENGTHTYPE#getBITLENGTH <em>BITLENGTH</em>}</li>
 *   <li>{@link OdxXhtml.STANDARDLENGTHTYPE#getBITMASK <em>BITMASK</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSTANDARDLENGTHTYPE()
 * @model extendedMetaData="name='STANDARD-LENGTH-TYPE' kind='elementOnly'"
 * @generated
 */
public interface STANDARDLENGTHTYPE extends DIAGCODEDTYPE {
	/**
	 * Returns the value of the '<em><b>BITLENGTH</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BITLENGTH</em>' attribute.
	 * @see #isSetBITLENGTH()
	 * @see #unsetBITLENGTH()
	 * @see #setBITLENGTH(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getSTANDARDLENGTHTYPE_BITLENGTH()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='BIT-LENGTH' namespace='##targetNamespace'"
	 * @generated
	 */
	long getBITLENGTH();

	/**
	 * Sets the value of the '{@link OdxXhtml.STANDARDLENGTHTYPE#getBITLENGTH <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BITLENGTH</em>' attribute.
	 * @see #isSetBITLENGTH()
	 * @see #unsetBITLENGTH()
	 * @see #getBITLENGTH()
	 * @generated
	 */
	void setBITLENGTH(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.STANDARDLENGTHTYPE#getBITLENGTH <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBITLENGTH()
	 * @see #getBITLENGTH()
	 * @see #setBITLENGTH(long)
	 * @generated
	 */
	void unsetBITLENGTH();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.STANDARDLENGTHTYPE#getBITLENGTH <em>BITLENGTH</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BITLENGTH</em>' attribute is set.
	 * @see #unsetBITLENGTH()
	 * @see #getBITLENGTH()
	 * @see #setBITLENGTH(long)
	 * @generated
	 */
	boolean isSetBITLENGTH();

	/**
	 * Returns the value of the '<em><b>BITMASK</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BITMASK</em>' attribute.
	 * @see #setBITMASK(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getSTANDARDLENGTHTYPE_BITMASK()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary"
	 *        extendedMetaData="kind='element' name='BIT-MASK' namespace='##targetNamespace'"
	 * @generated
	 */
	byte[] getBITMASK();

	/**
	 * Sets the value of the '{@link OdxXhtml.STANDARDLENGTHTYPE#getBITMASK <em>BITMASK</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BITMASK</em>' attribute.
	 * @see #getBITMASK()
	 * @generated
	 */
	void setBITMASK(byte[] value);

} // STANDARDLENGTHTYPE
