/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RELATEDDIAGCOMMREF</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.RELATEDDIAGCOMMREF#getRELATIONTYPE <em>RELATIONTYPE</em>}</li>
 *   <li>{@link OdxXhtml.RELATEDDIAGCOMMREF#getDOCREF <em>DOCREF</em>}</li>
 *   <li>{@link OdxXhtml.RELATEDDIAGCOMMREF#getDOCTYPE <em>DOCTYPE</em>}</li>
 *   <li>{@link OdxXhtml.RELATEDDIAGCOMMREF#getIDREF <em>IDREF</em>}</li>
 *   <li>{@link OdxXhtml.RELATEDDIAGCOMMREF#getREVISION <em>REVISION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDIAGCOMMREF()
 * @model extendedMetaData="name='RELATED-DIAG-COMM-REF' kind='elementOnly'"
 * @generated
 */
public interface RELATEDDIAGCOMMREF extends EObject {
	/**
	 * Returns the value of the '<em><b>RELATIONTYPE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RELATIONTYPE</em>' attribute.
	 * @see #setRELATIONTYPE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDIAGCOMMREF_RELATIONTYPE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='RELATION-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getRELATIONTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.RELATEDDIAGCOMMREF#getRELATIONTYPE <em>RELATIONTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RELATIONTYPE</em>' attribute.
	 * @see #getRELATIONTYPE()
	 * @generated
	 */
	void setRELATIONTYPE(String value);

	/**
	 * Returns the value of the '<em><b>DOCREF</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCREF</em>' attribute.
	 * @see #setDOCREF(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDIAGCOMMREF_DOCREF()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='DOCREF' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDOCREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.RELATEDDIAGCOMMREF#getDOCREF <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOCREF</em>' attribute.
	 * @see #getDOCREF()
	 * @generated
	 */
	void setDOCREF(String value);

	/**
	 * Returns the value of the '<em><b>DOCTYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.DOCTYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCTYPE</em>' attribute.
	 * @see OdxXhtml.DOCTYPE
	 * @see #isSetDOCTYPE()
	 * @see #unsetDOCTYPE()
	 * @see #setDOCTYPE(DOCTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDIAGCOMMREF_DOCTYPE()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='DOCTYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	DOCTYPE getDOCTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.RELATEDDIAGCOMMREF#getDOCTYPE <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOCTYPE</em>' attribute.
	 * @see OdxXhtml.DOCTYPE
	 * @see #isSetDOCTYPE()
	 * @see #unsetDOCTYPE()
	 * @see #getDOCTYPE()
	 * @generated
	 */
	void setDOCTYPE(DOCTYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.RELATEDDIAGCOMMREF#getDOCTYPE <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDOCTYPE()
	 * @see #getDOCTYPE()
	 * @see #setDOCTYPE(DOCTYPE)
	 * @generated
	 */
	void unsetDOCTYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.RELATEDDIAGCOMMREF#getDOCTYPE <em>DOCTYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>DOCTYPE</em>' attribute is set.
	 * @see #unsetDOCTYPE()
	 * @see #getDOCTYPE()
	 * @see #setDOCTYPE(DOCTYPE)
	 * @generated
	 */
	boolean isSetDOCTYPE();

	/**
	 * Returns the value of the '<em><b>IDREF</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IDREF</em>' attribute.
	 * @see #setIDREF(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDIAGCOMMREF_IDREF()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='ID-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	String getIDREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.RELATEDDIAGCOMMREF#getIDREF <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IDREF</em>' attribute.
	 * @see #getIDREF()
	 * @generated
	 */
	void setIDREF(String value);

	/**
	 * Returns the value of the '<em><b>REVISION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REVISION</em>' attribute.
	 * @see #setREVISION(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getRELATEDDIAGCOMMREF_REVISION()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='REVISION' namespace='##targetNamespace'"
	 * @generated
	 */
	String getREVISION();

	/**
	 * Sets the value of the '{@link OdxXhtml.RELATEDDIAGCOMMREF#getREVISION <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REVISION</em>' attribute.
	 * @see #getREVISION()
	 * @generated
	 */
	void setREVISION(String value);

} // RELATEDDIAGCOMMREF
