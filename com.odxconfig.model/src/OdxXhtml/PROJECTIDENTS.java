/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROJECTIDENTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PROJECTIDENTS#getPROJECTIDENT <em>PROJECTIDENT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTIDENTS()
 * @model extendedMetaData="name='PROJECT-IDENTS' kind='elementOnly'"
 * @generated
 */
public interface PROJECTIDENTS extends EObject {
	/**
	 * Returns the value of the '<em><b>PROJECTIDENT</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.PROJECTIDENT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROJECTIDENT</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTIDENTS_PROJECTIDENT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PROJECT-IDENT' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PROJECTIDENT> getPROJECTIDENT();

} // PROJECTIDENTS
