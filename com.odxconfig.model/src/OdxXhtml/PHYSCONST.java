/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PHYSCONST</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PHYSCONST#getPHYSCONSTANTVALUE <em>PHYSCONSTANTVALUE</em>}</li>
 *   <li>{@link OdxXhtml.PHYSCONST#getDOPREF <em>DOPREF</em>}</li>
 *   <li>{@link OdxXhtml.PHYSCONST#getDOPSNREF <em>DOPSNREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSCONST()
 * @model extendedMetaData="name='PHYS-CONST' kind='elementOnly'"
 * @generated
 */
public interface PHYSCONST extends POSITIONABLEPARAM {
	/**
	 * Returns the value of the '<em><b>PHYSCONSTANTVALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSCONSTANTVALUE</em>' attribute.
	 * @see #setPHYSCONSTANTVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSCONST_PHYSCONSTANTVALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='PHYS-CONSTANT-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPHYSCONSTANTVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSCONST#getPHYSCONSTANTVALUE <em>PHYSCONSTANTVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSCONSTANTVALUE</em>' attribute.
	 * @see #getPHYSCONSTANTVALUE()
	 * @generated
	 */
	void setPHYSCONSTANTVALUE(String value);

	/**
	 * Returns the value of the '<em><b>DOPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPREF</em>' containment reference.
	 * @see #setDOPREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSCONST_DOPREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DOP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDOPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSCONST#getDOPREF <em>DOPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPREF</em>' containment reference.
	 * @see #getDOPREF()
	 * @generated
	 */
	void setDOPREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>DOPSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPSNREF</em>' containment reference.
	 * @see #setDOPSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSCONST_DOPSNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DOP-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDOPSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSCONST#getDOPSNREF <em>DOPSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPSNREF</em>' containment reference.
	 * @see #getDOPSNREF()
	 * @generated
	 */
	void setDOPSNREF(SNREF value);

} // PHYSCONST
