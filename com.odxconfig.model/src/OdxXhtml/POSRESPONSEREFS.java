/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>POSRESPONSEREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.POSRESPONSEREFS#getPOSRESPONSEREF <em>POSRESPONSEREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPOSRESPONSEREFS()
 * @model extendedMetaData="name='POS-RESPONSE-REFS' kind='elementOnly'"
 * @generated
 */
public interface POSRESPONSEREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>POSRESPONSEREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>POSRESPONSEREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPOSRESPONSEREFS_POSRESPONSEREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='POS-RESPONSE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getPOSRESPONSEREF();

} // POSRESPONSEREFS
