/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OUTPUTPARAM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.OUTPUTPARAM#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.OUTPUTPARAM#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.OUTPUTPARAM#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.OUTPUTPARAM#getDOPBASEREF <em>DOPBASEREF</em>}</li>
 *   <li>{@link OdxXhtml.OUTPUTPARAM#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.OUTPUTPARAM#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.OUTPUTPARAM#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAM()
 * @model extendedMetaData="name='OUTPUT-PARAM' kind='elementOnly'"
 * @generated
 */
public interface OUTPUTPARAM extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAM_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.OUTPUTPARAM#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAM_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.OUTPUTPARAM#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAM_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.OUTPUTPARAM#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>DOPBASEREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPBASEREF</em>' containment reference.
	 * @see #setDOPBASEREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAM_DOPBASEREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DOP-BASE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDOPBASEREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.OUTPUTPARAM#getDOPBASEREF <em>DOPBASEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPBASEREF</em>' containment reference.
	 * @see #getDOPBASEREF()
	 * @generated
	 */
	void setDOPBASEREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAM_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.OUTPUTPARAM#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAM_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.OUTPUTPARAM#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

	/**
	 * Returns the value of the '<em><b>SEMANTIC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SEMANTIC</em>' attribute.
	 * @see #setSEMANTIC(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getOUTPUTPARAM_SEMANTIC()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='SEMANTIC' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSEMANTIC();

	/**
	 * Sets the value of the '{@link OdxXhtml.OUTPUTPARAM#getSEMANTIC <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SEMANTIC</em>' attribute.
	 * @see #getSEMANTIC()
	 * @generated
	 */
	void setSEMANTIC(String value);

} // OUTPUTPARAM
