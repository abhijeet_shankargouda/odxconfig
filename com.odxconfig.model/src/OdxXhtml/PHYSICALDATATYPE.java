/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>PHYSICALDATATYPE</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALDATATYPE()
 * @model extendedMetaData="name='PHYSICAL-DATA-TYPE'"
 * @generated
 */
public enum PHYSICALDATATYPE implements Enumerator {
	/**
	 * The '<em><b>AINT32</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT32_VALUE
	 * @generated
	 * @ordered
	 */
	AINT32(0, "AINT32", "A_INT32"),

	/**
	 * The '<em><b>AUINT32</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT32_VALUE
	 * @generated
	 * @ordered
	 */
	AUINT32(1, "AUINT32", "A_UINT32"),

	/**
	 * The '<em><b>AFLOAT32</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFLOAT32_VALUE
	 * @generated
	 * @ordered
	 */
	AFLOAT32(2, "AFLOAT32", "A_FLOAT32"),

	/**
	 * The '<em><b>AFLOAT64</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFLOAT64_VALUE
	 * @generated
	 * @ordered
	 */
	AFLOAT64(3, "AFLOAT64", "A_FLOAT64"),

	/**
	 * The '<em><b>AUNICODE2STRING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUNICODE2STRING_VALUE
	 * @generated
	 * @ordered
	 */
	AUNICODE2STRING(4, "AUNICODE2STRING", "A_UNICODE2STRING"),

	/**
	 * The '<em><b>ABYTEFIELD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABYTEFIELD_VALUE
	 * @generated
	 * @ordered
	 */
	ABYTEFIELD(5, "ABYTEFIELD", "A_BYTEFIELD");

	/**
	 * The '<em><b>AINT32</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT32
	 * @model literal="A_INT32"
	 * @generated
	 * @ordered
	 */
	public static final int AINT32_VALUE = 0;

	/**
	 * The '<em><b>AUINT32</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT32
	 * @model literal="A_UINT32"
	 * @generated
	 * @ordered
	 */
	public static final int AUINT32_VALUE = 1;

	/**
	 * The '<em><b>AFLOAT32</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFLOAT32
	 * @model literal="A_FLOAT32"
	 * @generated
	 * @ordered
	 */
	public static final int AFLOAT32_VALUE = 2;

	/**
	 * The '<em><b>AFLOAT64</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFLOAT64
	 * @model literal="A_FLOAT64"
	 * @generated
	 * @ordered
	 */
	public static final int AFLOAT64_VALUE = 3;

	/**
	 * The '<em><b>AUNICODE2STRING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUNICODE2STRING
	 * @model literal="A_UNICODE2STRING"
	 * @generated
	 * @ordered
	 */
	public static final int AUNICODE2STRING_VALUE = 4;

	/**
	 * The '<em><b>ABYTEFIELD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABYTEFIELD
	 * @model literal="A_BYTEFIELD"
	 * @generated
	 * @ordered
	 */
	public static final int ABYTEFIELD_VALUE = 5;

	/**
	 * An array of all the '<em><b>PHYSICALDATATYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final PHYSICALDATATYPE[] VALUES_ARRAY =
		new PHYSICALDATATYPE[] {
			AINT32,
			AUINT32,
			AFLOAT32,
			AFLOAT64,
			AUNICODE2STRING,
			ABYTEFIELD,
		};

	/**
	 * A public read-only list of all the '<em><b>PHYSICALDATATYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<PHYSICALDATATYPE> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>PHYSICALDATATYPE</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static PHYSICALDATATYPE get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			PHYSICALDATATYPE result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>PHYSICALDATATYPE</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static PHYSICALDATATYPE getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			PHYSICALDATATYPE result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>PHYSICALDATATYPE</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static PHYSICALDATATYPE get(int value) {
		switch (value) {
			case AINT32_VALUE: return AINT32;
			case AUINT32_VALUE: return AUINT32;
			case AFLOAT32_VALUE: return AFLOAT32;
			case AFLOAT64_VALUE: return AFLOAT64;
			case AUNICODE2STRING_VALUE: return AUNICODE2STRING;
			case ABYTEFIELD_VALUE: return ABYTEFIELD;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private PHYSICALDATATYPE(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //PHYSICALDATATYPE
