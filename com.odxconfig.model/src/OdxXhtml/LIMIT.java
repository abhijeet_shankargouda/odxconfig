/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LIMIT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.LIMIT#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.LIMIT#getINTERVALTYPE <em>INTERVALTYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLIMIT()
 * @model extendedMetaData="name='LIMIT' kind='simple'"
 * @generated
 */
public interface LIMIT extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getLIMIT_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link OdxXhtml.LIMIT#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>INTERVALTYPE</b></em>' attribute.
	 * The default value is <code>"CLOSED"</code>.
	 * The literals are from the enumeration {@link OdxXhtml.INTERVALTYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INTERVALTYPE</em>' attribute.
	 * @see OdxXhtml.INTERVALTYPE
	 * @see #isSetINTERVALTYPE()
	 * @see #unsetINTERVALTYPE()
	 * @see #setINTERVALTYPE(INTERVALTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getLIMIT_INTERVALTYPE()
	 * @model default="CLOSED" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='INTERVAL-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	INTERVALTYPE getINTERVALTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.LIMIT#getINTERVALTYPE <em>INTERVALTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>INTERVALTYPE</em>' attribute.
	 * @see OdxXhtml.INTERVALTYPE
	 * @see #isSetINTERVALTYPE()
	 * @see #unsetINTERVALTYPE()
	 * @see #getINTERVALTYPE()
	 * @generated
	 */
	void setINTERVALTYPE(INTERVALTYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.LIMIT#getINTERVALTYPE <em>INTERVALTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetINTERVALTYPE()
	 * @see #getINTERVALTYPE()
	 * @see #setINTERVALTYPE(INTERVALTYPE)
	 * @generated
	 */
	void unsetINTERVALTYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.LIMIT#getINTERVALTYPE <em>INTERVALTYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>INTERVALTYPE</em>' attribute is set.
	 * @see #unsetINTERVALTYPE()
	 * @see #getINTERVALTYPE()
	 * @see #setINTERVALTYPE(INTERVALTYPE)
	 * @generated
	 */
	boolean isSetINTERVALTYPE();

} // LIMIT
