/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IDENTVALUE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.IDENTVALUE#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.IDENTVALUE#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getIDENTVALUE()
 * @model extendedMetaData="name='IDENT-VALUE' kind='simple'"
 * @generated
 */
public interface IDENTVALUE extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getIDENTVALUE_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link OdxXhtml.IDENTVALUE#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>TYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.IDENTVALUETYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TYPE</em>' attribute.
	 * @see OdxXhtml.IDENTVALUETYPE
	 * @see #isSetTYPE()
	 * @see #unsetTYPE()
	 * @see #setTYPE(IDENTVALUETYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getIDENTVALUE_TYPE()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	IDENTVALUETYPE getTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.IDENTVALUE#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TYPE</em>' attribute.
	 * @see OdxXhtml.IDENTVALUETYPE
	 * @see #isSetTYPE()
	 * @see #unsetTYPE()
	 * @see #getTYPE()
	 * @generated
	 */
	void setTYPE(IDENTVALUETYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.IDENTVALUE#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTYPE()
	 * @see #getTYPE()
	 * @see #setTYPE(IDENTVALUETYPE)
	 * @generated
	 */
	void unsetTYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.IDENTVALUE#getTYPE <em>TYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TYPE</em>' attribute is set.
	 * @see #unsetTYPE()
	 * @see #getTYPE()
	 * @see #setTYPE(IDENTVALUETYPE)
	 * @generated
	 */
	boolean isSetTYPE();

} // IDENTVALUE
