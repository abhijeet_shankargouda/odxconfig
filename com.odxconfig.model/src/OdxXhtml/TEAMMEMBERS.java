/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TEAMMEMBERS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.TEAMMEMBERS#getTEAMMEMBER <em>TEAMMEMBER</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBERS()
 * @model extendedMetaData="name='TEAM-MEMBERS' kind='elementOnly'"
 * @generated
 */
public interface TEAMMEMBERS extends EObject {
	/**
	 * Returns the value of the '<em><b>TEAMMEMBER</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.TEAMMEMBER}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TEAMMEMBER</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBERS_TEAMMEMBER()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='TEAM-MEMBER' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TEAMMEMBER> getTEAMMEMBER();

} // TEAMMEMBERS
