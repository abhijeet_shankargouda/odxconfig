/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ABLOCK</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ABLOCK#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.ABLOCK#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.ABLOCK#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.ABLOCK#getCATEGORY <em>CATEGORY</em>}</li>
 *   <li>{@link OdxXhtml.ABLOCK#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.ABLOCK#getFILES <em>FILES</em>}</li>
 *   <li>{@link OdxXhtml.ABLOCK#getUPD <em>UPD</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getABLOCK()
 * @model extendedMetaData="name='ABLOCK' kind='elementOnly'"
 * @generated
 */
public interface ABLOCK extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getABLOCK_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.ABLOCK#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT1)
	 * @see OdxXhtml.OdxXhtmlPackage#getABLOCK_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT1 getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.ABLOCK#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT1 value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION1)
	 * @see OdxXhtml.OdxXhtmlPackage#getABLOCK_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION1 getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.ABLOCK#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION1 value);

	/**
	 * Returns the value of the '<em><b>CATEGORY</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CATEGORY</em>' attribute.
	 * @see #setCATEGORY(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getABLOCK_CATEGORY()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='CATEGORY' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCATEGORY();

	/**
	 * Sets the value of the '{@link OdxXhtml.ABLOCK#getCATEGORY <em>CATEGORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CATEGORY</em>' attribute.
	 * @see #getCATEGORY()
	 * @generated
	 */
	void setCATEGORY(String value);

	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA1)
	 * @see OdxXhtml.OdxXhtmlPackage#getABLOCK_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA1 getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.ABLOCK#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA1 value);

	/**
	 * Returns the value of the '<em><b>FILES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FILES</em>' containment reference.
	 * @see #setFILES(FILESType)
	 * @see OdxXhtml.OdxXhtmlPackage#getABLOCK_FILES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FILES' namespace='##targetNamespace'"
	 * @generated
	 */
	FILESType getFILES();

	/**
	 * Sets the value of the '{@link OdxXhtml.ABLOCK#getFILES <em>FILES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FILES</em>' containment reference.
	 * @see #getFILES()
	 * @generated
	 */
	void setFILES(FILESType value);

	/**
	 * Returns the value of the '<em><b>UPD</b></em>' attribute.
	 * The default value is <code>"UNDEFINED"</code>.
	 * The literals are from the enumeration {@link OdxXhtml.UPDSTATUS}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UPD</em>' attribute.
	 * @see OdxXhtml.UPDSTATUS
	 * @see #isSetUPD()
	 * @see #unsetUPD()
	 * @see #setUPD(UPDSTATUS)
	 * @see OdxXhtml.OdxXhtmlPackage#getABLOCK_UPD()
	 * @model default="UNDEFINED" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='UPD' namespace='##targetNamespace'"
	 * @generated
	 */
	UPDSTATUS getUPD();

	/**
	 * Sets the value of the '{@link OdxXhtml.ABLOCK#getUPD <em>UPD</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UPD</em>' attribute.
	 * @see OdxXhtml.UPDSTATUS
	 * @see #isSetUPD()
	 * @see #unsetUPD()
	 * @see #getUPD()
	 * @generated
	 */
	void setUPD(UPDSTATUS value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.ABLOCK#getUPD <em>UPD</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUPD()
	 * @see #getUPD()
	 * @see #setUPD(UPDSTATUS)
	 * @generated
	 */
	void unsetUPD();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.ABLOCK#getUPD <em>UPD</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>UPD</em>' attribute is set.
	 * @see #unsetUPD()
	 * @see #getUPD()
	 * @see #setUPD(UPDSTATUS)
	 * @generated
	 */
	boolean isSetUPD();

} // ABLOCK
