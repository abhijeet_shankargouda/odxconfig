/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PHYSICALTYPE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PHYSICALTYPE#getPRECISION <em>PRECISION</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALTYPE#getBASEDATATYPE <em>BASEDATATYPE</em>}</li>
 *   <li>{@link OdxXhtml.PHYSICALTYPE#getDISPLAYRADIX <em>DISPLAYRADIX</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALTYPE()
 * @model extendedMetaData="name='PHYSICAL-TYPE' kind='elementOnly'"
 * @generated
 */
public interface PHYSICALTYPE extends EObject {
	/**
	 * Returns the value of the '<em><b>PRECISION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PRECISION</em>' attribute.
	 * @see #isSetPRECISION()
	 * @see #unsetPRECISION()
	 * @see #setPRECISION(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALTYPE_PRECISION()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='element' name='PRECISION' namespace='##targetNamespace'"
	 * @generated
	 */
	long getPRECISION();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALTYPE#getPRECISION <em>PRECISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PRECISION</em>' attribute.
	 * @see #isSetPRECISION()
	 * @see #unsetPRECISION()
	 * @see #getPRECISION()
	 * @generated
	 */
	void setPRECISION(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALTYPE#getPRECISION <em>PRECISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPRECISION()
	 * @see #getPRECISION()
	 * @see #setPRECISION(long)
	 * @generated
	 */
	void unsetPRECISION();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALTYPE#getPRECISION <em>PRECISION</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>PRECISION</em>' attribute is set.
	 * @see #unsetPRECISION()
	 * @see #getPRECISION()
	 * @see #setPRECISION(long)
	 * @generated
	 */
	boolean isSetPRECISION();

	/**
	 * Returns the value of the '<em><b>BASEDATATYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.PHYSICALDATATYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BASEDATATYPE</em>' attribute.
	 * @see OdxXhtml.PHYSICALDATATYPE
	 * @see #isSetBASEDATATYPE()
	 * @see #unsetBASEDATATYPE()
	 * @see #setBASEDATATYPE(PHYSICALDATATYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALTYPE_BASEDATATYPE()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='BASE-DATA-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	PHYSICALDATATYPE getBASEDATATYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALTYPE#getBASEDATATYPE <em>BASEDATATYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BASEDATATYPE</em>' attribute.
	 * @see OdxXhtml.PHYSICALDATATYPE
	 * @see #isSetBASEDATATYPE()
	 * @see #unsetBASEDATATYPE()
	 * @see #getBASEDATATYPE()
	 * @generated
	 */
	void setBASEDATATYPE(PHYSICALDATATYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALTYPE#getBASEDATATYPE <em>BASEDATATYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBASEDATATYPE()
	 * @see #getBASEDATATYPE()
	 * @see #setBASEDATATYPE(PHYSICALDATATYPE)
	 * @generated
	 */
	void unsetBASEDATATYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALTYPE#getBASEDATATYPE <em>BASEDATATYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BASEDATATYPE</em>' attribute is set.
	 * @see #unsetBASEDATATYPE()
	 * @see #getBASEDATATYPE()
	 * @see #setBASEDATATYPE(PHYSICALDATATYPE)
	 * @generated
	 */
	boolean isSetBASEDATATYPE();

	/**
	 * Returns the value of the '<em><b>DISPLAYRADIX</b></em>' attribute.
	 * The default value is <code>"DEC"</code>.
	 * The literals are from the enumeration {@link OdxXhtml.RADIX}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DISPLAYRADIX</em>' attribute.
	 * @see OdxXhtml.RADIX
	 * @see #isSetDISPLAYRADIX()
	 * @see #unsetDISPLAYRADIX()
	 * @see #setDISPLAYRADIX(RADIX)
	 * @see OdxXhtml.OdxXhtmlPackage#getPHYSICALTYPE_DISPLAYRADIX()
	 * @model default="DEC" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='DISPLAY-RADIX' namespace='##targetNamespace'"
	 * @generated
	 */
	RADIX getDISPLAYRADIX();

	/**
	 * Sets the value of the '{@link OdxXhtml.PHYSICALTYPE#getDISPLAYRADIX <em>DISPLAYRADIX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DISPLAYRADIX</em>' attribute.
	 * @see OdxXhtml.RADIX
	 * @see #isSetDISPLAYRADIX()
	 * @see #unsetDISPLAYRADIX()
	 * @see #getDISPLAYRADIX()
	 * @generated
	 */
	void setDISPLAYRADIX(RADIX value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PHYSICALTYPE#getDISPLAYRADIX <em>DISPLAYRADIX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDISPLAYRADIX()
	 * @see #getDISPLAYRADIX()
	 * @see #setDISPLAYRADIX(RADIX)
	 * @generated
	 */
	void unsetDISPLAYRADIX();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PHYSICALTYPE#getDISPLAYRADIX <em>DISPLAYRADIX</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>DISPLAYRADIX</em>' attribute is set.
	 * @see #unsetDISPLAYRADIX()
	 * @see #getDISPLAYRADIX()
	 * @see #setDISPLAYRADIX(RADIX)
	 * @generated
	 */
	boolean isSetDISPLAYRADIX();

} // PHYSICALTYPE
