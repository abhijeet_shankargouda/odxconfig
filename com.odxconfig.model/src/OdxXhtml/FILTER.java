/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FILTER</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FILTER#getFILTERSTART <em>FILTERSTART</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFILTER()
 * @model extendedMetaData="name='FILTER' kind='elementOnly'"
 * @generated
 */
public interface FILTER extends EObject {
	/**
	 * Returns the value of the '<em><b>FILTERSTART</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FILTERSTART</em>' attribute.
	 * @see #setFILTERSTART(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getFILTER_FILTERSTART()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary" required="true"
	 *        extendedMetaData="kind='element' name='FILTER-START' namespace='##targetNamespace'"
	 * @generated
	 */
	byte[] getFILTERSTART();

	/**
	 * Sets the value of the '{@link OdxXhtml.FILTER#getFILTERSTART <em>FILTERSTART</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FILTERSTART</em>' attribute.
	 * @see #getFILTERSTART()
	 * @generated
	 */
	void setFILTERSTART(byte[] value);

} // FILTER
