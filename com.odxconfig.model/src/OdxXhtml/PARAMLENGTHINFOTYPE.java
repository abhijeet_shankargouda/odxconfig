/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PARAMLENGTHINFOTYPE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PARAMLENGTHINFOTYPE#getLENGTHKEYREF <em>LENGTHKEYREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPARAMLENGTHINFOTYPE()
 * @model extendedMetaData="name='PARAM-LENGTH-INFO-TYPE' kind='elementOnly'"
 * @generated
 */
public interface PARAMLENGTHINFOTYPE extends DIAGCODEDTYPE {
	/**
	 * Returns the value of the '<em><b>LENGTHKEYREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LENGTHKEYREF</em>' containment reference.
	 * @see #setLENGTHKEYREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getPARAMLENGTHINFOTYPE_LENGTHKEYREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='LENGTH-KEY-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getLENGTHKEYREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.PARAMLENGTHINFOTYPE#getLENGTHKEYREF <em>LENGTHKEYREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LENGTHKEYREF</em>' containment reference.
	 * @see #getLENGTHKEYREF()
	 * @generated
	 */
	void setLENGTHKEYREF(ODXLINK value);

} // PARAMLENGTHINFOTYPE
