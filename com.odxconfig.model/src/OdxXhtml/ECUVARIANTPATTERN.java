/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUVARIANTPATTERN</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUVARIANTPATTERN#getMATCHINGPARAMETERS <em>MATCHINGPARAMETERS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUVARIANTPATTERN()
 * @model extendedMetaData="name='ECU-VARIANT-PATTERN' kind='elementOnly'"
 * @generated
 */
public interface ECUVARIANTPATTERN extends EObject {
	/**
	 * Returns the value of the '<em><b>MATCHINGPARAMETERS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MATCHINGPARAMETERS</em>' containment reference.
	 * @see #setMATCHINGPARAMETERS(MATCHINGPARAMETERS)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUVARIANTPATTERN_MATCHINGPARAMETERS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MATCHING-PARAMETERS' namespace='##targetNamespace'"
	 * @generated
	 */
	MATCHINGPARAMETERS getMATCHINGPARAMETERS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUVARIANTPATTERN#getMATCHINGPARAMETERS <em>MATCHINGPARAMETERS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MATCHINGPARAMETERS</em>' containment reference.
	 * @see #getMATCHINGPARAMETERS()
	 * @generated
	 */
	void setMATCHINGPARAMETERS(MATCHINGPARAMETERS value);

} // ECUVARIANTPATTERN
