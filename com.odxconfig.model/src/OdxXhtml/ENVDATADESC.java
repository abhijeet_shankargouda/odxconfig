/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ENVDATADESC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ENVDATADESC#getPARAMSNREF <em>PARAMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.ENVDATADESC#getENVDATAS <em>ENVDATAS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getENVDATADESC()
 * @model extendedMetaData="name='ENV-DATA-DESC' kind='elementOnly'"
 * @generated
 */
public interface ENVDATADESC extends COMPLEXDOP {
	/**
	 * Returns the value of the '<em><b>PARAMSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PARAMSNREF</em>' containment reference.
	 * @see #setPARAMSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getENVDATADESC_PARAMSNREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PARAM-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getPARAMSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.ENVDATADESC#getPARAMSNREF <em>PARAMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PARAMSNREF</em>' containment reference.
	 * @see #getPARAMSNREF()
	 * @generated
	 */
	void setPARAMSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>ENVDATAS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENVDATAS</em>' containment reference.
	 * @see #setENVDATAS(ENVDATAS)
	 * @see OdxXhtml.OdxXhtmlPackage#getENVDATADESC_ENVDATAS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ENV-DATAS' namespace='##targetNamespace'"
	 * @generated
	 */
	ENVDATAS getENVDATAS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ENVDATADESC#getENVDATAS <em>ENVDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ENVDATAS</em>' containment reference.
	 * @see #getENVDATAS()
	 * @generated
	 */
	void setENVDATAS(ENVDATAS value);

} // ENVDATADESC
