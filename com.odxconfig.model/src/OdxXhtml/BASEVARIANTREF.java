/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BASEVARIANTREF</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getBASEVARIANTREF()
 * @model extendedMetaData="name='BASE-VARIANT-REF' kind='elementOnly'"
 * @generated
 */
public interface BASEVARIANTREF extends PARENTREF {
} // BASEVARIANTREF
