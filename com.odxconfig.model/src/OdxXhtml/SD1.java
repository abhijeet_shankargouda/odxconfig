/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SD1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SD1#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.SD1#getSI <em>SI</em>}</li>
 *   <li>{@link OdxXhtml.SD1#getTI <em>TI</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSD1()
 * @model extendedMetaData="name='SD' kind='simple'"
 * @generated
 */
public interface SD1 extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSD1_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link OdxXhtml.SD1#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>SI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SI</em>' attribute.
	 * @see #setSI(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSD1_SI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='SI' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSI();

	/**
	 * Sets the value of the '{@link OdxXhtml.SD1#getSI <em>SI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SI</em>' attribute.
	 * @see #getSI()
	 * @generated
	 */
	void setSI(String value);

	/**
	 * Returns the value of the '<em><b>TI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TI</em>' attribute.
	 * @see #setTI(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSD1_TI()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='TI' namespace='##targetNamespace'"
	 * @generated
	 */
	String getTI();

	/**
	 * Sets the value of the '{@link OdxXhtml.SD1#getTI <em>TI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TI</em>' attribute.
	 * @see #getTI()
	 * @generated
	 */
	void setTI(String value);

} // SD1
