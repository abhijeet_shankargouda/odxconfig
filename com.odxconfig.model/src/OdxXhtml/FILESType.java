/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FILES Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FILESType#getFILE <em>FILE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFILESType()
 * @model extendedMetaData="name='FILES_._type' kind='elementOnly'"
 * @generated
 */
public interface FILESType extends EObject {
	/**
	 * Returns the value of the '<em><b>FILE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.FILE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FILE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getFILESType_FILE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FILE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FILE> getFILE();

} // FILESType
