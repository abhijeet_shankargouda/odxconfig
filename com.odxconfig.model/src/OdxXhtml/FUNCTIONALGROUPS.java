/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FUNCTIONALGROUPS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FUNCTIONALGROUPS#getFUNCTIONALGROUP <em>FUNCTIONALGROUP</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTIONALGROUPS()
 * @model extendedMetaData="name='FUNCTIONAL-GROUPS' kind='elementOnly'"
 * @generated
 */
public interface FUNCTIONALGROUPS extends EObject {
	/**
	 * Returns the value of the '<em><b>FUNCTIONALGROUP</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.FUNCTIONALGROUP}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FUNCTIONALGROUP</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTIONALGROUPS_FUNCTIONALGROUP()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FUNCTIONAL-GROUP' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FUNCTIONALGROUP> getFUNCTIONALGROUP();

} // FUNCTIONALGROUPS
