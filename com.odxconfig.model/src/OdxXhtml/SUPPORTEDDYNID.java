/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SUPPORTEDDYNID</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SUPPORTEDDYNID#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSUPPORTEDDYNID()
 * @model extendedMetaData="name='SUPPORTED-DYN-ID' kind='simple'"
 * @generated
 */
public interface SUPPORTEDDYNID extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getSUPPORTEDDYNID_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	byte[] getValue();

	/**
	 * Sets the value of the '{@link OdxXhtml.SUPPORTEDDYNID#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(byte[] value);

} // SUPPORTEDDYNID
