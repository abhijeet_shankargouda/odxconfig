/**
 */
package OdxXhtml.util;

import OdxXhtml.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage
 * @generated
 */
public class OdxXhtmlAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OdxXhtmlPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdxXhtmlAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = OdxXhtmlPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OdxXhtmlSwitch<Adapter> modelSwitch =
		new OdxXhtmlSwitch<Adapter>() {
			@Override
			public Adapter caseABLOCK(ABLOCK object) {
				return createABLOCKAdapter();
			}
			@Override
			public Adapter caseABLOCKSType(ABLOCKSType object) {
				return createABLOCKSTypeAdapter();
			}
			@Override
			public Adapter caseACCESSLEVEL(ACCESSLEVEL object) {
				return createACCESSLEVELAdapter();
			}
			@Override
			public Adapter caseACCESSLEVELS(ACCESSLEVELS object) {
				return createACCESSLEVELSAdapter();
			}
			@Override
			public Adapter caseADDRDEFFILTER(ADDRDEFFILTER object) {
				return createADDRDEFFILTERAdapter();
			}
			@Override
			public Adapter caseADDRDEFPHYSSEGMENT(ADDRDEFPHYSSEGMENT object) {
				return createADDRDEFPHYSSEGMENTAdapter();
			}
			@Override
			public Adapter caseADMINDATA(ADMINDATA object) {
				return createADMINDATAAdapter();
			}
			@Override
			public Adapter caseADMINDATA1(ADMINDATA1 object) {
				return createADMINDATA1Adapter();
			}
			@Override
			public Adapter caseALLVALUE(ALLVALUE object) {
				return createALLVALUEAdapter();
			}
			@Override
			public Adapter caseAUDIENCE(AUDIENCE object) {
				return createAUDIENCEAdapter();
			}
			@Override
			public Adapter caseAUTMETHOD(AUTMETHOD object) {
				return createAUTMETHODAdapter();
			}
			@Override
			public Adapter caseAUTMETHODS(AUTMETHODS object) {
				return createAUTMETHODSAdapter();
			}
			@Override
			public Adapter caseBASEVARIANT(BASEVARIANT object) {
				return createBASEVARIANTAdapter();
			}
			@Override
			public Adapter caseBASEVARIANTREF(BASEVARIANTREF object) {
				return createBASEVARIANTREFAdapter();
			}
			@Override
			public Adapter caseBASEVARIANTS(BASEVARIANTS object) {
				return createBASEVARIANTSAdapter();
			}
			@Override
			public Adapter caseBASICSTRUCTURE(BASICSTRUCTURE object) {
				return createBASICSTRUCTUREAdapter();
			}
			@Override
			public Adapter caseBlock(Block object) {
				return createBlockAdapter();
			}
			@Override
			public Adapter caseBType(BType object) {
				return createBTypeAdapter();
			}
			@Override
			public Adapter caseCASE(CASE object) {
				return createCASEAdapter();
			}
			@Override
			public Adapter caseCASES(CASES object) {
				return createCASESAdapter();
			}
			@Override
			public Adapter caseCATALOG(CATALOG object) {
				return createCATALOGAdapter();
			}
			@Override
			public Adapter caseCHECKSUM(CHECKSUM object) {
				return createCHECKSUMAdapter();
			}
			@Override
			public Adapter caseCHECKSUMRESULT(CHECKSUMRESULT object) {
				return createCHECKSUMRESULTAdapter();
			}
			@Override
			public Adapter caseCHECKSUMS(CHECKSUMS object) {
				return createCHECKSUMSAdapter();
			}
			@Override
			public Adapter caseCODEDCONST(CODEDCONST object) {
				return createCODEDCONSTAdapter();
			}
			@Override
			public Adapter caseCOMMRELATION(COMMRELATION object) {
				return createCOMMRELATIONAdapter();
			}
			@Override
			public Adapter caseCOMMRELATIONS(COMMRELATIONS object) {
				return createCOMMRELATIONSAdapter();
			}
			@Override
			public Adapter caseCOMPANYDATA(COMPANYDATA object) {
				return createCOMPANYDATAAdapter();
			}
			@Override
			public Adapter caseCOMPANYDATA1(COMPANYDATA1 object) {
				return createCOMPANYDATA1Adapter();
			}
			@Override
			public Adapter caseCOMPANYDATAS(COMPANYDATAS object) {
				return createCOMPANYDATASAdapter();
			}
			@Override
			public Adapter caseCOMPANYDATASType(COMPANYDATASType object) {
				return createCOMPANYDATASTypeAdapter();
			}
			@Override
			public Adapter caseCOMPANYDOCINFO(COMPANYDOCINFO object) {
				return createCOMPANYDOCINFOAdapter();
			}
			@Override
			public Adapter caseCOMPANYDOCINFO1(COMPANYDOCINFO1 object) {
				return createCOMPANYDOCINFO1Adapter();
			}
			@Override
			public Adapter caseCOMPANYDOCINFOS(COMPANYDOCINFOS object) {
				return createCOMPANYDOCINFOSAdapter();
			}
			@Override
			public Adapter caseCOMPANYDOCINFOS1(COMPANYDOCINFOS1 object) {
				return createCOMPANYDOCINFOS1Adapter();
			}
			@Override
			public Adapter caseCOMPANYREVISIONINFO(COMPANYREVISIONINFO object) {
				return createCOMPANYREVISIONINFOAdapter();
			}
			@Override
			public Adapter caseCOMPANYREVISIONINFO1(COMPANYREVISIONINFO1 object) {
				return createCOMPANYREVISIONINFO1Adapter();
			}
			@Override
			public Adapter caseCOMPANYREVISIONINFOS(COMPANYREVISIONINFOS object) {
				return createCOMPANYREVISIONINFOSAdapter();
			}
			@Override
			public Adapter caseCOMPANYREVISIONINFOS1(COMPANYREVISIONINFOS1 object) {
				return createCOMPANYREVISIONINFOS1Adapter();
			}
			@Override
			public Adapter caseCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO object) {
				return createCOMPANYSPECIFICINFOAdapter();
			}
			@Override
			public Adapter caseCOMPANYSPECIFICINFO1(COMPANYSPECIFICINFO1 object) {
				return createCOMPANYSPECIFICINFO1Adapter();
			}
			@Override
			public Adapter caseCOMPARAM(COMPARAM object) {
				return createCOMPARAMAdapter();
			}
			@Override
			public Adapter caseCOMPARAMREF(COMPARAMREF object) {
				return createCOMPARAMREFAdapter();
			}
			@Override
			public Adapter caseCOMPARAMREFS(COMPARAMREFS object) {
				return createCOMPARAMREFSAdapter();
			}
			@Override
			public Adapter caseCOMPARAMS(COMPARAMS object) {
				return createCOMPARAMSAdapter();
			}
			@Override
			public Adapter caseCOMPARAMSPEC(COMPARAMSPEC object) {
				return createCOMPARAMSPECAdapter();
			}
			@Override
			public Adapter caseCOMPLEXDOP(COMPLEXDOP object) {
				return createCOMPLEXDOPAdapter();
			}
			@Override
			public Adapter caseCOMPUCONST(COMPUCONST object) {
				return createCOMPUCONSTAdapter();
			}
			@Override
			public Adapter caseCOMPUDEFAULTVALUE(COMPUDEFAULTVALUE object) {
				return createCOMPUDEFAULTVALUEAdapter();
			}
			@Override
			public Adapter caseCOMPUDENOMINATOR(COMPUDENOMINATOR object) {
				return createCOMPUDENOMINATORAdapter();
			}
			@Override
			public Adapter caseCOMPUINTERNALTOPHYS(COMPUINTERNALTOPHYS object) {
				return createCOMPUINTERNALTOPHYSAdapter();
			}
			@Override
			public Adapter caseCOMPUINVERSEVALUE(COMPUINVERSEVALUE object) {
				return createCOMPUINVERSEVALUEAdapter();
			}
			@Override
			public Adapter caseCOMPUMETHOD(COMPUMETHOD object) {
				return createCOMPUMETHODAdapter();
			}
			@Override
			public Adapter caseCOMPUNUMERATOR(COMPUNUMERATOR object) {
				return createCOMPUNUMERATORAdapter();
			}
			@Override
			public Adapter caseCOMPUPHYSTOINTERNAL(COMPUPHYSTOINTERNAL object) {
				return createCOMPUPHYSTOINTERNALAdapter();
			}
			@Override
			public Adapter caseCOMPURATIONALCOEFFS(COMPURATIONALCOEFFS object) {
				return createCOMPURATIONALCOEFFSAdapter();
			}
			@Override
			public Adapter caseCOMPUSCALE(COMPUSCALE object) {
				return createCOMPUSCALEAdapter();
			}
			@Override
			public Adapter caseCOMPUSCALES(COMPUSCALES object) {
				return createCOMPUSCALESAdapter();
			}
			@Override
			public Adapter caseDATABLOCK(DATABLOCK object) {
				return createDATABLOCKAdapter();
			}
			@Override
			public Adapter caseDATABLOCKREFS(DATABLOCKREFS object) {
				return createDATABLOCKREFSAdapter();
			}
			@Override
			public Adapter caseDATABLOCKS(DATABLOCKS object) {
				return createDATABLOCKSAdapter();
			}
			@Override
			public Adapter caseDATAFILE(DATAFILE object) {
				return createDATAFILEAdapter();
			}
			@Override
			public Adapter caseDATAFORMAT(DATAFORMAT object) {
				return createDATAFORMATAdapter();
			}
			@Override
			public Adapter caseDATAOBJECTPROP(DATAOBJECTPROP object) {
				return createDATAOBJECTPROPAdapter();
			}
			@Override
			public Adapter caseDATAOBJECTPROPS(DATAOBJECTPROPS object) {
				return createDATAOBJECTPROPSAdapter();
			}
			@Override
			public Adapter caseDEFAULTCASE(DEFAULTCASE object) {
				return createDEFAULTCASEAdapter();
			}
			@Override
			public Adapter caseDESCRIPTION(DESCRIPTION object) {
				return createDESCRIPTIONAdapter();
			}
			@Override
			public Adapter caseDESCRIPTION1(DESCRIPTION1 object) {
				return createDESCRIPTION1Adapter();
			}
			@Override
			public Adapter caseDETERMINENUMBEROFITEMS(DETERMINENUMBEROFITEMS object) {
				return createDETERMINENUMBEROFITEMSAdapter();
			}
			@Override
			public Adapter caseDIAGCODEDTYPE(DIAGCODEDTYPE object) {
				return createDIAGCODEDTYPEAdapter();
			}
			@Override
			public Adapter caseDIAGCOMM(DIAGCOMM object) {
				return createDIAGCOMMAdapter();
			}
			@Override
			public Adapter caseDIAGCOMMS(DIAGCOMMS object) {
				return createDIAGCOMMSAdapter();
			}
			@Override
			public Adapter caseDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC object) {
				return createDIAGDATADICTIONARYSPECAdapter();
			}
			@Override
			public Adapter caseDIAGLAYER(DIAGLAYER object) {
				return createDIAGLAYERAdapter();
			}
			@Override
			public Adapter caseDIAGLAYERCONTAINER(DIAGLAYERCONTAINER object) {
				return createDIAGLAYERCONTAINERAdapter();
			}
			@Override
			public Adapter caseDIAGLAYERREFS(DIAGLAYERREFS object) {
				return createDIAGLAYERREFSAdapter();
			}
			@Override
			public Adapter caseDIAGSERVICE(DIAGSERVICE object) {
				return createDIAGSERVICEAdapter();
			}
			@Override
			public Adapter caseDIAGVARIABLE(DIAGVARIABLE object) {
				return createDIAGVARIABLEAdapter();
			}
			@Override
			public Adapter caseDIAGVARIABLES(DIAGVARIABLES object) {
				return createDIAGVARIABLESAdapter();
			}
			@Override
			public Adapter caseDOCREVISION(DOCREVISION object) {
				return createDOCREVISIONAdapter();
			}
			@Override
			public Adapter caseDOCREVISION1(DOCREVISION1 object) {
				return createDOCREVISION1Adapter();
			}
			@Override
			public Adapter caseDOCREVISIONS(DOCREVISIONS object) {
				return createDOCREVISIONSAdapter();
			}
			@Override
			public Adapter caseDOCREVISIONS1(DOCREVISIONS1 object) {
				return createDOCREVISIONS1Adapter();
			}
			@Override
			public Adapter caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			@Override
			public Adapter caseDOPBASE(DOPBASE object) {
				return createDOPBASEAdapter();
			}
			@Override
			public Adapter caseDTC(DTC object) {
				return createDTCAdapter();
			}
			@Override
			public Adapter caseDTCDOP(DTCDOP object) {
				return createDTCDOPAdapter();
			}
			@Override
			public Adapter caseDTCDOPS(DTCDOPS object) {
				return createDTCDOPSAdapter();
			}
			@Override
			public Adapter caseDTCS(DTCS object) {
				return createDTCSAdapter();
			}
			@Override
			public Adapter caseDTCVALUE(DTCVALUE object) {
				return createDTCVALUEAdapter();
			}
			@Override
			public Adapter caseDTCVALUES(DTCVALUES object) {
				return createDTCVALUESAdapter();
			}
			@Override
			public Adapter caseDYNAMIC(DYNAMIC object) {
				return createDYNAMICAdapter();
			}
			@Override
			public Adapter caseDYNAMICENDMARKERFIELD(DYNAMICENDMARKERFIELD object) {
				return createDYNAMICENDMARKERFIELDAdapter();
			}
			@Override
			public Adapter caseDYNAMICENDMARKERFIELDS(DYNAMICENDMARKERFIELDS object) {
				return createDYNAMICENDMARKERFIELDSAdapter();
			}
			@Override
			public Adapter caseDYNAMICLENGTHFIELD(DYNAMICLENGTHFIELD object) {
				return createDYNAMICLENGTHFIELDAdapter();
			}
			@Override
			public Adapter caseDYNAMICLENGTHFIELDS(DYNAMICLENGTHFIELDS object) {
				return createDYNAMICLENGTHFIELDSAdapter();
			}
			@Override
			public Adapter caseDYNDEFINEDSPEC(DYNDEFINEDSPEC object) {
				return createDYNDEFINEDSPECAdapter();
			}
			@Override
			public Adapter caseDYNENDDOPREF(DYNENDDOPREF object) {
				return createDYNENDDOPREFAdapter();
			}
			@Override
			public Adapter caseDYNIDDEFMODEINFO(DYNIDDEFMODEINFO object) {
				return createDYNIDDEFMODEINFOAdapter();
			}
			@Override
			public Adapter caseDYNIDDEFMODEINFOS(DYNIDDEFMODEINFOS object) {
				return createDYNIDDEFMODEINFOSAdapter();
			}
			@Override
			public Adapter caseECUGROUP(ECUGROUP object) {
				return createECUGROUPAdapter();
			}
			@Override
			public Adapter caseECUGROUPS(ECUGROUPS object) {
				return createECUGROUPSAdapter();
			}
			@Override
			public Adapter caseECUMEM(ECUMEM object) {
				return createECUMEMAdapter();
			}
			@Override
			public Adapter caseECUMEMCONNECTOR(ECUMEMCONNECTOR object) {
				return createECUMEMCONNECTORAdapter();
			}
			@Override
			public Adapter caseECUMEMCONNECTORS(ECUMEMCONNECTORS object) {
				return createECUMEMCONNECTORSAdapter();
			}
			@Override
			public Adapter caseECUMEMS(ECUMEMS object) {
				return createECUMEMSAdapter();
			}
			@Override
			public Adapter caseECUPROXY(ECUPROXY object) {
				return createECUPROXYAdapter();
			}
			@Override
			public Adapter caseECUPROXYREFS(ECUPROXYREFS object) {
				return createECUPROXYREFSAdapter();
			}
			@Override
			public Adapter caseECUSHAREDDATA(ECUSHAREDDATA object) {
				return createECUSHAREDDATAAdapter();
			}
			@Override
			public Adapter caseECUSHAREDDATAREF(ECUSHAREDDATAREF object) {
				return createECUSHAREDDATAREFAdapter();
			}
			@Override
			public Adapter caseECUSHAREDDATAS(ECUSHAREDDATAS object) {
				return createECUSHAREDDATASAdapter();
			}
			@Override
			public Adapter caseECUVARIANT(ECUVARIANT object) {
				return createECUVARIANTAdapter();
			}
			@Override
			public Adapter caseECUVARIANTPATTERN(ECUVARIANTPATTERN object) {
				return createECUVARIANTPATTERNAdapter();
			}
			@Override
			public Adapter caseECUVARIANTPATTERNS(ECUVARIANTPATTERNS object) {
				return createECUVARIANTPATTERNSAdapter();
			}
			@Override
			public Adapter caseECUVARIANTS(ECUVARIANTS object) {
				return createECUVARIANTSAdapter();
			}
			@Override
			public Adapter caseENCRYPTCOMPRESSMETHOD(ENCRYPTCOMPRESSMETHOD object) {
				return createENCRYPTCOMPRESSMETHODAdapter();
			}
			@Override
			public Adapter caseENDOFPDUFIELD(ENDOFPDUFIELD object) {
				return createENDOFPDUFIELDAdapter();
			}
			@Override
			public Adapter caseENDOFPDUFIELDS(ENDOFPDUFIELDS object) {
				return createENDOFPDUFIELDSAdapter();
			}
			@Override
			public Adapter caseENVDATA(ENVDATA object) {
				return createENVDATAAdapter();
			}
			@Override
			public Adapter caseENVDATADESC(ENVDATADESC object) {
				return createENVDATADESCAdapter();
			}
			@Override
			public Adapter caseENVDATADESCS(ENVDATADESCS object) {
				return createENVDATADESCSAdapter();
			}
			@Override
			public Adapter caseENVDATAS(ENVDATAS object) {
				return createENVDATASAdapter();
			}
			@Override
			public Adapter caseEXPECTEDIDENT(EXPECTEDIDENT object) {
				return createEXPECTEDIDENTAdapter();
			}
			@Override
			public Adapter caseEXPECTEDIDENTS(EXPECTEDIDENTS object) {
				return createEXPECTEDIDENTSAdapter();
			}
			@Override
			public Adapter caseEXTERNALACCESSMETHOD(EXTERNALACCESSMETHOD object) {
				return createEXTERNALACCESSMETHODAdapter();
			}
			@Override
			public Adapter caseEXTERNFLASHDATA(EXTERNFLASHDATA object) {
				return createEXTERNFLASHDATAAdapter();
			}
			@Override
			public Adapter caseFIELD(FIELD object) {
				return createFIELDAdapter();
			}
			@Override
			public Adapter caseFILE(FILE object) {
				return createFILEAdapter();
			}
			@Override
			public Adapter caseFILESType(FILESType object) {
				return createFILESTypeAdapter();
			}
			@Override
			public Adapter caseFILTER(FILTER object) {
				return createFILTERAdapter();
			}
			@Override
			public Adapter caseFILTERS(FILTERS object) {
				return createFILTERSAdapter();
			}
			@Override
			public Adapter caseFLASH(FLASH object) {
				return createFLASHAdapter();
			}
			@Override
			public Adapter caseFLASHCLASS(FLASHCLASS object) {
				return createFLASHCLASSAdapter();
			}
			@Override
			public Adapter caseFLASHCLASSREFS(FLASHCLASSREFS object) {
				return createFLASHCLASSREFSAdapter();
			}
			@Override
			public Adapter caseFLASHCLASSS(FLASHCLASSS object) {
				return createFLASHCLASSSAdapter();
			}
			@Override
			public Adapter caseFLASHDATA(FLASHDATA object) {
				return createFLASHDATAAdapter();
			}
			@Override
			public Adapter caseFLASHDATAS(FLASHDATAS object) {
				return createFLASHDATASAdapter();
			}
			@Override
			public Adapter caseFlow(Flow object) {
				return createFlowAdapter();
			}
			@Override
			public Adapter caseFUNCTCLASS(FUNCTCLASS object) {
				return createFUNCTCLASSAdapter();
			}
			@Override
			public Adapter caseFUNCTCLASSREFS(FUNCTCLASSREFS object) {
				return createFUNCTCLASSREFSAdapter();
			}
			@Override
			public Adapter caseFUNCTCLASSS(FUNCTCLASSS object) {
				return createFUNCTCLASSSAdapter();
			}
			@Override
			public Adapter caseFUNCTIONALGROUP(FUNCTIONALGROUP object) {
				return createFUNCTIONALGROUPAdapter();
			}
			@Override
			public Adapter caseFUNCTIONALGROUPREF(FUNCTIONALGROUPREF object) {
				return createFUNCTIONALGROUPREFAdapter();
			}
			@Override
			public Adapter caseFUNCTIONALGROUPS(FUNCTIONALGROUPS object) {
				return createFUNCTIONALGROUPSAdapter();
			}
			@Override
			public Adapter caseFWCHECKSUM(FWCHECKSUM object) {
				return createFWCHECKSUMAdapter();
			}
			@Override
			public Adapter caseFWSIGNATURE(FWSIGNATURE object) {
				return createFWSIGNATUREAdapter();
			}
			@Override
			public Adapter caseGATEWAYLOGICALLINK(GATEWAYLOGICALLINK object) {
				return createGATEWAYLOGICALLINKAdapter();
			}
			@Override
			public Adapter caseGATEWAYLOGICALLINKREFS(GATEWAYLOGICALLINKREFS object) {
				return createGATEWAYLOGICALLINKREFSAdapter();
			}
			@Override
			public Adapter caseGLOBALNEGRESPONSE(GLOBALNEGRESPONSE object) {
				return createGLOBALNEGRESPONSEAdapter();
			}
			@Override
			public Adapter caseGLOBALNEGRESPONSES(GLOBALNEGRESPONSES object) {
				return createGLOBALNEGRESPONSESAdapter();
			}
			@Override
			public Adapter caseHIERARCHYELEMENT(HIERARCHYELEMENT object) {
				return createHIERARCHYELEMENTAdapter();
			}
			@Override
			public Adapter caseIDENTDESC(IDENTDESC object) {
				return createIDENTDESCAdapter();
			}
			@Override
			public Adapter caseIDENTDESCS(IDENTDESCS object) {
				return createIDENTDESCSAdapter();
			}
			@Override
			public Adapter caseIDENTVALUE(IDENTVALUE object) {
				return createIDENTVALUEAdapter();
			}
			@Override
			public Adapter caseIDENTVALUES(IDENTVALUES object) {
				return createIDENTVALUESAdapter();
			}
			@Override
			public Adapter caseIMPORTREFS(IMPORTREFS object) {
				return createIMPORTREFSAdapter();
			}
			@Override
			public Adapter caseINFOCOMPONENT(INFOCOMPONENT object) {
				return createINFOCOMPONENTAdapter();
			}
			@Override
			public Adapter caseINFOCOMPONENTREFS(INFOCOMPONENTREFS object) {
				return createINFOCOMPONENTREFSAdapter();
			}
			@Override
			public Adapter caseINFOCOMPONENTS(INFOCOMPONENTS object) {
				return createINFOCOMPONENTSAdapter();
			}
			@Override
			public Adapter caseInline(Inline object) {
				return createInlineAdapter();
			}
			@Override
			public Adapter caseINPUTPARAM(INPUTPARAM object) {
				return createINPUTPARAMAdapter();
			}
			@Override
			public Adapter caseINPUTPARAMS(INPUTPARAMS object) {
				return createINPUTPARAMSAdapter();
			}
			@Override
			public Adapter caseINTERNALCONSTR(INTERNALCONSTR object) {
				return createINTERNALCONSTRAdapter();
			}
			@Override
			public Adapter caseINTERNFLASHDATA(INTERNFLASHDATA object) {
				return createINTERNFLASHDATAAdapter();
			}
			@Override
			public Adapter caseIType(IType object) {
				return createITypeAdapter();
			}
			@Override
			public Adapter caseLAYERREFS(LAYERREFS object) {
				return createLAYERREFSAdapter();
			}
			@Override
			public Adapter caseLEADINGLENGTHINFOTYPE(LEADINGLENGTHINFOTYPE object) {
				return createLEADINGLENGTHINFOTYPEAdapter();
			}
			@Override
			public Adapter caseLENGTHDESCRIPTOR(LENGTHDESCRIPTOR object) {
				return createLENGTHDESCRIPTORAdapter();
			}
			@Override
			public Adapter caseLENGTHKEY(LENGTHKEY object) {
				return createLENGTHKEYAdapter();
			}
			@Override
			public Adapter caseLIMIT(LIMIT object) {
				return createLIMITAdapter();
			}
			@Override
			public Adapter caseLINKCOMPARAMREF(LINKCOMPARAMREF object) {
				return createLINKCOMPARAMREFAdapter();
			}
			@Override
			public Adapter caseLINKCOMPARAMREFS(LINKCOMPARAMREFS object) {
				return createLINKCOMPARAMREFSAdapter();
			}
			@Override
			public Adapter caseLiType(LiType object) {
				return createLiTypeAdapter();
			}
			@Override
			public Adapter caseLOGICALLINK(LOGICALLINK object) {
				return createLOGICALLINKAdapter();
			}
			@Override
			public Adapter caseLOGICALLINKREFS(LOGICALLINKREFS object) {
				return createLOGICALLINKREFSAdapter();
			}
			@Override
			public Adapter caseLOGICALLINKS(LOGICALLINKS object) {
				return createLOGICALLINKSAdapter();
			}
			@Override
			public Adapter caseMATCHINGCOMPONENT(MATCHINGCOMPONENT object) {
				return createMATCHINGCOMPONENTAdapter();
			}
			@Override
			public Adapter caseMATCHINGCOMPONENTS(MATCHINGCOMPONENTS object) {
				return createMATCHINGCOMPONENTSAdapter();
			}
			@Override
			public Adapter caseMATCHINGPARAMETER(MATCHINGPARAMETER object) {
				return createMATCHINGPARAMETERAdapter();
			}
			@Override
			public Adapter caseMATCHINGPARAMETERS(MATCHINGPARAMETERS object) {
				return createMATCHINGPARAMETERSAdapter();
			}
			@Override
			public Adapter caseMATCHINGREQUESTPARAM(MATCHINGREQUESTPARAM object) {
				return createMATCHINGREQUESTPARAMAdapter();
			}
			@Override
			public Adapter caseMEM(MEM object) {
				return createMEMAdapter();
			}
			@Override
			public Adapter caseMEMBERLOGICALLINK(MEMBERLOGICALLINK object) {
				return createMEMBERLOGICALLINKAdapter();
			}
			@Override
			public Adapter caseMINMAXLENGTHTYPE(MINMAXLENGTHTYPE object) {
				return createMINMAXLENGTHTYPEAdapter();
			}
			@Override
			public Adapter caseMODELYEAR(MODELYEAR object) {
				return createMODELYEARAdapter();
			}
			@Override
			public Adapter caseMODIFICATION(MODIFICATION object) {
				return createMODIFICATIONAdapter();
			}
			@Override
			public Adapter caseMODIFICATION1(MODIFICATION1 object) {
				return createMODIFICATION1Adapter();
			}
			@Override
			public Adapter caseMODIFICATIONS(MODIFICATIONS object) {
				return createMODIFICATIONSAdapter();
			}
			@Override
			public Adapter caseMODIFICATIONS1(MODIFICATIONS1 object) {
				return createMODIFICATIONS1Adapter();
			}
			@Override
			public Adapter caseMULTIPLEECUJOB(MULTIPLEECUJOB object) {
				return createMULTIPLEECUJOBAdapter();
			}
			@Override
			public Adapter caseMULTIPLEECUJOBS(MULTIPLEECUJOBS object) {
				return createMULTIPLEECUJOBSAdapter();
			}
			@Override
			public Adapter caseMULTIPLEECUJOBSPEC(MULTIPLEECUJOBSPEC object) {
				return createMULTIPLEECUJOBSPECAdapter();
			}
			@Override
			public Adapter caseMUX(MUX object) {
				return createMUXAdapter();
			}
			@Override
			public Adapter caseMUXS(MUXS object) {
				return createMUXSAdapter();
			}
			@Override
			public Adapter caseNEGOFFSET(NEGOFFSET object) {
				return createNEGOFFSETAdapter();
			}
			@Override
			public Adapter caseNEGOUTPUTPARAM(NEGOUTPUTPARAM object) {
				return createNEGOUTPUTPARAMAdapter();
			}
			@Override
			public Adapter caseNEGOUTPUTPARAMS(NEGOUTPUTPARAMS object) {
				return createNEGOUTPUTPARAMSAdapter();
			}
			@Override
			public Adapter caseNEGRESPONSE(NEGRESPONSE object) {
				return createNEGRESPONSEAdapter();
			}
			@Override
			public Adapter caseNEGRESPONSEREFS(NEGRESPONSEREFS object) {
				return createNEGRESPONSEREFSAdapter();
			}
			@Override
			public Adapter caseNEGRESPONSES(NEGRESPONSES object) {
				return createNEGRESPONSESAdapter();
			}
			@Override
			public Adapter caseNOTINHERITEDDIAGCOMM(NOTINHERITEDDIAGCOMM object) {
				return createNOTINHERITEDDIAGCOMMAdapter();
			}
			@Override
			public Adapter caseNOTINHERITEDDIAGCOMMS(NOTINHERITEDDIAGCOMMS object) {
				return createNOTINHERITEDDIAGCOMMSAdapter();
			}
			@Override
			public Adapter caseNOTINHERITEDVARIABLE(NOTINHERITEDVARIABLE object) {
				return createNOTINHERITEDVARIABLEAdapter();
			}
			@Override
			public Adapter caseNOTINHERITEDVARIABLES(NOTINHERITEDVARIABLES object) {
				return createNOTINHERITEDVARIABLESAdapter();
			}
			@Override
			public Adapter caseODX(ODX object) {
				return createODXAdapter();
			}
			@Override
			public Adapter caseODXCATEGORY(ODXCATEGORY object) {
				return createODXCATEGORYAdapter();
			}
			@Override
			public Adapter caseODXLINK(ODXLINK object) {
				return createODXLINKAdapter();
			}
			@Override
			public Adapter caseODXLINK1(ODXLINK1 object) {
				return createODXLINK1Adapter();
			}
			@Override
			public Adapter caseOEM(OEM object) {
				return createOEMAdapter();
			}
			@Override
			public Adapter caseOlType(OlType object) {
				return createOlTypeAdapter();
			}
			@Override
			public Adapter caseOUTPUTPARAM(OUTPUTPARAM object) {
				return createOUTPUTPARAMAdapter();
			}
			@Override
			public Adapter caseOUTPUTPARAMS(OUTPUTPARAMS object) {
				return createOUTPUTPARAMSAdapter();
			}
			@Override
			public Adapter caseOWNIDENT(OWNIDENT object) {
				return createOWNIDENTAdapter();
			}
			@Override
			public Adapter caseOWNIDENTS(OWNIDENTS object) {
				return createOWNIDENTSAdapter();
			}
			@Override
			public Adapter caseP(P object) {
				return createPAdapter();
			}
			@Override
			public Adapter casePARAM(PARAM object) {
				return createPARAMAdapter();
			}
			@Override
			public Adapter casePARAMLENGTHINFOTYPE(PARAMLENGTHINFOTYPE object) {
				return createPARAMLENGTHINFOTYPEAdapter();
			}
			@Override
			public Adapter casePARAMS(PARAMS object) {
				return createPARAMSAdapter();
			}
			@Override
			public Adapter casePARENTREF(PARENTREF object) {
				return createPARENTREFAdapter();
			}
			@Override
			public Adapter casePARENTREFS(PARENTREFS object) {
				return createPARENTREFSAdapter();
			}
			@Override
			public Adapter casePHYSCONST(PHYSCONST object) {
				return createPHYSCONSTAdapter();
			}
			@Override
			public Adapter casePHYSICALDIMENSION(PHYSICALDIMENSION object) {
				return createPHYSICALDIMENSIONAdapter();
			}
			@Override
			public Adapter casePHYSICALDIMENSIONS(PHYSICALDIMENSIONS object) {
				return createPHYSICALDIMENSIONSAdapter();
			}
			@Override
			public Adapter casePHYSICALTYPE(PHYSICALTYPE object) {
				return createPHYSICALTYPEAdapter();
			}
			@Override
			public Adapter casePHYSICALVEHICLELINK(PHYSICALVEHICLELINK object) {
				return createPHYSICALVEHICLELINKAdapter();
			}
			@Override
			public Adapter casePHYSICALVEHICLELINKS(PHYSICALVEHICLELINKS object) {
				return createPHYSICALVEHICLELINKSAdapter();
			}
			@Override
			public Adapter casePHYSMEM(PHYSMEM object) {
				return createPHYSMEMAdapter();
			}
			@Override
			public Adapter casePHYSSEGMENT(PHYSSEGMENT object) {
				return createPHYSSEGMENTAdapter();
			}
			@Override
			public Adapter casePHYSSEGMENTS(PHYSSEGMENTS object) {
				return createPHYSSEGMENTSAdapter();
			}
			@Override
			public Adapter casePOSITIONABLEPARAM(POSITIONABLEPARAM object) {
				return createPOSITIONABLEPARAMAdapter();
			}
			@Override
			public Adapter casePOSOFFSET(POSOFFSET object) {
				return createPOSOFFSETAdapter();
			}
			@Override
			public Adapter casePOSRESPONSE(POSRESPONSE object) {
				return createPOSRESPONSEAdapter();
			}
			@Override
			public Adapter casePOSRESPONSEREFS(POSRESPONSEREFS object) {
				return createPOSRESPONSEREFSAdapter();
			}
			@Override
			public Adapter casePOSRESPONSES(POSRESPONSES object) {
				return createPOSRESPONSESAdapter();
			}
			@Override
			public Adapter casePROGCODE(PROGCODE object) {
				return createPROGCODEAdapter();
			}
			@Override
			public Adapter casePROGCODES(PROGCODES object) {
				return createPROGCODESAdapter();
			}
			@Override
			public Adapter casePROJECTIDENT(PROJECTIDENT object) {
				return createPROJECTIDENTAdapter();
			}
			@Override
			public Adapter casePROJECTIDENTS(PROJECTIDENTS object) {
				return createPROJECTIDENTSAdapter();
			}
			@Override
			public Adapter casePROJECTINFO(PROJECTINFO object) {
				return createPROJECTINFOAdapter();
			}
			@Override
			public Adapter casePROJECTINFOS(PROJECTINFOS object) {
				return createPROJECTINFOSAdapter();
			}
			@Override
			public Adapter casePROTOCOL(PROTOCOL object) {
				return createPROTOCOLAdapter();
			}
			@Override
			public Adapter casePROTOCOLREF(PROTOCOLREF object) {
				return createPROTOCOLREFAdapter();
			}
			@Override
			public Adapter casePROTOCOLS(PROTOCOLS object) {
				return createPROTOCOLSAdapter();
			}
			@Override
			public Adapter casePROTOCOLSNREFS(PROTOCOLSNREFS object) {
				return createPROTOCOLSNREFSAdapter();
			}
			@Override
			public Adapter caseRELATEDDIAGCOMMREF(RELATEDDIAGCOMMREF object) {
				return createRELATEDDIAGCOMMREFAdapter();
			}
			@Override
			public Adapter caseRELATEDDIAGCOMMREFS(RELATEDDIAGCOMMREFS object) {
				return createRELATEDDIAGCOMMREFSAdapter();
			}
			@Override
			public Adapter caseRELATEDDOC(RELATEDDOC object) {
				return createRELATEDDOCAdapter();
			}
			@Override
			public Adapter caseRELATEDDOC1(RELATEDDOC1 object) {
				return createRELATEDDOC1Adapter();
			}
			@Override
			public Adapter caseRELATEDDOCS(RELATEDDOCS object) {
				return createRELATEDDOCSAdapter();
			}
			@Override
			public Adapter caseRELATEDDOCS1(RELATEDDOCS1 object) {
				return createRELATEDDOCS1Adapter();
			}
			@Override
			public Adapter caseREQUEST(REQUEST object) {
				return createREQUESTAdapter();
			}
			@Override
			public Adapter caseREQUESTS(REQUESTS object) {
				return createREQUESTSAdapter();
			}
			@Override
			public Adapter caseRESERVED(RESERVED object) {
				return createRESERVEDAdapter();
			}
			@Override
			public Adapter caseRESPONSE(RESPONSE object) {
				return createRESPONSEAdapter();
			}
			@Override
			public Adapter caseROLES(ROLES object) {
				return createROLESAdapter();
			}
			@Override
			public Adapter caseROLES1(ROLES1 object) {
				return createROLES1Adapter();
			}
			@Override
			public Adapter caseSCALECONSTR(SCALECONSTR object) {
				return createSCALECONSTRAdapter();
			}
			@Override
			public Adapter caseSCALECONSTRS(SCALECONSTRS object) {
				return createSCALECONSTRSAdapter();
			}
			@Override
			public Adapter caseSD(SD object) {
				return createSDAdapter();
			}
			@Override
			public Adapter caseSD1(SD1 object) {
				return createSD1Adapter();
			}
			@Override
			public Adapter caseSDG(SDG object) {
				return createSDGAdapter();
			}
			@Override
			public Adapter caseSDG1(SDG1 object) {
				return createSDG1Adapter();
			}
			@Override
			public Adapter caseSDGCAPTION(SDGCAPTION object) {
				return createSDGCAPTIONAdapter();
			}
			@Override
			public Adapter caseSDGCAPTION1(SDGCAPTION1 object) {
				return createSDGCAPTION1Adapter();
			}
			@Override
			public Adapter caseSDGS(SDGS object) {
				return createSDGSAdapter();
			}
			@Override
			public Adapter caseSDGS1(SDGS1 object) {
				return createSDGS1Adapter();
			}
			@Override
			public Adapter caseSECURITY(SECURITY object) {
				return createSECURITYAdapter();
			}
			@Override
			public Adapter caseSECURITYMETHOD(SECURITYMETHOD object) {
				return createSECURITYMETHODAdapter();
			}
			@Override
			public Adapter caseSECURITYS(SECURITYS object) {
				return createSECURITYSAdapter();
			}
			@Override
			public Adapter caseSEGMENT(SEGMENT object) {
				return createSEGMENTAdapter();
			}
			@Override
			public Adapter caseSEGMENTS(SEGMENTS object) {
				return createSEGMENTSAdapter();
			}
			@Override
			public Adapter caseSELECTIONTABLEREFS(SELECTIONTABLEREFS object) {
				return createSELECTIONTABLEREFSAdapter();
			}
			@Override
			public Adapter caseSESSION(SESSION object) {
				return createSESSIONAdapter();
			}
			@Override
			public Adapter caseSESSIONDESC(SESSIONDESC object) {
				return createSESSIONDESCAdapter();
			}
			@Override
			public Adapter caseSESSIONDESCS(SESSIONDESCS object) {
				return createSESSIONDESCSAdapter();
			}
			@Override
			public Adapter caseSESSIONS(SESSIONS object) {
				return createSESSIONSAdapter();
			}
			@Override
			public Adapter caseSINGLEECUJOB(SINGLEECUJOB object) {
				return createSINGLEECUJOBAdapter();
			}
			@Override
			public Adapter caseSIZEDEFFILTER(SIZEDEFFILTER object) {
				return createSIZEDEFFILTERAdapter();
			}
			@Override
			public Adapter caseSIZEDEFPHYSSEGMENT(SIZEDEFPHYSSEGMENT object) {
				return createSIZEDEFPHYSSEGMENTAdapter();
			}
			@Override
			public Adapter caseSNREF(SNREF object) {
				return createSNREFAdapter();
			}
			@Override
			public Adapter caseSOURCEENDADDRESS(SOURCEENDADDRESS object) {
				return createSOURCEENDADDRESSAdapter();
			}
			@Override
			public Adapter caseSPECIALDATA(SPECIALDATA object) {
				return createSPECIALDATAAdapter();
			}
			@Override
			public Adapter caseSPECIALDATA1(SPECIALDATA1 object) {
				return createSPECIALDATA1Adapter();
			}
			@Override
			public Adapter caseSTANDARDLENGTHTYPE(STANDARDLENGTHTYPE object) {
				return createSTANDARDLENGTHTYPEAdapter();
			}
			@Override
			public Adapter caseSTATICFIELD(STATICFIELD object) {
				return createSTATICFIELDAdapter();
			}
			@Override
			public Adapter caseSTATICFIELDS(STATICFIELDS object) {
				return createSTATICFIELDSAdapter();
			}
			@Override
			public Adapter caseSTRUCTURE(STRUCTURE object) {
				return createSTRUCTUREAdapter();
			}
			@Override
			public Adapter caseSTRUCTURES(STRUCTURES object) {
				return createSTRUCTURESAdapter();
			}
			@Override
			public Adapter caseSubType(SubType object) {
				return createSubTypeAdapter();
			}
			@Override
			public Adapter caseSUPPORTEDDYNID(SUPPORTEDDYNID object) {
				return createSUPPORTEDDYNIDAdapter();
			}
			@Override
			public Adapter caseSUPPORTEDDYNIDS(SUPPORTEDDYNIDS object) {
				return createSUPPORTEDDYNIDSAdapter();
			}
			@Override
			public Adapter caseSupType(SupType object) {
				return createSupTypeAdapter();
			}
			@Override
			public Adapter caseSWITCHKEY(SWITCHKEY object) {
				return createSWITCHKEYAdapter();
			}
			@Override
			public Adapter caseSWVARIABLE(SWVARIABLE object) {
				return createSWVARIABLEAdapter();
			}
			@Override
			public Adapter caseSWVARIABLES(SWVARIABLES object) {
				return createSWVARIABLESAdapter();
			}
			@Override
			public Adapter caseSYSTEM(SYSTEM object) {
				return createSYSTEMAdapter();
			}
			@Override
			public Adapter caseTABLE(TABLE object) {
				return createTABLEAdapter();
			}
			@Override
			public Adapter caseTABLEENTRY(TABLEENTRY object) {
				return createTABLEENTRYAdapter();
			}
			@Override
			public Adapter caseTABLEKEY(TABLEKEY object) {
				return createTABLEKEYAdapter();
			}
			@Override
			public Adapter caseTABLEROW(TABLEROW object) {
				return createTABLEROWAdapter();
			}
			@Override
			public Adapter caseTABLES(TABLES object) {
				return createTABLESAdapter();
			}
			@Override
			public Adapter caseTABLESTRUCT(TABLESTRUCT object) {
				return createTABLESTRUCTAdapter();
			}
			@Override
			public Adapter caseTARGETADDROFFSET(TARGETADDROFFSET object) {
				return createTARGETADDROFFSETAdapter();
			}
			@Override
			public Adapter caseTEAMMEMBER(TEAMMEMBER object) {
				return createTEAMMEMBERAdapter();
			}
			@Override
			public Adapter caseTEAMMEMBER1(TEAMMEMBER1 object) {
				return createTEAMMEMBER1Adapter();
			}
			@Override
			public Adapter caseTEAMMEMBERS(TEAMMEMBERS object) {
				return createTEAMMEMBERSAdapter();
			}
			@Override
			public Adapter caseTEAMMEMBERS1(TEAMMEMBERS1 object) {
				return createTEAMMEMBERS1Adapter();
			}
			@Override
			public Adapter caseTEXT(TEXT object) {
				return createTEXTAdapter();
			}
			@Override
			public Adapter caseTEXT1(TEXT1 object) {
				return createTEXT1Adapter();
			}
			@Override
			public Adapter caseUlType(UlType object) {
				return createUlTypeAdapter();
			}
			@Override
			public Adapter caseUNCOMPRESSEDSIZE(UNCOMPRESSEDSIZE object) {
				return createUNCOMPRESSEDSIZEAdapter();
			}
			@Override
			public Adapter caseUNIONVALUE(UNIONVALUE object) {
				return createUNIONVALUEAdapter();
			}
			@Override
			public Adapter caseUNIT(UNIT object) {
				return createUNITAdapter();
			}
			@Override
			public Adapter caseUNITGROUP(UNITGROUP object) {
				return createUNITGROUPAdapter();
			}
			@Override
			public Adapter caseUNITGROUPS(UNITGROUPS object) {
				return createUNITGROUPSAdapter();
			}
			@Override
			public Adapter caseUNITREFS(UNITREFS object) {
				return createUNITREFSAdapter();
			}
			@Override
			public Adapter caseUNITS(UNITS object) {
				return createUNITSAdapter();
			}
			@Override
			public Adapter caseUNITSPEC(UNITSPEC object) {
				return createUNITSPECAdapter();
			}
			@Override
			public Adapter caseV(V object) {
				return createVAdapter();
			}
			@Override
			public Adapter caseVALIDITYFOR(VALIDITYFOR object) {
				return createVALIDITYFORAdapter();
			}
			@Override
			public Adapter caseVALUE(VALUE object) {
				return createVALUEAdapter();
			}
			@Override
			public Adapter caseVARIABLEGROUP(VARIABLEGROUP object) {
				return createVARIABLEGROUPAdapter();
			}
			@Override
			public Adapter caseVARIABLEGROUPS(VARIABLEGROUPS object) {
				return createVARIABLEGROUPSAdapter();
			}
			@Override
			public Adapter caseVEHICLECONNECTOR(VEHICLECONNECTOR object) {
				return createVEHICLECONNECTORAdapter();
			}
			@Override
			public Adapter caseVEHICLECONNECTORPIN(VEHICLECONNECTORPIN object) {
				return createVEHICLECONNECTORPINAdapter();
			}
			@Override
			public Adapter caseVEHICLECONNECTORPINREFS(VEHICLECONNECTORPINREFS object) {
				return createVEHICLECONNECTORPINREFSAdapter();
			}
			@Override
			public Adapter caseVEHICLECONNECTORPINS(VEHICLECONNECTORPINS object) {
				return createVEHICLECONNECTORPINSAdapter();
			}
			@Override
			public Adapter caseVEHICLECONNECTORS(VEHICLECONNECTORS object) {
				return createVEHICLECONNECTORSAdapter();
			}
			@Override
			public Adapter caseVEHICLEINFORMATION(VEHICLEINFORMATION object) {
				return createVEHICLEINFORMATIONAdapter();
			}
			@Override
			public Adapter caseVEHICLEINFORMATIONS(VEHICLEINFORMATIONS object) {
				return createVEHICLEINFORMATIONSAdapter();
			}
			@Override
			public Adapter caseVEHICLEINFOSPEC(VEHICLEINFOSPEC object) {
				return createVEHICLEINFOSPECAdapter();
			}
			@Override
			public Adapter caseVEHICLEMODEL(VEHICLEMODEL object) {
				return createVEHICLEMODELAdapter();
			}
			@Override
			public Adapter caseVEHICLETYPE(VEHICLETYPE object) {
				return createVEHICLETYPEAdapter();
			}
			@Override
			public Adapter caseVT(VT object) {
				return createVTAdapter();
			}
			@Override
			public Adapter caseXDOC(XDOC object) {
				return createXDOCAdapter();
			}
			@Override
			public Adapter caseXDOC1(XDOC1 object) {
				return createXDOC1Adapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ABLOCK <em>ABLOCK</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ABLOCK
	 * @generated
	 */
	public Adapter createABLOCKAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ABLOCKSType <em>ABLOCKS Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ABLOCKSType
	 * @generated
	 */
	public Adapter createABLOCKSTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ACCESSLEVEL <em>ACCESSLEVEL</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ACCESSLEVEL
	 * @generated
	 */
	public Adapter createACCESSLEVELAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ACCESSLEVELS <em>ACCESSLEVELS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ACCESSLEVELS
	 * @generated
	 */
	public Adapter createACCESSLEVELSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ADDRDEFFILTER <em>ADDRDEFFILTER</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ADDRDEFFILTER
	 * @generated
	 */
	public Adapter createADDRDEFFILTERAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ADDRDEFPHYSSEGMENT <em>ADDRDEFPHYSSEGMENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ADDRDEFPHYSSEGMENT
	 * @generated
	 */
	public Adapter createADDRDEFPHYSSEGMENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ADMINDATA <em>ADMINDATA</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ADMINDATA
	 * @generated
	 */
	public Adapter createADMINDATAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ADMINDATA1 <em>ADMINDATA1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ADMINDATA1
	 * @generated
	 */
	public Adapter createADMINDATA1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ALLVALUE <em>ALLVALUE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ALLVALUE
	 * @generated
	 */
	public Adapter createALLVALUEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.AUDIENCE <em>AUDIENCE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.AUDIENCE
	 * @generated
	 */
	public Adapter createAUDIENCEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.AUTMETHOD <em>AUTMETHOD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.AUTMETHOD
	 * @generated
	 */
	public Adapter createAUTMETHODAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.AUTMETHODS <em>AUTMETHODS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.AUTMETHODS
	 * @generated
	 */
	public Adapter createAUTMETHODSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.BASEVARIANT <em>BASEVARIANT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.BASEVARIANT
	 * @generated
	 */
	public Adapter createBASEVARIANTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.BASEVARIANTREF <em>BASEVARIANTREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.BASEVARIANTREF
	 * @generated
	 */
	public Adapter createBASEVARIANTREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.BASEVARIANTS <em>BASEVARIANTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.BASEVARIANTS
	 * @generated
	 */
	public Adapter createBASEVARIANTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.BASICSTRUCTURE <em>BASICSTRUCTURE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.BASICSTRUCTURE
	 * @generated
	 */
	public Adapter createBASICSTRUCTUREAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.Block
	 * @generated
	 */
	public Adapter createBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.BType <em>BType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.BType
	 * @generated
	 */
	public Adapter createBTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.CASE <em>CASE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.CASE
	 * @generated
	 */
	public Adapter createCASEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.CASES <em>CASES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.CASES
	 * @generated
	 */
	public Adapter createCASESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.CATALOG <em>CATALOG</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.CATALOG
	 * @generated
	 */
	public Adapter createCATALOGAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.CHECKSUM <em>CHECKSUM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.CHECKSUM
	 * @generated
	 */
	public Adapter createCHECKSUMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.CHECKSUMRESULT <em>CHECKSUMRESULT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.CHECKSUMRESULT
	 * @generated
	 */
	public Adapter createCHECKSUMRESULTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.CHECKSUMS <em>CHECKSUMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.CHECKSUMS
	 * @generated
	 */
	public Adapter createCHECKSUMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.CODEDCONST <em>CODEDCONST</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.CODEDCONST
	 * @generated
	 */
	public Adapter createCODEDCONSTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMMRELATION <em>COMMRELATION</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMMRELATION
	 * @generated
	 */
	public Adapter createCOMMRELATIONAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMMRELATIONS <em>COMMRELATIONS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMMRELATIONS
	 * @generated
	 */
	public Adapter createCOMMRELATIONSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYDATA <em>COMPANYDATA</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYDATA
	 * @generated
	 */
	public Adapter createCOMPANYDATAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYDATA1 <em>COMPANYDATA1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYDATA1
	 * @generated
	 */
	public Adapter createCOMPANYDATA1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYDATAS <em>COMPANYDATAS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYDATAS
	 * @generated
	 */
	public Adapter createCOMPANYDATASAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYDATASType <em>COMPANYDATAS Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYDATASType
	 * @generated
	 */
	public Adapter createCOMPANYDATASTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYDOCINFO <em>COMPANYDOCINFO</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYDOCINFO
	 * @generated
	 */
	public Adapter createCOMPANYDOCINFOAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYDOCINFO1 <em>COMPANYDOCINFO1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYDOCINFO1
	 * @generated
	 */
	public Adapter createCOMPANYDOCINFO1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYDOCINFOS <em>COMPANYDOCINFOS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYDOCINFOS
	 * @generated
	 */
	public Adapter createCOMPANYDOCINFOSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYDOCINFOS1 <em>COMPANYDOCINFOS1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYDOCINFOS1
	 * @generated
	 */
	public Adapter createCOMPANYDOCINFOS1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYREVISIONINFO <em>COMPANYREVISIONINFO</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYREVISIONINFO
	 * @generated
	 */
	public Adapter createCOMPANYREVISIONINFOAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYREVISIONINFO1 <em>COMPANYREVISIONINFO1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYREVISIONINFO1
	 * @generated
	 */
	public Adapter createCOMPANYREVISIONINFO1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYREVISIONINFOS <em>COMPANYREVISIONINFOS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYREVISIONINFOS
	 * @generated
	 */
	public Adapter createCOMPANYREVISIONINFOSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYREVISIONINFOS1 <em>COMPANYREVISIONINFOS1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYREVISIONINFOS1
	 * @generated
	 */
	public Adapter createCOMPANYREVISIONINFOS1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYSPECIFICINFO <em>COMPANYSPECIFICINFO</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYSPECIFICINFO
	 * @generated
	 */
	public Adapter createCOMPANYSPECIFICINFOAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPANYSPECIFICINFO1 <em>COMPANYSPECIFICINFO1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPANYSPECIFICINFO1
	 * @generated
	 */
	public Adapter createCOMPANYSPECIFICINFO1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPARAM <em>COMPARAM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPARAM
	 * @generated
	 */
	public Adapter createCOMPARAMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPARAMREF <em>COMPARAMREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPARAMREF
	 * @generated
	 */
	public Adapter createCOMPARAMREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPARAMREFS <em>COMPARAMREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPARAMREFS
	 * @generated
	 */
	public Adapter createCOMPARAMREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPARAMS <em>COMPARAMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPARAMS
	 * @generated
	 */
	public Adapter createCOMPARAMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPARAMSPEC <em>COMPARAMSPEC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPARAMSPEC
	 * @generated
	 */
	public Adapter createCOMPARAMSPECAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPLEXDOP <em>COMPLEXDOP</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPLEXDOP
	 * @generated
	 */
	public Adapter createCOMPLEXDOPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUCONST <em>COMPUCONST</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUCONST
	 * @generated
	 */
	public Adapter createCOMPUCONSTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUDEFAULTVALUE <em>COMPUDEFAULTVALUE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUDEFAULTVALUE
	 * @generated
	 */
	public Adapter createCOMPUDEFAULTVALUEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUDENOMINATOR <em>COMPUDENOMINATOR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUDENOMINATOR
	 * @generated
	 */
	public Adapter createCOMPUDENOMINATORAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUINTERNALTOPHYS <em>COMPUINTERNALTOPHYS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUINTERNALTOPHYS
	 * @generated
	 */
	public Adapter createCOMPUINTERNALTOPHYSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUINVERSEVALUE <em>COMPUINVERSEVALUE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUINVERSEVALUE
	 * @generated
	 */
	public Adapter createCOMPUINVERSEVALUEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUMETHOD <em>COMPUMETHOD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUMETHOD
	 * @generated
	 */
	public Adapter createCOMPUMETHODAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUNUMERATOR <em>COMPUNUMERATOR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUNUMERATOR
	 * @generated
	 */
	public Adapter createCOMPUNUMERATORAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUPHYSTOINTERNAL <em>COMPUPHYSTOINTERNAL</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUPHYSTOINTERNAL
	 * @generated
	 */
	public Adapter createCOMPUPHYSTOINTERNALAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPURATIONALCOEFFS <em>COMPURATIONALCOEFFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPURATIONALCOEFFS
	 * @generated
	 */
	public Adapter createCOMPURATIONALCOEFFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUSCALE <em>COMPUSCALE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUSCALE
	 * @generated
	 */
	public Adapter createCOMPUSCALEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.COMPUSCALES <em>COMPUSCALES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.COMPUSCALES
	 * @generated
	 */
	public Adapter createCOMPUSCALESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DATABLOCK <em>DATABLOCK</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DATABLOCK
	 * @generated
	 */
	public Adapter createDATABLOCKAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DATABLOCKREFS <em>DATABLOCKREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DATABLOCKREFS
	 * @generated
	 */
	public Adapter createDATABLOCKREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DATABLOCKS <em>DATABLOCKS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DATABLOCKS
	 * @generated
	 */
	public Adapter createDATABLOCKSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DATAFILE <em>DATAFILE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DATAFILE
	 * @generated
	 */
	public Adapter createDATAFILEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DATAFORMAT <em>DATAFORMAT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DATAFORMAT
	 * @generated
	 */
	public Adapter createDATAFORMATAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DATAOBJECTPROP <em>DATAOBJECTPROP</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DATAOBJECTPROP
	 * @generated
	 */
	public Adapter createDATAOBJECTPROPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DATAOBJECTPROPS <em>DATAOBJECTPROPS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DATAOBJECTPROPS
	 * @generated
	 */
	public Adapter createDATAOBJECTPROPSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DEFAULTCASE <em>DEFAULTCASE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DEFAULTCASE
	 * @generated
	 */
	public Adapter createDEFAULTCASEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DESCRIPTION <em>DESCRIPTION</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DESCRIPTION
	 * @generated
	 */
	public Adapter createDESCRIPTIONAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DESCRIPTION1 <em>DESCRIPTION1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DESCRIPTION1
	 * @generated
	 */
	public Adapter createDESCRIPTION1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DETERMINENUMBEROFITEMS <em>DETERMINENUMBEROFITEMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DETERMINENUMBEROFITEMS
	 * @generated
	 */
	public Adapter createDETERMINENUMBEROFITEMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGCODEDTYPE
	 * @generated
	 */
	public Adapter createDIAGCODEDTYPEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGCOMM <em>DIAGCOMM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGCOMM
	 * @generated
	 */
	public Adapter createDIAGCOMMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGCOMMS <em>DIAGCOMMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGCOMMS
	 * @generated
	 */
	public Adapter createDIAGCOMMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGDATADICTIONARYSPEC <em>DIAGDATADICTIONARYSPEC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGDATADICTIONARYSPEC
	 * @generated
	 */
	public Adapter createDIAGDATADICTIONARYSPECAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGLAYER <em>DIAGLAYER</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGLAYER
	 * @generated
	 */
	public Adapter createDIAGLAYERAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGLAYERCONTAINER <em>DIAGLAYERCONTAINER</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGLAYERCONTAINER
	 * @generated
	 */
	public Adapter createDIAGLAYERCONTAINERAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGLAYERREFS <em>DIAGLAYERREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGLAYERREFS
	 * @generated
	 */
	public Adapter createDIAGLAYERREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGSERVICE <em>DIAGSERVICE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGSERVICE
	 * @generated
	 */
	public Adapter createDIAGSERVICEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGVARIABLE <em>DIAGVARIABLE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGVARIABLE
	 * @generated
	 */
	public Adapter createDIAGVARIABLEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DIAGVARIABLES <em>DIAGVARIABLES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DIAGVARIABLES
	 * @generated
	 */
	public Adapter createDIAGVARIABLESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DOCREVISION <em>DOCREVISION</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DOCREVISION
	 * @generated
	 */
	public Adapter createDOCREVISIONAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DOCREVISION1 <em>DOCREVISION1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DOCREVISION1
	 * @generated
	 */
	public Adapter createDOCREVISION1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DOCREVISIONS <em>DOCREVISIONS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DOCREVISIONS
	 * @generated
	 */
	public Adapter createDOCREVISIONSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DOCREVISIONS1 <em>DOCREVISIONS1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DOCREVISIONS1
	 * @generated
	 */
	public Adapter createDOCREVISIONS1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DOPBASE <em>DOPBASE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DOPBASE
	 * @generated
	 */
	public Adapter createDOPBASEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DTC <em>DTC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DTC
	 * @generated
	 */
	public Adapter createDTCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DTCDOP <em>DTCDOP</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DTCDOP
	 * @generated
	 */
	public Adapter createDTCDOPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DTCDOPS <em>DTCDOPS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DTCDOPS
	 * @generated
	 */
	public Adapter createDTCDOPSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DTCS <em>DTCS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DTCS
	 * @generated
	 */
	public Adapter createDTCSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DTCVALUE <em>DTCVALUE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DTCVALUE
	 * @generated
	 */
	public Adapter createDTCVALUEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DTCVALUES <em>DTCVALUES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DTCVALUES
	 * @generated
	 */
	public Adapter createDTCVALUESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DYNAMIC <em>DYNAMIC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DYNAMIC
	 * @generated
	 */
	public Adapter createDYNAMICAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DYNAMICENDMARKERFIELD <em>DYNAMICENDMARKERFIELD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DYNAMICENDMARKERFIELD
	 * @generated
	 */
	public Adapter createDYNAMICENDMARKERFIELDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DYNAMICENDMARKERFIELDS <em>DYNAMICENDMARKERFIELDS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DYNAMICENDMARKERFIELDS
	 * @generated
	 */
	public Adapter createDYNAMICENDMARKERFIELDSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DYNAMICLENGTHFIELD <em>DYNAMICLENGTHFIELD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DYNAMICLENGTHFIELD
	 * @generated
	 */
	public Adapter createDYNAMICLENGTHFIELDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DYNAMICLENGTHFIELDS <em>DYNAMICLENGTHFIELDS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DYNAMICLENGTHFIELDS
	 * @generated
	 */
	public Adapter createDYNAMICLENGTHFIELDSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DYNDEFINEDSPEC <em>DYNDEFINEDSPEC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DYNDEFINEDSPEC
	 * @generated
	 */
	public Adapter createDYNDEFINEDSPECAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DYNENDDOPREF <em>DYNENDDOPREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DYNENDDOPREF
	 * @generated
	 */
	public Adapter createDYNENDDOPREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DYNIDDEFMODEINFO <em>DYNIDDEFMODEINFO</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DYNIDDEFMODEINFO
	 * @generated
	 */
	public Adapter createDYNIDDEFMODEINFOAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.DYNIDDEFMODEINFOS <em>DYNIDDEFMODEINFOS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.DYNIDDEFMODEINFOS
	 * @generated
	 */
	public Adapter createDYNIDDEFMODEINFOSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUGROUP <em>ECUGROUP</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUGROUP
	 * @generated
	 */
	public Adapter createECUGROUPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUGROUPS <em>ECUGROUPS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUGROUPS
	 * @generated
	 */
	public Adapter createECUGROUPSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUMEM <em>ECUMEM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUMEM
	 * @generated
	 */
	public Adapter createECUMEMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUMEMCONNECTOR <em>ECUMEMCONNECTOR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUMEMCONNECTOR
	 * @generated
	 */
	public Adapter createECUMEMCONNECTORAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUMEMCONNECTORS <em>ECUMEMCONNECTORS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUMEMCONNECTORS
	 * @generated
	 */
	public Adapter createECUMEMCONNECTORSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUMEMS <em>ECUMEMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUMEMS
	 * @generated
	 */
	public Adapter createECUMEMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUPROXY <em>ECUPROXY</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUPROXY
	 * @generated
	 */
	public Adapter createECUPROXYAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUPROXYREFS <em>ECUPROXYREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUPROXYREFS
	 * @generated
	 */
	public Adapter createECUPROXYREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUSHAREDDATA <em>ECUSHAREDDATA</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUSHAREDDATA
	 * @generated
	 */
	public Adapter createECUSHAREDDATAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUSHAREDDATAREF <em>ECUSHAREDDATAREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUSHAREDDATAREF
	 * @generated
	 */
	public Adapter createECUSHAREDDATAREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUSHAREDDATAS <em>ECUSHAREDDATAS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUSHAREDDATAS
	 * @generated
	 */
	public Adapter createECUSHAREDDATASAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUVARIANT <em>ECUVARIANT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUVARIANT
	 * @generated
	 */
	public Adapter createECUVARIANTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUVARIANTPATTERN <em>ECUVARIANTPATTERN</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUVARIANTPATTERN
	 * @generated
	 */
	public Adapter createECUVARIANTPATTERNAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUVARIANTPATTERNS <em>ECUVARIANTPATTERNS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUVARIANTPATTERNS
	 * @generated
	 */
	public Adapter createECUVARIANTPATTERNSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ECUVARIANTS <em>ECUVARIANTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ECUVARIANTS
	 * @generated
	 */
	public Adapter createECUVARIANTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ENCRYPTCOMPRESSMETHOD <em>ENCRYPTCOMPRESSMETHOD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ENCRYPTCOMPRESSMETHOD
	 * @generated
	 */
	public Adapter createENCRYPTCOMPRESSMETHODAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ENDOFPDUFIELD <em>ENDOFPDUFIELD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ENDOFPDUFIELD
	 * @generated
	 */
	public Adapter createENDOFPDUFIELDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ENDOFPDUFIELDS <em>ENDOFPDUFIELDS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ENDOFPDUFIELDS
	 * @generated
	 */
	public Adapter createENDOFPDUFIELDSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ENVDATA <em>ENVDATA</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ENVDATA
	 * @generated
	 */
	public Adapter createENVDATAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ENVDATADESC <em>ENVDATADESC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ENVDATADESC
	 * @generated
	 */
	public Adapter createENVDATADESCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ENVDATADESCS <em>ENVDATADESCS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ENVDATADESCS
	 * @generated
	 */
	public Adapter createENVDATADESCSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ENVDATAS <em>ENVDATAS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ENVDATAS
	 * @generated
	 */
	public Adapter createENVDATASAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.EXPECTEDIDENT <em>EXPECTEDIDENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.EXPECTEDIDENT
	 * @generated
	 */
	public Adapter createEXPECTEDIDENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.EXPECTEDIDENTS <em>EXPECTEDIDENTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.EXPECTEDIDENTS
	 * @generated
	 */
	public Adapter createEXPECTEDIDENTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.EXTERNALACCESSMETHOD <em>EXTERNALACCESSMETHOD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.EXTERNALACCESSMETHOD
	 * @generated
	 */
	public Adapter createEXTERNALACCESSMETHODAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.EXTERNFLASHDATA <em>EXTERNFLASHDATA</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.EXTERNFLASHDATA
	 * @generated
	 */
	public Adapter createEXTERNFLASHDATAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FIELD <em>FIELD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FIELD
	 * @generated
	 */
	public Adapter createFIELDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FILE <em>FILE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FILE
	 * @generated
	 */
	public Adapter createFILEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FILESType <em>FILES Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FILESType
	 * @generated
	 */
	public Adapter createFILESTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FILTER <em>FILTER</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FILTER
	 * @generated
	 */
	public Adapter createFILTERAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FILTERS <em>FILTERS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FILTERS
	 * @generated
	 */
	public Adapter createFILTERSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FLASH <em>FLASH</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FLASH
	 * @generated
	 */
	public Adapter createFLASHAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FLASHCLASS <em>FLASHCLASS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FLASHCLASS
	 * @generated
	 */
	public Adapter createFLASHCLASSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FLASHCLASSREFS <em>FLASHCLASSREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FLASHCLASSREFS
	 * @generated
	 */
	public Adapter createFLASHCLASSREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FLASHCLASSS <em>FLASHCLASSS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FLASHCLASSS
	 * @generated
	 */
	public Adapter createFLASHCLASSSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FLASHDATA <em>FLASHDATA</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FLASHDATA
	 * @generated
	 */
	public Adapter createFLASHDATAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FLASHDATAS <em>FLASHDATAS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FLASHDATAS
	 * @generated
	 */
	public Adapter createFLASHDATASAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.Flow <em>Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.Flow
	 * @generated
	 */
	public Adapter createFlowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FUNCTCLASS <em>FUNCTCLASS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FUNCTCLASS
	 * @generated
	 */
	public Adapter createFUNCTCLASSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FUNCTCLASSREFS <em>FUNCTCLASSREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FUNCTCLASSREFS
	 * @generated
	 */
	public Adapter createFUNCTCLASSREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FUNCTCLASSS <em>FUNCTCLASSS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FUNCTCLASSS
	 * @generated
	 */
	public Adapter createFUNCTCLASSSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FUNCTIONALGROUP <em>FUNCTIONALGROUP</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FUNCTIONALGROUP
	 * @generated
	 */
	public Adapter createFUNCTIONALGROUPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FUNCTIONALGROUPREF <em>FUNCTIONALGROUPREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FUNCTIONALGROUPREF
	 * @generated
	 */
	public Adapter createFUNCTIONALGROUPREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FUNCTIONALGROUPS <em>FUNCTIONALGROUPS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FUNCTIONALGROUPS
	 * @generated
	 */
	public Adapter createFUNCTIONALGROUPSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FWCHECKSUM <em>FWCHECKSUM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FWCHECKSUM
	 * @generated
	 */
	public Adapter createFWCHECKSUMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.FWSIGNATURE <em>FWSIGNATURE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.FWSIGNATURE
	 * @generated
	 */
	public Adapter createFWSIGNATUREAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.GATEWAYLOGICALLINK <em>GATEWAYLOGICALLINK</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.GATEWAYLOGICALLINK
	 * @generated
	 */
	public Adapter createGATEWAYLOGICALLINKAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.GATEWAYLOGICALLINKREFS <em>GATEWAYLOGICALLINKREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.GATEWAYLOGICALLINKREFS
	 * @generated
	 */
	public Adapter createGATEWAYLOGICALLINKREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.GLOBALNEGRESPONSE <em>GLOBALNEGRESPONSE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.GLOBALNEGRESPONSE
	 * @generated
	 */
	public Adapter createGLOBALNEGRESPONSEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.GLOBALNEGRESPONSES <em>GLOBALNEGRESPONSES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.GLOBALNEGRESPONSES
	 * @generated
	 */
	public Adapter createGLOBALNEGRESPONSESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.HIERARCHYELEMENT <em>HIERARCHYELEMENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.HIERARCHYELEMENT
	 * @generated
	 */
	public Adapter createHIERARCHYELEMENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.IDENTDESC <em>IDENTDESC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.IDENTDESC
	 * @generated
	 */
	public Adapter createIDENTDESCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.IDENTDESCS <em>IDENTDESCS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.IDENTDESCS
	 * @generated
	 */
	public Adapter createIDENTDESCSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.IDENTVALUE <em>IDENTVALUE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.IDENTVALUE
	 * @generated
	 */
	public Adapter createIDENTVALUEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.IDENTVALUES <em>IDENTVALUES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.IDENTVALUES
	 * @generated
	 */
	public Adapter createIDENTVALUESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.IMPORTREFS <em>IMPORTREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.IMPORTREFS
	 * @generated
	 */
	public Adapter createIMPORTREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.INFOCOMPONENT <em>INFOCOMPONENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.INFOCOMPONENT
	 * @generated
	 */
	public Adapter createINFOCOMPONENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.INFOCOMPONENTREFS <em>INFOCOMPONENTREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.INFOCOMPONENTREFS
	 * @generated
	 */
	public Adapter createINFOCOMPONENTREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.INFOCOMPONENTS <em>INFOCOMPONENTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.INFOCOMPONENTS
	 * @generated
	 */
	public Adapter createINFOCOMPONENTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.Inline <em>Inline</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.Inline
	 * @generated
	 */
	public Adapter createInlineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.INPUTPARAM <em>INPUTPARAM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.INPUTPARAM
	 * @generated
	 */
	public Adapter createINPUTPARAMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.INPUTPARAMS <em>INPUTPARAMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.INPUTPARAMS
	 * @generated
	 */
	public Adapter createINPUTPARAMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.INTERNALCONSTR <em>INTERNALCONSTR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.INTERNALCONSTR
	 * @generated
	 */
	public Adapter createINTERNALCONSTRAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.INTERNFLASHDATA <em>INTERNFLASHDATA</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.INTERNFLASHDATA
	 * @generated
	 */
	public Adapter createINTERNFLASHDATAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.IType <em>IType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.IType
	 * @generated
	 */
	public Adapter createITypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LAYERREFS <em>LAYERREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LAYERREFS
	 * @generated
	 */
	public Adapter createLAYERREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LEADINGLENGTHINFOTYPE <em>LEADINGLENGTHINFOTYPE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LEADINGLENGTHINFOTYPE
	 * @generated
	 */
	public Adapter createLEADINGLENGTHINFOTYPEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LENGTHDESCRIPTOR <em>LENGTHDESCRIPTOR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LENGTHDESCRIPTOR
	 * @generated
	 */
	public Adapter createLENGTHDESCRIPTORAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LENGTHKEY <em>LENGTHKEY</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LENGTHKEY
	 * @generated
	 */
	public Adapter createLENGTHKEYAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LIMIT <em>LIMIT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LIMIT
	 * @generated
	 */
	public Adapter createLIMITAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LINKCOMPARAMREF <em>LINKCOMPARAMREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LINKCOMPARAMREF
	 * @generated
	 */
	public Adapter createLINKCOMPARAMREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LINKCOMPARAMREFS <em>LINKCOMPARAMREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LINKCOMPARAMREFS
	 * @generated
	 */
	public Adapter createLINKCOMPARAMREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LiType <em>Li Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LiType
	 * @generated
	 */
	public Adapter createLiTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LOGICALLINK <em>LOGICALLINK</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LOGICALLINK
	 * @generated
	 */
	public Adapter createLOGICALLINKAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LOGICALLINKREFS <em>LOGICALLINKREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LOGICALLINKREFS
	 * @generated
	 */
	public Adapter createLOGICALLINKREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.LOGICALLINKS <em>LOGICALLINKS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.LOGICALLINKS
	 * @generated
	 */
	public Adapter createLOGICALLINKSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MATCHINGCOMPONENT <em>MATCHINGCOMPONENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MATCHINGCOMPONENT
	 * @generated
	 */
	public Adapter createMATCHINGCOMPONENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MATCHINGCOMPONENTS <em>MATCHINGCOMPONENTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MATCHINGCOMPONENTS
	 * @generated
	 */
	public Adapter createMATCHINGCOMPONENTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MATCHINGPARAMETER <em>MATCHINGPARAMETER</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MATCHINGPARAMETER
	 * @generated
	 */
	public Adapter createMATCHINGPARAMETERAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MATCHINGPARAMETERS <em>MATCHINGPARAMETERS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MATCHINGPARAMETERS
	 * @generated
	 */
	public Adapter createMATCHINGPARAMETERSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MATCHINGREQUESTPARAM <em>MATCHINGREQUESTPARAM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MATCHINGREQUESTPARAM
	 * @generated
	 */
	public Adapter createMATCHINGREQUESTPARAMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MEM <em>MEM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MEM
	 * @generated
	 */
	public Adapter createMEMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MEMBERLOGICALLINK <em>MEMBERLOGICALLINK</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MEMBERLOGICALLINK
	 * @generated
	 */
	public Adapter createMEMBERLOGICALLINKAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MINMAXLENGTHTYPE <em>MINMAXLENGTHTYPE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MINMAXLENGTHTYPE
	 * @generated
	 */
	public Adapter createMINMAXLENGTHTYPEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MODELYEAR <em>MODELYEAR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MODELYEAR
	 * @generated
	 */
	public Adapter createMODELYEARAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MODIFICATION <em>MODIFICATION</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MODIFICATION
	 * @generated
	 */
	public Adapter createMODIFICATIONAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MODIFICATION1 <em>MODIFICATION1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MODIFICATION1
	 * @generated
	 */
	public Adapter createMODIFICATION1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MODIFICATIONS <em>MODIFICATIONS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MODIFICATIONS
	 * @generated
	 */
	public Adapter createMODIFICATIONSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MODIFICATIONS1 <em>MODIFICATIONS1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MODIFICATIONS1
	 * @generated
	 */
	public Adapter createMODIFICATIONS1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MULTIPLEECUJOB <em>MULTIPLEECUJOB</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MULTIPLEECUJOB
	 * @generated
	 */
	public Adapter createMULTIPLEECUJOBAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MULTIPLEECUJOBS <em>MULTIPLEECUJOBS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MULTIPLEECUJOBS
	 * @generated
	 */
	public Adapter createMULTIPLEECUJOBSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MULTIPLEECUJOBSPEC <em>MULTIPLEECUJOBSPEC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MULTIPLEECUJOBSPEC
	 * @generated
	 */
	public Adapter createMULTIPLEECUJOBSPECAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MUX <em>MUX</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MUX
	 * @generated
	 */
	public Adapter createMUXAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.MUXS <em>MUXS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.MUXS
	 * @generated
	 */
	public Adapter createMUXSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NEGOFFSET <em>NEGOFFSET</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NEGOFFSET
	 * @generated
	 */
	public Adapter createNEGOFFSETAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NEGOUTPUTPARAM <em>NEGOUTPUTPARAM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NEGOUTPUTPARAM
	 * @generated
	 */
	public Adapter createNEGOUTPUTPARAMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NEGOUTPUTPARAMS <em>NEGOUTPUTPARAMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NEGOUTPUTPARAMS
	 * @generated
	 */
	public Adapter createNEGOUTPUTPARAMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NEGRESPONSE <em>NEGRESPONSE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NEGRESPONSE
	 * @generated
	 */
	public Adapter createNEGRESPONSEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NEGRESPONSEREFS <em>NEGRESPONSEREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NEGRESPONSEREFS
	 * @generated
	 */
	public Adapter createNEGRESPONSEREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NEGRESPONSES <em>NEGRESPONSES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NEGRESPONSES
	 * @generated
	 */
	public Adapter createNEGRESPONSESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NOTINHERITEDDIAGCOMM <em>NOTINHERITEDDIAGCOMM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NOTINHERITEDDIAGCOMM
	 * @generated
	 */
	public Adapter createNOTINHERITEDDIAGCOMMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NOTINHERITEDDIAGCOMMS <em>NOTINHERITEDDIAGCOMMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NOTINHERITEDDIAGCOMMS
	 * @generated
	 */
	public Adapter createNOTINHERITEDDIAGCOMMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NOTINHERITEDVARIABLE <em>NOTINHERITEDVARIABLE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NOTINHERITEDVARIABLE
	 * @generated
	 */
	public Adapter createNOTINHERITEDVARIABLEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.NOTINHERITEDVARIABLES <em>NOTINHERITEDVARIABLES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.NOTINHERITEDVARIABLES
	 * @generated
	 */
	public Adapter createNOTINHERITEDVARIABLESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ODX <em>ODX</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ODX
	 * @generated
	 */
	public Adapter createODXAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ODXCATEGORY <em>ODXCATEGORY</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ODXCATEGORY
	 * @generated
	 */
	public Adapter createODXCATEGORYAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ODXLINK <em>ODXLINK</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ODXLINK
	 * @generated
	 */
	public Adapter createODXLINKAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ODXLINK1 <em>ODXLINK1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ODXLINK1
	 * @generated
	 */
	public Adapter createODXLINK1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.OEM <em>OEM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.OEM
	 * @generated
	 */
	public Adapter createOEMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.OlType <em>Ol Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.OlType
	 * @generated
	 */
	public Adapter createOlTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.OUTPUTPARAM <em>OUTPUTPARAM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.OUTPUTPARAM
	 * @generated
	 */
	public Adapter createOUTPUTPARAMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.OUTPUTPARAMS <em>OUTPUTPARAMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.OUTPUTPARAMS
	 * @generated
	 */
	public Adapter createOUTPUTPARAMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.OWNIDENT <em>OWNIDENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.OWNIDENT
	 * @generated
	 */
	public Adapter createOWNIDENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.OWNIDENTS <em>OWNIDENTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.OWNIDENTS
	 * @generated
	 */
	public Adapter createOWNIDENTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.P <em>P</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.P
	 * @generated
	 */
	public Adapter createPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PARAM <em>PARAM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PARAM
	 * @generated
	 */
	public Adapter createPARAMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PARAMLENGTHINFOTYPE <em>PARAMLENGTHINFOTYPE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PARAMLENGTHINFOTYPE
	 * @generated
	 */
	public Adapter createPARAMLENGTHINFOTYPEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PARAMS <em>PARAMS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PARAMS
	 * @generated
	 */
	public Adapter createPARAMSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PARENTREF <em>PARENTREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PARENTREF
	 * @generated
	 */
	public Adapter createPARENTREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PARENTREFS <em>PARENTREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PARENTREFS
	 * @generated
	 */
	public Adapter createPARENTREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PHYSCONST <em>PHYSCONST</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PHYSCONST
	 * @generated
	 */
	public Adapter createPHYSCONSTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PHYSICALDIMENSION <em>PHYSICALDIMENSION</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PHYSICALDIMENSION
	 * @generated
	 */
	public Adapter createPHYSICALDIMENSIONAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PHYSICALDIMENSIONS <em>PHYSICALDIMENSIONS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PHYSICALDIMENSIONS
	 * @generated
	 */
	public Adapter createPHYSICALDIMENSIONSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PHYSICALTYPE <em>PHYSICALTYPE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PHYSICALTYPE
	 * @generated
	 */
	public Adapter createPHYSICALTYPEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PHYSICALVEHICLELINK <em>PHYSICALVEHICLELINK</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PHYSICALVEHICLELINK
	 * @generated
	 */
	public Adapter createPHYSICALVEHICLELINKAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PHYSICALVEHICLELINKS <em>PHYSICALVEHICLELINKS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PHYSICALVEHICLELINKS
	 * @generated
	 */
	public Adapter createPHYSICALVEHICLELINKSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PHYSMEM <em>PHYSMEM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PHYSMEM
	 * @generated
	 */
	public Adapter createPHYSMEMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PHYSSEGMENT <em>PHYSSEGMENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PHYSSEGMENT
	 * @generated
	 */
	public Adapter createPHYSSEGMENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PHYSSEGMENTS <em>PHYSSEGMENTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PHYSSEGMENTS
	 * @generated
	 */
	public Adapter createPHYSSEGMENTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.POSITIONABLEPARAM <em>POSITIONABLEPARAM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.POSITIONABLEPARAM
	 * @generated
	 */
	public Adapter createPOSITIONABLEPARAMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.POSOFFSET <em>POSOFFSET</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.POSOFFSET
	 * @generated
	 */
	public Adapter createPOSOFFSETAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.POSRESPONSE <em>POSRESPONSE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.POSRESPONSE
	 * @generated
	 */
	public Adapter createPOSRESPONSEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.POSRESPONSEREFS <em>POSRESPONSEREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.POSRESPONSEREFS
	 * @generated
	 */
	public Adapter createPOSRESPONSEREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.POSRESPONSES <em>POSRESPONSES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.POSRESPONSES
	 * @generated
	 */
	public Adapter createPOSRESPONSESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROGCODE <em>PROGCODE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROGCODE
	 * @generated
	 */
	public Adapter createPROGCODEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROGCODES <em>PROGCODES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROGCODES
	 * @generated
	 */
	public Adapter createPROGCODESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROJECTIDENT <em>PROJECTIDENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROJECTIDENT
	 * @generated
	 */
	public Adapter createPROJECTIDENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROJECTIDENTS <em>PROJECTIDENTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROJECTIDENTS
	 * @generated
	 */
	public Adapter createPROJECTIDENTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROJECTINFO <em>PROJECTINFO</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROJECTINFO
	 * @generated
	 */
	public Adapter createPROJECTINFOAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROJECTINFOS <em>PROJECTINFOS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROJECTINFOS
	 * @generated
	 */
	public Adapter createPROJECTINFOSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROTOCOL <em>PROTOCOL</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROTOCOL
	 * @generated
	 */
	public Adapter createPROTOCOLAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROTOCOLREF <em>PROTOCOLREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROTOCOLREF
	 * @generated
	 */
	public Adapter createPROTOCOLREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROTOCOLS <em>PROTOCOLS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROTOCOLS
	 * @generated
	 */
	public Adapter createPROTOCOLSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.PROTOCOLSNREFS <em>PROTOCOLSNREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.PROTOCOLSNREFS
	 * @generated
	 */
	public Adapter createPROTOCOLSNREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.RELATEDDIAGCOMMREF <em>RELATEDDIAGCOMMREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.RELATEDDIAGCOMMREF
	 * @generated
	 */
	public Adapter createRELATEDDIAGCOMMREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.RELATEDDIAGCOMMREFS <em>RELATEDDIAGCOMMREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.RELATEDDIAGCOMMREFS
	 * @generated
	 */
	public Adapter createRELATEDDIAGCOMMREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.RELATEDDOC <em>RELATEDDOC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.RELATEDDOC
	 * @generated
	 */
	public Adapter createRELATEDDOCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.RELATEDDOC1 <em>RELATEDDOC1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.RELATEDDOC1
	 * @generated
	 */
	public Adapter createRELATEDDOC1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.RELATEDDOCS <em>RELATEDDOCS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.RELATEDDOCS
	 * @generated
	 */
	public Adapter createRELATEDDOCSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.RELATEDDOCS1 <em>RELATEDDOCS1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.RELATEDDOCS1
	 * @generated
	 */
	public Adapter createRELATEDDOCS1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.REQUEST <em>REQUEST</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.REQUEST
	 * @generated
	 */
	public Adapter createREQUESTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.REQUESTS <em>REQUESTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.REQUESTS
	 * @generated
	 */
	public Adapter createREQUESTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.RESERVED <em>RESERVED</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.RESERVED
	 * @generated
	 */
	public Adapter createRESERVEDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.RESPONSE <em>RESPONSE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.RESPONSE
	 * @generated
	 */
	public Adapter createRESPONSEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ROLES <em>ROLES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ROLES
	 * @generated
	 */
	public Adapter createROLESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.ROLES1 <em>ROLES1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.ROLES1
	 * @generated
	 */
	public Adapter createROLES1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SCALECONSTR <em>SCALECONSTR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SCALECONSTR
	 * @generated
	 */
	public Adapter createSCALECONSTRAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SCALECONSTRS <em>SCALECONSTRS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SCALECONSTRS
	 * @generated
	 */
	public Adapter createSCALECONSTRSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SD <em>SD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SD
	 * @generated
	 */
	public Adapter createSDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SD1 <em>SD1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SD1
	 * @generated
	 */
	public Adapter createSD1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SDG <em>SDG</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SDG
	 * @generated
	 */
	public Adapter createSDGAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SDG1 <em>SDG1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SDG1
	 * @generated
	 */
	public Adapter createSDG1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SDGCAPTION <em>SDGCAPTION</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SDGCAPTION
	 * @generated
	 */
	public Adapter createSDGCAPTIONAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SDGCAPTION1 <em>SDGCAPTION1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SDGCAPTION1
	 * @generated
	 */
	public Adapter createSDGCAPTION1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SDGS <em>SDGS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SDGS
	 * @generated
	 */
	public Adapter createSDGSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SDGS1 <em>SDGS1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SDGS1
	 * @generated
	 */
	public Adapter createSDGS1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SECURITY <em>SECURITY</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SECURITY
	 * @generated
	 */
	public Adapter createSECURITYAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SECURITYMETHOD <em>SECURITYMETHOD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SECURITYMETHOD
	 * @generated
	 */
	public Adapter createSECURITYMETHODAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SECURITYS <em>SECURITYS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SECURITYS
	 * @generated
	 */
	public Adapter createSECURITYSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SEGMENT <em>SEGMENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SEGMENT
	 * @generated
	 */
	public Adapter createSEGMENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SEGMENTS <em>SEGMENTS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SEGMENTS
	 * @generated
	 */
	public Adapter createSEGMENTSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SELECTIONTABLEREFS <em>SELECTIONTABLEREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SELECTIONTABLEREFS
	 * @generated
	 */
	public Adapter createSELECTIONTABLEREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SESSION <em>SESSION</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SESSION
	 * @generated
	 */
	public Adapter createSESSIONAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SESSIONDESC <em>SESSIONDESC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SESSIONDESC
	 * @generated
	 */
	public Adapter createSESSIONDESCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SESSIONDESCS <em>SESSIONDESCS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SESSIONDESCS
	 * @generated
	 */
	public Adapter createSESSIONDESCSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SESSIONS <em>SESSIONS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SESSIONS
	 * @generated
	 */
	public Adapter createSESSIONSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SINGLEECUJOB <em>SINGLEECUJOB</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SINGLEECUJOB
	 * @generated
	 */
	public Adapter createSINGLEECUJOBAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SIZEDEFFILTER <em>SIZEDEFFILTER</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SIZEDEFFILTER
	 * @generated
	 */
	public Adapter createSIZEDEFFILTERAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SIZEDEFPHYSSEGMENT <em>SIZEDEFPHYSSEGMENT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SIZEDEFPHYSSEGMENT
	 * @generated
	 */
	public Adapter createSIZEDEFPHYSSEGMENTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SNREF <em>SNREF</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SNREF
	 * @generated
	 */
	public Adapter createSNREFAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SOURCEENDADDRESS <em>SOURCEENDADDRESS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SOURCEENDADDRESS
	 * @generated
	 */
	public Adapter createSOURCEENDADDRESSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SPECIALDATA <em>SPECIALDATA</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SPECIALDATA
	 * @generated
	 */
	public Adapter createSPECIALDATAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SPECIALDATA1 <em>SPECIALDATA1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SPECIALDATA1
	 * @generated
	 */
	public Adapter createSPECIALDATA1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.STANDARDLENGTHTYPE <em>STANDARDLENGTHTYPE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.STANDARDLENGTHTYPE
	 * @generated
	 */
	public Adapter createSTANDARDLENGTHTYPEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.STATICFIELD <em>STATICFIELD</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.STATICFIELD
	 * @generated
	 */
	public Adapter createSTATICFIELDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.STATICFIELDS <em>STATICFIELDS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.STATICFIELDS
	 * @generated
	 */
	public Adapter createSTATICFIELDSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.STRUCTURE <em>STRUCTURE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.STRUCTURE
	 * @generated
	 */
	public Adapter createSTRUCTUREAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.STRUCTURES <em>STRUCTURES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.STRUCTURES
	 * @generated
	 */
	public Adapter createSTRUCTURESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SubType <em>Sub Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SubType
	 * @generated
	 */
	public Adapter createSubTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SUPPORTEDDYNID <em>SUPPORTEDDYNID</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SUPPORTEDDYNID
	 * @generated
	 */
	public Adapter createSUPPORTEDDYNIDAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SUPPORTEDDYNIDS <em>SUPPORTEDDYNIDS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SUPPORTEDDYNIDS
	 * @generated
	 */
	public Adapter createSUPPORTEDDYNIDSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SupType <em>Sup Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SupType
	 * @generated
	 */
	public Adapter createSupTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SWITCHKEY <em>SWITCHKEY</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SWITCHKEY
	 * @generated
	 */
	public Adapter createSWITCHKEYAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SWVARIABLE <em>SWVARIABLE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SWVARIABLE
	 * @generated
	 */
	public Adapter createSWVARIABLEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SWVARIABLES <em>SWVARIABLES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SWVARIABLES
	 * @generated
	 */
	public Adapter createSWVARIABLESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.SYSTEM <em>SYSTEM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.SYSTEM
	 * @generated
	 */
	public Adapter createSYSTEMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TABLE <em>TABLE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TABLE
	 * @generated
	 */
	public Adapter createTABLEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TABLEENTRY <em>TABLEENTRY</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TABLEENTRY
	 * @generated
	 */
	public Adapter createTABLEENTRYAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TABLEKEY <em>TABLEKEY</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TABLEKEY
	 * @generated
	 */
	public Adapter createTABLEKEYAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TABLEROW <em>TABLEROW</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TABLEROW
	 * @generated
	 */
	public Adapter createTABLEROWAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TABLES <em>TABLES</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TABLES
	 * @generated
	 */
	public Adapter createTABLESAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TABLESTRUCT <em>TABLESTRUCT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TABLESTRUCT
	 * @generated
	 */
	public Adapter createTABLESTRUCTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TARGETADDROFFSET <em>TARGETADDROFFSET</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TARGETADDROFFSET
	 * @generated
	 */
	public Adapter createTARGETADDROFFSETAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TEAMMEMBER <em>TEAMMEMBER</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TEAMMEMBER
	 * @generated
	 */
	public Adapter createTEAMMEMBERAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TEAMMEMBER1 <em>TEAMMEMBER1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TEAMMEMBER1
	 * @generated
	 */
	public Adapter createTEAMMEMBER1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TEAMMEMBERS <em>TEAMMEMBERS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TEAMMEMBERS
	 * @generated
	 */
	public Adapter createTEAMMEMBERSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TEAMMEMBERS1 <em>TEAMMEMBERS1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TEAMMEMBERS1
	 * @generated
	 */
	public Adapter createTEAMMEMBERS1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TEXT <em>TEXT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TEXT
	 * @generated
	 */
	public Adapter createTEXTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.TEXT1 <em>TEXT1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.TEXT1
	 * @generated
	 */
	public Adapter createTEXT1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.UlType <em>Ul Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.UlType
	 * @generated
	 */
	public Adapter createUlTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.UNCOMPRESSEDSIZE <em>UNCOMPRESSEDSIZE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.UNCOMPRESSEDSIZE
	 * @generated
	 */
	public Adapter createUNCOMPRESSEDSIZEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.UNIONVALUE <em>UNIONVALUE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.UNIONVALUE
	 * @generated
	 */
	public Adapter createUNIONVALUEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.UNIT <em>UNIT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.UNIT
	 * @generated
	 */
	public Adapter createUNITAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.UNITGROUP <em>UNITGROUP</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.UNITGROUP
	 * @generated
	 */
	public Adapter createUNITGROUPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.UNITGROUPS <em>UNITGROUPS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.UNITGROUPS
	 * @generated
	 */
	public Adapter createUNITGROUPSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.UNITREFS <em>UNITREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.UNITREFS
	 * @generated
	 */
	public Adapter createUNITREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.UNITS <em>UNITS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.UNITS
	 * @generated
	 */
	public Adapter createUNITSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.UNITSPEC <em>UNITSPEC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.UNITSPEC
	 * @generated
	 */
	public Adapter createUNITSPECAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.V <em>V</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.V
	 * @generated
	 */
	public Adapter createVAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VALIDITYFOR <em>VALIDITYFOR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VALIDITYFOR
	 * @generated
	 */
	public Adapter createVALIDITYFORAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VALUE <em>VALUE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VALUE
	 * @generated
	 */
	public Adapter createVALUEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VARIABLEGROUP <em>VARIABLEGROUP</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VARIABLEGROUP
	 * @generated
	 */
	public Adapter createVARIABLEGROUPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VARIABLEGROUPS <em>VARIABLEGROUPS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VARIABLEGROUPS
	 * @generated
	 */
	public Adapter createVARIABLEGROUPSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLECONNECTOR <em>VEHICLECONNECTOR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLECONNECTOR
	 * @generated
	 */
	public Adapter createVEHICLECONNECTORAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLECONNECTORPIN <em>VEHICLECONNECTORPIN</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLECONNECTORPIN
	 * @generated
	 */
	public Adapter createVEHICLECONNECTORPINAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLECONNECTORPINREFS <em>VEHICLECONNECTORPINREFS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLECONNECTORPINREFS
	 * @generated
	 */
	public Adapter createVEHICLECONNECTORPINREFSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLECONNECTORPINS <em>VEHICLECONNECTORPINS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLECONNECTORPINS
	 * @generated
	 */
	public Adapter createVEHICLECONNECTORPINSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLECONNECTORS <em>VEHICLECONNECTORS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLECONNECTORS
	 * @generated
	 */
	public Adapter createVEHICLECONNECTORSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLEINFORMATION <em>VEHICLEINFORMATION</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLEINFORMATION
	 * @generated
	 */
	public Adapter createVEHICLEINFORMATIONAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLEINFORMATIONS <em>VEHICLEINFORMATIONS</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLEINFORMATIONS
	 * @generated
	 */
	public Adapter createVEHICLEINFORMATIONSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLEINFOSPEC <em>VEHICLEINFOSPEC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLEINFOSPEC
	 * @generated
	 */
	public Adapter createVEHICLEINFOSPECAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLEMODEL <em>VEHICLEMODEL</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLEMODEL
	 * @generated
	 */
	public Adapter createVEHICLEMODELAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VEHICLETYPE <em>VEHICLETYPE</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VEHICLETYPE
	 * @generated
	 */
	public Adapter createVEHICLETYPEAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.VT <em>VT</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.VT
	 * @generated
	 */
	public Adapter createVTAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.XDOC <em>XDOC</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.XDOC
	 * @generated
	 */
	public Adapter createXDOCAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link OdxXhtml.XDOC1 <em>XDOC1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see OdxXhtml.XDOC1
	 * @generated
	 */
	public Adapter createXDOC1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //OdxXhtmlAdapterFactory
