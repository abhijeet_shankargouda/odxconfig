/**
 */
package OdxXhtml.util;

import OdxXhtml.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage
 * @generated
 */
public class OdxXhtmlSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OdxXhtmlPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdxXhtmlSwitch() {
		if (modelPackage == null) {
			modelPackage = OdxXhtmlPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OdxXhtmlPackage.ABLOCK: {
				ABLOCK ablock = (ABLOCK)theEObject;
				T result = caseABLOCK(ablock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ABLOCKS_TYPE: {
				ABLOCKSType ablocksType = (ABLOCKSType)theEObject;
				T result = caseABLOCKSType(ablocksType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ACCESSLEVEL: {
				ACCESSLEVEL accesslevel = (ACCESSLEVEL)theEObject;
				T result = caseACCESSLEVEL(accesslevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ACCESSLEVELS: {
				ACCESSLEVELS accesslevels = (ACCESSLEVELS)theEObject;
				T result = caseACCESSLEVELS(accesslevels);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ADDRDEFFILTER: {
				ADDRDEFFILTER addrdeffilter = (ADDRDEFFILTER)theEObject;
				T result = caseADDRDEFFILTER(addrdeffilter);
				if (result == null) result = caseFILTER(addrdeffilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ADDRDEFPHYSSEGMENT: {
				ADDRDEFPHYSSEGMENT addrdefphyssegment = (ADDRDEFPHYSSEGMENT)theEObject;
				T result = caseADDRDEFPHYSSEGMENT(addrdefphyssegment);
				if (result == null) result = casePHYSSEGMENT(addrdefphyssegment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ADMINDATA: {
				ADMINDATA admindata = (ADMINDATA)theEObject;
				T result = caseADMINDATA(admindata);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ADMINDATA1: {
				ADMINDATA1 admindata1 = (ADMINDATA1)theEObject;
				T result = caseADMINDATA1(admindata1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ALLVALUE: {
				ALLVALUE allvalue = (ALLVALUE)theEObject;
				T result = caseALLVALUE(allvalue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.AUDIENCE: {
				AUDIENCE audience = (AUDIENCE)theEObject;
				T result = caseAUDIENCE(audience);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.AUTMETHOD: {
				AUTMETHOD autmethod = (AUTMETHOD)theEObject;
				T result = caseAUTMETHOD(autmethod);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.AUTMETHODS: {
				AUTMETHODS autmethods = (AUTMETHODS)theEObject;
				T result = caseAUTMETHODS(autmethods);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.BASEVARIANT: {
				BASEVARIANT basevariant = (BASEVARIANT)theEObject;
				T result = caseBASEVARIANT(basevariant);
				if (result == null) result = caseHIERARCHYELEMENT(basevariant);
				if (result == null) result = caseDIAGLAYER(basevariant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.BASEVARIANTREF: {
				BASEVARIANTREF basevariantref = (BASEVARIANTREF)theEObject;
				T result = caseBASEVARIANTREF(basevariantref);
				if (result == null) result = casePARENTREF(basevariantref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.BASEVARIANTS: {
				BASEVARIANTS basevariants = (BASEVARIANTS)theEObject;
				T result = caseBASEVARIANTS(basevariants);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.BASICSTRUCTURE: {
				BASICSTRUCTURE basicstructure = (BASICSTRUCTURE)theEObject;
				T result = caseBASICSTRUCTURE(basicstructure);
				if (result == null) result = caseCOMPLEXDOP(basicstructure);
				if (result == null) result = caseDOPBASE(basicstructure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.BLOCK: {
				Block block = (Block)theEObject;
				T result = caseBlock(block);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.BTYPE: {
				BType bType = (BType)theEObject;
				T result = caseBType(bType);
				if (result == null) result = caseInline(bType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.CASE: {
				CASE case_ = (CASE)theEObject;
				T result = caseCASE(case_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.CASES: {
				CASES cases = (CASES)theEObject;
				T result = caseCASES(cases);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.CATALOG: {
				CATALOG catalog = (CATALOG)theEObject;
				T result = caseCATALOG(catalog);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.CHECKSUM: {
				CHECKSUM checksum = (CHECKSUM)theEObject;
				T result = caseCHECKSUM(checksum);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.CHECKSUMRESULT: {
				CHECKSUMRESULT checksumresult = (CHECKSUMRESULT)theEObject;
				T result = caseCHECKSUMRESULT(checksumresult);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.CHECKSUMS: {
				CHECKSUMS checksums = (CHECKSUMS)theEObject;
				T result = caseCHECKSUMS(checksums);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.CODEDCONST: {
				CODEDCONST codedconst = (CODEDCONST)theEObject;
				T result = caseCODEDCONST(codedconst);
				if (result == null) result = casePOSITIONABLEPARAM(codedconst);
				if (result == null) result = casePARAM(codedconst);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMMRELATION: {
				COMMRELATION commrelation = (COMMRELATION)theEObject;
				T result = caseCOMMRELATION(commrelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMMRELATIONS: {
				COMMRELATIONS commrelations = (COMMRELATIONS)theEObject;
				T result = caseCOMMRELATIONS(commrelations);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYDATA: {
				COMPANYDATA companydata = (COMPANYDATA)theEObject;
				T result = caseCOMPANYDATA(companydata);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYDATA1: {
				COMPANYDATA1 companydata1 = (COMPANYDATA1)theEObject;
				T result = caseCOMPANYDATA1(companydata1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYDATAS: {
				COMPANYDATAS companydatas = (COMPANYDATAS)theEObject;
				T result = caseCOMPANYDATAS(companydatas);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYDATAS_TYPE: {
				COMPANYDATASType companydatasType = (COMPANYDATASType)theEObject;
				T result = caseCOMPANYDATASType(companydatasType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYDOCINFO: {
				COMPANYDOCINFO companydocinfo = (COMPANYDOCINFO)theEObject;
				T result = caseCOMPANYDOCINFO(companydocinfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYDOCINFO1: {
				COMPANYDOCINFO1 companydocinfo1 = (COMPANYDOCINFO1)theEObject;
				T result = caseCOMPANYDOCINFO1(companydocinfo1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYDOCINFOS: {
				COMPANYDOCINFOS companydocinfos = (COMPANYDOCINFOS)theEObject;
				T result = caseCOMPANYDOCINFOS(companydocinfos);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYDOCINFOS1: {
				COMPANYDOCINFOS1 companydocinfos1 = (COMPANYDOCINFOS1)theEObject;
				T result = caseCOMPANYDOCINFOS1(companydocinfos1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYREVISIONINFO: {
				COMPANYREVISIONINFO companyrevisioninfo = (COMPANYREVISIONINFO)theEObject;
				T result = caseCOMPANYREVISIONINFO(companyrevisioninfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYREVISIONINFO1: {
				COMPANYREVISIONINFO1 companyrevisioninfo1 = (COMPANYREVISIONINFO1)theEObject;
				T result = caseCOMPANYREVISIONINFO1(companyrevisioninfo1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYREVISIONINFOS: {
				COMPANYREVISIONINFOS companyrevisioninfos = (COMPANYREVISIONINFOS)theEObject;
				T result = caseCOMPANYREVISIONINFOS(companyrevisioninfos);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYREVISIONINFOS1: {
				COMPANYREVISIONINFOS1 companyrevisioninfos1 = (COMPANYREVISIONINFOS1)theEObject;
				T result = caseCOMPANYREVISIONINFOS1(companyrevisioninfos1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYSPECIFICINFO: {
				COMPANYSPECIFICINFO companyspecificinfo = (COMPANYSPECIFICINFO)theEObject;
				T result = caseCOMPANYSPECIFICINFO(companyspecificinfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1: {
				COMPANYSPECIFICINFO1 companyspecificinfo1 = (COMPANYSPECIFICINFO1)theEObject;
				T result = caseCOMPANYSPECIFICINFO1(companyspecificinfo1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPARAM: {
				COMPARAM comparam = (COMPARAM)theEObject;
				T result = caseCOMPARAM(comparam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPARAMREF: {
				COMPARAMREF comparamref = (COMPARAMREF)theEObject;
				T result = caseCOMPARAMREF(comparamref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPARAMREFS: {
				COMPARAMREFS comparamrefs = (COMPARAMREFS)theEObject;
				T result = caseCOMPARAMREFS(comparamrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPARAMS: {
				COMPARAMS comparams = (COMPARAMS)theEObject;
				T result = caseCOMPARAMS(comparams);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPARAMSPEC: {
				COMPARAMSPEC comparamspec = (COMPARAMSPEC)theEObject;
				T result = caseCOMPARAMSPEC(comparamspec);
				if (result == null) result = caseODXCATEGORY(comparamspec);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPLEXDOP: {
				COMPLEXDOP complexdop = (COMPLEXDOP)theEObject;
				T result = caseCOMPLEXDOP(complexdop);
				if (result == null) result = caseDOPBASE(complexdop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUCONST: {
				COMPUCONST compuconst = (COMPUCONST)theEObject;
				T result = caseCOMPUCONST(compuconst);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUDEFAULTVALUE: {
				COMPUDEFAULTVALUE compudefaultvalue = (COMPUDEFAULTVALUE)theEObject;
				T result = caseCOMPUDEFAULTVALUE(compudefaultvalue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUDENOMINATOR: {
				COMPUDENOMINATOR compudenominator = (COMPUDENOMINATOR)theEObject;
				T result = caseCOMPUDENOMINATOR(compudenominator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS: {
				COMPUINTERNALTOPHYS compuinternaltophys = (COMPUINTERNALTOPHYS)theEObject;
				T result = caseCOMPUINTERNALTOPHYS(compuinternaltophys);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUINVERSEVALUE: {
				COMPUINVERSEVALUE compuinversevalue = (COMPUINVERSEVALUE)theEObject;
				T result = caseCOMPUINVERSEVALUE(compuinversevalue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUMETHOD: {
				COMPUMETHOD compumethod = (COMPUMETHOD)theEObject;
				T result = caseCOMPUMETHOD(compumethod);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUNUMERATOR: {
				COMPUNUMERATOR compunumerator = (COMPUNUMERATOR)theEObject;
				T result = caseCOMPUNUMERATOR(compunumerator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUPHYSTOINTERNAL: {
				COMPUPHYSTOINTERNAL compuphystointernal = (COMPUPHYSTOINTERNAL)theEObject;
				T result = caseCOMPUPHYSTOINTERNAL(compuphystointernal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPURATIONALCOEFFS: {
				COMPURATIONALCOEFFS compurationalcoeffs = (COMPURATIONALCOEFFS)theEObject;
				T result = caseCOMPURATIONALCOEFFS(compurationalcoeffs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUSCALE: {
				COMPUSCALE compuscale = (COMPUSCALE)theEObject;
				T result = caseCOMPUSCALE(compuscale);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.COMPUSCALES: {
				COMPUSCALES compuscales = (COMPUSCALES)theEObject;
				T result = caseCOMPUSCALES(compuscales);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DATABLOCK: {
				DATABLOCK datablock = (DATABLOCK)theEObject;
				T result = caseDATABLOCK(datablock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DATABLOCKREFS: {
				DATABLOCKREFS datablockrefs = (DATABLOCKREFS)theEObject;
				T result = caseDATABLOCKREFS(datablockrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DATABLOCKS: {
				DATABLOCKS datablocks = (DATABLOCKS)theEObject;
				T result = caseDATABLOCKS(datablocks);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DATAFILE: {
				DATAFILE datafile = (DATAFILE)theEObject;
				T result = caseDATAFILE(datafile);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DATAFORMAT: {
				DATAFORMAT dataformat = (DATAFORMAT)theEObject;
				T result = caseDATAFORMAT(dataformat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DATAOBJECTPROP: {
				DATAOBJECTPROP dataobjectprop = (DATAOBJECTPROP)theEObject;
				T result = caseDATAOBJECTPROP(dataobjectprop);
				if (result == null) result = caseDOPBASE(dataobjectprop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DATAOBJECTPROPS: {
				DATAOBJECTPROPS dataobjectprops = (DATAOBJECTPROPS)theEObject;
				T result = caseDATAOBJECTPROPS(dataobjectprops);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DEFAULTCASE: {
				DEFAULTCASE defaultcase = (DEFAULTCASE)theEObject;
				T result = caseDEFAULTCASE(defaultcase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DESCRIPTION: {
				DESCRIPTION description = (DESCRIPTION)theEObject;
				T result = caseDESCRIPTION(description);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DESCRIPTION1: {
				DESCRIPTION1 description1 = (DESCRIPTION1)theEObject;
				T result = caseDESCRIPTION1(description1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DETERMINENUMBEROFITEMS: {
				DETERMINENUMBEROFITEMS determinenumberofitems = (DETERMINENUMBEROFITEMS)theEObject;
				T result = caseDETERMINENUMBEROFITEMS(determinenumberofitems);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGCODEDTYPE: {
				DIAGCODEDTYPE diagcodedtype = (DIAGCODEDTYPE)theEObject;
				T result = caseDIAGCODEDTYPE(diagcodedtype);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGCOMM: {
				DIAGCOMM diagcomm = (DIAGCOMM)theEObject;
				T result = caseDIAGCOMM(diagcomm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGCOMMS: {
				DIAGCOMMS diagcomms = (DIAGCOMMS)theEObject;
				T result = caseDIAGCOMMS(diagcomms);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC: {
				DIAGDATADICTIONARYSPEC diagdatadictionaryspec = (DIAGDATADICTIONARYSPEC)theEObject;
				T result = caseDIAGDATADICTIONARYSPEC(diagdatadictionaryspec);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGLAYER: {
				DIAGLAYER diaglayer = (DIAGLAYER)theEObject;
				T result = caseDIAGLAYER(diaglayer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGLAYERCONTAINER: {
				DIAGLAYERCONTAINER diaglayercontainer = (DIAGLAYERCONTAINER)theEObject;
				T result = caseDIAGLAYERCONTAINER(diaglayercontainer);
				if (result == null) result = caseODXCATEGORY(diaglayercontainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGLAYERREFS: {
				DIAGLAYERREFS diaglayerrefs = (DIAGLAYERREFS)theEObject;
				T result = caseDIAGLAYERREFS(diaglayerrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGSERVICE: {
				DIAGSERVICE diagservice = (DIAGSERVICE)theEObject;
				T result = caseDIAGSERVICE(diagservice);
				if (result == null) result = caseDIAGCOMM(diagservice);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGVARIABLE: {
				DIAGVARIABLE diagvariable = (DIAGVARIABLE)theEObject;
				T result = caseDIAGVARIABLE(diagvariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DIAGVARIABLES: {
				DIAGVARIABLES diagvariables = (DIAGVARIABLES)theEObject;
				T result = caseDIAGVARIABLES(diagvariables);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DOCREVISION: {
				DOCREVISION docrevision = (DOCREVISION)theEObject;
				T result = caseDOCREVISION(docrevision);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DOCREVISION1: {
				DOCREVISION1 docrevision1 = (DOCREVISION1)theEObject;
				T result = caseDOCREVISION1(docrevision1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DOCREVISIONS: {
				DOCREVISIONS docrevisions = (DOCREVISIONS)theEObject;
				T result = caseDOCREVISIONS(docrevisions);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DOCREVISIONS1: {
				DOCREVISIONS1 docrevisions1 = (DOCREVISIONS1)theEObject;
				T result = caseDOCREVISIONS1(docrevisions1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				T result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DOPBASE: {
				DOPBASE dopbase = (DOPBASE)theEObject;
				T result = caseDOPBASE(dopbase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DTC: {
				DTC dtc = (DTC)theEObject;
				T result = caseDTC(dtc);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DTCDOP: {
				DTCDOP dtcdop = (DTCDOP)theEObject;
				T result = caseDTCDOP(dtcdop);
				if (result == null) result = caseDOPBASE(dtcdop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DTCDOPS: {
				DTCDOPS dtcdops = (DTCDOPS)theEObject;
				T result = caseDTCDOPS(dtcdops);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DTCS: {
				DTCS dtcs = (DTCS)theEObject;
				T result = caseDTCS(dtcs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DTCVALUE: {
				DTCVALUE dtcvalue = (DTCVALUE)theEObject;
				T result = caseDTCVALUE(dtcvalue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DTCVALUES: {
				DTCVALUES dtcvalues = (DTCVALUES)theEObject;
				T result = caseDTCVALUES(dtcvalues);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DYNAMIC: {
				DYNAMIC dynamic = (DYNAMIC)theEObject;
				T result = caseDYNAMIC(dynamic);
				if (result == null) result = casePOSITIONABLEPARAM(dynamic);
				if (result == null) result = casePARAM(dynamic);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELD: {
				DYNAMICENDMARKERFIELD dynamicendmarkerfield = (DYNAMICENDMARKERFIELD)theEObject;
				T result = caseDYNAMICENDMARKERFIELD(dynamicendmarkerfield);
				if (result == null) result = caseFIELD(dynamicendmarkerfield);
				if (result == null) result = caseCOMPLEXDOP(dynamicendmarkerfield);
				if (result == null) result = caseDOPBASE(dynamicendmarkerfield);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELDS: {
				DYNAMICENDMARKERFIELDS dynamicendmarkerfields = (DYNAMICENDMARKERFIELDS)theEObject;
				T result = caseDYNAMICENDMARKERFIELDS(dynamicendmarkerfields);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD: {
				DYNAMICLENGTHFIELD dynamiclengthfield = (DYNAMICLENGTHFIELD)theEObject;
				T result = caseDYNAMICLENGTHFIELD(dynamiclengthfield);
				if (result == null) result = caseFIELD(dynamiclengthfield);
				if (result == null) result = caseCOMPLEXDOP(dynamiclengthfield);
				if (result == null) result = caseDOPBASE(dynamiclengthfield);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DYNAMICLENGTHFIELDS: {
				DYNAMICLENGTHFIELDS dynamiclengthfields = (DYNAMICLENGTHFIELDS)theEObject;
				T result = caseDYNAMICLENGTHFIELDS(dynamiclengthfields);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DYNDEFINEDSPEC: {
				DYNDEFINEDSPEC dyndefinedspec = (DYNDEFINEDSPEC)theEObject;
				T result = caseDYNDEFINEDSPEC(dyndefinedspec);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DYNENDDOPREF: {
				DYNENDDOPREF dynenddopref = (DYNENDDOPREF)theEObject;
				T result = caseDYNENDDOPREF(dynenddopref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DYNIDDEFMODEINFO: {
				DYNIDDEFMODEINFO dyniddefmodeinfo = (DYNIDDEFMODEINFO)theEObject;
				T result = caseDYNIDDEFMODEINFO(dyniddefmodeinfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.DYNIDDEFMODEINFOS: {
				DYNIDDEFMODEINFOS dyniddefmodeinfos = (DYNIDDEFMODEINFOS)theEObject;
				T result = caseDYNIDDEFMODEINFOS(dyniddefmodeinfos);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUGROUP: {
				ECUGROUP ecugroup = (ECUGROUP)theEObject;
				T result = caseECUGROUP(ecugroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUGROUPS: {
				ECUGROUPS ecugroups = (ECUGROUPS)theEObject;
				T result = caseECUGROUPS(ecugroups);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUMEM: {
				ECUMEM ecumem = (ECUMEM)theEObject;
				T result = caseECUMEM(ecumem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUMEMCONNECTOR: {
				ECUMEMCONNECTOR ecumemconnector = (ECUMEMCONNECTOR)theEObject;
				T result = caseECUMEMCONNECTOR(ecumemconnector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUMEMCONNECTORS: {
				ECUMEMCONNECTORS ecumemconnectors = (ECUMEMCONNECTORS)theEObject;
				T result = caseECUMEMCONNECTORS(ecumemconnectors);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUMEMS: {
				ECUMEMS ecumems = (ECUMEMS)theEObject;
				T result = caseECUMEMS(ecumems);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUPROXY: {
				ECUPROXY ecuproxy = (ECUPROXY)theEObject;
				T result = caseECUPROXY(ecuproxy);
				if (result == null) result = caseINFOCOMPONENT(ecuproxy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUPROXYREFS: {
				ECUPROXYREFS ecuproxyrefs = (ECUPROXYREFS)theEObject;
				T result = caseECUPROXYREFS(ecuproxyrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUSHAREDDATA: {
				ECUSHAREDDATA ecushareddata = (ECUSHAREDDATA)theEObject;
				T result = caseECUSHAREDDATA(ecushareddata);
				if (result == null) result = caseDIAGLAYER(ecushareddata);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUSHAREDDATAREF: {
				ECUSHAREDDATAREF ecushareddataref = (ECUSHAREDDATAREF)theEObject;
				T result = caseECUSHAREDDATAREF(ecushareddataref);
				if (result == null) result = casePARENTREF(ecushareddataref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUSHAREDDATAS: {
				ECUSHAREDDATAS ecushareddatas = (ECUSHAREDDATAS)theEObject;
				T result = caseECUSHAREDDATAS(ecushareddatas);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUVARIANT: {
				ECUVARIANT ecuvariant = (ECUVARIANT)theEObject;
				T result = caseECUVARIANT(ecuvariant);
				if (result == null) result = caseHIERARCHYELEMENT(ecuvariant);
				if (result == null) result = caseDIAGLAYER(ecuvariant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUVARIANTPATTERN: {
				ECUVARIANTPATTERN ecuvariantpattern = (ECUVARIANTPATTERN)theEObject;
				T result = caseECUVARIANTPATTERN(ecuvariantpattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUVARIANTPATTERNS: {
				ECUVARIANTPATTERNS ecuvariantpatterns = (ECUVARIANTPATTERNS)theEObject;
				T result = caseECUVARIANTPATTERNS(ecuvariantpatterns);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ECUVARIANTS: {
				ECUVARIANTS ecuvariants = (ECUVARIANTS)theEObject;
				T result = caseECUVARIANTS(ecuvariants);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ENCRYPTCOMPRESSMETHOD: {
				ENCRYPTCOMPRESSMETHOD encryptcompressmethod = (ENCRYPTCOMPRESSMETHOD)theEObject;
				T result = caseENCRYPTCOMPRESSMETHOD(encryptcompressmethod);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ENDOFPDUFIELD: {
				ENDOFPDUFIELD endofpdufield = (ENDOFPDUFIELD)theEObject;
				T result = caseENDOFPDUFIELD(endofpdufield);
				if (result == null) result = caseFIELD(endofpdufield);
				if (result == null) result = caseCOMPLEXDOP(endofpdufield);
				if (result == null) result = caseDOPBASE(endofpdufield);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ENDOFPDUFIELDS: {
				ENDOFPDUFIELDS endofpdufields = (ENDOFPDUFIELDS)theEObject;
				T result = caseENDOFPDUFIELDS(endofpdufields);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ENVDATA: {
				ENVDATA envdata = (ENVDATA)theEObject;
				T result = caseENVDATA(envdata);
				if (result == null) result = caseBASICSTRUCTURE(envdata);
				if (result == null) result = caseCOMPLEXDOP(envdata);
				if (result == null) result = caseDOPBASE(envdata);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ENVDATADESC: {
				ENVDATADESC envdatadesc = (ENVDATADESC)theEObject;
				T result = caseENVDATADESC(envdatadesc);
				if (result == null) result = caseCOMPLEXDOP(envdatadesc);
				if (result == null) result = caseDOPBASE(envdatadesc);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ENVDATADESCS: {
				ENVDATADESCS envdatadescs = (ENVDATADESCS)theEObject;
				T result = caseENVDATADESCS(envdatadescs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ENVDATAS: {
				ENVDATAS envdatas = (ENVDATAS)theEObject;
				T result = caseENVDATAS(envdatas);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.EXPECTEDIDENT: {
				EXPECTEDIDENT expectedident = (EXPECTEDIDENT)theEObject;
				T result = caseEXPECTEDIDENT(expectedident);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.EXPECTEDIDENTS: {
				EXPECTEDIDENTS expectedidents = (EXPECTEDIDENTS)theEObject;
				T result = caseEXPECTEDIDENTS(expectedidents);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.EXTERNALACCESSMETHOD: {
				EXTERNALACCESSMETHOD externalaccessmethod = (EXTERNALACCESSMETHOD)theEObject;
				T result = caseEXTERNALACCESSMETHOD(externalaccessmethod);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.EXTERNFLASHDATA: {
				EXTERNFLASHDATA externflashdata = (EXTERNFLASHDATA)theEObject;
				T result = caseEXTERNFLASHDATA(externflashdata);
				if (result == null) result = caseFLASHDATA(externflashdata);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FIELD: {
				FIELD field = (FIELD)theEObject;
				T result = caseFIELD(field);
				if (result == null) result = caseCOMPLEXDOP(field);
				if (result == null) result = caseDOPBASE(field);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FILE: {
				FILE file = (FILE)theEObject;
				T result = caseFILE(file);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FILES_TYPE: {
				FILESType filesType = (FILESType)theEObject;
				T result = caseFILESType(filesType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FILTER: {
				FILTER filter = (FILTER)theEObject;
				T result = caseFILTER(filter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FILTERS: {
				FILTERS filters = (FILTERS)theEObject;
				T result = caseFILTERS(filters);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FLASH: {
				FLASH flash = (FLASH)theEObject;
				T result = caseFLASH(flash);
				if (result == null) result = caseODXCATEGORY(flash);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FLASHCLASS: {
				FLASHCLASS flashclass = (FLASHCLASS)theEObject;
				T result = caseFLASHCLASS(flashclass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FLASHCLASSREFS: {
				FLASHCLASSREFS flashclassrefs = (FLASHCLASSREFS)theEObject;
				T result = caseFLASHCLASSREFS(flashclassrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FLASHCLASSS: {
				FLASHCLASSS flashclasss = (FLASHCLASSS)theEObject;
				T result = caseFLASHCLASSS(flashclasss);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FLASHDATA: {
				FLASHDATA flashdata = (FLASHDATA)theEObject;
				T result = caseFLASHDATA(flashdata);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FLASHDATAS: {
				FLASHDATAS flashdatas = (FLASHDATAS)theEObject;
				T result = caseFLASHDATAS(flashdatas);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FLOW: {
				Flow flow = (Flow)theEObject;
				T result = caseFlow(flow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FUNCTCLASS: {
				FUNCTCLASS functclass = (FUNCTCLASS)theEObject;
				T result = caseFUNCTCLASS(functclass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FUNCTCLASSREFS: {
				FUNCTCLASSREFS functclassrefs = (FUNCTCLASSREFS)theEObject;
				T result = caseFUNCTCLASSREFS(functclassrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FUNCTCLASSS: {
				FUNCTCLASSS functclasss = (FUNCTCLASSS)theEObject;
				T result = caseFUNCTCLASSS(functclasss);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FUNCTIONALGROUP: {
				FUNCTIONALGROUP functionalgroup = (FUNCTIONALGROUP)theEObject;
				T result = caseFUNCTIONALGROUP(functionalgroup);
				if (result == null) result = caseHIERARCHYELEMENT(functionalgroup);
				if (result == null) result = caseDIAGLAYER(functionalgroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FUNCTIONALGROUPREF: {
				FUNCTIONALGROUPREF functionalgroupref = (FUNCTIONALGROUPREF)theEObject;
				T result = caseFUNCTIONALGROUPREF(functionalgroupref);
				if (result == null) result = casePARENTREF(functionalgroupref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FUNCTIONALGROUPS: {
				FUNCTIONALGROUPS functionalgroups = (FUNCTIONALGROUPS)theEObject;
				T result = caseFUNCTIONALGROUPS(functionalgroups);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FWCHECKSUM: {
				FWCHECKSUM fwchecksum = (FWCHECKSUM)theEObject;
				T result = caseFWCHECKSUM(fwchecksum);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.FWSIGNATURE: {
				FWSIGNATURE fwsignature = (FWSIGNATURE)theEObject;
				T result = caseFWSIGNATURE(fwsignature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.GATEWAYLOGICALLINK: {
				GATEWAYLOGICALLINK gatewaylogicallink = (GATEWAYLOGICALLINK)theEObject;
				T result = caseGATEWAYLOGICALLINK(gatewaylogicallink);
				if (result == null) result = caseLOGICALLINK(gatewaylogicallink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.GATEWAYLOGICALLINKREFS: {
				GATEWAYLOGICALLINKREFS gatewaylogicallinkrefs = (GATEWAYLOGICALLINKREFS)theEObject;
				T result = caseGATEWAYLOGICALLINKREFS(gatewaylogicallinkrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.GLOBALNEGRESPONSE: {
				GLOBALNEGRESPONSE globalnegresponse = (GLOBALNEGRESPONSE)theEObject;
				T result = caseGLOBALNEGRESPONSE(globalnegresponse);
				if (result == null) result = caseRESPONSE(globalnegresponse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.GLOBALNEGRESPONSES: {
				GLOBALNEGRESPONSES globalnegresponses = (GLOBALNEGRESPONSES)theEObject;
				T result = caseGLOBALNEGRESPONSES(globalnegresponses);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.HIERARCHYELEMENT: {
				HIERARCHYELEMENT hierarchyelement = (HIERARCHYELEMENT)theEObject;
				T result = caseHIERARCHYELEMENT(hierarchyelement);
				if (result == null) result = caseDIAGLAYER(hierarchyelement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.IDENTDESC: {
				IDENTDESC identdesc = (IDENTDESC)theEObject;
				T result = caseIDENTDESC(identdesc);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.IDENTDESCS: {
				IDENTDESCS identdescs = (IDENTDESCS)theEObject;
				T result = caseIDENTDESCS(identdescs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.IDENTVALUE: {
				IDENTVALUE identvalue = (IDENTVALUE)theEObject;
				T result = caseIDENTVALUE(identvalue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.IDENTVALUES: {
				IDENTVALUES identvalues = (IDENTVALUES)theEObject;
				T result = caseIDENTVALUES(identvalues);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.IMPORTREFS: {
				IMPORTREFS importrefs = (IMPORTREFS)theEObject;
				T result = caseIMPORTREFS(importrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.INFOCOMPONENT: {
				INFOCOMPONENT infocomponent = (INFOCOMPONENT)theEObject;
				T result = caseINFOCOMPONENT(infocomponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.INFOCOMPONENTREFS: {
				INFOCOMPONENTREFS infocomponentrefs = (INFOCOMPONENTREFS)theEObject;
				T result = caseINFOCOMPONENTREFS(infocomponentrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.INFOCOMPONENTS: {
				INFOCOMPONENTS infocomponents = (INFOCOMPONENTS)theEObject;
				T result = caseINFOCOMPONENTS(infocomponents);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.INLINE: {
				Inline inline = (Inline)theEObject;
				T result = caseInline(inline);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.INPUTPARAM: {
				INPUTPARAM inputparam = (INPUTPARAM)theEObject;
				T result = caseINPUTPARAM(inputparam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.INPUTPARAMS: {
				INPUTPARAMS inputparams = (INPUTPARAMS)theEObject;
				T result = caseINPUTPARAMS(inputparams);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.INTERNALCONSTR: {
				INTERNALCONSTR internalconstr = (INTERNALCONSTR)theEObject;
				T result = caseINTERNALCONSTR(internalconstr);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.INTERNFLASHDATA: {
				INTERNFLASHDATA internflashdata = (INTERNFLASHDATA)theEObject;
				T result = caseINTERNFLASHDATA(internflashdata);
				if (result == null) result = caseFLASHDATA(internflashdata);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ITYPE: {
				IType iType = (IType)theEObject;
				T result = caseIType(iType);
				if (result == null) result = caseInline(iType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LAYERREFS: {
				LAYERREFS layerrefs = (LAYERREFS)theEObject;
				T result = caseLAYERREFS(layerrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LEADINGLENGTHINFOTYPE: {
				LEADINGLENGTHINFOTYPE leadinglengthinfotype = (LEADINGLENGTHINFOTYPE)theEObject;
				T result = caseLEADINGLENGTHINFOTYPE(leadinglengthinfotype);
				if (result == null) result = caseDIAGCODEDTYPE(leadinglengthinfotype);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LENGTHDESCRIPTOR: {
				LENGTHDESCRIPTOR lengthdescriptor = (LENGTHDESCRIPTOR)theEObject;
				T result = caseLENGTHDESCRIPTOR(lengthdescriptor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LENGTHKEY: {
				LENGTHKEY lengthkey = (LENGTHKEY)theEObject;
				T result = caseLENGTHKEY(lengthkey);
				if (result == null) result = casePOSITIONABLEPARAM(lengthkey);
				if (result == null) result = casePARAM(lengthkey);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LIMIT: {
				LIMIT limit = (LIMIT)theEObject;
				T result = caseLIMIT(limit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LINKCOMPARAMREF: {
				LINKCOMPARAMREF linkcomparamref = (LINKCOMPARAMREF)theEObject;
				T result = caseLINKCOMPARAMREF(linkcomparamref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LINKCOMPARAMREFS: {
				LINKCOMPARAMREFS linkcomparamrefs = (LINKCOMPARAMREFS)theEObject;
				T result = caseLINKCOMPARAMREFS(linkcomparamrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LI_TYPE: {
				LiType liType = (LiType)theEObject;
				T result = caseLiType(liType);
				if (result == null) result = caseFlow(liType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LOGICALLINK: {
				LOGICALLINK logicallink = (LOGICALLINK)theEObject;
				T result = caseLOGICALLINK(logicallink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LOGICALLINKREFS: {
				LOGICALLINKREFS logicallinkrefs = (LOGICALLINKREFS)theEObject;
				T result = caseLOGICALLINKREFS(logicallinkrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.LOGICALLINKS: {
				LOGICALLINKS logicallinks = (LOGICALLINKS)theEObject;
				T result = caseLOGICALLINKS(logicallinks);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MATCHINGCOMPONENT: {
				MATCHINGCOMPONENT matchingcomponent = (MATCHINGCOMPONENT)theEObject;
				T result = caseMATCHINGCOMPONENT(matchingcomponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MATCHINGCOMPONENTS: {
				MATCHINGCOMPONENTS matchingcomponents = (MATCHINGCOMPONENTS)theEObject;
				T result = caseMATCHINGCOMPONENTS(matchingcomponents);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MATCHINGPARAMETER: {
				MATCHINGPARAMETER matchingparameter = (MATCHINGPARAMETER)theEObject;
				T result = caseMATCHINGPARAMETER(matchingparameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MATCHINGPARAMETERS: {
				MATCHINGPARAMETERS matchingparameters = (MATCHINGPARAMETERS)theEObject;
				T result = caseMATCHINGPARAMETERS(matchingparameters);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM: {
				MATCHINGREQUESTPARAM matchingrequestparam = (MATCHINGREQUESTPARAM)theEObject;
				T result = caseMATCHINGREQUESTPARAM(matchingrequestparam);
				if (result == null) result = casePOSITIONABLEPARAM(matchingrequestparam);
				if (result == null) result = casePARAM(matchingrequestparam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MEM: {
				MEM mem = (MEM)theEObject;
				T result = caseMEM(mem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MEMBERLOGICALLINK: {
				MEMBERLOGICALLINK memberlogicallink = (MEMBERLOGICALLINK)theEObject;
				T result = caseMEMBERLOGICALLINK(memberlogicallink);
				if (result == null) result = caseLOGICALLINK(memberlogicallink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MINMAXLENGTHTYPE: {
				MINMAXLENGTHTYPE minmaxlengthtype = (MINMAXLENGTHTYPE)theEObject;
				T result = caseMINMAXLENGTHTYPE(minmaxlengthtype);
				if (result == null) result = caseDIAGCODEDTYPE(minmaxlengthtype);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MODELYEAR: {
				MODELYEAR modelyear = (MODELYEAR)theEObject;
				T result = caseMODELYEAR(modelyear);
				if (result == null) result = caseINFOCOMPONENT(modelyear);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MODIFICATION: {
				MODIFICATION modification = (MODIFICATION)theEObject;
				T result = caseMODIFICATION(modification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MODIFICATION1: {
				MODIFICATION1 modification1 = (MODIFICATION1)theEObject;
				T result = caseMODIFICATION1(modification1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MODIFICATIONS: {
				MODIFICATIONS modifications = (MODIFICATIONS)theEObject;
				T result = caseMODIFICATIONS(modifications);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MODIFICATIONS1: {
				MODIFICATIONS1 modifications1 = (MODIFICATIONS1)theEObject;
				T result = caseMODIFICATIONS1(modifications1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MULTIPLEECUJOB: {
				MULTIPLEECUJOB multipleecujob = (MULTIPLEECUJOB)theEObject;
				T result = caseMULTIPLEECUJOB(multipleecujob);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MULTIPLEECUJOBS: {
				MULTIPLEECUJOBS multipleecujobs = (MULTIPLEECUJOBS)theEObject;
				T result = caseMULTIPLEECUJOBS(multipleecujobs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC: {
				MULTIPLEECUJOBSPEC multipleecujobspec = (MULTIPLEECUJOBSPEC)theEObject;
				T result = caseMULTIPLEECUJOBSPEC(multipleecujobspec);
				if (result == null) result = caseODXCATEGORY(multipleecujobspec);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MUX: {
				MUX mux = (MUX)theEObject;
				T result = caseMUX(mux);
				if (result == null) result = caseCOMPLEXDOP(mux);
				if (result == null) result = caseDOPBASE(mux);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.MUXS: {
				MUXS muxs = (MUXS)theEObject;
				T result = caseMUXS(muxs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NEGOFFSET: {
				NEGOFFSET negoffset = (NEGOFFSET)theEObject;
				T result = caseNEGOFFSET(negoffset);
				if (result == null) result = caseTARGETADDROFFSET(negoffset);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NEGOUTPUTPARAM: {
				NEGOUTPUTPARAM negoutputparam = (NEGOUTPUTPARAM)theEObject;
				T result = caseNEGOUTPUTPARAM(negoutputparam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NEGOUTPUTPARAMS: {
				NEGOUTPUTPARAMS negoutputparams = (NEGOUTPUTPARAMS)theEObject;
				T result = caseNEGOUTPUTPARAMS(negoutputparams);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NEGRESPONSE: {
				NEGRESPONSE negresponse = (NEGRESPONSE)theEObject;
				T result = caseNEGRESPONSE(negresponse);
				if (result == null) result = caseRESPONSE(negresponse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NEGRESPONSEREFS: {
				NEGRESPONSEREFS negresponserefs = (NEGRESPONSEREFS)theEObject;
				T result = caseNEGRESPONSEREFS(negresponserefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NEGRESPONSES: {
				NEGRESPONSES negresponses = (NEGRESPONSES)theEObject;
				T result = caseNEGRESPONSES(negresponses);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NOTINHERITEDDIAGCOMM: {
				NOTINHERITEDDIAGCOMM notinheriteddiagcomm = (NOTINHERITEDDIAGCOMM)theEObject;
				T result = caseNOTINHERITEDDIAGCOMM(notinheriteddiagcomm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NOTINHERITEDDIAGCOMMS: {
				NOTINHERITEDDIAGCOMMS notinheriteddiagcomms = (NOTINHERITEDDIAGCOMMS)theEObject;
				T result = caseNOTINHERITEDDIAGCOMMS(notinheriteddiagcomms);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NOTINHERITEDVARIABLE: {
				NOTINHERITEDVARIABLE notinheritedvariable = (NOTINHERITEDVARIABLE)theEObject;
				T result = caseNOTINHERITEDVARIABLE(notinheritedvariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.NOTINHERITEDVARIABLES: {
				NOTINHERITEDVARIABLES notinheritedvariables = (NOTINHERITEDVARIABLES)theEObject;
				T result = caseNOTINHERITEDVARIABLES(notinheritedvariables);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ODX: {
				ODX odx = (ODX)theEObject;
				T result = caseODX(odx);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ODXCATEGORY: {
				ODXCATEGORY odxcategory = (ODXCATEGORY)theEObject;
				T result = caseODXCATEGORY(odxcategory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ODXLINK: {
				ODXLINK odxlink = (ODXLINK)theEObject;
				T result = caseODXLINK(odxlink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ODXLINK1: {
				ODXLINK1 odxlink1 = (ODXLINK1)theEObject;
				T result = caseODXLINK1(odxlink1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.OEM: {
				OEM oem = (OEM)theEObject;
				T result = caseOEM(oem);
				if (result == null) result = caseINFOCOMPONENT(oem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.OL_TYPE: {
				OlType olType = (OlType)theEObject;
				T result = caseOlType(olType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.OUTPUTPARAM: {
				OUTPUTPARAM outputparam = (OUTPUTPARAM)theEObject;
				T result = caseOUTPUTPARAM(outputparam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.OUTPUTPARAMS: {
				OUTPUTPARAMS outputparams = (OUTPUTPARAMS)theEObject;
				T result = caseOUTPUTPARAMS(outputparams);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.OWNIDENT: {
				OWNIDENT ownident = (OWNIDENT)theEObject;
				T result = caseOWNIDENT(ownident);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.OWNIDENTS: {
				OWNIDENTS ownidents = (OWNIDENTS)theEObject;
				T result = caseOWNIDENTS(ownidents);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.P: {
				P p = (P)theEObject;
				T result = caseP(p);
				if (result == null) result = caseFlow(p);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PARAM: {
				PARAM param = (PARAM)theEObject;
				T result = casePARAM(param);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PARAMLENGTHINFOTYPE: {
				PARAMLENGTHINFOTYPE paramlengthinfotype = (PARAMLENGTHINFOTYPE)theEObject;
				T result = casePARAMLENGTHINFOTYPE(paramlengthinfotype);
				if (result == null) result = caseDIAGCODEDTYPE(paramlengthinfotype);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PARAMS: {
				PARAMS params = (PARAMS)theEObject;
				T result = casePARAMS(params);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PARENTREF: {
				PARENTREF parentref = (PARENTREF)theEObject;
				T result = casePARENTREF(parentref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PARENTREFS: {
				PARENTREFS parentrefs = (PARENTREFS)theEObject;
				T result = casePARENTREFS(parentrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PHYSCONST: {
				PHYSCONST physconst = (PHYSCONST)theEObject;
				T result = casePHYSCONST(physconst);
				if (result == null) result = casePOSITIONABLEPARAM(physconst);
				if (result == null) result = casePARAM(physconst);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PHYSICALDIMENSION: {
				PHYSICALDIMENSION physicaldimension = (PHYSICALDIMENSION)theEObject;
				T result = casePHYSICALDIMENSION(physicaldimension);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PHYSICALDIMENSIONS: {
				PHYSICALDIMENSIONS physicaldimensions = (PHYSICALDIMENSIONS)theEObject;
				T result = casePHYSICALDIMENSIONS(physicaldimensions);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PHYSICALTYPE: {
				PHYSICALTYPE physicaltype = (PHYSICALTYPE)theEObject;
				T result = casePHYSICALTYPE(physicaltype);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PHYSICALVEHICLELINK: {
				PHYSICALVEHICLELINK physicalvehiclelink = (PHYSICALVEHICLELINK)theEObject;
				T result = casePHYSICALVEHICLELINK(physicalvehiclelink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PHYSICALVEHICLELINKS: {
				PHYSICALVEHICLELINKS physicalvehiclelinks = (PHYSICALVEHICLELINKS)theEObject;
				T result = casePHYSICALVEHICLELINKS(physicalvehiclelinks);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PHYSMEM: {
				PHYSMEM physmem = (PHYSMEM)theEObject;
				T result = casePHYSMEM(physmem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PHYSSEGMENT: {
				PHYSSEGMENT physsegment = (PHYSSEGMENT)theEObject;
				T result = casePHYSSEGMENT(physsegment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PHYSSEGMENTS: {
				PHYSSEGMENTS physsegments = (PHYSSEGMENTS)theEObject;
				T result = casePHYSSEGMENTS(physsegments);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.POSITIONABLEPARAM: {
				POSITIONABLEPARAM positionableparam = (POSITIONABLEPARAM)theEObject;
				T result = casePOSITIONABLEPARAM(positionableparam);
				if (result == null) result = casePARAM(positionableparam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.POSOFFSET: {
				POSOFFSET posoffset = (POSOFFSET)theEObject;
				T result = casePOSOFFSET(posoffset);
				if (result == null) result = caseTARGETADDROFFSET(posoffset);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.POSRESPONSE: {
				POSRESPONSE posresponse = (POSRESPONSE)theEObject;
				T result = casePOSRESPONSE(posresponse);
				if (result == null) result = caseRESPONSE(posresponse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.POSRESPONSEREFS: {
				POSRESPONSEREFS posresponserefs = (POSRESPONSEREFS)theEObject;
				T result = casePOSRESPONSEREFS(posresponserefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.POSRESPONSES: {
				POSRESPONSES posresponses = (POSRESPONSES)theEObject;
				T result = casePOSRESPONSES(posresponses);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROGCODE: {
				PROGCODE progcode = (PROGCODE)theEObject;
				T result = casePROGCODE(progcode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROGCODES: {
				PROGCODES progcodes = (PROGCODES)theEObject;
				T result = casePROGCODES(progcodes);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROJECTIDENT: {
				PROJECTIDENT projectident = (PROJECTIDENT)theEObject;
				T result = casePROJECTIDENT(projectident);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROJECTIDENTS: {
				PROJECTIDENTS projectidents = (PROJECTIDENTS)theEObject;
				T result = casePROJECTIDENTS(projectidents);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROJECTINFO: {
				PROJECTINFO projectinfo = (PROJECTINFO)theEObject;
				T result = casePROJECTINFO(projectinfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROJECTINFOS: {
				PROJECTINFOS projectinfos = (PROJECTINFOS)theEObject;
				T result = casePROJECTINFOS(projectinfos);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROTOCOL: {
				PROTOCOL protocol = (PROTOCOL)theEObject;
				T result = casePROTOCOL(protocol);
				if (result == null) result = caseHIERARCHYELEMENT(protocol);
				if (result == null) result = caseDIAGLAYER(protocol);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROTOCOLREF: {
				PROTOCOLREF protocolref = (PROTOCOLREF)theEObject;
				T result = casePROTOCOLREF(protocolref);
				if (result == null) result = casePARENTREF(protocolref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROTOCOLS: {
				PROTOCOLS protocols = (PROTOCOLS)theEObject;
				T result = casePROTOCOLS(protocols);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.PROTOCOLSNREFS: {
				PROTOCOLSNREFS protocolsnrefs = (PROTOCOLSNREFS)theEObject;
				T result = casePROTOCOLSNREFS(protocolsnrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.RELATEDDIAGCOMMREF: {
				RELATEDDIAGCOMMREF relateddiagcommref = (RELATEDDIAGCOMMREF)theEObject;
				T result = caseRELATEDDIAGCOMMREF(relateddiagcommref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.RELATEDDIAGCOMMREFS: {
				RELATEDDIAGCOMMREFS relateddiagcommrefs = (RELATEDDIAGCOMMREFS)theEObject;
				T result = caseRELATEDDIAGCOMMREFS(relateddiagcommrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.RELATEDDOC: {
				RELATEDDOC relateddoc = (RELATEDDOC)theEObject;
				T result = caseRELATEDDOC(relateddoc);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.RELATEDDOC1: {
				RELATEDDOC1 relateddoc1 = (RELATEDDOC1)theEObject;
				T result = caseRELATEDDOC1(relateddoc1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.RELATEDDOCS: {
				RELATEDDOCS relateddocs = (RELATEDDOCS)theEObject;
				T result = caseRELATEDDOCS(relateddocs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.RELATEDDOCS1: {
				RELATEDDOCS1 relateddocs1 = (RELATEDDOCS1)theEObject;
				T result = caseRELATEDDOCS1(relateddocs1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.REQUEST: {
				REQUEST request = (REQUEST)theEObject;
				T result = caseREQUEST(request);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.REQUESTS: {
				REQUESTS requests = (REQUESTS)theEObject;
				T result = caseREQUESTS(requests);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.RESERVED: {
				RESERVED reserved = (RESERVED)theEObject;
				T result = caseRESERVED(reserved);
				if (result == null) result = casePOSITIONABLEPARAM(reserved);
				if (result == null) result = casePARAM(reserved);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.RESPONSE: {
				RESPONSE response = (RESPONSE)theEObject;
				T result = caseRESPONSE(response);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ROLES: {
				ROLES roles = (ROLES)theEObject;
				T result = caseROLES(roles);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.ROLES1: {
				ROLES1 roles1 = (ROLES1)theEObject;
				T result = caseROLES1(roles1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SCALECONSTR: {
				SCALECONSTR scaleconstr = (SCALECONSTR)theEObject;
				T result = caseSCALECONSTR(scaleconstr);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SCALECONSTRS: {
				SCALECONSTRS scaleconstrs = (SCALECONSTRS)theEObject;
				T result = caseSCALECONSTRS(scaleconstrs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SD: {
				SD sd = (SD)theEObject;
				T result = caseSD(sd);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SD1: {
				SD1 sd1 = (SD1)theEObject;
				T result = caseSD1(sd1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SDG: {
				SDG sdg = (SDG)theEObject;
				T result = caseSDG(sdg);
				if (result == null) result = caseSPECIALDATA(sdg);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SDG1: {
				SDG1 sdg1 = (SDG1)theEObject;
				T result = caseSDG1(sdg1);
				if (result == null) result = caseSPECIALDATA1(sdg1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SDGCAPTION: {
				SDGCAPTION sdgcaption = (SDGCAPTION)theEObject;
				T result = caseSDGCAPTION(sdgcaption);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SDGCAPTION1: {
				SDGCAPTION1 sdgcaption1 = (SDGCAPTION1)theEObject;
				T result = caseSDGCAPTION1(sdgcaption1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SDGS: {
				SDGS sdgs = (SDGS)theEObject;
				T result = caseSDGS(sdgs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SDGS1: {
				SDGS1 sdgs1 = (SDGS1)theEObject;
				T result = caseSDGS1(sdgs1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SECURITY: {
				SECURITY security = (SECURITY)theEObject;
				T result = caseSECURITY(security);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SECURITYMETHOD: {
				SECURITYMETHOD securitymethod = (SECURITYMETHOD)theEObject;
				T result = caseSECURITYMETHOD(securitymethod);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SECURITYS: {
				SECURITYS securitys = (SECURITYS)theEObject;
				T result = caseSECURITYS(securitys);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SEGMENT: {
				SEGMENT segment = (SEGMENT)theEObject;
				T result = caseSEGMENT(segment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SEGMENTS: {
				SEGMENTS segments = (SEGMENTS)theEObject;
				T result = caseSEGMENTS(segments);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SELECTIONTABLEREFS: {
				SELECTIONTABLEREFS selectiontablerefs = (SELECTIONTABLEREFS)theEObject;
				T result = caseSELECTIONTABLEREFS(selectiontablerefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SESSION: {
				SESSION session = (SESSION)theEObject;
				T result = caseSESSION(session);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SESSIONDESC: {
				SESSIONDESC sessiondesc = (SESSIONDESC)theEObject;
				T result = caseSESSIONDESC(sessiondesc);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SESSIONDESCS: {
				SESSIONDESCS sessiondescs = (SESSIONDESCS)theEObject;
				T result = caseSESSIONDESCS(sessiondescs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SESSIONS: {
				SESSIONS sessions = (SESSIONS)theEObject;
				T result = caseSESSIONS(sessions);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SINGLEECUJOB: {
				SINGLEECUJOB singleecujob = (SINGLEECUJOB)theEObject;
				T result = caseSINGLEECUJOB(singleecujob);
				if (result == null) result = caseDIAGCOMM(singleecujob);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SIZEDEFFILTER: {
				SIZEDEFFILTER sizedeffilter = (SIZEDEFFILTER)theEObject;
				T result = caseSIZEDEFFILTER(sizedeffilter);
				if (result == null) result = caseFILTER(sizedeffilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SIZEDEFPHYSSEGMENT: {
				SIZEDEFPHYSSEGMENT sizedefphyssegment = (SIZEDEFPHYSSEGMENT)theEObject;
				T result = caseSIZEDEFPHYSSEGMENT(sizedefphyssegment);
				if (result == null) result = casePHYSSEGMENT(sizedefphyssegment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SNREF: {
				SNREF snref = (SNREF)theEObject;
				T result = caseSNREF(snref);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SOURCEENDADDRESS: {
				SOURCEENDADDRESS sourceendaddress = (SOURCEENDADDRESS)theEObject;
				T result = caseSOURCEENDADDRESS(sourceendaddress);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SPECIALDATA: {
				SPECIALDATA specialdata = (SPECIALDATA)theEObject;
				T result = caseSPECIALDATA(specialdata);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SPECIALDATA1: {
				SPECIALDATA1 specialdata1 = (SPECIALDATA1)theEObject;
				T result = caseSPECIALDATA1(specialdata1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.STANDARDLENGTHTYPE: {
				STANDARDLENGTHTYPE standardlengthtype = (STANDARDLENGTHTYPE)theEObject;
				T result = caseSTANDARDLENGTHTYPE(standardlengthtype);
				if (result == null) result = caseDIAGCODEDTYPE(standardlengthtype);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.STATICFIELD: {
				STATICFIELD staticfield = (STATICFIELD)theEObject;
				T result = caseSTATICFIELD(staticfield);
				if (result == null) result = caseFIELD(staticfield);
				if (result == null) result = caseCOMPLEXDOP(staticfield);
				if (result == null) result = caseDOPBASE(staticfield);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.STATICFIELDS: {
				STATICFIELDS staticfields = (STATICFIELDS)theEObject;
				T result = caseSTATICFIELDS(staticfields);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.STRUCTURE: {
				STRUCTURE structure = (STRUCTURE)theEObject;
				T result = caseSTRUCTURE(structure);
				if (result == null) result = caseBASICSTRUCTURE(structure);
				if (result == null) result = caseCOMPLEXDOP(structure);
				if (result == null) result = caseDOPBASE(structure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.STRUCTURES: {
				STRUCTURES structures = (STRUCTURES)theEObject;
				T result = caseSTRUCTURES(structures);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SUB_TYPE: {
				SubType subType = (SubType)theEObject;
				T result = caseSubType(subType);
				if (result == null) result = caseInline(subType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SUPPORTEDDYNID: {
				SUPPORTEDDYNID supporteddynid = (SUPPORTEDDYNID)theEObject;
				T result = caseSUPPORTEDDYNID(supporteddynid);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SUPPORTEDDYNIDS: {
				SUPPORTEDDYNIDS supporteddynids = (SUPPORTEDDYNIDS)theEObject;
				T result = caseSUPPORTEDDYNIDS(supporteddynids);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SUP_TYPE: {
				SupType supType = (SupType)theEObject;
				T result = caseSupType(supType);
				if (result == null) result = caseInline(supType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SWITCHKEY: {
				SWITCHKEY switchkey = (SWITCHKEY)theEObject;
				T result = caseSWITCHKEY(switchkey);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SWVARIABLE: {
				SWVARIABLE swvariable = (SWVARIABLE)theEObject;
				T result = caseSWVARIABLE(swvariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SWVARIABLES: {
				SWVARIABLES swvariables = (SWVARIABLES)theEObject;
				T result = caseSWVARIABLES(swvariables);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.SYSTEM: {
				SYSTEM system = (SYSTEM)theEObject;
				T result = caseSYSTEM(system);
				if (result == null) result = casePOSITIONABLEPARAM(system);
				if (result == null) result = casePARAM(system);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TABLE: {
				TABLE table = (TABLE)theEObject;
				T result = caseTABLE(table);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TABLEENTRY: {
				TABLEENTRY tableentry = (TABLEENTRY)theEObject;
				T result = caseTABLEENTRY(tableentry);
				if (result == null) result = casePARAM(tableentry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TABLEKEY: {
				TABLEKEY tablekey = (TABLEKEY)theEObject;
				T result = caseTABLEKEY(tablekey);
				if (result == null) result = casePOSITIONABLEPARAM(tablekey);
				if (result == null) result = casePARAM(tablekey);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TABLEROW: {
				TABLEROW tablerow = (TABLEROW)theEObject;
				T result = caseTABLEROW(tablerow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TABLES: {
				TABLES tables = (TABLES)theEObject;
				T result = caseTABLES(tables);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TABLESTRUCT: {
				TABLESTRUCT tablestruct = (TABLESTRUCT)theEObject;
				T result = caseTABLESTRUCT(tablestruct);
				if (result == null) result = casePOSITIONABLEPARAM(tablestruct);
				if (result == null) result = casePARAM(tablestruct);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TARGETADDROFFSET: {
				TARGETADDROFFSET targetaddroffset = (TARGETADDROFFSET)theEObject;
				T result = caseTARGETADDROFFSET(targetaddroffset);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TEAMMEMBER: {
				TEAMMEMBER teammember = (TEAMMEMBER)theEObject;
				T result = caseTEAMMEMBER(teammember);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TEAMMEMBER1: {
				TEAMMEMBER1 teammember1 = (TEAMMEMBER1)theEObject;
				T result = caseTEAMMEMBER1(teammember1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TEAMMEMBERS: {
				TEAMMEMBERS teammembers = (TEAMMEMBERS)theEObject;
				T result = caseTEAMMEMBERS(teammembers);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TEAMMEMBERS1: {
				TEAMMEMBERS1 teammembers1 = (TEAMMEMBERS1)theEObject;
				T result = caseTEAMMEMBERS1(teammembers1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TEXT: {
				TEXT text = (TEXT)theEObject;
				T result = caseTEXT(text);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.TEXT1: {
				TEXT1 text1 = (TEXT1)theEObject;
				T result = caseTEXT1(text1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.UL_TYPE: {
				UlType ulType = (UlType)theEObject;
				T result = caseUlType(ulType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.UNCOMPRESSEDSIZE: {
				UNCOMPRESSEDSIZE uncompressedsize = (UNCOMPRESSEDSIZE)theEObject;
				T result = caseUNCOMPRESSEDSIZE(uncompressedsize);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.UNIONVALUE: {
				UNIONVALUE unionvalue = (UNIONVALUE)theEObject;
				T result = caseUNIONVALUE(unionvalue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.UNIT: {
				UNIT unit = (UNIT)theEObject;
				T result = caseUNIT(unit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.UNITGROUP: {
				UNITGROUP unitgroup = (UNITGROUP)theEObject;
				T result = caseUNITGROUP(unitgroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.UNITGROUPS: {
				UNITGROUPS unitgroups = (UNITGROUPS)theEObject;
				T result = caseUNITGROUPS(unitgroups);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.UNITREFS: {
				UNITREFS unitrefs = (UNITREFS)theEObject;
				T result = caseUNITREFS(unitrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.UNITS: {
				UNITS units = (UNITS)theEObject;
				T result = caseUNITS(units);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.UNITSPEC: {
				UNITSPEC unitspec = (UNITSPEC)theEObject;
				T result = caseUNITSPEC(unitspec);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.V: {
				V v = (V)theEObject;
				T result = caseV(v);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VALIDITYFOR: {
				VALIDITYFOR validityfor = (VALIDITYFOR)theEObject;
				T result = caseVALIDITYFOR(validityfor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VALUE: {
				VALUE value = (VALUE)theEObject;
				T result = caseVALUE(value);
				if (result == null) result = casePOSITIONABLEPARAM(value);
				if (result == null) result = casePARAM(value);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VARIABLEGROUP: {
				VARIABLEGROUP variablegroup = (VARIABLEGROUP)theEObject;
				T result = caseVARIABLEGROUP(variablegroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VARIABLEGROUPS: {
				VARIABLEGROUPS variablegroups = (VARIABLEGROUPS)theEObject;
				T result = caseVARIABLEGROUPS(variablegroups);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLECONNECTOR: {
				VEHICLECONNECTOR vehicleconnector = (VEHICLECONNECTOR)theEObject;
				T result = caseVEHICLECONNECTOR(vehicleconnector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLECONNECTORPIN: {
				VEHICLECONNECTORPIN vehicleconnectorpin = (VEHICLECONNECTORPIN)theEObject;
				T result = caseVEHICLECONNECTORPIN(vehicleconnectorpin);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLECONNECTORPINREFS: {
				VEHICLECONNECTORPINREFS vehicleconnectorpinrefs = (VEHICLECONNECTORPINREFS)theEObject;
				T result = caseVEHICLECONNECTORPINREFS(vehicleconnectorpinrefs);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLECONNECTORPINS: {
				VEHICLECONNECTORPINS vehicleconnectorpins = (VEHICLECONNECTORPINS)theEObject;
				T result = caseVEHICLECONNECTORPINS(vehicleconnectorpins);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLECONNECTORS: {
				VEHICLECONNECTORS vehicleconnectors = (VEHICLECONNECTORS)theEObject;
				T result = caseVEHICLECONNECTORS(vehicleconnectors);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLEINFORMATION: {
				VEHICLEINFORMATION vehicleinformation = (VEHICLEINFORMATION)theEObject;
				T result = caseVEHICLEINFORMATION(vehicleinformation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLEINFORMATIONS: {
				VEHICLEINFORMATIONS vehicleinformations = (VEHICLEINFORMATIONS)theEObject;
				T result = caseVEHICLEINFORMATIONS(vehicleinformations);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLEINFOSPEC: {
				VEHICLEINFOSPEC vehicleinfospec = (VEHICLEINFOSPEC)theEObject;
				T result = caseVEHICLEINFOSPEC(vehicleinfospec);
				if (result == null) result = caseODXCATEGORY(vehicleinfospec);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLEMODEL: {
				VEHICLEMODEL vehiclemodel = (VEHICLEMODEL)theEObject;
				T result = caseVEHICLEMODEL(vehiclemodel);
				if (result == null) result = caseINFOCOMPONENT(vehiclemodel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VEHICLETYPE: {
				VEHICLETYPE vehicletype = (VEHICLETYPE)theEObject;
				T result = caseVEHICLETYPE(vehicletype);
				if (result == null) result = caseINFOCOMPONENT(vehicletype);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.VT: {
				VT vt = (VT)theEObject;
				T result = caseVT(vt);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.XDOC: {
				XDOC xdoc = (XDOC)theEObject;
				T result = caseXDOC(xdoc);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OdxXhtmlPackage.XDOC1: {
				XDOC1 xdoc1 = (XDOC1)theEObject;
				T result = caseXDOC1(xdoc1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ABLOCK</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ABLOCK</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseABLOCK(ABLOCK object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ABLOCKS Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ABLOCKS Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseABLOCKSType(ABLOCKSType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ACCESSLEVEL</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ACCESSLEVEL</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseACCESSLEVEL(ACCESSLEVEL object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ACCESSLEVELS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ACCESSLEVELS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseACCESSLEVELS(ACCESSLEVELS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ADDRDEFFILTER</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ADDRDEFFILTER</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseADDRDEFFILTER(ADDRDEFFILTER object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ADDRDEFPHYSSEGMENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ADDRDEFPHYSSEGMENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseADDRDEFPHYSSEGMENT(ADDRDEFPHYSSEGMENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ADMINDATA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ADMINDATA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseADMINDATA(ADMINDATA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ADMINDATA1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ADMINDATA1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseADMINDATA1(ADMINDATA1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ALLVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ALLVALUE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseALLVALUE(ALLVALUE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AUDIENCE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AUDIENCE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAUDIENCE(AUDIENCE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AUTMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AUTMETHOD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAUTMETHOD(AUTMETHOD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AUTMETHODS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AUTMETHODS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAUTMETHODS(AUTMETHODS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BASEVARIANT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BASEVARIANT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBASEVARIANT(BASEVARIANT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BASEVARIANTREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BASEVARIANTREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBASEVARIANTREF(BASEVARIANTREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BASEVARIANTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BASEVARIANTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBASEVARIANTS(BASEVARIANTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BASICSTRUCTURE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BASICSTRUCTURE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBASICSTRUCTURE(BASICSTRUCTURE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBType(BType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CASE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CASE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCASE(CASE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CASES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CASES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCASES(CASES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CATALOG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CATALOG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCATALOG(CATALOG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CHECKSUM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CHECKSUM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCHECKSUM(CHECKSUM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CHECKSUMRESULT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CHECKSUMRESULT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCHECKSUMRESULT(CHECKSUMRESULT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CHECKSUMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CHECKSUMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCHECKSUMS(CHECKSUMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CODEDCONST</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CODEDCONST</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCODEDCONST(CODEDCONST object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMMRELATION</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMMRELATION</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMMRELATION(COMMRELATION object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMMRELATIONS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMMRELATIONS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMMRELATIONS(COMMRELATIONS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYDATA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYDATA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYDATA(COMPANYDATA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYDATA1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYDATA1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYDATA1(COMPANYDATA1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYDATAS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYDATAS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYDATAS(COMPANYDATAS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYDATAS Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYDATAS Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYDATASType(COMPANYDATASType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYDOCINFO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYDOCINFO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYDOCINFO(COMPANYDOCINFO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYDOCINFO1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYDOCINFO1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYDOCINFO1(COMPANYDOCINFO1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYDOCINFOS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYDOCINFOS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYDOCINFOS(COMPANYDOCINFOS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYDOCINFOS1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYDOCINFOS1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYDOCINFOS1(COMPANYDOCINFOS1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYREVISIONINFO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYREVISIONINFO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYREVISIONINFO(COMPANYREVISIONINFO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYREVISIONINFO1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYREVISIONINFO1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYREVISIONINFO1(COMPANYREVISIONINFO1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYREVISIONINFOS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYREVISIONINFOS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYREVISIONINFOS(COMPANYREVISIONINFOS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYREVISIONINFOS1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYREVISIONINFOS1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYREVISIONINFOS1(COMPANYREVISIONINFOS1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYSPECIFICINFO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYSPECIFICINFO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPANYSPECIFICINFO1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPANYSPECIFICINFO1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPANYSPECIFICINFO1(COMPANYSPECIFICINFO1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPARAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPARAM(COMPARAM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPARAMREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPARAMREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPARAMREF(COMPARAMREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPARAMREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPARAMREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPARAMREFS(COMPARAMREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPARAMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPARAMS(COMPARAMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPARAMSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPARAMSPEC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPARAMSPEC(COMPARAMSPEC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPLEXDOP</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPLEXDOP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPLEXDOP(COMPLEXDOP object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUCONST</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUCONST</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUCONST(COMPUCONST object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUDEFAULTVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUDEFAULTVALUE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUDEFAULTVALUE(COMPUDEFAULTVALUE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUDENOMINATOR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUDENOMINATOR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUDENOMINATOR(COMPUDENOMINATOR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUINTERNALTOPHYS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUINTERNALTOPHYS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUINTERNALTOPHYS(COMPUINTERNALTOPHYS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUINVERSEVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUINVERSEVALUE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUINVERSEVALUE(COMPUINVERSEVALUE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUMETHOD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUMETHOD(COMPUMETHOD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUNUMERATOR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUNUMERATOR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUNUMERATOR(COMPUNUMERATOR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUPHYSTOINTERNAL</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUPHYSTOINTERNAL</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUPHYSTOINTERNAL(COMPUPHYSTOINTERNAL object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPURATIONALCOEFFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPURATIONALCOEFFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPURATIONALCOEFFS(COMPURATIONALCOEFFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUSCALE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUSCALE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUSCALE(COMPUSCALE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>COMPUSCALES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>COMPUSCALES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCOMPUSCALES(COMPUSCALES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DATABLOCK</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DATABLOCK</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDATABLOCK(DATABLOCK object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DATABLOCKREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DATABLOCKREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDATABLOCKREFS(DATABLOCKREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DATABLOCKS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DATABLOCKS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDATABLOCKS(DATABLOCKS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DATAFILE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DATAFILE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDATAFILE(DATAFILE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DATAFORMAT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DATAFORMAT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDATAFORMAT(DATAFORMAT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DATAOBJECTPROP</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DATAOBJECTPROP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDATAOBJECTPROP(DATAOBJECTPROP object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DATAOBJECTPROPS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DATAOBJECTPROPS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDATAOBJECTPROPS(DATAOBJECTPROPS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DEFAULTCASE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DEFAULTCASE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDEFAULTCASE(DEFAULTCASE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DESCRIPTION</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DESCRIPTION</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDESCRIPTION(DESCRIPTION object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DESCRIPTION1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DESCRIPTION1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDESCRIPTION1(DESCRIPTION1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DETERMINENUMBEROFITEMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DETERMINENUMBEROFITEMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDETERMINENUMBEROFITEMS(DETERMINENUMBEROFITEMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGCODEDTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGCODEDTYPE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGCODEDTYPE(DIAGCODEDTYPE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGCOMM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGCOMM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGCOMM(DIAGCOMM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGCOMMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGCOMMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGCOMMS(DIAGCOMMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGDATADICTIONARYSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGDATADICTIONARYSPEC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGLAYER</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGLAYER</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGLAYER(DIAGLAYER object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGLAYERCONTAINER</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGLAYERCONTAINER</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGLAYERCONTAINER(DIAGLAYERCONTAINER object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGLAYERREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGLAYERREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGLAYERREFS(DIAGLAYERREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGSERVICE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGSERVICE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGSERVICE(DIAGSERVICE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGVARIABLE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGVARIABLE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGVARIABLE(DIAGVARIABLE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DIAGVARIABLES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DIAGVARIABLES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDIAGVARIABLES(DIAGVARIABLES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DOCREVISION</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DOCREVISION</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDOCREVISION(DOCREVISION object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DOCREVISION1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DOCREVISION1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDOCREVISION1(DOCREVISION1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DOCREVISIONS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DOCREVISIONS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDOCREVISIONS(DOCREVISIONS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DOCREVISIONS1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DOCREVISIONS1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDOCREVISIONS1(DOCREVISIONS1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DOPBASE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DOPBASE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDOPBASE(DOPBASE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DTC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DTC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDTC(DTC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DTCDOP</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DTCDOP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDTCDOP(DTCDOP object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DTCDOPS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DTCDOPS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDTCDOPS(DTCDOPS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DTCS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DTCS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDTCS(DTCS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DTCVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DTCVALUE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDTCVALUE(DTCVALUE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DTCVALUES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DTCVALUES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDTCVALUES(DTCVALUES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DYNAMIC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DYNAMIC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDYNAMIC(DYNAMIC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DYNAMICENDMARKERFIELD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DYNAMICENDMARKERFIELD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDYNAMICENDMARKERFIELD(DYNAMICENDMARKERFIELD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DYNAMICENDMARKERFIELDS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DYNAMICENDMARKERFIELDS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDYNAMICENDMARKERFIELDS(DYNAMICENDMARKERFIELDS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DYNAMICLENGTHFIELD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DYNAMICLENGTHFIELD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDYNAMICLENGTHFIELD(DYNAMICLENGTHFIELD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DYNAMICLENGTHFIELDS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DYNAMICLENGTHFIELDS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDYNAMICLENGTHFIELDS(DYNAMICLENGTHFIELDS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DYNDEFINEDSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DYNDEFINEDSPEC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDYNDEFINEDSPEC(DYNDEFINEDSPEC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DYNENDDOPREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DYNENDDOPREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDYNENDDOPREF(DYNENDDOPREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DYNIDDEFMODEINFO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DYNIDDEFMODEINFO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDYNIDDEFMODEINFO(DYNIDDEFMODEINFO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DYNIDDEFMODEINFOS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DYNIDDEFMODEINFOS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDYNIDDEFMODEINFOS(DYNIDDEFMODEINFOS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUGROUP</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUGROUP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUGROUP(ECUGROUP object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUGROUPS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUGROUPS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUGROUPS(ECUGROUPS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUMEM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUMEM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUMEM(ECUMEM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUMEMCONNECTOR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUMEMCONNECTOR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUMEMCONNECTOR(ECUMEMCONNECTOR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUMEMCONNECTORS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUMEMCONNECTORS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUMEMCONNECTORS(ECUMEMCONNECTORS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUMEMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUMEMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUMEMS(ECUMEMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUPROXY</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUPROXY</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUPROXY(ECUPROXY object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUPROXYREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUPROXYREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUPROXYREFS(ECUPROXYREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUSHAREDDATA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUSHAREDDATA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUSHAREDDATA(ECUSHAREDDATA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUSHAREDDATAREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUSHAREDDATAREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUSHAREDDATAREF(ECUSHAREDDATAREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUSHAREDDATAS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUSHAREDDATAS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUSHAREDDATAS(ECUSHAREDDATAS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUVARIANT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUVARIANT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUVARIANT(ECUVARIANT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUVARIANTPATTERN</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUVARIANTPATTERN</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUVARIANTPATTERN(ECUVARIANTPATTERN object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUVARIANTPATTERNS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUVARIANTPATTERNS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUVARIANTPATTERNS(ECUVARIANTPATTERNS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ECUVARIANTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ECUVARIANTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseECUVARIANTS(ECUVARIANTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ENCRYPTCOMPRESSMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ENCRYPTCOMPRESSMETHOD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseENCRYPTCOMPRESSMETHOD(ENCRYPTCOMPRESSMETHOD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ENDOFPDUFIELD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ENDOFPDUFIELD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseENDOFPDUFIELD(ENDOFPDUFIELD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ENDOFPDUFIELDS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ENDOFPDUFIELDS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseENDOFPDUFIELDS(ENDOFPDUFIELDS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ENVDATA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ENVDATA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseENVDATA(ENVDATA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ENVDATADESC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ENVDATADESC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseENVDATADESC(ENVDATADESC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ENVDATADESCS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ENVDATADESCS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseENVDATADESCS(ENVDATADESCS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ENVDATAS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ENVDATAS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseENVDATAS(ENVDATAS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EXPECTEDIDENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EXPECTEDIDENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEXPECTEDIDENT(EXPECTEDIDENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EXPECTEDIDENTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EXPECTEDIDENTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEXPECTEDIDENTS(EXPECTEDIDENTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EXTERNALACCESSMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EXTERNALACCESSMETHOD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEXTERNALACCESSMETHOD(EXTERNALACCESSMETHOD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EXTERNFLASHDATA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EXTERNFLASHDATA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEXTERNFLASHDATA(EXTERNFLASHDATA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FIELD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FIELD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFIELD(FIELD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FILE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FILE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFILE(FILE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FILES Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FILES Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFILESType(FILESType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FILTER</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FILTER</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFILTER(FILTER object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FILTERS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FILTERS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFILTERS(FILTERS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FLASH</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FLASH</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFLASH(FLASH object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FLASHCLASS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FLASHCLASS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFLASHCLASS(FLASHCLASS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FLASHCLASSREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FLASHCLASSREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFLASHCLASSREFS(FLASHCLASSREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FLASHCLASSS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FLASHCLASSS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFLASHCLASSS(FLASHCLASSS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FLASHDATA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FLASHDATA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFLASHDATA(FLASHDATA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FLASHDATAS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FLASHDATAS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFLASHDATAS(FLASHDATAS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Flow</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flow</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFlow(Flow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FUNCTCLASS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FUNCTCLASS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFUNCTCLASS(FUNCTCLASS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FUNCTCLASSREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FUNCTCLASSREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFUNCTCLASSREFS(FUNCTCLASSREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FUNCTCLASSS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FUNCTCLASSS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFUNCTCLASSS(FUNCTCLASSS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FUNCTIONALGROUP</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FUNCTIONALGROUP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFUNCTIONALGROUP(FUNCTIONALGROUP object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FUNCTIONALGROUPREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FUNCTIONALGROUPREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFUNCTIONALGROUPREF(FUNCTIONALGROUPREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FUNCTIONALGROUPS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FUNCTIONALGROUPS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFUNCTIONALGROUPS(FUNCTIONALGROUPS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FWCHECKSUM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FWCHECKSUM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFWCHECKSUM(FWCHECKSUM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FWSIGNATURE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FWSIGNATURE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFWSIGNATURE(FWSIGNATURE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GATEWAYLOGICALLINK</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GATEWAYLOGICALLINK</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGATEWAYLOGICALLINK(GATEWAYLOGICALLINK object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GATEWAYLOGICALLINKREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GATEWAYLOGICALLINKREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGATEWAYLOGICALLINKREFS(GATEWAYLOGICALLINKREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GLOBALNEGRESPONSE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GLOBALNEGRESPONSE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGLOBALNEGRESPONSE(GLOBALNEGRESPONSE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>GLOBALNEGRESPONSES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>GLOBALNEGRESPONSES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGLOBALNEGRESPONSES(GLOBALNEGRESPONSES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HIERARCHYELEMENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HIERARCHYELEMENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHIERARCHYELEMENT(HIERARCHYELEMENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IDENTDESC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IDENTDESC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIDENTDESC(IDENTDESC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IDENTDESCS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IDENTDESCS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIDENTDESCS(IDENTDESCS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IDENTVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IDENTVALUE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIDENTVALUE(IDENTVALUE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IDENTVALUES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IDENTVALUES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIDENTVALUES(IDENTVALUES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IMPORTREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IMPORTREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIMPORTREFS(IMPORTREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INFOCOMPONENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INFOCOMPONENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINFOCOMPONENT(INFOCOMPONENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INFOCOMPONENTREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INFOCOMPONENTREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINFOCOMPONENTREFS(INFOCOMPONENTREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INFOCOMPONENTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INFOCOMPONENTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINFOCOMPONENTS(INFOCOMPONENTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inline</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inline</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInline(Inline object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INPUTPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INPUTPARAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINPUTPARAM(INPUTPARAM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INPUTPARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INPUTPARAMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINPUTPARAMS(INPUTPARAMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INTERNALCONSTR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INTERNALCONSTR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINTERNALCONSTR(INTERNALCONSTR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INTERNFLASHDATA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INTERNFLASHDATA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINTERNFLASHDATA(INTERNFLASHDATA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIType(IType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LAYERREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LAYERREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLAYERREFS(LAYERREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LEADINGLENGTHINFOTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LEADINGLENGTHINFOTYPE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLEADINGLENGTHINFOTYPE(LEADINGLENGTHINFOTYPE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LENGTHDESCRIPTOR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LENGTHDESCRIPTOR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLENGTHDESCRIPTOR(LENGTHDESCRIPTOR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LENGTHKEY</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LENGTHKEY</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLENGTHKEY(LENGTHKEY object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LIMIT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LIMIT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLIMIT(LIMIT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LINKCOMPARAMREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LINKCOMPARAMREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLINKCOMPARAMREF(LINKCOMPARAMREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LINKCOMPARAMREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LINKCOMPARAMREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLINKCOMPARAMREFS(LINKCOMPARAMREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Li Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Li Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLiType(LiType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LOGICALLINK</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LOGICALLINK</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLOGICALLINK(LOGICALLINK object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LOGICALLINKREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LOGICALLINKREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLOGICALLINKREFS(LOGICALLINKREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LOGICALLINKS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LOGICALLINKS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLOGICALLINKS(LOGICALLINKS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MATCHINGCOMPONENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MATCHINGCOMPONENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMATCHINGCOMPONENT(MATCHINGCOMPONENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MATCHINGCOMPONENTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MATCHINGCOMPONENTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMATCHINGCOMPONENTS(MATCHINGCOMPONENTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MATCHINGPARAMETER</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MATCHINGPARAMETER</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMATCHINGPARAMETER(MATCHINGPARAMETER object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MATCHINGPARAMETERS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MATCHINGPARAMETERS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMATCHINGPARAMETERS(MATCHINGPARAMETERS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MATCHINGREQUESTPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MATCHINGREQUESTPARAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMATCHINGREQUESTPARAM(MATCHINGREQUESTPARAM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MEM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MEM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMEM(MEM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MEMBERLOGICALLINK</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MEMBERLOGICALLINK</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMEMBERLOGICALLINK(MEMBERLOGICALLINK object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MINMAXLENGTHTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MINMAXLENGTHTYPE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMINMAXLENGTHTYPE(MINMAXLENGTHTYPE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MODELYEAR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MODELYEAR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMODELYEAR(MODELYEAR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MODIFICATION</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MODIFICATION</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMODIFICATION(MODIFICATION object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MODIFICATION1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MODIFICATION1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMODIFICATION1(MODIFICATION1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MODIFICATIONS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MODIFICATIONS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMODIFICATIONS(MODIFICATIONS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MODIFICATIONS1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MODIFICATIONS1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMODIFICATIONS1(MODIFICATIONS1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MULTIPLEECUJOB</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MULTIPLEECUJOB</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMULTIPLEECUJOB(MULTIPLEECUJOB object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MULTIPLEECUJOBS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MULTIPLEECUJOBS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMULTIPLEECUJOBS(MULTIPLEECUJOBS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MULTIPLEECUJOBSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MULTIPLEECUJOBSPEC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMULTIPLEECUJOBSPEC(MULTIPLEECUJOBSPEC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUX</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUX</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUX(MUX object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MUXS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MUXS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMUXS(MUXS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NEGOFFSET</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NEGOFFSET</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNEGOFFSET(NEGOFFSET object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NEGOUTPUTPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NEGOUTPUTPARAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNEGOUTPUTPARAM(NEGOUTPUTPARAM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NEGOUTPUTPARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NEGOUTPUTPARAMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNEGOUTPUTPARAMS(NEGOUTPUTPARAMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NEGRESPONSE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NEGRESPONSE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNEGRESPONSE(NEGRESPONSE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NEGRESPONSEREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NEGRESPONSEREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNEGRESPONSEREFS(NEGRESPONSEREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NEGRESPONSES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NEGRESPONSES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNEGRESPONSES(NEGRESPONSES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NOTINHERITEDDIAGCOMM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NOTINHERITEDDIAGCOMM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNOTINHERITEDDIAGCOMM(NOTINHERITEDDIAGCOMM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NOTINHERITEDDIAGCOMMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NOTINHERITEDDIAGCOMMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNOTINHERITEDDIAGCOMMS(NOTINHERITEDDIAGCOMMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NOTINHERITEDVARIABLE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NOTINHERITEDVARIABLE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNOTINHERITEDVARIABLE(NOTINHERITEDVARIABLE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>NOTINHERITEDVARIABLES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>NOTINHERITEDVARIABLES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNOTINHERITEDVARIABLES(NOTINHERITEDVARIABLES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ODX</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ODX</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseODX(ODX object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ODXCATEGORY</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ODXCATEGORY</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseODXCATEGORY(ODXCATEGORY object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ODXLINK</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ODXLINK</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseODXLINK(ODXLINK object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ODXLINK1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ODXLINK1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseODXLINK1(ODXLINK1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OEM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OEM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOEM(OEM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ol Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ol Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOlType(OlType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OUTPUTPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OUTPUTPARAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOUTPUTPARAM(OUTPUTPARAM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OUTPUTPARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OUTPUTPARAMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOUTPUTPARAMS(OUTPUTPARAMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OWNIDENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OWNIDENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOWNIDENT(OWNIDENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OWNIDENTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OWNIDENTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOWNIDENTS(OWNIDENTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>P</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>P</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseP(P object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PARAM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PARAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePARAM(PARAM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PARAMLENGTHINFOTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PARAMLENGTHINFOTYPE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePARAMLENGTHINFOTYPE(PARAMLENGTHINFOTYPE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PARAMS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PARAMS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePARAMS(PARAMS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PARENTREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PARENTREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePARENTREF(PARENTREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PARENTREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PARENTREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePARENTREFS(PARENTREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PHYSCONST</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PHYSCONST</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePHYSCONST(PHYSCONST object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PHYSICALDIMENSION</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PHYSICALDIMENSION</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePHYSICALDIMENSION(PHYSICALDIMENSION object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PHYSICALDIMENSIONS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PHYSICALDIMENSIONS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePHYSICALDIMENSIONS(PHYSICALDIMENSIONS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PHYSICALTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PHYSICALTYPE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePHYSICALTYPE(PHYSICALTYPE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PHYSICALVEHICLELINK</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PHYSICALVEHICLELINK</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePHYSICALVEHICLELINK(PHYSICALVEHICLELINK object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PHYSICALVEHICLELINKS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PHYSICALVEHICLELINKS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePHYSICALVEHICLELINKS(PHYSICALVEHICLELINKS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PHYSMEM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PHYSMEM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePHYSMEM(PHYSMEM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PHYSSEGMENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PHYSSEGMENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePHYSSEGMENT(PHYSSEGMENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PHYSSEGMENTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PHYSSEGMENTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePHYSSEGMENTS(PHYSSEGMENTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>POSITIONABLEPARAM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>POSITIONABLEPARAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePOSITIONABLEPARAM(POSITIONABLEPARAM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>POSOFFSET</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>POSOFFSET</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePOSOFFSET(POSOFFSET object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>POSRESPONSE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>POSRESPONSE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePOSRESPONSE(POSRESPONSE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>POSRESPONSEREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>POSRESPONSEREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePOSRESPONSEREFS(POSRESPONSEREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>POSRESPONSES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>POSRESPONSES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePOSRESPONSES(POSRESPONSES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROGCODE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROGCODE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROGCODE(PROGCODE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROGCODES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROGCODES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROGCODES(PROGCODES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROJECTIDENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROJECTIDENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROJECTIDENT(PROJECTIDENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROJECTIDENTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROJECTIDENTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROJECTIDENTS(PROJECTIDENTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROJECTINFO</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROJECTINFO</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROJECTINFO(PROJECTINFO object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROJECTINFOS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROJECTINFOS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROJECTINFOS(PROJECTINFOS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROTOCOL</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROTOCOL</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROTOCOL(PROTOCOL object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROTOCOLREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROTOCOLREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROTOCOLREF(PROTOCOLREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROTOCOLS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROTOCOLS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROTOCOLS(PROTOCOLS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>PROTOCOLSNREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>PROTOCOLSNREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePROTOCOLSNREFS(PROTOCOLSNREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RELATEDDIAGCOMMREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RELATEDDIAGCOMMREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRELATEDDIAGCOMMREF(RELATEDDIAGCOMMREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RELATEDDIAGCOMMREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RELATEDDIAGCOMMREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRELATEDDIAGCOMMREFS(RELATEDDIAGCOMMREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RELATEDDOC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RELATEDDOC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRELATEDDOC(RELATEDDOC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RELATEDDOC1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RELATEDDOC1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRELATEDDOC1(RELATEDDOC1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RELATEDDOCS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RELATEDDOCS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRELATEDDOCS(RELATEDDOCS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RELATEDDOCS1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RELATEDDOCS1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRELATEDDOCS1(RELATEDDOCS1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>REQUEST</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>REQUEST</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseREQUEST(REQUEST object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>REQUESTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>REQUESTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseREQUESTS(REQUESTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RESERVED</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RESERVED</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRESERVED(RESERVED object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RESPONSE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RESPONSE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRESPONSE(RESPONSE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ROLES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ROLES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseROLES(ROLES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ROLES1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ROLES1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseROLES1(ROLES1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SCALECONSTR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SCALECONSTR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSCALECONSTR(SCALECONSTR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SCALECONSTRS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SCALECONSTRS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSCALECONSTRS(SCALECONSTRS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSD(SD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SD1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SD1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSD1(SD1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SDG</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SDG</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSDG(SDG object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SDG1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SDG1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSDG1(SDG1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SDGCAPTION</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SDGCAPTION</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSDGCAPTION(SDGCAPTION object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SDGCAPTION1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SDGCAPTION1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSDGCAPTION1(SDGCAPTION1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SDGS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SDGS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSDGS(SDGS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SDGS1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SDGS1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSDGS1(SDGS1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SECURITY</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SECURITY</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSECURITY(SECURITY object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SECURITYMETHOD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SECURITYMETHOD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSECURITYMETHOD(SECURITYMETHOD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SECURITYS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SECURITYS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSECURITYS(SECURITYS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SEGMENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SEGMENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSEGMENT(SEGMENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SEGMENTS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SEGMENTS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSEGMENTS(SEGMENTS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SELECTIONTABLEREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SELECTIONTABLEREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSELECTIONTABLEREFS(SELECTIONTABLEREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SESSION</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SESSION</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSESSION(SESSION object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SESSIONDESC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SESSIONDESC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSESSIONDESC(SESSIONDESC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SESSIONDESCS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SESSIONDESCS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSESSIONDESCS(SESSIONDESCS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SESSIONS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SESSIONS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSESSIONS(SESSIONS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SINGLEECUJOB</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SINGLEECUJOB</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSINGLEECUJOB(SINGLEECUJOB object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SIZEDEFFILTER</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SIZEDEFFILTER</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSIZEDEFFILTER(SIZEDEFFILTER object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SIZEDEFPHYSSEGMENT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SIZEDEFPHYSSEGMENT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSIZEDEFPHYSSEGMENT(SIZEDEFPHYSSEGMENT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SNREF</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SNREF</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSNREF(SNREF object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SOURCEENDADDRESS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SOURCEENDADDRESS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSOURCEENDADDRESS(SOURCEENDADDRESS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SPECIALDATA</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SPECIALDATA</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSPECIALDATA(SPECIALDATA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SPECIALDATA1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SPECIALDATA1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSPECIALDATA1(SPECIALDATA1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>STANDARDLENGTHTYPE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>STANDARDLENGTHTYPE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSTANDARDLENGTHTYPE(STANDARDLENGTHTYPE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>STATICFIELD</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>STATICFIELD</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSTATICFIELD(STATICFIELD object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>STATICFIELDS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>STATICFIELDS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSTATICFIELDS(STATICFIELDS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>STRUCTURE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>STRUCTURE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSTRUCTURE(STRUCTURE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>STRUCTURES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>STRUCTURES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSTRUCTURES(STRUCTURES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sub Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sub Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubType(SubType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SUPPORTEDDYNID</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SUPPORTEDDYNID</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSUPPORTEDDYNID(SUPPORTEDDYNID object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SUPPORTEDDYNIDS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SUPPORTEDDYNIDS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSUPPORTEDDYNIDS(SUPPORTEDDYNIDS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sup Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sup Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSupType(SupType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SWITCHKEY</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SWITCHKEY</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSWITCHKEY(SWITCHKEY object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SWVARIABLE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SWVARIABLE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSWVARIABLE(SWVARIABLE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SWVARIABLES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SWVARIABLES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSWVARIABLES(SWVARIABLES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SYSTEM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SYSTEM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSYSTEM(SYSTEM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TABLE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TABLE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTABLE(TABLE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TABLEENTRY</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TABLEENTRY</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTABLEENTRY(TABLEENTRY object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TABLEKEY</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TABLEKEY</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTABLEKEY(TABLEKEY object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TABLEROW</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TABLEROW</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTABLEROW(TABLEROW object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TABLES</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TABLES</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTABLES(TABLES object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TABLESTRUCT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TABLESTRUCT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTABLESTRUCT(TABLESTRUCT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TARGETADDROFFSET</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TARGETADDROFFSET</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTARGETADDROFFSET(TARGETADDROFFSET object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TEAMMEMBER</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TEAMMEMBER</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTEAMMEMBER(TEAMMEMBER object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TEAMMEMBER1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TEAMMEMBER1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTEAMMEMBER1(TEAMMEMBER1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TEAMMEMBERS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TEAMMEMBERS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTEAMMEMBERS(TEAMMEMBERS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TEAMMEMBERS1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TEAMMEMBERS1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTEAMMEMBERS1(TEAMMEMBERS1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TEXT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TEXT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTEXT(TEXT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>TEXT1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>TEXT1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTEXT1(TEXT1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ul Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ul Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUlType(UlType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UNCOMPRESSEDSIZE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UNCOMPRESSEDSIZE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUNCOMPRESSEDSIZE(UNCOMPRESSEDSIZE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UNIONVALUE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UNIONVALUE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUNIONVALUE(UNIONVALUE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UNIT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UNIT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUNIT(UNIT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UNITGROUP</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UNITGROUP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUNITGROUP(UNITGROUP object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UNITGROUPS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UNITGROUPS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUNITGROUPS(UNITGROUPS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UNITREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UNITREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUNITREFS(UNITREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UNITS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UNITS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUNITS(UNITS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UNITSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UNITSPEC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUNITSPEC(UNITSPEC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>V</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>V</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseV(V object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VALIDITYFOR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VALIDITYFOR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVALIDITYFOR(VALIDITYFOR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VALUE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VALUE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVALUE(VALUE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VARIABLEGROUP</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VARIABLEGROUP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVARIABLEGROUP(VARIABLEGROUP object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VARIABLEGROUPS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VARIABLEGROUPS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVARIABLEGROUPS(VARIABLEGROUPS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLECONNECTOR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLECONNECTOR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLECONNECTOR(VEHICLECONNECTOR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLECONNECTORPIN</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLECONNECTORPIN</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLECONNECTORPIN(VEHICLECONNECTORPIN object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLECONNECTORPINREFS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLECONNECTORPINREFS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLECONNECTORPINREFS(VEHICLECONNECTORPINREFS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLECONNECTORPINS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLECONNECTORPINS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLECONNECTORPINS(VEHICLECONNECTORPINS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLECONNECTORS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLECONNECTORS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLECONNECTORS(VEHICLECONNECTORS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLEINFORMATION</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLEINFORMATION</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLEINFORMATION(VEHICLEINFORMATION object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLEINFORMATIONS</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLEINFORMATIONS</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLEINFORMATIONS(VEHICLEINFORMATIONS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLEINFOSPEC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLEINFOSPEC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLEINFOSPEC(VEHICLEINFOSPEC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLEMODEL</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLEMODEL</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLEMODEL(VEHICLEMODEL object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VEHICLETYPE</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VEHICLETYPE</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVEHICLETYPE(VEHICLETYPE object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VT</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VT</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVT(VT object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XDOC</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XDOC</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXDOC(XDOC object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XDOC1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XDOC1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXDOC1(XDOC1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OdxXhtmlSwitch
