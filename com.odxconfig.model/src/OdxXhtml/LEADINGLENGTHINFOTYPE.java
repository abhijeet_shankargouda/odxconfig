/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LEADINGLENGTHINFOTYPE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.LEADINGLENGTHINFOTYPE#getBITLENGTH <em>BITLENGTH</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLEADINGLENGTHINFOTYPE()
 * @model extendedMetaData="name='LEADING-LENGTH-INFO-TYPE' kind='elementOnly'"
 * @generated
 */
public interface LEADINGLENGTHINFOTYPE extends DIAGCODEDTYPE {
	/**
	 * Returns the value of the '<em><b>BITLENGTH</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BITLENGTH</em>' attribute.
	 * @see #isSetBITLENGTH()
	 * @see #unsetBITLENGTH()
	 * @see #setBITLENGTH(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getLEADINGLENGTHINFOTYPE_BITLENGTH()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='BIT-LENGTH' namespace='##targetNamespace'"
	 * @generated
	 */
	long getBITLENGTH();

	/**
	 * Sets the value of the '{@link OdxXhtml.LEADINGLENGTHINFOTYPE#getBITLENGTH <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BITLENGTH</em>' attribute.
	 * @see #isSetBITLENGTH()
	 * @see #unsetBITLENGTH()
	 * @see #getBITLENGTH()
	 * @generated
	 */
	void setBITLENGTH(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.LEADINGLENGTHINFOTYPE#getBITLENGTH <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBITLENGTH()
	 * @see #getBITLENGTH()
	 * @see #setBITLENGTH(long)
	 * @generated
	 */
	void unsetBITLENGTH();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.LEADINGLENGTHINFOTYPE#getBITLENGTH <em>BITLENGTH</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BITLENGTH</em>' attribute is set.
	 * @see #unsetBITLENGTH()
	 * @see #getBITLENGTH()
	 * @see #setBITLENGTH(long)
	 * @generated
	 */
	boolean isSetBITLENGTH();

} // LEADINGLENGTHINFOTYPE
