/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLECONNECTORPINREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORPINREFS#getVEHICLECONNECTORPINREF <em>VEHICLECONNECTORPINREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPINREFS()
 * @model extendedMetaData="name='VEHICLE-CONNECTOR-PIN-REFS' kind='elementOnly'"
 * @generated
 */
public interface VEHICLECONNECTORPINREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>VEHICLECONNECTORPINREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VEHICLECONNECTORPINREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPINREFS_VEHICLECONNECTORPINREF()
	 * @model containment="true" required="true" upper="2"
	 *        extendedMetaData="kind='element' name='VEHICLE-CONNECTOR-PIN-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getVEHICLECONNECTORPINREF();

} // VEHICLECONNECTORPINREFS
