/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TABLESTRUCT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.TABLESTRUCT#getTABLEKEYREF <em>TABLEKEYREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getTABLESTRUCT()
 * @model extendedMetaData="name='TABLE-STRUCT' kind='elementOnly'"
 * @generated
 */
public interface TABLESTRUCT extends POSITIONABLEPARAM {
	/**
	 * Returns the value of the '<em><b>TABLEKEYREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TABLEKEYREF</em>' containment reference.
	 * @see #setTABLEKEYREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLESTRUCT_TABLEKEYREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='TABLE-KEY-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getTABLEKEYREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLESTRUCT#getTABLEKEYREF <em>TABLEKEYREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TABLEKEYREF</em>' containment reference.
	 * @see #getTABLEKEYREF()
	 * @generated
	 */
	void setTABLEKEYREF(ODXLINK value);

} // TABLESTRUCT
