/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FUNCTIONALGROUP</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FUNCTIONALGROUP#getDIAGVARIABLES <em>DIAGVARIABLES</em>}</li>
 *   <li>{@link OdxXhtml.FUNCTIONALGROUP#getVARIABLEGROUPS <em>VARIABLEGROUPS</em>}</li>
 *   <li>{@link OdxXhtml.FUNCTIONALGROUP#getACCESSLEVELS <em>ACCESSLEVELS</em>}</li>
 *   <li>{@link OdxXhtml.FUNCTIONALGROUP#getAUTMETHODS <em>AUTMETHODS</em>}</li>
 *   <li>{@link OdxXhtml.FUNCTIONALGROUP#getPARENTREFS <em>PARENTREFS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTIONALGROUP()
 * @model extendedMetaData="name='FUNCTIONAL-GROUP' kind='elementOnly'"
 * @generated
 */
public interface FUNCTIONALGROUP extends HIERARCHYELEMENT {
	/**
	 * Returns the value of the '<em><b>DIAGVARIABLES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGVARIABLES</em>' containment reference.
	 * @see #setDIAGVARIABLES(DIAGVARIABLES)
	 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTIONALGROUP_DIAGVARIABLES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-VARIABLES' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGVARIABLES getDIAGVARIABLES();

	/**
	 * Sets the value of the '{@link OdxXhtml.FUNCTIONALGROUP#getDIAGVARIABLES <em>DIAGVARIABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGVARIABLES</em>' containment reference.
	 * @see #getDIAGVARIABLES()
	 * @generated
	 */
	void setDIAGVARIABLES(DIAGVARIABLES value);

	/**
	 * Returns the value of the '<em><b>VARIABLEGROUPS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VARIABLEGROUPS</em>' containment reference.
	 * @see #setVARIABLEGROUPS(VARIABLEGROUPS)
	 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTIONALGROUP_VARIABLEGROUPS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='VARIABLE-GROUPS' namespace='##targetNamespace'"
	 * @generated
	 */
	VARIABLEGROUPS getVARIABLEGROUPS();

	/**
	 * Sets the value of the '{@link OdxXhtml.FUNCTIONALGROUP#getVARIABLEGROUPS <em>VARIABLEGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VARIABLEGROUPS</em>' containment reference.
	 * @see #getVARIABLEGROUPS()
	 * @generated
	 */
	void setVARIABLEGROUPS(VARIABLEGROUPS value);

	/**
	 * Returns the value of the '<em><b>ACCESSLEVELS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ACCESSLEVELS</em>' containment reference.
	 * @see #setACCESSLEVELS(ACCESSLEVELS)
	 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTIONALGROUP_ACCESSLEVELS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ACCESS-LEVELS' namespace='##targetNamespace'"
	 * @generated
	 */
	ACCESSLEVELS getACCESSLEVELS();

	/**
	 * Sets the value of the '{@link OdxXhtml.FUNCTIONALGROUP#getACCESSLEVELS <em>ACCESSLEVELS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ACCESSLEVELS</em>' containment reference.
	 * @see #getACCESSLEVELS()
	 * @generated
	 */
	void setACCESSLEVELS(ACCESSLEVELS value);

	/**
	 * Returns the value of the '<em><b>AUTMETHODS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUTMETHODS</em>' containment reference.
	 * @see #setAUTMETHODS(AUTMETHODS)
	 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTIONALGROUP_AUTMETHODS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='AUT-METHODS' namespace='##targetNamespace'"
	 * @generated
	 */
	AUTMETHODS getAUTMETHODS();

	/**
	 * Sets the value of the '{@link OdxXhtml.FUNCTIONALGROUP#getAUTMETHODS <em>AUTMETHODS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AUTMETHODS</em>' containment reference.
	 * @see #getAUTMETHODS()
	 * @generated
	 */
	void setAUTMETHODS(AUTMETHODS value);

	/**
	 * Returns the value of the '<em><b>PARENTREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PARENTREFS</em>' containment reference.
	 * @see #setPARENTREFS(PARENTREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTIONALGROUP_PARENTREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PARENT-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	PARENTREFS getPARENTREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.FUNCTIONALGROUP#getPARENTREFS <em>PARENTREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PARENTREFS</em>' containment reference.
	 * @see #getPARENTREFS()
	 * @generated
	 */
	void setPARENTREFS(PARENTREFS value);

} // FUNCTIONALGROUP
