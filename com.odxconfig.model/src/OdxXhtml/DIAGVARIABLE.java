/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGVARIABLE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#getVARIABLEGROUPREF <em>VARIABLEGROUPREF</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#getSWVARIABLES <em>SWVARIABLES</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#getCOMMRELATIONS <em>COMMRELATIONS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#isISREADBEFOREWRITE <em>ISREADBEFOREWRITE</em>}</li>
 *   <li>{@link OdxXhtml.DIAGVARIABLE#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE()
 * @model extendedMetaData="name='DIAG-VARIABLE' kind='elementOnly'"
 * @generated
 */
public interface DIAGVARIABLE extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA value);

	/**
	 * Returns the value of the '<em><b>VARIABLEGROUPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VARIABLEGROUPREF</em>' containment reference.
	 * @see #setVARIABLEGROUPREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_VARIABLEGROUPREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='VARIABLE-GROUP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getVARIABLEGROUPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#getVARIABLEGROUPREF <em>VARIABLEGROUPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VARIABLEGROUPREF</em>' containment reference.
	 * @see #getVARIABLEGROUPREF()
	 * @generated
	 */
	void setVARIABLEGROUPREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>SWVARIABLES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SWVARIABLES</em>' containment reference.
	 * @see #setSWVARIABLES(SWVARIABLES)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_SWVARIABLES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SW-VARIABLES' namespace='##targetNamespace'"
	 * @generated
	 */
	SWVARIABLES getSWVARIABLES();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#getSWVARIABLES <em>SWVARIABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SWVARIABLES</em>' containment reference.
	 * @see #getSWVARIABLES()
	 * @generated
	 */
	void setSWVARIABLES(SWVARIABLES value);

	/**
	 * Returns the value of the '<em><b>COMMRELATIONS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMMRELATIONS</em>' containment reference.
	 * @see #setCOMMRELATIONS(COMMRELATIONS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_COMMRELATIONS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMM-RELATIONS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMMRELATIONS getCOMMRELATIONS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#getCOMMRELATIONS <em>COMMRELATIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMMRELATIONS</em>' containment reference.
	 * @see #getCOMMRELATIONS()
	 * @generated
	 */
	void setCOMMRELATIONS(COMMRELATIONS value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>ISREADBEFOREWRITE</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISREADBEFOREWRITE</em>' attribute.
	 * @see #isSetISREADBEFOREWRITE()
	 * @see #unsetISREADBEFOREWRITE()
	 * @see #setISREADBEFOREWRITE(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_ISREADBEFOREWRITE()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-READ-BEFORE-WRITE' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISREADBEFOREWRITE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#isISREADBEFOREWRITE <em>ISREADBEFOREWRITE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISREADBEFOREWRITE</em>' attribute.
	 * @see #isSetISREADBEFOREWRITE()
	 * @see #unsetISREADBEFOREWRITE()
	 * @see #isISREADBEFOREWRITE()
	 * @generated
	 */
	void setISREADBEFOREWRITE(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGVARIABLE#isISREADBEFOREWRITE <em>ISREADBEFOREWRITE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISREADBEFOREWRITE()
	 * @see #isISREADBEFOREWRITE()
	 * @see #setISREADBEFOREWRITE(boolean)
	 * @generated
	 */
	void unsetISREADBEFOREWRITE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGVARIABLE#isISREADBEFOREWRITE <em>ISREADBEFOREWRITE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISREADBEFOREWRITE</em>' attribute is set.
	 * @see #unsetISREADBEFOREWRITE()
	 * @see #isISREADBEFOREWRITE()
	 * @see #setISREADBEFOREWRITE(boolean)
	 * @generated
	 */
	boolean isSetISREADBEFOREWRITE();

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGVARIABLE_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGVARIABLE#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // DIAGVARIABLE
