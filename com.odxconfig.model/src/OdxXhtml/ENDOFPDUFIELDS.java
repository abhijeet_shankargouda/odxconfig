/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ENDOFPDUFIELDS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ENDOFPDUFIELDS#getENDOFPDUFIELD <em>ENDOFPDUFIELD</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getENDOFPDUFIELDS()
 * @model extendedMetaData="name='END-OF-PDU-FIELDS' kind='elementOnly'"
 * @generated
 */
public interface ENDOFPDUFIELDS extends EObject {
	/**
	 * Returns the value of the '<em><b>ENDOFPDUFIELD</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ENDOFPDUFIELD}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENDOFPDUFIELD</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getENDOFPDUFIELDS_ENDOFPDUFIELD()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='END-OF-PDU-FIELD' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ENDOFPDUFIELD> getENDOFPDUFIELD();

} // ENDOFPDUFIELDS
