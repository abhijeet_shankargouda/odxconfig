/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>IDENTVALUETYPE</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getIDENTVALUETYPE()
 * @model extendedMetaData="name='IDENT-VALUE-TYPE'"
 * @generated
 */
public enum IDENTVALUETYPE implements Enumerator {
	/**
	 * The '<em><b>AUINT32</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT32_VALUE
	 * @generated
	 * @ordered
	 */
	AUINT32(0, "AUINT32", "A_UINT32"),

	/**
	 * The '<em><b>ABYTEFIELD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABYTEFIELD_VALUE
	 * @generated
	 * @ordered
	 */
	ABYTEFIELD(1, "ABYTEFIELD", "A_BYTEFIELD"),

	/**
	 * The '<em><b>AASCIISTRING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AASCIISTRING_VALUE
	 * @generated
	 * @ordered
	 */
	AASCIISTRING(2, "AASCIISTRING", "A_ASCIISTRING");

	/**
	 * The '<em><b>AUINT32</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT32
	 * @model literal="A_UINT32"
	 * @generated
	 * @ordered
	 */
	public static final int AUINT32_VALUE = 0;

	/**
	 * The '<em><b>ABYTEFIELD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABYTEFIELD
	 * @model literal="A_BYTEFIELD"
	 * @generated
	 * @ordered
	 */
	public static final int ABYTEFIELD_VALUE = 1;

	/**
	 * The '<em><b>AASCIISTRING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AASCIISTRING
	 * @model literal="A_ASCIISTRING"
	 * @generated
	 * @ordered
	 */
	public static final int AASCIISTRING_VALUE = 2;

	/**
	 * An array of all the '<em><b>IDENTVALUETYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final IDENTVALUETYPE[] VALUES_ARRAY =
		new IDENTVALUETYPE[] {
			AUINT32,
			ABYTEFIELD,
			AASCIISTRING,
		};

	/**
	 * A public read-only list of all the '<em><b>IDENTVALUETYPE</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<IDENTVALUETYPE> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>IDENTVALUETYPE</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static IDENTVALUETYPE get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			IDENTVALUETYPE result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>IDENTVALUETYPE</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static IDENTVALUETYPE getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			IDENTVALUETYPE result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>IDENTVALUETYPE</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static IDENTVALUETYPE get(int value) {
		switch (value) {
			case AUINT32_VALUE: return AUINT32;
			case ABYTEFIELD_VALUE: return ABYTEFIELD;
			case AASCIISTRING_VALUE: return AASCIISTRING;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private IDENTVALUETYPE(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //IDENTVALUETYPE
