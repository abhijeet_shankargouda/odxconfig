/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FILTERS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FILTERS#getFILTER <em>FILTER</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFILTERS()
 * @model extendedMetaData="name='FILTERS' kind='elementOnly'"
 * @generated
 */
public interface FILTERS extends EObject {
	/**
	 * Returns the value of the '<em><b>FILTER</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.FILTER}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FILTER</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getFILTERS_FILTER()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FILTER' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<FILTER> getFILTER();

} // FILTERS
