/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SDGS1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SDGS1#getSDG <em>SDG</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSDGS1()
 * @model extendedMetaData="name='SDGS' kind='elementOnly'"
 * @generated
 */
public interface SDGS1 extends EObject {
	/**
	 * Returns the value of the '<em><b>SDG</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SDG1}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDG</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSDGS1_SDG()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SDG' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SDG1> getSDG();

} // SDGS1
