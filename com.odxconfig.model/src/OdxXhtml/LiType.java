/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Li Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getLiType()
 * @model extendedMetaData="name='li_._type' kind='mixed'"
 * @generated
 */
public interface LiType extends Flow {
} // LiType
