/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ODX</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ODX#getDIAGLAYERCONTAINER <em>DIAGLAYERCONTAINER</em>}</li>
 *   <li>{@link OdxXhtml.ODX#getCOMPARAMSPEC <em>COMPARAMSPEC</em>}</li>
 *   <li>{@link OdxXhtml.ODX#getVEHICLEINFOSPEC <em>VEHICLEINFOSPEC</em>}</li>
 *   <li>{@link OdxXhtml.ODX#getFLASH <em>FLASH</em>}</li>
 *   <li>{@link OdxXhtml.ODX#getMULTIPLEECUJOBSPEC <em>MULTIPLEECUJOBSPEC</em>}</li>
 *   <li>{@link OdxXhtml.ODX#getMODELVERSION <em>MODELVERSION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getODX()
 * @model extendedMetaData="name='ODX' kind='elementOnly'"
 * @generated
 */
public interface ODX extends EObject {
	/**
	 * Returns the value of the '<em><b>DIAGLAYERCONTAINER</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGLAYERCONTAINER</em>' containment reference.
	 * @see #setDIAGLAYERCONTAINER(DIAGLAYERCONTAINER)
	 * @see OdxXhtml.OdxXhtmlPackage#getODX_DIAGLAYERCONTAINER()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-LAYER-CONTAINER' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGLAYERCONTAINER getDIAGLAYERCONTAINER();

	/**
	 * Sets the value of the '{@link OdxXhtml.ODX#getDIAGLAYERCONTAINER <em>DIAGLAYERCONTAINER</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGLAYERCONTAINER</em>' containment reference.
	 * @see #getDIAGLAYERCONTAINER()
	 * @generated
	 */
	void setDIAGLAYERCONTAINER(DIAGLAYERCONTAINER value);

	/**
	 * Returns the value of the '<em><b>COMPARAMSPEC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPARAMSPEC</em>' containment reference.
	 * @see #setCOMPARAMSPEC(COMPARAMSPEC)
	 * @see OdxXhtml.OdxXhtmlPackage#getODX_COMPARAMSPEC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPARAM-SPEC' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPARAMSPEC getCOMPARAMSPEC();

	/**
	 * Sets the value of the '{@link OdxXhtml.ODX#getCOMPARAMSPEC <em>COMPARAMSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPARAMSPEC</em>' containment reference.
	 * @see #getCOMPARAMSPEC()
	 * @generated
	 */
	void setCOMPARAMSPEC(COMPARAMSPEC value);

	/**
	 * Returns the value of the '<em><b>VEHICLEINFOSPEC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VEHICLEINFOSPEC</em>' containment reference.
	 * @see #setVEHICLEINFOSPEC(VEHICLEINFOSPEC)
	 * @see OdxXhtml.OdxXhtmlPackage#getODX_VEHICLEINFOSPEC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='VEHICLE-INFO-SPEC' namespace='##targetNamespace'"
	 * @generated
	 */
	VEHICLEINFOSPEC getVEHICLEINFOSPEC();

	/**
	 * Sets the value of the '{@link OdxXhtml.ODX#getVEHICLEINFOSPEC <em>VEHICLEINFOSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VEHICLEINFOSPEC</em>' containment reference.
	 * @see #getVEHICLEINFOSPEC()
	 * @generated
	 */
	void setVEHICLEINFOSPEC(VEHICLEINFOSPEC value);

	/**
	 * Returns the value of the '<em><b>FLASH</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FLASH</em>' containment reference.
	 * @see #setFLASH(FLASH)
	 * @see OdxXhtml.OdxXhtmlPackage#getODX_FLASH()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FLASH' namespace='##targetNamespace'"
	 * @generated
	 */
	FLASH getFLASH();

	/**
	 * Sets the value of the '{@link OdxXhtml.ODX#getFLASH <em>FLASH</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FLASH</em>' containment reference.
	 * @see #getFLASH()
	 * @generated
	 */
	void setFLASH(FLASH value);

	/**
	 * Returns the value of the '<em><b>MULTIPLEECUJOBSPEC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MULTIPLEECUJOBSPEC</em>' containment reference.
	 * @see #setMULTIPLEECUJOBSPEC(MULTIPLEECUJOBSPEC)
	 * @see OdxXhtml.OdxXhtmlPackage#getODX_MULTIPLEECUJOBSPEC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='MULTIPLE-ECU-JOB-SPEC' namespace='##targetNamespace'"
	 * @generated
	 */
	MULTIPLEECUJOBSPEC getMULTIPLEECUJOBSPEC();

	/**
	 * Sets the value of the '{@link OdxXhtml.ODX#getMULTIPLEECUJOBSPEC <em>MULTIPLEECUJOBSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MULTIPLEECUJOBSPEC</em>' containment reference.
	 * @see #getMULTIPLEECUJOBSPEC()
	 * @generated
	 */
	void setMULTIPLEECUJOBSPEC(MULTIPLEECUJOBSPEC value);

	/**
	 * Returns the value of the '<em><b>MODELVERSION</b></em>' attribute.
	 * The default value is <code>"2.0.1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MODELVERSION</em>' attribute.
	 * @see #isSetMODELVERSION()
	 * @see #unsetMODELVERSION()
	 * @see #setMODELVERSION(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getODX_MODELVERSION()
	 * @model default="2.0.1" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='MODEL-VERSION' namespace='##targetNamespace'"
	 * @generated
	 */
	String getMODELVERSION();

	/**
	 * Sets the value of the '{@link OdxXhtml.ODX#getMODELVERSION <em>MODELVERSION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MODELVERSION</em>' attribute.
	 * @see #isSetMODELVERSION()
	 * @see #unsetMODELVERSION()
	 * @see #getMODELVERSION()
	 * @generated
	 */
	void setMODELVERSION(String value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.ODX#getMODELVERSION <em>MODELVERSION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMODELVERSION()
	 * @see #getMODELVERSION()
	 * @see #setMODELVERSION(String)
	 * @generated
	 */
	void unsetMODELVERSION();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.ODX#getMODELVERSION <em>MODELVERSION</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MODELVERSION</em>' attribute is set.
	 * @see #unsetMODELVERSION()
	 * @see #getMODELVERSION()
	 * @see #setMODELVERSION(String)
	 * @generated
	 */
	boolean isSetMODELVERSION();

} // ODX
