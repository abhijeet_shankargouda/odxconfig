/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLEINFORMATIONS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VEHICLEINFORMATIONS#getVEHICLEINFORMATION <em>VEHICLEINFORMATION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATIONS()
 * @model extendedMetaData="name='VEHICLE-INFORMATIONS' kind='elementOnly'"
 * @generated
 */
public interface VEHICLEINFORMATIONS extends EObject {
	/**
	 * Returns the value of the '<em><b>VEHICLEINFORMATION</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.VEHICLEINFORMATION}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VEHICLEINFORMATION</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFORMATIONS_VEHICLEINFORMATION()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='VEHICLE-INFORMATION' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<VEHICLEINFORMATION> getVEHICLEINFORMATION();

} // VEHICLEINFORMATIONS
