/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUMEMCONNECTORS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUMEMCONNECTORS#getECUMEMCONNECTOR <em>ECUMEMCONNECTOR</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTORS()
 * @model extendedMetaData="name='ECU-MEM-CONNECTORS' kind='elementOnly'"
 * @generated
 */
public interface ECUMEMCONNECTORS extends EObject {
	/**
	 * Returns the value of the '<em><b>ECUMEMCONNECTOR</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ECUMEMCONNECTOR}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUMEMCONNECTOR</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMCONNECTORS_ECUMEMCONNECTOR()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ECU-MEM-CONNECTOR' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ECUMEMCONNECTOR> getECUMEMCONNECTOR();

} // ECUMEMCONNECTORS
