/**
 */
package OdxXhtml;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>ENCODING</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see OdxXhtml.OdxXhtmlPackage#getENCODING()
 * @model extendedMetaData="name='ENCODING'"
 * @generated
 */
public enum ENCODING implements Enumerator {
	/**
	 * The '<em><b>BCDP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BCDP_VALUE
	 * @generated
	 * @ordered
	 */
	BCDP(0, "BCDP", "BCD-P"),

	/**
	 * The '<em><b>BCDUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BCDUP_VALUE
	 * @generated
	 * @ordered
	 */
	BCDUP(1, "BCDUP", "BCD-UP"),

	/**
	 * The '<em><b>1C</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #_1C_VALUE
	 * @generated
	 * @ordered
	 */
	_1C(2, "_1C", "1C"),

	/**
	 * The '<em><b>2C</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #_2C_VALUE
	 * @generated
	 * @ordered
	 */
	_2C(3, "_2C", "2C"),

	/**
	 * The '<em><b>SM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SM_VALUE
	 * @generated
	 * @ordered
	 */
	SM(4, "SM", "SM"),

	/**
	 * The '<em><b>UTF8</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UTF8_VALUE
	 * @generated
	 * @ordered
	 */
	UTF8(5, "UTF8", "UTF-8"),

	/**
	 * The '<em><b>UCS2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UCS2_VALUE
	 * @generated
	 * @ordered
	 */
	UCS2(6, "UCS2", "UCS-2"),

	/**
	 * The '<em><b>ISO88591</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO88591_VALUE
	 * @generated
	 * @ordered
	 */
	ISO88591(7, "ISO88591", "ISO-8859-1"),

	/**
	 * The '<em><b>ISO88592</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO88592_VALUE
	 * @generated
	 * @ordered
	 */
	ISO88592(8, "ISO88592", "ISO-8859-2"),

	/**
	 * The '<em><b>WINDOWS1252</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WINDOWS1252_VALUE
	 * @generated
	 * @ordered
	 */
	WINDOWS1252(9, "WINDOWS1252", "WINDOWS-1252"),

	/**
	 * The '<em><b>NONE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NONE_VALUE
	 * @generated
	 * @ordered
	 */
	NONE(10, "NONE", "NONE");

	/**
	 * The '<em><b>BCDP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BCDP
	 * @model literal="BCD-P"
	 * @generated
	 * @ordered
	 */
	public static final int BCDP_VALUE = 0;

	/**
	 * The '<em><b>BCDUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BCDUP
	 * @model literal="BCD-UP"
	 * @generated
	 * @ordered
	 */
	public static final int BCDUP_VALUE = 1;

	/**
	 * The '<em><b>1C</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #_1C
	 * @model literal="1C"
	 * @generated
	 * @ordered
	 */
	public static final int _1C_VALUE = 2;

	/**
	 * The '<em><b>2C</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #_2C
	 * @model literal="2C"
	 * @generated
	 * @ordered
	 */
	public static final int _2C_VALUE = 3;

	/**
	 * The '<em><b>SM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SM_VALUE = 4;

	/**
	 * The '<em><b>UTF8</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UTF8
	 * @model literal="UTF-8"
	 * @generated
	 * @ordered
	 */
	public static final int UTF8_VALUE = 5;

	/**
	 * The '<em><b>UCS2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UCS2
	 * @model literal="UCS-2"
	 * @generated
	 * @ordered
	 */
	public static final int UCS2_VALUE = 6;

	/**
	 * The '<em><b>ISO88591</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO88591
	 * @model literal="ISO-8859-1"
	 * @generated
	 * @ordered
	 */
	public static final int ISO88591_VALUE = 7;

	/**
	 * The '<em><b>ISO88592</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ISO88592
	 * @model literal="ISO-8859-2"
	 * @generated
	 * @ordered
	 */
	public static final int ISO88592_VALUE = 8;

	/**
	 * The '<em><b>WINDOWS1252</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WINDOWS1252
	 * @model literal="WINDOWS-1252"
	 * @generated
	 * @ordered
	 */
	public static final int WINDOWS1252_VALUE = 9;

	/**
	 * The '<em><b>NONE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NONE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NONE_VALUE = 10;

	/**
	 * An array of all the '<em><b>ENCODING</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ENCODING[] VALUES_ARRAY =
		new ENCODING[] {
			BCDP,
			BCDUP,
			_1C,
			_2C,
			SM,
			UTF8,
			UCS2,
			ISO88591,
			ISO88592,
			WINDOWS1252,
			NONE,
		};

	/**
	 * A public read-only list of all the '<em><b>ENCODING</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ENCODING> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>ENCODING</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENCODING get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENCODING result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENCODING</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENCODING getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ENCODING result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>ENCODING</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ENCODING get(int value) {
		switch (value) {
			case BCDP_VALUE: return BCDP;
			case BCDUP_VALUE: return BCDUP;
			case _1C_VALUE: return _1C;
			case _2C_VALUE: return _2C;
			case SM_VALUE: return SM;
			case UTF8_VALUE: return UTF8;
			case UCS2_VALUE: return UCS2;
			case ISO88591_VALUE: return ISO88591;
			case ISO88592_VALUE: return ISO88592;
			case WINDOWS1252_VALUE: return WINDOWS1252;
			case NONE_VALUE: return NONE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ENCODING(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ENCODING
