/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>POSOFFSET</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.POSOFFSET#getPOSITIVEOFFSET <em>POSITIVEOFFSET</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPOSOFFSET()
 * @model extendedMetaData="name='POS-OFFSET' kind='elementOnly'"
 * @generated
 */
public interface POSOFFSET extends TARGETADDROFFSET {
	/**
	 * Returns the value of the '<em><b>POSITIVEOFFSET</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>POSITIVEOFFSET</em>' attribute.
	 * @see #setPOSITIVEOFFSET(byte[])
	 * @see OdxXhtml.OdxXhtmlPackage#getPOSOFFSET_POSITIVEOFFSET()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.HexBinary" required="true"
	 *        extendedMetaData="kind='element' name='POSITIVE-OFFSET' namespace='##targetNamespace'"
	 * @generated
	 */
	byte[] getPOSITIVEOFFSET();

	/**
	 * Sets the value of the '{@link OdxXhtml.POSOFFSET#getPOSITIVEOFFSET <em>POSITIVEOFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>POSITIVEOFFSET</em>' attribute.
	 * @see #getPOSITIVEOFFSET()
	 * @generated
	 */
	void setPOSITIVEOFFSET(byte[] value);

} // POSOFFSET
