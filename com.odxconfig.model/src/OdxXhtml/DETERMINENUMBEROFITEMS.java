/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DETERMINENUMBEROFITEMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DETERMINENUMBEROFITEMS#getBYTEPOSITION <em>BYTEPOSITION</em>}</li>
 *   <li>{@link OdxXhtml.DETERMINENUMBEROFITEMS#getBITPOSITION <em>BITPOSITION</em>}</li>
 *   <li>{@link OdxXhtml.DETERMINENUMBEROFITEMS#getDATAOBJECTPROPREF <em>DATAOBJECTPROPREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDETERMINENUMBEROFITEMS()
 * @model extendedMetaData="name='DETERMINE-NUMBER-OF-ITEMS' kind='elementOnly'"
 * @generated
 */
public interface DETERMINENUMBEROFITEMS extends EObject {
	/**
	 * Returns the value of the '<em><b>BYTEPOSITION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BYTEPOSITION</em>' attribute.
	 * @see #isSetBYTEPOSITION()
	 * @see #unsetBYTEPOSITION()
	 * @see #setBYTEPOSITION(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getDETERMINENUMBEROFITEMS_BYTEPOSITION()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='BYTE-POSITION' namespace='##targetNamespace'"
	 * @generated
	 */
	long getBYTEPOSITION();

	/**
	 * Sets the value of the '{@link OdxXhtml.DETERMINENUMBEROFITEMS#getBYTEPOSITION <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BYTEPOSITION</em>' attribute.
	 * @see #isSetBYTEPOSITION()
	 * @see #unsetBYTEPOSITION()
	 * @see #getBYTEPOSITION()
	 * @generated
	 */
	void setBYTEPOSITION(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DETERMINENUMBEROFITEMS#getBYTEPOSITION <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBYTEPOSITION()
	 * @see #getBYTEPOSITION()
	 * @see #setBYTEPOSITION(long)
	 * @generated
	 */
	void unsetBYTEPOSITION();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DETERMINENUMBEROFITEMS#getBYTEPOSITION <em>BYTEPOSITION</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BYTEPOSITION</em>' attribute is set.
	 * @see #unsetBYTEPOSITION()
	 * @see #getBYTEPOSITION()
	 * @see #setBYTEPOSITION(long)
	 * @generated
	 */
	boolean isSetBYTEPOSITION();

	/**
	 * Returns the value of the '<em><b>BITPOSITION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BITPOSITION</em>' attribute.
	 * @see #isSetBITPOSITION()
	 * @see #unsetBITPOSITION()
	 * @see #setBITPOSITION(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getDETERMINENUMBEROFITEMS_BITPOSITION()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt"
	 *        extendedMetaData="kind='element' name='BIT-POSITION' namespace='##targetNamespace'"
	 * @generated
	 */
	long getBITPOSITION();

	/**
	 * Sets the value of the '{@link OdxXhtml.DETERMINENUMBEROFITEMS#getBITPOSITION <em>BITPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BITPOSITION</em>' attribute.
	 * @see #isSetBITPOSITION()
	 * @see #unsetBITPOSITION()
	 * @see #getBITPOSITION()
	 * @generated
	 */
	void setBITPOSITION(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DETERMINENUMBEROFITEMS#getBITPOSITION <em>BITPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBITPOSITION()
	 * @see #getBITPOSITION()
	 * @see #setBITPOSITION(long)
	 * @generated
	 */
	void unsetBITPOSITION();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DETERMINENUMBEROFITEMS#getBITPOSITION <em>BITPOSITION</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>BITPOSITION</em>' attribute is set.
	 * @see #unsetBITPOSITION()
	 * @see #getBITPOSITION()
	 * @see #setBITPOSITION(long)
	 * @generated
	 */
	boolean isSetBITPOSITION();

	/**
	 * Returns the value of the '<em><b>DATAOBJECTPROPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATAOBJECTPROPREF</em>' containment reference.
	 * @see #setDATAOBJECTPROPREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getDETERMINENUMBEROFITEMS_DATAOBJECTPROPREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATA-OBJECT-PROP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDATAOBJECTPROPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DETERMINENUMBEROFITEMS#getDATAOBJECTPROPREF <em>DATAOBJECTPROPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATAOBJECTPROPREF</em>' containment reference.
	 * @see #getDATAOBJECTPROPREF()
	 * @generated
	 */
	void setDATAOBJECTPROPREF(ODXLINK value);

} // DETERMINENUMBEROFITEMS
