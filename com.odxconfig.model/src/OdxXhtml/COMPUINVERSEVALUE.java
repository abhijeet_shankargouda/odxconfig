/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPUINVERSEVALUE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPUINVERSEVALUE#getV <em>V</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUINVERSEVALUE()
 * @model extendedMetaData="name='COMPU-INVERSE-VALUE' kind='elementOnly'"
 * @generated
 */
public interface COMPUINVERSEVALUE extends EObject {
	/**
	 * Returns the value of the '<em><b>V</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>V</em>' containment reference.
	 * @see #setV(V)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUINVERSEVALUE_V()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='V' namespace='##targetNamespace'"
	 * @generated
	 */
	V getV();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUINVERSEVALUE#getV <em>V</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>V</em>' containment reference.
	 * @see #getV()
	 * @generated
	 */
	void setV(V value);

} // COMPUINVERSEVALUE
