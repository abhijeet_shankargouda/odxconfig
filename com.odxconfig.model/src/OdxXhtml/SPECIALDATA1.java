/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SPECIALDATA1</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSPECIALDATA1()
 * @model extendedMetaData="name='SPECIAL-DATA' kind='empty'"
 * @generated
 */
public interface SPECIALDATA1 extends EObject {
} // SPECIALDATA1
