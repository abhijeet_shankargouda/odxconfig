/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROJECTIDENT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PROJECTIDENT#getNAME <em>NAME</em>}</li>
 *   <li>{@link OdxXhtml.PROJECTIDENT#getVALUE <em>VALUE</em>}</li>
 *   <li>{@link OdxXhtml.PROJECTIDENT#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTIDENT()
 * @model extendedMetaData="name='PROJECT-IDENT' kind='elementOnly'"
 * @generated
 */
public interface PROJECTIDENT extends EObject {
	/**
	 * Returns the value of the '<em><b>NAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NAME</em>' attribute.
	 * @see #setNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTIDENT_NAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROJECTIDENT#getNAME <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NAME</em>' attribute.
	 * @see #getNAME()
	 * @generated
	 */
	void setNAME(String value);

	/**
	 * Returns the value of the '<em><b>VALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VALUE</em>' attribute.
	 * @see #setVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTIDENT_VALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROJECTIDENT#getVALUE <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VALUE</em>' attribute.
	 * @see #getVALUE()
	 * @generated
	 */
	void setVALUE(String value);

	/**
	 * Returns the value of the '<em><b>TYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.PROJIDENTTYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TYPE</em>' attribute.
	 * @see OdxXhtml.PROJIDENTTYPE
	 * @see #isSetTYPE()
	 * @see #unsetTYPE()
	 * @see #setTYPE(PROJIDENTTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROJECTIDENT_TYPE()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	PROJIDENTTYPE getTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROJECTIDENT#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TYPE</em>' attribute.
	 * @see OdxXhtml.PROJIDENTTYPE
	 * @see #isSetTYPE()
	 * @see #unsetTYPE()
	 * @see #getTYPE()
	 * @generated
	 */
	void setTYPE(PROJIDENTTYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.PROJECTIDENT#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTYPE()
	 * @see #getTYPE()
	 * @see #setTYPE(PROJIDENTTYPE)
	 * @generated
	 */
	void unsetTYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.PROJECTIDENT#getTYPE <em>TYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TYPE</em>' attribute is set.
	 * @see #unsetTYPE()
	 * @see #getTYPE()
	 * @see #setTYPE(PROJIDENTTYPE)
	 * @generated
	 */
	boolean isSetTYPE();

} // PROJECTIDENT
