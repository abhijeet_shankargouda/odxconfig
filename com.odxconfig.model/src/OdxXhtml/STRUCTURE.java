/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>STRUCTURE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.STRUCTURE#isISVISIBLE <em>ISVISIBLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSTRUCTURE()
 * @model extendedMetaData="name='STRUCTURE' kind='elementOnly'"
 * @generated
 */
public interface STRUCTURE extends BASICSTRUCTURE {
	/**
	 * Returns the value of the '<em><b>ISVISIBLE</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISVISIBLE</em>' attribute.
	 * @see #isSetISVISIBLE()
	 * @see #unsetISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getSTRUCTURE_ISVISIBLE()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-VISIBLE' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISVISIBLE();

	/**
	 * Sets the value of the '{@link OdxXhtml.STRUCTURE#isISVISIBLE <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISVISIBLE</em>' attribute.
	 * @see #isSetISVISIBLE()
	 * @see #unsetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @generated
	 */
	void setISVISIBLE(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.STRUCTURE#isISVISIBLE <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @generated
	 */
	void unsetISVISIBLE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.STRUCTURE#isISVISIBLE <em>ISVISIBLE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISVISIBLE</em>' attribute is set.
	 * @see #unsetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @generated
	 */
	boolean isSetISVISIBLE();

} // STRUCTURE
