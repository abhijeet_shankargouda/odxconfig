/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ENVDATAS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ENVDATAS#getENVDATAPROXY <em>ENVDATAPROXY</em>}</li>
 *   <li>{@link OdxXhtml.ENVDATAS#getENVDATAREF <em>ENVDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.ENVDATAS#getENVDATA <em>ENVDATA</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getENVDATAS()
 * @model extendedMetaData="name='ENV-DATAS' kind='elementOnly'"
 * @generated
 */
public interface ENVDATAS extends EObject {
	/**
	 * Returns the value of the '<em><b>ENVDATAPROXY</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENVDATAPROXY</em>' attribute list.
	 * @see OdxXhtml.OdxXhtmlPackage#getENVDATAS_ENVDATAPROXY()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='ENVDATAPROXY:0'"
	 * @generated
	 */
	FeatureMap getENVDATAPROXY();

	/**
	 * Returns the value of the '<em><b>ENVDATAREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENVDATAREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getENVDATAS_ENVDATAREF()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ENV-DATA-REF' namespace='##targetNamespace' group='ENVDATAPROXY:0'"
	 * @generated
	 */
	EList<ODXLINK> getENVDATAREF();

	/**
	 * Returns the value of the '<em><b>ENVDATA</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ENVDATA}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENVDATA</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getENVDATAS_ENVDATA()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ENV-DATA' namespace='##targetNamespace' group='ENVDATAPROXY:0'"
	 * @generated
	 */
	EList<ENVDATA> getENVDATA();

} // ENVDATAS
