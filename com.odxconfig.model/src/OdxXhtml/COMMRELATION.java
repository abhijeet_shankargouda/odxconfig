/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMMRELATION</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMMRELATION#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.COMMRELATION#getRELATIONTYPE <em>RELATIONTYPE</em>}</li>
 *   <li>{@link OdxXhtml.COMMRELATION#getDIAGCOMMREF <em>DIAGCOMMREF</em>}</li>
 *   <li>{@link OdxXhtml.COMMRELATION#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.COMMRELATION#getINPARAMIFSNREF <em>INPARAMIFSNREF</em>}</li>
 *   <li>{@link OdxXhtml.COMMRELATION#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}</li>
 *   <li>{@link OdxXhtml.COMMRELATION#getVALUETYPE <em>VALUETYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATION()
 * @model extendedMetaData="name='COMM-RELATION' kind='elementOnly'"
 * @generated
 */
public interface COMMRELATION extends EObject {
	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATION_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMMRELATION#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>RELATIONTYPE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RELATIONTYPE</em>' attribute.
	 * @see #setRELATIONTYPE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATION_RELATIONTYPE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='RELATION-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getRELATIONTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMMRELATION#getRELATIONTYPE <em>RELATIONTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RELATIONTYPE</em>' attribute.
	 * @see #getRELATIONTYPE()
	 * @generated
	 */
	void setRELATIONTYPE(String value);

	/**
	 * Returns the value of the '<em><b>DIAGCOMMREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMREF</em>' containment reference.
	 * @see #setDIAGCOMMREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATION_DIAGCOMMREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMM-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDIAGCOMMREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMMRELATION#getDIAGCOMMREF <em>DIAGCOMMREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCOMMREF</em>' containment reference.
	 * @see #getDIAGCOMMREF()
	 * @generated
	 */
	void setDIAGCOMMREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>DIAGCOMMSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #setDIAGCOMMSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATION_DIAGCOMMSNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMM-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDIAGCOMMSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMMRELATION#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 */
	void setDIAGCOMMSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>INPARAMIFSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INPARAMIFSNREF</em>' containment reference.
	 * @see #setINPARAMIFSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATION_INPARAMIFSNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='IN-PARAM-IF-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getINPARAMIFSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMMRELATION#getINPARAMIFSNREF <em>INPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>INPARAMIFSNREF</em>' containment reference.
	 * @see #getINPARAMIFSNREF()
	 * @generated
	 */
	void setINPARAMIFSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>OUTPARAMIFSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OUTPARAMIFSNREF</em>' containment reference.
	 * @see #setOUTPARAMIFSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATION_OUTPARAMIFSNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OUT-PARAM-IF-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getOUTPARAMIFSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMMRELATION#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OUTPARAMIFSNREF</em>' containment reference.
	 * @see #getOUTPARAMIFSNREF()
	 * @generated
	 */
	void setOUTPARAMIFSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>VALUETYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.COMMRELATIONVALUETYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VALUETYPE</em>' attribute.
	 * @see OdxXhtml.COMMRELATIONVALUETYPE
	 * @see #isSetVALUETYPE()
	 * @see #unsetVALUETYPE()
	 * @see #setVALUETYPE(COMMRELATIONVALUETYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMMRELATION_VALUETYPE()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='VALUE-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	COMMRELATIONVALUETYPE getVALUETYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMMRELATION#getVALUETYPE <em>VALUETYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VALUETYPE</em>' attribute.
	 * @see OdxXhtml.COMMRELATIONVALUETYPE
	 * @see #isSetVALUETYPE()
	 * @see #unsetVALUETYPE()
	 * @see #getVALUETYPE()
	 * @generated
	 */
	void setVALUETYPE(COMMRELATIONVALUETYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.COMMRELATION#getVALUETYPE <em>VALUETYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVALUETYPE()
	 * @see #getVALUETYPE()
	 * @see #setVALUETYPE(COMMRELATIONVALUETYPE)
	 * @generated
	 */
	void unsetVALUETYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.COMMRELATION#getVALUETYPE <em>VALUETYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>VALUETYPE</em>' attribute is set.
	 * @see #unsetVALUETYPE()
	 * @see #getVALUETYPE()
	 * @see #setVALUETYPE(COMMRELATIONVALUETYPE)
	 * @generated
	 */
	boolean isSetVALUETYPE();

} // COMMRELATION
