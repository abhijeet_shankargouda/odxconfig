/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPUMETHOD</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPUMETHOD#getCATEGORY <em>CATEGORY</em>}</li>
 *   <li>{@link OdxXhtml.COMPUMETHOD#getCOMPUINTERNALTOPHYS <em>COMPUINTERNALTOPHYS</em>}</li>
 *   <li>{@link OdxXhtml.COMPUMETHOD#getCOMPUPHYSTOINTERNAL <em>COMPUPHYSTOINTERNAL</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUMETHOD()
 * @model extendedMetaData="name='COMPU-METHOD' kind='elementOnly'"
 * @generated
 */
public interface COMPUMETHOD extends EObject {
	/**
	 * Returns the value of the '<em><b>CATEGORY</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.COMPUCATEGORY}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CATEGORY</em>' attribute.
	 * @see OdxXhtml.COMPUCATEGORY
	 * @see #isSetCATEGORY()
	 * @see #unsetCATEGORY()
	 * @see #setCATEGORY(COMPUCATEGORY)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUMETHOD_CATEGORY()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='element' name='CATEGORY' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUCATEGORY getCATEGORY();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUMETHOD#getCATEGORY <em>CATEGORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CATEGORY</em>' attribute.
	 * @see OdxXhtml.COMPUCATEGORY
	 * @see #isSetCATEGORY()
	 * @see #unsetCATEGORY()
	 * @see #getCATEGORY()
	 * @generated
	 */
	void setCATEGORY(COMPUCATEGORY value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.COMPUMETHOD#getCATEGORY <em>CATEGORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCATEGORY()
	 * @see #getCATEGORY()
	 * @see #setCATEGORY(COMPUCATEGORY)
	 * @generated
	 */
	void unsetCATEGORY();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.COMPUMETHOD#getCATEGORY <em>CATEGORY</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>CATEGORY</em>' attribute is set.
	 * @see #unsetCATEGORY()
	 * @see #getCATEGORY()
	 * @see #setCATEGORY(COMPUCATEGORY)
	 * @generated
	 */
	boolean isSetCATEGORY();

	/**
	 * Returns the value of the '<em><b>COMPUINTERNALTOPHYS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUINTERNALTOPHYS</em>' containment reference.
	 * @see #setCOMPUINTERNALTOPHYS(COMPUINTERNALTOPHYS)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUMETHOD_COMPUINTERNALTOPHYS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPU-INTERNAL-TO-PHYS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUINTERNALTOPHYS getCOMPUINTERNALTOPHYS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUMETHOD#getCOMPUINTERNALTOPHYS <em>COMPUINTERNALTOPHYS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUINTERNALTOPHYS</em>' containment reference.
	 * @see #getCOMPUINTERNALTOPHYS()
	 * @generated
	 */
	void setCOMPUINTERNALTOPHYS(COMPUINTERNALTOPHYS value);

	/**
	 * Returns the value of the '<em><b>COMPUPHYSTOINTERNAL</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUPHYSTOINTERNAL</em>' containment reference.
	 * @see #setCOMPUPHYSTOINTERNAL(COMPUPHYSTOINTERNAL)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUMETHOD_COMPUPHYSTOINTERNAL()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPU-PHYS-TO-INTERNAL' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUPHYSTOINTERNAL getCOMPUPHYSTOINTERNAL();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUMETHOD#getCOMPUPHYSTOINTERNAL <em>COMPUPHYSTOINTERNAL</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUPHYSTOINTERNAL</em>' containment reference.
	 * @see #getCOMPUPHYSTOINTERNAL()
	 * @generated
	 */
	void setCOMPUPHYSTOINTERNAL(COMPUPHYSTOINTERNAL value);

} // COMPUMETHOD
