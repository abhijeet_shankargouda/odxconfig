/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DIAGSERVICE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DIAGSERVICE#getREQUESTREF <em>REQUESTREF</em>}</li>
 *   <li>{@link OdxXhtml.DIAGSERVICE#getPOSRESPONSEREFS <em>POSRESPONSEREFS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGSERVICE#getNEGRESPONSEREFS <em>NEGRESPONSEREFS</em>}</li>
 *   <li>{@link OdxXhtml.DIAGSERVICE#getADDRESSING <em>ADDRESSING</em>}</li>
 *   <li>{@link OdxXhtml.DIAGSERVICE#isISCYCLIC <em>ISCYCLIC</em>}</li>
 *   <li>{@link OdxXhtml.DIAGSERVICE#isISMULTIPLE <em>ISMULTIPLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDIAGSERVICE()
 * @model extendedMetaData="name='DIAG-SERVICE' kind='elementOnly'"
 * @generated
 */
public interface DIAGSERVICE extends DIAGCOMM {
	/**
	 * Returns the value of the '<em><b>REQUESTREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REQUESTREF</em>' containment reference.
	 * @see #setREQUESTREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGSERVICE_REQUESTREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='REQUEST-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getREQUESTREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGSERVICE#getREQUESTREF <em>REQUESTREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REQUESTREF</em>' containment reference.
	 * @see #getREQUESTREF()
	 * @generated
	 */
	void setREQUESTREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>POSRESPONSEREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>POSRESPONSEREFS</em>' containment reference.
	 * @see #setPOSRESPONSEREFS(POSRESPONSEREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGSERVICE_POSRESPONSEREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='POS-RESPONSE-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	POSRESPONSEREFS getPOSRESPONSEREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGSERVICE#getPOSRESPONSEREFS <em>POSRESPONSEREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>POSRESPONSEREFS</em>' containment reference.
	 * @see #getPOSRESPONSEREFS()
	 * @generated
	 */
	void setPOSRESPONSEREFS(POSRESPONSEREFS value);

	/**
	 * Returns the value of the '<em><b>NEGRESPONSEREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NEGRESPONSEREFS</em>' containment reference.
	 * @see #setNEGRESPONSEREFS(NEGRESPONSEREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGSERVICE_NEGRESPONSEREFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='NEG-RESPONSE-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	NEGRESPONSEREFS getNEGRESPONSEREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGSERVICE#getNEGRESPONSEREFS <em>NEGRESPONSEREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NEGRESPONSEREFS</em>' containment reference.
	 * @see #getNEGRESPONSEREFS()
	 * @generated
	 */
	void setNEGRESPONSEREFS(NEGRESPONSEREFS value);

	/**
	 * Returns the value of the '<em><b>ADDRESSING</b></em>' attribute.
	 * The default value is <code>"PHYSICAL"</code>.
	 * The literals are from the enumeration {@link OdxXhtml.ADDRESSING}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADDRESSING</em>' attribute.
	 * @see OdxXhtml.ADDRESSING
	 * @see #isSetADDRESSING()
	 * @see #unsetADDRESSING()
	 * @see #setADDRESSING(ADDRESSING)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGSERVICE_ADDRESSING()
	 * @model default="PHYSICAL" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='ADDRESSING' namespace='##targetNamespace'"
	 * @generated
	 */
	ADDRESSING getADDRESSING();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGSERVICE#getADDRESSING <em>ADDRESSING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADDRESSING</em>' attribute.
	 * @see OdxXhtml.ADDRESSING
	 * @see #isSetADDRESSING()
	 * @see #unsetADDRESSING()
	 * @see #getADDRESSING()
	 * @generated
	 */
	void setADDRESSING(ADDRESSING value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGSERVICE#getADDRESSING <em>ADDRESSING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetADDRESSING()
	 * @see #getADDRESSING()
	 * @see #setADDRESSING(ADDRESSING)
	 * @generated
	 */
	void unsetADDRESSING();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGSERVICE#getADDRESSING <em>ADDRESSING</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ADDRESSING</em>' attribute is set.
	 * @see #unsetADDRESSING()
	 * @see #getADDRESSING()
	 * @see #setADDRESSING(ADDRESSING)
	 * @generated
	 */
	boolean isSetADDRESSING();

	/**
	 * Returns the value of the '<em><b>ISCYCLIC</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISCYCLIC</em>' attribute.
	 * @see #isSetISCYCLIC()
	 * @see #unsetISCYCLIC()
	 * @see #setISCYCLIC(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGSERVICE_ISCYCLIC()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-CYCLIC' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISCYCLIC();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGSERVICE#isISCYCLIC <em>ISCYCLIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISCYCLIC</em>' attribute.
	 * @see #isSetISCYCLIC()
	 * @see #unsetISCYCLIC()
	 * @see #isISCYCLIC()
	 * @generated
	 */
	void setISCYCLIC(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGSERVICE#isISCYCLIC <em>ISCYCLIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISCYCLIC()
	 * @see #isISCYCLIC()
	 * @see #setISCYCLIC(boolean)
	 * @generated
	 */
	void unsetISCYCLIC();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGSERVICE#isISCYCLIC <em>ISCYCLIC</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISCYCLIC</em>' attribute is set.
	 * @see #unsetISCYCLIC()
	 * @see #isISCYCLIC()
	 * @see #setISCYCLIC(boolean)
	 * @generated
	 */
	boolean isSetISCYCLIC();

	/**
	 * Returns the value of the '<em><b>ISMULTIPLE</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISMULTIPLE</em>' attribute.
	 * @see #isSetISMULTIPLE()
	 * @see #unsetISMULTIPLE()
	 * @see #setISMULTIPLE(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getDIAGSERVICE_ISMULTIPLE()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-MULTIPLE' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISMULTIPLE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DIAGSERVICE#isISMULTIPLE <em>ISMULTIPLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISMULTIPLE</em>' attribute.
	 * @see #isSetISMULTIPLE()
	 * @see #unsetISMULTIPLE()
	 * @see #isISMULTIPLE()
	 * @generated
	 */
	void setISMULTIPLE(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DIAGSERVICE#isISMULTIPLE <em>ISMULTIPLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISMULTIPLE()
	 * @see #isISMULTIPLE()
	 * @see #setISMULTIPLE(boolean)
	 * @generated
	 */
	void unsetISMULTIPLE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DIAGSERVICE#isISMULTIPLE <em>ISMULTIPLE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISMULTIPLE</em>' attribute is set.
	 * @see #unsetISMULTIPLE()
	 * @see #isISMULTIPLE()
	 * @see #setISMULTIPLE(boolean)
	 * @generated
	 */
	boolean isSetISMULTIPLE();

} // DIAGSERVICE
