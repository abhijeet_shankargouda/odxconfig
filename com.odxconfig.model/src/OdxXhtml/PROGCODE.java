/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROGCODE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PROGCODE#getCODEFILE <em>CODEFILE</em>}</li>
 *   <li>{@link OdxXhtml.PROGCODE#getENCRYPTION <em>ENCRYPTION</em>}</li>
 *   <li>{@link OdxXhtml.PROGCODE#getSYNTAX <em>SYNTAX</em>}</li>
 *   <li>{@link OdxXhtml.PROGCODE#getREVISION <em>REVISION</em>}</li>
 *   <li>{@link OdxXhtml.PROGCODE#getENTRYPOINT <em>ENTRYPOINT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROGCODE()
 * @model extendedMetaData="name='PROG-CODE' kind='elementOnly'"
 * @generated
 */
public interface PROGCODE extends EObject {
	/**
	 * Returns the value of the '<em><b>CODEFILE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CODEFILE</em>' attribute.
	 * @see #setCODEFILE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROGCODE_CODEFILE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='CODE-FILE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCODEFILE();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROGCODE#getCODEFILE <em>CODEFILE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CODEFILE</em>' attribute.
	 * @see #getCODEFILE()
	 * @generated
	 */
	void setCODEFILE(String value);

	/**
	 * Returns the value of the '<em><b>ENCRYPTION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENCRYPTION</em>' attribute.
	 * @see #setENCRYPTION(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROGCODE_ENCRYPTION()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='ENCRYPTION' namespace='##targetNamespace'"
	 * @generated
	 */
	String getENCRYPTION();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROGCODE#getENCRYPTION <em>ENCRYPTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ENCRYPTION</em>' attribute.
	 * @see #getENCRYPTION()
	 * @generated
	 */
	void setENCRYPTION(String value);

	/**
	 * Returns the value of the '<em><b>SYNTAX</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SYNTAX</em>' attribute.
	 * @see #setSYNTAX(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROGCODE_SYNTAX()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SYNTAX' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSYNTAX();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROGCODE#getSYNTAX <em>SYNTAX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SYNTAX</em>' attribute.
	 * @see #getSYNTAX()
	 * @generated
	 */
	void setSYNTAX(String value);

	/**
	 * Returns the value of the '<em><b>REVISION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REVISION</em>' attribute.
	 * @see #setREVISION(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROGCODE_REVISION()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='REVISION' namespace='##targetNamespace'"
	 * @generated
	 */
	String getREVISION();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROGCODE#getREVISION <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REVISION</em>' attribute.
	 * @see #getREVISION()
	 * @generated
	 */
	void setREVISION(String value);

	/**
	 * Returns the value of the '<em><b>ENTRYPOINT</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ENTRYPOINT</em>' attribute.
	 * @see #setENTRYPOINT(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getPROGCODE_ENTRYPOINT()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='ENTRYPOINT' namespace='##targetNamespace'"
	 * @generated
	 */
	String getENTRYPOINT();

	/**
	 * Sets the value of the '{@link OdxXhtml.PROGCODE#getENTRYPOINT <em>ENTRYPOINT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ENTRYPOINT</em>' attribute.
	 * @see #getENTRYPOINT()
	 * @generated
	 */
	void setENTRYPOINT(String value);

} // PROGCODE
