/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROJECTIDENTS;
import OdxXhtml.PROJECTINFO;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PROJECTINFO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PROJECTINFOImpl#getECUFAMILYDESC <em>ECUFAMILYDESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROJECTINFOImpl#getMODIFICATIONLETTER <em>MODIFICATIONLETTER</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROJECTINFOImpl#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROJECTINFOImpl#getPROJECTIDENTS <em>PROJECTIDENTS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PROJECTINFOImpl extends MinimalEObjectImpl.Container implements PROJECTINFO {
	/**
	 * The default value of the '{@link #getECUFAMILYDESC() <em>ECUFAMILYDESC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUFAMILYDESC()
	 * @generated
	 * @ordered
	 */
	protected static final String ECUFAMILYDESC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getECUFAMILYDESC() <em>ECUFAMILYDESC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUFAMILYDESC()
	 * @generated
	 * @ordered
	 */
	protected String eCUFAMILYDESC = ECUFAMILYDESC_EDEFAULT;

	/**
	 * The default value of the '{@link #getMODIFICATIONLETTER() <em>MODIFICATIONLETTER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMODIFICATIONLETTER()
	 * @generated
	 * @ordered
	 */
	protected static final String MODIFICATIONLETTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMODIFICATIONLETTER() <em>MODIFICATIONLETTER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMODIFICATIONLETTER()
	 * @generated
	 * @ordered
	 */
	protected String mODIFICATIONLETTER = MODIFICATIONLETTER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCOMPANYDATAREF() <em>COMPANYDATAREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDATAREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK cOMPANYDATAREF;

	/**
	 * The cached value of the '{@link #getPROJECTIDENTS() <em>PROJECTIDENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROJECTIDENTS()
	 * @generated
	 * @ordered
	 */
	protected PROJECTIDENTS pROJECTIDENTS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PROJECTINFOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPROJECTINFO();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getECUFAMILYDESC() {
		return eCUFAMILYDESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setECUFAMILYDESC(String newECUFAMILYDESC) {
		String oldECUFAMILYDESC = eCUFAMILYDESC;
		eCUFAMILYDESC = newECUFAMILYDESC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROJECTINFO__ECUFAMILYDESC, oldECUFAMILYDESC, eCUFAMILYDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMODIFICATIONLETTER() {
		return mODIFICATIONLETTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMODIFICATIONLETTER(String newMODIFICATIONLETTER) {
		String oldMODIFICATIONLETTER = mODIFICATIONLETTER;
		mODIFICATIONLETTER = newMODIFICATIONLETTER;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROJECTINFO__MODIFICATIONLETTER, oldMODIFICATIONLETTER, mODIFICATIONLETTER));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getCOMPANYDATAREF() {
		return cOMPANYDATAREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYDATAREF(ODXLINK newCOMPANYDATAREF, NotificationChain msgs) {
		ODXLINK oldCOMPANYDATAREF = cOMPANYDATAREF;
		cOMPANYDATAREF = newCOMPANYDATAREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROJECTINFO__COMPANYDATAREF, oldCOMPANYDATAREF, newCOMPANYDATAREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYDATAREF(ODXLINK newCOMPANYDATAREF) {
		if (newCOMPANYDATAREF != cOMPANYDATAREF) {
			NotificationChain msgs = null;
			if (cOMPANYDATAREF != null)
				msgs = ((InternalEObject)cOMPANYDATAREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROJECTINFO__COMPANYDATAREF, null, msgs);
			if (newCOMPANYDATAREF != null)
				msgs = ((InternalEObject)newCOMPANYDATAREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROJECTINFO__COMPANYDATAREF, null, msgs);
			msgs = basicSetCOMPANYDATAREF(newCOMPANYDATAREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROJECTINFO__COMPANYDATAREF, newCOMPANYDATAREF, newCOMPANYDATAREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROJECTIDENTS getPROJECTIDENTS() {
		return pROJECTIDENTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROJECTIDENTS(PROJECTIDENTS newPROJECTIDENTS, NotificationChain msgs) {
		PROJECTIDENTS oldPROJECTIDENTS = pROJECTIDENTS;
		pROJECTIDENTS = newPROJECTIDENTS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROJECTINFO__PROJECTIDENTS, oldPROJECTIDENTS, newPROJECTIDENTS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROJECTIDENTS(PROJECTIDENTS newPROJECTIDENTS) {
		if (newPROJECTIDENTS != pROJECTIDENTS) {
			NotificationChain msgs = null;
			if (pROJECTIDENTS != null)
				msgs = ((InternalEObject)pROJECTIDENTS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROJECTINFO__PROJECTIDENTS, null, msgs);
			if (newPROJECTIDENTS != null)
				msgs = ((InternalEObject)newPROJECTIDENTS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROJECTINFO__PROJECTIDENTS, null, msgs);
			msgs = basicSetPROJECTIDENTS(newPROJECTIDENTS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROJECTINFO__PROJECTIDENTS, newPROJECTIDENTS, newPROJECTIDENTS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFO__COMPANYDATAREF:
				return basicSetCOMPANYDATAREF(null, msgs);
			case OdxXhtmlPackage.PROJECTINFO__PROJECTIDENTS:
				return basicSetPROJECTIDENTS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFO__ECUFAMILYDESC:
				return getECUFAMILYDESC();
			case OdxXhtmlPackage.PROJECTINFO__MODIFICATIONLETTER:
				return getMODIFICATIONLETTER();
			case OdxXhtmlPackage.PROJECTINFO__COMPANYDATAREF:
				return getCOMPANYDATAREF();
			case OdxXhtmlPackage.PROJECTINFO__PROJECTIDENTS:
				return getPROJECTIDENTS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFO__ECUFAMILYDESC:
				setECUFAMILYDESC((String)newValue);
				return;
			case OdxXhtmlPackage.PROJECTINFO__MODIFICATIONLETTER:
				setMODIFICATIONLETTER((String)newValue);
				return;
			case OdxXhtmlPackage.PROJECTINFO__COMPANYDATAREF:
				setCOMPANYDATAREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.PROJECTINFO__PROJECTIDENTS:
				setPROJECTIDENTS((PROJECTIDENTS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFO__ECUFAMILYDESC:
				setECUFAMILYDESC(ECUFAMILYDESC_EDEFAULT);
				return;
			case OdxXhtmlPackage.PROJECTINFO__MODIFICATIONLETTER:
				setMODIFICATIONLETTER(MODIFICATIONLETTER_EDEFAULT);
				return;
			case OdxXhtmlPackage.PROJECTINFO__COMPANYDATAREF:
				setCOMPANYDATAREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.PROJECTINFO__PROJECTIDENTS:
				setPROJECTIDENTS((PROJECTIDENTS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFO__ECUFAMILYDESC:
				return ECUFAMILYDESC_EDEFAULT == null ? eCUFAMILYDESC != null : !ECUFAMILYDESC_EDEFAULT.equals(eCUFAMILYDESC);
			case OdxXhtmlPackage.PROJECTINFO__MODIFICATIONLETTER:
				return MODIFICATIONLETTER_EDEFAULT == null ? mODIFICATIONLETTER != null : !MODIFICATIONLETTER_EDEFAULT.equals(mODIFICATIONLETTER);
			case OdxXhtmlPackage.PROJECTINFO__COMPANYDATAREF:
				return cOMPANYDATAREF != null;
			case OdxXhtmlPackage.PROJECTINFO__PROJECTIDENTS:
				return pROJECTIDENTS != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (eCUFAMILYDESC: ");
		result.append(eCUFAMILYDESC);
		result.append(", mODIFICATIONLETTER: ");
		result.append(mODIFICATIONLETTER);
		result.append(')');
		return result.toString();
	}

} //PROJECTINFOImpl
