/**
 */
package OdxXhtml.impl;

import OdxXhtml.AUTMETHOD;
import OdxXhtml.AUTMETHODS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AUTMETHODS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.AUTMETHODSImpl#getAUTMETHOD <em>AUTMETHOD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AUTMETHODSImpl extends MinimalEObjectImpl.Container implements AUTMETHODS {
	/**
	 * The cached value of the '{@link #getAUTMETHOD() <em>AUTMETHOD</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUTMETHOD()
	 * @generated
	 * @ordered
	 */
	protected EList<AUTMETHOD> aUTMETHOD;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AUTMETHODSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getAUTMETHODS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AUTMETHOD> getAUTMETHOD() {
		if (aUTMETHOD == null) {
			aUTMETHOD = new EObjectContainmentEList<AUTMETHOD>(AUTMETHOD.class, this, OdxXhtmlPackage.AUTMETHODS__AUTMETHOD);
		}
		return aUTMETHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.AUTMETHODS__AUTMETHOD:
				return ((InternalEList<?>)getAUTMETHOD()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.AUTMETHODS__AUTMETHOD:
				return getAUTMETHOD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.AUTMETHODS__AUTMETHOD:
				getAUTMETHOD().clear();
				getAUTMETHOD().addAll((Collection<? extends AUTMETHOD>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.AUTMETHODS__AUTMETHOD:
				getAUTMETHOD().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.AUTMETHODS__AUTMETHOD:
				return aUTMETHOD != null && !aUTMETHOD.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AUTMETHODSImpl
