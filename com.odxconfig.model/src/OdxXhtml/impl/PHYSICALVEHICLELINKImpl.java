/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.LINKCOMPARAMREFS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSICALLINKTYPE;
import OdxXhtml.PHYSICALVEHICLELINK;
import OdxXhtml.TEXT;
import OdxXhtml.VEHICLECONNECTORPINREFS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PHYSICALVEHICLELINK</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PHYSICALVEHICLELINKImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALVEHICLELINKImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALVEHICLELINKImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALVEHICLELINKImpl#getVEHICLECONNECTORPINREFS <em>VEHICLECONNECTORPINREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALVEHICLELINKImpl#getLINKCOMPARAMREFS <em>LINKCOMPARAMREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALVEHICLELINKImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALVEHICLELINKImpl#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALVEHICLELINKImpl#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PHYSICALVEHICLELINKImpl extends MinimalEObjectImpl.Container implements PHYSICALVEHICLELINK {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getVEHICLECONNECTORPINREFS() <em>VEHICLECONNECTORPINREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVEHICLECONNECTORPINREFS()
	 * @generated
	 * @ordered
	 */
	protected VEHICLECONNECTORPINREFS vEHICLECONNECTORPINREFS;

	/**
	 * The cached value of the '{@link #getLINKCOMPARAMREFS() <em>LINKCOMPARAMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLINKCOMPARAMREFS()
	 * @generated
	 * @ordered
	 */
	protected LINKCOMPARAMREFS lINKCOMPARAMREFS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * The default value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final PHYSICALLINKTYPE TYPE_EDEFAULT = PHYSICALLINKTYPE.ISO118982DWCAN;

	/**
	 * The cached value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected PHYSICALLINKTYPE tYPE = TYPE_EDEFAULT;

	/**
	 * This is true if the TYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tYPEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PHYSICALVEHICLELINKImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPHYSICALVEHICLELINK();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALVEHICLELINK__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALVEHICLELINK__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALVEHICLELINK__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALVEHICLELINK__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLECONNECTORPINREFS getVEHICLECONNECTORPINREFS() {
		return vEHICLECONNECTORPINREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVEHICLECONNECTORPINREFS(VEHICLECONNECTORPINREFS newVEHICLECONNECTORPINREFS, NotificationChain msgs) {
		VEHICLECONNECTORPINREFS oldVEHICLECONNECTORPINREFS = vEHICLECONNECTORPINREFS;
		vEHICLECONNECTORPINREFS = newVEHICLECONNECTORPINREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__VEHICLECONNECTORPINREFS, oldVEHICLECONNECTORPINREFS, newVEHICLECONNECTORPINREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVEHICLECONNECTORPINREFS(VEHICLECONNECTORPINREFS newVEHICLECONNECTORPINREFS) {
		if (newVEHICLECONNECTORPINREFS != vEHICLECONNECTORPINREFS) {
			NotificationChain msgs = null;
			if (vEHICLECONNECTORPINREFS != null)
				msgs = ((InternalEObject)vEHICLECONNECTORPINREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALVEHICLELINK__VEHICLECONNECTORPINREFS, null, msgs);
			if (newVEHICLECONNECTORPINREFS != null)
				msgs = ((InternalEObject)newVEHICLECONNECTORPINREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALVEHICLELINK__VEHICLECONNECTORPINREFS, null, msgs);
			msgs = basicSetVEHICLECONNECTORPINREFS(newVEHICLECONNECTORPINREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__VEHICLECONNECTORPINREFS, newVEHICLECONNECTORPINREFS, newVEHICLECONNECTORPINREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LINKCOMPARAMREFS getLINKCOMPARAMREFS() {
		return lINKCOMPARAMREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLINKCOMPARAMREFS(LINKCOMPARAMREFS newLINKCOMPARAMREFS, NotificationChain msgs) {
		LINKCOMPARAMREFS oldLINKCOMPARAMREFS = lINKCOMPARAMREFS;
		lINKCOMPARAMREFS = newLINKCOMPARAMREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__LINKCOMPARAMREFS, oldLINKCOMPARAMREFS, newLINKCOMPARAMREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLINKCOMPARAMREFS(LINKCOMPARAMREFS newLINKCOMPARAMREFS) {
		if (newLINKCOMPARAMREFS != lINKCOMPARAMREFS) {
			NotificationChain msgs = null;
			if (lINKCOMPARAMREFS != null)
				msgs = ((InternalEObject)lINKCOMPARAMREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALVEHICLELINK__LINKCOMPARAMREFS, null, msgs);
			if (newLINKCOMPARAMREFS != null)
				msgs = ((InternalEObject)newLINKCOMPARAMREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALVEHICLELINK__LINKCOMPARAMREFS, null, msgs);
			msgs = basicSetLINKCOMPARAMREFS(newLINKCOMPARAMREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__LINKCOMPARAMREFS, newLINKCOMPARAMREFS, newLINKCOMPARAMREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALLINKTYPE getTYPE() {
		return tYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTYPE(PHYSICALLINKTYPE newTYPE) {
		PHYSICALLINKTYPE oldTYPE = tYPE;
		tYPE = newTYPE == null ? TYPE_EDEFAULT : newTYPE;
		boolean oldTYPEESet = tYPEESet;
		tYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALVEHICLELINK__TYPE, oldTYPE, tYPE, !oldTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetTYPE() {
		PHYSICALLINKTYPE oldTYPE = tYPE;
		boolean oldTYPEESet = tYPEESet;
		tYPE = TYPE_EDEFAULT;
		tYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALVEHICLELINK__TYPE, oldTYPE, TYPE_EDEFAULT, oldTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetTYPE() {
		return tYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__VEHICLECONNECTORPINREFS:
				return basicSetVEHICLECONNECTORPINREFS(null, msgs);
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LINKCOMPARAMREFS:
				return basicSetLINKCOMPARAMREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__DESC:
				return getDESC();
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__VEHICLECONNECTORPINREFS:
				return getVEHICLECONNECTORPINREFS();
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LINKCOMPARAMREFS:
				return getLINKCOMPARAMREFS();
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__ID:
				return getID();
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__OID:
				return getOID();
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__TYPE:
				return getTYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__VEHICLECONNECTORPINREFS:
				setVEHICLECONNECTORPINREFS((VEHICLECONNECTORPINREFS)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LINKCOMPARAMREFS:
				setLINKCOMPARAMREFS((LINKCOMPARAMREFS)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__OID:
				setOID((String)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__TYPE:
				setTYPE((PHYSICALLINKTYPE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__VEHICLECONNECTORPINREFS:
				setVEHICLECONNECTORPINREFS((VEHICLECONNECTORPINREFS)null);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LINKCOMPARAMREFS:
				setLINKCOMPARAMREFS((LINKCOMPARAMREFS)null);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__OID:
				setOID(OID_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__TYPE:
				unsetTYPE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__DESC:
				return dESC != null;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__VEHICLECONNECTORPINREFS:
				return vEHICLECONNECTORPINREFS != null;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__LINKCOMPARAMREFS:
				return lINKCOMPARAMREFS != null;
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
			case OdxXhtmlPackage.PHYSICALVEHICLELINK__TYPE:
				return isSetTYPE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(", tYPE: ");
		if (tYPEESet) result.append(tYPE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //PHYSICALVEHICLELINKImpl
