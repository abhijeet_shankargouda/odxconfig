/**
 */
package OdxXhtml.impl;

import OdxXhtml.GLOBALNEGRESPONSE;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GLOBALNEGRESPONSE</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GLOBALNEGRESPONSEImpl extends RESPONSEImpl implements GLOBALNEGRESPONSE {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GLOBALNEGRESPONSEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getGLOBALNEGRESPONSE();
	}

} //GLOBALNEGRESPONSEImpl
