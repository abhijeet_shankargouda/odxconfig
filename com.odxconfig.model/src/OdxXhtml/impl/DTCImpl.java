/**
 */
package OdxXhtml.impl;

import OdxXhtml.DTC;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SDGS;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DTC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DTCImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCImpl#getTROUBLECODE <em>TROUBLECODE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCImpl#getDISPLAYTROUBLECODE <em>DISPLAYTROUBLECODE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCImpl#getTEXT <em>TEXT</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCImpl#getLEVEL <em>LEVEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCImpl#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DTCImpl extends MinimalEObjectImpl.Container implements DTC {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTROUBLECODE() <em>TROUBLECODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTROUBLECODE()
	 * @generated
	 * @ordered
	 */
	protected static final int TROUBLECODE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTROUBLECODE() <em>TROUBLECODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTROUBLECODE()
	 * @generated
	 * @ordered
	 */
	protected int tROUBLECODE = TROUBLECODE_EDEFAULT;

	/**
	 * This is true if the TROUBLECODE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tROUBLECODEESet;

	/**
	 * The default value of the '{@link #getDISPLAYTROUBLECODE() <em>DISPLAYTROUBLECODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDISPLAYTROUBLECODE()
	 * @generated
	 * @ordered
	 */
	protected static final String DISPLAYTROUBLECODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDISPLAYTROUBLECODE() <em>DISPLAYTROUBLECODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDISPLAYTROUBLECODE()
	 * @generated
	 * @ordered
	 */
	protected String dISPLAYTROUBLECODE = DISPLAYTROUBLECODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTEXT() <em>TEXT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTEXT()
	 * @generated
	 * @ordered
	 */
	protected TEXT tEXT;

	/**
	 * The default value of the '{@link #getLEVEL() <em>LEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLEVEL()
	 * @generated
	 * @ordered
	 */
	protected static final short LEVEL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLEVEL() <em>LEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLEVEL()
	 * @generated
	 * @ordered
	 */
	protected short lEVEL = LEVEL_EDEFAULT;

	/**
	 * This is true if the LEVEL attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean lEVELESet;

	/**
	 * The cached value of the '{@link #getSDGS() <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGS()
	 * @generated
	 * @ordered
	 */
	protected SDGS sDGS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DTCImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDTC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getTROUBLECODE() {
		return tROUBLECODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTROUBLECODE(int newTROUBLECODE) {
		int oldTROUBLECODE = tROUBLECODE;
		tROUBLECODE = newTROUBLECODE;
		boolean oldTROUBLECODEESet = tROUBLECODEESet;
		tROUBLECODEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__TROUBLECODE, oldTROUBLECODE, tROUBLECODE, !oldTROUBLECODEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetTROUBLECODE() {
		int oldTROUBLECODE = tROUBLECODE;
		boolean oldTROUBLECODEESet = tROUBLECODEESet;
		tROUBLECODE = TROUBLECODE_EDEFAULT;
		tROUBLECODEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DTC__TROUBLECODE, oldTROUBLECODE, TROUBLECODE_EDEFAULT, oldTROUBLECODEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetTROUBLECODE() {
		return tROUBLECODEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDISPLAYTROUBLECODE() {
		return dISPLAYTROUBLECODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDISPLAYTROUBLECODE(String newDISPLAYTROUBLECODE) {
		String oldDISPLAYTROUBLECODE = dISPLAYTROUBLECODE;
		dISPLAYTROUBLECODE = newDISPLAYTROUBLECODE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__DISPLAYTROUBLECODE, oldDISPLAYTROUBLECODE, dISPLAYTROUBLECODE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getTEXT() {
		return tEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTEXT(TEXT newTEXT, NotificationChain msgs) {
		TEXT oldTEXT = tEXT;
		tEXT = newTEXT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__TEXT, oldTEXT, newTEXT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTEXT(TEXT newTEXT) {
		if (newTEXT != tEXT) {
			NotificationChain msgs = null;
			if (tEXT != null)
				msgs = ((InternalEObject)tEXT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTC__TEXT, null, msgs);
			if (newTEXT != null)
				msgs = ((InternalEObject)newTEXT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTC__TEXT, null, msgs);
			msgs = basicSetTEXT(newTEXT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__TEXT, newTEXT, newTEXT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public short getLEVEL() {
		return lEVEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLEVEL(short newLEVEL) {
		short oldLEVEL = lEVEL;
		lEVEL = newLEVEL;
		boolean oldLEVELESet = lEVELESet;
		lEVELESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__LEVEL, oldLEVEL, lEVEL, !oldLEVELESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetLEVEL() {
		short oldLEVEL = lEVEL;
		boolean oldLEVELESet = lEVELESet;
		lEVEL = LEVEL_EDEFAULT;
		lEVELESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DTC__LEVEL, oldLEVEL, LEVEL_EDEFAULT, oldLEVELESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetLEVEL() {
		return lEVELESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS getSDGS() {
		return sDGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGS(SDGS newSDGS, NotificationChain msgs) {
		SDGS oldSDGS = sDGS;
		sDGS = newSDGS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__SDGS, oldSDGS, newSDGS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGS(SDGS newSDGS) {
		if (newSDGS != sDGS) {
			NotificationChain msgs = null;
			if (sDGS != null)
				msgs = ((InternalEObject)sDGS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTC__SDGS, null, msgs);
			if (newSDGS != null)
				msgs = ((InternalEObject)newSDGS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTC__SDGS, null, msgs);
			msgs = basicSetSDGS(newSDGS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__SDGS, newSDGS, newSDGS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTC__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DTC__TEXT:
				return basicSetTEXT(null, msgs);
			case OdxXhtmlPackage.DTC__SDGS:
				return basicSetSDGS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DTC__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.DTC__TROUBLECODE:
				return getTROUBLECODE();
			case OdxXhtmlPackage.DTC__DISPLAYTROUBLECODE:
				return getDISPLAYTROUBLECODE();
			case OdxXhtmlPackage.DTC__TEXT:
				return getTEXT();
			case OdxXhtmlPackage.DTC__LEVEL:
				return getLEVEL();
			case OdxXhtmlPackage.DTC__SDGS:
				return getSDGS();
			case OdxXhtmlPackage.DTC__ID:
				return getID();
			case OdxXhtmlPackage.DTC__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DTC__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.DTC__TROUBLECODE:
				setTROUBLECODE((Integer)newValue);
				return;
			case OdxXhtmlPackage.DTC__DISPLAYTROUBLECODE:
				setDISPLAYTROUBLECODE((String)newValue);
				return;
			case OdxXhtmlPackage.DTC__TEXT:
				setTEXT((TEXT)newValue);
				return;
			case OdxXhtmlPackage.DTC__LEVEL:
				setLEVEL((Short)newValue);
				return;
			case OdxXhtmlPackage.DTC__SDGS:
				setSDGS((SDGS)newValue);
				return;
			case OdxXhtmlPackage.DTC__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.DTC__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DTC__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.DTC__TROUBLECODE:
				unsetTROUBLECODE();
				return;
			case OdxXhtmlPackage.DTC__DISPLAYTROUBLECODE:
				setDISPLAYTROUBLECODE(DISPLAYTROUBLECODE_EDEFAULT);
				return;
			case OdxXhtmlPackage.DTC__TEXT:
				setTEXT((TEXT)null);
				return;
			case OdxXhtmlPackage.DTC__LEVEL:
				unsetLEVEL();
				return;
			case OdxXhtmlPackage.DTC__SDGS:
				setSDGS((SDGS)null);
				return;
			case OdxXhtmlPackage.DTC__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.DTC__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DTC__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.DTC__TROUBLECODE:
				return isSetTROUBLECODE();
			case OdxXhtmlPackage.DTC__DISPLAYTROUBLECODE:
				return DISPLAYTROUBLECODE_EDEFAULT == null ? dISPLAYTROUBLECODE != null : !DISPLAYTROUBLECODE_EDEFAULT.equals(dISPLAYTROUBLECODE);
			case OdxXhtmlPackage.DTC__TEXT:
				return tEXT != null;
			case OdxXhtmlPackage.DTC__LEVEL:
				return isSetLEVEL();
			case OdxXhtmlPackage.DTC__SDGS:
				return sDGS != null;
			case OdxXhtmlPackage.DTC__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.DTC__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", tROUBLECODE: ");
		if (tROUBLECODEESet) result.append(tROUBLECODE); else result.append("<unset>");
		result.append(", dISPLAYTROUBLECODE: ");
		result.append(dISPLAYTROUBLECODE);
		result.append(", lEVEL: ");
		if (lEVELESet) result.append(lEVEL); else result.append("<unset>");
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //DTCImpl
