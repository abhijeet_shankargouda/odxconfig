/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SELECTIONTABLEREFS;
import OdxXhtml.SNREF;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SELECTIONTABLEREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SELECTIONTABLEREFSImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link OdxXhtml.impl.SELECTIONTABLEREFSImpl#getSELECTIONTABLEREF <em>SELECTIONTABLEREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.SELECTIONTABLEREFSImpl#getSELECTIONTABLESNREF <em>SELECTIONTABLESNREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SELECTIONTABLEREFSImpl extends MinimalEObjectImpl.Container implements SELECTIONTABLEREFS {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SELECTIONTABLEREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSELECTIONTABLEREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, OdxXhtmlPackage.SELECTIONTABLEREFS__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getSELECTIONTABLEREF() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getSELECTIONTABLEREFS_SELECTIONTABLEREF());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SNREF> getSELECTIONTABLESNREF() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getSELECTIONTABLEREFS_SELECTIONTABLESNREF());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SELECTIONTABLEREFS__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLEREF:
				return ((InternalEList<?>)getSELECTIONTABLEREF()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLESNREF:
				return ((InternalEList<?>)getSELECTIONTABLESNREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SELECTIONTABLEREFS__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLEREF:
				return getSELECTIONTABLEREF();
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLESNREF:
				return getSELECTIONTABLESNREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SELECTIONTABLEREFS__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLEREF:
				getSELECTIONTABLEREF().clear();
				getSELECTIONTABLEREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLESNREF:
				getSELECTIONTABLESNREF().clear();
				getSELECTIONTABLESNREF().addAll((Collection<? extends SNREF>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SELECTIONTABLEREFS__GROUP:
				getGroup().clear();
				return;
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLEREF:
				getSELECTIONTABLEREF().clear();
				return;
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLESNREF:
				getSELECTIONTABLESNREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SELECTIONTABLEREFS__GROUP:
				return group != null && !group.isEmpty();
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLEREF:
				return !getSELECTIONTABLEREF().isEmpty();
			case OdxXhtmlPackage.SELECTIONTABLEREFS__SELECTIONTABLESNREF:
				return !getSELECTIONTABLESNREF().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //SELECTIONTABLEREFSImpl
