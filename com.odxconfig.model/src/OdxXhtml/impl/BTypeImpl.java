/**
 */
package OdxXhtml.impl;

import OdxXhtml.BType;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BType</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BTypeImpl extends InlineImpl implements BType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getBType();
	}

} //BTypeImpl
