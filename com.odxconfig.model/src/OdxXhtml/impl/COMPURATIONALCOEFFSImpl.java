/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPUDENOMINATOR;
import OdxXhtml.COMPUNUMERATOR;
import OdxXhtml.COMPURATIONALCOEFFS;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPURATIONALCOEFFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPURATIONALCOEFFSImpl#getCOMPUNUMERATOR <em>COMPUNUMERATOR</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPURATIONALCOEFFSImpl#getCOMPUDENOMINATOR <em>COMPUDENOMINATOR</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPURATIONALCOEFFSImpl extends MinimalEObjectImpl.Container implements COMPURATIONALCOEFFS {
	/**
	 * The cached value of the '{@link #getCOMPUNUMERATOR() <em>COMPUNUMERATOR</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUNUMERATOR()
	 * @generated
	 * @ordered
	 */
	protected COMPUNUMERATOR cOMPUNUMERATOR;

	/**
	 * The cached value of the '{@link #getCOMPUDENOMINATOR() <em>COMPUDENOMINATOR</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUDENOMINATOR()
	 * @generated
	 * @ordered
	 */
	protected COMPUDENOMINATOR cOMPUDENOMINATOR;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPURATIONALCOEFFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPURATIONALCOEFFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUNUMERATOR getCOMPUNUMERATOR() {
		return cOMPUNUMERATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUNUMERATOR(COMPUNUMERATOR newCOMPUNUMERATOR, NotificationChain msgs) {
		COMPUNUMERATOR oldCOMPUNUMERATOR = cOMPUNUMERATOR;
		cOMPUNUMERATOR = newCOMPUNUMERATOR;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUNUMERATOR, oldCOMPUNUMERATOR, newCOMPUNUMERATOR);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUNUMERATOR(COMPUNUMERATOR newCOMPUNUMERATOR) {
		if (newCOMPUNUMERATOR != cOMPUNUMERATOR) {
			NotificationChain msgs = null;
			if (cOMPUNUMERATOR != null)
				msgs = ((InternalEObject)cOMPUNUMERATOR).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUNUMERATOR, null, msgs);
			if (newCOMPUNUMERATOR != null)
				msgs = ((InternalEObject)newCOMPUNUMERATOR).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUNUMERATOR, null, msgs);
			msgs = basicSetCOMPUNUMERATOR(newCOMPUNUMERATOR, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUNUMERATOR, newCOMPUNUMERATOR, newCOMPUNUMERATOR));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUDENOMINATOR getCOMPUDENOMINATOR() {
		return cOMPUDENOMINATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUDENOMINATOR(COMPUDENOMINATOR newCOMPUDENOMINATOR, NotificationChain msgs) {
		COMPUDENOMINATOR oldCOMPUDENOMINATOR = cOMPUDENOMINATOR;
		cOMPUDENOMINATOR = newCOMPUDENOMINATOR;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUDENOMINATOR, oldCOMPUDENOMINATOR, newCOMPUDENOMINATOR);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUDENOMINATOR(COMPUDENOMINATOR newCOMPUDENOMINATOR) {
		if (newCOMPUDENOMINATOR != cOMPUDENOMINATOR) {
			NotificationChain msgs = null;
			if (cOMPUDENOMINATOR != null)
				msgs = ((InternalEObject)cOMPUDENOMINATOR).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUDENOMINATOR, null, msgs);
			if (newCOMPUDENOMINATOR != null)
				msgs = ((InternalEObject)newCOMPUDENOMINATOR).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUDENOMINATOR, null, msgs);
			msgs = basicSetCOMPUDENOMINATOR(newCOMPUDENOMINATOR, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUDENOMINATOR, newCOMPUDENOMINATOR, newCOMPUDENOMINATOR));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUNUMERATOR:
				return basicSetCOMPUNUMERATOR(null, msgs);
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUDENOMINATOR:
				return basicSetCOMPUDENOMINATOR(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUNUMERATOR:
				return getCOMPUNUMERATOR();
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUDENOMINATOR:
				return getCOMPUDENOMINATOR();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUNUMERATOR:
				setCOMPUNUMERATOR((COMPUNUMERATOR)newValue);
				return;
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUDENOMINATOR:
				setCOMPUDENOMINATOR((COMPUDENOMINATOR)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUNUMERATOR:
				setCOMPUNUMERATOR((COMPUNUMERATOR)null);
				return;
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUDENOMINATOR:
				setCOMPUDENOMINATOR((COMPUDENOMINATOR)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUNUMERATOR:
				return cOMPUNUMERATOR != null;
			case OdxXhtmlPackage.COMPURATIONALCOEFFS__COMPUDENOMINATOR:
				return cOMPUDENOMINATOR != null;
		}
		return super.eIsSet(featureID);
	}

} //COMPURATIONALCOEFFSImpl
