/**
 */
package OdxXhtml.impl;

import OdxXhtml.DIAGLAYERREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGLAYERREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERREFSImpl#getDIAGLAYERREF <em>DIAGLAYERREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGLAYERREFSImpl extends MinimalEObjectImpl.Container implements DIAGLAYERREFS {
	/**
	 * The cached value of the '{@link #getDIAGLAYERREF() <em>DIAGLAYERREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGLAYERREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> dIAGLAYERREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGLAYERREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGLAYERREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getDIAGLAYERREF() {
		if (dIAGLAYERREF == null) {
			dIAGLAYERREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.DIAGLAYERREFS__DIAGLAYERREF);
		}
		return dIAGLAYERREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERREFS__DIAGLAYERREF:
				return ((InternalEList<?>)getDIAGLAYERREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERREFS__DIAGLAYERREF:
				return getDIAGLAYERREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERREFS__DIAGLAYERREF:
				getDIAGLAYERREF().clear();
				getDIAGLAYERREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERREFS__DIAGLAYERREF:
				getDIAGLAYERREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERREFS__DIAGLAYERREF:
				return dIAGLAYERREF != null && !dIAGLAYERREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DIAGLAYERREFSImpl
