/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.ECUMEM;
import OdxXhtml.MEM;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSMEM;
import OdxXhtml.PROJECTINFOS;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUMEM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ECUMEMImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMImpl#getPROJECTINFOS <em>PROJECTINFOS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMImpl#getMEM <em>MEM</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMImpl#getPHYSMEM <em>PHYSMEM</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECUMEMImpl extends MinimalEObjectImpl.Container implements ECUMEM {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getPROJECTINFOS() <em>PROJECTINFOS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROJECTINFOS()
	 * @generated
	 * @ordered
	 */
	protected PROJECTINFOS pROJECTINFOS;

	/**
	 * The cached value of the '{@link #getMEM() <em>MEM</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMEM()
	 * @generated
	 * @ordered
	 */
	protected MEM mEM;

	/**
	 * The cached value of the '{@link #getPHYSMEM() <em>PHYSMEM</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSMEM()
	 * @generated
	 * @ordered
	 */
	protected PHYSMEM pHYSMEM;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUMEMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUMEM();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROJECTINFOS getPROJECTINFOS() {
		return pROJECTINFOS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROJECTINFOS(PROJECTINFOS newPROJECTINFOS, NotificationChain msgs) {
		PROJECTINFOS oldPROJECTINFOS = pROJECTINFOS;
		pROJECTINFOS = newPROJECTINFOS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__PROJECTINFOS, oldPROJECTINFOS, newPROJECTINFOS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROJECTINFOS(PROJECTINFOS newPROJECTINFOS) {
		if (newPROJECTINFOS != pROJECTINFOS) {
			NotificationChain msgs = null;
			if (pROJECTINFOS != null)
				msgs = ((InternalEObject)pROJECTINFOS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__PROJECTINFOS, null, msgs);
			if (newPROJECTINFOS != null)
				msgs = ((InternalEObject)newPROJECTINFOS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__PROJECTINFOS, null, msgs);
			msgs = basicSetPROJECTINFOS(newPROJECTINFOS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__PROJECTINFOS, newPROJECTINFOS, newPROJECTINFOS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MEM getMEM() {
		return mEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMEM(MEM newMEM, NotificationChain msgs) {
		MEM oldMEM = mEM;
		mEM = newMEM;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__MEM, oldMEM, newMEM);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMEM(MEM newMEM) {
		if (newMEM != mEM) {
			NotificationChain msgs = null;
			if (mEM != null)
				msgs = ((InternalEObject)mEM).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__MEM, null, msgs);
			if (newMEM != null)
				msgs = ((InternalEObject)newMEM).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__MEM, null, msgs);
			msgs = basicSetMEM(newMEM, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__MEM, newMEM, newMEM));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSMEM getPHYSMEM() {
		return pHYSMEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPHYSMEM(PHYSMEM newPHYSMEM, NotificationChain msgs) {
		PHYSMEM oldPHYSMEM = pHYSMEM;
		pHYSMEM = newPHYSMEM;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__PHYSMEM, oldPHYSMEM, newPHYSMEM);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHYSMEM(PHYSMEM newPHYSMEM) {
		if (newPHYSMEM != pHYSMEM) {
			NotificationChain msgs = null;
			if (pHYSMEM != null)
				msgs = ((InternalEObject)pHYSMEM).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__PHYSMEM, null, msgs);
			if (newPHYSMEM != null)
				msgs = ((InternalEObject)newPHYSMEM).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEM__PHYSMEM, null, msgs);
			msgs = basicSetPHYSMEM(newPHYSMEM, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__PHYSMEM, newPHYSMEM, newPHYSMEM));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEM__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEM__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.ECUMEM__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.ECUMEM__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.ECUMEM__PROJECTINFOS:
				return basicSetPROJECTINFOS(null, msgs);
			case OdxXhtmlPackage.ECUMEM__MEM:
				return basicSetMEM(null, msgs);
			case OdxXhtmlPackage.ECUMEM__PHYSMEM:
				return basicSetPHYSMEM(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEM__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.ECUMEM__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.ECUMEM__DESC:
				return getDESC();
			case OdxXhtmlPackage.ECUMEM__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.ECUMEM__PROJECTINFOS:
				return getPROJECTINFOS();
			case OdxXhtmlPackage.ECUMEM__MEM:
				return getMEM();
			case OdxXhtmlPackage.ECUMEM__PHYSMEM:
				return getPHYSMEM();
			case OdxXhtmlPackage.ECUMEM__ID:
				return getID();
			case OdxXhtmlPackage.ECUMEM__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEM__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.ECUMEM__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.ECUMEM__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.ECUMEM__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.ECUMEM__PROJECTINFOS:
				setPROJECTINFOS((PROJECTINFOS)newValue);
				return;
			case OdxXhtmlPackage.ECUMEM__MEM:
				setMEM((MEM)newValue);
				return;
			case OdxXhtmlPackage.ECUMEM__PHYSMEM:
				setPHYSMEM((PHYSMEM)newValue);
				return;
			case OdxXhtmlPackage.ECUMEM__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.ECUMEM__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEM__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.ECUMEM__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.ECUMEM__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.ECUMEM__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.ECUMEM__PROJECTINFOS:
				setPROJECTINFOS((PROJECTINFOS)null);
				return;
			case OdxXhtmlPackage.ECUMEM__MEM:
				setMEM((MEM)null);
				return;
			case OdxXhtmlPackage.ECUMEM__PHYSMEM:
				setPHYSMEM((PHYSMEM)null);
				return;
			case OdxXhtmlPackage.ECUMEM__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.ECUMEM__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEM__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.ECUMEM__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.ECUMEM__DESC:
				return dESC != null;
			case OdxXhtmlPackage.ECUMEM__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.ECUMEM__PROJECTINFOS:
				return pROJECTINFOS != null;
			case OdxXhtmlPackage.ECUMEM__MEM:
				return mEM != null;
			case OdxXhtmlPackage.ECUMEM__PHYSMEM:
				return pHYSMEM != null;
			case OdxXhtmlPackage.ECUMEM__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.ECUMEM__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //ECUMEMImpl
