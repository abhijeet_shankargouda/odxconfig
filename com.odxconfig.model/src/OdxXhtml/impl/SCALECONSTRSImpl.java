/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SCALECONSTR;
import OdxXhtml.SCALECONSTRS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SCALECONSTRS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SCALECONSTRSImpl#getSCALECONSTR <em>SCALECONSTR</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SCALECONSTRSImpl extends MinimalEObjectImpl.Container implements SCALECONSTRS {
	/**
	 * The cached value of the '{@link #getSCALECONSTR() <em>SCALECONSTR</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSCALECONSTR()
	 * @generated
	 * @ordered
	 */
	protected EList<SCALECONSTR> sCALECONSTR;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SCALECONSTRSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSCALECONSTRS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SCALECONSTR> getSCALECONSTR() {
		if (sCALECONSTR == null) {
			sCALECONSTR = new EObjectContainmentEList<SCALECONSTR>(SCALECONSTR.class, this, OdxXhtmlPackage.SCALECONSTRS__SCALECONSTR);
		}
		return sCALECONSTR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTRS__SCALECONSTR:
				return ((InternalEList<?>)getSCALECONSTR()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTRS__SCALECONSTR:
				return getSCALECONSTR();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTRS__SCALECONSTR:
				getSCALECONSTR().clear();
				getSCALECONSTR().addAll((Collection<? extends SCALECONSTR>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTRS__SCALECONSTR:
				getSCALECONSTR().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTRS__SCALECONSTR:
				return sCALECONSTR != null && !sCALECONSTR.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SCALECONSTRSImpl
