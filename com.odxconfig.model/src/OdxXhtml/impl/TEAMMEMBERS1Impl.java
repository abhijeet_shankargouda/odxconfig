/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEAMMEMBER1;
import OdxXhtml.TEAMMEMBERS1;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TEAMMEMBERS1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBERS1Impl#getTEAMMEMBER <em>TEAMMEMBER</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TEAMMEMBERS1Impl extends MinimalEObjectImpl.Container implements TEAMMEMBERS1 {
	/**
	 * The cached value of the '{@link #getTEAMMEMBER() <em>TEAMMEMBER</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTEAMMEMBER()
	 * @generated
	 * @ordered
	 */
	protected EList<TEAMMEMBER1> tEAMMEMBER;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TEAMMEMBERS1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getTEAMMEMBERS1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TEAMMEMBER1> getTEAMMEMBER() {
		if (tEAMMEMBER == null) {
			tEAMMEMBER = new EObjectContainmentEList<TEAMMEMBER1>(TEAMMEMBER1.class, this, OdxXhtmlPackage.TEAMMEMBERS1__TEAMMEMBER);
		}
		return tEAMMEMBER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS1__TEAMMEMBER:
				return ((InternalEList<?>)getTEAMMEMBER()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS1__TEAMMEMBER:
				return getTEAMMEMBER();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS1__TEAMMEMBER:
				getTEAMMEMBER().clear();
				getTEAMMEMBER().addAll((Collection<? extends TEAMMEMBER1>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS1__TEAMMEMBER:
				getTEAMMEMBER().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS1__TEAMMEMBER:
				return tEAMMEMBER != null && !tEAMMEMBER.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TEAMMEMBERS1Impl
