/**
 */
package OdxXhtml.impl;

import OdxXhtml.BASICSTRUCTURE;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PARAMS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BASICSTRUCTURE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.BASICSTRUCTUREImpl#getBYTESIZE <em>BYTESIZE</em>}</li>
 *   <li>{@link OdxXhtml.impl.BASICSTRUCTUREImpl#getPARAMS <em>PARAMS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BASICSTRUCTUREImpl extends COMPLEXDOPImpl implements BASICSTRUCTURE {
	/**
	 * The default value of the '{@link #getBYTESIZE() <em>BYTESIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTESIZE()
	 * @generated
	 * @ordered
	 */
	protected static final long BYTESIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBYTESIZE() <em>BYTESIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTESIZE()
	 * @generated
	 * @ordered
	 */
	protected long bYTESIZE = BYTESIZE_EDEFAULT;

	/**
	 * This is true if the BYTESIZE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bYTESIZEESet;

	/**
	 * The cached value of the '{@link #getPARAMS() <em>PARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARAMS()
	 * @generated
	 * @ordered
	 */
	protected PARAMS pARAMS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BASICSTRUCTUREImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getBASICSTRUCTURE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBYTESIZE() {
		return bYTESIZE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBYTESIZE(long newBYTESIZE) {
		long oldBYTESIZE = bYTESIZE;
		bYTESIZE = newBYTESIZE;
		boolean oldBYTESIZEESet = bYTESIZEESet;
		bYTESIZEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASICSTRUCTURE__BYTESIZE, oldBYTESIZE, bYTESIZE, !oldBYTESIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBYTESIZE() {
		long oldBYTESIZE = bYTESIZE;
		boolean oldBYTESIZEESet = bYTESIZEESet;
		bYTESIZE = BYTESIZE_EDEFAULT;
		bYTESIZEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.BASICSTRUCTURE__BYTESIZE, oldBYTESIZE, BYTESIZE_EDEFAULT, oldBYTESIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBYTESIZE() {
		return bYTESIZEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARAMS getPARAMS() {
		return pARAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPARAMS(PARAMS newPARAMS, NotificationChain msgs) {
		PARAMS oldPARAMS = pARAMS;
		pARAMS = newPARAMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASICSTRUCTURE__PARAMS, oldPARAMS, newPARAMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPARAMS(PARAMS newPARAMS) {
		if (newPARAMS != pARAMS) {
			NotificationChain msgs = null;
			if (pARAMS != null)
				msgs = ((InternalEObject)pARAMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASICSTRUCTURE__PARAMS, null, msgs);
			if (newPARAMS != null)
				msgs = ((InternalEObject)newPARAMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASICSTRUCTURE__PARAMS, null, msgs);
			msgs = basicSetPARAMS(newPARAMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASICSTRUCTURE__PARAMS, newPARAMS, newPARAMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.BASICSTRUCTURE__PARAMS:
				return basicSetPARAMS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.BASICSTRUCTURE__BYTESIZE:
				return getBYTESIZE();
			case OdxXhtmlPackage.BASICSTRUCTURE__PARAMS:
				return getPARAMS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.BASICSTRUCTURE__BYTESIZE:
				setBYTESIZE((Long)newValue);
				return;
			case OdxXhtmlPackage.BASICSTRUCTURE__PARAMS:
				setPARAMS((PARAMS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.BASICSTRUCTURE__BYTESIZE:
				unsetBYTESIZE();
				return;
			case OdxXhtmlPackage.BASICSTRUCTURE__PARAMS:
				setPARAMS((PARAMS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.BASICSTRUCTURE__BYTESIZE:
				return isSetBYTESIZE();
			case OdxXhtmlPackage.BASICSTRUCTURE__PARAMS:
				return pARAMS != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bYTESIZE: ");
		if (bYTESIZEESet) result.append(bYTESIZE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //BASICSTRUCTUREImpl
