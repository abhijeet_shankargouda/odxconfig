/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA1;
import OdxXhtml.COMPANYDOCINFOS1;
import OdxXhtml.DOCREVISIONS1;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ADMINDATA1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ADMINDATA1Impl#getLANGUAGE <em>LANGUAGE</em>}</li>
 *   <li>{@link OdxXhtml.impl.ADMINDATA1Impl#getCOMPANYDOCINFOS <em>COMPANYDOCINFOS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ADMINDATA1Impl#getDOCREVISIONS <em>DOCREVISIONS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ADMINDATA1Impl extends MinimalEObjectImpl.Container implements ADMINDATA1 {
	/**
	 * The default value of the '{@link #getLANGUAGE() <em>LANGUAGE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLANGUAGE()
	 * @generated
	 * @ordered
	 */
	protected static final String LANGUAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLANGUAGE() <em>LANGUAGE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLANGUAGE()
	 * @generated
	 * @ordered
	 */
	protected String lANGUAGE = LANGUAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCOMPANYDOCINFOS() <em>COMPANYDOCINFOS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDOCINFOS()
	 * @generated
	 * @ordered
	 */
	protected COMPANYDOCINFOS1 cOMPANYDOCINFOS;

	/**
	 * The cached value of the '{@link #getDOCREVISIONS() <em>DOCREVISIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREVISIONS()
	 * @generated
	 * @ordered
	 */
	protected DOCREVISIONS1 dOCREVISIONS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ADMINDATA1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getADMINDATA1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLANGUAGE() {
		return lANGUAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLANGUAGE(String newLANGUAGE) {
		String oldLANGUAGE = lANGUAGE;
		lANGUAGE = newLANGUAGE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ADMINDATA1__LANGUAGE, oldLANGUAGE, lANGUAGE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDOCINFOS1 getCOMPANYDOCINFOS() {
		return cOMPANYDOCINFOS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYDOCINFOS(COMPANYDOCINFOS1 newCOMPANYDOCINFOS, NotificationChain msgs) {
		COMPANYDOCINFOS1 oldCOMPANYDOCINFOS = cOMPANYDOCINFOS;
		cOMPANYDOCINFOS = newCOMPANYDOCINFOS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ADMINDATA1__COMPANYDOCINFOS, oldCOMPANYDOCINFOS, newCOMPANYDOCINFOS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYDOCINFOS(COMPANYDOCINFOS1 newCOMPANYDOCINFOS) {
		if (newCOMPANYDOCINFOS != cOMPANYDOCINFOS) {
			NotificationChain msgs = null;
			if (cOMPANYDOCINFOS != null)
				msgs = ((InternalEObject)cOMPANYDOCINFOS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ADMINDATA1__COMPANYDOCINFOS, null, msgs);
			if (newCOMPANYDOCINFOS != null)
				msgs = ((InternalEObject)newCOMPANYDOCINFOS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ADMINDATA1__COMPANYDOCINFOS, null, msgs);
			msgs = basicSetCOMPANYDOCINFOS(newCOMPANYDOCINFOS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ADMINDATA1__COMPANYDOCINFOS, newCOMPANYDOCINFOS, newCOMPANYDOCINFOS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOCREVISIONS1 getDOCREVISIONS() {
		return dOCREVISIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDOCREVISIONS(DOCREVISIONS1 newDOCREVISIONS, NotificationChain msgs) {
		DOCREVISIONS1 oldDOCREVISIONS = dOCREVISIONS;
		dOCREVISIONS = newDOCREVISIONS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ADMINDATA1__DOCREVISIONS, oldDOCREVISIONS, newDOCREVISIONS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCREVISIONS(DOCREVISIONS1 newDOCREVISIONS) {
		if (newDOCREVISIONS != dOCREVISIONS) {
			NotificationChain msgs = null;
			if (dOCREVISIONS != null)
				msgs = ((InternalEObject)dOCREVISIONS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ADMINDATA1__DOCREVISIONS, null, msgs);
			if (newDOCREVISIONS != null)
				msgs = ((InternalEObject)newDOCREVISIONS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ADMINDATA1__DOCREVISIONS, null, msgs);
			msgs = basicSetDOCREVISIONS(newDOCREVISIONS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ADMINDATA1__DOCREVISIONS, newDOCREVISIONS, newDOCREVISIONS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ADMINDATA1__COMPANYDOCINFOS:
				return basicSetCOMPANYDOCINFOS(null, msgs);
			case OdxXhtmlPackage.ADMINDATA1__DOCREVISIONS:
				return basicSetDOCREVISIONS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ADMINDATA1__LANGUAGE:
				return getLANGUAGE();
			case OdxXhtmlPackage.ADMINDATA1__COMPANYDOCINFOS:
				return getCOMPANYDOCINFOS();
			case OdxXhtmlPackage.ADMINDATA1__DOCREVISIONS:
				return getDOCREVISIONS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ADMINDATA1__LANGUAGE:
				setLANGUAGE((String)newValue);
				return;
			case OdxXhtmlPackage.ADMINDATA1__COMPANYDOCINFOS:
				setCOMPANYDOCINFOS((COMPANYDOCINFOS1)newValue);
				return;
			case OdxXhtmlPackage.ADMINDATA1__DOCREVISIONS:
				setDOCREVISIONS((DOCREVISIONS1)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ADMINDATA1__LANGUAGE:
				setLANGUAGE(LANGUAGE_EDEFAULT);
				return;
			case OdxXhtmlPackage.ADMINDATA1__COMPANYDOCINFOS:
				setCOMPANYDOCINFOS((COMPANYDOCINFOS1)null);
				return;
			case OdxXhtmlPackage.ADMINDATA1__DOCREVISIONS:
				setDOCREVISIONS((DOCREVISIONS1)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ADMINDATA1__LANGUAGE:
				return LANGUAGE_EDEFAULT == null ? lANGUAGE != null : !LANGUAGE_EDEFAULT.equals(lANGUAGE);
			case OdxXhtmlPackage.ADMINDATA1__COMPANYDOCINFOS:
				return cOMPANYDOCINFOS != null;
			case OdxXhtmlPackage.ADMINDATA1__DOCREVISIONS:
				return dOCREVISIONS != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (lANGUAGE: ");
		result.append(lANGUAGE);
		result.append(')');
		return result.toString();
	}

} //ADMINDATA1Impl
