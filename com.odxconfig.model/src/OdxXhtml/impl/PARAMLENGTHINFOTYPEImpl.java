/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PARAMLENGTHINFOTYPE;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PARAMLENGTHINFOTYPE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PARAMLENGTHINFOTYPEImpl#getLENGTHKEYREF <em>LENGTHKEYREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PARAMLENGTHINFOTYPEImpl extends DIAGCODEDTYPEImpl implements PARAMLENGTHINFOTYPE {
	/**
	 * The cached value of the '{@link #getLENGTHKEYREF() <em>LENGTHKEYREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLENGTHKEYREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK lENGTHKEYREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PARAMLENGTHINFOTYPEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPARAMLENGTHINFOTYPE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getLENGTHKEYREF() {
		return lENGTHKEYREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLENGTHKEYREF(ODXLINK newLENGTHKEYREF, NotificationChain msgs) {
		ODXLINK oldLENGTHKEYREF = lENGTHKEYREF;
		lENGTHKEYREF = newLENGTHKEYREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARAMLENGTHINFOTYPE__LENGTHKEYREF, oldLENGTHKEYREF, newLENGTHKEYREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLENGTHKEYREF(ODXLINK newLENGTHKEYREF) {
		if (newLENGTHKEYREF != lENGTHKEYREF) {
			NotificationChain msgs = null;
			if (lENGTHKEYREF != null)
				msgs = ((InternalEObject)lENGTHKEYREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PARAMLENGTHINFOTYPE__LENGTHKEYREF, null, msgs);
			if (newLENGTHKEYREF != null)
				msgs = ((InternalEObject)newLENGTHKEYREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PARAMLENGTHINFOTYPE__LENGTHKEYREF, null, msgs);
			msgs = basicSetLENGTHKEYREF(newLENGTHKEYREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARAMLENGTHINFOTYPE__LENGTHKEYREF, newLENGTHKEYREF, newLENGTHKEYREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PARAMLENGTHINFOTYPE__LENGTHKEYREF:
				return basicSetLENGTHKEYREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PARAMLENGTHINFOTYPE__LENGTHKEYREF:
				return getLENGTHKEYREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PARAMLENGTHINFOTYPE__LENGTHKEYREF:
				setLENGTHKEYREF((ODXLINK)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PARAMLENGTHINFOTYPE__LENGTHKEYREF:
				setLENGTHKEYREF((ODXLINK)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PARAMLENGTHINFOTYPE__LENGTHKEYREF:
				return lENGTHKEYREF != null;
		}
		return super.eIsSet(featureID);
	}

} //PARAMLENGTHINFOTYPEImpl
