/**
 */
package OdxXhtml.impl;

import OdxXhtml.ENVDATA;
import OdxXhtml.ENVDATAS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ENVDATAS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ENVDATASImpl#getENVDATAPROXY <em>ENVDATAPROXY</em>}</li>
 *   <li>{@link OdxXhtml.impl.ENVDATASImpl#getENVDATAREF <em>ENVDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.ENVDATASImpl#getENVDATA <em>ENVDATA</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ENVDATASImpl extends MinimalEObjectImpl.Container implements ENVDATAS {
	/**
	 * The cached value of the '{@link #getENVDATAPROXY() <em>ENVDATAPROXY</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENVDATAPROXY()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap eNVDATAPROXY;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ENVDATASImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getENVDATAS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getENVDATAPROXY() {
		if (eNVDATAPROXY == null) {
			eNVDATAPROXY = new BasicFeatureMap(this, OdxXhtmlPackage.ENVDATAS__ENVDATAPROXY);
		}
		return eNVDATAPROXY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getENVDATAREF() {
		return getENVDATAPROXY().list(OdxXhtmlPackage.eINSTANCE.getENVDATAS_ENVDATAREF());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ENVDATA> getENVDATA() {
		return getENVDATAPROXY().list(OdxXhtmlPackage.eINSTANCE.getENVDATAS_ENVDATA());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATAS__ENVDATAPROXY:
				return ((InternalEList<?>)getENVDATAPROXY()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.ENVDATAS__ENVDATAREF:
				return ((InternalEList<?>)getENVDATAREF()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.ENVDATAS__ENVDATA:
				return ((InternalEList<?>)getENVDATA()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATAS__ENVDATAPROXY:
				if (coreType) return getENVDATAPROXY();
				return ((FeatureMap.Internal)getENVDATAPROXY()).getWrapper();
			case OdxXhtmlPackage.ENVDATAS__ENVDATAREF:
				return getENVDATAREF();
			case OdxXhtmlPackage.ENVDATAS__ENVDATA:
				return getENVDATA();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATAS__ENVDATAPROXY:
				((FeatureMap.Internal)getENVDATAPROXY()).set(newValue);
				return;
			case OdxXhtmlPackage.ENVDATAS__ENVDATAREF:
				getENVDATAREF().clear();
				getENVDATAREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
			case OdxXhtmlPackage.ENVDATAS__ENVDATA:
				getENVDATA().clear();
				getENVDATA().addAll((Collection<? extends ENVDATA>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATAS__ENVDATAPROXY:
				getENVDATAPROXY().clear();
				return;
			case OdxXhtmlPackage.ENVDATAS__ENVDATAREF:
				getENVDATAREF().clear();
				return;
			case OdxXhtmlPackage.ENVDATAS__ENVDATA:
				getENVDATA().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATAS__ENVDATAPROXY:
				return eNVDATAPROXY != null && !eNVDATAPROXY.isEmpty();
			case OdxXhtmlPackage.ENVDATAS__ENVDATAREF:
				return !getENVDATAREF().isEmpty();
			case OdxXhtmlPackage.ENVDATAS__ENVDATA:
				return !getENVDATA().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (eNVDATAPROXY: ");
		result.append(eNVDATAPROXY);
		result.append(')');
		return result.toString();
	}

} //ENVDATASImpl
