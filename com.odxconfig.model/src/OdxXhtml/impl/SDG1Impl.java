/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SD1;
import OdxXhtml.SDG1;
import OdxXhtml.SDGCAPTION1;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SDG1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SDG1Impl#getSDGCAPTION <em>SDGCAPTION</em>}</li>
 *   <li>{@link OdxXhtml.impl.SDG1Impl#getGroup <em>Group</em>}</li>
 *   <li>{@link OdxXhtml.impl.SDG1Impl#getSDG <em>SDG</em>}</li>
 *   <li>{@link OdxXhtml.impl.SDG1Impl#getSD <em>SD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SDG1Impl extends SPECIALDATA1Impl implements SDG1 {
	/**
	 * The cached value of the '{@link #getSDGCAPTION() <em>SDGCAPTION</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGCAPTION()
	 * @generated
	 * @ordered
	 */
	protected SDGCAPTION1 sDGCAPTION;

	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SDG1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSDG1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGCAPTION1 getSDGCAPTION() {
		return sDGCAPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGCAPTION(SDGCAPTION1 newSDGCAPTION, NotificationChain msgs) {
		SDGCAPTION1 oldSDGCAPTION = sDGCAPTION;
		sDGCAPTION = newSDGCAPTION;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SDG1__SDGCAPTION, oldSDGCAPTION, newSDGCAPTION);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGCAPTION(SDGCAPTION1 newSDGCAPTION) {
		if (newSDGCAPTION != sDGCAPTION) {
			NotificationChain msgs = null;
			if (sDGCAPTION != null)
				msgs = ((InternalEObject)sDGCAPTION).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SDG1__SDGCAPTION, null, msgs);
			if (newSDGCAPTION != null)
				msgs = ((InternalEObject)newSDGCAPTION).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SDG1__SDGCAPTION, null, msgs);
			msgs = basicSetSDGCAPTION(newSDGCAPTION, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SDG1__SDGCAPTION, newSDGCAPTION, newSDGCAPTION));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, OdxXhtmlPackage.SDG1__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SDG1> getSDG() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getSDG1_SDG());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SD1> getSD() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getSDG1_SD());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG1__SDGCAPTION:
				return basicSetSDGCAPTION(null, msgs);
			case OdxXhtmlPackage.SDG1__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.SDG1__SDG:
				return ((InternalEList<?>)getSDG()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.SDG1__SD:
				return ((InternalEList<?>)getSD()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG1__SDGCAPTION:
				return getSDGCAPTION();
			case OdxXhtmlPackage.SDG1__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case OdxXhtmlPackage.SDG1__SDG:
				return getSDG();
			case OdxXhtmlPackage.SDG1__SD:
				return getSD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG1__SDGCAPTION:
				setSDGCAPTION((SDGCAPTION1)newValue);
				return;
			case OdxXhtmlPackage.SDG1__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case OdxXhtmlPackage.SDG1__SDG:
				getSDG().clear();
				getSDG().addAll((Collection<? extends SDG1>)newValue);
				return;
			case OdxXhtmlPackage.SDG1__SD:
				getSD().clear();
				getSD().addAll((Collection<? extends SD1>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG1__SDGCAPTION:
				setSDGCAPTION((SDGCAPTION1)null);
				return;
			case OdxXhtmlPackage.SDG1__GROUP:
				getGroup().clear();
				return;
			case OdxXhtmlPackage.SDG1__SDG:
				getSDG().clear();
				return;
			case OdxXhtmlPackage.SDG1__SD:
				getSD().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG1__SDGCAPTION:
				return sDGCAPTION != null;
			case OdxXhtmlPackage.SDG1__GROUP:
				return group != null && !group.isEmpty();
			case OdxXhtmlPackage.SDG1__SDG:
				return !getSDG().isEmpty();
			case OdxXhtmlPackage.SDG1__SD:
				return !getSD().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //SDG1Impl
