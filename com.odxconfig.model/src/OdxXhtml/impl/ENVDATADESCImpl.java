/**
 */
package OdxXhtml.impl;

import OdxXhtml.ENVDATADESC;
import OdxXhtml.ENVDATAS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ENVDATADESC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ENVDATADESCImpl#getPARAMSNREF <em>PARAMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.ENVDATADESCImpl#getENVDATAS <em>ENVDATAS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ENVDATADESCImpl extends COMPLEXDOPImpl implements ENVDATADESC {
	/**
	 * The cached value of the '{@link #getPARAMSNREF() <em>PARAMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARAMSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF pARAMSNREF;

	/**
	 * The cached value of the '{@link #getENVDATAS() <em>ENVDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENVDATAS()
	 * @generated
	 * @ordered
	 */
	protected ENVDATAS eNVDATAS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ENVDATADESCImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getENVDATADESC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getPARAMSNREF() {
		return pARAMSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPARAMSNREF(SNREF newPARAMSNREF, NotificationChain msgs) {
		SNREF oldPARAMSNREF = pARAMSNREF;
		pARAMSNREF = newPARAMSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENVDATADESC__PARAMSNREF, oldPARAMSNREF, newPARAMSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPARAMSNREF(SNREF newPARAMSNREF) {
		if (newPARAMSNREF != pARAMSNREF) {
			NotificationChain msgs = null;
			if (pARAMSNREF != null)
				msgs = ((InternalEObject)pARAMSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ENVDATADESC__PARAMSNREF, null, msgs);
			if (newPARAMSNREF != null)
				msgs = ((InternalEObject)newPARAMSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ENVDATADESC__PARAMSNREF, null, msgs);
			msgs = basicSetPARAMSNREF(newPARAMSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENVDATADESC__PARAMSNREF, newPARAMSNREF, newPARAMSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENVDATAS getENVDATAS() {
		return eNVDATAS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetENVDATAS(ENVDATAS newENVDATAS, NotificationChain msgs) {
		ENVDATAS oldENVDATAS = eNVDATAS;
		eNVDATAS = newENVDATAS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENVDATADESC__ENVDATAS, oldENVDATAS, newENVDATAS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setENVDATAS(ENVDATAS newENVDATAS) {
		if (newENVDATAS != eNVDATAS) {
			NotificationChain msgs = null;
			if (eNVDATAS != null)
				msgs = ((InternalEObject)eNVDATAS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ENVDATADESC__ENVDATAS, null, msgs);
			if (newENVDATAS != null)
				msgs = ((InternalEObject)newENVDATAS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ENVDATADESC__ENVDATAS, null, msgs);
			msgs = basicSetENVDATAS(newENVDATAS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENVDATADESC__ENVDATAS, newENVDATAS, newENVDATAS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESC__PARAMSNREF:
				return basicSetPARAMSNREF(null, msgs);
			case OdxXhtmlPackage.ENVDATADESC__ENVDATAS:
				return basicSetENVDATAS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESC__PARAMSNREF:
				return getPARAMSNREF();
			case OdxXhtmlPackage.ENVDATADESC__ENVDATAS:
				return getENVDATAS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESC__PARAMSNREF:
				setPARAMSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.ENVDATADESC__ENVDATAS:
				setENVDATAS((ENVDATAS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESC__PARAMSNREF:
				setPARAMSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.ENVDATADESC__ENVDATAS:
				setENVDATAS((ENVDATAS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESC__PARAMSNREF:
				return pARAMSNREF != null;
			case OdxXhtmlPackage.ENVDATADESC__ENVDATAS:
				return eNVDATAS != null;
		}
		return super.eIsSet(featureID);
	}

} //ENVDATADESCImpl
