/**
 */
package OdxXhtml.impl;

import OdxXhtml.INTERVALTYPE;
import OdxXhtml.LIMIT;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LIMIT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.LIMITImpl#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.impl.LIMITImpl#getINTERVALTYPE <em>INTERVALTYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LIMITImpl extends MinimalEObjectImpl.Container implements LIMIT {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getINTERVALTYPE() <em>INTERVALTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINTERVALTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final INTERVALTYPE INTERVALTYPE_EDEFAULT = INTERVALTYPE.CLOSED;

	/**
	 * The cached value of the '{@link #getINTERVALTYPE() <em>INTERVALTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINTERVALTYPE()
	 * @generated
	 * @ordered
	 */
	protected INTERVALTYPE iNTERVALTYPE = INTERVALTYPE_EDEFAULT;

	/**
	 * This is true if the INTERVALTYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iNTERVALTYPEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LIMITImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLIMIT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LIMIT__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INTERVALTYPE getINTERVALTYPE() {
		return iNTERVALTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setINTERVALTYPE(INTERVALTYPE newINTERVALTYPE) {
		INTERVALTYPE oldINTERVALTYPE = iNTERVALTYPE;
		iNTERVALTYPE = newINTERVALTYPE == null ? INTERVALTYPE_EDEFAULT : newINTERVALTYPE;
		boolean oldINTERVALTYPEESet = iNTERVALTYPEESet;
		iNTERVALTYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LIMIT__INTERVALTYPE, oldINTERVALTYPE, iNTERVALTYPE, !oldINTERVALTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetINTERVALTYPE() {
		INTERVALTYPE oldINTERVALTYPE = iNTERVALTYPE;
		boolean oldINTERVALTYPEESet = iNTERVALTYPEESet;
		iNTERVALTYPE = INTERVALTYPE_EDEFAULT;
		iNTERVALTYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.LIMIT__INTERVALTYPE, oldINTERVALTYPE, INTERVALTYPE_EDEFAULT, oldINTERVALTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetINTERVALTYPE() {
		return iNTERVALTYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.LIMIT__VALUE:
				return getValue();
			case OdxXhtmlPackage.LIMIT__INTERVALTYPE:
				return getINTERVALTYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.LIMIT__VALUE:
				setValue((String)newValue);
				return;
			case OdxXhtmlPackage.LIMIT__INTERVALTYPE:
				setINTERVALTYPE((INTERVALTYPE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LIMIT__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.LIMIT__INTERVALTYPE:
				unsetINTERVALTYPE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LIMIT__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case OdxXhtmlPackage.LIMIT__INTERVALTYPE:
				return isSetINTERVALTYPE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", iNTERVALTYPE: ");
		if (iNTERVALTYPEESet) result.append(iNTERVALTYPE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //LIMITImpl
