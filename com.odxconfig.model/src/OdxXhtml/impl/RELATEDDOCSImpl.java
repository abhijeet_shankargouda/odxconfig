/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.RELATEDDOC;
import OdxXhtml.RELATEDDOCS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RELATEDDOCS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.RELATEDDOCSImpl#getRELATEDDOC <em>RELATEDDOC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RELATEDDOCSImpl extends MinimalEObjectImpl.Container implements RELATEDDOCS {
	/**
	 * The cached value of the '{@link #getRELATEDDOC() <em>RELATEDDOC</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRELATEDDOC()
	 * @generated
	 * @ordered
	 */
	protected EList<RELATEDDOC> rELATEDDOC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RELATEDDOCSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getRELATEDDOCS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RELATEDDOC> getRELATEDDOC() {
		if (rELATEDDOC == null) {
			rELATEDDOC = new EObjectContainmentEList<RELATEDDOC>(RELATEDDOC.class, this, OdxXhtmlPackage.RELATEDDOCS__RELATEDDOC);
		}
		return rELATEDDOC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS__RELATEDDOC:
				return ((InternalEList<?>)getRELATEDDOC()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS__RELATEDDOC:
				return getRELATEDDOC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS__RELATEDDOC:
				getRELATEDDOC().clear();
				getRELATEDDOC().addAll((Collection<? extends RELATEDDOC>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS__RELATEDDOC:
				getRELATEDDOC().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS__RELATEDDOC:
				return rELATEDDOC != null && !rELATEDDOC.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RELATEDDOCSImpl
