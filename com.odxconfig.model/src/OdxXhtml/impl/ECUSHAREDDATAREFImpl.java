/**
 */
package OdxXhtml.impl;

import OdxXhtml.ECUSHAREDDATAREF;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUSHAREDDATAREF</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ECUSHAREDDATAREFImpl extends PARENTREFImpl implements ECUSHAREDDATAREF {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUSHAREDDATAREFImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUSHAREDDATAREF();
	}

} //ECUSHAREDDATAREFImpl
