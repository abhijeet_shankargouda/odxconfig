/**
 */
package OdxXhtml.impl;

import OdxXhtml.LiType;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Li Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LiTypeImpl extends FlowImpl implements LiType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LiTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLiType();
	}

} //LiTypeImpl
