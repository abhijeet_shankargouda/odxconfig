/**
 */
package OdxXhtml.impl;

import OdxXhtml.CHECKSUMRESULT;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SESSIONSUBELEMTYPE;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CHECKSUMRESULT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.CHECKSUMRESULTImpl#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMRESULTImpl#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CHECKSUMRESULTImpl extends MinimalEObjectImpl.Container implements CHECKSUMRESULT {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final SESSIONSUBELEMTYPE TYPE_EDEFAULT = SESSIONSUBELEMTYPE.AUINT32;

	/**
	 * The cached value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected SESSIONSUBELEMTYPE tYPE = TYPE_EDEFAULT;

	/**
	 * This is true if the TYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tYPEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CHECKSUMRESULTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCHECKSUMRESULT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUMRESULT__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SESSIONSUBELEMTYPE getTYPE() {
		return tYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTYPE(SESSIONSUBELEMTYPE newTYPE) {
		SESSIONSUBELEMTYPE oldTYPE = tYPE;
		tYPE = newTYPE == null ? TYPE_EDEFAULT : newTYPE;
		boolean oldTYPEESet = tYPEESet;
		tYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUMRESULT__TYPE, oldTYPE, tYPE, !oldTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetTYPE() {
		SESSIONSUBELEMTYPE oldTYPE = tYPE;
		boolean oldTYPEESet = tYPEESet;
		tYPE = TYPE_EDEFAULT;
		tYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.CHECKSUMRESULT__TYPE, oldTYPE, TYPE_EDEFAULT, oldTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetTYPE() {
		return tYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUMRESULT__VALUE:
				return getValue();
			case OdxXhtmlPackage.CHECKSUMRESULT__TYPE:
				return getTYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUMRESULT__VALUE:
				setValue((String)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUMRESULT__TYPE:
				setTYPE((SESSIONSUBELEMTYPE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUMRESULT__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.CHECKSUMRESULT__TYPE:
				unsetTYPE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUMRESULT__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case OdxXhtmlPackage.CHECKSUMRESULT__TYPE:
				return isSetTYPE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", tYPE: ");
		if (tYPEESet) result.append(tYPE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //CHECKSUMRESULTImpl
