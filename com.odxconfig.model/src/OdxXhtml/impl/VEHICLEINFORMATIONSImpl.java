/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.VEHICLEINFORMATION;
import OdxXhtml.VEHICLEINFORMATIONS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VEHICLEINFORMATIONS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONSImpl#getVEHICLEINFORMATION <em>VEHICLEINFORMATION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VEHICLEINFORMATIONSImpl extends MinimalEObjectImpl.Container implements VEHICLEINFORMATIONS {
	/**
	 * The cached value of the '{@link #getVEHICLEINFORMATION() <em>VEHICLEINFORMATION</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVEHICLEINFORMATION()
	 * @generated
	 * @ordered
	 */
	protected EList<VEHICLEINFORMATION> vEHICLEINFORMATION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VEHICLEINFORMATIONSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVEHICLEINFORMATIONS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<VEHICLEINFORMATION> getVEHICLEINFORMATION() {
		if (vEHICLEINFORMATION == null) {
			vEHICLEINFORMATION = new EObjectContainmentEList<VEHICLEINFORMATION>(VEHICLEINFORMATION.class, this, OdxXhtmlPackage.VEHICLEINFORMATIONS__VEHICLEINFORMATION);
		}
		return vEHICLEINFORMATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATIONS__VEHICLEINFORMATION:
				return ((InternalEList<?>)getVEHICLEINFORMATION()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATIONS__VEHICLEINFORMATION:
				return getVEHICLEINFORMATION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATIONS__VEHICLEINFORMATION:
				getVEHICLEINFORMATION().clear();
				getVEHICLEINFORMATION().addAll((Collection<? extends VEHICLEINFORMATION>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATIONS__VEHICLEINFORMATION:
				getVEHICLEINFORMATION().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATIONS__VEHICLEINFORMATION:
				return vEHICLEINFORMATION != null && !vEHICLEINFORMATION.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //VEHICLEINFORMATIONSImpl
