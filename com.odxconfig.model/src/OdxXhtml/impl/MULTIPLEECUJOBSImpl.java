/**
 */
package OdxXhtml.impl;

import OdxXhtml.MULTIPLEECUJOB;
import OdxXhtml.MULTIPLEECUJOBS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MULTIPLEECUJOBS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBSImpl#getMULTIPLEECUJOB <em>MULTIPLEECUJOB</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MULTIPLEECUJOBSImpl extends MinimalEObjectImpl.Container implements MULTIPLEECUJOBS {
	/**
	 * The cached value of the '{@link #getMULTIPLEECUJOB() <em>MULTIPLEECUJOB</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMULTIPLEECUJOB()
	 * @generated
	 * @ordered
	 */
	protected EList<MULTIPLEECUJOB> mULTIPLEECUJOB;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MULTIPLEECUJOBSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMULTIPLEECUJOBS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<MULTIPLEECUJOB> getMULTIPLEECUJOB() {
		if (mULTIPLEECUJOB == null) {
			mULTIPLEECUJOB = new EObjectContainmentEList<MULTIPLEECUJOB>(MULTIPLEECUJOB.class, this, OdxXhtmlPackage.MULTIPLEECUJOBS__MULTIPLEECUJOB);
		}
		return mULTIPLEECUJOB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBS__MULTIPLEECUJOB:
				return ((InternalEList<?>)getMULTIPLEECUJOB()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBS__MULTIPLEECUJOB:
				return getMULTIPLEECUJOB();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBS__MULTIPLEECUJOB:
				getMULTIPLEECUJOB().clear();
				getMULTIPLEECUJOB().addAll((Collection<? extends MULTIPLEECUJOB>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBS__MULTIPLEECUJOB:
				getMULTIPLEECUJOB().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBS__MULTIPLEECUJOB:
				return mULTIPLEECUJOB != null && !mULTIPLEECUJOB.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MULTIPLEECUJOBSImpl
