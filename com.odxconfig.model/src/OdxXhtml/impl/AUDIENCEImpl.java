/**
 */
package OdxXhtml.impl;

import OdxXhtml.AUDIENCE;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AUDIENCE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.AUDIENCEImpl#isISAFTERMARKET <em>ISAFTERMARKET</em>}</li>
 *   <li>{@link OdxXhtml.impl.AUDIENCEImpl#isISAFTERSALES <em>ISAFTERSALES</em>}</li>
 *   <li>{@link OdxXhtml.impl.AUDIENCEImpl#isISDEVELOPMENT <em>ISDEVELOPMENT</em>}</li>
 *   <li>{@link OdxXhtml.impl.AUDIENCEImpl#isISMANUFACTURING <em>ISMANUFACTURING</em>}</li>
 *   <li>{@link OdxXhtml.impl.AUDIENCEImpl#isISSUPPLIER <em>ISSUPPLIER</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AUDIENCEImpl extends MinimalEObjectImpl.Container implements AUDIENCE {
	/**
	 * The default value of the '{@link #isISAFTERMARKET() <em>ISAFTERMARKET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISAFTERMARKET()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISAFTERMARKET_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isISAFTERMARKET() <em>ISAFTERMARKET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISAFTERMARKET()
	 * @generated
	 * @ordered
	 */
	protected boolean iSAFTERMARKET = ISAFTERMARKET_EDEFAULT;

	/**
	 * This is true if the ISAFTERMARKET attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSAFTERMARKETESet;

	/**
	 * The default value of the '{@link #isISAFTERSALES() <em>ISAFTERSALES</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISAFTERSALES()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISAFTERSALES_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isISAFTERSALES() <em>ISAFTERSALES</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISAFTERSALES()
	 * @generated
	 * @ordered
	 */
	protected boolean iSAFTERSALES = ISAFTERSALES_EDEFAULT;

	/**
	 * This is true if the ISAFTERSALES attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSAFTERSALESESet;

	/**
	 * The default value of the '{@link #isISDEVELOPMENT() <em>ISDEVELOPMENT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISDEVELOPMENT()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISDEVELOPMENT_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isISDEVELOPMENT() <em>ISDEVELOPMENT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISDEVELOPMENT()
	 * @generated
	 * @ordered
	 */
	protected boolean iSDEVELOPMENT = ISDEVELOPMENT_EDEFAULT;

	/**
	 * This is true if the ISDEVELOPMENT attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSDEVELOPMENTESet;

	/**
	 * The default value of the '{@link #isISMANUFACTURING() <em>ISMANUFACTURING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISMANUFACTURING()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISMANUFACTURING_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isISMANUFACTURING() <em>ISMANUFACTURING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISMANUFACTURING()
	 * @generated
	 * @ordered
	 */
	protected boolean iSMANUFACTURING = ISMANUFACTURING_EDEFAULT;

	/**
	 * This is true if the ISMANUFACTURING attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSMANUFACTURINGESet;

	/**
	 * The default value of the '{@link #isISSUPPLIER() <em>ISSUPPLIER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISSUPPLIER()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISSUPPLIER_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isISSUPPLIER() <em>ISSUPPLIER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISSUPPLIER()
	 * @generated
	 * @ordered
	 */
	protected boolean iSSUPPLIER = ISSUPPLIER_EDEFAULT;

	/**
	 * This is true if the ISSUPPLIER attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSSUPPLIERESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AUDIENCEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getAUDIENCE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISAFTERMARKET() {
		return iSAFTERMARKET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISAFTERMARKET(boolean newISAFTERMARKET) {
		boolean oldISAFTERMARKET = iSAFTERMARKET;
		iSAFTERMARKET = newISAFTERMARKET;
		boolean oldISAFTERMARKETESet = iSAFTERMARKETESet;
		iSAFTERMARKETESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.AUDIENCE__ISAFTERMARKET, oldISAFTERMARKET, iSAFTERMARKET, !oldISAFTERMARKETESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISAFTERMARKET() {
		boolean oldISAFTERMARKET = iSAFTERMARKET;
		boolean oldISAFTERMARKETESet = iSAFTERMARKETESet;
		iSAFTERMARKET = ISAFTERMARKET_EDEFAULT;
		iSAFTERMARKETESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.AUDIENCE__ISAFTERMARKET, oldISAFTERMARKET, ISAFTERMARKET_EDEFAULT, oldISAFTERMARKETESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISAFTERMARKET() {
		return iSAFTERMARKETESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISAFTERSALES() {
		return iSAFTERSALES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISAFTERSALES(boolean newISAFTERSALES) {
		boolean oldISAFTERSALES = iSAFTERSALES;
		iSAFTERSALES = newISAFTERSALES;
		boolean oldISAFTERSALESESet = iSAFTERSALESESet;
		iSAFTERSALESESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.AUDIENCE__ISAFTERSALES, oldISAFTERSALES, iSAFTERSALES, !oldISAFTERSALESESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISAFTERSALES() {
		boolean oldISAFTERSALES = iSAFTERSALES;
		boolean oldISAFTERSALESESet = iSAFTERSALESESet;
		iSAFTERSALES = ISAFTERSALES_EDEFAULT;
		iSAFTERSALESESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.AUDIENCE__ISAFTERSALES, oldISAFTERSALES, ISAFTERSALES_EDEFAULT, oldISAFTERSALESESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISAFTERSALES() {
		return iSAFTERSALESESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISDEVELOPMENT() {
		return iSDEVELOPMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISDEVELOPMENT(boolean newISDEVELOPMENT) {
		boolean oldISDEVELOPMENT = iSDEVELOPMENT;
		iSDEVELOPMENT = newISDEVELOPMENT;
		boolean oldISDEVELOPMENTESet = iSDEVELOPMENTESet;
		iSDEVELOPMENTESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.AUDIENCE__ISDEVELOPMENT, oldISDEVELOPMENT, iSDEVELOPMENT, !oldISDEVELOPMENTESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISDEVELOPMENT() {
		boolean oldISDEVELOPMENT = iSDEVELOPMENT;
		boolean oldISDEVELOPMENTESet = iSDEVELOPMENTESet;
		iSDEVELOPMENT = ISDEVELOPMENT_EDEFAULT;
		iSDEVELOPMENTESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.AUDIENCE__ISDEVELOPMENT, oldISDEVELOPMENT, ISDEVELOPMENT_EDEFAULT, oldISDEVELOPMENTESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISDEVELOPMENT() {
		return iSDEVELOPMENTESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISMANUFACTURING() {
		return iSMANUFACTURING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISMANUFACTURING(boolean newISMANUFACTURING) {
		boolean oldISMANUFACTURING = iSMANUFACTURING;
		iSMANUFACTURING = newISMANUFACTURING;
		boolean oldISMANUFACTURINGESet = iSMANUFACTURINGESet;
		iSMANUFACTURINGESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.AUDIENCE__ISMANUFACTURING, oldISMANUFACTURING, iSMANUFACTURING, !oldISMANUFACTURINGESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISMANUFACTURING() {
		boolean oldISMANUFACTURING = iSMANUFACTURING;
		boolean oldISMANUFACTURINGESet = iSMANUFACTURINGESet;
		iSMANUFACTURING = ISMANUFACTURING_EDEFAULT;
		iSMANUFACTURINGESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.AUDIENCE__ISMANUFACTURING, oldISMANUFACTURING, ISMANUFACTURING_EDEFAULT, oldISMANUFACTURINGESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISMANUFACTURING() {
		return iSMANUFACTURINGESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISSUPPLIER() {
		return iSSUPPLIER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISSUPPLIER(boolean newISSUPPLIER) {
		boolean oldISSUPPLIER = iSSUPPLIER;
		iSSUPPLIER = newISSUPPLIER;
		boolean oldISSUPPLIERESet = iSSUPPLIERESet;
		iSSUPPLIERESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.AUDIENCE__ISSUPPLIER, oldISSUPPLIER, iSSUPPLIER, !oldISSUPPLIERESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISSUPPLIER() {
		boolean oldISSUPPLIER = iSSUPPLIER;
		boolean oldISSUPPLIERESet = iSSUPPLIERESet;
		iSSUPPLIER = ISSUPPLIER_EDEFAULT;
		iSSUPPLIERESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.AUDIENCE__ISSUPPLIER, oldISSUPPLIER, ISSUPPLIER_EDEFAULT, oldISSUPPLIERESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISSUPPLIER() {
		return iSSUPPLIERESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.AUDIENCE__ISAFTERMARKET:
				return isISAFTERMARKET();
			case OdxXhtmlPackage.AUDIENCE__ISAFTERSALES:
				return isISAFTERSALES();
			case OdxXhtmlPackage.AUDIENCE__ISDEVELOPMENT:
				return isISDEVELOPMENT();
			case OdxXhtmlPackage.AUDIENCE__ISMANUFACTURING:
				return isISMANUFACTURING();
			case OdxXhtmlPackage.AUDIENCE__ISSUPPLIER:
				return isISSUPPLIER();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.AUDIENCE__ISAFTERMARKET:
				setISAFTERMARKET((Boolean)newValue);
				return;
			case OdxXhtmlPackage.AUDIENCE__ISAFTERSALES:
				setISAFTERSALES((Boolean)newValue);
				return;
			case OdxXhtmlPackage.AUDIENCE__ISDEVELOPMENT:
				setISDEVELOPMENT((Boolean)newValue);
				return;
			case OdxXhtmlPackage.AUDIENCE__ISMANUFACTURING:
				setISMANUFACTURING((Boolean)newValue);
				return;
			case OdxXhtmlPackage.AUDIENCE__ISSUPPLIER:
				setISSUPPLIER((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.AUDIENCE__ISAFTERMARKET:
				unsetISAFTERMARKET();
				return;
			case OdxXhtmlPackage.AUDIENCE__ISAFTERSALES:
				unsetISAFTERSALES();
				return;
			case OdxXhtmlPackage.AUDIENCE__ISDEVELOPMENT:
				unsetISDEVELOPMENT();
				return;
			case OdxXhtmlPackage.AUDIENCE__ISMANUFACTURING:
				unsetISMANUFACTURING();
				return;
			case OdxXhtmlPackage.AUDIENCE__ISSUPPLIER:
				unsetISSUPPLIER();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.AUDIENCE__ISAFTERMARKET:
				return isSetISAFTERMARKET();
			case OdxXhtmlPackage.AUDIENCE__ISAFTERSALES:
				return isSetISAFTERSALES();
			case OdxXhtmlPackage.AUDIENCE__ISDEVELOPMENT:
				return isSetISDEVELOPMENT();
			case OdxXhtmlPackage.AUDIENCE__ISMANUFACTURING:
				return isSetISMANUFACTURING();
			case OdxXhtmlPackage.AUDIENCE__ISSUPPLIER:
				return isSetISSUPPLIER();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (iSAFTERMARKET: ");
		if (iSAFTERMARKETESet) result.append(iSAFTERMARKET); else result.append("<unset>");
		result.append(", iSAFTERSALES: ");
		if (iSAFTERSALESESet) result.append(iSAFTERSALES); else result.append("<unset>");
		result.append(", iSDEVELOPMENT: ");
		if (iSDEVELOPMENTESet) result.append(iSDEVELOPMENT); else result.append("<unset>");
		result.append(", iSMANUFACTURING: ");
		if (iSMANUFACTURINGESet) result.append(iSMANUFACTURING); else result.append("<unset>");
		result.append(", iSSUPPLIER: ");
		if (iSSUPPLIERESet) result.append(iSSUPPLIER); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //AUDIENCEImpl
