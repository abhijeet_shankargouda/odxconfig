/**
 */
package OdxXhtml.impl;

import OdxXhtml.LENGTHKEY;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LENGTHKEY</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.LENGTHKEYImpl#getDOPREF <em>DOPREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.LENGTHKEYImpl#getDOPSNREF <em>DOPSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.LENGTHKEYImpl#getID <em>ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LENGTHKEYImpl extends POSITIONABLEPARAMImpl implements LENGTHKEY {
	/**
	 * The cached value of the '{@link #getDOPREF() <em>DOPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOPREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK dOPREF;

	/**
	 * The cached value of the '{@link #getDOPSNREF() <em>DOPSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOPSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dOPSNREF;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LENGTHKEYImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLENGTHKEY();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getDOPREF() {
		return dOPREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDOPREF(ODXLINK newDOPREF, NotificationChain msgs) {
		ODXLINK oldDOPREF = dOPREF;
		dOPREF = newDOPREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LENGTHKEY__DOPREF, oldDOPREF, newDOPREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOPREF(ODXLINK newDOPREF) {
		if (newDOPREF != dOPREF) {
			NotificationChain msgs = null;
			if (dOPREF != null)
				msgs = ((InternalEObject)dOPREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LENGTHKEY__DOPREF, null, msgs);
			if (newDOPREF != null)
				msgs = ((InternalEObject)newDOPREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LENGTHKEY__DOPREF, null, msgs);
			msgs = basicSetDOPREF(newDOPREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LENGTHKEY__DOPREF, newDOPREF, newDOPREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDOPSNREF() {
		return dOPSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDOPSNREF(SNREF newDOPSNREF, NotificationChain msgs) {
		SNREF oldDOPSNREF = dOPSNREF;
		dOPSNREF = newDOPSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LENGTHKEY__DOPSNREF, oldDOPSNREF, newDOPSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOPSNREF(SNREF newDOPSNREF) {
		if (newDOPSNREF != dOPSNREF) {
			NotificationChain msgs = null;
			if (dOPSNREF != null)
				msgs = ((InternalEObject)dOPSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LENGTHKEY__DOPSNREF, null, msgs);
			if (newDOPSNREF != null)
				msgs = ((InternalEObject)newDOPSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LENGTHKEY__DOPSNREF, null, msgs);
			msgs = basicSetDOPSNREF(newDOPSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LENGTHKEY__DOPSNREF, newDOPSNREF, newDOPSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LENGTHKEY__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.LENGTHKEY__DOPREF:
				return basicSetDOPREF(null, msgs);
			case OdxXhtmlPackage.LENGTHKEY__DOPSNREF:
				return basicSetDOPSNREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.LENGTHKEY__DOPREF:
				return getDOPREF();
			case OdxXhtmlPackage.LENGTHKEY__DOPSNREF:
				return getDOPSNREF();
			case OdxXhtmlPackage.LENGTHKEY__ID:
				return getID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.LENGTHKEY__DOPREF:
				setDOPREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.LENGTHKEY__DOPSNREF:
				setDOPSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.LENGTHKEY__ID:
				setID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LENGTHKEY__DOPREF:
				setDOPREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.LENGTHKEY__DOPSNREF:
				setDOPSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.LENGTHKEY__ID:
				setID(ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LENGTHKEY__DOPREF:
				return dOPREF != null;
			case OdxXhtmlPackage.LENGTHKEY__DOPSNREF:
				return dOPSNREF != null;
			case OdxXhtmlPackage.LENGTHKEY__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (iD: ");
		result.append(iD);
		result.append(')');
		return result.toString();
	}

} //LENGTHKEYImpl
