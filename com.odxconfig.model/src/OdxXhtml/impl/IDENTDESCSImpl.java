/**
 */
package OdxXhtml.impl;

import OdxXhtml.IDENTDESC;
import OdxXhtml.IDENTDESCS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IDENTDESCS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.IDENTDESCSImpl#getIDENTDESC <em>IDENTDESC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IDENTDESCSImpl extends MinimalEObjectImpl.Container implements IDENTDESCS {
	/**
	 * The cached value of the '{@link #getIDENTDESC() <em>IDENTDESC</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDENTDESC()
	 * @generated
	 * @ordered
	 */
	protected EList<IDENTDESC> iDENTDESC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IDENTDESCSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getIDENTDESCS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IDENTDESC> getIDENTDESC() {
		if (iDENTDESC == null) {
			iDENTDESC = new EObjectContainmentEList<IDENTDESC>(IDENTDESC.class, this, OdxXhtmlPackage.IDENTDESCS__IDENTDESC);
		}
		return iDENTDESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESCS__IDENTDESC:
				return ((InternalEList<?>)getIDENTDESC()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESCS__IDENTDESC:
				return getIDENTDESC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESCS__IDENTDESC:
				getIDENTDESC().clear();
				getIDENTDESC().addAll((Collection<? extends IDENTDESC>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESCS__IDENTDESC:
				getIDENTDESC().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESCS__IDENTDESC:
				return iDENTDESC != null && !iDENTDESC.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IDENTDESCSImpl
