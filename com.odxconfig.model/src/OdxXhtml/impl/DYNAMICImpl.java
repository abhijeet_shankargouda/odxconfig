/**
 */
package OdxXhtml.impl;

import OdxXhtml.DYNAMIC;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DYNAMIC</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DYNAMICImpl extends POSITIONABLEPARAMImpl implements DYNAMIC {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DYNAMICImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDYNAMIC();
	}

} //DYNAMICImpl
