/**
 */
package OdxXhtml.impl;

import OdxXhtml.ECUMEMCONNECTORS;
import OdxXhtml.ECUMEMS;
import OdxXhtml.FLASH;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FLASH</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FLASHImpl#getECUMEMS <em>ECUMEMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.FLASHImpl#getECUMEMCONNECTORS <em>ECUMEMCONNECTORS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FLASHImpl extends ODXCATEGORYImpl implements FLASH {
	/**
	 * The cached value of the '{@link #getECUMEMS() <em>ECUMEMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUMEMS()
	 * @generated
	 * @ordered
	 */
	protected ECUMEMS eCUMEMS;

	/**
	 * The cached value of the '{@link #getECUMEMCONNECTORS() <em>ECUMEMCONNECTORS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUMEMCONNECTORS()
	 * @generated
	 * @ordered
	 */
	protected ECUMEMCONNECTORS eCUMEMCONNECTORS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FLASHImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFLASH();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUMEMS getECUMEMS() {
		return eCUMEMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetECUMEMS(ECUMEMS newECUMEMS, NotificationChain msgs) {
		ECUMEMS oldECUMEMS = eCUMEMS;
		eCUMEMS = newECUMEMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASH__ECUMEMS, oldECUMEMS, newECUMEMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setECUMEMS(ECUMEMS newECUMEMS) {
		if (newECUMEMS != eCUMEMS) {
			NotificationChain msgs = null;
			if (eCUMEMS != null)
				msgs = ((InternalEObject)eCUMEMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASH__ECUMEMS, null, msgs);
			if (newECUMEMS != null)
				msgs = ((InternalEObject)newECUMEMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASH__ECUMEMS, null, msgs);
			msgs = basicSetECUMEMS(newECUMEMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASH__ECUMEMS, newECUMEMS, newECUMEMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUMEMCONNECTORS getECUMEMCONNECTORS() {
		return eCUMEMCONNECTORS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetECUMEMCONNECTORS(ECUMEMCONNECTORS newECUMEMCONNECTORS, NotificationChain msgs) {
		ECUMEMCONNECTORS oldECUMEMCONNECTORS = eCUMEMCONNECTORS;
		eCUMEMCONNECTORS = newECUMEMCONNECTORS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASH__ECUMEMCONNECTORS, oldECUMEMCONNECTORS, newECUMEMCONNECTORS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setECUMEMCONNECTORS(ECUMEMCONNECTORS newECUMEMCONNECTORS) {
		if (newECUMEMCONNECTORS != eCUMEMCONNECTORS) {
			NotificationChain msgs = null;
			if (eCUMEMCONNECTORS != null)
				msgs = ((InternalEObject)eCUMEMCONNECTORS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASH__ECUMEMCONNECTORS, null, msgs);
			if (newECUMEMCONNECTORS != null)
				msgs = ((InternalEObject)newECUMEMCONNECTORS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASH__ECUMEMCONNECTORS, null, msgs);
			msgs = basicSetECUMEMCONNECTORS(newECUMEMCONNECTORS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASH__ECUMEMCONNECTORS, newECUMEMCONNECTORS, newECUMEMCONNECTORS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASH__ECUMEMS:
				return basicSetECUMEMS(null, msgs);
			case OdxXhtmlPackage.FLASH__ECUMEMCONNECTORS:
				return basicSetECUMEMCONNECTORS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASH__ECUMEMS:
				return getECUMEMS();
			case OdxXhtmlPackage.FLASH__ECUMEMCONNECTORS:
				return getECUMEMCONNECTORS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASH__ECUMEMS:
				setECUMEMS((ECUMEMS)newValue);
				return;
			case OdxXhtmlPackage.FLASH__ECUMEMCONNECTORS:
				setECUMEMCONNECTORS((ECUMEMCONNECTORS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASH__ECUMEMS:
				setECUMEMS((ECUMEMS)null);
				return;
			case OdxXhtmlPackage.FLASH__ECUMEMCONNECTORS:
				setECUMEMCONNECTORS((ECUMEMCONNECTORS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASH__ECUMEMS:
				return eCUMEMS != null;
			case OdxXhtmlPackage.FLASH__ECUMEMCONNECTORS:
				return eCUMEMCONNECTORS != null;
		}
		return super.eIsSet(featureID);
	}

} //FLASHImpl
