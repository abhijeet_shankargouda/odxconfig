/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.POSITIONABLEPARAM;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>POSITIONABLEPARAM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.POSITIONABLEPARAMImpl#getBYTEPOSITION <em>BYTEPOSITION</em>}</li>
 *   <li>{@link OdxXhtml.impl.POSITIONABLEPARAMImpl#getBITPOSITION <em>BITPOSITION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class POSITIONABLEPARAMImpl extends PARAMImpl implements POSITIONABLEPARAM {
	/**
	 * The default value of the '{@link #getBYTEPOSITION() <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTEPOSITION()
	 * @generated
	 * @ordered
	 */
	protected static final long BYTEPOSITION_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBYTEPOSITION() <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTEPOSITION()
	 * @generated
	 * @ordered
	 */
	protected long bYTEPOSITION = BYTEPOSITION_EDEFAULT;

	/**
	 * This is true if the BYTEPOSITION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bYTEPOSITIONESet;

	/**
	 * The default value of the '{@link #getBITPOSITION() <em>BITPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITPOSITION()
	 * @generated
	 * @ordered
	 */
	protected static final long BITPOSITION_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBITPOSITION() <em>BITPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITPOSITION()
	 * @generated
	 * @ordered
	 */
	protected long bITPOSITION = BITPOSITION_EDEFAULT;

	/**
	 * This is true if the BITPOSITION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bITPOSITIONESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected POSITIONABLEPARAMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPOSITIONABLEPARAM();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBYTEPOSITION() {
		return bYTEPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBYTEPOSITION(long newBYTEPOSITION) {
		long oldBYTEPOSITION = bYTEPOSITION;
		bYTEPOSITION = newBYTEPOSITION;
		boolean oldBYTEPOSITIONESet = bYTEPOSITIONESet;
		bYTEPOSITIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.POSITIONABLEPARAM__BYTEPOSITION, oldBYTEPOSITION, bYTEPOSITION, !oldBYTEPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBYTEPOSITION() {
		long oldBYTEPOSITION = bYTEPOSITION;
		boolean oldBYTEPOSITIONESet = bYTEPOSITIONESet;
		bYTEPOSITION = BYTEPOSITION_EDEFAULT;
		bYTEPOSITIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.POSITIONABLEPARAM__BYTEPOSITION, oldBYTEPOSITION, BYTEPOSITION_EDEFAULT, oldBYTEPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBYTEPOSITION() {
		return bYTEPOSITIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBITPOSITION() {
		return bITPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBITPOSITION(long newBITPOSITION) {
		long oldBITPOSITION = bITPOSITION;
		bITPOSITION = newBITPOSITION;
		boolean oldBITPOSITIONESet = bITPOSITIONESet;
		bITPOSITIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.POSITIONABLEPARAM__BITPOSITION, oldBITPOSITION, bITPOSITION, !oldBITPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBITPOSITION() {
		long oldBITPOSITION = bITPOSITION;
		boolean oldBITPOSITIONESet = bITPOSITIONESet;
		bITPOSITION = BITPOSITION_EDEFAULT;
		bITPOSITIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.POSITIONABLEPARAM__BITPOSITION, oldBITPOSITION, BITPOSITION_EDEFAULT, oldBITPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBITPOSITION() {
		return bITPOSITIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.POSITIONABLEPARAM__BYTEPOSITION:
				return getBYTEPOSITION();
			case OdxXhtmlPackage.POSITIONABLEPARAM__BITPOSITION:
				return getBITPOSITION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.POSITIONABLEPARAM__BYTEPOSITION:
				setBYTEPOSITION((Long)newValue);
				return;
			case OdxXhtmlPackage.POSITIONABLEPARAM__BITPOSITION:
				setBITPOSITION((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.POSITIONABLEPARAM__BYTEPOSITION:
				unsetBYTEPOSITION();
				return;
			case OdxXhtmlPackage.POSITIONABLEPARAM__BITPOSITION:
				unsetBITPOSITION();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.POSITIONABLEPARAM__BYTEPOSITION:
				return isSetBYTEPOSITION();
			case OdxXhtmlPackage.POSITIONABLEPARAM__BITPOSITION:
				return isSetBITPOSITION();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bYTEPOSITION: ");
		if (bYTEPOSITIONESet) result.append(bYTEPOSITION); else result.append("<unset>");
		result.append(", bITPOSITION: ");
		if (bITPOSITIONESet) result.append(bITPOSITION); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //POSITIONABLEPARAMImpl
