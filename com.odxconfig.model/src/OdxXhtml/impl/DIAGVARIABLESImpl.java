/**
 */
package OdxXhtml.impl;

import OdxXhtml.DIAGVARIABLE;
import OdxXhtml.DIAGVARIABLES;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGVARIABLES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLESImpl#getDIAGVARIABLEPROXY <em>DIAGVARIABLEPROXY</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLESImpl#getDIAGVARIABLEREF <em>DIAGVARIABLEREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLESImpl#getDIAGVARIABLE <em>DIAGVARIABLE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGVARIABLESImpl extends MinimalEObjectImpl.Container implements DIAGVARIABLES {
	/**
	 * The cached value of the '{@link #getDIAGVARIABLEPROXY() <em>DIAGVARIABLEPROXY</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGVARIABLEPROXY()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap dIAGVARIABLEPROXY;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGVARIABLESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGVARIABLES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getDIAGVARIABLEPROXY() {
		if (dIAGVARIABLEPROXY == null) {
			dIAGVARIABLEPROXY = new BasicFeatureMap(this, OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEPROXY);
		}
		return dIAGVARIABLEPROXY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getDIAGVARIABLEREF() {
		return getDIAGVARIABLEPROXY().list(OdxXhtmlPackage.eINSTANCE.getDIAGVARIABLES_DIAGVARIABLEREF());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DIAGVARIABLE> getDIAGVARIABLE() {
		return getDIAGVARIABLEPROXY().list(OdxXhtmlPackage.eINSTANCE.getDIAGVARIABLES_DIAGVARIABLE());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEPROXY:
				return ((InternalEList<?>)getDIAGVARIABLEPROXY()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEREF:
				return ((InternalEList<?>)getDIAGVARIABLEREF()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLE:
				return ((InternalEList<?>)getDIAGVARIABLE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEPROXY:
				if (coreType) return getDIAGVARIABLEPROXY();
				return ((FeatureMap.Internal)getDIAGVARIABLEPROXY()).getWrapper();
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEREF:
				return getDIAGVARIABLEREF();
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLE:
				return getDIAGVARIABLE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEPROXY:
				((FeatureMap.Internal)getDIAGVARIABLEPROXY()).set(newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEREF:
				getDIAGVARIABLEREF().clear();
				getDIAGVARIABLEREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLE:
				getDIAGVARIABLE().clear();
				getDIAGVARIABLE().addAll((Collection<? extends DIAGVARIABLE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEPROXY:
				getDIAGVARIABLEPROXY().clear();
				return;
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEREF:
				getDIAGVARIABLEREF().clear();
				return;
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLE:
				getDIAGVARIABLE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEPROXY:
				return dIAGVARIABLEPROXY != null && !dIAGVARIABLEPROXY.isEmpty();
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLEREF:
				return !getDIAGVARIABLEREF().isEmpty();
			case OdxXhtmlPackage.DIAGVARIABLES__DIAGVARIABLE:
				return !getDIAGVARIABLE().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (dIAGVARIABLEPROXY: ");
		result.append(dIAGVARIABLEPROXY);
		result.append(')');
		return result.toString();
	}

} //DIAGVARIABLESImpl
