/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.DATAOBJECTPROPS;
import OdxXhtml.DIAGDATADICTIONARYSPEC;
import OdxXhtml.DTCDOPS;
import OdxXhtml.DYNAMICENDMARKERFIELDS;
import OdxXhtml.DYNAMICLENGTHFIELDS;
import OdxXhtml.ENDOFPDUFIELDS;
import OdxXhtml.ENVDATADESCS;
import OdxXhtml.ENVDATAS;
import OdxXhtml.MUXS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.STATICFIELDS;
import OdxXhtml.STRUCTURES;
import OdxXhtml.TABLES;
import OdxXhtml.UNITSPEC;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGDATADICTIONARYSPEC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getDTCDOPS <em>DTCDOPS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getENVDATADESCS <em>ENVDATADESCS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getDATAOBJECTPROPS <em>DATAOBJECTPROPS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getSTRUCTURES <em>STRUCTURES</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getSTATICFIELDS <em>STATICFIELDS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getDYNAMICLENGTHFIELDS <em>DYNAMICLENGTHFIELDS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getDYNAMICENDMARKERFIELDS <em>DYNAMICENDMARKERFIELDS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getENDOFPDUFIELDS <em>ENDOFPDUFIELDS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getMUXS <em>MUXS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getENVDATAS <em>ENVDATAS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getUNITSPEC <em>UNITSPEC</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGDATADICTIONARYSPECImpl#getTABLES <em>TABLES</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGDATADICTIONARYSPECImpl extends MinimalEObjectImpl.Container implements DIAGDATADICTIONARYSPEC {
	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getDTCDOPS() <em>DTCDOPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDTCDOPS()
	 * @generated
	 * @ordered
	 */
	protected DTCDOPS dTCDOPS;

	/**
	 * The cached value of the '{@link #getENVDATADESCS() <em>ENVDATADESCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENVDATADESCS()
	 * @generated
	 * @ordered
	 */
	protected ENVDATADESCS eNVDATADESCS;

	/**
	 * The cached value of the '{@link #getDATAOBJECTPROPS() <em>DATAOBJECTPROPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATAOBJECTPROPS()
	 * @generated
	 * @ordered
	 */
	protected DATAOBJECTPROPS dATAOBJECTPROPS;

	/**
	 * The cached value of the '{@link #getSTRUCTURES() <em>STRUCTURES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTRUCTURES()
	 * @generated
	 * @ordered
	 */
	protected STRUCTURES sTRUCTURES;

	/**
	 * The cached value of the '{@link #getSTATICFIELDS() <em>STATICFIELDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTATICFIELDS()
	 * @generated
	 * @ordered
	 */
	protected STATICFIELDS sTATICFIELDS;

	/**
	 * The cached value of the '{@link #getDYNAMICLENGTHFIELDS() <em>DYNAMICLENGTHFIELDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNAMICLENGTHFIELDS()
	 * @generated
	 * @ordered
	 */
	protected DYNAMICLENGTHFIELDS dYNAMICLENGTHFIELDS;

	/**
	 * The cached value of the '{@link #getDYNAMICENDMARKERFIELDS() <em>DYNAMICENDMARKERFIELDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNAMICENDMARKERFIELDS()
	 * @generated
	 * @ordered
	 */
	protected DYNAMICENDMARKERFIELDS dYNAMICENDMARKERFIELDS;

	/**
	 * The cached value of the '{@link #getENDOFPDUFIELDS() <em>ENDOFPDUFIELDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENDOFPDUFIELDS()
	 * @generated
	 * @ordered
	 */
	protected ENDOFPDUFIELDS eNDOFPDUFIELDS;

	/**
	 * The cached value of the '{@link #getMUXS() <em>MUXS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMUXS()
	 * @generated
	 * @ordered
	 */
	protected MUXS mUXS;

	/**
	 * The cached value of the '{@link #getENVDATAS() <em>ENVDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENVDATAS()
	 * @generated
	 * @ordered
	 */
	protected ENVDATAS eNVDATAS;

	/**
	 * The cached value of the '{@link #getUNITSPEC() <em>UNITSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUNITSPEC()
	 * @generated
	 * @ordered
	 */
	protected UNITSPEC uNITSPEC;

	/**
	 * The cached value of the '{@link #getTABLES() <em>TABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTABLES()
	 * @generated
	 * @ordered
	 */
	protected TABLES tABLES;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGDATADICTIONARYSPECImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGDATADICTIONARYSPEC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DTCDOPS getDTCDOPS() {
		return dTCDOPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDTCDOPS(DTCDOPS newDTCDOPS, NotificationChain msgs) {
		DTCDOPS oldDTCDOPS = dTCDOPS;
		dTCDOPS = newDTCDOPS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DTCDOPS, oldDTCDOPS, newDTCDOPS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDTCDOPS(DTCDOPS newDTCDOPS) {
		if (newDTCDOPS != dTCDOPS) {
			NotificationChain msgs = null;
			if (dTCDOPS != null)
				msgs = ((InternalEObject)dTCDOPS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DTCDOPS, null, msgs);
			if (newDTCDOPS != null)
				msgs = ((InternalEObject)newDTCDOPS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DTCDOPS, null, msgs);
			msgs = basicSetDTCDOPS(newDTCDOPS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DTCDOPS, newDTCDOPS, newDTCDOPS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENVDATADESCS getENVDATADESCS() {
		return eNVDATADESCS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetENVDATADESCS(ENVDATADESCS newENVDATADESCS, NotificationChain msgs) {
		ENVDATADESCS oldENVDATADESCS = eNVDATADESCS;
		eNVDATADESCS = newENVDATADESCS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATADESCS, oldENVDATADESCS, newENVDATADESCS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setENVDATADESCS(ENVDATADESCS newENVDATADESCS) {
		if (newENVDATADESCS != eNVDATADESCS) {
			NotificationChain msgs = null;
			if (eNVDATADESCS != null)
				msgs = ((InternalEObject)eNVDATADESCS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATADESCS, null, msgs);
			if (newENVDATADESCS != null)
				msgs = ((InternalEObject)newENVDATADESCS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATADESCS, null, msgs);
			msgs = basicSetENVDATADESCS(newENVDATADESCS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATADESCS, newENVDATADESCS, newENVDATADESCS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATAOBJECTPROPS getDATAOBJECTPROPS() {
		return dATAOBJECTPROPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDATAOBJECTPROPS(DATAOBJECTPROPS newDATAOBJECTPROPS, NotificationChain msgs) {
		DATAOBJECTPROPS oldDATAOBJECTPROPS = dATAOBJECTPROPS;
		dATAOBJECTPROPS = newDATAOBJECTPROPS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DATAOBJECTPROPS, oldDATAOBJECTPROPS, newDATAOBJECTPROPS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATAOBJECTPROPS(DATAOBJECTPROPS newDATAOBJECTPROPS) {
		if (newDATAOBJECTPROPS != dATAOBJECTPROPS) {
			NotificationChain msgs = null;
			if (dATAOBJECTPROPS != null)
				msgs = ((InternalEObject)dATAOBJECTPROPS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DATAOBJECTPROPS, null, msgs);
			if (newDATAOBJECTPROPS != null)
				msgs = ((InternalEObject)newDATAOBJECTPROPS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DATAOBJECTPROPS, null, msgs);
			msgs = basicSetDATAOBJECTPROPS(newDATAOBJECTPROPS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DATAOBJECTPROPS, newDATAOBJECTPROPS, newDATAOBJECTPROPS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public STRUCTURES getSTRUCTURES() {
		return sTRUCTURES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSTRUCTURES(STRUCTURES newSTRUCTURES, NotificationChain msgs) {
		STRUCTURES oldSTRUCTURES = sTRUCTURES;
		sTRUCTURES = newSTRUCTURES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STRUCTURES, oldSTRUCTURES, newSTRUCTURES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSTRUCTURES(STRUCTURES newSTRUCTURES) {
		if (newSTRUCTURES != sTRUCTURES) {
			NotificationChain msgs = null;
			if (sTRUCTURES != null)
				msgs = ((InternalEObject)sTRUCTURES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STRUCTURES, null, msgs);
			if (newSTRUCTURES != null)
				msgs = ((InternalEObject)newSTRUCTURES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STRUCTURES, null, msgs);
			msgs = basicSetSTRUCTURES(newSTRUCTURES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STRUCTURES, newSTRUCTURES, newSTRUCTURES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public STATICFIELDS getSTATICFIELDS() {
		return sTATICFIELDS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSTATICFIELDS(STATICFIELDS newSTATICFIELDS, NotificationChain msgs) {
		STATICFIELDS oldSTATICFIELDS = sTATICFIELDS;
		sTATICFIELDS = newSTATICFIELDS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STATICFIELDS, oldSTATICFIELDS, newSTATICFIELDS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSTATICFIELDS(STATICFIELDS newSTATICFIELDS) {
		if (newSTATICFIELDS != sTATICFIELDS) {
			NotificationChain msgs = null;
			if (sTATICFIELDS != null)
				msgs = ((InternalEObject)sTATICFIELDS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STATICFIELDS, null, msgs);
			if (newSTATICFIELDS != null)
				msgs = ((InternalEObject)newSTATICFIELDS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STATICFIELDS, null, msgs);
			msgs = basicSetSTATICFIELDS(newSTATICFIELDS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STATICFIELDS, newSTATICFIELDS, newSTATICFIELDS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNAMICLENGTHFIELDS getDYNAMICLENGTHFIELDS() {
		return dYNAMICLENGTHFIELDS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDYNAMICLENGTHFIELDS(DYNAMICLENGTHFIELDS newDYNAMICLENGTHFIELDS, NotificationChain msgs) {
		DYNAMICLENGTHFIELDS oldDYNAMICLENGTHFIELDS = dYNAMICLENGTHFIELDS;
		dYNAMICLENGTHFIELDS = newDYNAMICLENGTHFIELDS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICLENGTHFIELDS, oldDYNAMICLENGTHFIELDS, newDYNAMICLENGTHFIELDS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDYNAMICLENGTHFIELDS(DYNAMICLENGTHFIELDS newDYNAMICLENGTHFIELDS) {
		if (newDYNAMICLENGTHFIELDS != dYNAMICLENGTHFIELDS) {
			NotificationChain msgs = null;
			if (dYNAMICLENGTHFIELDS != null)
				msgs = ((InternalEObject)dYNAMICLENGTHFIELDS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICLENGTHFIELDS, null, msgs);
			if (newDYNAMICLENGTHFIELDS != null)
				msgs = ((InternalEObject)newDYNAMICLENGTHFIELDS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICLENGTHFIELDS, null, msgs);
			msgs = basicSetDYNAMICLENGTHFIELDS(newDYNAMICLENGTHFIELDS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICLENGTHFIELDS, newDYNAMICLENGTHFIELDS, newDYNAMICLENGTHFIELDS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNAMICENDMARKERFIELDS getDYNAMICENDMARKERFIELDS() {
		return dYNAMICENDMARKERFIELDS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDYNAMICENDMARKERFIELDS(DYNAMICENDMARKERFIELDS newDYNAMICENDMARKERFIELDS, NotificationChain msgs) {
		DYNAMICENDMARKERFIELDS oldDYNAMICENDMARKERFIELDS = dYNAMICENDMARKERFIELDS;
		dYNAMICENDMARKERFIELDS = newDYNAMICENDMARKERFIELDS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICENDMARKERFIELDS, oldDYNAMICENDMARKERFIELDS, newDYNAMICENDMARKERFIELDS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDYNAMICENDMARKERFIELDS(DYNAMICENDMARKERFIELDS newDYNAMICENDMARKERFIELDS) {
		if (newDYNAMICENDMARKERFIELDS != dYNAMICENDMARKERFIELDS) {
			NotificationChain msgs = null;
			if (dYNAMICENDMARKERFIELDS != null)
				msgs = ((InternalEObject)dYNAMICENDMARKERFIELDS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICENDMARKERFIELDS, null, msgs);
			if (newDYNAMICENDMARKERFIELDS != null)
				msgs = ((InternalEObject)newDYNAMICENDMARKERFIELDS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICENDMARKERFIELDS, null, msgs);
			msgs = basicSetDYNAMICENDMARKERFIELDS(newDYNAMICENDMARKERFIELDS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICENDMARKERFIELDS, newDYNAMICENDMARKERFIELDS, newDYNAMICENDMARKERFIELDS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENDOFPDUFIELDS getENDOFPDUFIELDS() {
		return eNDOFPDUFIELDS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetENDOFPDUFIELDS(ENDOFPDUFIELDS newENDOFPDUFIELDS, NotificationChain msgs) {
		ENDOFPDUFIELDS oldENDOFPDUFIELDS = eNDOFPDUFIELDS;
		eNDOFPDUFIELDS = newENDOFPDUFIELDS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENDOFPDUFIELDS, oldENDOFPDUFIELDS, newENDOFPDUFIELDS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setENDOFPDUFIELDS(ENDOFPDUFIELDS newENDOFPDUFIELDS) {
		if (newENDOFPDUFIELDS != eNDOFPDUFIELDS) {
			NotificationChain msgs = null;
			if (eNDOFPDUFIELDS != null)
				msgs = ((InternalEObject)eNDOFPDUFIELDS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENDOFPDUFIELDS, null, msgs);
			if (newENDOFPDUFIELDS != null)
				msgs = ((InternalEObject)newENDOFPDUFIELDS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENDOFPDUFIELDS, null, msgs);
			msgs = basicSetENDOFPDUFIELDS(newENDOFPDUFIELDS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENDOFPDUFIELDS, newENDOFPDUFIELDS, newENDOFPDUFIELDS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MUXS getMUXS() {
		return mUXS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMUXS(MUXS newMUXS, NotificationChain msgs) {
		MUXS oldMUXS = mUXS;
		mUXS = newMUXS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__MUXS, oldMUXS, newMUXS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMUXS(MUXS newMUXS) {
		if (newMUXS != mUXS) {
			NotificationChain msgs = null;
			if (mUXS != null)
				msgs = ((InternalEObject)mUXS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__MUXS, null, msgs);
			if (newMUXS != null)
				msgs = ((InternalEObject)newMUXS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__MUXS, null, msgs);
			msgs = basicSetMUXS(newMUXS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__MUXS, newMUXS, newMUXS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENVDATAS getENVDATAS() {
		return eNVDATAS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetENVDATAS(ENVDATAS newENVDATAS, NotificationChain msgs) {
		ENVDATAS oldENVDATAS = eNVDATAS;
		eNVDATAS = newENVDATAS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATAS, oldENVDATAS, newENVDATAS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setENVDATAS(ENVDATAS newENVDATAS) {
		if (newENVDATAS != eNVDATAS) {
			NotificationChain msgs = null;
			if (eNVDATAS != null)
				msgs = ((InternalEObject)eNVDATAS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATAS, null, msgs);
			if (newENVDATAS != null)
				msgs = ((InternalEObject)newENVDATAS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATAS, null, msgs);
			msgs = basicSetENVDATAS(newENVDATAS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATAS, newENVDATAS, newENVDATAS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNITSPEC getUNITSPEC() {
		return uNITSPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUNITSPEC(UNITSPEC newUNITSPEC, NotificationChain msgs) {
		UNITSPEC oldUNITSPEC = uNITSPEC;
		uNITSPEC = newUNITSPEC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__UNITSPEC, oldUNITSPEC, newUNITSPEC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUNITSPEC(UNITSPEC newUNITSPEC) {
		if (newUNITSPEC != uNITSPEC) {
			NotificationChain msgs = null;
			if (uNITSPEC != null)
				msgs = ((InternalEObject)uNITSPEC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__UNITSPEC, null, msgs);
			if (newUNITSPEC != null)
				msgs = ((InternalEObject)newUNITSPEC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__UNITSPEC, null, msgs);
			msgs = basicSetUNITSPEC(newUNITSPEC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__UNITSPEC, newUNITSPEC, newUNITSPEC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TABLES getTABLES() {
		return tABLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTABLES(TABLES newTABLES, NotificationChain msgs) {
		TABLES oldTABLES = tABLES;
		tABLES = newTABLES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__TABLES, oldTABLES, newTABLES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTABLES(TABLES newTABLES) {
		if (newTABLES != tABLES) {
			NotificationChain msgs = null;
			if (tABLES != null)
				msgs = ((InternalEObject)tABLES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__TABLES, null, msgs);
			if (newTABLES != null)
				msgs = ((InternalEObject)newTABLES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__TABLES, null, msgs);
			msgs = basicSetTABLES(newTABLES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__TABLES, newTABLES, newTABLES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DTCDOPS:
				return basicSetDTCDOPS(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATADESCS:
				return basicSetENVDATADESCS(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DATAOBJECTPROPS:
				return basicSetDATAOBJECTPROPS(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STRUCTURES:
				return basicSetSTRUCTURES(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STATICFIELDS:
				return basicSetSTATICFIELDS(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICLENGTHFIELDS:
				return basicSetDYNAMICLENGTHFIELDS(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICENDMARKERFIELDS:
				return basicSetDYNAMICENDMARKERFIELDS(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENDOFPDUFIELDS:
				return basicSetENDOFPDUFIELDS(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__MUXS:
				return basicSetMUXS(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATAS:
				return basicSetENVDATAS(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__UNITSPEC:
				return basicSetUNITSPEC(null, msgs);
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__TABLES:
				return basicSetTABLES(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DTCDOPS:
				return getDTCDOPS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATADESCS:
				return getENVDATADESCS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DATAOBJECTPROPS:
				return getDATAOBJECTPROPS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STRUCTURES:
				return getSTRUCTURES();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STATICFIELDS:
				return getSTATICFIELDS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICLENGTHFIELDS:
				return getDYNAMICLENGTHFIELDS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICENDMARKERFIELDS:
				return getDYNAMICENDMARKERFIELDS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENDOFPDUFIELDS:
				return getENDOFPDUFIELDS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__MUXS:
				return getMUXS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATAS:
				return getENVDATAS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__UNITSPEC:
				return getUNITSPEC();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__TABLES:
				return getTABLES();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DTCDOPS:
				setDTCDOPS((DTCDOPS)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATADESCS:
				setENVDATADESCS((ENVDATADESCS)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DATAOBJECTPROPS:
				setDATAOBJECTPROPS((DATAOBJECTPROPS)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STRUCTURES:
				setSTRUCTURES((STRUCTURES)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STATICFIELDS:
				setSTATICFIELDS((STATICFIELDS)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICLENGTHFIELDS:
				setDYNAMICLENGTHFIELDS((DYNAMICLENGTHFIELDS)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICENDMARKERFIELDS:
				setDYNAMICENDMARKERFIELDS((DYNAMICENDMARKERFIELDS)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENDOFPDUFIELDS:
				setENDOFPDUFIELDS((ENDOFPDUFIELDS)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__MUXS:
				setMUXS((MUXS)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATAS:
				setENVDATAS((ENVDATAS)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__UNITSPEC:
				setUNITSPEC((UNITSPEC)newValue);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__TABLES:
				setTABLES((TABLES)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DTCDOPS:
				setDTCDOPS((DTCDOPS)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATADESCS:
				setENVDATADESCS((ENVDATADESCS)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DATAOBJECTPROPS:
				setDATAOBJECTPROPS((DATAOBJECTPROPS)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STRUCTURES:
				setSTRUCTURES((STRUCTURES)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STATICFIELDS:
				setSTATICFIELDS((STATICFIELDS)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICLENGTHFIELDS:
				setDYNAMICLENGTHFIELDS((DYNAMICLENGTHFIELDS)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICENDMARKERFIELDS:
				setDYNAMICENDMARKERFIELDS((DYNAMICENDMARKERFIELDS)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENDOFPDUFIELDS:
				setENDOFPDUFIELDS((ENDOFPDUFIELDS)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__MUXS:
				setMUXS((MUXS)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATAS:
				setENVDATAS((ENVDATAS)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__UNITSPEC:
				setUNITSPEC((UNITSPEC)null);
				return;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__TABLES:
				setTABLES((TABLES)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DTCDOPS:
				return dTCDOPS != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATADESCS:
				return eNVDATADESCS != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DATAOBJECTPROPS:
				return dATAOBJECTPROPS != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STRUCTURES:
				return sTRUCTURES != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__STATICFIELDS:
				return sTATICFIELDS != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICLENGTHFIELDS:
				return dYNAMICLENGTHFIELDS != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__DYNAMICENDMARKERFIELDS:
				return dYNAMICENDMARKERFIELDS != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENDOFPDUFIELDS:
				return eNDOFPDUFIELDS != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__MUXS:
				return mUXS != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__ENVDATAS:
				return eNVDATAS != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__UNITSPEC:
				return uNITSPEC != null;
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC__TABLES:
				return tABLES != null;
		}
		return super.eIsSet(featureID);
	}

} //DIAGDATADICTIONARYSPECImpl
