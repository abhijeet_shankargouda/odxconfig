/**
 */
package OdxXhtml.impl;

import OdxXhtml.MATCHINGPARAMETER;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MATCHINGPARAMETER</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MATCHINGPARAMETERImpl#getEXPECTEDVALUE <em>EXPECTEDVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.MATCHINGPARAMETERImpl#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.MATCHINGPARAMETERImpl#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MATCHINGPARAMETERImpl extends MinimalEObjectImpl.Container implements MATCHINGPARAMETER {
	/**
	 * The default value of the '{@link #getEXPECTEDVALUE() <em>EXPECTEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEXPECTEDVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPECTEDVALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEXPECTEDVALUE() <em>EXPECTEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEXPECTEDVALUE()
	 * @generated
	 * @ordered
	 */
	protected String eXPECTEDVALUE = EXPECTEDVALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDIAGCOMMSNREF() <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dIAGCOMMSNREF;

	/**
	 * The cached value of the '{@link #getOUTPARAMIFSNREF() <em>OUTPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOUTPARAMIFSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF oUTPARAMIFSNREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MATCHINGPARAMETERImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMATCHINGPARAMETER();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getEXPECTEDVALUE() {
		return eXPECTEDVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEXPECTEDVALUE(String newEXPECTEDVALUE) {
		String oldEXPECTEDVALUE = eXPECTEDVALUE;
		eXPECTEDVALUE = newEXPECTEDVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGPARAMETER__EXPECTEDVALUE, oldEXPECTEDVALUE, eXPECTEDVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDIAGCOMMSNREF() {
		return dIAGCOMMSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF, NotificationChain msgs) {
		SNREF oldDIAGCOMMSNREF = dIAGCOMMSNREF;
		dIAGCOMMSNREF = newDIAGCOMMSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGPARAMETER__DIAGCOMMSNREF, oldDIAGCOMMSNREF, newDIAGCOMMSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF) {
		if (newDIAGCOMMSNREF != dIAGCOMMSNREF) {
			NotificationChain msgs = null;
			if (dIAGCOMMSNREF != null)
				msgs = ((InternalEObject)dIAGCOMMSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGPARAMETER__DIAGCOMMSNREF, null, msgs);
			if (newDIAGCOMMSNREF != null)
				msgs = ((InternalEObject)newDIAGCOMMSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGPARAMETER__DIAGCOMMSNREF, null, msgs);
			msgs = basicSetDIAGCOMMSNREF(newDIAGCOMMSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGPARAMETER__DIAGCOMMSNREF, newDIAGCOMMSNREF, newDIAGCOMMSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getOUTPARAMIFSNREF() {
		return oUTPARAMIFSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOUTPARAMIFSNREF(SNREF newOUTPARAMIFSNREF, NotificationChain msgs) {
		SNREF oldOUTPARAMIFSNREF = oUTPARAMIFSNREF;
		oUTPARAMIFSNREF = newOUTPARAMIFSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGPARAMETER__OUTPARAMIFSNREF, oldOUTPARAMIFSNREF, newOUTPARAMIFSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOUTPARAMIFSNREF(SNREF newOUTPARAMIFSNREF) {
		if (newOUTPARAMIFSNREF != oUTPARAMIFSNREF) {
			NotificationChain msgs = null;
			if (oUTPARAMIFSNREF != null)
				msgs = ((InternalEObject)oUTPARAMIFSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGPARAMETER__OUTPARAMIFSNREF, null, msgs);
			if (newOUTPARAMIFSNREF != null)
				msgs = ((InternalEObject)newOUTPARAMIFSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGPARAMETER__OUTPARAMIFSNREF, null, msgs);
			msgs = basicSetOUTPARAMIFSNREF(newOUTPARAMIFSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGPARAMETER__OUTPARAMIFSNREF, newOUTPARAMIFSNREF, newOUTPARAMIFSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETER__DIAGCOMMSNREF:
				return basicSetDIAGCOMMSNREF(null, msgs);
			case OdxXhtmlPackage.MATCHINGPARAMETER__OUTPARAMIFSNREF:
				return basicSetOUTPARAMIFSNREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETER__EXPECTEDVALUE:
				return getEXPECTEDVALUE();
			case OdxXhtmlPackage.MATCHINGPARAMETER__DIAGCOMMSNREF:
				return getDIAGCOMMSNREF();
			case OdxXhtmlPackage.MATCHINGPARAMETER__OUTPARAMIFSNREF:
				return getOUTPARAMIFSNREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETER__EXPECTEDVALUE:
				setEXPECTEDVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.MATCHINGPARAMETER__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.MATCHINGPARAMETER__OUTPARAMIFSNREF:
				setOUTPARAMIFSNREF((SNREF)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETER__EXPECTEDVALUE:
				setEXPECTEDVALUE(EXPECTEDVALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.MATCHINGPARAMETER__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.MATCHINGPARAMETER__OUTPARAMIFSNREF:
				setOUTPARAMIFSNREF((SNREF)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETER__EXPECTEDVALUE:
				return EXPECTEDVALUE_EDEFAULT == null ? eXPECTEDVALUE != null : !EXPECTEDVALUE_EDEFAULT.equals(eXPECTEDVALUE);
			case OdxXhtmlPackage.MATCHINGPARAMETER__DIAGCOMMSNREF:
				return dIAGCOMMSNREF != null;
			case OdxXhtmlPackage.MATCHINGPARAMETER__OUTPARAMIFSNREF:
				return oUTPARAMIFSNREF != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (eXPECTEDVALUE: ");
		result.append(eXPECTEDVALUE);
		result.append(')');
		return result.toString();
	}

} //MATCHINGPARAMETERImpl
