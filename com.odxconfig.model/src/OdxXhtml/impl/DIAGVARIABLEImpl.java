/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.COMMRELATIONS;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.DIAGVARIABLE;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SWVARIABLES;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGVARIABLE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#getVARIABLEGROUPREF <em>VARIABLEGROUPREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#getSWVARIABLES <em>SWVARIABLES</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#getCOMMRELATIONS <em>COMMRELATIONS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#isISREADBEFOREWRITE <em>ISREADBEFOREWRITE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGVARIABLEImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGVARIABLEImpl extends MinimalEObjectImpl.Container implements DIAGVARIABLE {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getVARIABLEGROUPREF() <em>VARIABLEGROUPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVARIABLEGROUPREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK vARIABLEGROUPREF;

	/**
	 * The cached value of the '{@link #getSWVARIABLES() <em>SWVARIABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSWVARIABLES()
	 * @generated
	 * @ordered
	 */
	protected SWVARIABLES sWVARIABLES;

	/**
	 * The cached value of the '{@link #getCOMMRELATIONS() <em>COMMRELATIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMMRELATIONS()
	 * @generated
	 * @ordered
	 */
	protected COMMRELATIONS cOMMRELATIONS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isISREADBEFOREWRITE() <em>ISREADBEFOREWRITE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISREADBEFOREWRITE()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISREADBEFOREWRITE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISREADBEFOREWRITE() <em>ISREADBEFOREWRITE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISREADBEFOREWRITE()
	 * @generated
	 * @ordered
	 */
	protected boolean iSREADBEFOREWRITE = ISREADBEFOREWRITE_EDEFAULT;

	/**
	 * This is true if the ISREADBEFOREWRITE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSREADBEFOREWRITEESet;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGVARIABLEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGVARIABLE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getVARIABLEGROUPREF() {
		return vARIABLEGROUPREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVARIABLEGROUPREF(ODXLINK newVARIABLEGROUPREF, NotificationChain msgs) {
		ODXLINK oldVARIABLEGROUPREF = vARIABLEGROUPREF;
		vARIABLEGROUPREF = newVARIABLEGROUPREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__VARIABLEGROUPREF, oldVARIABLEGROUPREF, newVARIABLEGROUPREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVARIABLEGROUPREF(ODXLINK newVARIABLEGROUPREF) {
		if (newVARIABLEGROUPREF != vARIABLEGROUPREF) {
			NotificationChain msgs = null;
			if (vARIABLEGROUPREF != null)
				msgs = ((InternalEObject)vARIABLEGROUPREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__VARIABLEGROUPREF, null, msgs);
			if (newVARIABLEGROUPREF != null)
				msgs = ((InternalEObject)newVARIABLEGROUPREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__VARIABLEGROUPREF, null, msgs);
			msgs = basicSetVARIABLEGROUPREF(newVARIABLEGROUPREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__VARIABLEGROUPREF, newVARIABLEGROUPREF, newVARIABLEGROUPREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SWVARIABLES getSWVARIABLES() {
		return sWVARIABLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSWVARIABLES(SWVARIABLES newSWVARIABLES, NotificationChain msgs) {
		SWVARIABLES oldSWVARIABLES = sWVARIABLES;
		sWVARIABLES = newSWVARIABLES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__SWVARIABLES, oldSWVARIABLES, newSWVARIABLES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSWVARIABLES(SWVARIABLES newSWVARIABLES) {
		if (newSWVARIABLES != sWVARIABLES) {
			NotificationChain msgs = null;
			if (sWVARIABLES != null)
				msgs = ((InternalEObject)sWVARIABLES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__SWVARIABLES, null, msgs);
			if (newSWVARIABLES != null)
				msgs = ((InternalEObject)newSWVARIABLES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__SWVARIABLES, null, msgs);
			msgs = basicSetSWVARIABLES(newSWVARIABLES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__SWVARIABLES, newSWVARIABLES, newSWVARIABLES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMMRELATIONS getCOMMRELATIONS() {
		return cOMMRELATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMMRELATIONS(COMMRELATIONS newCOMMRELATIONS, NotificationChain msgs) {
		COMMRELATIONS oldCOMMRELATIONS = cOMMRELATIONS;
		cOMMRELATIONS = newCOMMRELATIONS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__COMMRELATIONS, oldCOMMRELATIONS, newCOMMRELATIONS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMMRELATIONS(COMMRELATIONS newCOMMRELATIONS) {
		if (newCOMMRELATIONS != cOMMRELATIONS) {
			NotificationChain msgs = null;
			if (cOMMRELATIONS != null)
				msgs = ((InternalEObject)cOMMRELATIONS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__COMMRELATIONS, null, msgs);
			if (newCOMMRELATIONS != null)
				msgs = ((InternalEObject)newCOMMRELATIONS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGVARIABLE__COMMRELATIONS, null, msgs);
			msgs = basicSetCOMMRELATIONS(newCOMMRELATIONS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__COMMRELATIONS, newCOMMRELATIONS, newCOMMRELATIONS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISREADBEFOREWRITE() {
		return iSREADBEFOREWRITE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISREADBEFOREWRITE(boolean newISREADBEFOREWRITE) {
		boolean oldISREADBEFOREWRITE = iSREADBEFOREWRITE;
		iSREADBEFOREWRITE = newISREADBEFOREWRITE;
		boolean oldISREADBEFOREWRITEESet = iSREADBEFOREWRITEESet;
		iSREADBEFOREWRITEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__ISREADBEFOREWRITE, oldISREADBEFOREWRITE, iSREADBEFOREWRITE, !oldISREADBEFOREWRITEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISREADBEFOREWRITE() {
		boolean oldISREADBEFOREWRITE = iSREADBEFOREWRITE;
		boolean oldISREADBEFOREWRITEESet = iSREADBEFOREWRITEESet;
		iSREADBEFOREWRITE = ISREADBEFOREWRITE_EDEFAULT;
		iSREADBEFOREWRITEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGVARIABLE__ISREADBEFOREWRITE, oldISREADBEFOREWRITE, ISREADBEFOREWRITE_EDEFAULT, oldISREADBEFOREWRITEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISREADBEFOREWRITE() {
		return iSREADBEFOREWRITEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGVARIABLE__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLE__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.DIAGVARIABLE__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.DIAGVARIABLE__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.DIAGVARIABLE__VARIABLEGROUPREF:
				return basicSetVARIABLEGROUPREF(null, msgs);
			case OdxXhtmlPackage.DIAGVARIABLE__SWVARIABLES:
				return basicSetSWVARIABLES(null, msgs);
			case OdxXhtmlPackage.DIAGVARIABLE__COMMRELATIONS:
				return basicSetCOMMRELATIONS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLE__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.DIAGVARIABLE__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.DIAGVARIABLE__DESC:
				return getDESC();
			case OdxXhtmlPackage.DIAGVARIABLE__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.DIAGVARIABLE__VARIABLEGROUPREF:
				return getVARIABLEGROUPREF();
			case OdxXhtmlPackage.DIAGVARIABLE__SWVARIABLES:
				return getSWVARIABLES();
			case OdxXhtmlPackage.DIAGVARIABLE__COMMRELATIONS:
				return getCOMMRELATIONS();
			case OdxXhtmlPackage.DIAGVARIABLE__ID:
				return getID();
			case OdxXhtmlPackage.DIAGVARIABLE__ISREADBEFOREWRITE:
				return isISREADBEFOREWRITE();
			case OdxXhtmlPackage.DIAGVARIABLE__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLE__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__VARIABLEGROUPREF:
				setVARIABLEGROUPREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__SWVARIABLES:
				setSWVARIABLES((SWVARIABLES)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__COMMRELATIONS:
				setCOMMRELATIONS((COMMRELATIONS)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__ISREADBEFOREWRITE:
				setISREADBEFOREWRITE((Boolean)newValue);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLE__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__VARIABLEGROUPREF:
				setVARIABLEGROUPREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__SWVARIABLES:
				setSWVARIABLES((SWVARIABLES)null);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__COMMRELATIONS:
				setCOMMRELATIONS((COMMRELATIONS)null);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__ISREADBEFOREWRITE:
				unsetISREADBEFOREWRITE();
				return;
			case OdxXhtmlPackage.DIAGVARIABLE__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGVARIABLE__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.DIAGVARIABLE__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.DIAGVARIABLE__DESC:
				return dESC != null;
			case OdxXhtmlPackage.DIAGVARIABLE__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.DIAGVARIABLE__VARIABLEGROUPREF:
				return vARIABLEGROUPREF != null;
			case OdxXhtmlPackage.DIAGVARIABLE__SWVARIABLES:
				return sWVARIABLES != null;
			case OdxXhtmlPackage.DIAGVARIABLE__COMMRELATIONS:
				return cOMMRELATIONS != null;
			case OdxXhtmlPackage.DIAGVARIABLE__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.DIAGVARIABLE__ISREADBEFOREWRITE:
				return isSetISREADBEFOREWRITE();
			case OdxXhtmlPackage.DIAGVARIABLE__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", iSREADBEFOREWRITE: ");
		if (iSREADBEFOREWRITEESet) result.append(iSREADBEFOREWRITE); else result.append("<unset>");
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //DIAGVARIABLEImpl
