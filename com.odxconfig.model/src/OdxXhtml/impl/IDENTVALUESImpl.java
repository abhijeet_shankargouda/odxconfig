/**
 */
package OdxXhtml.impl;

import OdxXhtml.IDENTVALUE;
import OdxXhtml.IDENTVALUES;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IDENTVALUES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.IDENTVALUESImpl#getIDENTVALUE <em>IDENTVALUE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IDENTVALUESImpl extends MinimalEObjectImpl.Container implements IDENTVALUES {
	/**
	 * The cached value of the '{@link #getIDENTVALUE() <em>IDENTVALUE</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDENTVALUE()
	 * @generated
	 * @ordered
	 */
	protected EList<IDENTVALUE> iDENTVALUE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IDENTVALUESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getIDENTVALUES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IDENTVALUE> getIDENTVALUE() {
		if (iDENTVALUE == null) {
			iDENTVALUE = new EObjectContainmentEList<IDENTVALUE>(IDENTVALUE.class, this, OdxXhtmlPackage.IDENTVALUES__IDENTVALUE);
		}
		return iDENTVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTVALUES__IDENTVALUE:
				return ((InternalEList<?>)getIDENTVALUE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTVALUES__IDENTVALUE:
				return getIDENTVALUE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTVALUES__IDENTVALUE:
				getIDENTVALUE().clear();
				getIDENTVALUE().addAll((Collection<? extends IDENTVALUE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTVALUES__IDENTVALUE:
				getIDENTVALUE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTVALUES__IDENTVALUE:
				return iDENTVALUE != null && !iDENTVALUE.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //IDENTVALUESImpl
