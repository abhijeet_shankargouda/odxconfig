/**
 */
package OdxXhtml.impl;

import OdxXhtml.ECUVARIANTPATTERN;
import OdxXhtml.MATCHINGPARAMETERS;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUVARIANTPATTERN</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTPATTERNImpl#getMATCHINGPARAMETERS <em>MATCHINGPARAMETERS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECUVARIANTPATTERNImpl extends MinimalEObjectImpl.Container implements ECUVARIANTPATTERN {
	/**
	 * The cached value of the '{@link #getMATCHINGPARAMETERS() <em>MATCHINGPARAMETERS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMATCHINGPARAMETERS()
	 * @generated
	 * @ordered
	 */
	protected MATCHINGPARAMETERS mATCHINGPARAMETERS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUVARIANTPATTERNImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUVARIANTPATTERN();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MATCHINGPARAMETERS getMATCHINGPARAMETERS() {
		return mATCHINGPARAMETERS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMATCHINGPARAMETERS(MATCHINGPARAMETERS newMATCHINGPARAMETERS, NotificationChain msgs) {
		MATCHINGPARAMETERS oldMATCHINGPARAMETERS = mATCHINGPARAMETERS;
		mATCHINGPARAMETERS = newMATCHINGPARAMETERS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANTPATTERN__MATCHINGPARAMETERS, oldMATCHINGPARAMETERS, newMATCHINGPARAMETERS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMATCHINGPARAMETERS(MATCHINGPARAMETERS newMATCHINGPARAMETERS) {
		if (newMATCHINGPARAMETERS != mATCHINGPARAMETERS) {
			NotificationChain msgs = null;
			if (mATCHINGPARAMETERS != null)
				msgs = ((InternalEObject)mATCHINGPARAMETERS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANTPATTERN__MATCHINGPARAMETERS, null, msgs);
			if (newMATCHINGPARAMETERS != null)
				msgs = ((InternalEObject)newMATCHINGPARAMETERS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANTPATTERN__MATCHINGPARAMETERS, null, msgs);
			msgs = basicSetMATCHINGPARAMETERS(newMATCHINGPARAMETERS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANTPATTERN__MATCHINGPARAMETERS, newMATCHINGPARAMETERS, newMATCHINGPARAMETERS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERN__MATCHINGPARAMETERS:
				return basicSetMATCHINGPARAMETERS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERN__MATCHINGPARAMETERS:
				return getMATCHINGPARAMETERS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERN__MATCHINGPARAMETERS:
				setMATCHINGPARAMETERS((MATCHINGPARAMETERS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERN__MATCHINGPARAMETERS:
				setMATCHINGPARAMETERS((MATCHINGPARAMETERS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERN__MATCHINGPARAMETERS:
				return mATCHINGPARAMETERS != null;
		}
		return super.eIsSet(featureID);
	}

} //ECUVARIANTPATTERNImpl
