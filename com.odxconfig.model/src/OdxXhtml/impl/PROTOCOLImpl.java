/**
 */
package OdxXhtml.impl;

import OdxXhtml.ACCESSLEVELS;
import OdxXhtml.AUTMETHODS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PARENTREFS;
import OdxXhtml.PROTOCOL;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PROTOCOL</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PROTOCOLImpl#getCOMPARAMSPECREF <em>COMPARAMSPECREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROTOCOLImpl#getACCESSLEVELS <em>ACCESSLEVELS</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROTOCOLImpl#getAUTMETHODS <em>AUTMETHODS</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROTOCOLImpl#getPARENTREFS <em>PARENTREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROTOCOLImpl#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PROTOCOLImpl extends HIERARCHYELEMENTImpl implements PROTOCOL {
	/**
	 * The cached value of the '{@link #getCOMPARAMSPECREF() <em>COMPARAMSPECREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPARAMSPECREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK cOMPARAMSPECREF;

	/**
	 * The cached value of the '{@link #getACCESSLEVELS() <em>ACCESSLEVELS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACCESSLEVELS()
	 * @generated
	 * @ordered
	 */
	protected ACCESSLEVELS aCCESSLEVELS;

	/**
	 * The cached value of the '{@link #getAUTMETHODS() <em>AUTMETHODS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUTMETHODS()
	 * @generated
	 * @ordered
	 */
	protected AUTMETHODS aUTMETHODS;

	/**
	 * The cached value of the '{@link #getPARENTREFS() <em>PARENTREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARENTREFS()
	 * @generated
	 * @ordered
	 */
	protected PARENTREFS pARENTREFS;

	/**
	 * The default value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected String tYPE = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PROTOCOLImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPROTOCOL();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getCOMPARAMSPECREF() {
		return cOMPARAMSPECREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPARAMSPECREF(ODXLINK newCOMPARAMSPECREF, NotificationChain msgs) {
		ODXLINK oldCOMPARAMSPECREF = cOMPARAMSPECREF;
		cOMPARAMSPECREF = newCOMPARAMSPECREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROTOCOL__COMPARAMSPECREF, oldCOMPARAMSPECREF, newCOMPARAMSPECREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPARAMSPECREF(ODXLINK newCOMPARAMSPECREF) {
		if (newCOMPARAMSPECREF != cOMPARAMSPECREF) {
			NotificationChain msgs = null;
			if (cOMPARAMSPECREF != null)
				msgs = ((InternalEObject)cOMPARAMSPECREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROTOCOL__COMPARAMSPECREF, null, msgs);
			if (newCOMPARAMSPECREF != null)
				msgs = ((InternalEObject)newCOMPARAMSPECREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROTOCOL__COMPARAMSPECREF, null, msgs);
			msgs = basicSetCOMPARAMSPECREF(newCOMPARAMSPECREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROTOCOL__COMPARAMSPECREF, newCOMPARAMSPECREF, newCOMPARAMSPECREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ACCESSLEVELS getACCESSLEVELS() {
		return aCCESSLEVELS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetACCESSLEVELS(ACCESSLEVELS newACCESSLEVELS, NotificationChain msgs) {
		ACCESSLEVELS oldACCESSLEVELS = aCCESSLEVELS;
		aCCESSLEVELS = newACCESSLEVELS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROTOCOL__ACCESSLEVELS, oldACCESSLEVELS, newACCESSLEVELS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setACCESSLEVELS(ACCESSLEVELS newACCESSLEVELS) {
		if (newACCESSLEVELS != aCCESSLEVELS) {
			NotificationChain msgs = null;
			if (aCCESSLEVELS != null)
				msgs = ((InternalEObject)aCCESSLEVELS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROTOCOL__ACCESSLEVELS, null, msgs);
			if (newACCESSLEVELS != null)
				msgs = ((InternalEObject)newACCESSLEVELS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROTOCOL__ACCESSLEVELS, null, msgs);
			msgs = basicSetACCESSLEVELS(newACCESSLEVELS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROTOCOL__ACCESSLEVELS, newACCESSLEVELS, newACCESSLEVELS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AUTMETHODS getAUTMETHODS() {
		return aUTMETHODS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAUTMETHODS(AUTMETHODS newAUTMETHODS, NotificationChain msgs) {
		AUTMETHODS oldAUTMETHODS = aUTMETHODS;
		aUTMETHODS = newAUTMETHODS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROTOCOL__AUTMETHODS, oldAUTMETHODS, newAUTMETHODS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAUTMETHODS(AUTMETHODS newAUTMETHODS) {
		if (newAUTMETHODS != aUTMETHODS) {
			NotificationChain msgs = null;
			if (aUTMETHODS != null)
				msgs = ((InternalEObject)aUTMETHODS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROTOCOL__AUTMETHODS, null, msgs);
			if (newAUTMETHODS != null)
				msgs = ((InternalEObject)newAUTMETHODS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROTOCOL__AUTMETHODS, null, msgs);
			msgs = basicSetAUTMETHODS(newAUTMETHODS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROTOCOL__AUTMETHODS, newAUTMETHODS, newAUTMETHODS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARENTREFS getPARENTREFS() {
		return pARENTREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPARENTREFS(PARENTREFS newPARENTREFS, NotificationChain msgs) {
		PARENTREFS oldPARENTREFS = pARENTREFS;
		pARENTREFS = newPARENTREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROTOCOL__PARENTREFS, oldPARENTREFS, newPARENTREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPARENTREFS(PARENTREFS newPARENTREFS) {
		if (newPARENTREFS != pARENTREFS) {
			NotificationChain msgs = null;
			if (pARENTREFS != null)
				msgs = ((InternalEObject)pARENTREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROTOCOL__PARENTREFS, null, msgs);
			if (newPARENTREFS != null)
				msgs = ((InternalEObject)newPARENTREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PROTOCOL__PARENTREFS, null, msgs);
			msgs = basicSetPARENTREFS(newPARENTREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROTOCOL__PARENTREFS, newPARENTREFS, newPARENTREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTYPE() {
		return tYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTYPE(String newTYPE) {
		String oldTYPE = tYPE;
		tYPE = newTYPE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROTOCOL__TYPE, oldTYPE, tYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOL__COMPARAMSPECREF:
				return basicSetCOMPARAMSPECREF(null, msgs);
			case OdxXhtmlPackage.PROTOCOL__ACCESSLEVELS:
				return basicSetACCESSLEVELS(null, msgs);
			case OdxXhtmlPackage.PROTOCOL__AUTMETHODS:
				return basicSetAUTMETHODS(null, msgs);
			case OdxXhtmlPackage.PROTOCOL__PARENTREFS:
				return basicSetPARENTREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOL__COMPARAMSPECREF:
				return getCOMPARAMSPECREF();
			case OdxXhtmlPackage.PROTOCOL__ACCESSLEVELS:
				return getACCESSLEVELS();
			case OdxXhtmlPackage.PROTOCOL__AUTMETHODS:
				return getAUTMETHODS();
			case OdxXhtmlPackage.PROTOCOL__PARENTREFS:
				return getPARENTREFS();
			case OdxXhtmlPackage.PROTOCOL__TYPE:
				return getTYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOL__COMPARAMSPECREF:
				setCOMPARAMSPECREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.PROTOCOL__ACCESSLEVELS:
				setACCESSLEVELS((ACCESSLEVELS)newValue);
				return;
			case OdxXhtmlPackage.PROTOCOL__AUTMETHODS:
				setAUTMETHODS((AUTMETHODS)newValue);
				return;
			case OdxXhtmlPackage.PROTOCOL__PARENTREFS:
				setPARENTREFS((PARENTREFS)newValue);
				return;
			case OdxXhtmlPackage.PROTOCOL__TYPE:
				setTYPE((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOL__COMPARAMSPECREF:
				setCOMPARAMSPECREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.PROTOCOL__ACCESSLEVELS:
				setACCESSLEVELS((ACCESSLEVELS)null);
				return;
			case OdxXhtmlPackage.PROTOCOL__AUTMETHODS:
				setAUTMETHODS((AUTMETHODS)null);
				return;
			case OdxXhtmlPackage.PROTOCOL__PARENTREFS:
				setPARENTREFS((PARENTREFS)null);
				return;
			case OdxXhtmlPackage.PROTOCOL__TYPE:
				setTYPE(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOL__COMPARAMSPECREF:
				return cOMPARAMSPECREF != null;
			case OdxXhtmlPackage.PROTOCOL__ACCESSLEVELS:
				return aCCESSLEVELS != null;
			case OdxXhtmlPackage.PROTOCOL__AUTMETHODS:
				return aUTMETHODS != null;
			case OdxXhtmlPackage.PROTOCOL__PARENTREFS:
				return pARENTREFS != null;
			case OdxXhtmlPackage.PROTOCOL__TYPE:
				return TYPE_EDEFAULT == null ? tYPE != null : !TYPE_EDEFAULT.equals(tYPE);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (tYPE: ");
		result.append(tYPE);
		result.append(')');
		return result.toString();
	}

} //PROTOCOLImpl
