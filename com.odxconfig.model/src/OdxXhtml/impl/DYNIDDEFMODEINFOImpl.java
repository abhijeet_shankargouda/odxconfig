/**
 */
package OdxXhtml.impl;

import OdxXhtml.DYNIDDEFMODEINFO;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SELECTIONTABLEREFS;
import OdxXhtml.SNREF;
import OdxXhtml.SUPPORTEDDYNIDS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DYNIDDEFMODEINFO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOImpl#getDEFMODE <em>DEFMODE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOImpl#getCLEARDYNDEFMESSAGEREF <em>CLEARDYNDEFMESSAGEREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOImpl#getCLEARDYNDEFMESSAGESNREF <em>CLEARDYNDEFMESSAGESNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOImpl#getREADDYNDEFMESSAGEREF <em>READDYNDEFMESSAGEREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOImpl#getREADDYNDEFMESSAGESNREF <em>READDYNDEFMESSAGESNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOImpl#getDYNDEFMESSAGEREF <em>DYNDEFMESSAGEREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOImpl#getDYNDEFMESSAGESNREF <em>DYNDEFMESSAGESNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOImpl#getSUPPORTEDDYNIDS <em>SUPPORTEDDYNIDS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOImpl#getSELECTIONTABLEREFS <em>SELECTIONTABLEREFS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DYNIDDEFMODEINFOImpl extends MinimalEObjectImpl.Container implements DYNIDDEFMODEINFO {
	/**
	 * The default value of the '{@link #getDEFMODE() <em>DEFMODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDEFMODE()
	 * @generated
	 * @ordered
	 */
	protected static final String DEFMODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDEFMODE() <em>DEFMODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDEFMODE()
	 * @generated
	 * @ordered
	 */
	protected String dEFMODE = DEFMODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCLEARDYNDEFMESSAGEREF() <em>CLEARDYNDEFMESSAGEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCLEARDYNDEFMESSAGEREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK cLEARDYNDEFMESSAGEREF;

	/**
	 * The cached value of the '{@link #getCLEARDYNDEFMESSAGESNREF() <em>CLEARDYNDEFMESSAGESNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCLEARDYNDEFMESSAGESNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF cLEARDYNDEFMESSAGESNREF;

	/**
	 * The cached value of the '{@link #getREADDYNDEFMESSAGEREF() <em>READDYNDEFMESSAGEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREADDYNDEFMESSAGEREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK rEADDYNDEFMESSAGEREF;

	/**
	 * The cached value of the '{@link #getREADDYNDEFMESSAGESNREF() <em>READDYNDEFMESSAGESNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREADDYNDEFMESSAGESNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF rEADDYNDEFMESSAGESNREF;

	/**
	 * The cached value of the '{@link #getDYNDEFMESSAGEREF() <em>DYNDEFMESSAGEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNDEFMESSAGEREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK dYNDEFMESSAGEREF;

	/**
	 * The cached value of the '{@link #getDYNDEFMESSAGESNREF() <em>DYNDEFMESSAGESNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNDEFMESSAGESNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dYNDEFMESSAGESNREF;

	/**
	 * The cached value of the '{@link #getSUPPORTEDDYNIDS() <em>SUPPORTEDDYNIDS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSUPPORTEDDYNIDS()
	 * @generated
	 * @ordered
	 */
	protected SUPPORTEDDYNIDS sUPPORTEDDYNIDS;

	/**
	 * The cached value of the '{@link #getSELECTIONTABLEREFS() <em>SELECTIONTABLEREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSELECTIONTABLEREFS()
	 * @generated
	 * @ordered
	 */
	protected SELECTIONTABLEREFS sELECTIONTABLEREFS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DYNIDDEFMODEINFOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDYNIDDEFMODEINFO();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDEFMODE() {
		return dEFMODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDEFMODE(String newDEFMODE) {
		String oldDEFMODE = dEFMODE;
		dEFMODE = newDEFMODE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__DEFMODE, oldDEFMODE, dEFMODE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getCLEARDYNDEFMESSAGEREF() {
		return cLEARDYNDEFMESSAGEREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCLEARDYNDEFMESSAGEREF(ODXLINK newCLEARDYNDEFMESSAGEREF, NotificationChain msgs) {
		ODXLINK oldCLEARDYNDEFMESSAGEREF = cLEARDYNDEFMESSAGEREF;
		cLEARDYNDEFMESSAGEREF = newCLEARDYNDEFMESSAGEREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGEREF, oldCLEARDYNDEFMESSAGEREF, newCLEARDYNDEFMESSAGEREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCLEARDYNDEFMESSAGEREF(ODXLINK newCLEARDYNDEFMESSAGEREF) {
		if (newCLEARDYNDEFMESSAGEREF != cLEARDYNDEFMESSAGEREF) {
			NotificationChain msgs = null;
			if (cLEARDYNDEFMESSAGEREF != null)
				msgs = ((InternalEObject)cLEARDYNDEFMESSAGEREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGEREF, null, msgs);
			if (newCLEARDYNDEFMESSAGEREF != null)
				msgs = ((InternalEObject)newCLEARDYNDEFMESSAGEREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGEREF, null, msgs);
			msgs = basicSetCLEARDYNDEFMESSAGEREF(newCLEARDYNDEFMESSAGEREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGEREF, newCLEARDYNDEFMESSAGEREF, newCLEARDYNDEFMESSAGEREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getCLEARDYNDEFMESSAGESNREF() {
		return cLEARDYNDEFMESSAGESNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCLEARDYNDEFMESSAGESNREF(SNREF newCLEARDYNDEFMESSAGESNREF, NotificationChain msgs) {
		SNREF oldCLEARDYNDEFMESSAGESNREF = cLEARDYNDEFMESSAGESNREF;
		cLEARDYNDEFMESSAGESNREF = newCLEARDYNDEFMESSAGESNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGESNREF, oldCLEARDYNDEFMESSAGESNREF, newCLEARDYNDEFMESSAGESNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCLEARDYNDEFMESSAGESNREF(SNREF newCLEARDYNDEFMESSAGESNREF) {
		if (newCLEARDYNDEFMESSAGESNREF != cLEARDYNDEFMESSAGESNREF) {
			NotificationChain msgs = null;
			if (cLEARDYNDEFMESSAGESNREF != null)
				msgs = ((InternalEObject)cLEARDYNDEFMESSAGESNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGESNREF, null, msgs);
			if (newCLEARDYNDEFMESSAGESNREF != null)
				msgs = ((InternalEObject)newCLEARDYNDEFMESSAGESNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGESNREF, null, msgs);
			msgs = basicSetCLEARDYNDEFMESSAGESNREF(newCLEARDYNDEFMESSAGESNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGESNREF, newCLEARDYNDEFMESSAGESNREF, newCLEARDYNDEFMESSAGESNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getREADDYNDEFMESSAGEREF() {
		return rEADDYNDEFMESSAGEREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetREADDYNDEFMESSAGEREF(ODXLINK newREADDYNDEFMESSAGEREF, NotificationChain msgs) {
		ODXLINK oldREADDYNDEFMESSAGEREF = rEADDYNDEFMESSAGEREF;
		rEADDYNDEFMESSAGEREF = newREADDYNDEFMESSAGEREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGEREF, oldREADDYNDEFMESSAGEREF, newREADDYNDEFMESSAGEREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREADDYNDEFMESSAGEREF(ODXLINK newREADDYNDEFMESSAGEREF) {
		if (newREADDYNDEFMESSAGEREF != rEADDYNDEFMESSAGEREF) {
			NotificationChain msgs = null;
			if (rEADDYNDEFMESSAGEREF != null)
				msgs = ((InternalEObject)rEADDYNDEFMESSAGEREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGEREF, null, msgs);
			if (newREADDYNDEFMESSAGEREF != null)
				msgs = ((InternalEObject)newREADDYNDEFMESSAGEREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGEREF, null, msgs);
			msgs = basicSetREADDYNDEFMESSAGEREF(newREADDYNDEFMESSAGEREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGEREF, newREADDYNDEFMESSAGEREF, newREADDYNDEFMESSAGEREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getREADDYNDEFMESSAGESNREF() {
		return rEADDYNDEFMESSAGESNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetREADDYNDEFMESSAGESNREF(SNREF newREADDYNDEFMESSAGESNREF, NotificationChain msgs) {
		SNREF oldREADDYNDEFMESSAGESNREF = rEADDYNDEFMESSAGESNREF;
		rEADDYNDEFMESSAGESNREF = newREADDYNDEFMESSAGESNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGESNREF, oldREADDYNDEFMESSAGESNREF, newREADDYNDEFMESSAGESNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREADDYNDEFMESSAGESNREF(SNREF newREADDYNDEFMESSAGESNREF) {
		if (newREADDYNDEFMESSAGESNREF != rEADDYNDEFMESSAGESNREF) {
			NotificationChain msgs = null;
			if (rEADDYNDEFMESSAGESNREF != null)
				msgs = ((InternalEObject)rEADDYNDEFMESSAGESNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGESNREF, null, msgs);
			if (newREADDYNDEFMESSAGESNREF != null)
				msgs = ((InternalEObject)newREADDYNDEFMESSAGESNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGESNREF, null, msgs);
			msgs = basicSetREADDYNDEFMESSAGESNREF(newREADDYNDEFMESSAGESNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGESNREF, newREADDYNDEFMESSAGESNREF, newREADDYNDEFMESSAGESNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getDYNDEFMESSAGEREF() {
		return dYNDEFMESSAGEREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDYNDEFMESSAGEREF(ODXLINK newDYNDEFMESSAGEREF, NotificationChain msgs) {
		ODXLINK oldDYNDEFMESSAGEREF = dYNDEFMESSAGEREF;
		dYNDEFMESSAGEREF = newDYNDEFMESSAGEREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGEREF, oldDYNDEFMESSAGEREF, newDYNDEFMESSAGEREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDYNDEFMESSAGEREF(ODXLINK newDYNDEFMESSAGEREF) {
		if (newDYNDEFMESSAGEREF != dYNDEFMESSAGEREF) {
			NotificationChain msgs = null;
			if (dYNDEFMESSAGEREF != null)
				msgs = ((InternalEObject)dYNDEFMESSAGEREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGEREF, null, msgs);
			if (newDYNDEFMESSAGEREF != null)
				msgs = ((InternalEObject)newDYNDEFMESSAGEREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGEREF, null, msgs);
			msgs = basicSetDYNDEFMESSAGEREF(newDYNDEFMESSAGEREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGEREF, newDYNDEFMESSAGEREF, newDYNDEFMESSAGEREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDYNDEFMESSAGESNREF() {
		return dYNDEFMESSAGESNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDYNDEFMESSAGESNREF(SNREF newDYNDEFMESSAGESNREF, NotificationChain msgs) {
		SNREF oldDYNDEFMESSAGESNREF = dYNDEFMESSAGESNREF;
		dYNDEFMESSAGESNREF = newDYNDEFMESSAGESNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGESNREF, oldDYNDEFMESSAGESNREF, newDYNDEFMESSAGESNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDYNDEFMESSAGESNREF(SNREF newDYNDEFMESSAGESNREF) {
		if (newDYNDEFMESSAGESNREF != dYNDEFMESSAGESNREF) {
			NotificationChain msgs = null;
			if (dYNDEFMESSAGESNREF != null)
				msgs = ((InternalEObject)dYNDEFMESSAGESNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGESNREF, null, msgs);
			if (newDYNDEFMESSAGESNREF != null)
				msgs = ((InternalEObject)newDYNDEFMESSAGESNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGESNREF, null, msgs);
			msgs = basicSetDYNDEFMESSAGESNREF(newDYNDEFMESSAGESNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGESNREF, newDYNDEFMESSAGESNREF, newDYNDEFMESSAGESNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SUPPORTEDDYNIDS getSUPPORTEDDYNIDS() {
		return sUPPORTEDDYNIDS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSUPPORTEDDYNIDS(SUPPORTEDDYNIDS newSUPPORTEDDYNIDS, NotificationChain msgs) {
		SUPPORTEDDYNIDS oldSUPPORTEDDYNIDS = sUPPORTEDDYNIDS;
		sUPPORTEDDYNIDS = newSUPPORTEDDYNIDS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__SUPPORTEDDYNIDS, oldSUPPORTEDDYNIDS, newSUPPORTEDDYNIDS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSUPPORTEDDYNIDS(SUPPORTEDDYNIDS newSUPPORTEDDYNIDS) {
		if (newSUPPORTEDDYNIDS != sUPPORTEDDYNIDS) {
			NotificationChain msgs = null;
			if (sUPPORTEDDYNIDS != null)
				msgs = ((InternalEObject)sUPPORTEDDYNIDS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__SUPPORTEDDYNIDS, null, msgs);
			if (newSUPPORTEDDYNIDS != null)
				msgs = ((InternalEObject)newSUPPORTEDDYNIDS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__SUPPORTEDDYNIDS, null, msgs);
			msgs = basicSetSUPPORTEDDYNIDS(newSUPPORTEDDYNIDS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__SUPPORTEDDYNIDS, newSUPPORTEDDYNIDS, newSUPPORTEDDYNIDS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SELECTIONTABLEREFS getSELECTIONTABLEREFS() {
		return sELECTIONTABLEREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSELECTIONTABLEREFS(SELECTIONTABLEREFS newSELECTIONTABLEREFS, NotificationChain msgs) {
		SELECTIONTABLEREFS oldSELECTIONTABLEREFS = sELECTIONTABLEREFS;
		sELECTIONTABLEREFS = newSELECTIONTABLEREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__SELECTIONTABLEREFS, oldSELECTIONTABLEREFS, newSELECTIONTABLEREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSELECTIONTABLEREFS(SELECTIONTABLEREFS newSELECTIONTABLEREFS) {
		if (newSELECTIONTABLEREFS != sELECTIONTABLEREFS) {
			NotificationChain msgs = null;
			if (sELECTIONTABLEREFS != null)
				msgs = ((InternalEObject)sELECTIONTABLEREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__SELECTIONTABLEREFS, null, msgs);
			if (newSELECTIONTABLEREFS != null)
				msgs = ((InternalEObject)newSELECTIONTABLEREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNIDDEFMODEINFO__SELECTIONTABLEREFS, null, msgs);
			msgs = basicSetSELECTIONTABLEREFS(newSELECTIONTABLEREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNIDDEFMODEINFO__SELECTIONTABLEREFS, newSELECTIONTABLEREFS, newSELECTIONTABLEREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGEREF:
				return basicSetCLEARDYNDEFMESSAGEREF(null, msgs);
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGESNREF:
				return basicSetCLEARDYNDEFMESSAGESNREF(null, msgs);
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGEREF:
				return basicSetREADDYNDEFMESSAGEREF(null, msgs);
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGESNREF:
				return basicSetREADDYNDEFMESSAGESNREF(null, msgs);
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGEREF:
				return basicSetDYNDEFMESSAGEREF(null, msgs);
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGESNREF:
				return basicSetDYNDEFMESSAGESNREF(null, msgs);
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SUPPORTEDDYNIDS:
				return basicSetSUPPORTEDDYNIDS(null, msgs);
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SELECTIONTABLEREFS:
				return basicSetSELECTIONTABLEREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DEFMODE:
				return getDEFMODE();
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGEREF:
				return getCLEARDYNDEFMESSAGEREF();
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGESNREF:
				return getCLEARDYNDEFMESSAGESNREF();
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGEREF:
				return getREADDYNDEFMESSAGEREF();
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGESNREF:
				return getREADDYNDEFMESSAGESNREF();
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGEREF:
				return getDYNDEFMESSAGEREF();
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGESNREF:
				return getDYNDEFMESSAGESNREF();
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SUPPORTEDDYNIDS:
				return getSUPPORTEDDYNIDS();
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SELECTIONTABLEREFS:
				return getSELECTIONTABLEREFS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DEFMODE:
				setDEFMODE((String)newValue);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGEREF:
				setCLEARDYNDEFMESSAGEREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGESNREF:
				setCLEARDYNDEFMESSAGESNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGEREF:
				setREADDYNDEFMESSAGEREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGESNREF:
				setREADDYNDEFMESSAGESNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGEREF:
				setDYNDEFMESSAGEREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGESNREF:
				setDYNDEFMESSAGESNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SUPPORTEDDYNIDS:
				setSUPPORTEDDYNIDS((SUPPORTEDDYNIDS)newValue);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SELECTIONTABLEREFS:
				setSELECTIONTABLEREFS((SELECTIONTABLEREFS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DEFMODE:
				setDEFMODE(DEFMODE_EDEFAULT);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGEREF:
				setCLEARDYNDEFMESSAGEREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGESNREF:
				setCLEARDYNDEFMESSAGESNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGEREF:
				setREADDYNDEFMESSAGEREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGESNREF:
				setREADDYNDEFMESSAGESNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGEREF:
				setDYNDEFMESSAGEREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGESNREF:
				setDYNDEFMESSAGESNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SUPPORTEDDYNIDS:
				setSUPPORTEDDYNIDS((SUPPORTEDDYNIDS)null);
				return;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SELECTIONTABLEREFS:
				setSELECTIONTABLEREFS((SELECTIONTABLEREFS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DEFMODE:
				return DEFMODE_EDEFAULT == null ? dEFMODE != null : !DEFMODE_EDEFAULT.equals(dEFMODE);
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGEREF:
				return cLEARDYNDEFMESSAGEREF != null;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__CLEARDYNDEFMESSAGESNREF:
				return cLEARDYNDEFMESSAGESNREF != null;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGEREF:
				return rEADDYNDEFMESSAGEREF != null;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__READDYNDEFMESSAGESNREF:
				return rEADDYNDEFMESSAGESNREF != null;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGEREF:
				return dYNDEFMESSAGEREF != null;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__DYNDEFMESSAGESNREF:
				return dYNDEFMESSAGESNREF != null;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SUPPORTEDDYNIDS:
				return sUPPORTEDDYNIDS != null;
			case OdxXhtmlPackage.DYNIDDEFMODEINFO__SELECTIONTABLEREFS:
				return sELECTIONTABLEREFS != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (dEFMODE: ");
		result.append(dEFMODE);
		result.append(')');
		return result.toString();
	}

} //DYNIDDEFMODEINFOImpl
