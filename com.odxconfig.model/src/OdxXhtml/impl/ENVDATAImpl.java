/**
 */
package OdxXhtml.impl;

import OdxXhtml.ALLVALUE;
import OdxXhtml.DTCVALUES;
import OdxXhtml.ENVDATA;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ENVDATA</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ENVDATAImpl#getALLVALUE <em>ALLVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.ENVDATAImpl#getDTCVALUES <em>DTCVALUES</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ENVDATAImpl extends BASICSTRUCTUREImpl implements ENVDATA {
	/**
	 * The cached value of the '{@link #getALLVALUE() <em>ALLVALUE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALLVALUE()
	 * @generated
	 * @ordered
	 */
	protected ALLVALUE aLLVALUE;

	/**
	 * The cached value of the '{@link #getDTCVALUES() <em>DTCVALUES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDTCVALUES()
	 * @generated
	 * @ordered
	 */
	protected DTCVALUES dTCVALUES;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ENVDATAImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getENVDATA();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ALLVALUE getALLVALUE() {
		return aLLVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetALLVALUE(ALLVALUE newALLVALUE, NotificationChain msgs) {
		ALLVALUE oldALLVALUE = aLLVALUE;
		aLLVALUE = newALLVALUE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENVDATA__ALLVALUE, oldALLVALUE, newALLVALUE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setALLVALUE(ALLVALUE newALLVALUE) {
		if (newALLVALUE != aLLVALUE) {
			NotificationChain msgs = null;
			if (aLLVALUE != null)
				msgs = ((InternalEObject)aLLVALUE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ENVDATA__ALLVALUE, null, msgs);
			if (newALLVALUE != null)
				msgs = ((InternalEObject)newALLVALUE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ENVDATA__ALLVALUE, null, msgs);
			msgs = basicSetALLVALUE(newALLVALUE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENVDATA__ALLVALUE, newALLVALUE, newALLVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DTCVALUES getDTCVALUES() {
		return dTCVALUES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDTCVALUES(DTCVALUES newDTCVALUES, NotificationChain msgs) {
		DTCVALUES oldDTCVALUES = dTCVALUES;
		dTCVALUES = newDTCVALUES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENVDATA__DTCVALUES, oldDTCVALUES, newDTCVALUES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDTCVALUES(DTCVALUES newDTCVALUES) {
		if (newDTCVALUES != dTCVALUES) {
			NotificationChain msgs = null;
			if (dTCVALUES != null)
				msgs = ((InternalEObject)dTCVALUES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ENVDATA__DTCVALUES, null, msgs);
			if (newDTCVALUES != null)
				msgs = ((InternalEObject)newDTCVALUES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ENVDATA__DTCVALUES, null, msgs);
			msgs = basicSetDTCVALUES(newDTCVALUES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENVDATA__DTCVALUES, newDTCVALUES, newDTCVALUES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATA__ALLVALUE:
				return basicSetALLVALUE(null, msgs);
			case OdxXhtmlPackage.ENVDATA__DTCVALUES:
				return basicSetDTCVALUES(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATA__ALLVALUE:
				return getALLVALUE();
			case OdxXhtmlPackage.ENVDATA__DTCVALUES:
				return getDTCVALUES();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATA__ALLVALUE:
				setALLVALUE((ALLVALUE)newValue);
				return;
			case OdxXhtmlPackage.ENVDATA__DTCVALUES:
				setDTCVALUES((DTCVALUES)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATA__ALLVALUE:
				setALLVALUE((ALLVALUE)null);
				return;
			case OdxXhtmlPackage.ENVDATA__DTCVALUES:
				setDTCVALUES((DTCVALUES)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATA__ALLVALUE:
				return aLLVALUE != null;
			case OdxXhtmlPackage.ENVDATA__DTCVALUES:
				return dTCVALUES != null;
		}
		return super.eIsSet(featureID);
	}

} //ENVDATAImpl
