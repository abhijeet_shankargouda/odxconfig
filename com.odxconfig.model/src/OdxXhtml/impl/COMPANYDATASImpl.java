/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYDATA;
import OdxXhtml.COMPANYDATAS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYDATAS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYDATASImpl#getCOMPANYDATA <em>COMPANYDATA</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYDATASImpl extends MinimalEObjectImpl.Container implements COMPANYDATAS {
	/**
	 * The cached value of the '{@link #getCOMPANYDATA() <em>COMPANYDATA</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDATA()
	 * @generated
	 * @ordered
	 */
	protected EList<COMPANYDATA> cOMPANYDATA;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYDATASImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYDATAS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<COMPANYDATA> getCOMPANYDATA() {
		if (cOMPANYDATA == null) {
			cOMPANYDATA = new EObjectContainmentEList<COMPANYDATA>(COMPANYDATA.class, this, OdxXhtmlPackage.COMPANYDATAS__COMPANYDATA);
		}
		return cOMPANYDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATAS__COMPANYDATA:
				return ((InternalEList<?>)getCOMPANYDATA()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATAS__COMPANYDATA:
				return getCOMPANYDATA();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATAS__COMPANYDATA:
				getCOMPANYDATA().clear();
				getCOMPANYDATA().addAll((Collection<? extends COMPANYDATA>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATAS__COMPANYDATA:
				getCOMPANYDATA().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATAS__COMPANYDATA:
				return cOMPANYDATA != null && !cOMPANYDATA.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //COMPANYDATASImpl
