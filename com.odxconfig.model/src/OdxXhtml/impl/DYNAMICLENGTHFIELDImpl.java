/**
 */
package OdxXhtml.impl;

import OdxXhtml.DETERMINENUMBEROFITEMS;
import OdxXhtml.DYNAMICLENGTHFIELD;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DYNAMICLENGTHFIELD</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DYNAMICLENGTHFIELDImpl#getOFFSET <em>OFFSET</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNAMICLENGTHFIELDImpl#getDETERMINENUMBEROFITEMS <em>DETERMINENUMBEROFITEMS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DYNAMICLENGTHFIELDImpl extends FIELDImpl implements DYNAMICLENGTHFIELD {
	/**
	 * The default value of the '{@link #getOFFSET() <em>OFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOFFSET()
	 * @generated
	 * @ordered
	 */
	protected static final long OFFSET_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getOFFSET() <em>OFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOFFSET()
	 * @generated
	 * @ordered
	 */
	protected long oFFSET = OFFSET_EDEFAULT;

	/**
	 * This is true if the OFFSET attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean oFFSETESet;

	/**
	 * The cached value of the '{@link #getDETERMINENUMBEROFITEMS() <em>DETERMINENUMBEROFITEMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDETERMINENUMBEROFITEMS()
	 * @generated
	 * @ordered
	 */
	protected DETERMINENUMBEROFITEMS dETERMINENUMBEROFITEMS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DYNAMICLENGTHFIELDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDYNAMICLENGTHFIELD();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getOFFSET() {
		return oFFSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOFFSET(long newOFFSET) {
		long oldOFFSET = oFFSET;
		oFFSET = newOFFSET;
		boolean oldOFFSETESet = oFFSETESet;
		oFFSETESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNAMICLENGTHFIELD__OFFSET, oldOFFSET, oFFSET, !oldOFFSETESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetOFFSET() {
		long oldOFFSET = oFFSET;
		boolean oldOFFSETESet = oFFSETESet;
		oFFSET = OFFSET_EDEFAULT;
		oFFSETESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DYNAMICLENGTHFIELD__OFFSET, oldOFFSET, OFFSET_EDEFAULT, oldOFFSETESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetOFFSET() {
		return oFFSETESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DETERMINENUMBEROFITEMS getDETERMINENUMBEROFITEMS() {
		return dETERMINENUMBEROFITEMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDETERMINENUMBEROFITEMS(DETERMINENUMBEROFITEMS newDETERMINENUMBEROFITEMS, NotificationChain msgs) {
		DETERMINENUMBEROFITEMS oldDETERMINENUMBEROFITEMS = dETERMINENUMBEROFITEMS;
		dETERMINENUMBEROFITEMS = newDETERMINENUMBEROFITEMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNAMICLENGTHFIELD__DETERMINENUMBEROFITEMS, oldDETERMINENUMBEROFITEMS, newDETERMINENUMBEROFITEMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDETERMINENUMBEROFITEMS(DETERMINENUMBEROFITEMS newDETERMINENUMBEROFITEMS) {
		if (newDETERMINENUMBEROFITEMS != dETERMINENUMBEROFITEMS) {
			NotificationChain msgs = null;
			if (dETERMINENUMBEROFITEMS != null)
				msgs = ((InternalEObject)dETERMINENUMBEROFITEMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNAMICLENGTHFIELD__DETERMINENUMBEROFITEMS, null, msgs);
			if (newDETERMINENUMBEROFITEMS != null)
				msgs = ((InternalEObject)newDETERMINENUMBEROFITEMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNAMICLENGTHFIELD__DETERMINENUMBEROFITEMS, null, msgs);
			msgs = basicSetDETERMINENUMBEROFITEMS(newDETERMINENUMBEROFITEMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNAMICLENGTHFIELD__DETERMINENUMBEROFITEMS, newDETERMINENUMBEROFITEMS, newDETERMINENUMBEROFITEMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD__DETERMINENUMBEROFITEMS:
				return basicSetDETERMINENUMBEROFITEMS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD__OFFSET:
				return getOFFSET();
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD__DETERMINENUMBEROFITEMS:
				return getDETERMINENUMBEROFITEMS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD__OFFSET:
				setOFFSET((Long)newValue);
				return;
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD__DETERMINENUMBEROFITEMS:
				setDETERMINENUMBEROFITEMS((DETERMINENUMBEROFITEMS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD__OFFSET:
				unsetOFFSET();
				return;
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD__DETERMINENUMBEROFITEMS:
				setDETERMINENUMBEROFITEMS((DETERMINENUMBEROFITEMS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD__OFFSET:
				return isSetOFFSET();
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD__DETERMINENUMBEROFITEMS:
				return dETERMINENUMBEROFITEMS != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (oFFSET: ");
		if (oFFSETESet) result.append(oFFSET); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //DYNAMICLENGTHFIELDImpl
