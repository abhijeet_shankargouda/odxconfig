/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TARGETADDROFFSET;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TARGETADDROFFSET</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TARGETADDROFFSETImpl extends MinimalEObjectImpl.Container implements TARGETADDROFFSET {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TARGETADDROFFSETImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getTARGETADDROFFSET();
	}

} //TARGETADDROFFSETImpl
