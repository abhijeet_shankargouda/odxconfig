/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.DOCTYPE;
import OdxXhtml.LINKCOMPARAMREF;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LINKCOMPARAMREF</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.LINKCOMPARAMREFImpl#getVALUE <em>VALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.LINKCOMPARAMREFImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.LINKCOMPARAMREFImpl#getDOCREF <em>DOCREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.LINKCOMPARAMREFImpl#getDOCTYPE <em>DOCTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.LINKCOMPARAMREFImpl#getIDREF <em>IDREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.LINKCOMPARAMREFImpl#getREVISION <em>REVISION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LINKCOMPARAMREFImpl extends MinimalEObjectImpl.Container implements LINKCOMPARAMREF {
	/**
	 * The default value of the '{@link #getVALUE() <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVALUE() <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUE()
	 * @generated
	 * @ordered
	 */
	protected String vALUE = VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getDOCREF() <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREF()
	 * @generated
	 * @ordered
	 */
	protected static final String DOCREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDOCREF() <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREF()
	 * @generated
	 * @ordered
	 */
	protected String dOCREF = DOCREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getDOCTYPE() <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final DOCTYPE DOCTYPE_EDEFAULT = DOCTYPE.FLASH;

	/**
	 * The cached value of the '{@link #getDOCTYPE() <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCTYPE()
	 * @generated
	 * @ordered
	 */
	protected DOCTYPE dOCTYPE = DOCTYPE_EDEFAULT;

	/**
	 * This is true if the DOCTYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dOCTYPEESet;

	/**
	 * The default value of the '{@link #getIDREF() <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDREF()
	 * @generated
	 * @ordered
	 */
	protected static final String IDREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIDREF() <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDREF()
	 * @generated
	 * @ordered
	 */
	protected String iDREF = IDREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected static final String REVISION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected String rEVISION = REVISION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LINKCOMPARAMREFImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLINKCOMPARAMREF();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVALUE() {
		return vALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVALUE(String newVALUE) {
		String oldVALUE = vALUE;
		vALUE = newVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LINKCOMPARAMREF__VALUE, oldVALUE, vALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LINKCOMPARAMREF__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LINKCOMPARAMREF__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LINKCOMPARAMREF__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LINKCOMPARAMREF__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDOCREF() {
		return dOCREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCREF(String newDOCREF) {
		String oldDOCREF = dOCREF;
		dOCREF = newDOCREF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LINKCOMPARAMREF__DOCREF, oldDOCREF, dOCREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOCTYPE getDOCTYPE() {
		return dOCTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCTYPE(DOCTYPE newDOCTYPE) {
		DOCTYPE oldDOCTYPE = dOCTYPE;
		dOCTYPE = newDOCTYPE == null ? DOCTYPE_EDEFAULT : newDOCTYPE;
		boolean oldDOCTYPEESet = dOCTYPEESet;
		dOCTYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LINKCOMPARAMREF__DOCTYPE, oldDOCTYPE, dOCTYPE, !oldDOCTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDOCTYPE() {
		DOCTYPE oldDOCTYPE = dOCTYPE;
		boolean oldDOCTYPEESet = dOCTYPEESet;
		dOCTYPE = DOCTYPE_EDEFAULT;
		dOCTYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.LINKCOMPARAMREF__DOCTYPE, oldDOCTYPE, DOCTYPE_EDEFAULT, oldDOCTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDOCTYPE() {
		return dOCTYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIDREF() {
		return iDREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIDREF(String newIDREF) {
		String oldIDREF = iDREF;
		iDREF = newIDREF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LINKCOMPARAMREF__IDREF, oldIDREF, iDREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getREVISION() {
		return rEVISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREVISION(String newREVISION) {
		String oldREVISION = rEVISION;
		rEVISION = newREVISION;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LINKCOMPARAMREF__REVISION, oldREVISION, rEVISION));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREF__DESC:
				return basicSetDESC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREF__VALUE:
				return getVALUE();
			case OdxXhtmlPackage.LINKCOMPARAMREF__DESC:
				return getDESC();
			case OdxXhtmlPackage.LINKCOMPARAMREF__DOCREF:
				return getDOCREF();
			case OdxXhtmlPackage.LINKCOMPARAMREF__DOCTYPE:
				return getDOCTYPE();
			case OdxXhtmlPackage.LINKCOMPARAMREF__IDREF:
				return getIDREF();
			case OdxXhtmlPackage.LINKCOMPARAMREF__REVISION:
				return getREVISION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREF__VALUE:
				setVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__DOCREF:
				setDOCREF((String)newValue);
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__DOCTYPE:
				setDOCTYPE((DOCTYPE)newValue);
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__IDREF:
				setIDREF((String)newValue);
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__REVISION:
				setREVISION((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREF__VALUE:
				setVALUE(VALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__DOCREF:
				setDOCREF(DOCREF_EDEFAULT);
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__DOCTYPE:
				unsetDOCTYPE();
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__IDREF:
				setIDREF(IDREF_EDEFAULT);
				return;
			case OdxXhtmlPackage.LINKCOMPARAMREF__REVISION:
				setREVISION(REVISION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREF__VALUE:
				return VALUE_EDEFAULT == null ? vALUE != null : !VALUE_EDEFAULT.equals(vALUE);
			case OdxXhtmlPackage.LINKCOMPARAMREF__DESC:
				return dESC != null;
			case OdxXhtmlPackage.LINKCOMPARAMREF__DOCREF:
				return DOCREF_EDEFAULT == null ? dOCREF != null : !DOCREF_EDEFAULT.equals(dOCREF);
			case OdxXhtmlPackage.LINKCOMPARAMREF__DOCTYPE:
				return isSetDOCTYPE();
			case OdxXhtmlPackage.LINKCOMPARAMREF__IDREF:
				return IDREF_EDEFAULT == null ? iDREF != null : !IDREF_EDEFAULT.equals(iDREF);
			case OdxXhtmlPackage.LINKCOMPARAMREF__REVISION:
				return REVISION_EDEFAULT == null ? rEVISION != null : !REVISION_EDEFAULT.equals(rEVISION);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (vALUE: ");
		result.append(vALUE);
		result.append(", dOCREF: ");
		result.append(dOCREF);
		result.append(", dOCTYPE: ");
		if (dOCTYPEESet) result.append(dOCTYPE); else result.append("<unset>");
		result.append(", iDREF: ");
		result.append(iDREF);
		result.append(", rEVISION: ");
		result.append(rEVISION);
		result.append(')');
		return result.toString();
	}

} //LINKCOMPARAMREFImpl
