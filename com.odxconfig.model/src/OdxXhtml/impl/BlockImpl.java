/**
 */
package OdxXhtml.impl;

import OdxXhtml.Block;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.OlType;
import OdxXhtml.P;
import OdxXhtml.UlType;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.BlockImpl#getBlock <em>Block</em>}</li>
 *   <li>{@link OdxXhtml.impl.BlockImpl#getP <em>P</em>}</li>
 *   <li>{@link OdxXhtml.impl.BlockImpl#getUl <em>Ul</em>}</li>
 *   <li>{@link OdxXhtml.impl.BlockImpl#getOl <em>Ol</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BlockImpl extends MinimalEObjectImpl.Container implements Block {
	/**
	 * The cached value of the '{@link #getBlock() <em>Block</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlock()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap block;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getBlock();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getBlock() {
		if (block == null) {
			block = new BasicFeatureMap(this, OdxXhtmlPackage.BLOCK__BLOCK);
		}
		return block;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<P> getP() {
		return getBlock().list(OdxXhtmlPackage.eINSTANCE.getBlock_P());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<UlType> getUl() {
		return getBlock().list(OdxXhtmlPackage.eINSTANCE.getBlock_Ul());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<OlType> getOl() {
		return getBlock().list(OdxXhtmlPackage.eINSTANCE.getBlock_Ol());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.BLOCK__BLOCK:
				return ((InternalEList<?>)getBlock()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.BLOCK__P:
				return ((InternalEList<?>)getP()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.BLOCK__UL:
				return ((InternalEList<?>)getUl()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.BLOCK__OL:
				return ((InternalEList<?>)getOl()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.BLOCK__BLOCK:
				if (coreType) return getBlock();
				return ((FeatureMap.Internal)getBlock()).getWrapper();
			case OdxXhtmlPackage.BLOCK__P:
				return getP();
			case OdxXhtmlPackage.BLOCK__UL:
				return getUl();
			case OdxXhtmlPackage.BLOCK__OL:
				return getOl();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.BLOCK__BLOCK:
				((FeatureMap.Internal)getBlock()).set(newValue);
				return;
			case OdxXhtmlPackage.BLOCK__P:
				getP().clear();
				getP().addAll((Collection<? extends P>)newValue);
				return;
			case OdxXhtmlPackage.BLOCK__UL:
				getUl().clear();
				getUl().addAll((Collection<? extends UlType>)newValue);
				return;
			case OdxXhtmlPackage.BLOCK__OL:
				getOl().clear();
				getOl().addAll((Collection<? extends OlType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.BLOCK__BLOCK:
				getBlock().clear();
				return;
			case OdxXhtmlPackage.BLOCK__P:
				getP().clear();
				return;
			case OdxXhtmlPackage.BLOCK__UL:
				getUl().clear();
				return;
			case OdxXhtmlPackage.BLOCK__OL:
				getOl().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.BLOCK__BLOCK:
				return block != null && !block.isEmpty();
			case OdxXhtmlPackage.BLOCK__P:
				return !getP().isEmpty();
			case OdxXhtmlPackage.BLOCK__UL:
				return !getUl().isEmpty();
			case OdxXhtmlPackage.BLOCK__OL:
				return !getOl().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (block: ");
		result.append(block);
		result.append(')');
		return result.toString();
	}

} //BlockImpl
