/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.VARIABLEGROUP;
import OdxXhtml.VARIABLEGROUPS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VARIABLEGROUPS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.VARIABLEGROUPSImpl#getVARIABLEGROUP <em>VARIABLEGROUP</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VARIABLEGROUPSImpl extends MinimalEObjectImpl.Container implements VARIABLEGROUPS {
	/**
	 * The cached value of the '{@link #getVARIABLEGROUP() <em>VARIABLEGROUP</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVARIABLEGROUP()
	 * @generated
	 * @ordered
	 */
	protected EList<VARIABLEGROUP> vARIABLEGROUP;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VARIABLEGROUPSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVARIABLEGROUPS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<VARIABLEGROUP> getVARIABLEGROUP() {
		if (vARIABLEGROUP == null) {
			vARIABLEGROUP = new EObjectContainmentEList<VARIABLEGROUP>(VARIABLEGROUP.class, this, OdxXhtmlPackage.VARIABLEGROUPS__VARIABLEGROUP);
		}
		return vARIABLEGROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.VARIABLEGROUPS__VARIABLEGROUP:
				return ((InternalEList<?>)getVARIABLEGROUP()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.VARIABLEGROUPS__VARIABLEGROUP:
				return getVARIABLEGROUP();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.VARIABLEGROUPS__VARIABLEGROUP:
				getVARIABLEGROUP().clear();
				getVARIABLEGROUP().addAll((Collection<? extends VARIABLEGROUP>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VARIABLEGROUPS__VARIABLEGROUP:
				getVARIABLEGROUP().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VARIABLEGROUPS__VARIABLEGROUP:
				return vARIABLEGROUP != null && !vARIABLEGROUP.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //VARIABLEGROUPSImpl
