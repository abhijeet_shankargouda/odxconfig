/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPLEXDOP;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPLEXDOP</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class COMPLEXDOPImpl extends DOPBASEImpl implements COMPLEXDOP {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPLEXDOPImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPLEXDOP();
	}

} //COMPLEXDOPImpl
