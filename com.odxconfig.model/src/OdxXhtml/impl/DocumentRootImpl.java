/**
 */
package OdxXhtml.impl;

import OdxXhtml.BType;
import OdxXhtml.CATALOG;
import OdxXhtml.DocumentRoot;
import OdxXhtml.IType;
import OdxXhtml.LiType;
import OdxXhtml.ODX;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.OlType;
import OdxXhtml.SubType;
import OdxXhtml.SupType;
import OdxXhtml.UlType;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getB <em>B</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getBr <em>Br</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getCATALOG <em>CATALOG</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getI <em>I</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getLi <em>Li</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getOl <em>Ol</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getSub <em>Sub</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getSup <em>Sup</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getUl <em>Ul</em>}</li>
 *   <li>{@link OdxXhtml.impl.DocumentRootImpl#getODX <em>ODX</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DocumentRootImpl extends MinimalEObjectImpl.Container implements DocumentRoot {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXMLNSPrefixMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xMLNSPrefixMap;

	/**
	 * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXSISchemaLocation()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xSISchemaLocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDocumentRoot();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, OdxXhtmlPackage.DOCUMENT_ROOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<String, String> getXMLNSPrefixMap() {
		if (xMLNSPrefixMap == null) {
			xMLNSPrefixMap = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, OdxXhtmlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		}
		return xMLNSPrefixMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<String, String> getXSISchemaLocation() {
		if (xSISchemaLocation == null) {
			xSISchemaLocation = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, OdxXhtmlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		}
		return xSISchemaLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BType getB() {
		return (BType)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_B(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetB(BType newB, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_B(), newB, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setB(BType newB) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_B(), newB);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject getBr() {
		return (EObject)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Br(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBr(EObject newBr, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Br(), newBr, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBr(EObject newBr) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Br(), newBr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CATALOG getCATALOG() {
		return (CATALOG)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_CATALOG(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCATALOG(CATALOG newCATALOG, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_CATALOG(), newCATALOG, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCATALOG(CATALOG newCATALOG) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_CATALOG(), newCATALOG);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IType getI() {
		return (IType)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_I(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetI(IType newI, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_I(), newI, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setI(IType newI) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_I(), newI);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LiType getLi() {
		return (LiType)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Li(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLi(LiType newLi, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Li(), newLi, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLi(LiType newLi) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Li(), newLi);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OlType getOl() {
		return (OlType)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Ol(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOl(OlType newOl, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Ol(), newOl, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOl(OlType newOl) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Ol(), newOl);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SubType getSub() {
		return (SubType)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Sub(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSub(SubType newSub, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Sub(), newSub, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSub(SubType newSub) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Sub(), newSub);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SupType getSup() {
		return (SupType)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Sup(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSup(SupType newSup, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Sup(), newSup, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSup(SupType newSup) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Sup(), newSup);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UlType getUl() {
		return (UlType)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Ul(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUl(UlType newUl, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Ul(), newUl, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUl(UlType newUl) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_Ul(), newUl);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODX getODX() {
		return (ODX)getMixed().get(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_ODX(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetODX(ODX newODX, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_ODX(), newODX, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setODX(ODX newODX) {
		((FeatureMap.Internal)getMixed()).set(OdxXhtmlPackage.eINSTANCE.getDocumentRoot_ODX(), newODX);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCUMENT_ROOT__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return ((InternalEList<?>)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return ((InternalEList<?>)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__B:
				return basicSetB(null, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__BR:
				return basicSetBr(null, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__CATALOG:
				return basicSetCATALOG(null, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__I:
				return basicSetI(null, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__LI:
				return basicSetLi(null, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__OL:
				return basicSetOl(null, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUB:
				return basicSetSub(null, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUP:
				return basicSetSup(null, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__UL:
				return basicSetUl(null, msgs);
			case OdxXhtmlPackage.DOCUMENT_ROOT__ODX:
				return basicSetODX(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCUMENT_ROOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case OdxXhtmlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				if (coreType) return getXMLNSPrefixMap();
				else return getXMLNSPrefixMap().map();
			case OdxXhtmlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				if (coreType) return getXSISchemaLocation();
				else return getXSISchemaLocation().map();
			case OdxXhtmlPackage.DOCUMENT_ROOT__B:
				return getB();
			case OdxXhtmlPackage.DOCUMENT_ROOT__BR:
				return getBr();
			case OdxXhtmlPackage.DOCUMENT_ROOT__CATALOG:
				return getCATALOG();
			case OdxXhtmlPackage.DOCUMENT_ROOT__I:
				return getI();
			case OdxXhtmlPackage.DOCUMENT_ROOT__LI:
				return getLi();
			case OdxXhtmlPackage.DOCUMENT_ROOT__OL:
				return getOl();
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUB:
				return getSub();
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUP:
				return getSup();
			case OdxXhtmlPackage.DOCUMENT_ROOT__UL:
				return getUl();
			case OdxXhtmlPackage.DOCUMENT_ROOT__ODX:
				return getODX();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCUMENT_ROOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				((EStructuralFeature.Setting)getXMLNSPrefixMap()).set(newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				((EStructuralFeature.Setting)getXSISchemaLocation()).set(newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__B:
				setB((BType)newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__BR:
				setBr((EObject)newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__CATALOG:
				setCATALOG((CATALOG)newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__I:
				setI((IType)newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__LI:
				setLi((LiType)newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__OL:
				setOl((OlType)newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUB:
				setSub((SubType)newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUP:
				setSup((SupType)newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__UL:
				setUl((UlType)newValue);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__ODX:
				setODX((ODX)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCUMENT_ROOT__MIXED:
				getMixed().clear();
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				getXMLNSPrefixMap().clear();
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				getXSISchemaLocation().clear();
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__B:
				setB((BType)null);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__BR:
				setBr((EObject)null);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__CATALOG:
				setCATALOG((CATALOG)null);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__I:
				setI((IType)null);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__LI:
				setLi((LiType)null);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__OL:
				setOl((OlType)null);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUB:
				setSub((SubType)null);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUP:
				setSup((SupType)null);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__UL:
				setUl((UlType)null);
				return;
			case OdxXhtmlPackage.DOCUMENT_ROOT__ODX:
				setODX((ODX)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCUMENT_ROOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case OdxXhtmlPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
			case OdxXhtmlPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
			case OdxXhtmlPackage.DOCUMENT_ROOT__B:
				return getB() != null;
			case OdxXhtmlPackage.DOCUMENT_ROOT__BR:
				return getBr() != null;
			case OdxXhtmlPackage.DOCUMENT_ROOT__CATALOG:
				return getCATALOG() != null;
			case OdxXhtmlPackage.DOCUMENT_ROOT__I:
				return getI() != null;
			case OdxXhtmlPackage.DOCUMENT_ROOT__LI:
				return getLi() != null;
			case OdxXhtmlPackage.DOCUMENT_ROOT__OL:
				return getOl() != null;
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUB:
				return getSub() != null;
			case OdxXhtmlPackage.DOCUMENT_ROOT__SUP:
				return getSup() != null;
			case OdxXhtmlPackage.DOCUMENT_ROOT__UL:
				return getUl() != null;
			case OdxXhtmlPackage.DOCUMENT_ROOT__ODX:
				return getODX() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DocumentRootImpl
