/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADDRDEFFILTER;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ADDRDEFFILTER</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ADDRDEFFILTERImpl#getFILTEREND <em>FILTEREND</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ADDRDEFFILTERImpl extends FILTERImpl implements ADDRDEFFILTER {
	/**
	 * The default value of the '{@link #getFILTEREND() <em>FILTEREND</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILTEREND()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] FILTEREND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFILTEREND() <em>FILTEREND</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILTEREND()
	 * @generated
	 * @ordered
	 */
	protected byte[] fILTEREND = FILTEREND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ADDRDEFFILTERImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getADDRDEFFILTER();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getFILTEREND() {
		return fILTEREND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFILTEREND(byte[] newFILTEREND) {
		byte[] oldFILTEREND = fILTEREND;
		fILTEREND = newFILTEREND;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ADDRDEFFILTER__FILTEREND, oldFILTEREND, fILTEREND));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ADDRDEFFILTER__FILTEREND:
				return getFILTEREND();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ADDRDEFFILTER__FILTEREND:
				setFILTEREND((byte[])newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ADDRDEFFILTER__FILTEREND:
				setFILTEREND(FILTEREND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ADDRDEFFILTER__FILTEREND:
				return FILTEREND_EDEFAULT == null ? fILTEREND != null : !FILTEREND_EDEFAULT.equals(fILTEREND);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (fILTEREND: ");
		result.append(fILTEREND);
		result.append(')');
		return result.toString();
	}

} //ADDRDEFFILTERImpl
