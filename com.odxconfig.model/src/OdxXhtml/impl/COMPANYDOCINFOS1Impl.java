/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYDOCINFO1;
import OdxXhtml.COMPANYDOCINFOS1;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYDOCINFOS1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYDOCINFOS1Impl#getCOMPANYDOCINFO <em>COMPANYDOCINFO</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYDOCINFOS1Impl extends MinimalEObjectImpl.Container implements COMPANYDOCINFOS1 {
	/**
	 * The cached value of the '{@link #getCOMPANYDOCINFO() <em>COMPANYDOCINFO</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDOCINFO()
	 * @generated
	 * @ordered
	 */
	protected EList<COMPANYDOCINFO1> cOMPANYDOCINFO;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYDOCINFOS1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYDOCINFOS1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<COMPANYDOCINFO1> getCOMPANYDOCINFO() {
		if (cOMPANYDOCINFO == null) {
			cOMPANYDOCINFO = new EObjectContainmentEList<COMPANYDOCINFO1>(COMPANYDOCINFO1.class, this, OdxXhtmlPackage.COMPANYDOCINFOS1__COMPANYDOCINFO);
		}
		return cOMPANYDOCINFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS1__COMPANYDOCINFO:
				return ((InternalEList<?>)getCOMPANYDOCINFO()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS1__COMPANYDOCINFO:
				return getCOMPANYDOCINFO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS1__COMPANYDOCINFO:
				getCOMPANYDOCINFO().clear();
				getCOMPANYDOCINFO().addAll((Collection<? extends COMPANYDOCINFO1>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS1__COMPANYDOCINFO:
				getCOMPANYDOCINFO().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS1__COMPANYDOCINFO:
				return cOMPANYDOCINFO != null && !cOMPANYDOCINFO.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //COMPANYDOCINFOS1Impl
