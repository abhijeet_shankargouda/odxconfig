/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATAFORMAT;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.ENCRYPTCOMPRESSMETHOD;
import OdxXhtml.FLASHDATA;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FLASHDATA</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FLASHDATAImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.FLASHDATAImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.FLASHDATAImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.FLASHDATAImpl#getDATAFORMAT <em>DATAFORMAT</em>}</li>
 *   <li>{@link OdxXhtml.impl.FLASHDATAImpl#getENCRYPTCOMPRESSMETHOD <em>ENCRYPTCOMPRESSMETHOD</em>}</li>
 *   <li>{@link OdxXhtml.impl.FLASHDATAImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.FLASHDATAImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FLASHDATAImpl extends MinimalEObjectImpl.Container implements FLASHDATA {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getDATAFORMAT() <em>DATAFORMAT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATAFORMAT()
	 * @generated
	 * @ordered
	 */
	protected DATAFORMAT dATAFORMAT;

	/**
	 * The cached value of the '{@link #getENCRYPTCOMPRESSMETHOD() <em>ENCRYPTCOMPRESSMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENCRYPTCOMPRESSMETHOD()
	 * @generated
	 * @ordered
	 */
	protected ENCRYPTCOMPRESSMETHOD eNCRYPTCOMPRESSMETHOD;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FLASHDATAImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFLASHDATA();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASHDATA__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASHDATA__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASHDATA__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASHDATA__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATAFORMAT getDATAFORMAT() {
		return dATAFORMAT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDATAFORMAT(DATAFORMAT newDATAFORMAT, NotificationChain msgs) {
		DATAFORMAT oldDATAFORMAT = dATAFORMAT;
		dATAFORMAT = newDATAFORMAT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__DATAFORMAT, oldDATAFORMAT, newDATAFORMAT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATAFORMAT(DATAFORMAT newDATAFORMAT) {
		if (newDATAFORMAT != dATAFORMAT) {
			NotificationChain msgs = null;
			if (dATAFORMAT != null)
				msgs = ((InternalEObject)dATAFORMAT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASHDATA__DATAFORMAT, null, msgs);
			if (newDATAFORMAT != null)
				msgs = ((InternalEObject)newDATAFORMAT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASHDATA__DATAFORMAT, null, msgs);
			msgs = basicSetDATAFORMAT(newDATAFORMAT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__DATAFORMAT, newDATAFORMAT, newDATAFORMAT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENCRYPTCOMPRESSMETHOD getENCRYPTCOMPRESSMETHOD() {
		return eNCRYPTCOMPRESSMETHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetENCRYPTCOMPRESSMETHOD(ENCRYPTCOMPRESSMETHOD newENCRYPTCOMPRESSMETHOD, NotificationChain msgs) {
		ENCRYPTCOMPRESSMETHOD oldENCRYPTCOMPRESSMETHOD = eNCRYPTCOMPRESSMETHOD;
		eNCRYPTCOMPRESSMETHOD = newENCRYPTCOMPRESSMETHOD;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__ENCRYPTCOMPRESSMETHOD, oldENCRYPTCOMPRESSMETHOD, newENCRYPTCOMPRESSMETHOD);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setENCRYPTCOMPRESSMETHOD(ENCRYPTCOMPRESSMETHOD newENCRYPTCOMPRESSMETHOD) {
		if (newENCRYPTCOMPRESSMETHOD != eNCRYPTCOMPRESSMETHOD) {
			NotificationChain msgs = null;
			if (eNCRYPTCOMPRESSMETHOD != null)
				msgs = ((InternalEObject)eNCRYPTCOMPRESSMETHOD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASHDATA__ENCRYPTCOMPRESSMETHOD, null, msgs);
			if (newENCRYPTCOMPRESSMETHOD != null)
				msgs = ((InternalEObject)newENCRYPTCOMPRESSMETHOD).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FLASHDATA__ENCRYPTCOMPRESSMETHOD, null, msgs);
			msgs = basicSetENCRYPTCOMPRESSMETHOD(newENCRYPTCOMPRESSMETHOD, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__ENCRYPTCOMPRESSMETHOD, newENCRYPTCOMPRESSMETHOD, newENCRYPTCOMPRESSMETHOD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FLASHDATA__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATA__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.FLASHDATA__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.FLASHDATA__DATAFORMAT:
				return basicSetDATAFORMAT(null, msgs);
			case OdxXhtmlPackage.FLASHDATA__ENCRYPTCOMPRESSMETHOD:
				return basicSetENCRYPTCOMPRESSMETHOD(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATA__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.FLASHDATA__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.FLASHDATA__DESC:
				return getDESC();
			case OdxXhtmlPackage.FLASHDATA__DATAFORMAT:
				return getDATAFORMAT();
			case OdxXhtmlPackage.FLASHDATA__ENCRYPTCOMPRESSMETHOD:
				return getENCRYPTCOMPRESSMETHOD();
			case OdxXhtmlPackage.FLASHDATA__ID:
				return getID();
			case OdxXhtmlPackage.FLASHDATA__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATA__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.FLASHDATA__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.FLASHDATA__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.FLASHDATA__DATAFORMAT:
				setDATAFORMAT((DATAFORMAT)newValue);
				return;
			case OdxXhtmlPackage.FLASHDATA__ENCRYPTCOMPRESSMETHOD:
				setENCRYPTCOMPRESSMETHOD((ENCRYPTCOMPRESSMETHOD)newValue);
				return;
			case OdxXhtmlPackage.FLASHDATA__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.FLASHDATA__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATA__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.FLASHDATA__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.FLASHDATA__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.FLASHDATA__DATAFORMAT:
				setDATAFORMAT((DATAFORMAT)null);
				return;
			case OdxXhtmlPackage.FLASHDATA__ENCRYPTCOMPRESSMETHOD:
				setENCRYPTCOMPRESSMETHOD((ENCRYPTCOMPRESSMETHOD)null);
				return;
			case OdxXhtmlPackage.FLASHDATA__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.FLASHDATA__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATA__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.FLASHDATA__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.FLASHDATA__DESC:
				return dESC != null;
			case OdxXhtmlPackage.FLASHDATA__DATAFORMAT:
				return dATAFORMAT != null;
			case OdxXhtmlPackage.FLASHDATA__ENCRYPTCOMPRESSMETHOD:
				return eNCRYPTCOMPRESSMETHOD != null;
			case OdxXhtmlPackage.FLASHDATA__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.FLASHDATA__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //FLASHDATAImpl
