/**
 */
package OdxXhtml.impl;

import OdxXhtml.NEGRESPONSE;
import OdxXhtml.NEGRESPONSES;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NEGRESPONSES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.NEGRESPONSESImpl#getNEGRESPONSE <em>NEGRESPONSE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NEGRESPONSESImpl extends MinimalEObjectImpl.Container implements NEGRESPONSES {
	/**
	 * The cached value of the '{@link #getNEGRESPONSE() <em>NEGRESPONSE</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNEGRESPONSE()
	 * @generated
	 * @ordered
	 */
	protected EList<NEGRESPONSE> nEGRESPONSE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NEGRESPONSESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getNEGRESPONSES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<NEGRESPONSE> getNEGRESPONSE() {
		if (nEGRESPONSE == null) {
			nEGRESPONSE = new EObjectContainmentEList<NEGRESPONSE>(NEGRESPONSE.class, this, OdxXhtmlPackage.NEGRESPONSES__NEGRESPONSE);
		}
		return nEGRESPONSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSES__NEGRESPONSE:
				return ((InternalEList<?>)getNEGRESPONSE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSES__NEGRESPONSE:
				return getNEGRESPONSE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSES__NEGRESPONSE:
				getNEGRESPONSE().clear();
				getNEGRESPONSE().addAll((Collection<? extends NEGRESPONSE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSES__NEGRESPONSE:
				getNEGRESPONSE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSES__NEGRESPONSE:
				return nEGRESPONSE != null && !nEGRESPONSE.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //NEGRESPONSESImpl
