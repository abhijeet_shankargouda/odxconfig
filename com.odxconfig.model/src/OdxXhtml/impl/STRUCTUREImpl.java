/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.STRUCTURE;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>STRUCTURE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.STRUCTUREImpl#isISVISIBLE <em>ISVISIBLE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class STRUCTUREImpl extends BASICSTRUCTUREImpl implements STRUCTURE {
	/**
	 * The default value of the '{@link #isISVISIBLE() <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISVISIBLE()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISVISIBLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISVISIBLE() <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISVISIBLE()
	 * @generated
	 * @ordered
	 */
	protected boolean iSVISIBLE = ISVISIBLE_EDEFAULT;

	/**
	 * This is true if the ISVISIBLE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSVISIBLEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected STRUCTUREImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSTRUCTURE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISVISIBLE() {
		return iSVISIBLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISVISIBLE(boolean newISVISIBLE) {
		boolean oldISVISIBLE = iSVISIBLE;
		iSVISIBLE = newISVISIBLE;
		boolean oldISVISIBLEESet = iSVISIBLEESet;
		iSVISIBLEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.STRUCTURE__ISVISIBLE, oldISVISIBLE, iSVISIBLE, !oldISVISIBLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISVISIBLE() {
		boolean oldISVISIBLE = iSVISIBLE;
		boolean oldISVISIBLEESet = iSVISIBLEESet;
		iSVISIBLE = ISVISIBLE_EDEFAULT;
		iSVISIBLEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.STRUCTURE__ISVISIBLE, oldISVISIBLE, ISVISIBLE_EDEFAULT, oldISVISIBLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISVISIBLE() {
		return iSVISIBLEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.STRUCTURE__ISVISIBLE:
				return isISVISIBLE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.STRUCTURE__ISVISIBLE:
				setISVISIBLE((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STRUCTURE__ISVISIBLE:
				unsetISVISIBLE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STRUCTURE__ISVISIBLE:
				return isSetISVISIBLE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (iSVISIBLE: ");
		if (iSVISIBLEESet) result.append(iSVISIBLE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //STRUCTUREImpl
