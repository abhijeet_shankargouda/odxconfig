/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYDATA;
import OdxXhtml.COMPANYSPECIFICINFO;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.ROLES;
import OdxXhtml.TEAMMEMBERS;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYDATA</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYDATAImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATAImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATAImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATAImpl#getROLES <em>ROLES</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATAImpl#getTEAMMEMBERS <em>TEAMMEMBERS</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATAImpl#getCOMPANYSPECIFICINFO <em>COMPANYSPECIFICINFO</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATAImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATAImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYDATAImpl extends MinimalEObjectImpl.Container implements COMPANYDATA {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getROLES() <em>ROLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getROLES()
	 * @generated
	 * @ordered
	 */
	protected ROLES rOLES;

	/**
	 * The cached value of the '{@link #getTEAMMEMBERS() <em>TEAMMEMBERS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTEAMMEMBERS()
	 * @generated
	 * @ordered
	 */
	protected TEAMMEMBERS tEAMMEMBERS;

	/**
	 * The cached value of the '{@link #getCOMPANYSPECIFICINFO() <em>COMPANYSPECIFICINFO</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYSPECIFICINFO()
	 * @generated
	 * @ordered
	 */
	protected COMPANYSPECIFICINFO cOMPANYSPECIFICINFO;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYDATAImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYDATA();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ROLES getROLES() {
		return rOLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetROLES(ROLES newROLES, NotificationChain msgs) {
		ROLES oldROLES = rOLES;
		rOLES = newROLES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__ROLES, oldROLES, newROLES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setROLES(ROLES newROLES) {
		if (newROLES != rOLES) {
			NotificationChain msgs = null;
			if (rOLES != null)
				msgs = ((InternalEObject)rOLES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__ROLES, null, msgs);
			if (newROLES != null)
				msgs = ((InternalEObject)newROLES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__ROLES, null, msgs);
			msgs = basicSetROLES(newROLES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__ROLES, newROLES, newROLES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEAMMEMBERS getTEAMMEMBERS() {
		return tEAMMEMBERS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTEAMMEMBERS(TEAMMEMBERS newTEAMMEMBERS, NotificationChain msgs) {
		TEAMMEMBERS oldTEAMMEMBERS = tEAMMEMBERS;
		tEAMMEMBERS = newTEAMMEMBERS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__TEAMMEMBERS, oldTEAMMEMBERS, newTEAMMEMBERS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTEAMMEMBERS(TEAMMEMBERS newTEAMMEMBERS) {
		if (newTEAMMEMBERS != tEAMMEMBERS) {
			NotificationChain msgs = null;
			if (tEAMMEMBERS != null)
				msgs = ((InternalEObject)tEAMMEMBERS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__TEAMMEMBERS, null, msgs);
			if (newTEAMMEMBERS != null)
				msgs = ((InternalEObject)newTEAMMEMBERS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__TEAMMEMBERS, null, msgs);
			msgs = basicSetTEAMMEMBERS(newTEAMMEMBERS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__TEAMMEMBERS, newTEAMMEMBERS, newTEAMMEMBERS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYSPECIFICINFO getCOMPANYSPECIFICINFO() {
		return cOMPANYSPECIFICINFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO newCOMPANYSPECIFICINFO, NotificationChain msgs) {
		COMPANYSPECIFICINFO oldCOMPANYSPECIFICINFO = cOMPANYSPECIFICINFO;
		cOMPANYSPECIFICINFO = newCOMPANYSPECIFICINFO;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__COMPANYSPECIFICINFO, oldCOMPANYSPECIFICINFO, newCOMPANYSPECIFICINFO);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO newCOMPANYSPECIFICINFO) {
		if (newCOMPANYSPECIFICINFO != cOMPANYSPECIFICINFO) {
			NotificationChain msgs = null;
			if (cOMPANYSPECIFICINFO != null)
				msgs = ((InternalEObject)cOMPANYSPECIFICINFO).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__COMPANYSPECIFICINFO, null, msgs);
			if (newCOMPANYSPECIFICINFO != null)
				msgs = ((InternalEObject)newCOMPANYSPECIFICINFO).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA__COMPANYSPECIFICINFO, null, msgs);
			msgs = basicSetCOMPANYSPECIFICINFO(newCOMPANYSPECIFICINFO, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__COMPANYSPECIFICINFO, newCOMPANYSPECIFICINFO, newCOMPANYSPECIFICINFO));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.COMPANYDATA__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.COMPANYDATA__ROLES:
				return basicSetROLES(null, msgs);
			case OdxXhtmlPackage.COMPANYDATA__TEAMMEMBERS:
				return basicSetTEAMMEMBERS(null, msgs);
			case OdxXhtmlPackage.COMPANYDATA__COMPANYSPECIFICINFO:
				return basicSetCOMPANYSPECIFICINFO(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.COMPANYDATA__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.COMPANYDATA__DESC:
				return getDESC();
			case OdxXhtmlPackage.COMPANYDATA__ROLES:
				return getROLES();
			case OdxXhtmlPackage.COMPANYDATA__TEAMMEMBERS:
				return getTEAMMEMBERS();
			case OdxXhtmlPackage.COMPANYDATA__COMPANYSPECIFICINFO:
				return getCOMPANYSPECIFICINFO();
			case OdxXhtmlPackage.COMPANYDATA__ID:
				return getID();
			case OdxXhtmlPackage.COMPANYDATA__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA__ROLES:
				setROLES((ROLES)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA__TEAMMEMBERS:
				setTEAMMEMBERS((TEAMMEMBERS)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA__COMPANYSPECIFICINFO:
				setCOMPANYSPECIFICINFO((COMPANYSPECIFICINFO)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPANYDATA__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA__ROLES:
				setROLES((ROLES)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA__TEAMMEMBERS:
				setTEAMMEMBERS((TEAMMEMBERS)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA__COMPANYSPECIFICINFO:
				setCOMPANYSPECIFICINFO((COMPANYSPECIFICINFO)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPANYDATA__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.COMPANYDATA__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.COMPANYDATA__DESC:
				return dESC != null;
			case OdxXhtmlPackage.COMPANYDATA__ROLES:
				return rOLES != null;
			case OdxXhtmlPackage.COMPANYDATA__TEAMMEMBERS:
				return tEAMMEMBERS != null;
			case OdxXhtmlPackage.COMPANYDATA__COMPANYSPECIFICINFO:
				return cOMPANYSPECIFICINFO != null;
			case OdxXhtmlPackage.COMPANYDATA__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.COMPANYDATA__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //COMPANYDATAImpl
