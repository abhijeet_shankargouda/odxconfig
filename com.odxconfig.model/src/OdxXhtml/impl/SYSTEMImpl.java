/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;
import OdxXhtml.SYSTEM;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SYSTEM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SYSTEMImpl#getDOPREF <em>DOPREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.SYSTEMImpl#getDOPSNREF <em>DOPSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.SYSTEMImpl#getSYSPARAM <em>SYSPARAM</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SYSTEMImpl extends POSITIONABLEPARAMImpl implements SYSTEM {
	/**
	 * The cached value of the '{@link #getDOPREF() <em>DOPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOPREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK dOPREF;

	/**
	 * The cached value of the '{@link #getDOPSNREF() <em>DOPSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOPSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dOPSNREF;

	/**
	 * The default value of the '{@link #getSYSPARAM() <em>SYSPARAM</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSYSPARAM()
	 * @generated
	 * @ordered
	 */
	protected static final String SYSPARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSYSPARAM() <em>SYSPARAM</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSYSPARAM()
	 * @generated
	 * @ordered
	 */
	protected String sYSPARAM = SYSPARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SYSTEMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSYSTEM();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getDOPREF() {
		return dOPREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDOPREF(ODXLINK newDOPREF, NotificationChain msgs) {
		ODXLINK oldDOPREF = dOPREF;
		dOPREF = newDOPREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SYSTEM__DOPREF, oldDOPREF, newDOPREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOPREF(ODXLINK newDOPREF) {
		if (newDOPREF != dOPREF) {
			NotificationChain msgs = null;
			if (dOPREF != null)
				msgs = ((InternalEObject)dOPREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SYSTEM__DOPREF, null, msgs);
			if (newDOPREF != null)
				msgs = ((InternalEObject)newDOPREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SYSTEM__DOPREF, null, msgs);
			msgs = basicSetDOPREF(newDOPREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SYSTEM__DOPREF, newDOPREF, newDOPREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDOPSNREF() {
		return dOPSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDOPSNREF(SNREF newDOPSNREF, NotificationChain msgs) {
		SNREF oldDOPSNREF = dOPSNREF;
		dOPSNREF = newDOPSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SYSTEM__DOPSNREF, oldDOPSNREF, newDOPSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOPSNREF(SNREF newDOPSNREF) {
		if (newDOPSNREF != dOPSNREF) {
			NotificationChain msgs = null;
			if (dOPSNREF != null)
				msgs = ((InternalEObject)dOPSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SYSTEM__DOPSNREF, null, msgs);
			if (newDOPSNREF != null)
				msgs = ((InternalEObject)newDOPSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SYSTEM__DOPSNREF, null, msgs);
			msgs = basicSetDOPSNREF(newDOPSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SYSTEM__DOPSNREF, newDOPSNREF, newDOPSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSYSPARAM() {
		return sYSPARAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSYSPARAM(String newSYSPARAM) {
		String oldSYSPARAM = sYSPARAM;
		sYSPARAM = newSYSPARAM;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SYSTEM__SYSPARAM, oldSYSPARAM, sYSPARAM));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SYSTEM__DOPREF:
				return basicSetDOPREF(null, msgs);
			case OdxXhtmlPackage.SYSTEM__DOPSNREF:
				return basicSetDOPSNREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SYSTEM__DOPREF:
				return getDOPREF();
			case OdxXhtmlPackage.SYSTEM__DOPSNREF:
				return getDOPSNREF();
			case OdxXhtmlPackage.SYSTEM__SYSPARAM:
				return getSYSPARAM();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SYSTEM__DOPREF:
				setDOPREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.SYSTEM__DOPSNREF:
				setDOPSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.SYSTEM__SYSPARAM:
				setSYSPARAM((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SYSTEM__DOPREF:
				setDOPREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.SYSTEM__DOPSNREF:
				setDOPSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.SYSTEM__SYSPARAM:
				setSYSPARAM(SYSPARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SYSTEM__DOPREF:
				return dOPREF != null;
			case OdxXhtmlPackage.SYSTEM__DOPSNREF:
				return dOPSNREF != null;
			case OdxXhtmlPackage.SYSTEM__SYSPARAM:
				return SYSPARAM_EDEFAULT == null ? sYSPARAM != null : !SYSPARAM_EDEFAULT.equals(sYSPARAM);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sYSPARAM: ");
		result.append(sYSPARAM);
		result.append(')');
		return result.toString();
	}

} //SYSTEMImpl
