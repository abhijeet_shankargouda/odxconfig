/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.STANDARDLENGTHTYPE;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>STANDARDLENGTHTYPE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.STANDARDLENGTHTYPEImpl#getBITLENGTH <em>BITLENGTH</em>}</li>
 *   <li>{@link OdxXhtml.impl.STANDARDLENGTHTYPEImpl#getBITMASK <em>BITMASK</em>}</li>
 * </ul>
 *
 * @generated
 */
public class STANDARDLENGTHTYPEImpl extends DIAGCODEDTYPEImpl implements STANDARDLENGTHTYPE {
	/**
	 * The default value of the '{@link #getBITLENGTH() <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITLENGTH()
	 * @generated
	 * @ordered
	 */
	protected static final long BITLENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBITLENGTH() <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITLENGTH()
	 * @generated
	 * @ordered
	 */
	protected long bITLENGTH = BITLENGTH_EDEFAULT;

	/**
	 * This is true if the BITLENGTH attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bITLENGTHESet;

	/**
	 * The default value of the '{@link #getBITMASK() <em>BITMASK</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITMASK()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] BITMASK_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBITMASK() <em>BITMASK</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITMASK()
	 * @generated
	 * @ordered
	 */
	protected byte[] bITMASK = BITMASK_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected STANDARDLENGTHTYPEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSTANDARDLENGTHTYPE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBITLENGTH() {
		return bITLENGTH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBITLENGTH(long newBITLENGTH) {
		long oldBITLENGTH = bITLENGTH;
		bITLENGTH = newBITLENGTH;
		boolean oldBITLENGTHESet = bITLENGTHESet;
		bITLENGTHESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.STANDARDLENGTHTYPE__BITLENGTH, oldBITLENGTH, bITLENGTH, !oldBITLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBITLENGTH() {
		long oldBITLENGTH = bITLENGTH;
		boolean oldBITLENGTHESet = bITLENGTHESet;
		bITLENGTH = BITLENGTH_EDEFAULT;
		bITLENGTHESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.STANDARDLENGTHTYPE__BITLENGTH, oldBITLENGTH, BITLENGTH_EDEFAULT, oldBITLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBITLENGTH() {
		return bITLENGTHESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getBITMASK() {
		return bITMASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBITMASK(byte[] newBITMASK) {
		byte[] oldBITMASK = bITMASK;
		bITMASK = newBITMASK;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.STANDARDLENGTHTYPE__BITMASK, oldBITMASK, bITMASK));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.STANDARDLENGTHTYPE__BITLENGTH:
				return getBITLENGTH();
			case OdxXhtmlPackage.STANDARDLENGTHTYPE__BITMASK:
				return getBITMASK();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.STANDARDLENGTHTYPE__BITLENGTH:
				setBITLENGTH((Long)newValue);
				return;
			case OdxXhtmlPackage.STANDARDLENGTHTYPE__BITMASK:
				setBITMASK((byte[])newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STANDARDLENGTHTYPE__BITLENGTH:
				unsetBITLENGTH();
				return;
			case OdxXhtmlPackage.STANDARDLENGTHTYPE__BITMASK:
				setBITMASK(BITMASK_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STANDARDLENGTHTYPE__BITLENGTH:
				return isSetBITLENGTH();
			case OdxXhtmlPackage.STANDARDLENGTHTYPE__BITMASK:
				return BITMASK_EDEFAULT == null ? bITMASK != null : !BITMASK_EDEFAULT.equals(bITMASK);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bITLENGTH: ");
		if (bITLENGTHESet) result.append(bITLENGTH); else result.append("<unset>");
		result.append(", bITMASK: ");
		result.append(bITMASK);
		result.append(')');
		return result.toString();
	}

} //STANDARDLENGTHTYPEImpl
