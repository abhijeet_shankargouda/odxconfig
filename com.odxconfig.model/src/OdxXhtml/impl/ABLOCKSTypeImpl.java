/**
 */
package OdxXhtml.impl;

import OdxXhtml.ABLOCK;
import OdxXhtml.ABLOCKSType;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ABLOCKS Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ABLOCKSTypeImpl#getABLOCK <em>ABLOCK</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ABLOCKSTypeImpl extends MinimalEObjectImpl.Container implements ABLOCKSType {
	/**
	 * The cached value of the '{@link #getABLOCK() <em>ABLOCK</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABLOCK()
	 * @generated
	 * @ordered
	 */
	protected EList<ABLOCK> aBLOCK;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ABLOCKSTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getABLOCKSType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ABLOCK> getABLOCK() {
		if (aBLOCK == null) {
			aBLOCK = new EObjectContainmentEList<ABLOCK>(ABLOCK.class, this, OdxXhtmlPackage.ABLOCKS_TYPE__ABLOCK);
		}
		return aBLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCKS_TYPE__ABLOCK:
				return ((InternalEList<?>)getABLOCK()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCKS_TYPE__ABLOCK:
				return getABLOCK();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCKS_TYPE__ABLOCK:
				getABLOCK().clear();
				getABLOCK().addAll((Collection<? extends ABLOCK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCKS_TYPE__ABLOCK:
				getABLOCK().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCKS_TYPE__ABLOCK:
				return aBLOCK != null && !aBLOCK.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ABLOCKSTypeImpl
