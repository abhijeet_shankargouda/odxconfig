/**
 */
package OdxXhtml.impl;

import OdxXhtml.FUNCTCLASSREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FUNCTCLASSREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FUNCTCLASSREFSImpl#getFUNCTCLASSREF <em>FUNCTCLASSREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FUNCTCLASSREFSImpl extends MinimalEObjectImpl.Container implements FUNCTCLASSREFS {
	/**
	 * The cached value of the '{@link #getFUNCTCLASSREF() <em>FUNCTCLASSREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFUNCTCLASSREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> fUNCTCLASSREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FUNCTCLASSREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFUNCTCLASSREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getFUNCTCLASSREF() {
		if (fUNCTCLASSREF == null) {
			fUNCTCLASSREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.FUNCTCLASSREFS__FUNCTCLASSREF);
		}
		return fUNCTCLASSREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSREFS__FUNCTCLASSREF:
				return ((InternalEList<?>)getFUNCTCLASSREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSREFS__FUNCTCLASSREF:
				return getFUNCTCLASSREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSREFS__FUNCTCLASSREF:
				getFUNCTCLASSREF().clear();
				getFUNCTCLASSREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSREFS__FUNCTCLASSREF:
				getFUNCTCLASSREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSREFS__FUNCTCLASSREF:
				return fUNCTCLASSREF != null && !fUNCTCLASSREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FUNCTCLASSREFSImpl
