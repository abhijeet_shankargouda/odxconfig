/**
 */
package OdxXhtml.impl;

import OdxXhtml.DIAGVARIABLES;
import OdxXhtml.ECUSHAREDDATA;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.VARIABLEGROUPS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUSHAREDDATA</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ECUSHAREDDATAImpl#getDIAGVARIABLES <em>DIAGVARIABLES</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUSHAREDDATAImpl#getVARIABLEGROUPS <em>VARIABLEGROUPS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECUSHAREDDATAImpl extends DIAGLAYERImpl implements ECUSHAREDDATA {
	/**
	 * The cached value of the '{@link #getDIAGVARIABLES() <em>DIAGVARIABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGVARIABLES()
	 * @generated
	 * @ordered
	 */
	protected DIAGVARIABLES dIAGVARIABLES;

	/**
	 * The cached value of the '{@link #getVARIABLEGROUPS() <em>VARIABLEGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVARIABLEGROUPS()
	 * @generated
	 * @ordered
	 */
	protected VARIABLEGROUPS vARIABLEGROUPS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUSHAREDDATAImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUSHAREDDATA();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGVARIABLES getDIAGVARIABLES() {
		return dIAGVARIABLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGVARIABLES(DIAGVARIABLES newDIAGVARIABLES, NotificationChain msgs) {
		DIAGVARIABLES oldDIAGVARIABLES = dIAGVARIABLES;
		dIAGVARIABLES = newDIAGVARIABLES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUSHAREDDATA__DIAGVARIABLES, oldDIAGVARIABLES, newDIAGVARIABLES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGVARIABLES(DIAGVARIABLES newDIAGVARIABLES) {
		if (newDIAGVARIABLES != dIAGVARIABLES) {
			NotificationChain msgs = null;
			if (dIAGVARIABLES != null)
				msgs = ((InternalEObject)dIAGVARIABLES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUSHAREDDATA__DIAGVARIABLES, null, msgs);
			if (newDIAGVARIABLES != null)
				msgs = ((InternalEObject)newDIAGVARIABLES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUSHAREDDATA__DIAGVARIABLES, null, msgs);
			msgs = basicSetDIAGVARIABLES(newDIAGVARIABLES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUSHAREDDATA__DIAGVARIABLES, newDIAGVARIABLES, newDIAGVARIABLES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VARIABLEGROUPS getVARIABLEGROUPS() {
		return vARIABLEGROUPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVARIABLEGROUPS(VARIABLEGROUPS newVARIABLEGROUPS, NotificationChain msgs) {
		VARIABLEGROUPS oldVARIABLEGROUPS = vARIABLEGROUPS;
		vARIABLEGROUPS = newVARIABLEGROUPS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUSHAREDDATA__VARIABLEGROUPS, oldVARIABLEGROUPS, newVARIABLEGROUPS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVARIABLEGROUPS(VARIABLEGROUPS newVARIABLEGROUPS) {
		if (newVARIABLEGROUPS != vARIABLEGROUPS) {
			NotificationChain msgs = null;
			if (vARIABLEGROUPS != null)
				msgs = ((InternalEObject)vARIABLEGROUPS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUSHAREDDATA__VARIABLEGROUPS, null, msgs);
			if (newVARIABLEGROUPS != null)
				msgs = ((InternalEObject)newVARIABLEGROUPS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUSHAREDDATA__VARIABLEGROUPS, null, msgs);
			msgs = basicSetVARIABLEGROUPS(newVARIABLEGROUPS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUSHAREDDATA__VARIABLEGROUPS, newVARIABLEGROUPS, newVARIABLEGROUPS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATA__DIAGVARIABLES:
				return basicSetDIAGVARIABLES(null, msgs);
			case OdxXhtmlPackage.ECUSHAREDDATA__VARIABLEGROUPS:
				return basicSetVARIABLEGROUPS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATA__DIAGVARIABLES:
				return getDIAGVARIABLES();
			case OdxXhtmlPackage.ECUSHAREDDATA__VARIABLEGROUPS:
				return getVARIABLEGROUPS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATA__DIAGVARIABLES:
				setDIAGVARIABLES((DIAGVARIABLES)newValue);
				return;
			case OdxXhtmlPackage.ECUSHAREDDATA__VARIABLEGROUPS:
				setVARIABLEGROUPS((VARIABLEGROUPS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATA__DIAGVARIABLES:
				setDIAGVARIABLES((DIAGVARIABLES)null);
				return;
			case OdxXhtmlPackage.ECUSHAREDDATA__VARIABLEGROUPS:
				setVARIABLEGROUPS((VARIABLEGROUPS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATA__DIAGVARIABLES:
				return dIAGVARIABLES != null;
			case OdxXhtmlPackage.ECUSHAREDDATA__VARIABLEGROUPS:
				return vARIABLEGROUPS != null;
		}
		return super.eIsSet(featureID);
	}

} //ECUSHAREDDATAImpl
