/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.ECUMEMCONNECTOR;
import OdxXhtml.FLASHCLASSS;
import OdxXhtml.IDENTDESCS;
import OdxXhtml.LAYERREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SESSIONDESCS;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUMEMCONNECTOR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getFLASHCLASSS <em>FLASHCLASSS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getSESSIONDESCS <em>SESSIONDESCS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getIDENTDESCS <em>IDENTDESCS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getECUMEMREF <em>ECUMEMREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getLAYERREFS <em>LAYERREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECUMEMCONNECTORImpl extends MinimalEObjectImpl.Container implements ECUMEMCONNECTOR {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getFLASHCLASSS() <em>FLASHCLASSS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFLASHCLASSS()
	 * @generated
	 * @ordered
	 */
	protected FLASHCLASSS fLASHCLASSS;

	/**
	 * The cached value of the '{@link #getSESSIONDESCS() <em>SESSIONDESCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSESSIONDESCS()
	 * @generated
	 * @ordered
	 */
	protected SESSIONDESCS sESSIONDESCS;

	/**
	 * The cached value of the '{@link #getIDENTDESCS() <em>IDENTDESCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDENTDESCS()
	 * @generated
	 * @ordered
	 */
	protected IDENTDESCS iDENTDESCS;

	/**
	 * The cached value of the '{@link #getECUMEMREF() <em>ECUMEMREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUMEMREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK eCUMEMREF;

	/**
	 * The cached value of the '{@link #getLAYERREFS() <em>LAYERREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLAYERREFS()
	 * @generated
	 * @ordered
	 */
	protected LAYERREFS lAYERREFS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUMEMCONNECTORImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUMEMCONNECTOR();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASHCLASSS getFLASHCLASSS() {
		return fLASHCLASSS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFLASHCLASSS(FLASHCLASSS newFLASHCLASSS, NotificationChain msgs) {
		FLASHCLASSS oldFLASHCLASSS = fLASHCLASSS;
		fLASHCLASSS = newFLASHCLASSS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__FLASHCLASSS, oldFLASHCLASSS, newFLASHCLASSS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFLASHCLASSS(FLASHCLASSS newFLASHCLASSS) {
		if (newFLASHCLASSS != fLASHCLASSS) {
			NotificationChain msgs = null;
			if (fLASHCLASSS != null)
				msgs = ((InternalEObject)fLASHCLASSS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__FLASHCLASSS, null, msgs);
			if (newFLASHCLASSS != null)
				msgs = ((InternalEObject)newFLASHCLASSS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__FLASHCLASSS, null, msgs);
			msgs = basicSetFLASHCLASSS(newFLASHCLASSS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__FLASHCLASSS, newFLASHCLASSS, newFLASHCLASSS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SESSIONDESCS getSESSIONDESCS() {
		return sESSIONDESCS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSESSIONDESCS(SESSIONDESCS newSESSIONDESCS, NotificationChain msgs) {
		SESSIONDESCS oldSESSIONDESCS = sESSIONDESCS;
		sESSIONDESCS = newSESSIONDESCS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__SESSIONDESCS, oldSESSIONDESCS, newSESSIONDESCS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSESSIONDESCS(SESSIONDESCS newSESSIONDESCS) {
		if (newSESSIONDESCS != sESSIONDESCS) {
			NotificationChain msgs = null;
			if (sESSIONDESCS != null)
				msgs = ((InternalEObject)sESSIONDESCS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__SESSIONDESCS, null, msgs);
			if (newSESSIONDESCS != null)
				msgs = ((InternalEObject)newSESSIONDESCS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__SESSIONDESCS, null, msgs);
			msgs = basicSetSESSIONDESCS(newSESSIONDESCS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__SESSIONDESCS, newSESSIONDESCS, newSESSIONDESCS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDENTDESCS getIDENTDESCS() {
		return iDENTDESCS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIDENTDESCS(IDENTDESCS newIDENTDESCS, NotificationChain msgs) {
		IDENTDESCS oldIDENTDESCS = iDENTDESCS;
		iDENTDESCS = newIDENTDESCS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__IDENTDESCS, oldIDENTDESCS, newIDENTDESCS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIDENTDESCS(IDENTDESCS newIDENTDESCS) {
		if (newIDENTDESCS != iDENTDESCS) {
			NotificationChain msgs = null;
			if (iDENTDESCS != null)
				msgs = ((InternalEObject)iDENTDESCS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__IDENTDESCS, null, msgs);
			if (newIDENTDESCS != null)
				msgs = ((InternalEObject)newIDENTDESCS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__IDENTDESCS, null, msgs);
			msgs = basicSetIDENTDESCS(newIDENTDESCS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__IDENTDESCS, newIDENTDESCS, newIDENTDESCS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getECUMEMREF() {
		return eCUMEMREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetECUMEMREF(ODXLINK newECUMEMREF, NotificationChain msgs) {
		ODXLINK oldECUMEMREF = eCUMEMREF;
		eCUMEMREF = newECUMEMREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__ECUMEMREF, oldECUMEMREF, newECUMEMREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setECUMEMREF(ODXLINK newECUMEMREF) {
		if (newECUMEMREF != eCUMEMREF) {
			NotificationChain msgs = null;
			if (eCUMEMREF != null)
				msgs = ((InternalEObject)eCUMEMREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__ECUMEMREF, null, msgs);
			if (newECUMEMREF != null)
				msgs = ((InternalEObject)newECUMEMREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__ECUMEMREF, null, msgs);
			msgs = basicSetECUMEMREF(newECUMEMREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__ECUMEMREF, newECUMEMREF, newECUMEMREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LAYERREFS getLAYERREFS() {
		return lAYERREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLAYERREFS(LAYERREFS newLAYERREFS, NotificationChain msgs) {
		LAYERREFS oldLAYERREFS = lAYERREFS;
		lAYERREFS = newLAYERREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__LAYERREFS, oldLAYERREFS, newLAYERREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLAYERREFS(LAYERREFS newLAYERREFS) {
		if (newLAYERREFS != lAYERREFS) {
			NotificationChain msgs = null;
			if (lAYERREFS != null)
				msgs = ((InternalEObject)lAYERREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__LAYERREFS, null, msgs);
			if (newLAYERREFS != null)
				msgs = ((InternalEObject)newLAYERREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUMEMCONNECTOR__LAYERREFS, null, msgs);
			msgs = basicSetLAYERREFS(newLAYERREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__LAYERREFS, newLAYERREFS, newLAYERREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUMEMCONNECTOR__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.ECUMEMCONNECTOR__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.ECUMEMCONNECTOR__FLASHCLASSS:
				return basicSetFLASHCLASSS(null, msgs);
			case OdxXhtmlPackage.ECUMEMCONNECTOR__SESSIONDESCS:
				return basicSetSESSIONDESCS(null, msgs);
			case OdxXhtmlPackage.ECUMEMCONNECTOR__IDENTDESCS:
				return basicSetIDENTDESCS(null, msgs);
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ECUMEMREF:
				return basicSetECUMEMREF(null, msgs);
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LAYERREFS:
				return basicSetLAYERREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTOR__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__DESC:
				return getDESC();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__FLASHCLASSS:
				return getFLASHCLASSS();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__SESSIONDESCS:
				return getSESSIONDESCS();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__IDENTDESCS:
				return getIDENTDESCS();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ECUMEMREF:
				return getECUMEMREF();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LAYERREFS:
				return getLAYERREFS();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ID:
				return getID();
			case OdxXhtmlPackage.ECUMEMCONNECTOR__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTOR__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__FLASHCLASSS:
				setFLASHCLASSS((FLASHCLASSS)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__SESSIONDESCS:
				setSESSIONDESCS((SESSIONDESCS)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__IDENTDESCS:
				setIDENTDESCS((IDENTDESCS)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ECUMEMREF:
				setECUMEMREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LAYERREFS:
				setLAYERREFS((LAYERREFS)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTOR__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__FLASHCLASSS:
				setFLASHCLASSS((FLASHCLASSS)null);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__SESSIONDESCS:
				setSESSIONDESCS((SESSIONDESCS)null);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__IDENTDESCS:
				setIDENTDESCS((IDENTDESCS)null);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ECUMEMREF:
				setECUMEMREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LAYERREFS:
				setLAYERREFS((LAYERREFS)null);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTOR__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__DESC:
				return dESC != null;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__FLASHCLASSS:
				return fLASHCLASSS != null;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__SESSIONDESCS:
				return sESSIONDESCS != null;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__IDENTDESCS:
				return iDENTDESCS != null;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ECUMEMREF:
				return eCUMEMREF != null;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__LAYERREFS:
				return lAYERREFS != null;
			case OdxXhtmlPackage.ECUMEMCONNECTOR__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.ECUMEMCONNECTOR__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //ECUMEMCONNECTORImpl
