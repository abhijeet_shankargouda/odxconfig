/**
 */
package OdxXhtml.impl;

import OdxXhtml.NEGRESPONSE;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NEGRESPONSE</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NEGRESPONSEImpl extends RESPONSEImpl implements NEGRESPONSE {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NEGRESPONSEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getNEGRESPONSE();
	}

} //NEGRESPONSEImpl
