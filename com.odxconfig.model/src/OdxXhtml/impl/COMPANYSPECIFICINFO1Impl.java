/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYSPECIFICINFO1;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.RELATEDDOCS1;
import OdxXhtml.SDGS1;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYSPECIFICINFO1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYSPECIFICINFO1Impl#getRELATEDDOCS <em>RELATEDDOCS</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYSPECIFICINFO1Impl#getSDGS <em>SDGS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYSPECIFICINFO1Impl extends MinimalEObjectImpl.Container implements COMPANYSPECIFICINFO1 {
	/**
	 * The cached value of the '{@link #getRELATEDDOCS() <em>RELATEDDOCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRELATEDDOCS()
	 * @generated
	 * @ordered
	 */
	protected RELATEDDOCS1 rELATEDDOCS;

	/**
	 * The cached value of the '{@link #getSDGS() <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGS()
	 * @generated
	 * @ordered
	 */
	protected SDGS1 sDGS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYSPECIFICINFO1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYSPECIFICINFO1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RELATEDDOCS1 getRELATEDDOCS() {
		return rELATEDDOCS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRELATEDDOCS(RELATEDDOCS1 newRELATEDDOCS, NotificationChain msgs) {
		RELATEDDOCS1 oldRELATEDDOCS = rELATEDDOCS;
		rELATEDDOCS = newRELATEDDOCS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYSPECIFICINFO1__RELATEDDOCS, oldRELATEDDOCS, newRELATEDDOCS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRELATEDDOCS(RELATEDDOCS1 newRELATEDDOCS) {
		if (newRELATEDDOCS != rELATEDDOCS) {
			NotificationChain msgs = null;
			if (rELATEDDOCS != null)
				msgs = ((InternalEObject)rELATEDDOCS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYSPECIFICINFO1__RELATEDDOCS, null, msgs);
			if (newRELATEDDOCS != null)
				msgs = ((InternalEObject)newRELATEDDOCS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYSPECIFICINFO1__RELATEDDOCS, null, msgs);
			msgs = basicSetRELATEDDOCS(newRELATEDDOCS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYSPECIFICINFO1__RELATEDDOCS, newRELATEDDOCS, newRELATEDDOCS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS1 getSDGS() {
		return sDGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGS(SDGS1 newSDGS, NotificationChain msgs) {
		SDGS1 oldSDGS = sDGS;
		sDGS = newSDGS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYSPECIFICINFO1__SDGS, oldSDGS, newSDGS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGS(SDGS1 newSDGS) {
		if (newSDGS != sDGS) {
			NotificationChain msgs = null;
			if (sDGS != null)
				msgs = ((InternalEObject)sDGS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYSPECIFICINFO1__SDGS, null, msgs);
			if (newSDGS != null)
				msgs = ((InternalEObject)newSDGS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYSPECIFICINFO1__SDGS, null, msgs);
			msgs = basicSetSDGS(newSDGS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYSPECIFICINFO1__SDGS, newSDGS, newSDGS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__RELATEDDOCS:
				return basicSetRELATEDDOCS(null, msgs);
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__SDGS:
				return basicSetSDGS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__RELATEDDOCS:
				return getRELATEDDOCS();
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__SDGS:
				return getSDGS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__RELATEDDOCS:
				setRELATEDDOCS((RELATEDDOCS1)newValue);
				return;
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__SDGS:
				setSDGS((SDGS1)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__RELATEDDOCS:
				setRELATEDDOCS((RELATEDDOCS1)null);
				return;
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__SDGS:
				setSDGS((SDGS1)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__RELATEDDOCS:
				return rELATEDDOCS != null;
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1__SDGS:
				return sDGS != null;
		}
		return super.eIsSet(featureID);
	}

} //COMPANYSPECIFICINFO1Impl
