/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SIZEDEFFILTER;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SIZEDEFFILTER</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SIZEDEFFILTERImpl#getFILTERSIZE <em>FILTERSIZE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SIZEDEFFILTERImpl extends FILTERImpl implements SIZEDEFFILTER {
	/**
	 * The default value of the '{@link #getFILTERSIZE() <em>FILTERSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILTERSIZE()
	 * @generated
	 * @ordered
	 */
	protected static final long FILTERSIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getFILTERSIZE() <em>FILTERSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILTERSIZE()
	 * @generated
	 * @ordered
	 */
	protected long fILTERSIZE = FILTERSIZE_EDEFAULT;

	/**
	 * This is true if the FILTERSIZE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fILTERSIZEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SIZEDEFFILTERImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSIZEDEFFILTER();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getFILTERSIZE() {
		return fILTERSIZE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFILTERSIZE(long newFILTERSIZE) {
		long oldFILTERSIZE = fILTERSIZE;
		fILTERSIZE = newFILTERSIZE;
		boolean oldFILTERSIZEESet = fILTERSIZEESet;
		fILTERSIZEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SIZEDEFFILTER__FILTERSIZE, oldFILTERSIZE, fILTERSIZE, !oldFILTERSIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetFILTERSIZE() {
		long oldFILTERSIZE = fILTERSIZE;
		boolean oldFILTERSIZEESet = fILTERSIZEESet;
		fILTERSIZE = FILTERSIZE_EDEFAULT;
		fILTERSIZEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.SIZEDEFFILTER__FILTERSIZE, oldFILTERSIZE, FILTERSIZE_EDEFAULT, oldFILTERSIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetFILTERSIZE() {
		return fILTERSIZEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SIZEDEFFILTER__FILTERSIZE:
				return getFILTERSIZE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SIZEDEFFILTER__FILTERSIZE:
				setFILTERSIZE((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SIZEDEFFILTER__FILTERSIZE:
				unsetFILTERSIZE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SIZEDEFFILTER__FILTERSIZE:
				return isSetFILTERSIZE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (fILTERSIZE: ");
		if (fILTERSIZEESet) result.append(fILTERSIZE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //SIZEDEFFILTERImpl
