/**
 */
package OdxXhtml.impl;

import OdxXhtml.BType;
import OdxXhtml.Flow;
import OdxXhtml.IType;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.OlType;
import OdxXhtml.P;
import OdxXhtml.SubType;
import OdxXhtml.SupType;
import OdxXhtml.UlType;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flow</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getP <em>P</em>}</li>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getUl <em>Ul</em>}</li>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getOl <em>Ol</em>}</li>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getBr <em>Br</em>}</li>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getI <em>I</em>}</li>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getB <em>B</em>}</li>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getSub <em>Sub</em>}</li>
 *   <li>{@link OdxXhtml.impl.FlowImpl#getSup <em>Sup</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FlowImpl extends MinimalEObjectImpl.Container implements Flow {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FlowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFlow();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, OdxXhtmlPackage.FLOW__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getGroup() {
		return (FeatureMap)getMixed().<FeatureMap.Entry>list(OdxXhtmlPackage.eINSTANCE.getFlow_Group());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<P> getP() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getFlow_P());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<UlType> getUl() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getFlow_Ul());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<OlType> getOl() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getFlow_Ol());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<EObject> getBr() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getFlow_Br());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IType> getI() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getFlow_I());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BType> getB() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getFlow_B());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SubType> getSub() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getFlow_Sub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SupType> getSup() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getFlow_Sup());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FLOW__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.FLOW__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.FLOW__P:
				return ((InternalEList<?>)getP()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.FLOW__UL:
				return ((InternalEList<?>)getUl()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.FLOW__OL:
				return ((InternalEList<?>)getOl()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.FLOW__BR:
				return ((InternalEList<?>)getBr()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.FLOW__I:
				return ((InternalEList<?>)getI()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.FLOW__B:
				return ((InternalEList<?>)getB()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.FLOW__SUB:
				return ((InternalEList<?>)getSub()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.FLOW__SUP:
				return ((InternalEList<?>)getSup()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FLOW__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case OdxXhtmlPackage.FLOW__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case OdxXhtmlPackage.FLOW__P:
				return getP();
			case OdxXhtmlPackage.FLOW__UL:
				return getUl();
			case OdxXhtmlPackage.FLOW__OL:
				return getOl();
			case OdxXhtmlPackage.FLOW__BR:
				return getBr();
			case OdxXhtmlPackage.FLOW__I:
				return getI();
			case OdxXhtmlPackage.FLOW__B:
				return getB();
			case OdxXhtmlPackage.FLOW__SUB:
				return getSub();
			case OdxXhtmlPackage.FLOW__SUP:
				return getSup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FLOW__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case OdxXhtmlPackage.FLOW__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case OdxXhtmlPackage.FLOW__P:
				getP().clear();
				getP().addAll((Collection<? extends P>)newValue);
				return;
			case OdxXhtmlPackage.FLOW__UL:
				getUl().clear();
				getUl().addAll((Collection<? extends UlType>)newValue);
				return;
			case OdxXhtmlPackage.FLOW__OL:
				getOl().clear();
				getOl().addAll((Collection<? extends OlType>)newValue);
				return;
			case OdxXhtmlPackage.FLOW__BR:
				getBr().clear();
				getBr().addAll((Collection<? extends EObject>)newValue);
				return;
			case OdxXhtmlPackage.FLOW__I:
				getI().clear();
				getI().addAll((Collection<? extends IType>)newValue);
				return;
			case OdxXhtmlPackage.FLOW__B:
				getB().clear();
				getB().addAll((Collection<? extends BType>)newValue);
				return;
			case OdxXhtmlPackage.FLOW__SUB:
				getSub().clear();
				getSub().addAll((Collection<? extends SubType>)newValue);
				return;
			case OdxXhtmlPackage.FLOW__SUP:
				getSup().clear();
				getSup().addAll((Collection<? extends SupType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLOW__MIXED:
				getMixed().clear();
				return;
			case OdxXhtmlPackage.FLOW__GROUP:
				getGroup().clear();
				return;
			case OdxXhtmlPackage.FLOW__P:
				getP().clear();
				return;
			case OdxXhtmlPackage.FLOW__UL:
				getUl().clear();
				return;
			case OdxXhtmlPackage.FLOW__OL:
				getOl().clear();
				return;
			case OdxXhtmlPackage.FLOW__BR:
				getBr().clear();
				return;
			case OdxXhtmlPackage.FLOW__I:
				getI().clear();
				return;
			case OdxXhtmlPackage.FLOW__B:
				getB().clear();
				return;
			case OdxXhtmlPackage.FLOW__SUB:
				getSub().clear();
				return;
			case OdxXhtmlPackage.FLOW__SUP:
				getSup().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLOW__MIXED:
				return mixed != null && !mixed.isEmpty();
			case OdxXhtmlPackage.FLOW__GROUP:
				return !getGroup().isEmpty();
			case OdxXhtmlPackage.FLOW__P:
				return !getP().isEmpty();
			case OdxXhtmlPackage.FLOW__UL:
				return !getUl().isEmpty();
			case OdxXhtmlPackage.FLOW__OL:
				return !getOl().isEmpty();
			case OdxXhtmlPackage.FLOW__BR:
				return !getBr().isEmpty();
			case OdxXhtmlPackage.FLOW__I:
				return !getI().isEmpty();
			case OdxXhtmlPackage.FLOW__B:
				return !getB().isEmpty();
			case OdxXhtmlPackage.FLOW__SUB:
				return !getSub().isEmpty();
			case OdxXhtmlPackage.FLOW__SUP:
				return !getSup().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //FlowImpl
