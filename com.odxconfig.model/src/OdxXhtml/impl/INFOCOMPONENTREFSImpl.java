/**
 */
package OdxXhtml.impl;

import OdxXhtml.INFOCOMPONENTREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>INFOCOMPONENTREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.INFOCOMPONENTREFSImpl#getINFOCOMPONENTREF <em>INFOCOMPONENTREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class INFOCOMPONENTREFSImpl extends MinimalEObjectImpl.Container implements INFOCOMPONENTREFS {
	/**
	 * The cached value of the '{@link #getINFOCOMPONENTREF() <em>INFOCOMPONENTREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINFOCOMPONENTREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> iNFOCOMPONENTREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INFOCOMPONENTREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getINFOCOMPONENTREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getINFOCOMPONENTREF() {
		if (iNFOCOMPONENTREF == null) {
			iNFOCOMPONENTREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.INFOCOMPONENTREFS__INFOCOMPONENTREF);
		}
		return iNFOCOMPONENTREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTREFS__INFOCOMPONENTREF:
				return ((InternalEList<?>)getINFOCOMPONENTREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTREFS__INFOCOMPONENTREF:
				return getINFOCOMPONENTREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTREFS__INFOCOMPONENTREF:
				getINFOCOMPONENTREF().clear();
				getINFOCOMPONENTREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTREFS__INFOCOMPONENTREF:
				getINFOCOMPONENTREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTREFS__INFOCOMPONENTREF:
				return iNFOCOMPONENTREF != null && !iNFOCOMPONENTREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //INFOCOMPONENTREFSImpl
