/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPARAMSPEC;
import OdxXhtml.DIAGLAYERCONTAINER;
import OdxXhtml.FLASH;
import OdxXhtml.MULTIPLEECUJOBSPEC;
import OdxXhtml.ODX;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.VEHICLEINFOSPEC;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ODX</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ODXImpl#getDIAGLAYERCONTAINER <em>DIAGLAYERCONTAINER</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXImpl#getCOMPARAMSPEC <em>COMPARAMSPEC</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXImpl#getVEHICLEINFOSPEC <em>VEHICLEINFOSPEC</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXImpl#getFLASH <em>FLASH</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXImpl#getMULTIPLEECUJOBSPEC <em>MULTIPLEECUJOBSPEC</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXImpl#getMODELVERSION <em>MODELVERSION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ODXImpl extends MinimalEObjectImpl.Container implements ODX {
	/**
	 * The cached value of the '{@link #getDIAGLAYERCONTAINER() <em>DIAGLAYERCONTAINER</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGLAYERCONTAINER()
	 * @generated
	 * @ordered
	 */
	protected DIAGLAYERCONTAINER dIAGLAYERCONTAINER;

	/**
	 * The cached value of the '{@link #getCOMPARAMSPEC() <em>COMPARAMSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPARAMSPEC()
	 * @generated
	 * @ordered
	 */
	protected COMPARAMSPEC cOMPARAMSPEC;

	/**
	 * The cached value of the '{@link #getVEHICLEINFOSPEC() <em>VEHICLEINFOSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVEHICLEINFOSPEC()
	 * @generated
	 * @ordered
	 */
	protected VEHICLEINFOSPEC vEHICLEINFOSPEC;

	/**
	 * The cached value of the '{@link #getFLASH() <em>FLASH</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFLASH()
	 * @generated
	 * @ordered
	 */
	protected FLASH fLASH;

	/**
	 * The cached value of the '{@link #getMULTIPLEECUJOBSPEC() <em>MULTIPLEECUJOBSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMULTIPLEECUJOBSPEC()
	 * @generated
	 * @ordered
	 */
	protected MULTIPLEECUJOBSPEC mULTIPLEECUJOBSPEC;

	/**
	 * The default value of the '{@link #getMODELVERSION() <em>MODELVERSION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMODELVERSION()
	 * @generated
	 * @ordered
	 */
	protected static final String MODELVERSION_EDEFAULT = "2.0.1";

	/**
	 * The cached value of the '{@link #getMODELVERSION() <em>MODELVERSION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMODELVERSION()
	 * @generated
	 * @ordered
	 */
	protected String mODELVERSION = MODELVERSION_EDEFAULT;

	/**
	 * This is true if the MODELVERSION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mODELVERSIONESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ODXImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getODX();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGLAYERCONTAINER getDIAGLAYERCONTAINER() {
		return dIAGLAYERCONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGLAYERCONTAINER(DIAGLAYERCONTAINER newDIAGLAYERCONTAINER, NotificationChain msgs) {
		DIAGLAYERCONTAINER oldDIAGLAYERCONTAINER = dIAGLAYERCONTAINER;
		dIAGLAYERCONTAINER = newDIAGLAYERCONTAINER;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__DIAGLAYERCONTAINER, oldDIAGLAYERCONTAINER, newDIAGLAYERCONTAINER);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGLAYERCONTAINER(DIAGLAYERCONTAINER newDIAGLAYERCONTAINER) {
		if (newDIAGLAYERCONTAINER != dIAGLAYERCONTAINER) {
			NotificationChain msgs = null;
			if (dIAGLAYERCONTAINER != null)
				msgs = ((InternalEObject)dIAGLAYERCONTAINER).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__DIAGLAYERCONTAINER, null, msgs);
			if (newDIAGLAYERCONTAINER != null)
				msgs = ((InternalEObject)newDIAGLAYERCONTAINER).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__DIAGLAYERCONTAINER, null, msgs);
			msgs = basicSetDIAGLAYERCONTAINER(newDIAGLAYERCONTAINER, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__DIAGLAYERCONTAINER, newDIAGLAYERCONTAINER, newDIAGLAYERCONTAINER));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPARAMSPEC getCOMPARAMSPEC() {
		return cOMPARAMSPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPARAMSPEC(COMPARAMSPEC newCOMPARAMSPEC, NotificationChain msgs) {
		COMPARAMSPEC oldCOMPARAMSPEC = cOMPARAMSPEC;
		cOMPARAMSPEC = newCOMPARAMSPEC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__COMPARAMSPEC, oldCOMPARAMSPEC, newCOMPARAMSPEC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPARAMSPEC(COMPARAMSPEC newCOMPARAMSPEC) {
		if (newCOMPARAMSPEC != cOMPARAMSPEC) {
			NotificationChain msgs = null;
			if (cOMPARAMSPEC != null)
				msgs = ((InternalEObject)cOMPARAMSPEC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__COMPARAMSPEC, null, msgs);
			if (newCOMPARAMSPEC != null)
				msgs = ((InternalEObject)newCOMPARAMSPEC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__COMPARAMSPEC, null, msgs);
			msgs = basicSetCOMPARAMSPEC(newCOMPARAMSPEC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__COMPARAMSPEC, newCOMPARAMSPEC, newCOMPARAMSPEC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLEINFOSPEC getVEHICLEINFOSPEC() {
		return vEHICLEINFOSPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVEHICLEINFOSPEC(VEHICLEINFOSPEC newVEHICLEINFOSPEC, NotificationChain msgs) {
		VEHICLEINFOSPEC oldVEHICLEINFOSPEC = vEHICLEINFOSPEC;
		vEHICLEINFOSPEC = newVEHICLEINFOSPEC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__VEHICLEINFOSPEC, oldVEHICLEINFOSPEC, newVEHICLEINFOSPEC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVEHICLEINFOSPEC(VEHICLEINFOSPEC newVEHICLEINFOSPEC) {
		if (newVEHICLEINFOSPEC != vEHICLEINFOSPEC) {
			NotificationChain msgs = null;
			if (vEHICLEINFOSPEC != null)
				msgs = ((InternalEObject)vEHICLEINFOSPEC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__VEHICLEINFOSPEC, null, msgs);
			if (newVEHICLEINFOSPEC != null)
				msgs = ((InternalEObject)newVEHICLEINFOSPEC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__VEHICLEINFOSPEC, null, msgs);
			msgs = basicSetVEHICLEINFOSPEC(newVEHICLEINFOSPEC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__VEHICLEINFOSPEC, newVEHICLEINFOSPEC, newVEHICLEINFOSPEC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASH getFLASH() {
		return fLASH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFLASH(FLASH newFLASH, NotificationChain msgs) {
		FLASH oldFLASH = fLASH;
		fLASH = newFLASH;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__FLASH, oldFLASH, newFLASH);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFLASH(FLASH newFLASH) {
		if (newFLASH != fLASH) {
			NotificationChain msgs = null;
			if (fLASH != null)
				msgs = ((InternalEObject)fLASH).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__FLASH, null, msgs);
			if (newFLASH != null)
				msgs = ((InternalEObject)newFLASH).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__FLASH, null, msgs);
			msgs = basicSetFLASH(newFLASH, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__FLASH, newFLASH, newFLASH));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MULTIPLEECUJOBSPEC getMULTIPLEECUJOBSPEC() {
		return mULTIPLEECUJOBSPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMULTIPLEECUJOBSPEC(MULTIPLEECUJOBSPEC newMULTIPLEECUJOBSPEC, NotificationChain msgs) {
		MULTIPLEECUJOBSPEC oldMULTIPLEECUJOBSPEC = mULTIPLEECUJOBSPEC;
		mULTIPLEECUJOBSPEC = newMULTIPLEECUJOBSPEC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__MULTIPLEECUJOBSPEC, oldMULTIPLEECUJOBSPEC, newMULTIPLEECUJOBSPEC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMULTIPLEECUJOBSPEC(MULTIPLEECUJOBSPEC newMULTIPLEECUJOBSPEC) {
		if (newMULTIPLEECUJOBSPEC != mULTIPLEECUJOBSPEC) {
			NotificationChain msgs = null;
			if (mULTIPLEECUJOBSPEC != null)
				msgs = ((InternalEObject)mULTIPLEECUJOBSPEC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__MULTIPLEECUJOBSPEC, null, msgs);
			if (newMULTIPLEECUJOBSPEC != null)
				msgs = ((InternalEObject)newMULTIPLEECUJOBSPEC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODX__MULTIPLEECUJOBSPEC, null, msgs);
			msgs = basicSetMULTIPLEECUJOBSPEC(newMULTIPLEECUJOBSPEC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__MULTIPLEECUJOBSPEC, newMULTIPLEECUJOBSPEC, newMULTIPLEECUJOBSPEC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMODELVERSION() {
		return mODELVERSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMODELVERSION(String newMODELVERSION) {
		String oldMODELVERSION = mODELVERSION;
		mODELVERSION = newMODELVERSION;
		boolean oldMODELVERSIONESet = mODELVERSIONESet;
		mODELVERSIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODX__MODELVERSION, oldMODELVERSION, mODELVERSION, !oldMODELVERSIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetMODELVERSION() {
		String oldMODELVERSION = mODELVERSION;
		boolean oldMODELVERSIONESet = mODELVERSIONESet;
		mODELVERSION = MODELVERSION_EDEFAULT;
		mODELVERSIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.ODX__MODELVERSION, oldMODELVERSION, MODELVERSION_EDEFAULT, oldMODELVERSIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetMODELVERSION() {
		return mODELVERSIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ODX__DIAGLAYERCONTAINER:
				return basicSetDIAGLAYERCONTAINER(null, msgs);
			case OdxXhtmlPackage.ODX__COMPARAMSPEC:
				return basicSetCOMPARAMSPEC(null, msgs);
			case OdxXhtmlPackage.ODX__VEHICLEINFOSPEC:
				return basicSetVEHICLEINFOSPEC(null, msgs);
			case OdxXhtmlPackage.ODX__FLASH:
				return basicSetFLASH(null, msgs);
			case OdxXhtmlPackage.ODX__MULTIPLEECUJOBSPEC:
				return basicSetMULTIPLEECUJOBSPEC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ODX__DIAGLAYERCONTAINER:
				return getDIAGLAYERCONTAINER();
			case OdxXhtmlPackage.ODX__COMPARAMSPEC:
				return getCOMPARAMSPEC();
			case OdxXhtmlPackage.ODX__VEHICLEINFOSPEC:
				return getVEHICLEINFOSPEC();
			case OdxXhtmlPackage.ODX__FLASH:
				return getFLASH();
			case OdxXhtmlPackage.ODX__MULTIPLEECUJOBSPEC:
				return getMULTIPLEECUJOBSPEC();
			case OdxXhtmlPackage.ODX__MODELVERSION:
				return getMODELVERSION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ODX__DIAGLAYERCONTAINER:
				setDIAGLAYERCONTAINER((DIAGLAYERCONTAINER)newValue);
				return;
			case OdxXhtmlPackage.ODX__COMPARAMSPEC:
				setCOMPARAMSPEC((COMPARAMSPEC)newValue);
				return;
			case OdxXhtmlPackage.ODX__VEHICLEINFOSPEC:
				setVEHICLEINFOSPEC((VEHICLEINFOSPEC)newValue);
				return;
			case OdxXhtmlPackage.ODX__FLASH:
				setFLASH((FLASH)newValue);
				return;
			case OdxXhtmlPackage.ODX__MULTIPLEECUJOBSPEC:
				setMULTIPLEECUJOBSPEC((MULTIPLEECUJOBSPEC)newValue);
				return;
			case OdxXhtmlPackage.ODX__MODELVERSION:
				setMODELVERSION((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ODX__DIAGLAYERCONTAINER:
				setDIAGLAYERCONTAINER((DIAGLAYERCONTAINER)null);
				return;
			case OdxXhtmlPackage.ODX__COMPARAMSPEC:
				setCOMPARAMSPEC((COMPARAMSPEC)null);
				return;
			case OdxXhtmlPackage.ODX__VEHICLEINFOSPEC:
				setVEHICLEINFOSPEC((VEHICLEINFOSPEC)null);
				return;
			case OdxXhtmlPackage.ODX__FLASH:
				setFLASH((FLASH)null);
				return;
			case OdxXhtmlPackage.ODX__MULTIPLEECUJOBSPEC:
				setMULTIPLEECUJOBSPEC((MULTIPLEECUJOBSPEC)null);
				return;
			case OdxXhtmlPackage.ODX__MODELVERSION:
				unsetMODELVERSION();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ODX__DIAGLAYERCONTAINER:
				return dIAGLAYERCONTAINER != null;
			case OdxXhtmlPackage.ODX__COMPARAMSPEC:
				return cOMPARAMSPEC != null;
			case OdxXhtmlPackage.ODX__VEHICLEINFOSPEC:
				return vEHICLEINFOSPEC != null;
			case OdxXhtmlPackage.ODX__FLASH:
				return fLASH != null;
			case OdxXhtmlPackage.ODX__MULTIPLEECUJOBSPEC:
				return mULTIPLEECUJOBSPEC != null;
			case OdxXhtmlPackage.ODX__MODELVERSION:
				return isSetMODELVERSION();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mODELVERSION: ");
		if (mODELVERSIONESet) result.append(mODELVERSION); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ODXImpl
