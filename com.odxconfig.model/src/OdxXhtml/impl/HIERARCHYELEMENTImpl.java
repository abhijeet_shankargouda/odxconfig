/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPARAMREFS;
import OdxXhtml.HIERARCHYELEMENT;
import OdxXhtml.IMPORTREFS;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HIERARCHYELEMENT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.HIERARCHYELEMENTImpl#getCOMPARAMREFS <em>COMPARAMREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.HIERARCHYELEMENTImpl#getIMPORTREFS <em>IMPORTREFS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HIERARCHYELEMENTImpl extends DIAGLAYERImpl implements HIERARCHYELEMENT {
	/**
	 * The cached value of the '{@link #getCOMPARAMREFS() <em>COMPARAMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPARAMREFS()
	 * @generated
	 * @ordered
	 */
	protected COMPARAMREFS cOMPARAMREFS;

	/**
	 * The cached value of the '{@link #getIMPORTREFS() <em>IMPORTREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIMPORTREFS()
	 * @generated
	 * @ordered
	 */
	protected IMPORTREFS iMPORTREFS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HIERARCHYELEMENTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getHIERARCHYELEMENT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPARAMREFS getCOMPARAMREFS() {
		return cOMPARAMREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPARAMREFS(COMPARAMREFS newCOMPARAMREFS, NotificationChain msgs) {
		COMPARAMREFS oldCOMPARAMREFS = cOMPARAMREFS;
		cOMPARAMREFS = newCOMPARAMREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.HIERARCHYELEMENT__COMPARAMREFS, oldCOMPARAMREFS, newCOMPARAMREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPARAMREFS(COMPARAMREFS newCOMPARAMREFS) {
		if (newCOMPARAMREFS != cOMPARAMREFS) {
			NotificationChain msgs = null;
			if (cOMPARAMREFS != null)
				msgs = ((InternalEObject)cOMPARAMREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.HIERARCHYELEMENT__COMPARAMREFS, null, msgs);
			if (newCOMPARAMREFS != null)
				msgs = ((InternalEObject)newCOMPARAMREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.HIERARCHYELEMENT__COMPARAMREFS, null, msgs);
			msgs = basicSetCOMPARAMREFS(newCOMPARAMREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.HIERARCHYELEMENT__COMPARAMREFS, newCOMPARAMREFS, newCOMPARAMREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IMPORTREFS getIMPORTREFS() {
		return iMPORTREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIMPORTREFS(IMPORTREFS newIMPORTREFS, NotificationChain msgs) {
		IMPORTREFS oldIMPORTREFS = iMPORTREFS;
		iMPORTREFS = newIMPORTREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.HIERARCHYELEMENT__IMPORTREFS, oldIMPORTREFS, newIMPORTREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIMPORTREFS(IMPORTREFS newIMPORTREFS) {
		if (newIMPORTREFS != iMPORTREFS) {
			NotificationChain msgs = null;
			if (iMPORTREFS != null)
				msgs = ((InternalEObject)iMPORTREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.HIERARCHYELEMENT__IMPORTREFS, null, msgs);
			if (newIMPORTREFS != null)
				msgs = ((InternalEObject)newIMPORTREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.HIERARCHYELEMENT__IMPORTREFS, null, msgs);
			msgs = basicSetIMPORTREFS(newIMPORTREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.HIERARCHYELEMENT__IMPORTREFS, newIMPORTREFS, newIMPORTREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.HIERARCHYELEMENT__COMPARAMREFS:
				return basicSetCOMPARAMREFS(null, msgs);
			case OdxXhtmlPackage.HIERARCHYELEMENT__IMPORTREFS:
				return basicSetIMPORTREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.HIERARCHYELEMENT__COMPARAMREFS:
				return getCOMPARAMREFS();
			case OdxXhtmlPackage.HIERARCHYELEMENT__IMPORTREFS:
				return getIMPORTREFS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.HIERARCHYELEMENT__COMPARAMREFS:
				setCOMPARAMREFS((COMPARAMREFS)newValue);
				return;
			case OdxXhtmlPackage.HIERARCHYELEMENT__IMPORTREFS:
				setIMPORTREFS((IMPORTREFS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.HIERARCHYELEMENT__COMPARAMREFS:
				setCOMPARAMREFS((COMPARAMREFS)null);
				return;
			case OdxXhtmlPackage.HIERARCHYELEMENT__IMPORTREFS:
				setIMPORTREFS((IMPORTREFS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.HIERARCHYELEMENT__COMPARAMREFS:
				return cOMPARAMREFS != null;
			case OdxXhtmlPackage.HIERARCHYELEMENT__IMPORTREFS:
				return iMPORTREFS != null;
		}
		return super.eIsSet(featureID);
	}

} //HIERARCHYELEMENTImpl
