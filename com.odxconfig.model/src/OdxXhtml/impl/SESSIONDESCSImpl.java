/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SESSIONDESC;
import OdxXhtml.SESSIONDESCS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SESSIONDESCS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCSImpl#getSESSIONDESC <em>SESSIONDESC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SESSIONDESCSImpl extends MinimalEObjectImpl.Container implements SESSIONDESCS {
	/**
	 * The cached value of the '{@link #getSESSIONDESC() <em>SESSIONDESC</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSESSIONDESC()
	 * @generated
	 * @ordered
	 */
	protected EList<SESSIONDESC> sESSIONDESC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SESSIONDESCSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSESSIONDESCS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SESSIONDESC> getSESSIONDESC() {
		if (sESSIONDESC == null) {
			sESSIONDESC = new EObjectContainmentEList<SESSIONDESC>(SESSIONDESC.class, this, OdxXhtmlPackage.SESSIONDESCS__SESSIONDESC);
		}
		return sESSIONDESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESCS__SESSIONDESC:
				return ((InternalEList<?>)getSESSIONDESC()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESCS__SESSIONDESC:
				return getSESSIONDESC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESCS__SESSIONDESC:
				getSESSIONDESC().clear();
				getSESSIONDESC().addAll((Collection<? extends SESSIONDESC>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESCS__SESSIONDESC:
				getSESSIONDESC().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESCS__SESSIONDESC:
				return sESSIONDESC != null && !sESSIONDESC.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SESSIONDESCSImpl
