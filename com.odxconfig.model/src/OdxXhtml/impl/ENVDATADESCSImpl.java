/**
 */
package OdxXhtml.impl;

import OdxXhtml.ENVDATADESC;
import OdxXhtml.ENVDATADESCS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ENVDATADESCS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ENVDATADESCSImpl#getENVDATADESC <em>ENVDATADESC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ENVDATADESCSImpl extends MinimalEObjectImpl.Container implements ENVDATADESCS {
	/**
	 * The cached value of the '{@link #getENVDATADESC() <em>ENVDATADESC</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENVDATADESC()
	 * @generated
	 * @ordered
	 */
	protected EList<ENVDATADESC> eNVDATADESC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ENVDATADESCSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getENVDATADESCS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ENVDATADESC> getENVDATADESC() {
		if (eNVDATADESC == null) {
			eNVDATADESC = new EObjectContainmentEList<ENVDATADESC>(ENVDATADESC.class, this, OdxXhtmlPackage.ENVDATADESCS__ENVDATADESC);
		}
		return eNVDATADESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESCS__ENVDATADESC:
				return ((InternalEList<?>)getENVDATADESC()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESCS__ENVDATADESC:
				return getENVDATADESC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESCS__ENVDATADESC:
				getENVDATADESC().clear();
				getENVDATADESC().addAll((Collection<? extends ENVDATADESC>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESCS__ENVDATADESC:
				getENVDATADESC().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENVDATADESCS__ENVDATADESC:
				return eNVDATADESC != null && !eNVDATADESC.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ENVDATADESCSImpl
