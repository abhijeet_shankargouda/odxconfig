/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.POSRESPONSE;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>POSRESPONSE</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class POSRESPONSEImpl extends RESPONSEImpl implements POSRESPONSE {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected POSRESPONSEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPOSRESPONSE();
	}

} //POSRESPONSEImpl
