/**
 */
package OdxXhtml.impl;

import OdxXhtml.CHECKSUM;
import OdxXhtml.CHECKSUMRESULT;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SOURCEENDADDRESS;
import OdxXhtml.TEXT;
import OdxXhtml.UNCOMPRESSEDSIZE;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CHECKSUM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getFILLBYTE <em>FILLBYTE</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getSOURCESTARTADDRESS <em>SOURCESTARTADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getCOMPRESSEDSIZE <em>COMPRESSEDSIZE</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getCHECKSUMALG <em>CHECKSUMALG</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getSOURCEENDADDRESS <em>SOURCEENDADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getUNCOMPRESSEDSIZE <em>UNCOMPRESSEDSIZE</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getCHECKSUMRESULT <em>CHECKSUMRESULT</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.CHECKSUMImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CHECKSUMImpl extends MinimalEObjectImpl.Container implements CHECKSUM {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getFILLBYTE() <em>FILLBYTE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILLBYTE()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] FILLBYTE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFILLBYTE() <em>FILLBYTE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILLBYTE()
	 * @generated
	 * @ordered
	 */
	protected byte[] fILLBYTE = FILLBYTE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSOURCESTARTADDRESS() <em>SOURCESTARTADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSOURCESTARTADDRESS()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] SOURCESTARTADDRESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSOURCESTARTADDRESS() <em>SOURCESTARTADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSOURCESTARTADDRESS()
	 * @generated
	 * @ordered
	 */
	protected byte[] sOURCESTARTADDRESS = SOURCESTARTADDRESS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCOMPRESSEDSIZE() <em>COMPRESSEDSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPRESSEDSIZE()
	 * @generated
	 * @ordered
	 */
	protected static final long COMPRESSEDSIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getCOMPRESSEDSIZE() <em>COMPRESSEDSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPRESSEDSIZE()
	 * @generated
	 * @ordered
	 */
	protected long cOMPRESSEDSIZE = COMPRESSEDSIZE_EDEFAULT;

	/**
	 * This is true if the COMPRESSEDSIZE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean cOMPRESSEDSIZEESet;

	/**
	 * The default value of the '{@link #getCHECKSUMALG() <em>CHECKSUMALG</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCHECKSUMALG()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKSUMALG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCHECKSUMALG() <em>CHECKSUMALG</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCHECKSUMALG()
	 * @generated
	 * @ordered
	 */
	protected String cHECKSUMALG = CHECKSUMALG_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSOURCEENDADDRESS() <em>SOURCEENDADDRESS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSOURCEENDADDRESS()
	 * @generated
	 * @ordered
	 */
	protected SOURCEENDADDRESS sOURCEENDADDRESS;

	/**
	 * The cached value of the '{@link #getUNCOMPRESSEDSIZE() <em>UNCOMPRESSEDSIZE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUNCOMPRESSEDSIZE()
	 * @generated
	 * @ordered
	 */
	protected UNCOMPRESSEDSIZE uNCOMPRESSEDSIZE;

	/**
	 * The cached value of the '{@link #getCHECKSUMRESULT() <em>CHECKSUMRESULT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCHECKSUMRESULT()
	 * @generated
	 * @ordered
	 */
	protected CHECKSUMRESULT cHECKSUMRESULT;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CHECKSUMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCHECKSUM();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getFILLBYTE() {
		return fILLBYTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFILLBYTE(byte[] newFILLBYTE) {
		byte[] oldFILLBYTE = fILLBYTE;
		fILLBYTE = newFILLBYTE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__FILLBYTE, oldFILLBYTE, fILLBYTE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getSOURCESTARTADDRESS() {
		return sOURCESTARTADDRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSOURCESTARTADDRESS(byte[] newSOURCESTARTADDRESS) {
		byte[] oldSOURCESTARTADDRESS = sOURCESTARTADDRESS;
		sOURCESTARTADDRESS = newSOURCESTARTADDRESS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__SOURCESTARTADDRESS, oldSOURCESTARTADDRESS, sOURCESTARTADDRESS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getCOMPRESSEDSIZE() {
		return cOMPRESSEDSIZE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPRESSEDSIZE(long newCOMPRESSEDSIZE) {
		long oldCOMPRESSEDSIZE = cOMPRESSEDSIZE;
		cOMPRESSEDSIZE = newCOMPRESSEDSIZE;
		boolean oldCOMPRESSEDSIZEESet = cOMPRESSEDSIZEESet;
		cOMPRESSEDSIZEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__COMPRESSEDSIZE, oldCOMPRESSEDSIZE, cOMPRESSEDSIZE, !oldCOMPRESSEDSIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetCOMPRESSEDSIZE() {
		long oldCOMPRESSEDSIZE = cOMPRESSEDSIZE;
		boolean oldCOMPRESSEDSIZEESet = cOMPRESSEDSIZEESet;
		cOMPRESSEDSIZE = COMPRESSEDSIZE_EDEFAULT;
		cOMPRESSEDSIZEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.CHECKSUM__COMPRESSEDSIZE, oldCOMPRESSEDSIZE, COMPRESSEDSIZE_EDEFAULT, oldCOMPRESSEDSIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetCOMPRESSEDSIZE() {
		return cOMPRESSEDSIZEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCHECKSUMALG() {
		return cHECKSUMALG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCHECKSUMALG(String newCHECKSUMALG) {
		String oldCHECKSUMALG = cHECKSUMALG;
		cHECKSUMALG = newCHECKSUMALG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__CHECKSUMALG, oldCHECKSUMALG, cHECKSUMALG));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SOURCEENDADDRESS getSOURCEENDADDRESS() {
		return sOURCEENDADDRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSOURCEENDADDRESS(SOURCEENDADDRESS newSOURCEENDADDRESS, NotificationChain msgs) {
		SOURCEENDADDRESS oldSOURCEENDADDRESS = sOURCEENDADDRESS;
		sOURCEENDADDRESS = newSOURCEENDADDRESS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__SOURCEENDADDRESS, oldSOURCEENDADDRESS, newSOURCEENDADDRESS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSOURCEENDADDRESS(SOURCEENDADDRESS newSOURCEENDADDRESS) {
		if (newSOURCEENDADDRESS != sOURCEENDADDRESS) {
			NotificationChain msgs = null;
			if (sOURCEENDADDRESS != null)
				msgs = ((InternalEObject)sOURCEENDADDRESS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__SOURCEENDADDRESS, null, msgs);
			if (newSOURCEENDADDRESS != null)
				msgs = ((InternalEObject)newSOURCEENDADDRESS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__SOURCEENDADDRESS, null, msgs);
			msgs = basicSetSOURCEENDADDRESS(newSOURCEENDADDRESS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__SOURCEENDADDRESS, newSOURCEENDADDRESS, newSOURCEENDADDRESS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNCOMPRESSEDSIZE getUNCOMPRESSEDSIZE() {
		return uNCOMPRESSEDSIZE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUNCOMPRESSEDSIZE(UNCOMPRESSEDSIZE newUNCOMPRESSEDSIZE, NotificationChain msgs) {
		UNCOMPRESSEDSIZE oldUNCOMPRESSEDSIZE = uNCOMPRESSEDSIZE;
		uNCOMPRESSEDSIZE = newUNCOMPRESSEDSIZE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__UNCOMPRESSEDSIZE, oldUNCOMPRESSEDSIZE, newUNCOMPRESSEDSIZE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUNCOMPRESSEDSIZE(UNCOMPRESSEDSIZE newUNCOMPRESSEDSIZE) {
		if (newUNCOMPRESSEDSIZE != uNCOMPRESSEDSIZE) {
			NotificationChain msgs = null;
			if (uNCOMPRESSEDSIZE != null)
				msgs = ((InternalEObject)uNCOMPRESSEDSIZE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__UNCOMPRESSEDSIZE, null, msgs);
			if (newUNCOMPRESSEDSIZE != null)
				msgs = ((InternalEObject)newUNCOMPRESSEDSIZE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__UNCOMPRESSEDSIZE, null, msgs);
			msgs = basicSetUNCOMPRESSEDSIZE(newUNCOMPRESSEDSIZE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__UNCOMPRESSEDSIZE, newUNCOMPRESSEDSIZE, newUNCOMPRESSEDSIZE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CHECKSUMRESULT getCHECKSUMRESULT() {
		return cHECKSUMRESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCHECKSUMRESULT(CHECKSUMRESULT newCHECKSUMRESULT, NotificationChain msgs) {
		CHECKSUMRESULT oldCHECKSUMRESULT = cHECKSUMRESULT;
		cHECKSUMRESULT = newCHECKSUMRESULT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__CHECKSUMRESULT, oldCHECKSUMRESULT, newCHECKSUMRESULT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCHECKSUMRESULT(CHECKSUMRESULT newCHECKSUMRESULT) {
		if (newCHECKSUMRESULT != cHECKSUMRESULT) {
			NotificationChain msgs = null;
			if (cHECKSUMRESULT != null)
				msgs = ((InternalEObject)cHECKSUMRESULT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__CHECKSUMRESULT, null, msgs);
			if (newCHECKSUMRESULT != null)
				msgs = ((InternalEObject)newCHECKSUMRESULT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CHECKSUM__CHECKSUMRESULT, null, msgs);
			msgs = basicSetCHECKSUMRESULT(newCHECKSUMRESULT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__CHECKSUMRESULT, newCHECKSUMRESULT, newCHECKSUMRESULT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CHECKSUM__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUM__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.CHECKSUM__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.CHECKSUM__SOURCEENDADDRESS:
				return basicSetSOURCEENDADDRESS(null, msgs);
			case OdxXhtmlPackage.CHECKSUM__UNCOMPRESSEDSIZE:
				return basicSetUNCOMPRESSEDSIZE(null, msgs);
			case OdxXhtmlPackage.CHECKSUM__CHECKSUMRESULT:
				return basicSetCHECKSUMRESULT(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUM__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.CHECKSUM__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.CHECKSUM__DESC:
				return getDESC();
			case OdxXhtmlPackage.CHECKSUM__FILLBYTE:
				return getFILLBYTE();
			case OdxXhtmlPackage.CHECKSUM__SOURCESTARTADDRESS:
				return getSOURCESTARTADDRESS();
			case OdxXhtmlPackage.CHECKSUM__COMPRESSEDSIZE:
				return getCOMPRESSEDSIZE();
			case OdxXhtmlPackage.CHECKSUM__CHECKSUMALG:
				return getCHECKSUMALG();
			case OdxXhtmlPackage.CHECKSUM__SOURCEENDADDRESS:
				return getSOURCEENDADDRESS();
			case OdxXhtmlPackage.CHECKSUM__UNCOMPRESSEDSIZE:
				return getUNCOMPRESSEDSIZE();
			case OdxXhtmlPackage.CHECKSUM__CHECKSUMRESULT:
				return getCHECKSUMRESULT();
			case OdxXhtmlPackage.CHECKSUM__ID:
				return getID();
			case OdxXhtmlPackage.CHECKSUM__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUM__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__FILLBYTE:
				setFILLBYTE((byte[])newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__SOURCESTARTADDRESS:
				setSOURCESTARTADDRESS((byte[])newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__COMPRESSEDSIZE:
				setCOMPRESSEDSIZE((Long)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__CHECKSUMALG:
				setCHECKSUMALG((String)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__SOURCEENDADDRESS:
				setSOURCEENDADDRESS((SOURCEENDADDRESS)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__UNCOMPRESSEDSIZE:
				setUNCOMPRESSEDSIZE((UNCOMPRESSEDSIZE)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__CHECKSUMRESULT:
				setCHECKSUMRESULT((CHECKSUMRESULT)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.CHECKSUM__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUM__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.CHECKSUM__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.CHECKSUM__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.CHECKSUM__FILLBYTE:
				setFILLBYTE(FILLBYTE_EDEFAULT);
				return;
			case OdxXhtmlPackage.CHECKSUM__SOURCESTARTADDRESS:
				setSOURCESTARTADDRESS(SOURCESTARTADDRESS_EDEFAULT);
				return;
			case OdxXhtmlPackage.CHECKSUM__COMPRESSEDSIZE:
				unsetCOMPRESSEDSIZE();
				return;
			case OdxXhtmlPackage.CHECKSUM__CHECKSUMALG:
				setCHECKSUMALG(CHECKSUMALG_EDEFAULT);
				return;
			case OdxXhtmlPackage.CHECKSUM__SOURCEENDADDRESS:
				setSOURCEENDADDRESS((SOURCEENDADDRESS)null);
				return;
			case OdxXhtmlPackage.CHECKSUM__UNCOMPRESSEDSIZE:
				setUNCOMPRESSEDSIZE((UNCOMPRESSEDSIZE)null);
				return;
			case OdxXhtmlPackage.CHECKSUM__CHECKSUMRESULT:
				setCHECKSUMRESULT((CHECKSUMRESULT)null);
				return;
			case OdxXhtmlPackage.CHECKSUM__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.CHECKSUM__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUM__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.CHECKSUM__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.CHECKSUM__DESC:
				return dESC != null;
			case OdxXhtmlPackage.CHECKSUM__FILLBYTE:
				return FILLBYTE_EDEFAULT == null ? fILLBYTE != null : !FILLBYTE_EDEFAULT.equals(fILLBYTE);
			case OdxXhtmlPackage.CHECKSUM__SOURCESTARTADDRESS:
				return SOURCESTARTADDRESS_EDEFAULT == null ? sOURCESTARTADDRESS != null : !SOURCESTARTADDRESS_EDEFAULT.equals(sOURCESTARTADDRESS);
			case OdxXhtmlPackage.CHECKSUM__COMPRESSEDSIZE:
				return isSetCOMPRESSEDSIZE();
			case OdxXhtmlPackage.CHECKSUM__CHECKSUMALG:
				return CHECKSUMALG_EDEFAULT == null ? cHECKSUMALG != null : !CHECKSUMALG_EDEFAULT.equals(cHECKSUMALG);
			case OdxXhtmlPackage.CHECKSUM__SOURCEENDADDRESS:
				return sOURCEENDADDRESS != null;
			case OdxXhtmlPackage.CHECKSUM__UNCOMPRESSEDSIZE:
				return uNCOMPRESSEDSIZE != null;
			case OdxXhtmlPackage.CHECKSUM__CHECKSUMRESULT:
				return cHECKSUMRESULT != null;
			case OdxXhtmlPackage.CHECKSUM__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.CHECKSUM__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", fILLBYTE: ");
		result.append(fILLBYTE);
		result.append(", sOURCESTARTADDRESS: ");
		result.append(sOURCESTARTADDRESS);
		result.append(", cOMPRESSEDSIZE: ");
		if (cOMPRESSEDSIZEESet) result.append(cOMPRESSEDSIZE); else result.append("<unset>");
		result.append(", cHECKSUMALG: ");
		result.append(cHECKSUMALG);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //CHECKSUMImpl
