/**
 */
package OdxXhtml.impl;

import OdxXhtml.DTC;
import OdxXhtml.DTCS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DTCS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DTCSImpl#getDTCPROXY <em>DTCPROXY</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCSImpl#getDTCREF <em>DTCREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCSImpl#getDTC <em>DTC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DTCSImpl extends MinimalEObjectImpl.Container implements DTCS {
	/**
	 * The cached value of the '{@link #getDTCPROXY() <em>DTCPROXY</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDTCPROXY()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap dTCPROXY;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DTCSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDTCS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getDTCPROXY() {
		if (dTCPROXY == null) {
			dTCPROXY = new BasicFeatureMap(this, OdxXhtmlPackage.DTCS__DTCPROXY);
		}
		return dTCPROXY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getDTCREF() {
		return getDTCPROXY().list(OdxXhtmlPackage.eINSTANCE.getDTCS_DTCREF());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DTC> getDTC() {
		return getDTCPROXY().list(OdxXhtmlPackage.eINSTANCE.getDTCS_DTC());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCS__DTCPROXY:
				return ((InternalEList<?>)getDTCPROXY()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DTCS__DTCREF:
				return ((InternalEList<?>)getDTCREF()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DTCS__DTC:
				return ((InternalEList<?>)getDTC()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCS__DTCPROXY:
				if (coreType) return getDTCPROXY();
				return ((FeatureMap.Internal)getDTCPROXY()).getWrapper();
			case OdxXhtmlPackage.DTCS__DTCREF:
				return getDTCREF();
			case OdxXhtmlPackage.DTCS__DTC:
				return getDTC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCS__DTCPROXY:
				((FeatureMap.Internal)getDTCPROXY()).set(newValue);
				return;
			case OdxXhtmlPackage.DTCS__DTCREF:
				getDTCREF().clear();
				getDTCREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
			case OdxXhtmlPackage.DTCS__DTC:
				getDTC().clear();
				getDTC().addAll((Collection<? extends DTC>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCS__DTCPROXY:
				getDTCPROXY().clear();
				return;
			case OdxXhtmlPackage.DTCS__DTCREF:
				getDTCREF().clear();
				return;
			case OdxXhtmlPackage.DTCS__DTC:
				getDTC().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCS__DTCPROXY:
				return dTCPROXY != null && !dTCPROXY.isEmpty();
			case OdxXhtmlPackage.DTCS__DTCREF:
				return !getDTCREF().isEmpty();
			case OdxXhtmlPackage.DTCS__DTC:
				return !getDTC().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (dTCPROXY: ");
		result.append(dTCPROXY);
		result.append(')');
		return result.toString();
	}

} //DTCSImpl
