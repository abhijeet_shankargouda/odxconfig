/**
 */
package OdxXhtml.impl;

import OdxXhtml.OUTPUTPARAM;
import OdxXhtml.OUTPUTPARAMS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OUTPUTPARAMS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.OUTPUTPARAMSImpl#getOUTPUTPARAM <em>OUTPUTPARAM</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OUTPUTPARAMSImpl extends MinimalEObjectImpl.Container implements OUTPUTPARAMS {
	/**
	 * The cached value of the '{@link #getOUTPUTPARAM() <em>OUTPUTPARAM</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOUTPUTPARAM()
	 * @generated
	 * @ordered
	 */
	protected EList<OUTPUTPARAM> oUTPUTPARAM;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OUTPUTPARAMSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getOUTPUTPARAMS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<OUTPUTPARAM> getOUTPUTPARAM() {
		if (oUTPUTPARAM == null) {
			oUTPUTPARAM = new EObjectContainmentEList<OUTPUTPARAM>(OUTPUTPARAM.class, this, OdxXhtmlPackage.OUTPUTPARAMS__OUTPUTPARAM);
		}
		return oUTPUTPARAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.OUTPUTPARAMS__OUTPUTPARAM:
				return ((InternalEList<?>)getOUTPUTPARAM()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.OUTPUTPARAMS__OUTPUTPARAM:
				return getOUTPUTPARAM();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.OUTPUTPARAMS__OUTPUTPARAM:
				getOUTPUTPARAM().clear();
				getOUTPUTPARAM().addAll((Collection<? extends OUTPUTPARAM>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.OUTPUTPARAMS__OUTPUTPARAM:
				getOUTPUTPARAM().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.OUTPUTPARAMS__OUTPUTPARAM:
				return oUTPUTPARAM != null && !oUTPUTPARAM.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OUTPUTPARAMSImpl
