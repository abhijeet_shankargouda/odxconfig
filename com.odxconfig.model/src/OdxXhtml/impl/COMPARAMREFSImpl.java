/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPARAMREF;
import OdxXhtml.COMPARAMREFS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPARAMREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPARAMREFSImpl#getCOMPARAMREF <em>COMPARAMREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPARAMREFSImpl extends MinimalEObjectImpl.Container implements COMPARAMREFS {
	/**
	 * The cached value of the '{@link #getCOMPARAMREF() <em>COMPARAMREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPARAMREF()
	 * @generated
	 * @ordered
	 */
	protected EList<COMPARAMREF> cOMPARAMREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPARAMREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPARAMREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<COMPARAMREF> getCOMPARAMREF() {
		if (cOMPARAMREF == null) {
			cOMPARAMREF = new EObjectContainmentEList<COMPARAMREF>(COMPARAMREF.class, this, OdxXhtmlPackage.COMPARAMREFS__COMPARAMREF);
		}
		return cOMPARAMREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREFS__COMPARAMREF:
				return ((InternalEList<?>)getCOMPARAMREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREFS__COMPARAMREF:
				return getCOMPARAMREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREFS__COMPARAMREF:
				getCOMPARAMREF().clear();
				getCOMPARAMREF().addAll((Collection<? extends COMPARAMREF>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREFS__COMPARAMREF:
				getCOMPARAMREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREFS__COMPARAMREF:
				return cOMPARAMREF != null && !cOMPARAMREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //COMPARAMREFSImpl
