/**
 */
package OdxXhtml.impl;

import OdxXhtml.ENDOFPDUFIELD;
import OdxXhtml.ENDOFPDUFIELDS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ENDOFPDUFIELDS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ENDOFPDUFIELDSImpl#getENDOFPDUFIELD <em>ENDOFPDUFIELD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ENDOFPDUFIELDSImpl extends MinimalEObjectImpl.Container implements ENDOFPDUFIELDS {
	/**
	 * The cached value of the '{@link #getENDOFPDUFIELD() <em>ENDOFPDUFIELD</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENDOFPDUFIELD()
	 * @generated
	 * @ordered
	 */
	protected EList<ENDOFPDUFIELD> eNDOFPDUFIELD;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ENDOFPDUFIELDSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getENDOFPDUFIELDS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ENDOFPDUFIELD> getENDOFPDUFIELD() {
		if (eNDOFPDUFIELD == null) {
			eNDOFPDUFIELD = new EObjectContainmentEList<ENDOFPDUFIELD>(ENDOFPDUFIELD.class, this, OdxXhtmlPackage.ENDOFPDUFIELDS__ENDOFPDUFIELD);
		}
		return eNDOFPDUFIELD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ENDOFPDUFIELDS__ENDOFPDUFIELD:
				return ((InternalEList<?>)getENDOFPDUFIELD()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ENDOFPDUFIELDS__ENDOFPDUFIELD:
				return getENDOFPDUFIELD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ENDOFPDUFIELDS__ENDOFPDUFIELD:
				getENDOFPDUFIELD().clear();
				getENDOFPDUFIELD().addAll((Collection<? extends ENDOFPDUFIELD>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENDOFPDUFIELDS__ENDOFPDUFIELD:
				getENDOFPDUFIELD().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENDOFPDUFIELDS__ENDOFPDUFIELD:
				return eNDOFPDUFIELD != null && !eNDOFPDUFIELD.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ENDOFPDUFIELDSImpl
