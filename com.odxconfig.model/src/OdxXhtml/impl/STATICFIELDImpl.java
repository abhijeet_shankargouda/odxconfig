/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.STATICFIELD;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>STATICFIELD</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.STATICFIELDImpl#getFIXEDNUMBEROFITEMS <em>FIXEDNUMBEROFITEMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.STATICFIELDImpl#getITEMBYTESIZE <em>ITEMBYTESIZE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class STATICFIELDImpl extends FIELDImpl implements STATICFIELD {
	/**
	 * The default value of the '{@link #getFIXEDNUMBEROFITEMS() <em>FIXEDNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFIXEDNUMBEROFITEMS()
	 * @generated
	 * @ordered
	 */
	protected static final long FIXEDNUMBEROFITEMS_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getFIXEDNUMBEROFITEMS() <em>FIXEDNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFIXEDNUMBEROFITEMS()
	 * @generated
	 * @ordered
	 */
	protected long fIXEDNUMBEROFITEMS = FIXEDNUMBEROFITEMS_EDEFAULT;

	/**
	 * This is true if the FIXEDNUMBEROFITEMS attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fIXEDNUMBEROFITEMSESet;

	/**
	 * The default value of the '{@link #getITEMBYTESIZE() <em>ITEMBYTESIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getITEMBYTESIZE()
	 * @generated
	 * @ordered
	 */
	protected static final long ITEMBYTESIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getITEMBYTESIZE() <em>ITEMBYTESIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getITEMBYTESIZE()
	 * @generated
	 * @ordered
	 */
	protected long iTEMBYTESIZE = ITEMBYTESIZE_EDEFAULT;

	/**
	 * This is true if the ITEMBYTESIZE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iTEMBYTESIZEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected STATICFIELDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSTATICFIELD();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getFIXEDNUMBEROFITEMS() {
		return fIXEDNUMBEROFITEMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFIXEDNUMBEROFITEMS(long newFIXEDNUMBEROFITEMS) {
		long oldFIXEDNUMBEROFITEMS = fIXEDNUMBEROFITEMS;
		fIXEDNUMBEROFITEMS = newFIXEDNUMBEROFITEMS;
		boolean oldFIXEDNUMBEROFITEMSESet = fIXEDNUMBEROFITEMSESet;
		fIXEDNUMBEROFITEMSESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.STATICFIELD__FIXEDNUMBEROFITEMS, oldFIXEDNUMBEROFITEMS, fIXEDNUMBEROFITEMS, !oldFIXEDNUMBEROFITEMSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetFIXEDNUMBEROFITEMS() {
		long oldFIXEDNUMBEROFITEMS = fIXEDNUMBEROFITEMS;
		boolean oldFIXEDNUMBEROFITEMSESet = fIXEDNUMBEROFITEMSESet;
		fIXEDNUMBEROFITEMS = FIXEDNUMBEROFITEMS_EDEFAULT;
		fIXEDNUMBEROFITEMSESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.STATICFIELD__FIXEDNUMBEROFITEMS, oldFIXEDNUMBEROFITEMS, FIXEDNUMBEROFITEMS_EDEFAULT, oldFIXEDNUMBEROFITEMSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetFIXEDNUMBEROFITEMS() {
		return fIXEDNUMBEROFITEMSESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getITEMBYTESIZE() {
		return iTEMBYTESIZE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setITEMBYTESIZE(long newITEMBYTESIZE) {
		long oldITEMBYTESIZE = iTEMBYTESIZE;
		iTEMBYTESIZE = newITEMBYTESIZE;
		boolean oldITEMBYTESIZEESet = iTEMBYTESIZEESet;
		iTEMBYTESIZEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.STATICFIELD__ITEMBYTESIZE, oldITEMBYTESIZE, iTEMBYTESIZE, !oldITEMBYTESIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetITEMBYTESIZE() {
		long oldITEMBYTESIZE = iTEMBYTESIZE;
		boolean oldITEMBYTESIZEESet = iTEMBYTESIZEESet;
		iTEMBYTESIZE = ITEMBYTESIZE_EDEFAULT;
		iTEMBYTESIZEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.STATICFIELD__ITEMBYTESIZE, oldITEMBYTESIZE, ITEMBYTESIZE_EDEFAULT, oldITEMBYTESIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetITEMBYTESIZE() {
		return iTEMBYTESIZEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.STATICFIELD__FIXEDNUMBEROFITEMS:
				return getFIXEDNUMBEROFITEMS();
			case OdxXhtmlPackage.STATICFIELD__ITEMBYTESIZE:
				return getITEMBYTESIZE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.STATICFIELD__FIXEDNUMBEROFITEMS:
				setFIXEDNUMBEROFITEMS((Long)newValue);
				return;
			case OdxXhtmlPackage.STATICFIELD__ITEMBYTESIZE:
				setITEMBYTESIZE((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STATICFIELD__FIXEDNUMBEROFITEMS:
				unsetFIXEDNUMBEROFITEMS();
				return;
			case OdxXhtmlPackage.STATICFIELD__ITEMBYTESIZE:
				unsetITEMBYTESIZE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STATICFIELD__FIXEDNUMBEROFITEMS:
				return isSetFIXEDNUMBEROFITEMS();
			case OdxXhtmlPackage.STATICFIELD__ITEMBYTESIZE:
				return isSetITEMBYTESIZE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (fIXEDNUMBEROFITEMS: ");
		if (fIXEDNUMBEROFITEMSESet) result.append(fIXEDNUMBEROFITEMS); else result.append("<unset>");
		result.append(", iTEMBYTESIZE: ");
		if (iTEMBYTESIZEESet) result.append(iTEMBYTESIZE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //STATICFIELDImpl
