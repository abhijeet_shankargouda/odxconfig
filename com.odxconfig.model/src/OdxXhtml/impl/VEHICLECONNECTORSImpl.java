/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.VEHICLECONNECTOR;
import OdxXhtml.VEHICLECONNECTORS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VEHICLECONNECTORS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORSImpl#getVEHICLECONNECTOR <em>VEHICLECONNECTOR</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VEHICLECONNECTORSImpl extends MinimalEObjectImpl.Container implements VEHICLECONNECTORS {
	/**
	 * The cached value of the '{@link #getVEHICLECONNECTOR() <em>VEHICLECONNECTOR</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVEHICLECONNECTOR()
	 * @generated
	 * @ordered
	 */
	protected EList<VEHICLECONNECTOR> vEHICLECONNECTOR;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VEHICLECONNECTORSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVEHICLECONNECTORS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<VEHICLECONNECTOR> getVEHICLECONNECTOR() {
		if (vEHICLECONNECTOR == null) {
			vEHICLECONNECTOR = new EObjectContainmentEList<VEHICLECONNECTOR>(VEHICLECONNECTOR.class, this, OdxXhtmlPackage.VEHICLECONNECTORS__VEHICLECONNECTOR);
		}
		return vEHICLECONNECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORS__VEHICLECONNECTOR:
				return ((InternalEList<?>)getVEHICLECONNECTOR()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORS__VEHICLECONNECTOR:
				return getVEHICLECONNECTOR();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORS__VEHICLECONNECTOR:
				getVEHICLECONNECTOR().clear();
				getVEHICLECONNECTOR().addAll((Collection<? extends VEHICLECONNECTOR>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORS__VEHICLECONNECTOR:
				getVEHICLECONNECTOR().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORS__VEHICLECONNECTOR:
				return vEHICLECONNECTOR != null && !vEHICLECONNECTOR.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //VEHICLECONNECTORSImpl
