/**
 */
package OdxXhtml.impl;

import OdxXhtml.ECUSHAREDDATA;
import OdxXhtml.ECUSHAREDDATAS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUSHAREDDATAS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ECUSHAREDDATASImpl#getECUSHAREDDATA <em>ECUSHAREDDATA</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECUSHAREDDATASImpl extends MinimalEObjectImpl.Container implements ECUSHAREDDATAS {
	/**
	 * The cached value of the '{@link #getECUSHAREDDATA() <em>ECUSHAREDDATA</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUSHAREDDATA()
	 * @generated
	 * @ordered
	 */
	protected EList<ECUSHAREDDATA> eCUSHAREDDATA;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUSHAREDDATASImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUSHAREDDATAS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ECUSHAREDDATA> getECUSHAREDDATA() {
		if (eCUSHAREDDATA == null) {
			eCUSHAREDDATA = new EObjectContainmentEList<ECUSHAREDDATA>(ECUSHAREDDATA.class, this, OdxXhtmlPackage.ECUSHAREDDATAS__ECUSHAREDDATA);
		}
		return eCUSHAREDDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATAS__ECUSHAREDDATA:
				return ((InternalEList<?>)getECUSHAREDDATA()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATAS__ECUSHAREDDATA:
				return getECUSHAREDDATA();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATAS__ECUSHAREDDATA:
				getECUSHAREDDATA().clear();
				getECUSHAREDDATA().addAll((Collection<? extends ECUSHAREDDATA>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATAS__ECUSHAREDDATA:
				getECUSHAREDDATA().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUSHAREDDATAS__ECUSHAREDDATA:
				return eCUSHAREDDATA != null && !eCUSHAREDDATA.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ECUSHAREDDATASImpl
