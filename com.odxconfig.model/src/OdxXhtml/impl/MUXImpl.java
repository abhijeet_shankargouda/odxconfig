/**
 */
package OdxXhtml.impl;

import OdxXhtml.CASES;
import OdxXhtml.DEFAULTCASE;
import OdxXhtml.MUX;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SWITCHKEY;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUX</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MUXImpl#getBYTEPOSITION <em>BYTEPOSITION</em>}</li>
 *   <li>{@link OdxXhtml.impl.MUXImpl#getSWITCHKEY <em>SWITCHKEY</em>}</li>
 *   <li>{@link OdxXhtml.impl.MUXImpl#getDEFAULTCASE <em>DEFAULTCASE</em>}</li>
 *   <li>{@link OdxXhtml.impl.MUXImpl#getCASES <em>CASES</em>}</li>
 *   <li>{@link OdxXhtml.impl.MUXImpl#isISVISIBLE <em>ISVISIBLE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MUXImpl extends COMPLEXDOPImpl implements MUX {
	/**
	 * The default value of the '{@link #getBYTEPOSITION() <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTEPOSITION()
	 * @generated
	 * @ordered
	 */
	protected static final long BYTEPOSITION_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBYTEPOSITION() <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTEPOSITION()
	 * @generated
	 * @ordered
	 */
	protected long bYTEPOSITION = BYTEPOSITION_EDEFAULT;

	/**
	 * This is true if the BYTEPOSITION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bYTEPOSITIONESet;

	/**
	 * The cached value of the '{@link #getSWITCHKEY() <em>SWITCHKEY</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSWITCHKEY()
	 * @generated
	 * @ordered
	 */
	protected SWITCHKEY sWITCHKEY;

	/**
	 * The cached value of the '{@link #getDEFAULTCASE() <em>DEFAULTCASE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDEFAULTCASE()
	 * @generated
	 * @ordered
	 */
	protected DEFAULTCASE dEFAULTCASE;

	/**
	 * The cached value of the '{@link #getCASES() <em>CASES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCASES()
	 * @generated
	 * @ordered
	 */
	protected CASES cASES;

	/**
	 * The default value of the '{@link #isISVISIBLE() <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISVISIBLE()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISVISIBLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISVISIBLE() <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISVISIBLE()
	 * @generated
	 * @ordered
	 */
	protected boolean iSVISIBLE = ISVISIBLE_EDEFAULT;

	/**
	 * This is true if the ISVISIBLE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSVISIBLEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUXImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMUX();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBYTEPOSITION() {
		return bYTEPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBYTEPOSITION(long newBYTEPOSITION) {
		long oldBYTEPOSITION = bYTEPOSITION;
		bYTEPOSITION = newBYTEPOSITION;
		boolean oldBYTEPOSITIONESet = bYTEPOSITIONESet;
		bYTEPOSITIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MUX__BYTEPOSITION, oldBYTEPOSITION, bYTEPOSITION, !oldBYTEPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBYTEPOSITION() {
		long oldBYTEPOSITION = bYTEPOSITION;
		boolean oldBYTEPOSITIONESet = bYTEPOSITIONESet;
		bYTEPOSITION = BYTEPOSITION_EDEFAULT;
		bYTEPOSITIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MUX__BYTEPOSITION, oldBYTEPOSITION, BYTEPOSITION_EDEFAULT, oldBYTEPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBYTEPOSITION() {
		return bYTEPOSITIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SWITCHKEY getSWITCHKEY() {
		return sWITCHKEY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSWITCHKEY(SWITCHKEY newSWITCHKEY, NotificationChain msgs) {
		SWITCHKEY oldSWITCHKEY = sWITCHKEY;
		sWITCHKEY = newSWITCHKEY;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MUX__SWITCHKEY, oldSWITCHKEY, newSWITCHKEY);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSWITCHKEY(SWITCHKEY newSWITCHKEY) {
		if (newSWITCHKEY != sWITCHKEY) {
			NotificationChain msgs = null;
			if (sWITCHKEY != null)
				msgs = ((InternalEObject)sWITCHKEY).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MUX__SWITCHKEY, null, msgs);
			if (newSWITCHKEY != null)
				msgs = ((InternalEObject)newSWITCHKEY).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MUX__SWITCHKEY, null, msgs);
			msgs = basicSetSWITCHKEY(newSWITCHKEY, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MUX__SWITCHKEY, newSWITCHKEY, newSWITCHKEY));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DEFAULTCASE getDEFAULTCASE() {
		return dEFAULTCASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDEFAULTCASE(DEFAULTCASE newDEFAULTCASE, NotificationChain msgs) {
		DEFAULTCASE oldDEFAULTCASE = dEFAULTCASE;
		dEFAULTCASE = newDEFAULTCASE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MUX__DEFAULTCASE, oldDEFAULTCASE, newDEFAULTCASE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDEFAULTCASE(DEFAULTCASE newDEFAULTCASE) {
		if (newDEFAULTCASE != dEFAULTCASE) {
			NotificationChain msgs = null;
			if (dEFAULTCASE != null)
				msgs = ((InternalEObject)dEFAULTCASE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MUX__DEFAULTCASE, null, msgs);
			if (newDEFAULTCASE != null)
				msgs = ((InternalEObject)newDEFAULTCASE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MUX__DEFAULTCASE, null, msgs);
			msgs = basicSetDEFAULTCASE(newDEFAULTCASE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MUX__DEFAULTCASE, newDEFAULTCASE, newDEFAULTCASE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CASES getCASES() {
		return cASES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCASES(CASES newCASES, NotificationChain msgs) {
		CASES oldCASES = cASES;
		cASES = newCASES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MUX__CASES, oldCASES, newCASES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCASES(CASES newCASES) {
		if (newCASES != cASES) {
			NotificationChain msgs = null;
			if (cASES != null)
				msgs = ((InternalEObject)cASES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MUX__CASES, null, msgs);
			if (newCASES != null)
				msgs = ((InternalEObject)newCASES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MUX__CASES, null, msgs);
			msgs = basicSetCASES(newCASES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MUX__CASES, newCASES, newCASES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISVISIBLE() {
		return iSVISIBLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISVISIBLE(boolean newISVISIBLE) {
		boolean oldISVISIBLE = iSVISIBLE;
		iSVISIBLE = newISVISIBLE;
		boolean oldISVISIBLEESet = iSVISIBLEESet;
		iSVISIBLEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MUX__ISVISIBLE, oldISVISIBLE, iSVISIBLE, !oldISVISIBLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISVISIBLE() {
		boolean oldISVISIBLE = iSVISIBLE;
		boolean oldISVISIBLEESet = iSVISIBLEESet;
		iSVISIBLE = ISVISIBLE_EDEFAULT;
		iSVISIBLEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MUX__ISVISIBLE, oldISVISIBLE, ISVISIBLE_EDEFAULT, oldISVISIBLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISVISIBLE() {
		return iSVISIBLEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MUX__SWITCHKEY:
				return basicSetSWITCHKEY(null, msgs);
			case OdxXhtmlPackage.MUX__DEFAULTCASE:
				return basicSetDEFAULTCASE(null, msgs);
			case OdxXhtmlPackage.MUX__CASES:
				return basicSetCASES(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MUX__BYTEPOSITION:
				return getBYTEPOSITION();
			case OdxXhtmlPackage.MUX__SWITCHKEY:
				return getSWITCHKEY();
			case OdxXhtmlPackage.MUX__DEFAULTCASE:
				return getDEFAULTCASE();
			case OdxXhtmlPackage.MUX__CASES:
				return getCASES();
			case OdxXhtmlPackage.MUX__ISVISIBLE:
				return isISVISIBLE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MUX__BYTEPOSITION:
				setBYTEPOSITION((Long)newValue);
				return;
			case OdxXhtmlPackage.MUX__SWITCHKEY:
				setSWITCHKEY((SWITCHKEY)newValue);
				return;
			case OdxXhtmlPackage.MUX__DEFAULTCASE:
				setDEFAULTCASE((DEFAULTCASE)newValue);
				return;
			case OdxXhtmlPackage.MUX__CASES:
				setCASES((CASES)newValue);
				return;
			case OdxXhtmlPackage.MUX__ISVISIBLE:
				setISVISIBLE((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MUX__BYTEPOSITION:
				unsetBYTEPOSITION();
				return;
			case OdxXhtmlPackage.MUX__SWITCHKEY:
				setSWITCHKEY((SWITCHKEY)null);
				return;
			case OdxXhtmlPackage.MUX__DEFAULTCASE:
				setDEFAULTCASE((DEFAULTCASE)null);
				return;
			case OdxXhtmlPackage.MUX__CASES:
				setCASES((CASES)null);
				return;
			case OdxXhtmlPackage.MUX__ISVISIBLE:
				unsetISVISIBLE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MUX__BYTEPOSITION:
				return isSetBYTEPOSITION();
			case OdxXhtmlPackage.MUX__SWITCHKEY:
				return sWITCHKEY != null;
			case OdxXhtmlPackage.MUX__DEFAULTCASE:
				return dEFAULTCASE != null;
			case OdxXhtmlPackage.MUX__CASES:
				return cASES != null;
			case OdxXhtmlPackage.MUX__ISVISIBLE:
				return isSetISVISIBLE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bYTEPOSITION: ");
		if (bYTEPOSITIONESet) result.append(bYTEPOSITION); else result.append("<unset>");
		result.append(", iSVISIBLE: ");
		if (iSVISIBLEESet) result.append(iSVISIBLE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MUXImpl
