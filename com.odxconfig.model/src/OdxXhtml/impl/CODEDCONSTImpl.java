/**
 */
package OdxXhtml.impl;

import OdxXhtml.CODEDCONST;
import OdxXhtml.DIAGCODEDTYPE;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CODEDCONST</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.CODEDCONSTImpl#getCODEDVALUE <em>CODEDVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.CODEDCONSTImpl#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CODEDCONSTImpl extends POSITIONABLEPARAMImpl implements CODEDCONST {
	/**
	 * The default value of the '{@link #getCODEDVALUE() <em>CODEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCODEDVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String CODEDVALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCODEDVALUE() <em>CODEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCODEDVALUE()
	 * @generated
	 * @ordered
	 */
	protected String cODEDVALUE = CODEDVALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDIAGCODEDTYPE() <em>DIAGCODEDTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCODEDTYPE()
	 * @generated
	 * @ordered
	 */
	protected DIAGCODEDTYPE dIAGCODEDTYPE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CODEDCONSTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCODEDCONST();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCODEDVALUE() {
		return cODEDVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCODEDVALUE(String newCODEDVALUE) {
		String oldCODEDVALUE = cODEDVALUE;
		cODEDVALUE = newCODEDVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CODEDCONST__CODEDVALUE, oldCODEDVALUE, cODEDVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGCODEDTYPE getDIAGCODEDTYPE() {
		return dIAGCODEDTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCODEDTYPE(DIAGCODEDTYPE newDIAGCODEDTYPE, NotificationChain msgs) {
		DIAGCODEDTYPE oldDIAGCODEDTYPE = dIAGCODEDTYPE;
		dIAGCODEDTYPE = newDIAGCODEDTYPE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CODEDCONST__DIAGCODEDTYPE, oldDIAGCODEDTYPE, newDIAGCODEDTYPE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCODEDTYPE(DIAGCODEDTYPE newDIAGCODEDTYPE) {
		if (newDIAGCODEDTYPE != dIAGCODEDTYPE) {
			NotificationChain msgs = null;
			if (dIAGCODEDTYPE != null)
				msgs = ((InternalEObject)dIAGCODEDTYPE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CODEDCONST__DIAGCODEDTYPE, null, msgs);
			if (newDIAGCODEDTYPE != null)
				msgs = ((InternalEObject)newDIAGCODEDTYPE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CODEDCONST__DIAGCODEDTYPE, null, msgs);
			msgs = basicSetDIAGCODEDTYPE(newDIAGCODEDTYPE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CODEDCONST__DIAGCODEDTYPE, newDIAGCODEDTYPE, newDIAGCODEDTYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.CODEDCONST__DIAGCODEDTYPE:
				return basicSetDIAGCODEDTYPE(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.CODEDCONST__CODEDVALUE:
				return getCODEDVALUE();
			case OdxXhtmlPackage.CODEDCONST__DIAGCODEDTYPE:
				return getDIAGCODEDTYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.CODEDCONST__CODEDVALUE:
				setCODEDVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.CODEDCONST__DIAGCODEDTYPE:
				setDIAGCODEDTYPE((DIAGCODEDTYPE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CODEDCONST__CODEDVALUE:
				setCODEDVALUE(CODEDVALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.CODEDCONST__DIAGCODEDTYPE:
				setDIAGCODEDTYPE((DIAGCODEDTYPE)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CODEDCONST__CODEDVALUE:
				return CODEDVALUE_EDEFAULT == null ? cODEDVALUE != null : !CODEDVALUE_EDEFAULT.equals(cODEDVALUE);
			case OdxXhtmlPackage.CODEDCONST__DIAGCODEDTYPE:
				return dIAGCODEDTYPE != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (cODEDVALUE: ");
		result.append(cODEDVALUE);
		result.append(')');
		return result.toString();
	}

} //CODEDCONSTImpl
