/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PARAMS;
import OdxXhtml.REQUEST;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>REQUEST</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.REQUESTImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.REQUESTImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.REQUESTImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.REQUESTImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.REQUESTImpl#getPARAMS <em>PARAMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.REQUESTImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.REQUESTImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class REQUESTImpl extends MinimalEObjectImpl.Container implements REQUEST {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getPARAMS() <em>PARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARAMS()
	 * @generated
	 * @ordered
	 */
	protected PARAMS pARAMS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected REQUESTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getREQUEST();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.REQUEST__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.REQUEST__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.REQUEST__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.REQUEST__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.REQUEST__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.REQUEST__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARAMS getPARAMS() {
		return pARAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPARAMS(PARAMS newPARAMS, NotificationChain msgs) {
		PARAMS oldPARAMS = pARAMS;
		pARAMS = newPARAMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__PARAMS, oldPARAMS, newPARAMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPARAMS(PARAMS newPARAMS) {
		if (newPARAMS != pARAMS) {
			NotificationChain msgs = null;
			if (pARAMS != null)
				msgs = ((InternalEObject)pARAMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.REQUEST__PARAMS, null, msgs);
			if (newPARAMS != null)
				msgs = ((InternalEObject)newPARAMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.REQUEST__PARAMS, null, msgs);
			msgs = basicSetPARAMS(newPARAMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__PARAMS, newPARAMS, newPARAMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.REQUEST__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.REQUEST__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.REQUEST__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.REQUEST__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.REQUEST__PARAMS:
				return basicSetPARAMS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.REQUEST__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.REQUEST__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.REQUEST__DESC:
				return getDESC();
			case OdxXhtmlPackage.REQUEST__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.REQUEST__PARAMS:
				return getPARAMS();
			case OdxXhtmlPackage.REQUEST__ID:
				return getID();
			case OdxXhtmlPackage.REQUEST__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.REQUEST__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.REQUEST__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.REQUEST__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.REQUEST__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.REQUEST__PARAMS:
				setPARAMS((PARAMS)newValue);
				return;
			case OdxXhtmlPackage.REQUEST__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.REQUEST__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.REQUEST__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.REQUEST__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.REQUEST__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.REQUEST__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.REQUEST__PARAMS:
				setPARAMS((PARAMS)null);
				return;
			case OdxXhtmlPackage.REQUEST__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.REQUEST__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.REQUEST__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.REQUEST__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.REQUEST__DESC:
				return dESC != null;
			case OdxXhtmlPackage.REQUEST__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.REQUEST__PARAMS:
				return pARAMS != null;
			case OdxXhtmlPackage.REQUEST__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.REQUEST__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //REQUESTImpl
