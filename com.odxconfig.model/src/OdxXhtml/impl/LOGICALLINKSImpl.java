/**
 */
package OdxXhtml.impl;

import OdxXhtml.LOGICALLINK;
import OdxXhtml.LOGICALLINKS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LOGICALLINKS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKSImpl#getLOGICALLINK <em>LOGICALLINK</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LOGICALLINKSImpl extends MinimalEObjectImpl.Container implements LOGICALLINKS {
	/**
	 * The cached value of the '{@link #getLOGICALLINK() <em>LOGICALLINK</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLOGICALLINK()
	 * @generated
	 * @ordered
	 */
	protected EList<LOGICALLINK> lOGICALLINK;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LOGICALLINKSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLOGICALLINKS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LOGICALLINK> getLOGICALLINK() {
		if (lOGICALLINK == null) {
			lOGICALLINK = new EObjectContainmentEList<LOGICALLINK>(LOGICALLINK.class, this, OdxXhtmlPackage.LOGICALLINKS__LOGICALLINK);
		}
		return lOGICALLINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKS__LOGICALLINK:
				return ((InternalEList<?>)getLOGICALLINK()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKS__LOGICALLINK:
				return getLOGICALLINK();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKS__LOGICALLINK:
				getLOGICALLINK().clear();
				getLOGICALLINK().addAll((Collection<? extends LOGICALLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKS__LOGICALLINK:
				getLOGICALLINK().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKS__LOGICALLINK:
				return lOGICALLINK != null && !lOGICALLINK.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LOGICALLINKSImpl
