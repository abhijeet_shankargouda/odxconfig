/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.POSRESPONSE;
import OdxXhtml.POSRESPONSES;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>POSRESPONSES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.POSRESPONSESImpl#getPOSRESPONSE <em>POSRESPONSE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class POSRESPONSESImpl extends MinimalEObjectImpl.Container implements POSRESPONSES {
	/**
	 * The cached value of the '{@link #getPOSRESPONSE() <em>POSRESPONSE</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPOSRESPONSE()
	 * @generated
	 * @ordered
	 */
	protected EList<POSRESPONSE> pOSRESPONSE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected POSRESPONSESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPOSRESPONSES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<POSRESPONSE> getPOSRESPONSE() {
		if (pOSRESPONSE == null) {
			pOSRESPONSE = new EObjectContainmentEList<POSRESPONSE>(POSRESPONSE.class, this, OdxXhtmlPackage.POSRESPONSES__POSRESPONSE);
		}
		return pOSRESPONSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.POSRESPONSES__POSRESPONSE:
				return ((InternalEList<?>)getPOSRESPONSE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.POSRESPONSES__POSRESPONSE:
				return getPOSRESPONSE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.POSRESPONSES__POSRESPONSE:
				getPOSRESPONSE().clear();
				getPOSRESPONSE().addAll((Collection<? extends POSRESPONSE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.POSRESPONSES__POSRESPONSE:
				getPOSRESPONSE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.POSRESPONSES__POSRESPONSE:
				return pOSRESPONSE != null && !pOSRESPONSE.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //POSRESPONSESImpl
