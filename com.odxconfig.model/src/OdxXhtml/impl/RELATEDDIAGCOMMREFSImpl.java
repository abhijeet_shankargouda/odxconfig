/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.RELATEDDIAGCOMMREF;
import OdxXhtml.RELATEDDIAGCOMMREFS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RELATEDDIAGCOMMREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.RELATEDDIAGCOMMREFSImpl#getRELATEDDIAGCOMMREF <em>RELATEDDIAGCOMMREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RELATEDDIAGCOMMREFSImpl extends MinimalEObjectImpl.Container implements RELATEDDIAGCOMMREFS {
	/**
	 * The cached value of the '{@link #getRELATEDDIAGCOMMREF() <em>RELATEDDIAGCOMMREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRELATEDDIAGCOMMREF()
	 * @generated
	 * @ordered
	 */
	protected EList<RELATEDDIAGCOMMREF> rELATEDDIAGCOMMREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RELATEDDIAGCOMMREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getRELATEDDIAGCOMMREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RELATEDDIAGCOMMREF> getRELATEDDIAGCOMMREF() {
		if (rELATEDDIAGCOMMREF == null) {
			rELATEDDIAGCOMMREF = new EObjectContainmentEList<RELATEDDIAGCOMMREF>(RELATEDDIAGCOMMREF.class, this, OdxXhtmlPackage.RELATEDDIAGCOMMREFS__RELATEDDIAGCOMMREF);
		}
		return rELATEDDIAGCOMMREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDIAGCOMMREFS__RELATEDDIAGCOMMREF:
				return ((InternalEList<?>)getRELATEDDIAGCOMMREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDIAGCOMMREFS__RELATEDDIAGCOMMREF:
				return getRELATEDDIAGCOMMREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDIAGCOMMREFS__RELATEDDIAGCOMMREF:
				getRELATEDDIAGCOMMREF().clear();
				getRELATEDDIAGCOMMREF().addAll((Collection<? extends RELATEDDIAGCOMMREF>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDIAGCOMMREFS__RELATEDDIAGCOMMREF:
				getRELATEDDIAGCOMMREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDIAGCOMMREFS__RELATEDDIAGCOMMREF:
				return rELATEDDIAGCOMMREF != null && !rELATEDDIAGCOMMREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RELATEDDIAGCOMMREFSImpl
