/**
 */
package OdxXhtml.impl;

import OdxXhtml.FLASHCLASSREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FLASHCLASSREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FLASHCLASSREFSImpl#getFLASHCLASSREF <em>FLASHCLASSREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FLASHCLASSREFSImpl extends MinimalEObjectImpl.Container implements FLASHCLASSREFS {
	/**
	 * The cached value of the '{@link #getFLASHCLASSREF() <em>FLASHCLASSREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFLASHCLASSREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> fLASHCLASSREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FLASHCLASSREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFLASHCLASSREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getFLASHCLASSREF() {
		if (fLASHCLASSREF == null) {
			fLASHCLASSREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.FLASHCLASSREFS__FLASHCLASSREF);
		}
		return fLASHCLASSREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSREFS__FLASHCLASSREF:
				return ((InternalEList<?>)getFLASHCLASSREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSREFS__FLASHCLASSREF:
				return getFLASHCLASSREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSREFS__FLASHCLASSREF:
				getFLASHCLASSREF().clear();
				getFLASHCLASSREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSREFS__FLASHCLASSREF:
				getFLASHCLASSREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSREFS__FLASHCLASSREF:
				return fLASHCLASSREF != null && !fLASHCLASSREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FLASHCLASSREFSImpl
