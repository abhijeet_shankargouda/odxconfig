/**
 */
package OdxXhtml.impl;

import OdxXhtml.ACCESSLEVELS;
import OdxXhtml.AUTMETHODS;
import OdxXhtml.BASEVARIANT;
import OdxXhtml.DIAGVARIABLES;
import OdxXhtml.DYNDEFINEDSPEC;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PARENTREFS;
import OdxXhtml.VARIABLEGROUPS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BASEVARIANT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.BASEVARIANTImpl#getDIAGVARIABLES <em>DIAGVARIABLES</em>}</li>
 *   <li>{@link OdxXhtml.impl.BASEVARIANTImpl#getVARIABLEGROUPS <em>VARIABLEGROUPS</em>}</li>
 *   <li>{@link OdxXhtml.impl.BASEVARIANTImpl#getAUTMETHODS <em>AUTMETHODS</em>}</li>
 *   <li>{@link OdxXhtml.impl.BASEVARIANTImpl#getDYNDEFINEDSPEC <em>DYNDEFINEDSPEC</em>}</li>
 *   <li>{@link OdxXhtml.impl.BASEVARIANTImpl#getACCESSLEVELS <em>ACCESSLEVELS</em>}</li>
 *   <li>{@link OdxXhtml.impl.BASEVARIANTImpl#getPARENTREFS <em>PARENTREFS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BASEVARIANTImpl extends HIERARCHYELEMENTImpl implements BASEVARIANT {
	/**
	 * The cached value of the '{@link #getDIAGVARIABLES() <em>DIAGVARIABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGVARIABLES()
	 * @generated
	 * @ordered
	 */
	protected DIAGVARIABLES dIAGVARIABLES;

	/**
	 * The cached value of the '{@link #getVARIABLEGROUPS() <em>VARIABLEGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVARIABLEGROUPS()
	 * @generated
	 * @ordered
	 */
	protected VARIABLEGROUPS vARIABLEGROUPS;

	/**
	 * The cached value of the '{@link #getAUTMETHODS() <em>AUTMETHODS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUTMETHODS()
	 * @generated
	 * @ordered
	 */
	protected AUTMETHODS aUTMETHODS;

	/**
	 * The cached value of the '{@link #getDYNDEFINEDSPEC() <em>DYNDEFINEDSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNDEFINEDSPEC()
	 * @generated
	 * @ordered
	 */
	protected DYNDEFINEDSPEC dYNDEFINEDSPEC;

	/**
	 * The cached value of the '{@link #getACCESSLEVELS() <em>ACCESSLEVELS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACCESSLEVELS()
	 * @generated
	 * @ordered
	 */
	protected ACCESSLEVELS aCCESSLEVELS;

	/**
	 * The cached value of the '{@link #getPARENTREFS() <em>PARENTREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARENTREFS()
	 * @generated
	 * @ordered
	 */
	protected PARENTREFS pARENTREFS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BASEVARIANTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getBASEVARIANT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGVARIABLES getDIAGVARIABLES() {
		return dIAGVARIABLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGVARIABLES(DIAGVARIABLES newDIAGVARIABLES, NotificationChain msgs) {
		DIAGVARIABLES oldDIAGVARIABLES = dIAGVARIABLES;
		dIAGVARIABLES = newDIAGVARIABLES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__DIAGVARIABLES, oldDIAGVARIABLES, newDIAGVARIABLES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGVARIABLES(DIAGVARIABLES newDIAGVARIABLES) {
		if (newDIAGVARIABLES != dIAGVARIABLES) {
			NotificationChain msgs = null;
			if (dIAGVARIABLES != null)
				msgs = ((InternalEObject)dIAGVARIABLES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__DIAGVARIABLES, null, msgs);
			if (newDIAGVARIABLES != null)
				msgs = ((InternalEObject)newDIAGVARIABLES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__DIAGVARIABLES, null, msgs);
			msgs = basicSetDIAGVARIABLES(newDIAGVARIABLES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__DIAGVARIABLES, newDIAGVARIABLES, newDIAGVARIABLES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VARIABLEGROUPS getVARIABLEGROUPS() {
		return vARIABLEGROUPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVARIABLEGROUPS(VARIABLEGROUPS newVARIABLEGROUPS, NotificationChain msgs) {
		VARIABLEGROUPS oldVARIABLEGROUPS = vARIABLEGROUPS;
		vARIABLEGROUPS = newVARIABLEGROUPS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__VARIABLEGROUPS, oldVARIABLEGROUPS, newVARIABLEGROUPS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVARIABLEGROUPS(VARIABLEGROUPS newVARIABLEGROUPS) {
		if (newVARIABLEGROUPS != vARIABLEGROUPS) {
			NotificationChain msgs = null;
			if (vARIABLEGROUPS != null)
				msgs = ((InternalEObject)vARIABLEGROUPS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__VARIABLEGROUPS, null, msgs);
			if (newVARIABLEGROUPS != null)
				msgs = ((InternalEObject)newVARIABLEGROUPS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__VARIABLEGROUPS, null, msgs);
			msgs = basicSetVARIABLEGROUPS(newVARIABLEGROUPS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__VARIABLEGROUPS, newVARIABLEGROUPS, newVARIABLEGROUPS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AUTMETHODS getAUTMETHODS() {
		return aUTMETHODS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAUTMETHODS(AUTMETHODS newAUTMETHODS, NotificationChain msgs) {
		AUTMETHODS oldAUTMETHODS = aUTMETHODS;
		aUTMETHODS = newAUTMETHODS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__AUTMETHODS, oldAUTMETHODS, newAUTMETHODS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAUTMETHODS(AUTMETHODS newAUTMETHODS) {
		if (newAUTMETHODS != aUTMETHODS) {
			NotificationChain msgs = null;
			if (aUTMETHODS != null)
				msgs = ((InternalEObject)aUTMETHODS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__AUTMETHODS, null, msgs);
			if (newAUTMETHODS != null)
				msgs = ((InternalEObject)newAUTMETHODS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__AUTMETHODS, null, msgs);
			msgs = basicSetAUTMETHODS(newAUTMETHODS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__AUTMETHODS, newAUTMETHODS, newAUTMETHODS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNDEFINEDSPEC getDYNDEFINEDSPEC() {
		return dYNDEFINEDSPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDYNDEFINEDSPEC(DYNDEFINEDSPEC newDYNDEFINEDSPEC, NotificationChain msgs) {
		DYNDEFINEDSPEC oldDYNDEFINEDSPEC = dYNDEFINEDSPEC;
		dYNDEFINEDSPEC = newDYNDEFINEDSPEC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__DYNDEFINEDSPEC, oldDYNDEFINEDSPEC, newDYNDEFINEDSPEC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDYNDEFINEDSPEC(DYNDEFINEDSPEC newDYNDEFINEDSPEC) {
		if (newDYNDEFINEDSPEC != dYNDEFINEDSPEC) {
			NotificationChain msgs = null;
			if (dYNDEFINEDSPEC != null)
				msgs = ((InternalEObject)dYNDEFINEDSPEC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__DYNDEFINEDSPEC, null, msgs);
			if (newDYNDEFINEDSPEC != null)
				msgs = ((InternalEObject)newDYNDEFINEDSPEC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__DYNDEFINEDSPEC, null, msgs);
			msgs = basicSetDYNDEFINEDSPEC(newDYNDEFINEDSPEC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__DYNDEFINEDSPEC, newDYNDEFINEDSPEC, newDYNDEFINEDSPEC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ACCESSLEVELS getACCESSLEVELS() {
		return aCCESSLEVELS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetACCESSLEVELS(ACCESSLEVELS newACCESSLEVELS, NotificationChain msgs) {
		ACCESSLEVELS oldACCESSLEVELS = aCCESSLEVELS;
		aCCESSLEVELS = newACCESSLEVELS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__ACCESSLEVELS, oldACCESSLEVELS, newACCESSLEVELS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setACCESSLEVELS(ACCESSLEVELS newACCESSLEVELS) {
		if (newACCESSLEVELS != aCCESSLEVELS) {
			NotificationChain msgs = null;
			if (aCCESSLEVELS != null)
				msgs = ((InternalEObject)aCCESSLEVELS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__ACCESSLEVELS, null, msgs);
			if (newACCESSLEVELS != null)
				msgs = ((InternalEObject)newACCESSLEVELS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__ACCESSLEVELS, null, msgs);
			msgs = basicSetACCESSLEVELS(newACCESSLEVELS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__ACCESSLEVELS, newACCESSLEVELS, newACCESSLEVELS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARENTREFS getPARENTREFS() {
		return pARENTREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPARENTREFS(PARENTREFS newPARENTREFS, NotificationChain msgs) {
		PARENTREFS oldPARENTREFS = pARENTREFS;
		pARENTREFS = newPARENTREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__PARENTREFS, oldPARENTREFS, newPARENTREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPARENTREFS(PARENTREFS newPARENTREFS) {
		if (newPARENTREFS != pARENTREFS) {
			NotificationChain msgs = null;
			if (pARENTREFS != null)
				msgs = ((InternalEObject)pARENTREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__PARENTREFS, null, msgs);
			if (newPARENTREFS != null)
				msgs = ((InternalEObject)newPARENTREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.BASEVARIANT__PARENTREFS, null, msgs);
			msgs = basicSetPARENTREFS(newPARENTREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.BASEVARIANT__PARENTREFS, newPARENTREFS, newPARENTREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANT__DIAGVARIABLES:
				return basicSetDIAGVARIABLES(null, msgs);
			case OdxXhtmlPackage.BASEVARIANT__VARIABLEGROUPS:
				return basicSetVARIABLEGROUPS(null, msgs);
			case OdxXhtmlPackage.BASEVARIANT__AUTMETHODS:
				return basicSetAUTMETHODS(null, msgs);
			case OdxXhtmlPackage.BASEVARIANT__DYNDEFINEDSPEC:
				return basicSetDYNDEFINEDSPEC(null, msgs);
			case OdxXhtmlPackage.BASEVARIANT__ACCESSLEVELS:
				return basicSetACCESSLEVELS(null, msgs);
			case OdxXhtmlPackage.BASEVARIANT__PARENTREFS:
				return basicSetPARENTREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANT__DIAGVARIABLES:
				return getDIAGVARIABLES();
			case OdxXhtmlPackage.BASEVARIANT__VARIABLEGROUPS:
				return getVARIABLEGROUPS();
			case OdxXhtmlPackage.BASEVARIANT__AUTMETHODS:
				return getAUTMETHODS();
			case OdxXhtmlPackage.BASEVARIANT__DYNDEFINEDSPEC:
				return getDYNDEFINEDSPEC();
			case OdxXhtmlPackage.BASEVARIANT__ACCESSLEVELS:
				return getACCESSLEVELS();
			case OdxXhtmlPackage.BASEVARIANT__PARENTREFS:
				return getPARENTREFS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANT__DIAGVARIABLES:
				setDIAGVARIABLES((DIAGVARIABLES)newValue);
				return;
			case OdxXhtmlPackage.BASEVARIANT__VARIABLEGROUPS:
				setVARIABLEGROUPS((VARIABLEGROUPS)newValue);
				return;
			case OdxXhtmlPackage.BASEVARIANT__AUTMETHODS:
				setAUTMETHODS((AUTMETHODS)newValue);
				return;
			case OdxXhtmlPackage.BASEVARIANT__DYNDEFINEDSPEC:
				setDYNDEFINEDSPEC((DYNDEFINEDSPEC)newValue);
				return;
			case OdxXhtmlPackage.BASEVARIANT__ACCESSLEVELS:
				setACCESSLEVELS((ACCESSLEVELS)newValue);
				return;
			case OdxXhtmlPackage.BASEVARIANT__PARENTREFS:
				setPARENTREFS((PARENTREFS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANT__DIAGVARIABLES:
				setDIAGVARIABLES((DIAGVARIABLES)null);
				return;
			case OdxXhtmlPackage.BASEVARIANT__VARIABLEGROUPS:
				setVARIABLEGROUPS((VARIABLEGROUPS)null);
				return;
			case OdxXhtmlPackage.BASEVARIANT__AUTMETHODS:
				setAUTMETHODS((AUTMETHODS)null);
				return;
			case OdxXhtmlPackage.BASEVARIANT__DYNDEFINEDSPEC:
				setDYNDEFINEDSPEC((DYNDEFINEDSPEC)null);
				return;
			case OdxXhtmlPackage.BASEVARIANT__ACCESSLEVELS:
				setACCESSLEVELS((ACCESSLEVELS)null);
				return;
			case OdxXhtmlPackage.BASEVARIANT__PARENTREFS:
				setPARENTREFS((PARENTREFS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANT__DIAGVARIABLES:
				return dIAGVARIABLES != null;
			case OdxXhtmlPackage.BASEVARIANT__VARIABLEGROUPS:
				return vARIABLEGROUPS != null;
			case OdxXhtmlPackage.BASEVARIANT__AUTMETHODS:
				return aUTMETHODS != null;
			case OdxXhtmlPackage.BASEVARIANT__DYNDEFINEDSPEC:
				return dYNDEFINEDSPEC != null;
			case OdxXhtmlPackage.BASEVARIANT__ACCESSLEVELS:
				return aCCESSLEVELS != null;
			case OdxXhtmlPackage.BASEVARIANT__PARENTREFS:
				return pARENTREFS != null;
		}
		return super.eIsSet(featureID);
	}

} //BASEVARIANTImpl
