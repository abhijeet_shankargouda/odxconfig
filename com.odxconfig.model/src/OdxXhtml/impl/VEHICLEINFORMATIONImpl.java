/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.ECUGROUPS;
import OdxXhtml.INFOCOMPONENTREFS;
import OdxXhtml.LOGICALLINKS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSICALVEHICLELINKS;
import OdxXhtml.TEXT;
import OdxXhtml.VEHICLECONNECTORS;
import OdxXhtml.VEHICLEINFORMATION;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VEHICLEINFORMATION</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONImpl#getINFOCOMPONENTREFS <em>INFOCOMPONENTREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONImpl#getVEHICLECONNECTORS <em>VEHICLECONNECTORS</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONImpl#getLOGICALLINKS <em>LOGICALLINKS</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONImpl#getECUGROUPS <em>ECUGROUPS</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONImpl#getPHYSICALVEHICLELINKS <em>PHYSICALVEHICLELINKS</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFORMATIONImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VEHICLEINFORMATIONImpl extends MinimalEObjectImpl.Container implements VEHICLEINFORMATION {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getINFOCOMPONENTREFS() <em>INFOCOMPONENTREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINFOCOMPONENTREFS()
	 * @generated
	 * @ordered
	 */
	protected INFOCOMPONENTREFS iNFOCOMPONENTREFS;

	/**
	 * The cached value of the '{@link #getVEHICLECONNECTORS() <em>VEHICLECONNECTORS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVEHICLECONNECTORS()
	 * @generated
	 * @ordered
	 */
	protected VEHICLECONNECTORS vEHICLECONNECTORS;

	/**
	 * The cached value of the '{@link #getLOGICALLINKS() <em>LOGICALLINKS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLOGICALLINKS()
	 * @generated
	 * @ordered
	 */
	protected LOGICALLINKS lOGICALLINKS;

	/**
	 * The cached value of the '{@link #getECUGROUPS() <em>ECUGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUGROUPS()
	 * @generated
	 * @ordered
	 */
	protected ECUGROUPS eCUGROUPS;

	/**
	 * The cached value of the '{@link #getPHYSICALVEHICLELINKS() <em>PHYSICALVEHICLELINKS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALVEHICLELINKS()
	 * @generated
	 * @ordered
	 */
	protected PHYSICALVEHICLELINKS pHYSICALVEHICLELINKS;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VEHICLEINFORMATIONImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVEHICLEINFORMATION();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INFOCOMPONENTREFS getINFOCOMPONENTREFS() {
		return iNFOCOMPONENTREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetINFOCOMPONENTREFS(INFOCOMPONENTREFS newINFOCOMPONENTREFS, NotificationChain msgs) {
		INFOCOMPONENTREFS oldINFOCOMPONENTREFS = iNFOCOMPONENTREFS;
		iNFOCOMPONENTREFS = newINFOCOMPONENTREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__INFOCOMPONENTREFS, oldINFOCOMPONENTREFS, newINFOCOMPONENTREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setINFOCOMPONENTREFS(INFOCOMPONENTREFS newINFOCOMPONENTREFS) {
		if (newINFOCOMPONENTREFS != iNFOCOMPONENTREFS) {
			NotificationChain msgs = null;
			if (iNFOCOMPONENTREFS != null)
				msgs = ((InternalEObject)iNFOCOMPONENTREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__INFOCOMPONENTREFS, null, msgs);
			if (newINFOCOMPONENTREFS != null)
				msgs = ((InternalEObject)newINFOCOMPONENTREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__INFOCOMPONENTREFS, null, msgs);
			msgs = basicSetINFOCOMPONENTREFS(newINFOCOMPONENTREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__INFOCOMPONENTREFS, newINFOCOMPONENTREFS, newINFOCOMPONENTREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLECONNECTORS getVEHICLECONNECTORS() {
		return vEHICLECONNECTORS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVEHICLECONNECTORS(VEHICLECONNECTORS newVEHICLECONNECTORS, NotificationChain msgs) {
		VEHICLECONNECTORS oldVEHICLECONNECTORS = vEHICLECONNECTORS;
		vEHICLECONNECTORS = newVEHICLECONNECTORS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__VEHICLECONNECTORS, oldVEHICLECONNECTORS, newVEHICLECONNECTORS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVEHICLECONNECTORS(VEHICLECONNECTORS newVEHICLECONNECTORS) {
		if (newVEHICLECONNECTORS != vEHICLECONNECTORS) {
			NotificationChain msgs = null;
			if (vEHICLECONNECTORS != null)
				msgs = ((InternalEObject)vEHICLECONNECTORS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__VEHICLECONNECTORS, null, msgs);
			if (newVEHICLECONNECTORS != null)
				msgs = ((InternalEObject)newVEHICLECONNECTORS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__VEHICLECONNECTORS, null, msgs);
			msgs = basicSetVEHICLECONNECTORS(newVEHICLECONNECTORS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__VEHICLECONNECTORS, newVEHICLECONNECTORS, newVEHICLECONNECTORS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LOGICALLINKS getLOGICALLINKS() {
		return lOGICALLINKS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLOGICALLINKS(LOGICALLINKS newLOGICALLINKS, NotificationChain msgs) {
		LOGICALLINKS oldLOGICALLINKS = lOGICALLINKS;
		lOGICALLINKS = newLOGICALLINKS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__LOGICALLINKS, oldLOGICALLINKS, newLOGICALLINKS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLOGICALLINKS(LOGICALLINKS newLOGICALLINKS) {
		if (newLOGICALLINKS != lOGICALLINKS) {
			NotificationChain msgs = null;
			if (lOGICALLINKS != null)
				msgs = ((InternalEObject)lOGICALLINKS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__LOGICALLINKS, null, msgs);
			if (newLOGICALLINKS != null)
				msgs = ((InternalEObject)newLOGICALLINKS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__LOGICALLINKS, null, msgs);
			msgs = basicSetLOGICALLINKS(newLOGICALLINKS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__LOGICALLINKS, newLOGICALLINKS, newLOGICALLINKS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUGROUPS getECUGROUPS() {
		return eCUGROUPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetECUGROUPS(ECUGROUPS newECUGROUPS, NotificationChain msgs) {
		ECUGROUPS oldECUGROUPS = eCUGROUPS;
		eCUGROUPS = newECUGROUPS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__ECUGROUPS, oldECUGROUPS, newECUGROUPS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setECUGROUPS(ECUGROUPS newECUGROUPS) {
		if (newECUGROUPS != eCUGROUPS) {
			NotificationChain msgs = null;
			if (eCUGROUPS != null)
				msgs = ((InternalEObject)eCUGROUPS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__ECUGROUPS, null, msgs);
			if (newECUGROUPS != null)
				msgs = ((InternalEObject)newECUGROUPS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__ECUGROUPS, null, msgs);
			msgs = basicSetECUGROUPS(newECUGROUPS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__ECUGROUPS, newECUGROUPS, newECUGROUPS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALVEHICLELINKS getPHYSICALVEHICLELINKS() {
		return pHYSICALVEHICLELINKS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPHYSICALVEHICLELINKS(PHYSICALVEHICLELINKS newPHYSICALVEHICLELINKS, NotificationChain msgs) {
		PHYSICALVEHICLELINKS oldPHYSICALVEHICLELINKS = pHYSICALVEHICLELINKS;
		pHYSICALVEHICLELINKS = newPHYSICALVEHICLELINKS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__PHYSICALVEHICLELINKS, oldPHYSICALVEHICLELINKS, newPHYSICALVEHICLELINKS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHYSICALVEHICLELINKS(PHYSICALVEHICLELINKS newPHYSICALVEHICLELINKS) {
		if (newPHYSICALVEHICLELINKS != pHYSICALVEHICLELINKS) {
			NotificationChain msgs = null;
			if (pHYSICALVEHICLELINKS != null)
				msgs = ((InternalEObject)pHYSICALVEHICLELINKS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__PHYSICALVEHICLELINKS, null, msgs);
			if (newPHYSICALVEHICLELINKS != null)
				msgs = ((InternalEObject)newPHYSICALVEHICLELINKS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFORMATION__PHYSICALVEHICLELINKS, null, msgs);
			msgs = basicSetPHYSICALVEHICLELINKS(newPHYSICALVEHICLELINKS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__PHYSICALVEHICLELINKS, newPHYSICALVEHICLELINKS, newPHYSICALVEHICLELINKS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFORMATION__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATION__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.VEHICLEINFORMATION__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.VEHICLEINFORMATION__INFOCOMPONENTREFS:
				return basicSetINFOCOMPONENTREFS(null, msgs);
			case OdxXhtmlPackage.VEHICLEINFORMATION__VEHICLECONNECTORS:
				return basicSetVEHICLECONNECTORS(null, msgs);
			case OdxXhtmlPackage.VEHICLEINFORMATION__LOGICALLINKS:
				return basicSetLOGICALLINKS(null, msgs);
			case OdxXhtmlPackage.VEHICLEINFORMATION__ECUGROUPS:
				return basicSetECUGROUPS(null, msgs);
			case OdxXhtmlPackage.VEHICLEINFORMATION__PHYSICALVEHICLELINKS:
				return basicSetPHYSICALVEHICLELINKS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATION__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.VEHICLEINFORMATION__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.VEHICLEINFORMATION__DESC:
				return getDESC();
			case OdxXhtmlPackage.VEHICLEINFORMATION__INFOCOMPONENTREFS:
				return getINFOCOMPONENTREFS();
			case OdxXhtmlPackage.VEHICLEINFORMATION__VEHICLECONNECTORS:
				return getVEHICLECONNECTORS();
			case OdxXhtmlPackage.VEHICLEINFORMATION__LOGICALLINKS:
				return getLOGICALLINKS();
			case OdxXhtmlPackage.VEHICLEINFORMATION__ECUGROUPS:
				return getECUGROUPS();
			case OdxXhtmlPackage.VEHICLEINFORMATION__PHYSICALVEHICLELINKS:
				return getPHYSICALVEHICLELINKS();
			case OdxXhtmlPackage.VEHICLEINFORMATION__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATION__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__INFOCOMPONENTREFS:
				setINFOCOMPONENTREFS((INFOCOMPONENTREFS)newValue);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__VEHICLECONNECTORS:
				setVEHICLECONNECTORS((VEHICLECONNECTORS)newValue);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__LOGICALLINKS:
				setLOGICALLINKS((LOGICALLINKS)newValue);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__ECUGROUPS:
				setECUGROUPS((ECUGROUPS)newValue);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__PHYSICALVEHICLELINKS:
				setPHYSICALVEHICLELINKS((PHYSICALVEHICLELINKS)newValue);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATION__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__INFOCOMPONENTREFS:
				setINFOCOMPONENTREFS((INFOCOMPONENTREFS)null);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__VEHICLECONNECTORS:
				setVEHICLECONNECTORS((VEHICLECONNECTORS)null);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__LOGICALLINKS:
				setLOGICALLINKS((LOGICALLINKS)null);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__ECUGROUPS:
				setECUGROUPS((ECUGROUPS)null);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__PHYSICALVEHICLELINKS:
				setPHYSICALVEHICLELINKS((PHYSICALVEHICLELINKS)null);
				return;
			case OdxXhtmlPackage.VEHICLEINFORMATION__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFORMATION__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.VEHICLEINFORMATION__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.VEHICLEINFORMATION__DESC:
				return dESC != null;
			case OdxXhtmlPackage.VEHICLEINFORMATION__INFOCOMPONENTREFS:
				return iNFOCOMPONENTREFS != null;
			case OdxXhtmlPackage.VEHICLEINFORMATION__VEHICLECONNECTORS:
				return vEHICLECONNECTORS != null;
			case OdxXhtmlPackage.VEHICLEINFORMATION__LOGICALLINKS:
				return lOGICALLINKS != null;
			case OdxXhtmlPackage.VEHICLEINFORMATION__ECUGROUPS:
				return eCUGROUPS != null;
			case OdxXhtmlPackage.VEHICLEINFORMATION__PHYSICALVEHICLELINKS:
				return pHYSICALVEHICLELINKS != null;
			case OdxXhtmlPackage.VEHICLEINFORMATION__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //VEHICLEINFORMATIONImpl
