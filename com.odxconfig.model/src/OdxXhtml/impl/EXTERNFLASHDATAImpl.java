/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATAFILE;
import OdxXhtml.EXTERNFLASHDATA;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EXTERNFLASHDATA</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.EXTERNFLASHDATAImpl#getDATAFILE <em>DATAFILE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EXTERNFLASHDATAImpl extends FLASHDATAImpl implements EXTERNFLASHDATA {
	/**
	 * The cached value of the '{@link #getDATAFILE() <em>DATAFILE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATAFILE()
	 * @generated
	 * @ordered
	 */
	protected DATAFILE dATAFILE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EXTERNFLASHDATAImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getEXTERNFLASHDATA();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATAFILE getDATAFILE() {
		return dATAFILE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDATAFILE(DATAFILE newDATAFILE, NotificationChain msgs) {
		DATAFILE oldDATAFILE = dATAFILE;
		dATAFILE = newDATAFILE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.EXTERNFLASHDATA__DATAFILE, oldDATAFILE, newDATAFILE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATAFILE(DATAFILE newDATAFILE) {
		if (newDATAFILE != dATAFILE) {
			NotificationChain msgs = null;
			if (dATAFILE != null)
				msgs = ((InternalEObject)dATAFILE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.EXTERNFLASHDATA__DATAFILE, null, msgs);
			if (newDATAFILE != null)
				msgs = ((InternalEObject)newDATAFILE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.EXTERNFLASHDATA__DATAFILE, null, msgs);
			msgs = basicSetDATAFILE(newDATAFILE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.EXTERNFLASHDATA__DATAFILE, newDATAFILE, newDATAFILE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.EXTERNFLASHDATA__DATAFILE:
				return basicSetDATAFILE(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.EXTERNFLASHDATA__DATAFILE:
				return getDATAFILE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.EXTERNFLASHDATA__DATAFILE:
				setDATAFILE((DATAFILE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.EXTERNFLASHDATA__DATAFILE:
				setDATAFILE((DATAFILE)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.EXTERNFLASHDATA__DATAFILE:
				return dATAFILE != null;
		}
		return super.eIsSet(featureID);
	}

} //EXTERNFLASHDATAImpl
