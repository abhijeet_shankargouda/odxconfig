/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.DIRECTION;
import OdxXhtml.FLASHCLASSREFS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SDGS;
import OdxXhtml.SESSIONDESC;
import OdxXhtml.SNREF;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SESSIONDESC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getPARTNUMBER <em>PARTNUMBER</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getPRIORITY <em>PRIORITY</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getSESSIONSNREF <em>SESSIONSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getFLASHCLASSREFS <em>FLASHCLASSREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getDIRECTION <em>DIRECTION</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONDESCImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SESSIONDESCImpl extends MinimalEObjectImpl.Container implements SESSIONDESC {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getPARTNUMBER() <em>PARTNUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARTNUMBER()
	 * @generated
	 * @ordered
	 */
	protected static final String PARTNUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPARTNUMBER() <em>PARTNUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARTNUMBER()
	 * @generated
	 * @ordered
	 */
	protected String pARTNUMBER = PARTNUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getPRIORITY() <em>PRIORITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPRIORITY()
	 * @generated
	 * @ordered
	 */
	protected static final long PRIORITY_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getPRIORITY() <em>PRIORITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPRIORITY()
	 * @generated
	 * @ordered
	 */
	protected long pRIORITY = PRIORITY_EDEFAULT;

	/**
	 * This is true if the PRIORITY attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean pRIORITYESet;

	/**
	 * The cached value of the '{@link #getSESSIONSNREF() <em>SESSIONSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSESSIONSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF sESSIONSNREF;

	/**
	 * The cached value of the '{@link #getDIAGCOMMSNREF() <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dIAGCOMMSNREF;

	/**
	 * The cached value of the '{@link #getFLASHCLASSREFS() <em>FLASHCLASSREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFLASHCLASSREFS()
	 * @generated
	 * @ordered
	 */
	protected FLASHCLASSREFS fLASHCLASSREFS;

	/**
	 * The cached value of the '{@link #getSDGS() <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGS()
	 * @generated
	 * @ordered
	 */
	protected SDGS sDGS;

	/**
	 * The default value of the '{@link #getDIRECTION() <em>DIRECTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIRECTION()
	 * @generated
	 * @ordered
	 */
	protected static final DIRECTION DIRECTION_EDEFAULT = DIRECTION.DOWNLOAD;

	/**
	 * The cached value of the '{@link #getDIRECTION() <em>DIRECTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIRECTION()
	 * @generated
	 * @ordered
	 */
	protected DIRECTION dIRECTION = DIRECTION_EDEFAULT;

	/**
	 * This is true if the DIRECTION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dIRECTIONESet;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SESSIONDESCImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSESSIONDESC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPARTNUMBER() {
		return pARTNUMBER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPARTNUMBER(String newPARTNUMBER) {
		String oldPARTNUMBER = pARTNUMBER;
		pARTNUMBER = newPARTNUMBER;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__PARTNUMBER, oldPARTNUMBER, pARTNUMBER));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getPRIORITY() {
		return pRIORITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPRIORITY(long newPRIORITY) {
		long oldPRIORITY = pRIORITY;
		pRIORITY = newPRIORITY;
		boolean oldPRIORITYESet = pRIORITYESet;
		pRIORITYESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__PRIORITY, oldPRIORITY, pRIORITY, !oldPRIORITYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetPRIORITY() {
		long oldPRIORITY = pRIORITY;
		boolean oldPRIORITYESet = pRIORITYESet;
		pRIORITY = PRIORITY_EDEFAULT;
		pRIORITYESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.SESSIONDESC__PRIORITY, oldPRIORITY, PRIORITY_EDEFAULT, oldPRIORITYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetPRIORITY() {
		return pRIORITYESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getSESSIONSNREF() {
		return sESSIONSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSESSIONSNREF(SNREF newSESSIONSNREF, NotificationChain msgs) {
		SNREF oldSESSIONSNREF = sESSIONSNREF;
		sESSIONSNREF = newSESSIONSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__SESSIONSNREF, oldSESSIONSNREF, newSESSIONSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSESSIONSNREF(SNREF newSESSIONSNREF) {
		if (newSESSIONSNREF != sESSIONSNREF) {
			NotificationChain msgs = null;
			if (sESSIONSNREF != null)
				msgs = ((InternalEObject)sESSIONSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__SESSIONSNREF, null, msgs);
			if (newSESSIONSNREF != null)
				msgs = ((InternalEObject)newSESSIONSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__SESSIONSNREF, null, msgs);
			msgs = basicSetSESSIONSNREF(newSESSIONSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__SESSIONSNREF, newSESSIONSNREF, newSESSIONSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDIAGCOMMSNREF() {
		return dIAGCOMMSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF, NotificationChain msgs) {
		SNREF oldDIAGCOMMSNREF = dIAGCOMMSNREF;
		dIAGCOMMSNREF = newDIAGCOMMSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__DIAGCOMMSNREF, oldDIAGCOMMSNREF, newDIAGCOMMSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF) {
		if (newDIAGCOMMSNREF != dIAGCOMMSNREF) {
			NotificationChain msgs = null;
			if (dIAGCOMMSNREF != null)
				msgs = ((InternalEObject)dIAGCOMMSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__DIAGCOMMSNREF, null, msgs);
			if (newDIAGCOMMSNREF != null)
				msgs = ((InternalEObject)newDIAGCOMMSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__DIAGCOMMSNREF, null, msgs);
			msgs = basicSetDIAGCOMMSNREF(newDIAGCOMMSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__DIAGCOMMSNREF, newDIAGCOMMSNREF, newDIAGCOMMSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASHCLASSREFS getFLASHCLASSREFS() {
		return fLASHCLASSREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFLASHCLASSREFS(FLASHCLASSREFS newFLASHCLASSREFS, NotificationChain msgs) {
		FLASHCLASSREFS oldFLASHCLASSREFS = fLASHCLASSREFS;
		fLASHCLASSREFS = newFLASHCLASSREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__FLASHCLASSREFS, oldFLASHCLASSREFS, newFLASHCLASSREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFLASHCLASSREFS(FLASHCLASSREFS newFLASHCLASSREFS) {
		if (newFLASHCLASSREFS != fLASHCLASSREFS) {
			NotificationChain msgs = null;
			if (fLASHCLASSREFS != null)
				msgs = ((InternalEObject)fLASHCLASSREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__FLASHCLASSREFS, null, msgs);
			if (newFLASHCLASSREFS != null)
				msgs = ((InternalEObject)newFLASHCLASSREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__FLASHCLASSREFS, null, msgs);
			msgs = basicSetFLASHCLASSREFS(newFLASHCLASSREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__FLASHCLASSREFS, newFLASHCLASSREFS, newFLASHCLASSREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS getSDGS() {
		return sDGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGS(SDGS newSDGS, NotificationChain msgs) {
		SDGS oldSDGS = sDGS;
		sDGS = newSDGS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__SDGS, oldSDGS, newSDGS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGS(SDGS newSDGS) {
		if (newSDGS != sDGS) {
			NotificationChain msgs = null;
			if (sDGS != null)
				msgs = ((InternalEObject)sDGS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__SDGS, null, msgs);
			if (newSDGS != null)
				msgs = ((InternalEObject)newSDGS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSIONDESC__SDGS, null, msgs);
			msgs = basicSetSDGS(newSDGS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__SDGS, newSDGS, newSDGS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIRECTION getDIRECTION() {
		return dIRECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIRECTION(DIRECTION newDIRECTION) {
		DIRECTION oldDIRECTION = dIRECTION;
		dIRECTION = newDIRECTION == null ? DIRECTION_EDEFAULT : newDIRECTION;
		boolean oldDIRECTIONESet = dIRECTIONESet;
		dIRECTIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__DIRECTION, oldDIRECTION, dIRECTION, !oldDIRECTIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDIRECTION() {
		DIRECTION oldDIRECTION = dIRECTION;
		boolean oldDIRECTIONESet = dIRECTIONESet;
		dIRECTION = DIRECTION_EDEFAULT;
		dIRECTIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.SESSIONDESC__DIRECTION, oldDIRECTION, DIRECTION_EDEFAULT, oldDIRECTIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDIRECTION() {
		return dIRECTIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSIONDESC__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESC__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.SESSIONDESC__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.SESSIONDESC__SESSIONSNREF:
				return basicSetSESSIONSNREF(null, msgs);
			case OdxXhtmlPackage.SESSIONDESC__DIAGCOMMSNREF:
				return basicSetDIAGCOMMSNREF(null, msgs);
			case OdxXhtmlPackage.SESSIONDESC__FLASHCLASSREFS:
				return basicSetFLASHCLASSREFS(null, msgs);
			case OdxXhtmlPackage.SESSIONDESC__SDGS:
				return basicSetSDGS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESC__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.SESSIONDESC__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.SESSIONDESC__DESC:
				return getDESC();
			case OdxXhtmlPackage.SESSIONDESC__PARTNUMBER:
				return getPARTNUMBER();
			case OdxXhtmlPackage.SESSIONDESC__PRIORITY:
				return getPRIORITY();
			case OdxXhtmlPackage.SESSIONDESC__SESSIONSNREF:
				return getSESSIONSNREF();
			case OdxXhtmlPackage.SESSIONDESC__DIAGCOMMSNREF:
				return getDIAGCOMMSNREF();
			case OdxXhtmlPackage.SESSIONDESC__FLASHCLASSREFS:
				return getFLASHCLASSREFS();
			case OdxXhtmlPackage.SESSIONDESC__SDGS:
				return getSDGS();
			case OdxXhtmlPackage.SESSIONDESC__DIRECTION:
				return getDIRECTION();
			case OdxXhtmlPackage.SESSIONDESC__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESC__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__PARTNUMBER:
				setPARTNUMBER((String)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__PRIORITY:
				setPRIORITY((Long)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__SESSIONSNREF:
				setSESSIONSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__FLASHCLASSREFS:
				setFLASHCLASSREFS((FLASHCLASSREFS)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__SDGS:
				setSDGS((SDGS)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__DIRECTION:
				setDIRECTION((DIRECTION)newValue);
				return;
			case OdxXhtmlPackage.SESSIONDESC__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESC__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.SESSIONDESC__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.SESSIONDESC__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.SESSIONDESC__PARTNUMBER:
				setPARTNUMBER(PARTNUMBER_EDEFAULT);
				return;
			case OdxXhtmlPackage.SESSIONDESC__PRIORITY:
				unsetPRIORITY();
				return;
			case OdxXhtmlPackage.SESSIONDESC__SESSIONSNREF:
				setSESSIONSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.SESSIONDESC__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.SESSIONDESC__FLASHCLASSREFS:
				setFLASHCLASSREFS((FLASHCLASSREFS)null);
				return;
			case OdxXhtmlPackage.SESSIONDESC__SDGS:
				setSDGS((SDGS)null);
				return;
			case OdxXhtmlPackage.SESSIONDESC__DIRECTION:
				unsetDIRECTION();
				return;
			case OdxXhtmlPackage.SESSIONDESC__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSIONDESC__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.SESSIONDESC__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.SESSIONDESC__DESC:
				return dESC != null;
			case OdxXhtmlPackage.SESSIONDESC__PARTNUMBER:
				return PARTNUMBER_EDEFAULT == null ? pARTNUMBER != null : !PARTNUMBER_EDEFAULT.equals(pARTNUMBER);
			case OdxXhtmlPackage.SESSIONDESC__PRIORITY:
				return isSetPRIORITY();
			case OdxXhtmlPackage.SESSIONDESC__SESSIONSNREF:
				return sESSIONSNREF != null;
			case OdxXhtmlPackage.SESSIONDESC__DIAGCOMMSNREF:
				return dIAGCOMMSNREF != null;
			case OdxXhtmlPackage.SESSIONDESC__FLASHCLASSREFS:
				return fLASHCLASSREFS != null;
			case OdxXhtmlPackage.SESSIONDESC__SDGS:
				return sDGS != null;
			case OdxXhtmlPackage.SESSIONDESC__DIRECTION:
				return isSetDIRECTION();
			case OdxXhtmlPackage.SESSIONDESC__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", pARTNUMBER: ");
		result.append(pARTNUMBER);
		result.append(", pRIORITY: ");
		if (pRIORITYESet) result.append(pRIORITY); else result.append("<unset>");
		result.append(", dIRECTION: ");
		if (dIRECTIONESet) result.append(dIRECTION); else result.append("<unset>");
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //SESSIONDESCImpl
