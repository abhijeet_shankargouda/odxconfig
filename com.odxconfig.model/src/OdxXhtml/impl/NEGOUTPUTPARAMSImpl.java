/**
 */
package OdxXhtml.impl;

import OdxXhtml.NEGOUTPUTPARAM;
import OdxXhtml.NEGOUTPUTPARAMS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NEGOUTPUTPARAMS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.NEGOUTPUTPARAMSImpl#getNEGOUTPUTPARAM <em>NEGOUTPUTPARAM</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NEGOUTPUTPARAMSImpl extends MinimalEObjectImpl.Container implements NEGOUTPUTPARAMS {
	/**
	 * The cached value of the '{@link #getNEGOUTPUTPARAM() <em>NEGOUTPUTPARAM</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNEGOUTPUTPARAM()
	 * @generated
	 * @ordered
	 */
	protected EList<NEGOUTPUTPARAM> nEGOUTPUTPARAM;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NEGOUTPUTPARAMSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getNEGOUTPUTPARAMS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<NEGOUTPUTPARAM> getNEGOUTPUTPARAM() {
		if (nEGOUTPUTPARAM == null) {
			nEGOUTPUTPARAM = new EObjectContainmentEList<NEGOUTPUTPARAM>(NEGOUTPUTPARAM.class, this, OdxXhtmlPackage.NEGOUTPUTPARAMS__NEGOUTPUTPARAM);
		}
		return nEGOUTPUTPARAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGOUTPUTPARAMS__NEGOUTPUTPARAM:
				return ((InternalEList<?>)getNEGOUTPUTPARAM()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGOUTPUTPARAMS__NEGOUTPUTPARAM:
				return getNEGOUTPUTPARAM();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGOUTPUTPARAMS__NEGOUTPUTPARAM:
				getNEGOUTPUTPARAM().clear();
				getNEGOUTPUTPARAM().addAll((Collection<? extends NEGOUTPUTPARAM>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGOUTPUTPARAMS__NEGOUTPUTPARAM:
				getNEGOUTPUTPARAM().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGOUTPUTPARAMS__NEGOUTPUTPARAM:
				return nEGOUTPUTPARAM != null && !nEGOUTPUTPARAM.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //NEGOUTPUTPARAMSImpl
