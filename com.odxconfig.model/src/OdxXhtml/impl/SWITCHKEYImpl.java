/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SWITCHKEY;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SWITCHKEY</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SWITCHKEYImpl#getBYTEPOSITION <em>BYTEPOSITION</em>}</li>
 *   <li>{@link OdxXhtml.impl.SWITCHKEYImpl#getBITPOSITION <em>BITPOSITION</em>}</li>
 *   <li>{@link OdxXhtml.impl.SWITCHKEYImpl#getDATAOBJECTPROPREF <em>DATAOBJECTPROPREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SWITCHKEYImpl extends MinimalEObjectImpl.Container implements SWITCHKEY {
	/**
	 * The default value of the '{@link #getBYTEPOSITION() <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTEPOSITION()
	 * @generated
	 * @ordered
	 */
	protected static final long BYTEPOSITION_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBYTEPOSITION() <em>BYTEPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTEPOSITION()
	 * @generated
	 * @ordered
	 */
	protected long bYTEPOSITION = BYTEPOSITION_EDEFAULT;

	/**
	 * This is true if the BYTEPOSITION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bYTEPOSITIONESet;

	/**
	 * The default value of the '{@link #getBITPOSITION() <em>BITPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITPOSITION()
	 * @generated
	 * @ordered
	 */
	protected static final long BITPOSITION_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBITPOSITION() <em>BITPOSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITPOSITION()
	 * @generated
	 * @ordered
	 */
	protected long bITPOSITION = BITPOSITION_EDEFAULT;

	/**
	 * This is true if the BITPOSITION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bITPOSITIONESet;

	/**
	 * The cached value of the '{@link #getDATAOBJECTPROPREF() <em>DATAOBJECTPROPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATAOBJECTPROPREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK dATAOBJECTPROPREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SWITCHKEYImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSWITCHKEY();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBYTEPOSITION() {
		return bYTEPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBYTEPOSITION(long newBYTEPOSITION) {
		long oldBYTEPOSITION = bYTEPOSITION;
		bYTEPOSITION = newBYTEPOSITION;
		boolean oldBYTEPOSITIONESet = bYTEPOSITIONESet;
		bYTEPOSITIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SWITCHKEY__BYTEPOSITION, oldBYTEPOSITION, bYTEPOSITION, !oldBYTEPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBYTEPOSITION() {
		long oldBYTEPOSITION = bYTEPOSITION;
		boolean oldBYTEPOSITIONESet = bYTEPOSITIONESet;
		bYTEPOSITION = BYTEPOSITION_EDEFAULT;
		bYTEPOSITIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.SWITCHKEY__BYTEPOSITION, oldBYTEPOSITION, BYTEPOSITION_EDEFAULT, oldBYTEPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBYTEPOSITION() {
		return bYTEPOSITIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBITPOSITION() {
		return bITPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBITPOSITION(long newBITPOSITION) {
		long oldBITPOSITION = bITPOSITION;
		bITPOSITION = newBITPOSITION;
		boolean oldBITPOSITIONESet = bITPOSITIONESet;
		bITPOSITIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SWITCHKEY__BITPOSITION, oldBITPOSITION, bITPOSITION, !oldBITPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBITPOSITION() {
		long oldBITPOSITION = bITPOSITION;
		boolean oldBITPOSITIONESet = bITPOSITIONESet;
		bITPOSITION = BITPOSITION_EDEFAULT;
		bITPOSITIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.SWITCHKEY__BITPOSITION, oldBITPOSITION, BITPOSITION_EDEFAULT, oldBITPOSITIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBITPOSITION() {
		return bITPOSITIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getDATAOBJECTPROPREF() {
		return dATAOBJECTPROPREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDATAOBJECTPROPREF(ODXLINK newDATAOBJECTPROPREF, NotificationChain msgs) {
		ODXLINK oldDATAOBJECTPROPREF = dATAOBJECTPROPREF;
		dATAOBJECTPROPREF = newDATAOBJECTPROPREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SWITCHKEY__DATAOBJECTPROPREF, oldDATAOBJECTPROPREF, newDATAOBJECTPROPREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATAOBJECTPROPREF(ODXLINK newDATAOBJECTPROPREF) {
		if (newDATAOBJECTPROPREF != dATAOBJECTPROPREF) {
			NotificationChain msgs = null;
			if (dATAOBJECTPROPREF != null)
				msgs = ((InternalEObject)dATAOBJECTPROPREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SWITCHKEY__DATAOBJECTPROPREF, null, msgs);
			if (newDATAOBJECTPROPREF != null)
				msgs = ((InternalEObject)newDATAOBJECTPROPREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SWITCHKEY__DATAOBJECTPROPREF, null, msgs);
			msgs = basicSetDATAOBJECTPROPREF(newDATAOBJECTPROPREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SWITCHKEY__DATAOBJECTPROPREF, newDATAOBJECTPROPREF, newDATAOBJECTPROPREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SWITCHKEY__DATAOBJECTPROPREF:
				return basicSetDATAOBJECTPROPREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SWITCHKEY__BYTEPOSITION:
				return getBYTEPOSITION();
			case OdxXhtmlPackage.SWITCHKEY__BITPOSITION:
				return getBITPOSITION();
			case OdxXhtmlPackage.SWITCHKEY__DATAOBJECTPROPREF:
				return getDATAOBJECTPROPREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SWITCHKEY__BYTEPOSITION:
				setBYTEPOSITION((Long)newValue);
				return;
			case OdxXhtmlPackage.SWITCHKEY__BITPOSITION:
				setBITPOSITION((Long)newValue);
				return;
			case OdxXhtmlPackage.SWITCHKEY__DATAOBJECTPROPREF:
				setDATAOBJECTPROPREF((ODXLINK)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SWITCHKEY__BYTEPOSITION:
				unsetBYTEPOSITION();
				return;
			case OdxXhtmlPackage.SWITCHKEY__BITPOSITION:
				unsetBITPOSITION();
				return;
			case OdxXhtmlPackage.SWITCHKEY__DATAOBJECTPROPREF:
				setDATAOBJECTPROPREF((ODXLINK)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SWITCHKEY__BYTEPOSITION:
				return isSetBYTEPOSITION();
			case OdxXhtmlPackage.SWITCHKEY__BITPOSITION:
				return isSetBITPOSITION();
			case OdxXhtmlPackage.SWITCHKEY__DATAOBJECTPROPREF:
				return dATAOBJECTPROPREF != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bYTEPOSITION: ");
		if (bYTEPOSITIONESet) result.append(bYTEPOSITION); else result.append("<unset>");
		result.append(", bITPOSITION: ");
		if (bITPOSITIONESet) result.append(bITPOSITION); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //SWITCHKEYImpl
