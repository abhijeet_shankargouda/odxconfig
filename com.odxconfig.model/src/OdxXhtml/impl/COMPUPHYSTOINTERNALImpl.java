/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPUPHYSTOINTERNAL;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROGCODE;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPUPHYSTOINTERNAL</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPUPHYSTOINTERNALImpl#getPROGCODE <em>PROGCODE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPUPHYSTOINTERNALImpl extends MinimalEObjectImpl.Container implements COMPUPHYSTOINTERNAL {
	/**
	 * The cached value of the '{@link #getPROGCODE() <em>PROGCODE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGCODE()
	 * @generated
	 * @ordered
	 */
	protected PROGCODE pROGCODE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPUPHYSTOINTERNALImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPUPHYSTOINTERNAL();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROGCODE getPROGCODE() {
		return pROGCODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROGCODE(PROGCODE newPROGCODE, NotificationChain msgs) {
		PROGCODE oldPROGCODE = pROGCODE;
		pROGCODE = newPROGCODE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUPHYSTOINTERNAL__PROGCODE, oldPROGCODE, newPROGCODE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROGCODE(PROGCODE newPROGCODE) {
		if (newPROGCODE != pROGCODE) {
			NotificationChain msgs = null;
			if (pROGCODE != null)
				msgs = ((InternalEObject)pROGCODE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUPHYSTOINTERNAL__PROGCODE, null, msgs);
			if (newPROGCODE != null)
				msgs = ((InternalEObject)newPROGCODE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUPHYSTOINTERNAL__PROGCODE, null, msgs);
			msgs = basicSetPROGCODE(newPROGCODE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUPHYSTOINTERNAL__PROGCODE, newPROGCODE, newPROGCODE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUPHYSTOINTERNAL__PROGCODE:
				return basicSetPROGCODE(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUPHYSTOINTERNAL__PROGCODE:
				return getPROGCODE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUPHYSTOINTERNAL__PROGCODE:
				setPROGCODE((PROGCODE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUPHYSTOINTERNAL__PROGCODE:
				setPROGCODE((PROGCODE)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUPHYSTOINTERNAL__PROGCODE:
				return pROGCODE != null;
		}
		return super.eIsSet(featureID);
	}

} //COMPUPHYSTOINTERNALImpl
