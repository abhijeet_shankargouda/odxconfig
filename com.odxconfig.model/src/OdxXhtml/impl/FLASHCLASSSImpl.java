/**
 */
package OdxXhtml.impl;

import OdxXhtml.FLASHCLASS;
import OdxXhtml.FLASHCLASSS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FLASHCLASSS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FLASHCLASSSImpl#getFLASHCLASS <em>FLASHCLASS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FLASHCLASSSImpl extends MinimalEObjectImpl.Container implements FLASHCLASSS {
	/**
	 * The cached value of the '{@link #getFLASHCLASS() <em>FLASHCLASS</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFLASHCLASS()
	 * @generated
	 * @ordered
	 */
	protected EList<FLASHCLASS> fLASHCLASS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FLASHCLASSSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFLASHCLASSS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FLASHCLASS> getFLASHCLASS() {
		if (fLASHCLASS == null) {
			fLASHCLASS = new EObjectContainmentEList<FLASHCLASS>(FLASHCLASS.class, this, OdxXhtmlPackage.FLASHCLASSS__FLASHCLASS);
		}
		return fLASHCLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSS__FLASHCLASS:
				return ((InternalEList<?>)getFLASHCLASS()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSS__FLASHCLASS:
				return getFLASHCLASS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSS__FLASHCLASS:
				getFLASHCLASS().clear();
				getFLASHCLASS().addAll((Collection<? extends FLASHCLASS>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSS__FLASHCLASS:
				getFLASHCLASS().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHCLASSS__FLASHCLASS:
				return fLASHCLASS != null && !fLASHCLASS.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FLASHCLASSSImpl
