/**
 */
package OdxXhtml.impl;

import OdxXhtml.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OdxXhtmlFactoryImpl extends EFactoryImpl implements OdxXhtmlFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OdxXhtmlFactory init() {
		try {
			OdxXhtmlFactory theOdxXhtmlFactory = (OdxXhtmlFactory)EPackage.Registry.INSTANCE.getEFactory(OdxXhtmlPackage.eNS_URI);
			if (theOdxXhtmlFactory != null) {
				return theOdxXhtmlFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OdxXhtmlFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OdxXhtmlFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OdxXhtmlPackage.ABLOCK: return createABLOCK();
			case OdxXhtmlPackage.ABLOCKS_TYPE: return createABLOCKSType();
			case OdxXhtmlPackage.ACCESSLEVEL: return createACCESSLEVEL();
			case OdxXhtmlPackage.ACCESSLEVELS: return createACCESSLEVELS();
			case OdxXhtmlPackage.ADDRDEFFILTER: return createADDRDEFFILTER();
			case OdxXhtmlPackage.ADDRDEFPHYSSEGMENT: return createADDRDEFPHYSSEGMENT();
			case OdxXhtmlPackage.ADMINDATA: return createADMINDATA();
			case OdxXhtmlPackage.ADMINDATA1: return createADMINDATA1();
			case OdxXhtmlPackage.ALLVALUE: return createALLVALUE();
			case OdxXhtmlPackage.AUDIENCE: return createAUDIENCE();
			case OdxXhtmlPackage.AUTMETHOD: return createAUTMETHOD();
			case OdxXhtmlPackage.AUTMETHODS: return createAUTMETHODS();
			case OdxXhtmlPackage.BASEVARIANT: return createBASEVARIANT();
			case OdxXhtmlPackage.BASEVARIANTREF: return createBASEVARIANTREF();
			case OdxXhtmlPackage.BASEVARIANTS: return createBASEVARIANTS();
			case OdxXhtmlPackage.BASICSTRUCTURE: return createBASICSTRUCTURE();
			case OdxXhtmlPackage.BLOCK: return createBlock();
			case OdxXhtmlPackage.BTYPE: return createBType();
			case OdxXhtmlPackage.CASE: return createCASE();
			case OdxXhtmlPackage.CASES: return createCASES();
			case OdxXhtmlPackage.CATALOG: return createCATALOG();
			case OdxXhtmlPackage.CHECKSUM: return createCHECKSUM();
			case OdxXhtmlPackage.CHECKSUMRESULT: return createCHECKSUMRESULT();
			case OdxXhtmlPackage.CHECKSUMS: return createCHECKSUMS();
			case OdxXhtmlPackage.CODEDCONST: return createCODEDCONST();
			case OdxXhtmlPackage.COMMRELATION: return createCOMMRELATION();
			case OdxXhtmlPackage.COMMRELATIONS: return createCOMMRELATIONS();
			case OdxXhtmlPackage.COMPANYDATA: return createCOMPANYDATA();
			case OdxXhtmlPackage.COMPANYDATA1: return createCOMPANYDATA1();
			case OdxXhtmlPackage.COMPANYDATAS: return createCOMPANYDATAS();
			case OdxXhtmlPackage.COMPANYDATAS_TYPE: return createCOMPANYDATASType();
			case OdxXhtmlPackage.COMPANYDOCINFO: return createCOMPANYDOCINFO();
			case OdxXhtmlPackage.COMPANYDOCINFO1: return createCOMPANYDOCINFO1();
			case OdxXhtmlPackage.COMPANYDOCINFOS: return createCOMPANYDOCINFOS();
			case OdxXhtmlPackage.COMPANYDOCINFOS1: return createCOMPANYDOCINFOS1();
			case OdxXhtmlPackage.COMPANYREVISIONINFO: return createCOMPANYREVISIONINFO();
			case OdxXhtmlPackage.COMPANYREVISIONINFO1: return createCOMPANYREVISIONINFO1();
			case OdxXhtmlPackage.COMPANYREVISIONINFOS: return createCOMPANYREVISIONINFOS();
			case OdxXhtmlPackage.COMPANYREVISIONINFOS1: return createCOMPANYREVISIONINFOS1();
			case OdxXhtmlPackage.COMPANYSPECIFICINFO: return createCOMPANYSPECIFICINFO();
			case OdxXhtmlPackage.COMPANYSPECIFICINFO1: return createCOMPANYSPECIFICINFO1();
			case OdxXhtmlPackage.COMPARAM: return createCOMPARAM();
			case OdxXhtmlPackage.COMPARAMREF: return createCOMPARAMREF();
			case OdxXhtmlPackage.COMPARAMREFS: return createCOMPARAMREFS();
			case OdxXhtmlPackage.COMPARAMS: return createCOMPARAMS();
			case OdxXhtmlPackage.COMPARAMSPEC: return createCOMPARAMSPEC();
			case OdxXhtmlPackage.COMPLEXDOP: return createCOMPLEXDOP();
			case OdxXhtmlPackage.COMPUCONST: return createCOMPUCONST();
			case OdxXhtmlPackage.COMPUDEFAULTVALUE: return createCOMPUDEFAULTVALUE();
			case OdxXhtmlPackage.COMPUDENOMINATOR: return createCOMPUDENOMINATOR();
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS: return createCOMPUINTERNALTOPHYS();
			case OdxXhtmlPackage.COMPUINVERSEVALUE: return createCOMPUINVERSEVALUE();
			case OdxXhtmlPackage.COMPUMETHOD: return createCOMPUMETHOD();
			case OdxXhtmlPackage.COMPUNUMERATOR: return createCOMPUNUMERATOR();
			case OdxXhtmlPackage.COMPUPHYSTOINTERNAL: return createCOMPUPHYSTOINTERNAL();
			case OdxXhtmlPackage.COMPURATIONALCOEFFS: return createCOMPURATIONALCOEFFS();
			case OdxXhtmlPackage.COMPUSCALE: return createCOMPUSCALE();
			case OdxXhtmlPackage.COMPUSCALES: return createCOMPUSCALES();
			case OdxXhtmlPackage.DATABLOCK: return createDATABLOCK();
			case OdxXhtmlPackage.DATABLOCKREFS: return createDATABLOCKREFS();
			case OdxXhtmlPackage.DATABLOCKS: return createDATABLOCKS();
			case OdxXhtmlPackage.DATAFILE: return createDATAFILE();
			case OdxXhtmlPackage.DATAFORMAT: return createDATAFORMAT();
			case OdxXhtmlPackage.DATAOBJECTPROP: return createDATAOBJECTPROP();
			case OdxXhtmlPackage.DATAOBJECTPROPS: return createDATAOBJECTPROPS();
			case OdxXhtmlPackage.DEFAULTCASE: return createDEFAULTCASE();
			case OdxXhtmlPackage.DESCRIPTION: return createDESCRIPTION();
			case OdxXhtmlPackage.DESCRIPTION1: return createDESCRIPTION1();
			case OdxXhtmlPackage.DETERMINENUMBEROFITEMS: return createDETERMINENUMBEROFITEMS();
			case OdxXhtmlPackage.DIAGCODEDTYPE: return createDIAGCODEDTYPE();
			case OdxXhtmlPackage.DIAGCOMM: return createDIAGCOMM();
			case OdxXhtmlPackage.DIAGCOMMS: return createDIAGCOMMS();
			case OdxXhtmlPackage.DIAGDATADICTIONARYSPEC: return createDIAGDATADICTIONARYSPEC();
			case OdxXhtmlPackage.DIAGLAYER: return createDIAGLAYER();
			case OdxXhtmlPackage.DIAGLAYERCONTAINER: return createDIAGLAYERCONTAINER();
			case OdxXhtmlPackage.DIAGLAYERREFS: return createDIAGLAYERREFS();
			case OdxXhtmlPackage.DIAGSERVICE: return createDIAGSERVICE();
			case OdxXhtmlPackage.DIAGVARIABLE: return createDIAGVARIABLE();
			case OdxXhtmlPackage.DIAGVARIABLES: return createDIAGVARIABLES();
			case OdxXhtmlPackage.DOCREVISION: return createDOCREVISION();
			case OdxXhtmlPackage.DOCREVISION1: return createDOCREVISION1();
			case OdxXhtmlPackage.DOCREVISIONS: return createDOCREVISIONS();
			case OdxXhtmlPackage.DOCREVISIONS1: return createDOCREVISIONS1();
			case OdxXhtmlPackage.DOCUMENT_ROOT: return createDocumentRoot();
			case OdxXhtmlPackage.DOPBASE: return createDOPBASE();
			case OdxXhtmlPackage.DTC: return createDTC();
			case OdxXhtmlPackage.DTCDOP: return createDTCDOP();
			case OdxXhtmlPackage.DTCDOPS: return createDTCDOPS();
			case OdxXhtmlPackage.DTCS: return createDTCS();
			case OdxXhtmlPackage.DTCVALUE: return createDTCVALUE();
			case OdxXhtmlPackage.DTCVALUES: return createDTCVALUES();
			case OdxXhtmlPackage.DYNAMIC: return createDYNAMIC();
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELD: return createDYNAMICENDMARKERFIELD();
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELDS: return createDYNAMICENDMARKERFIELDS();
			case OdxXhtmlPackage.DYNAMICLENGTHFIELD: return createDYNAMICLENGTHFIELD();
			case OdxXhtmlPackage.DYNAMICLENGTHFIELDS: return createDYNAMICLENGTHFIELDS();
			case OdxXhtmlPackage.DYNDEFINEDSPEC: return createDYNDEFINEDSPEC();
			case OdxXhtmlPackage.DYNENDDOPREF: return createDYNENDDOPREF();
			case OdxXhtmlPackage.DYNIDDEFMODEINFO: return createDYNIDDEFMODEINFO();
			case OdxXhtmlPackage.DYNIDDEFMODEINFOS: return createDYNIDDEFMODEINFOS();
			case OdxXhtmlPackage.ECUGROUP: return createECUGROUP();
			case OdxXhtmlPackage.ECUGROUPS: return createECUGROUPS();
			case OdxXhtmlPackage.ECUMEM: return createECUMEM();
			case OdxXhtmlPackage.ECUMEMCONNECTOR: return createECUMEMCONNECTOR();
			case OdxXhtmlPackage.ECUMEMCONNECTORS: return createECUMEMCONNECTORS();
			case OdxXhtmlPackage.ECUMEMS: return createECUMEMS();
			case OdxXhtmlPackage.ECUPROXY: return createECUPROXY();
			case OdxXhtmlPackage.ECUPROXYREFS: return createECUPROXYREFS();
			case OdxXhtmlPackage.ECUSHAREDDATA: return createECUSHAREDDATA();
			case OdxXhtmlPackage.ECUSHAREDDATAREF: return createECUSHAREDDATAREF();
			case OdxXhtmlPackage.ECUSHAREDDATAS: return createECUSHAREDDATAS();
			case OdxXhtmlPackage.ECUVARIANT: return createECUVARIANT();
			case OdxXhtmlPackage.ECUVARIANTPATTERN: return createECUVARIANTPATTERN();
			case OdxXhtmlPackage.ECUVARIANTPATTERNS: return createECUVARIANTPATTERNS();
			case OdxXhtmlPackage.ECUVARIANTS: return createECUVARIANTS();
			case OdxXhtmlPackage.ENCRYPTCOMPRESSMETHOD: return createENCRYPTCOMPRESSMETHOD();
			case OdxXhtmlPackage.ENDOFPDUFIELD: return createENDOFPDUFIELD();
			case OdxXhtmlPackage.ENDOFPDUFIELDS: return createENDOFPDUFIELDS();
			case OdxXhtmlPackage.ENVDATA: return createENVDATA();
			case OdxXhtmlPackage.ENVDATADESC: return createENVDATADESC();
			case OdxXhtmlPackage.ENVDATADESCS: return createENVDATADESCS();
			case OdxXhtmlPackage.ENVDATAS: return createENVDATAS();
			case OdxXhtmlPackage.EXPECTEDIDENT: return createEXPECTEDIDENT();
			case OdxXhtmlPackage.EXPECTEDIDENTS: return createEXPECTEDIDENTS();
			case OdxXhtmlPackage.EXTERNALACCESSMETHOD: return createEXTERNALACCESSMETHOD();
			case OdxXhtmlPackage.EXTERNFLASHDATA: return createEXTERNFLASHDATA();
			case OdxXhtmlPackage.FIELD: return createFIELD();
			case OdxXhtmlPackage.FILE: return createFILE();
			case OdxXhtmlPackage.FILES_TYPE: return createFILESType();
			case OdxXhtmlPackage.FILTER: return createFILTER();
			case OdxXhtmlPackage.FILTERS: return createFILTERS();
			case OdxXhtmlPackage.FLASH: return createFLASH();
			case OdxXhtmlPackage.FLASHCLASS: return createFLASHCLASS();
			case OdxXhtmlPackage.FLASHCLASSREFS: return createFLASHCLASSREFS();
			case OdxXhtmlPackage.FLASHCLASSS: return createFLASHCLASSS();
			case OdxXhtmlPackage.FLASHDATA: return createFLASHDATA();
			case OdxXhtmlPackage.FLASHDATAS: return createFLASHDATAS();
			case OdxXhtmlPackage.FLOW: return createFlow();
			case OdxXhtmlPackage.FUNCTCLASS: return createFUNCTCLASS();
			case OdxXhtmlPackage.FUNCTCLASSREFS: return createFUNCTCLASSREFS();
			case OdxXhtmlPackage.FUNCTCLASSS: return createFUNCTCLASSS();
			case OdxXhtmlPackage.FUNCTIONALGROUP: return createFUNCTIONALGROUP();
			case OdxXhtmlPackage.FUNCTIONALGROUPREF: return createFUNCTIONALGROUPREF();
			case OdxXhtmlPackage.FUNCTIONALGROUPS: return createFUNCTIONALGROUPS();
			case OdxXhtmlPackage.FWCHECKSUM: return createFWCHECKSUM();
			case OdxXhtmlPackage.FWSIGNATURE: return createFWSIGNATURE();
			case OdxXhtmlPackage.GATEWAYLOGICALLINK: return createGATEWAYLOGICALLINK();
			case OdxXhtmlPackage.GATEWAYLOGICALLINKREFS: return createGATEWAYLOGICALLINKREFS();
			case OdxXhtmlPackage.GLOBALNEGRESPONSE: return createGLOBALNEGRESPONSE();
			case OdxXhtmlPackage.GLOBALNEGRESPONSES: return createGLOBALNEGRESPONSES();
			case OdxXhtmlPackage.HIERARCHYELEMENT: return createHIERARCHYELEMENT();
			case OdxXhtmlPackage.IDENTDESC: return createIDENTDESC();
			case OdxXhtmlPackage.IDENTDESCS: return createIDENTDESCS();
			case OdxXhtmlPackage.IDENTVALUE: return createIDENTVALUE();
			case OdxXhtmlPackage.IDENTVALUES: return createIDENTVALUES();
			case OdxXhtmlPackage.IMPORTREFS: return createIMPORTREFS();
			case OdxXhtmlPackage.INFOCOMPONENT: return createINFOCOMPONENT();
			case OdxXhtmlPackage.INFOCOMPONENTREFS: return createINFOCOMPONENTREFS();
			case OdxXhtmlPackage.INFOCOMPONENTS: return createINFOCOMPONENTS();
			case OdxXhtmlPackage.INLINE: return createInline();
			case OdxXhtmlPackage.INPUTPARAM: return createINPUTPARAM();
			case OdxXhtmlPackage.INPUTPARAMS: return createINPUTPARAMS();
			case OdxXhtmlPackage.INTERNALCONSTR: return createINTERNALCONSTR();
			case OdxXhtmlPackage.INTERNFLASHDATA: return createINTERNFLASHDATA();
			case OdxXhtmlPackage.ITYPE: return createIType();
			case OdxXhtmlPackage.LAYERREFS: return createLAYERREFS();
			case OdxXhtmlPackage.LEADINGLENGTHINFOTYPE: return createLEADINGLENGTHINFOTYPE();
			case OdxXhtmlPackage.LENGTHDESCRIPTOR: return createLENGTHDESCRIPTOR();
			case OdxXhtmlPackage.LENGTHKEY: return createLENGTHKEY();
			case OdxXhtmlPackage.LIMIT: return createLIMIT();
			case OdxXhtmlPackage.LINKCOMPARAMREF: return createLINKCOMPARAMREF();
			case OdxXhtmlPackage.LINKCOMPARAMREFS: return createLINKCOMPARAMREFS();
			case OdxXhtmlPackage.LI_TYPE: return createLiType();
			case OdxXhtmlPackage.LOGICALLINK: return createLOGICALLINK();
			case OdxXhtmlPackage.LOGICALLINKREFS: return createLOGICALLINKREFS();
			case OdxXhtmlPackage.LOGICALLINKS: return createLOGICALLINKS();
			case OdxXhtmlPackage.MATCHINGCOMPONENT: return createMATCHINGCOMPONENT();
			case OdxXhtmlPackage.MATCHINGCOMPONENTS: return createMATCHINGCOMPONENTS();
			case OdxXhtmlPackage.MATCHINGPARAMETER: return createMATCHINGPARAMETER();
			case OdxXhtmlPackage.MATCHINGPARAMETERS: return createMATCHINGPARAMETERS();
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM: return createMATCHINGREQUESTPARAM();
			case OdxXhtmlPackage.MEM: return createMEM();
			case OdxXhtmlPackage.MEMBERLOGICALLINK: return createMEMBERLOGICALLINK();
			case OdxXhtmlPackage.MINMAXLENGTHTYPE: return createMINMAXLENGTHTYPE();
			case OdxXhtmlPackage.MODELYEAR: return createMODELYEAR();
			case OdxXhtmlPackage.MODIFICATION: return createMODIFICATION();
			case OdxXhtmlPackage.MODIFICATION1: return createMODIFICATION1();
			case OdxXhtmlPackage.MODIFICATIONS: return createMODIFICATIONS();
			case OdxXhtmlPackage.MODIFICATIONS1: return createMODIFICATIONS1();
			case OdxXhtmlPackage.MULTIPLEECUJOB: return createMULTIPLEECUJOB();
			case OdxXhtmlPackage.MULTIPLEECUJOBS: return createMULTIPLEECUJOBS();
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC: return createMULTIPLEECUJOBSPEC();
			case OdxXhtmlPackage.MUX: return createMUX();
			case OdxXhtmlPackage.MUXS: return createMUXS();
			case OdxXhtmlPackage.NEGOFFSET: return createNEGOFFSET();
			case OdxXhtmlPackage.NEGOUTPUTPARAM: return createNEGOUTPUTPARAM();
			case OdxXhtmlPackage.NEGOUTPUTPARAMS: return createNEGOUTPUTPARAMS();
			case OdxXhtmlPackage.NEGRESPONSE: return createNEGRESPONSE();
			case OdxXhtmlPackage.NEGRESPONSEREFS: return createNEGRESPONSEREFS();
			case OdxXhtmlPackage.NEGRESPONSES: return createNEGRESPONSES();
			case OdxXhtmlPackage.NOTINHERITEDDIAGCOMM: return createNOTINHERITEDDIAGCOMM();
			case OdxXhtmlPackage.NOTINHERITEDDIAGCOMMS: return createNOTINHERITEDDIAGCOMMS();
			case OdxXhtmlPackage.NOTINHERITEDVARIABLE: return createNOTINHERITEDVARIABLE();
			case OdxXhtmlPackage.NOTINHERITEDVARIABLES: return createNOTINHERITEDVARIABLES();
			case OdxXhtmlPackage.ODX: return createODX();
			case OdxXhtmlPackage.ODXCATEGORY: return createODXCATEGORY();
			case OdxXhtmlPackage.ODXLINK: return createODXLINK();
			case OdxXhtmlPackage.ODXLINK1: return createODXLINK1();
			case OdxXhtmlPackage.OEM: return createOEM();
			case OdxXhtmlPackage.OL_TYPE: return createOlType();
			case OdxXhtmlPackage.OUTPUTPARAM: return createOUTPUTPARAM();
			case OdxXhtmlPackage.OUTPUTPARAMS: return createOUTPUTPARAMS();
			case OdxXhtmlPackage.OWNIDENT: return createOWNIDENT();
			case OdxXhtmlPackage.OWNIDENTS: return createOWNIDENTS();
			case OdxXhtmlPackage.P: return createP();
			case OdxXhtmlPackage.PARAM: return createPARAM();
			case OdxXhtmlPackage.PARAMLENGTHINFOTYPE: return createPARAMLENGTHINFOTYPE();
			case OdxXhtmlPackage.PARAMS: return createPARAMS();
			case OdxXhtmlPackage.PARENTREF: return createPARENTREF();
			case OdxXhtmlPackage.PARENTREFS: return createPARENTREFS();
			case OdxXhtmlPackage.PHYSCONST: return createPHYSCONST();
			case OdxXhtmlPackage.PHYSICALDIMENSION: return createPHYSICALDIMENSION();
			case OdxXhtmlPackage.PHYSICALDIMENSIONS: return createPHYSICALDIMENSIONS();
			case OdxXhtmlPackage.PHYSICALTYPE: return createPHYSICALTYPE();
			case OdxXhtmlPackage.PHYSICALVEHICLELINK: return createPHYSICALVEHICLELINK();
			case OdxXhtmlPackage.PHYSICALVEHICLELINKS: return createPHYSICALVEHICLELINKS();
			case OdxXhtmlPackage.PHYSMEM: return createPHYSMEM();
			case OdxXhtmlPackage.PHYSSEGMENT: return createPHYSSEGMENT();
			case OdxXhtmlPackage.PHYSSEGMENTS: return createPHYSSEGMENTS();
			case OdxXhtmlPackage.POSITIONABLEPARAM: return createPOSITIONABLEPARAM();
			case OdxXhtmlPackage.POSOFFSET: return createPOSOFFSET();
			case OdxXhtmlPackage.POSRESPONSE: return createPOSRESPONSE();
			case OdxXhtmlPackage.POSRESPONSEREFS: return createPOSRESPONSEREFS();
			case OdxXhtmlPackage.POSRESPONSES: return createPOSRESPONSES();
			case OdxXhtmlPackage.PROGCODE: return createPROGCODE();
			case OdxXhtmlPackage.PROGCODES: return createPROGCODES();
			case OdxXhtmlPackage.PROJECTIDENT: return createPROJECTIDENT();
			case OdxXhtmlPackage.PROJECTIDENTS: return createPROJECTIDENTS();
			case OdxXhtmlPackage.PROJECTINFO: return createPROJECTINFO();
			case OdxXhtmlPackage.PROJECTINFOS: return createPROJECTINFOS();
			case OdxXhtmlPackage.PROTOCOL: return createPROTOCOL();
			case OdxXhtmlPackage.PROTOCOLREF: return createPROTOCOLREF();
			case OdxXhtmlPackage.PROTOCOLS: return createPROTOCOLS();
			case OdxXhtmlPackage.PROTOCOLSNREFS: return createPROTOCOLSNREFS();
			case OdxXhtmlPackage.RELATEDDIAGCOMMREF: return createRELATEDDIAGCOMMREF();
			case OdxXhtmlPackage.RELATEDDIAGCOMMREFS: return createRELATEDDIAGCOMMREFS();
			case OdxXhtmlPackage.RELATEDDOC: return createRELATEDDOC();
			case OdxXhtmlPackage.RELATEDDOC1: return createRELATEDDOC1();
			case OdxXhtmlPackage.RELATEDDOCS: return createRELATEDDOCS();
			case OdxXhtmlPackage.RELATEDDOCS1: return createRELATEDDOCS1();
			case OdxXhtmlPackage.REQUEST: return createREQUEST();
			case OdxXhtmlPackage.REQUESTS: return createREQUESTS();
			case OdxXhtmlPackage.RESERVED: return createRESERVED();
			case OdxXhtmlPackage.RESPONSE: return createRESPONSE();
			case OdxXhtmlPackage.ROLES: return createROLES();
			case OdxXhtmlPackage.ROLES1: return createROLES1();
			case OdxXhtmlPackage.SCALECONSTR: return createSCALECONSTR();
			case OdxXhtmlPackage.SCALECONSTRS: return createSCALECONSTRS();
			case OdxXhtmlPackage.SD: return createSD();
			case OdxXhtmlPackage.SD1: return createSD1();
			case OdxXhtmlPackage.SDG: return createSDG();
			case OdxXhtmlPackage.SDG1: return createSDG1();
			case OdxXhtmlPackage.SDGCAPTION: return createSDGCAPTION();
			case OdxXhtmlPackage.SDGCAPTION1: return createSDGCAPTION1();
			case OdxXhtmlPackage.SDGS: return createSDGS();
			case OdxXhtmlPackage.SDGS1: return createSDGS1();
			case OdxXhtmlPackage.SECURITY: return createSECURITY();
			case OdxXhtmlPackage.SECURITYMETHOD: return createSECURITYMETHOD();
			case OdxXhtmlPackage.SECURITYS: return createSECURITYS();
			case OdxXhtmlPackage.SEGMENT: return createSEGMENT();
			case OdxXhtmlPackage.SEGMENTS: return createSEGMENTS();
			case OdxXhtmlPackage.SELECTIONTABLEREFS: return createSELECTIONTABLEREFS();
			case OdxXhtmlPackage.SESSION: return createSESSION();
			case OdxXhtmlPackage.SESSIONDESC: return createSESSIONDESC();
			case OdxXhtmlPackage.SESSIONDESCS: return createSESSIONDESCS();
			case OdxXhtmlPackage.SESSIONS: return createSESSIONS();
			case OdxXhtmlPackage.SINGLEECUJOB: return createSINGLEECUJOB();
			case OdxXhtmlPackage.SIZEDEFFILTER: return createSIZEDEFFILTER();
			case OdxXhtmlPackage.SIZEDEFPHYSSEGMENT: return createSIZEDEFPHYSSEGMENT();
			case OdxXhtmlPackage.SNREF: return createSNREF();
			case OdxXhtmlPackage.SOURCEENDADDRESS: return createSOURCEENDADDRESS();
			case OdxXhtmlPackage.SPECIALDATA: return createSPECIALDATA();
			case OdxXhtmlPackage.SPECIALDATA1: return createSPECIALDATA1();
			case OdxXhtmlPackage.STANDARDLENGTHTYPE: return createSTANDARDLENGTHTYPE();
			case OdxXhtmlPackage.STATICFIELD: return createSTATICFIELD();
			case OdxXhtmlPackage.STATICFIELDS: return createSTATICFIELDS();
			case OdxXhtmlPackage.STRUCTURE: return createSTRUCTURE();
			case OdxXhtmlPackage.STRUCTURES: return createSTRUCTURES();
			case OdxXhtmlPackage.SUB_TYPE: return createSubType();
			case OdxXhtmlPackage.SUPPORTEDDYNID: return createSUPPORTEDDYNID();
			case OdxXhtmlPackage.SUPPORTEDDYNIDS: return createSUPPORTEDDYNIDS();
			case OdxXhtmlPackage.SUP_TYPE: return createSupType();
			case OdxXhtmlPackage.SWITCHKEY: return createSWITCHKEY();
			case OdxXhtmlPackage.SWVARIABLE: return createSWVARIABLE();
			case OdxXhtmlPackage.SWVARIABLES: return createSWVARIABLES();
			case OdxXhtmlPackage.SYSTEM: return createSYSTEM();
			case OdxXhtmlPackage.TABLE: return createTABLE();
			case OdxXhtmlPackage.TABLEENTRY: return createTABLEENTRY();
			case OdxXhtmlPackage.TABLEKEY: return createTABLEKEY();
			case OdxXhtmlPackage.TABLEROW: return createTABLEROW();
			case OdxXhtmlPackage.TABLES: return createTABLES();
			case OdxXhtmlPackage.TABLESTRUCT: return createTABLESTRUCT();
			case OdxXhtmlPackage.TARGETADDROFFSET: return createTARGETADDROFFSET();
			case OdxXhtmlPackage.TEAMMEMBER: return createTEAMMEMBER();
			case OdxXhtmlPackage.TEAMMEMBER1: return createTEAMMEMBER1();
			case OdxXhtmlPackage.TEAMMEMBERS: return createTEAMMEMBERS();
			case OdxXhtmlPackage.TEAMMEMBERS1: return createTEAMMEMBERS1();
			case OdxXhtmlPackage.TEXT: return createTEXT();
			case OdxXhtmlPackage.TEXT1: return createTEXT1();
			case OdxXhtmlPackage.UL_TYPE: return createUlType();
			case OdxXhtmlPackage.UNCOMPRESSEDSIZE: return createUNCOMPRESSEDSIZE();
			case OdxXhtmlPackage.UNIONVALUE: return createUNIONVALUE();
			case OdxXhtmlPackage.UNIT: return createUNIT();
			case OdxXhtmlPackage.UNITGROUP: return createUNITGROUP();
			case OdxXhtmlPackage.UNITGROUPS: return createUNITGROUPS();
			case OdxXhtmlPackage.UNITREFS: return createUNITREFS();
			case OdxXhtmlPackage.UNITS: return createUNITS();
			case OdxXhtmlPackage.UNITSPEC: return createUNITSPEC();
			case OdxXhtmlPackage.V: return createV();
			case OdxXhtmlPackage.VALIDITYFOR: return createVALIDITYFOR();
			case OdxXhtmlPackage.VALUE: return createVALUE();
			case OdxXhtmlPackage.VARIABLEGROUP: return createVARIABLEGROUP();
			case OdxXhtmlPackage.VARIABLEGROUPS: return createVARIABLEGROUPS();
			case OdxXhtmlPackage.VEHICLECONNECTOR: return createVEHICLECONNECTOR();
			case OdxXhtmlPackage.VEHICLECONNECTORPIN: return createVEHICLECONNECTORPIN();
			case OdxXhtmlPackage.VEHICLECONNECTORPINREFS: return createVEHICLECONNECTORPINREFS();
			case OdxXhtmlPackage.VEHICLECONNECTORPINS: return createVEHICLECONNECTORPINS();
			case OdxXhtmlPackage.VEHICLECONNECTORS: return createVEHICLECONNECTORS();
			case OdxXhtmlPackage.VEHICLEINFORMATION: return createVEHICLEINFORMATION();
			case OdxXhtmlPackage.VEHICLEINFORMATIONS: return createVEHICLEINFORMATIONS();
			case OdxXhtmlPackage.VEHICLEINFOSPEC: return createVEHICLEINFOSPEC();
			case OdxXhtmlPackage.VEHICLEMODEL: return createVEHICLEMODEL();
			case OdxXhtmlPackage.VEHICLETYPE: return createVEHICLETYPE();
			case OdxXhtmlPackage.VT: return createVT();
			case OdxXhtmlPackage.XDOC: return createXDOC();
			case OdxXhtmlPackage.XDOC1: return createXDOC1();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OdxXhtmlPackage.ADDRESSING:
				return createADDRESSINGFromString(eDataType, initialValue);
			case OdxXhtmlPackage.ALIGN_TYPE:
				return createAlignTypeFromString(eDataType, initialValue);
			case OdxXhtmlPackage.COMMRELATIONVALUETYPE:
				return createCOMMRELATIONVALUETYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.COMPUCATEGORY:
				return createCOMPUCATEGORYFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DATAFORMATSELECTION:
				return createDATAFORMATSELECTIONFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DATATYPE:
				return createDATATYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DIAGCLASSTYPE:
				return createDIAGCLASSTYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DIRECTION:
				return createDIRECTIONFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DOCTYPE:
				return createDOCTYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DOCTYPE1:
				return createDOCTYPE1FromString(eDataType, initialValue);
			case OdxXhtmlPackage.ENCODING:
				return createENCODINGFromString(eDataType, initialValue);
			case OdxXhtmlPackage.ENCRYPTCOMPRESSMETHODTYPE:
				return createENCRYPTCOMPRESSMETHODTYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.IDENTVALUETYPE:
				return createIDENTVALUETYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.INTERVALTYPE:
				return createINTERVALTYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.PHYSICALDATATYPE:
				return createPHYSICALDATATYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.PHYSICALLINKTYPE:
				return createPHYSICALLINKTYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.PINTYPE:
				return createPINTYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.PROJIDENTTYPE:
				return createPROJIDENTTYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.RADIX:
				return createRADIXFromString(eDataType, initialValue);
			case OdxXhtmlPackage.ROWFRAGMENT:
				return createROWFRAGMENTFromString(eDataType, initialValue);
			case OdxXhtmlPackage.SESSIONSUBELEMTYPE:
				return createSESSIONSUBELEMTYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.STANDARDISATIONLEVEL:
				return createSTANDARDISATIONLEVELFromString(eDataType, initialValue);
			case OdxXhtmlPackage.TERMINATION:
				return createTERMINATIONFromString(eDataType, initialValue);
			case OdxXhtmlPackage.UNITGROUPCATEGORY:
				return createUNITGROUPCATEGORYFromString(eDataType, initialValue);
			case OdxXhtmlPackage.UPDSTATUS:
				return createUPDSTATUSFromString(eDataType, initialValue);
			case OdxXhtmlPackage.VALIDTYPE:
				return createVALIDTYPEFromString(eDataType, initialValue);
			case OdxXhtmlPackage.ADDRESSING_OBJECT:
				return createADDRESSINGObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.ALIGN_TYPE_OBJECT:
				return createAlignTypeObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.COMMRELATIONVALUETYPE_OBJECT:
				return createCOMMRELATIONVALUETYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.COMPUCATEGORY_OBJECT:
				return createCOMPUCATEGORYObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DATAFORMATSELECTION_OBJECT:
				return createDATAFORMATSELECTIONObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DATATYPE_OBJECT:
				return createDATATYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DIAGCLASSTYPE_OBJECT:
				return createDIAGCLASSTYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DIRECTION_OBJECT:
				return createDIRECTIONObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DOCTYPE_OBJECT:
				return createDOCTYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.DOCTYPE_OBJECT1:
				return createDOCTYPEObject1FromString(eDataType, initialValue);
			case OdxXhtmlPackage.ENCODING_OBJECT:
				return createENCODINGObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.ENCRYPTCOMPRESSMETHODTYPE_OBJECT:
				return createENCRYPTCOMPRESSMETHODTYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.IDENTVALUETYPE_OBJECT:
				return createIDENTVALUETYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.INTERVALTYPE_OBJECT:
				return createINTERVALTYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.PHYSICALDATATYPE_OBJECT:
				return createPHYSICALDATATYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.PHYSICALLINKTYPE_OBJECT:
				return createPHYSICALLINKTYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.PINTYPE_OBJECT:
				return createPINTYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.PROJIDENTTYPE_OBJECT:
				return createPROJIDENTTYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.RADIX_OBJECT:
				return createRADIXObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.ROWFRAGMENT_OBJECT:
				return createROWFRAGMENTObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.SESSIONSUBELEMTYPE_OBJECT:
				return createSESSIONSUBELEMTYPEObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.STANDARDISATIONLEVEL_OBJECT:
				return createSTANDARDISATIONLEVELObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.TERMINATION_OBJECT:
				return createTERMINATIONObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.UNITGROUPCATEGORY_OBJECT:
				return createUNITGROUPCATEGORYObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.UPDSTATUS_OBJECT:
				return createUPDSTATUSObjectFromString(eDataType, initialValue);
			case OdxXhtmlPackage.VALIDTYPE_OBJECT:
				return createVALIDTYPEObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OdxXhtmlPackage.ADDRESSING:
				return convertADDRESSINGToString(eDataType, instanceValue);
			case OdxXhtmlPackage.ALIGN_TYPE:
				return convertAlignTypeToString(eDataType, instanceValue);
			case OdxXhtmlPackage.COMMRELATIONVALUETYPE:
				return convertCOMMRELATIONVALUETYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.COMPUCATEGORY:
				return convertCOMPUCATEGORYToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DATAFORMATSELECTION:
				return convertDATAFORMATSELECTIONToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DATATYPE:
				return convertDATATYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DIAGCLASSTYPE:
				return convertDIAGCLASSTYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DIRECTION:
				return convertDIRECTIONToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DOCTYPE:
				return convertDOCTYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DOCTYPE1:
				return convertDOCTYPE1ToString(eDataType, instanceValue);
			case OdxXhtmlPackage.ENCODING:
				return convertENCODINGToString(eDataType, instanceValue);
			case OdxXhtmlPackage.ENCRYPTCOMPRESSMETHODTYPE:
				return convertENCRYPTCOMPRESSMETHODTYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.IDENTVALUETYPE:
				return convertIDENTVALUETYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.INTERVALTYPE:
				return convertINTERVALTYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.PHYSICALDATATYPE:
				return convertPHYSICALDATATYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.PHYSICALLINKTYPE:
				return convertPHYSICALLINKTYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.PINTYPE:
				return convertPINTYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.PROJIDENTTYPE:
				return convertPROJIDENTTYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.RADIX:
				return convertRADIXToString(eDataType, instanceValue);
			case OdxXhtmlPackage.ROWFRAGMENT:
				return convertROWFRAGMENTToString(eDataType, instanceValue);
			case OdxXhtmlPackage.SESSIONSUBELEMTYPE:
				return convertSESSIONSUBELEMTYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.STANDARDISATIONLEVEL:
				return convertSTANDARDISATIONLEVELToString(eDataType, instanceValue);
			case OdxXhtmlPackage.TERMINATION:
				return convertTERMINATIONToString(eDataType, instanceValue);
			case OdxXhtmlPackage.UNITGROUPCATEGORY:
				return convertUNITGROUPCATEGORYToString(eDataType, instanceValue);
			case OdxXhtmlPackage.UPDSTATUS:
				return convertUPDSTATUSToString(eDataType, instanceValue);
			case OdxXhtmlPackage.VALIDTYPE:
				return convertVALIDTYPEToString(eDataType, instanceValue);
			case OdxXhtmlPackage.ADDRESSING_OBJECT:
				return convertADDRESSINGObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.ALIGN_TYPE_OBJECT:
				return convertAlignTypeObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.COMMRELATIONVALUETYPE_OBJECT:
				return convertCOMMRELATIONVALUETYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.COMPUCATEGORY_OBJECT:
				return convertCOMPUCATEGORYObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DATAFORMATSELECTION_OBJECT:
				return convertDATAFORMATSELECTIONObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DATATYPE_OBJECT:
				return convertDATATYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DIAGCLASSTYPE_OBJECT:
				return convertDIAGCLASSTYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DIRECTION_OBJECT:
				return convertDIRECTIONObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DOCTYPE_OBJECT:
				return convertDOCTYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.DOCTYPE_OBJECT1:
				return convertDOCTYPEObject1ToString(eDataType, instanceValue);
			case OdxXhtmlPackage.ENCODING_OBJECT:
				return convertENCODINGObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.ENCRYPTCOMPRESSMETHODTYPE_OBJECT:
				return convertENCRYPTCOMPRESSMETHODTYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.IDENTVALUETYPE_OBJECT:
				return convertIDENTVALUETYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.INTERVALTYPE_OBJECT:
				return convertINTERVALTYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.PHYSICALDATATYPE_OBJECT:
				return convertPHYSICALDATATYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.PHYSICALLINKTYPE_OBJECT:
				return convertPHYSICALLINKTYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.PINTYPE_OBJECT:
				return convertPINTYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.PROJIDENTTYPE_OBJECT:
				return convertPROJIDENTTYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.RADIX_OBJECT:
				return convertRADIXObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.ROWFRAGMENT_OBJECT:
				return convertROWFRAGMENTObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.SESSIONSUBELEMTYPE_OBJECT:
				return convertSESSIONSUBELEMTYPEObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.STANDARDISATIONLEVEL_OBJECT:
				return convertSTANDARDISATIONLEVELObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.TERMINATION_OBJECT:
				return convertTERMINATIONObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.UNITGROUPCATEGORY_OBJECT:
				return convertUNITGROUPCATEGORYObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.UPDSTATUS_OBJECT:
				return convertUPDSTATUSObjectToString(eDataType, instanceValue);
			case OdxXhtmlPackage.VALIDTYPE_OBJECT:
				return convertVALIDTYPEObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ABLOCK createABLOCK() {
		ABLOCKImpl ablock = new ABLOCKImpl();
		return ablock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ABLOCKSType createABLOCKSType() {
		ABLOCKSTypeImpl ablocksType = new ABLOCKSTypeImpl();
		return ablocksType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ACCESSLEVEL createACCESSLEVEL() {
		ACCESSLEVELImpl accesslevel = new ACCESSLEVELImpl();
		return accesslevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ACCESSLEVELS createACCESSLEVELS() {
		ACCESSLEVELSImpl accesslevels = new ACCESSLEVELSImpl();
		return accesslevels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADDRDEFFILTER createADDRDEFFILTER() {
		ADDRDEFFILTERImpl addrdeffilter = new ADDRDEFFILTERImpl();
		return addrdeffilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADDRDEFPHYSSEGMENT createADDRDEFPHYSSEGMENT() {
		ADDRDEFPHYSSEGMENTImpl addrdefphyssegment = new ADDRDEFPHYSSEGMENTImpl();
		return addrdefphyssegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA createADMINDATA() {
		ADMINDATAImpl admindata = new ADMINDATAImpl();
		return admindata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA1 createADMINDATA1() {
		ADMINDATA1Impl admindata1 = new ADMINDATA1Impl();
		return admindata1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ALLVALUE createALLVALUE() {
		ALLVALUEImpl allvalue = new ALLVALUEImpl();
		return allvalue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AUDIENCE createAUDIENCE() {
		AUDIENCEImpl audience = new AUDIENCEImpl();
		return audience;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AUTMETHOD createAUTMETHOD() {
		AUTMETHODImpl autmethod = new AUTMETHODImpl();
		return autmethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AUTMETHODS createAUTMETHODS() {
		AUTMETHODSImpl autmethods = new AUTMETHODSImpl();
		return autmethods;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BASEVARIANT createBASEVARIANT() {
		BASEVARIANTImpl basevariant = new BASEVARIANTImpl();
		return basevariant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BASEVARIANTREF createBASEVARIANTREF() {
		BASEVARIANTREFImpl basevariantref = new BASEVARIANTREFImpl();
		return basevariantref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BASEVARIANTS createBASEVARIANTS() {
		BASEVARIANTSImpl basevariants = new BASEVARIANTSImpl();
		return basevariants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BASICSTRUCTURE createBASICSTRUCTURE() {
		BASICSTRUCTUREImpl basicstructure = new BASICSTRUCTUREImpl();
		return basicstructure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Block createBlock() {
		BlockImpl block = new BlockImpl();
		return block;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BType createBType() {
		BTypeImpl bType = new BTypeImpl();
		return bType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CASE createCASE() {
		CASEImpl case_ = new CASEImpl();
		return case_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CASES createCASES() {
		CASESImpl cases = new CASESImpl();
		return cases;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CATALOG createCATALOG() {
		CATALOGImpl catalog = new CATALOGImpl();
		return catalog;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CHECKSUM createCHECKSUM() {
		CHECKSUMImpl checksum = new CHECKSUMImpl();
		return checksum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CHECKSUMRESULT createCHECKSUMRESULT() {
		CHECKSUMRESULTImpl checksumresult = new CHECKSUMRESULTImpl();
		return checksumresult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CHECKSUMS createCHECKSUMS() {
		CHECKSUMSImpl checksums = new CHECKSUMSImpl();
		return checksums;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CODEDCONST createCODEDCONST() {
		CODEDCONSTImpl codedconst = new CODEDCONSTImpl();
		return codedconst;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMMRELATION createCOMMRELATION() {
		COMMRELATIONImpl commrelation = new COMMRELATIONImpl();
		return commrelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMMRELATIONS createCOMMRELATIONS() {
		COMMRELATIONSImpl commrelations = new COMMRELATIONSImpl();
		return commrelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDATA createCOMPANYDATA() {
		COMPANYDATAImpl companydata = new COMPANYDATAImpl();
		return companydata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDATA1 createCOMPANYDATA1() {
		COMPANYDATA1Impl companydata1 = new COMPANYDATA1Impl();
		return companydata1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDATAS createCOMPANYDATAS() {
		COMPANYDATASImpl companydatas = new COMPANYDATASImpl();
		return companydatas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDATASType createCOMPANYDATASType() {
		COMPANYDATASTypeImpl companydatasType = new COMPANYDATASTypeImpl();
		return companydatasType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDOCINFO createCOMPANYDOCINFO() {
		COMPANYDOCINFOImpl companydocinfo = new COMPANYDOCINFOImpl();
		return companydocinfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDOCINFO1 createCOMPANYDOCINFO1() {
		COMPANYDOCINFO1Impl companydocinfo1 = new COMPANYDOCINFO1Impl();
		return companydocinfo1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDOCINFOS createCOMPANYDOCINFOS() {
		COMPANYDOCINFOSImpl companydocinfos = new COMPANYDOCINFOSImpl();
		return companydocinfos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDOCINFOS1 createCOMPANYDOCINFOS1() {
		COMPANYDOCINFOS1Impl companydocinfos1 = new COMPANYDOCINFOS1Impl();
		return companydocinfos1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYREVISIONINFO createCOMPANYREVISIONINFO() {
		COMPANYREVISIONINFOImpl companyrevisioninfo = new COMPANYREVISIONINFOImpl();
		return companyrevisioninfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYREVISIONINFO1 createCOMPANYREVISIONINFO1() {
		COMPANYREVISIONINFO1Impl companyrevisioninfo1 = new COMPANYREVISIONINFO1Impl();
		return companyrevisioninfo1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYREVISIONINFOS createCOMPANYREVISIONINFOS() {
		COMPANYREVISIONINFOSImpl companyrevisioninfos = new COMPANYREVISIONINFOSImpl();
		return companyrevisioninfos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYREVISIONINFOS1 createCOMPANYREVISIONINFOS1() {
		COMPANYREVISIONINFOS1Impl companyrevisioninfos1 = new COMPANYREVISIONINFOS1Impl();
		return companyrevisioninfos1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYSPECIFICINFO createCOMPANYSPECIFICINFO() {
		COMPANYSPECIFICINFOImpl companyspecificinfo = new COMPANYSPECIFICINFOImpl();
		return companyspecificinfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYSPECIFICINFO1 createCOMPANYSPECIFICINFO1() {
		COMPANYSPECIFICINFO1Impl companyspecificinfo1 = new COMPANYSPECIFICINFO1Impl();
		return companyspecificinfo1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPARAM createCOMPARAM() {
		COMPARAMImpl comparam = new COMPARAMImpl();
		return comparam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPARAMREF createCOMPARAMREF() {
		COMPARAMREFImpl comparamref = new COMPARAMREFImpl();
		return comparamref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPARAMREFS createCOMPARAMREFS() {
		COMPARAMREFSImpl comparamrefs = new COMPARAMREFSImpl();
		return comparamrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPARAMS createCOMPARAMS() {
		COMPARAMSImpl comparams = new COMPARAMSImpl();
		return comparams;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPARAMSPEC createCOMPARAMSPEC() {
		COMPARAMSPECImpl comparamspec = new COMPARAMSPECImpl();
		return comparamspec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPLEXDOP createCOMPLEXDOP() {
		COMPLEXDOPImpl complexdop = new COMPLEXDOPImpl();
		return complexdop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUCONST createCOMPUCONST() {
		COMPUCONSTImpl compuconst = new COMPUCONSTImpl();
		return compuconst;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUDEFAULTVALUE createCOMPUDEFAULTVALUE() {
		COMPUDEFAULTVALUEImpl compudefaultvalue = new COMPUDEFAULTVALUEImpl();
		return compudefaultvalue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUDENOMINATOR createCOMPUDENOMINATOR() {
		COMPUDENOMINATORImpl compudenominator = new COMPUDENOMINATORImpl();
		return compudenominator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUINTERNALTOPHYS createCOMPUINTERNALTOPHYS() {
		COMPUINTERNALTOPHYSImpl compuinternaltophys = new COMPUINTERNALTOPHYSImpl();
		return compuinternaltophys;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUINVERSEVALUE createCOMPUINVERSEVALUE() {
		COMPUINVERSEVALUEImpl compuinversevalue = new COMPUINVERSEVALUEImpl();
		return compuinversevalue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUMETHOD createCOMPUMETHOD() {
		COMPUMETHODImpl compumethod = new COMPUMETHODImpl();
		return compumethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUNUMERATOR createCOMPUNUMERATOR() {
		COMPUNUMERATORImpl compunumerator = new COMPUNUMERATORImpl();
		return compunumerator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUPHYSTOINTERNAL createCOMPUPHYSTOINTERNAL() {
		COMPUPHYSTOINTERNALImpl compuphystointernal = new COMPUPHYSTOINTERNALImpl();
		return compuphystointernal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPURATIONALCOEFFS createCOMPURATIONALCOEFFS() {
		COMPURATIONALCOEFFSImpl compurationalcoeffs = new COMPURATIONALCOEFFSImpl();
		return compurationalcoeffs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUSCALE createCOMPUSCALE() {
		COMPUSCALEImpl compuscale = new COMPUSCALEImpl();
		return compuscale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUSCALES createCOMPUSCALES() {
		COMPUSCALESImpl compuscales = new COMPUSCALESImpl();
		return compuscales;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATABLOCK createDATABLOCK() {
		DATABLOCKImpl datablock = new DATABLOCKImpl();
		return datablock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATABLOCKREFS createDATABLOCKREFS() {
		DATABLOCKREFSImpl datablockrefs = new DATABLOCKREFSImpl();
		return datablockrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATABLOCKS createDATABLOCKS() {
		DATABLOCKSImpl datablocks = new DATABLOCKSImpl();
		return datablocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATAFILE createDATAFILE() {
		DATAFILEImpl datafile = new DATAFILEImpl();
		return datafile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATAFORMAT createDATAFORMAT() {
		DATAFORMATImpl dataformat = new DATAFORMATImpl();
		return dataformat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATAOBJECTPROP createDATAOBJECTPROP() {
		DATAOBJECTPROPImpl dataobjectprop = new DATAOBJECTPROPImpl();
		return dataobjectprop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATAOBJECTPROPS createDATAOBJECTPROPS() {
		DATAOBJECTPROPSImpl dataobjectprops = new DATAOBJECTPROPSImpl();
		return dataobjectprops;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DEFAULTCASE createDEFAULTCASE() {
		DEFAULTCASEImpl defaultcase = new DEFAULTCASEImpl();
		return defaultcase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION createDESCRIPTION() {
		DESCRIPTIONImpl description = new DESCRIPTIONImpl();
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION1 createDESCRIPTION1() {
		DESCRIPTION1Impl description1 = new DESCRIPTION1Impl();
		return description1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DETERMINENUMBEROFITEMS createDETERMINENUMBEROFITEMS() {
		DETERMINENUMBEROFITEMSImpl determinenumberofitems = new DETERMINENUMBEROFITEMSImpl();
		return determinenumberofitems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGCODEDTYPE createDIAGCODEDTYPE() {
		DIAGCODEDTYPEImpl diagcodedtype = new DIAGCODEDTYPEImpl();
		return diagcodedtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGCOMM createDIAGCOMM() {
		DIAGCOMMImpl diagcomm = new DIAGCOMMImpl();
		return diagcomm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGCOMMS createDIAGCOMMS() {
		DIAGCOMMSImpl diagcomms = new DIAGCOMMSImpl();
		return diagcomms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGDATADICTIONARYSPEC createDIAGDATADICTIONARYSPEC() {
		DIAGDATADICTIONARYSPECImpl diagdatadictionaryspec = new DIAGDATADICTIONARYSPECImpl();
		return diagdatadictionaryspec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGLAYER createDIAGLAYER() {
		DIAGLAYERImpl diaglayer = new DIAGLAYERImpl();
		return diaglayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGLAYERCONTAINER createDIAGLAYERCONTAINER() {
		DIAGLAYERCONTAINERImpl diaglayercontainer = new DIAGLAYERCONTAINERImpl();
		return diaglayercontainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGLAYERREFS createDIAGLAYERREFS() {
		DIAGLAYERREFSImpl diaglayerrefs = new DIAGLAYERREFSImpl();
		return diaglayerrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGSERVICE createDIAGSERVICE() {
		DIAGSERVICEImpl diagservice = new DIAGSERVICEImpl();
		return diagservice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGVARIABLE createDIAGVARIABLE() {
		DIAGVARIABLEImpl diagvariable = new DIAGVARIABLEImpl();
		return diagvariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGVARIABLES createDIAGVARIABLES() {
		DIAGVARIABLESImpl diagvariables = new DIAGVARIABLESImpl();
		return diagvariables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOCREVISION createDOCREVISION() {
		DOCREVISIONImpl docrevision = new DOCREVISIONImpl();
		return docrevision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOCREVISION1 createDOCREVISION1() {
		DOCREVISION1Impl docrevision1 = new DOCREVISION1Impl();
		return docrevision1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOCREVISIONS createDOCREVISIONS() {
		DOCREVISIONSImpl docrevisions = new DOCREVISIONSImpl();
		return docrevisions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOCREVISIONS1 createDOCREVISIONS1() {
		DOCREVISIONS1Impl docrevisions1 = new DOCREVISIONS1Impl();
		return docrevisions1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOPBASE createDOPBASE() {
		DOPBASEImpl dopbase = new DOPBASEImpl();
		return dopbase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DTC createDTC() {
		DTCImpl dtc = new DTCImpl();
		return dtc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DTCDOP createDTCDOP() {
		DTCDOPImpl dtcdop = new DTCDOPImpl();
		return dtcdop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DTCDOPS createDTCDOPS() {
		DTCDOPSImpl dtcdops = new DTCDOPSImpl();
		return dtcdops;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DTCS createDTCS() {
		DTCSImpl dtcs = new DTCSImpl();
		return dtcs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DTCVALUE createDTCVALUE() {
		DTCVALUEImpl dtcvalue = new DTCVALUEImpl();
		return dtcvalue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DTCVALUES createDTCVALUES() {
		DTCVALUESImpl dtcvalues = new DTCVALUESImpl();
		return dtcvalues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNAMIC createDYNAMIC() {
		DYNAMICImpl dynamic = new DYNAMICImpl();
		return dynamic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNAMICENDMARKERFIELD createDYNAMICENDMARKERFIELD() {
		DYNAMICENDMARKERFIELDImpl dynamicendmarkerfield = new DYNAMICENDMARKERFIELDImpl();
		return dynamicendmarkerfield;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNAMICENDMARKERFIELDS createDYNAMICENDMARKERFIELDS() {
		DYNAMICENDMARKERFIELDSImpl dynamicendmarkerfields = new DYNAMICENDMARKERFIELDSImpl();
		return dynamicendmarkerfields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNAMICLENGTHFIELD createDYNAMICLENGTHFIELD() {
		DYNAMICLENGTHFIELDImpl dynamiclengthfield = new DYNAMICLENGTHFIELDImpl();
		return dynamiclengthfield;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNAMICLENGTHFIELDS createDYNAMICLENGTHFIELDS() {
		DYNAMICLENGTHFIELDSImpl dynamiclengthfields = new DYNAMICLENGTHFIELDSImpl();
		return dynamiclengthfields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNDEFINEDSPEC createDYNDEFINEDSPEC() {
		DYNDEFINEDSPECImpl dyndefinedspec = new DYNDEFINEDSPECImpl();
		return dyndefinedspec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNENDDOPREF createDYNENDDOPREF() {
		DYNENDDOPREFImpl dynenddopref = new DYNENDDOPREFImpl();
		return dynenddopref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNIDDEFMODEINFO createDYNIDDEFMODEINFO() {
		DYNIDDEFMODEINFOImpl dyniddefmodeinfo = new DYNIDDEFMODEINFOImpl();
		return dyniddefmodeinfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNIDDEFMODEINFOS createDYNIDDEFMODEINFOS() {
		DYNIDDEFMODEINFOSImpl dyniddefmodeinfos = new DYNIDDEFMODEINFOSImpl();
		return dyniddefmodeinfos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUGROUP createECUGROUP() {
		ECUGROUPImpl ecugroup = new ECUGROUPImpl();
		return ecugroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUGROUPS createECUGROUPS() {
		ECUGROUPSImpl ecugroups = new ECUGROUPSImpl();
		return ecugroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUMEM createECUMEM() {
		ECUMEMImpl ecumem = new ECUMEMImpl();
		return ecumem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUMEMCONNECTOR createECUMEMCONNECTOR() {
		ECUMEMCONNECTORImpl ecumemconnector = new ECUMEMCONNECTORImpl();
		return ecumemconnector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUMEMCONNECTORS createECUMEMCONNECTORS() {
		ECUMEMCONNECTORSImpl ecumemconnectors = new ECUMEMCONNECTORSImpl();
		return ecumemconnectors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUMEMS createECUMEMS() {
		ECUMEMSImpl ecumems = new ECUMEMSImpl();
		return ecumems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUPROXY createECUPROXY() {
		ECUPROXYImpl ecuproxy = new ECUPROXYImpl();
		return ecuproxy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUPROXYREFS createECUPROXYREFS() {
		ECUPROXYREFSImpl ecuproxyrefs = new ECUPROXYREFSImpl();
		return ecuproxyrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUSHAREDDATA createECUSHAREDDATA() {
		ECUSHAREDDATAImpl ecushareddata = new ECUSHAREDDATAImpl();
		return ecushareddata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUSHAREDDATAREF createECUSHAREDDATAREF() {
		ECUSHAREDDATAREFImpl ecushareddataref = new ECUSHAREDDATAREFImpl();
		return ecushareddataref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUSHAREDDATAS createECUSHAREDDATAS() {
		ECUSHAREDDATASImpl ecushareddatas = new ECUSHAREDDATASImpl();
		return ecushareddatas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUVARIANT createECUVARIANT() {
		ECUVARIANTImpl ecuvariant = new ECUVARIANTImpl();
		return ecuvariant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUVARIANTPATTERN createECUVARIANTPATTERN() {
		ECUVARIANTPATTERNImpl ecuvariantpattern = new ECUVARIANTPATTERNImpl();
		return ecuvariantpattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUVARIANTPATTERNS createECUVARIANTPATTERNS() {
		ECUVARIANTPATTERNSImpl ecuvariantpatterns = new ECUVARIANTPATTERNSImpl();
		return ecuvariantpatterns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUVARIANTS createECUVARIANTS() {
		ECUVARIANTSImpl ecuvariants = new ECUVARIANTSImpl();
		return ecuvariants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENCRYPTCOMPRESSMETHOD createENCRYPTCOMPRESSMETHOD() {
		ENCRYPTCOMPRESSMETHODImpl encryptcompressmethod = new ENCRYPTCOMPRESSMETHODImpl();
		return encryptcompressmethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENDOFPDUFIELD createENDOFPDUFIELD() {
		ENDOFPDUFIELDImpl endofpdufield = new ENDOFPDUFIELDImpl();
		return endofpdufield;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENDOFPDUFIELDS createENDOFPDUFIELDS() {
		ENDOFPDUFIELDSImpl endofpdufields = new ENDOFPDUFIELDSImpl();
		return endofpdufields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENVDATA createENVDATA() {
		ENVDATAImpl envdata = new ENVDATAImpl();
		return envdata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENVDATADESC createENVDATADESC() {
		ENVDATADESCImpl envdatadesc = new ENVDATADESCImpl();
		return envdatadesc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENVDATADESCS createENVDATADESCS() {
		ENVDATADESCSImpl envdatadescs = new ENVDATADESCSImpl();
		return envdatadescs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENVDATAS createENVDATAS() {
		ENVDATASImpl envdatas = new ENVDATASImpl();
		return envdatas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EXPECTEDIDENT createEXPECTEDIDENT() {
		EXPECTEDIDENTImpl expectedident = new EXPECTEDIDENTImpl();
		return expectedident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EXPECTEDIDENTS createEXPECTEDIDENTS() {
		EXPECTEDIDENTSImpl expectedidents = new EXPECTEDIDENTSImpl();
		return expectedidents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EXTERNALACCESSMETHOD createEXTERNALACCESSMETHOD() {
		EXTERNALACCESSMETHODImpl externalaccessmethod = new EXTERNALACCESSMETHODImpl();
		return externalaccessmethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EXTERNFLASHDATA createEXTERNFLASHDATA() {
		EXTERNFLASHDATAImpl externflashdata = new EXTERNFLASHDATAImpl();
		return externflashdata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FIELD createFIELD() {
		FIELDImpl field = new FIELDImpl();
		return field;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FILE createFILE() {
		FILEImpl file = new FILEImpl();
		return file;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FILESType createFILESType() {
		FILESTypeImpl filesType = new FILESTypeImpl();
		return filesType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FILTER createFILTER() {
		FILTERImpl filter = new FILTERImpl();
		return filter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FILTERS createFILTERS() {
		FILTERSImpl filters = new FILTERSImpl();
		return filters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASH createFLASH() {
		FLASHImpl flash = new FLASHImpl();
		return flash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASHCLASS createFLASHCLASS() {
		FLASHCLASSImpl flashclass = new FLASHCLASSImpl();
		return flashclass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASHCLASSREFS createFLASHCLASSREFS() {
		FLASHCLASSREFSImpl flashclassrefs = new FLASHCLASSREFSImpl();
		return flashclassrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASHCLASSS createFLASHCLASSS() {
		FLASHCLASSSImpl flashclasss = new FLASHCLASSSImpl();
		return flashclasss;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASHDATA createFLASHDATA() {
		FLASHDATAImpl flashdata = new FLASHDATAImpl();
		return flashdata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASHDATAS createFLASHDATAS() {
		FLASHDATASImpl flashdatas = new FLASHDATASImpl();
		return flashdatas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Flow createFlow() {
		FlowImpl flow = new FlowImpl();
		return flow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTCLASS createFUNCTCLASS() {
		FUNCTCLASSImpl functclass = new FUNCTCLASSImpl();
		return functclass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTCLASSREFS createFUNCTCLASSREFS() {
		FUNCTCLASSREFSImpl functclassrefs = new FUNCTCLASSREFSImpl();
		return functclassrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTCLASSS createFUNCTCLASSS() {
		FUNCTCLASSSImpl functclasss = new FUNCTCLASSSImpl();
		return functclasss;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTIONALGROUP createFUNCTIONALGROUP() {
		FUNCTIONALGROUPImpl functionalgroup = new FUNCTIONALGROUPImpl();
		return functionalgroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTIONALGROUPREF createFUNCTIONALGROUPREF() {
		FUNCTIONALGROUPREFImpl functionalgroupref = new FUNCTIONALGROUPREFImpl();
		return functionalgroupref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTIONALGROUPS createFUNCTIONALGROUPS() {
		FUNCTIONALGROUPSImpl functionalgroups = new FUNCTIONALGROUPSImpl();
		return functionalgroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FWCHECKSUM createFWCHECKSUM() {
		FWCHECKSUMImpl fwchecksum = new FWCHECKSUMImpl();
		return fwchecksum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FWSIGNATURE createFWSIGNATURE() {
		FWSIGNATUREImpl fwsignature = new FWSIGNATUREImpl();
		return fwsignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GATEWAYLOGICALLINK createGATEWAYLOGICALLINK() {
		GATEWAYLOGICALLINKImpl gatewaylogicallink = new GATEWAYLOGICALLINKImpl();
		return gatewaylogicallink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GATEWAYLOGICALLINKREFS createGATEWAYLOGICALLINKREFS() {
		GATEWAYLOGICALLINKREFSImpl gatewaylogicallinkrefs = new GATEWAYLOGICALLINKREFSImpl();
		return gatewaylogicallinkrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GLOBALNEGRESPONSE createGLOBALNEGRESPONSE() {
		GLOBALNEGRESPONSEImpl globalnegresponse = new GLOBALNEGRESPONSEImpl();
		return globalnegresponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GLOBALNEGRESPONSES createGLOBALNEGRESPONSES() {
		GLOBALNEGRESPONSESImpl globalnegresponses = new GLOBALNEGRESPONSESImpl();
		return globalnegresponses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public HIERARCHYELEMENT createHIERARCHYELEMENT() {
		HIERARCHYELEMENTImpl hierarchyelement = new HIERARCHYELEMENTImpl();
		return hierarchyelement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDENTDESC createIDENTDESC() {
		IDENTDESCImpl identdesc = new IDENTDESCImpl();
		return identdesc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDENTDESCS createIDENTDESCS() {
		IDENTDESCSImpl identdescs = new IDENTDESCSImpl();
		return identdescs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDENTVALUE createIDENTVALUE() {
		IDENTVALUEImpl identvalue = new IDENTVALUEImpl();
		return identvalue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDENTVALUES createIDENTVALUES() {
		IDENTVALUESImpl identvalues = new IDENTVALUESImpl();
		return identvalues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IMPORTREFS createIMPORTREFS() {
		IMPORTREFSImpl importrefs = new IMPORTREFSImpl();
		return importrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INFOCOMPONENT createINFOCOMPONENT() {
		INFOCOMPONENTImpl infocomponent = new INFOCOMPONENTImpl();
		return infocomponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INFOCOMPONENTREFS createINFOCOMPONENTREFS() {
		INFOCOMPONENTREFSImpl infocomponentrefs = new INFOCOMPONENTREFSImpl();
		return infocomponentrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INFOCOMPONENTS createINFOCOMPONENTS() {
		INFOCOMPONENTSImpl infocomponents = new INFOCOMPONENTSImpl();
		return infocomponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Inline createInline() {
		InlineImpl inline = new InlineImpl();
		return inline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INPUTPARAM createINPUTPARAM() {
		INPUTPARAMImpl inputparam = new INPUTPARAMImpl();
		return inputparam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INPUTPARAMS createINPUTPARAMS() {
		INPUTPARAMSImpl inputparams = new INPUTPARAMSImpl();
		return inputparams;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INTERNALCONSTR createINTERNALCONSTR() {
		INTERNALCONSTRImpl internalconstr = new INTERNALCONSTRImpl();
		return internalconstr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INTERNFLASHDATA createINTERNFLASHDATA() {
		INTERNFLASHDATAImpl internflashdata = new INTERNFLASHDATAImpl();
		return internflashdata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IType createIType() {
		ITypeImpl iType = new ITypeImpl();
		return iType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LAYERREFS createLAYERREFS() {
		LAYERREFSImpl layerrefs = new LAYERREFSImpl();
		return layerrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LEADINGLENGTHINFOTYPE createLEADINGLENGTHINFOTYPE() {
		LEADINGLENGTHINFOTYPEImpl leadinglengthinfotype = new LEADINGLENGTHINFOTYPEImpl();
		return leadinglengthinfotype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LENGTHDESCRIPTOR createLENGTHDESCRIPTOR() {
		LENGTHDESCRIPTORImpl lengthdescriptor = new LENGTHDESCRIPTORImpl();
		return lengthdescriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LENGTHKEY createLENGTHKEY() {
		LENGTHKEYImpl lengthkey = new LENGTHKEYImpl();
		return lengthkey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LIMIT createLIMIT() {
		LIMITImpl limit = new LIMITImpl();
		return limit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LINKCOMPARAMREF createLINKCOMPARAMREF() {
		LINKCOMPARAMREFImpl linkcomparamref = new LINKCOMPARAMREFImpl();
		return linkcomparamref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LINKCOMPARAMREFS createLINKCOMPARAMREFS() {
		LINKCOMPARAMREFSImpl linkcomparamrefs = new LINKCOMPARAMREFSImpl();
		return linkcomparamrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LiType createLiType() {
		LiTypeImpl liType = new LiTypeImpl();
		return liType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LOGICALLINK createLOGICALLINK() {
		LOGICALLINKImpl logicallink = new LOGICALLINKImpl();
		return logicallink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LOGICALLINKREFS createLOGICALLINKREFS() {
		LOGICALLINKREFSImpl logicallinkrefs = new LOGICALLINKREFSImpl();
		return logicallinkrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LOGICALLINKS createLOGICALLINKS() {
		LOGICALLINKSImpl logicallinks = new LOGICALLINKSImpl();
		return logicallinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MATCHINGCOMPONENT createMATCHINGCOMPONENT() {
		MATCHINGCOMPONENTImpl matchingcomponent = new MATCHINGCOMPONENTImpl();
		return matchingcomponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MATCHINGCOMPONENTS createMATCHINGCOMPONENTS() {
		MATCHINGCOMPONENTSImpl matchingcomponents = new MATCHINGCOMPONENTSImpl();
		return matchingcomponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MATCHINGPARAMETER createMATCHINGPARAMETER() {
		MATCHINGPARAMETERImpl matchingparameter = new MATCHINGPARAMETERImpl();
		return matchingparameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MATCHINGPARAMETERS createMATCHINGPARAMETERS() {
		MATCHINGPARAMETERSImpl matchingparameters = new MATCHINGPARAMETERSImpl();
		return matchingparameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MATCHINGREQUESTPARAM createMATCHINGREQUESTPARAM() {
		MATCHINGREQUESTPARAMImpl matchingrequestparam = new MATCHINGREQUESTPARAMImpl();
		return matchingrequestparam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MEM createMEM() {
		MEMImpl mem = new MEMImpl();
		return mem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MEMBERLOGICALLINK createMEMBERLOGICALLINK() {
		MEMBERLOGICALLINKImpl memberlogicallink = new MEMBERLOGICALLINKImpl();
		return memberlogicallink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MINMAXLENGTHTYPE createMINMAXLENGTHTYPE() {
		MINMAXLENGTHTYPEImpl minmaxlengthtype = new MINMAXLENGTHTYPEImpl();
		return minmaxlengthtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MODELYEAR createMODELYEAR() {
		MODELYEARImpl modelyear = new MODELYEARImpl();
		return modelyear;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MODIFICATION createMODIFICATION() {
		MODIFICATIONImpl modification = new MODIFICATIONImpl();
		return modification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MODIFICATION1 createMODIFICATION1() {
		MODIFICATION1Impl modification1 = new MODIFICATION1Impl();
		return modification1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MODIFICATIONS createMODIFICATIONS() {
		MODIFICATIONSImpl modifications = new MODIFICATIONSImpl();
		return modifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MODIFICATIONS1 createMODIFICATIONS1() {
		MODIFICATIONS1Impl modifications1 = new MODIFICATIONS1Impl();
		return modifications1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MULTIPLEECUJOB createMULTIPLEECUJOB() {
		MULTIPLEECUJOBImpl multipleecujob = new MULTIPLEECUJOBImpl();
		return multipleecujob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MULTIPLEECUJOBS createMULTIPLEECUJOBS() {
		MULTIPLEECUJOBSImpl multipleecujobs = new MULTIPLEECUJOBSImpl();
		return multipleecujobs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MULTIPLEECUJOBSPEC createMULTIPLEECUJOBSPEC() {
		MULTIPLEECUJOBSPECImpl multipleecujobspec = new MULTIPLEECUJOBSPECImpl();
		return multipleecujobspec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MUX createMUX() {
		MUXImpl mux = new MUXImpl();
		return mux;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MUXS createMUXS() {
		MUXSImpl muxs = new MUXSImpl();
		return muxs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGOFFSET createNEGOFFSET() {
		NEGOFFSETImpl negoffset = new NEGOFFSETImpl();
		return negoffset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGOUTPUTPARAM createNEGOUTPUTPARAM() {
		NEGOUTPUTPARAMImpl negoutputparam = new NEGOUTPUTPARAMImpl();
		return negoutputparam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGOUTPUTPARAMS createNEGOUTPUTPARAMS() {
		NEGOUTPUTPARAMSImpl negoutputparams = new NEGOUTPUTPARAMSImpl();
		return negoutputparams;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGRESPONSE createNEGRESPONSE() {
		NEGRESPONSEImpl negresponse = new NEGRESPONSEImpl();
		return negresponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGRESPONSEREFS createNEGRESPONSEREFS() {
		NEGRESPONSEREFSImpl negresponserefs = new NEGRESPONSEREFSImpl();
		return negresponserefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGRESPONSES createNEGRESPONSES() {
		NEGRESPONSESImpl negresponses = new NEGRESPONSESImpl();
		return negresponses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NOTINHERITEDDIAGCOMM createNOTINHERITEDDIAGCOMM() {
		NOTINHERITEDDIAGCOMMImpl notinheriteddiagcomm = new NOTINHERITEDDIAGCOMMImpl();
		return notinheriteddiagcomm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NOTINHERITEDDIAGCOMMS createNOTINHERITEDDIAGCOMMS() {
		NOTINHERITEDDIAGCOMMSImpl notinheriteddiagcomms = new NOTINHERITEDDIAGCOMMSImpl();
		return notinheriteddiagcomms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NOTINHERITEDVARIABLE createNOTINHERITEDVARIABLE() {
		NOTINHERITEDVARIABLEImpl notinheritedvariable = new NOTINHERITEDVARIABLEImpl();
		return notinheritedvariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NOTINHERITEDVARIABLES createNOTINHERITEDVARIABLES() {
		NOTINHERITEDVARIABLESImpl notinheritedvariables = new NOTINHERITEDVARIABLESImpl();
		return notinheritedvariables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODX createODX() {
		ODXImpl odx = new ODXImpl();
		return odx;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXCATEGORY createODXCATEGORY() {
		ODXCATEGORYImpl odxcategory = new ODXCATEGORYImpl();
		return odxcategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK createODXLINK() {
		ODXLINKImpl odxlink = new ODXLINKImpl();
		return odxlink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK1 createODXLINK1() {
		ODXLINK1Impl odxlink1 = new ODXLINK1Impl();
		return odxlink1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OEM createOEM() {
		OEMImpl oem = new OEMImpl();
		return oem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OlType createOlType() {
		OlTypeImpl olType = new OlTypeImpl();
		return olType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OUTPUTPARAM createOUTPUTPARAM() {
		OUTPUTPARAMImpl outputparam = new OUTPUTPARAMImpl();
		return outputparam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OUTPUTPARAMS createOUTPUTPARAMS() {
		OUTPUTPARAMSImpl outputparams = new OUTPUTPARAMSImpl();
		return outputparams;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OWNIDENT createOWNIDENT() {
		OWNIDENTImpl ownident = new OWNIDENTImpl();
		return ownident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OWNIDENTS createOWNIDENTS() {
		OWNIDENTSImpl ownidents = new OWNIDENTSImpl();
		return ownidents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public P createP() {
		PImpl p = new PImpl();
		return p;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARAM createPARAM() {
		PARAMImpl param = new PARAMImpl();
		return param;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARAMLENGTHINFOTYPE createPARAMLENGTHINFOTYPE() {
		PARAMLENGTHINFOTYPEImpl paramlengthinfotype = new PARAMLENGTHINFOTYPEImpl();
		return paramlengthinfotype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARAMS createPARAMS() {
		PARAMSImpl params = new PARAMSImpl();
		return params;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARENTREF createPARENTREF() {
		PARENTREFImpl parentref = new PARENTREFImpl();
		return parentref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARENTREFS createPARENTREFS() {
		PARENTREFSImpl parentrefs = new PARENTREFSImpl();
		return parentrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSCONST createPHYSCONST() {
		PHYSCONSTImpl physconst = new PHYSCONSTImpl();
		return physconst;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALDIMENSION createPHYSICALDIMENSION() {
		PHYSICALDIMENSIONImpl physicaldimension = new PHYSICALDIMENSIONImpl();
		return physicaldimension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALDIMENSIONS createPHYSICALDIMENSIONS() {
		PHYSICALDIMENSIONSImpl physicaldimensions = new PHYSICALDIMENSIONSImpl();
		return physicaldimensions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALTYPE createPHYSICALTYPE() {
		PHYSICALTYPEImpl physicaltype = new PHYSICALTYPEImpl();
		return physicaltype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALVEHICLELINK createPHYSICALVEHICLELINK() {
		PHYSICALVEHICLELINKImpl physicalvehiclelink = new PHYSICALVEHICLELINKImpl();
		return physicalvehiclelink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALVEHICLELINKS createPHYSICALVEHICLELINKS() {
		PHYSICALVEHICLELINKSImpl physicalvehiclelinks = new PHYSICALVEHICLELINKSImpl();
		return physicalvehiclelinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSMEM createPHYSMEM() {
		PHYSMEMImpl physmem = new PHYSMEMImpl();
		return physmem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSSEGMENT createPHYSSEGMENT() {
		PHYSSEGMENTImpl physsegment = new PHYSSEGMENTImpl();
		return physsegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSSEGMENTS createPHYSSEGMENTS() {
		PHYSSEGMENTSImpl physsegments = new PHYSSEGMENTSImpl();
		return physsegments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public POSITIONABLEPARAM createPOSITIONABLEPARAM() {
		POSITIONABLEPARAMImpl positionableparam = new POSITIONABLEPARAMImpl();
		return positionableparam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public POSOFFSET createPOSOFFSET() {
		POSOFFSETImpl posoffset = new POSOFFSETImpl();
		return posoffset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public POSRESPONSE createPOSRESPONSE() {
		POSRESPONSEImpl posresponse = new POSRESPONSEImpl();
		return posresponse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public POSRESPONSEREFS createPOSRESPONSEREFS() {
		POSRESPONSEREFSImpl posresponserefs = new POSRESPONSEREFSImpl();
		return posresponserefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public POSRESPONSES createPOSRESPONSES() {
		POSRESPONSESImpl posresponses = new POSRESPONSESImpl();
		return posresponses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROGCODE createPROGCODE() {
		PROGCODEImpl progcode = new PROGCODEImpl();
		return progcode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROGCODES createPROGCODES() {
		PROGCODESImpl progcodes = new PROGCODESImpl();
		return progcodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROJECTIDENT createPROJECTIDENT() {
		PROJECTIDENTImpl projectident = new PROJECTIDENTImpl();
		return projectident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROJECTIDENTS createPROJECTIDENTS() {
		PROJECTIDENTSImpl projectidents = new PROJECTIDENTSImpl();
		return projectidents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROJECTINFO createPROJECTINFO() {
		PROJECTINFOImpl projectinfo = new PROJECTINFOImpl();
		return projectinfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROJECTINFOS createPROJECTINFOS() {
		PROJECTINFOSImpl projectinfos = new PROJECTINFOSImpl();
		return projectinfos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROTOCOL createPROTOCOL() {
		PROTOCOLImpl protocol = new PROTOCOLImpl();
		return protocol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROTOCOLREF createPROTOCOLREF() {
		PROTOCOLREFImpl protocolref = new PROTOCOLREFImpl();
		return protocolref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROTOCOLS createPROTOCOLS() {
		PROTOCOLSImpl protocols = new PROTOCOLSImpl();
		return protocols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROTOCOLSNREFS createPROTOCOLSNREFS() {
		PROTOCOLSNREFSImpl protocolsnrefs = new PROTOCOLSNREFSImpl();
		return protocolsnrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RELATEDDIAGCOMMREF createRELATEDDIAGCOMMREF() {
		RELATEDDIAGCOMMREFImpl relateddiagcommref = new RELATEDDIAGCOMMREFImpl();
		return relateddiagcommref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RELATEDDIAGCOMMREFS createRELATEDDIAGCOMMREFS() {
		RELATEDDIAGCOMMREFSImpl relateddiagcommrefs = new RELATEDDIAGCOMMREFSImpl();
		return relateddiagcommrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RELATEDDOC createRELATEDDOC() {
		RELATEDDOCImpl relateddoc = new RELATEDDOCImpl();
		return relateddoc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RELATEDDOC1 createRELATEDDOC1() {
		RELATEDDOC1Impl relateddoc1 = new RELATEDDOC1Impl();
		return relateddoc1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RELATEDDOCS createRELATEDDOCS() {
		RELATEDDOCSImpl relateddocs = new RELATEDDOCSImpl();
		return relateddocs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RELATEDDOCS1 createRELATEDDOCS1() {
		RELATEDDOCS1Impl relateddocs1 = new RELATEDDOCS1Impl();
		return relateddocs1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public REQUEST createREQUEST() {
		REQUESTImpl request = new REQUESTImpl();
		return request;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public REQUESTS createREQUESTS() {
		REQUESTSImpl requests = new REQUESTSImpl();
		return requests;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RESERVED createRESERVED() {
		RESERVEDImpl reserved = new RESERVEDImpl();
		return reserved;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RESPONSE createRESPONSE() {
		RESPONSEImpl response = new RESPONSEImpl();
		return response;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ROLES createROLES() {
		ROLESImpl roles = new ROLESImpl();
		return roles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ROLES1 createROLES1() {
		ROLES1Impl roles1 = new ROLES1Impl();
		return roles1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SCALECONSTR createSCALECONSTR() {
		SCALECONSTRImpl scaleconstr = new SCALECONSTRImpl();
		return scaleconstr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SCALECONSTRS createSCALECONSTRS() {
		SCALECONSTRSImpl scaleconstrs = new SCALECONSTRSImpl();
		return scaleconstrs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SD createSD() {
		SDImpl sd = new SDImpl();
		return sd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SD1 createSD1() {
		SD1Impl sd1 = new SD1Impl();
		return sd1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDG createSDG() {
		SDGImpl sdg = new SDGImpl();
		return sdg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDG1 createSDG1() {
		SDG1Impl sdg1 = new SDG1Impl();
		return sdg1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGCAPTION createSDGCAPTION() {
		SDGCAPTIONImpl sdgcaption = new SDGCAPTIONImpl();
		return sdgcaption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGCAPTION1 createSDGCAPTION1() {
		SDGCAPTION1Impl sdgcaption1 = new SDGCAPTION1Impl();
		return sdgcaption1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS createSDGS() {
		SDGSImpl sdgs = new SDGSImpl();
		return sdgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS1 createSDGS1() {
		SDGS1Impl sdgs1 = new SDGS1Impl();
		return sdgs1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SECURITY createSECURITY() {
		SECURITYImpl security = new SECURITYImpl();
		return security;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SECURITYMETHOD createSECURITYMETHOD() {
		SECURITYMETHODImpl securitymethod = new SECURITYMETHODImpl();
		return securitymethod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SECURITYS createSECURITYS() {
		SECURITYSImpl securitys = new SECURITYSImpl();
		return securitys;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SEGMENT createSEGMENT() {
		SEGMENTImpl segment = new SEGMENTImpl();
		return segment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SEGMENTS createSEGMENTS() {
		SEGMENTSImpl segments = new SEGMENTSImpl();
		return segments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SELECTIONTABLEREFS createSELECTIONTABLEREFS() {
		SELECTIONTABLEREFSImpl selectiontablerefs = new SELECTIONTABLEREFSImpl();
		return selectiontablerefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SESSION createSESSION() {
		SESSIONImpl session = new SESSIONImpl();
		return session;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SESSIONDESC createSESSIONDESC() {
		SESSIONDESCImpl sessiondesc = new SESSIONDESCImpl();
		return sessiondesc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SESSIONDESCS createSESSIONDESCS() {
		SESSIONDESCSImpl sessiondescs = new SESSIONDESCSImpl();
		return sessiondescs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SESSIONS createSESSIONS() {
		SESSIONSImpl sessions = new SESSIONSImpl();
		return sessions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SINGLEECUJOB createSINGLEECUJOB() {
		SINGLEECUJOBImpl singleecujob = new SINGLEECUJOBImpl();
		return singleecujob;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SIZEDEFFILTER createSIZEDEFFILTER() {
		SIZEDEFFILTERImpl sizedeffilter = new SIZEDEFFILTERImpl();
		return sizedeffilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SIZEDEFPHYSSEGMENT createSIZEDEFPHYSSEGMENT() {
		SIZEDEFPHYSSEGMENTImpl sizedefphyssegment = new SIZEDEFPHYSSEGMENTImpl();
		return sizedefphyssegment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF createSNREF() {
		SNREFImpl snref = new SNREFImpl();
		return snref;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SOURCEENDADDRESS createSOURCEENDADDRESS() {
		SOURCEENDADDRESSImpl sourceendaddress = new SOURCEENDADDRESSImpl();
		return sourceendaddress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SPECIALDATA createSPECIALDATA() {
		SPECIALDATAImpl specialdata = new SPECIALDATAImpl();
		return specialdata;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SPECIALDATA1 createSPECIALDATA1() {
		SPECIALDATA1Impl specialdata1 = new SPECIALDATA1Impl();
		return specialdata1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public STANDARDLENGTHTYPE createSTANDARDLENGTHTYPE() {
		STANDARDLENGTHTYPEImpl standardlengthtype = new STANDARDLENGTHTYPEImpl();
		return standardlengthtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public STATICFIELD createSTATICFIELD() {
		STATICFIELDImpl staticfield = new STATICFIELDImpl();
		return staticfield;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public STATICFIELDS createSTATICFIELDS() {
		STATICFIELDSImpl staticfields = new STATICFIELDSImpl();
		return staticfields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public STRUCTURE createSTRUCTURE() {
		STRUCTUREImpl structure = new STRUCTUREImpl();
		return structure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public STRUCTURES createSTRUCTURES() {
		STRUCTURESImpl structures = new STRUCTURESImpl();
		return structures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SubType createSubType() {
		SubTypeImpl subType = new SubTypeImpl();
		return subType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SUPPORTEDDYNID createSUPPORTEDDYNID() {
		SUPPORTEDDYNIDImpl supporteddynid = new SUPPORTEDDYNIDImpl();
		return supporteddynid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SUPPORTEDDYNIDS createSUPPORTEDDYNIDS() {
		SUPPORTEDDYNIDSImpl supporteddynids = new SUPPORTEDDYNIDSImpl();
		return supporteddynids;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SupType createSupType() {
		SupTypeImpl supType = new SupTypeImpl();
		return supType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SWITCHKEY createSWITCHKEY() {
		SWITCHKEYImpl switchkey = new SWITCHKEYImpl();
		return switchkey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SWVARIABLE createSWVARIABLE() {
		SWVARIABLEImpl swvariable = new SWVARIABLEImpl();
		return swvariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SWVARIABLES createSWVARIABLES() {
		SWVARIABLESImpl swvariables = new SWVARIABLESImpl();
		return swvariables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SYSTEM createSYSTEM() {
		SYSTEMImpl system = new SYSTEMImpl();
		return system;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TABLE createTABLE() {
		TABLEImpl table = new TABLEImpl();
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TABLEENTRY createTABLEENTRY() {
		TABLEENTRYImpl tableentry = new TABLEENTRYImpl();
		return tableentry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TABLEKEY createTABLEKEY() {
		TABLEKEYImpl tablekey = new TABLEKEYImpl();
		return tablekey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TABLEROW createTABLEROW() {
		TABLEROWImpl tablerow = new TABLEROWImpl();
		return tablerow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TABLES createTABLES() {
		TABLESImpl tables = new TABLESImpl();
		return tables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TABLESTRUCT createTABLESTRUCT() {
		TABLESTRUCTImpl tablestruct = new TABLESTRUCTImpl();
		return tablestruct;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TARGETADDROFFSET createTARGETADDROFFSET() {
		TARGETADDROFFSETImpl targetaddroffset = new TARGETADDROFFSETImpl();
		return targetaddroffset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEAMMEMBER createTEAMMEMBER() {
		TEAMMEMBERImpl teammember = new TEAMMEMBERImpl();
		return teammember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEAMMEMBER1 createTEAMMEMBER1() {
		TEAMMEMBER1Impl teammember1 = new TEAMMEMBER1Impl();
		return teammember1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEAMMEMBERS createTEAMMEMBERS() {
		TEAMMEMBERSImpl teammembers = new TEAMMEMBERSImpl();
		return teammembers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEAMMEMBERS1 createTEAMMEMBERS1() {
		TEAMMEMBERS1Impl teammembers1 = new TEAMMEMBERS1Impl();
		return teammembers1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT createTEXT() {
		TEXTImpl text = new TEXTImpl();
		return text;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT1 createTEXT1() {
		TEXT1Impl text1 = new TEXT1Impl();
		return text1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UlType createUlType() {
		UlTypeImpl ulType = new UlTypeImpl();
		return ulType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNCOMPRESSEDSIZE createUNCOMPRESSEDSIZE() {
		UNCOMPRESSEDSIZEImpl uncompressedsize = new UNCOMPRESSEDSIZEImpl();
		return uncompressedsize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNIONVALUE createUNIONVALUE() {
		UNIONVALUEImpl unionvalue = new UNIONVALUEImpl();
		return unionvalue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNIT createUNIT() {
		UNITImpl unit = new UNITImpl();
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNITGROUP createUNITGROUP() {
		UNITGROUPImpl unitgroup = new UNITGROUPImpl();
		return unitgroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNITGROUPS createUNITGROUPS() {
		UNITGROUPSImpl unitgroups = new UNITGROUPSImpl();
		return unitgroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNITREFS createUNITREFS() {
		UNITREFSImpl unitrefs = new UNITREFSImpl();
		return unitrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNITS createUNITS() {
		UNITSImpl units = new UNITSImpl();
		return units;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNITSPEC createUNITSPEC() {
		UNITSPECImpl unitspec = new UNITSPECImpl();
		return unitspec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public V createV() {
		VImpl v = new VImpl();
		return v;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VALIDITYFOR createVALIDITYFOR() {
		VALIDITYFORImpl validityfor = new VALIDITYFORImpl();
		return validityfor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VALUE createVALUE() {
		VALUEImpl value = new VALUEImpl();
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VARIABLEGROUP createVARIABLEGROUP() {
		VARIABLEGROUPImpl variablegroup = new VARIABLEGROUPImpl();
		return variablegroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VARIABLEGROUPS createVARIABLEGROUPS() {
		VARIABLEGROUPSImpl variablegroups = new VARIABLEGROUPSImpl();
		return variablegroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLECONNECTOR createVEHICLECONNECTOR() {
		VEHICLECONNECTORImpl vehicleconnector = new VEHICLECONNECTORImpl();
		return vehicleconnector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLECONNECTORPIN createVEHICLECONNECTORPIN() {
		VEHICLECONNECTORPINImpl vehicleconnectorpin = new VEHICLECONNECTORPINImpl();
		return vehicleconnectorpin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLECONNECTORPINREFS createVEHICLECONNECTORPINREFS() {
		VEHICLECONNECTORPINREFSImpl vehicleconnectorpinrefs = new VEHICLECONNECTORPINREFSImpl();
		return vehicleconnectorpinrefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLECONNECTORPINS createVEHICLECONNECTORPINS() {
		VEHICLECONNECTORPINSImpl vehicleconnectorpins = new VEHICLECONNECTORPINSImpl();
		return vehicleconnectorpins;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLECONNECTORS createVEHICLECONNECTORS() {
		VEHICLECONNECTORSImpl vehicleconnectors = new VEHICLECONNECTORSImpl();
		return vehicleconnectors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLEINFORMATION createVEHICLEINFORMATION() {
		VEHICLEINFORMATIONImpl vehicleinformation = new VEHICLEINFORMATIONImpl();
		return vehicleinformation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLEINFORMATIONS createVEHICLEINFORMATIONS() {
		VEHICLEINFORMATIONSImpl vehicleinformations = new VEHICLEINFORMATIONSImpl();
		return vehicleinformations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLEINFOSPEC createVEHICLEINFOSPEC() {
		VEHICLEINFOSPECImpl vehicleinfospec = new VEHICLEINFOSPECImpl();
		return vehicleinfospec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLEMODEL createVEHICLEMODEL() {
		VEHICLEMODELImpl vehiclemodel = new VEHICLEMODELImpl();
		return vehiclemodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLETYPE createVEHICLETYPE() {
		VEHICLETYPEImpl vehicletype = new VEHICLETYPEImpl();
		return vehicletype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VT createVT() {
		VTImpl vt = new VTImpl();
		return vt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public XDOC createXDOC() {
		XDOCImpl xdoc = new XDOCImpl();
		return xdoc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public XDOC1 createXDOC1() {
		XDOC1Impl xdoc1 = new XDOC1Impl();
		return xdoc1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ADDRESSING createADDRESSINGFromString(EDataType eDataType, String initialValue) {
		ADDRESSING result = ADDRESSING.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertADDRESSINGToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlignType createAlignTypeFromString(EDataType eDataType, String initialValue) {
		AlignType result = AlignType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAlignTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COMMRELATIONVALUETYPE createCOMMRELATIONVALUETYPEFromString(EDataType eDataType, String initialValue) {
		COMMRELATIONVALUETYPE result = COMMRELATIONVALUETYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCOMMRELATIONVALUETYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COMPUCATEGORY createCOMPUCATEGORYFromString(EDataType eDataType, String initialValue) {
		COMPUCATEGORY result = COMPUCATEGORY.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCOMPUCATEGORYToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DATAFORMATSELECTION createDATAFORMATSELECTIONFromString(EDataType eDataType, String initialValue) {
		DATAFORMATSELECTION result = DATAFORMATSELECTION.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDATAFORMATSELECTIONToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DATATYPE createDATATYPEFromString(EDataType eDataType, String initialValue) {
		DATATYPE result = DATATYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDATATYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DIAGCLASSTYPE createDIAGCLASSTYPEFromString(EDataType eDataType, String initialValue) {
		DIAGCLASSTYPE result = DIAGCLASSTYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDIAGCLASSTYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DIRECTION createDIRECTIONFromString(EDataType eDataType, String initialValue) {
		DIRECTION result = DIRECTION.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDIRECTIONToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DOCTYPE createDOCTYPEFromString(EDataType eDataType, String initialValue) {
		DOCTYPE result = DOCTYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDOCTYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DOCTYPE1 createDOCTYPE1FromString(EDataType eDataType, String initialValue) {
		DOCTYPE1 result = DOCTYPE1.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDOCTYPE1ToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ENCODING createENCODINGFromString(EDataType eDataType, String initialValue) {
		ENCODING result = ENCODING.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertENCODINGToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ENCRYPTCOMPRESSMETHODTYPE createENCRYPTCOMPRESSMETHODTYPEFromString(EDataType eDataType, String initialValue) {
		ENCRYPTCOMPRESSMETHODTYPE result = ENCRYPTCOMPRESSMETHODTYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertENCRYPTCOMPRESSMETHODTYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IDENTVALUETYPE createIDENTVALUETYPEFromString(EDataType eDataType, String initialValue) {
		IDENTVALUETYPE result = IDENTVALUETYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIDENTVALUETYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public INTERVALTYPE createINTERVALTYPEFromString(EDataType eDataType, String initialValue) {
		INTERVALTYPE result = INTERVALTYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertINTERVALTYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PHYSICALDATATYPE createPHYSICALDATATYPEFromString(EDataType eDataType, String initialValue) {
		PHYSICALDATATYPE result = PHYSICALDATATYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPHYSICALDATATYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PHYSICALLINKTYPE createPHYSICALLINKTYPEFromString(EDataType eDataType, String initialValue) {
		PHYSICALLINKTYPE result = PHYSICALLINKTYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPHYSICALLINKTYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PINTYPE createPINTYPEFromString(EDataType eDataType, String initialValue) {
		PINTYPE result = PINTYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPINTYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PROJIDENTTYPE createPROJIDENTTYPEFromString(EDataType eDataType, String initialValue) {
		PROJIDENTTYPE result = PROJIDENTTYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPROJIDENTTYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RADIX createRADIXFromString(EDataType eDataType, String initialValue) {
		RADIX result = RADIX.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRADIXToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROWFRAGMENT createROWFRAGMENTFromString(EDataType eDataType, String initialValue) {
		ROWFRAGMENT result = ROWFRAGMENT.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertROWFRAGMENTToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SESSIONSUBELEMTYPE createSESSIONSUBELEMTYPEFromString(EDataType eDataType, String initialValue) {
		SESSIONSUBELEMTYPE result = SESSIONSUBELEMTYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSESSIONSUBELEMTYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public STANDARDISATIONLEVEL createSTANDARDISATIONLEVELFromString(EDataType eDataType, String initialValue) {
		STANDARDISATIONLEVEL result = STANDARDISATIONLEVEL.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSTANDARDISATIONLEVELToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TERMINATION createTERMINATIONFromString(EDataType eDataType, String initialValue) {
		TERMINATION result = TERMINATION.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTERMINATIONToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UNITGROUPCATEGORY createUNITGROUPCATEGORYFromString(EDataType eDataType, String initialValue) {
		UNITGROUPCATEGORY result = UNITGROUPCATEGORY.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUNITGROUPCATEGORYToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UPDSTATUS createUPDSTATUSFromString(EDataType eDataType, String initialValue) {
		UPDSTATUS result = UPDSTATUS.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUPDSTATUSToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VALIDTYPE createVALIDTYPEFromString(EDataType eDataType, String initialValue) {
		VALIDTYPE result = VALIDTYPE.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVALIDTYPEToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ADDRESSING createADDRESSINGObjectFromString(EDataType eDataType, String initialValue) {
		return createADDRESSINGFromString(OdxXhtmlPackage.eINSTANCE.getADDRESSING(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertADDRESSINGObjectToString(EDataType eDataType, Object instanceValue) {
		return convertADDRESSINGToString(OdxXhtmlPackage.eINSTANCE.getADDRESSING(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlignType createAlignTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createAlignTypeFromString(OdxXhtmlPackage.eINSTANCE.getAlignType(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAlignTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertAlignTypeToString(OdxXhtmlPackage.eINSTANCE.getAlignType(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COMMRELATIONVALUETYPE createCOMMRELATIONVALUETYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createCOMMRELATIONVALUETYPEFromString(OdxXhtmlPackage.eINSTANCE.getCOMMRELATIONVALUETYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCOMMRELATIONVALUETYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertCOMMRELATIONVALUETYPEToString(OdxXhtmlPackage.eINSTANCE.getCOMMRELATIONVALUETYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COMPUCATEGORY createCOMPUCATEGORYObjectFromString(EDataType eDataType, String initialValue) {
		return createCOMPUCATEGORYFromString(OdxXhtmlPackage.eINSTANCE.getCOMPUCATEGORY(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCOMPUCATEGORYObjectToString(EDataType eDataType, Object instanceValue) {
		return convertCOMPUCATEGORYToString(OdxXhtmlPackage.eINSTANCE.getCOMPUCATEGORY(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DATAFORMATSELECTION createDATAFORMATSELECTIONObjectFromString(EDataType eDataType, String initialValue) {
		return createDATAFORMATSELECTIONFromString(OdxXhtmlPackage.eINSTANCE.getDATAFORMATSELECTION(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDATAFORMATSELECTIONObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDATAFORMATSELECTIONToString(OdxXhtmlPackage.eINSTANCE.getDATAFORMATSELECTION(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DATATYPE createDATATYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createDATATYPEFromString(OdxXhtmlPackage.eINSTANCE.getDATATYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDATATYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDATATYPEToString(OdxXhtmlPackage.eINSTANCE.getDATATYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DIAGCLASSTYPE createDIAGCLASSTYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createDIAGCLASSTYPEFromString(OdxXhtmlPackage.eINSTANCE.getDIAGCLASSTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDIAGCLASSTYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDIAGCLASSTYPEToString(OdxXhtmlPackage.eINSTANCE.getDIAGCLASSTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DIRECTION createDIRECTIONObjectFromString(EDataType eDataType, String initialValue) {
		return createDIRECTIONFromString(OdxXhtmlPackage.eINSTANCE.getDIRECTION(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDIRECTIONObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDIRECTIONToString(OdxXhtmlPackage.eINSTANCE.getDIRECTION(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DOCTYPE createDOCTYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createDOCTYPEFromString(OdxXhtmlPackage.eINSTANCE.getDOCTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDOCTYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDOCTYPEToString(OdxXhtmlPackage.eINSTANCE.getDOCTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DOCTYPE createDOCTYPEObject1FromString(EDataType eDataType, String initialValue) {
		return createDOCTYPEFromString(OdxXhtmlPackage.eINSTANCE.getDOCTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDOCTYPEObject1ToString(EDataType eDataType, Object instanceValue) {
		return convertDOCTYPEToString(OdxXhtmlPackage.eINSTANCE.getDOCTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ENCODING createENCODINGObjectFromString(EDataType eDataType, String initialValue) {
		return createENCODINGFromString(OdxXhtmlPackage.eINSTANCE.getENCODING(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertENCODINGObjectToString(EDataType eDataType, Object instanceValue) {
		return convertENCODINGToString(OdxXhtmlPackage.eINSTANCE.getENCODING(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ENCRYPTCOMPRESSMETHODTYPE createENCRYPTCOMPRESSMETHODTYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createENCRYPTCOMPRESSMETHODTYPEFromString(OdxXhtmlPackage.eINSTANCE.getENCRYPTCOMPRESSMETHODTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertENCRYPTCOMPRESSMETHODTYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertENCRYPTCOMPRESSMETHODTYPEToString(OdxXhtmlPackage.eINSTANCE.getENCRYPTCOMPRESSMETHODTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IDENTVALUETYPE createIDENTVALUETYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createIDENTVALUETYPEFromString(OdxXhtmlPackage.eINSTANCE.getIDENTVALUETYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIDENTVALUETYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertIDENTVALUETYPEToString(OdxXhtmlPackage.eINSTANCE.getIDENTVALUETYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public INTERVALTYPE createINTERVALTYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createINTERVALTYPEFromString(OdxXhtmlPackage.eINSTANCE.getINTERVALTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertINTERVALTYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertINTERVALTYPEToString(OdxXhtmlPackage.eINSTANCE.getINTERVALTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PHYSICALDATATYPE createPHYSICALDATATYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createPHYSICALDATATYPEFromString(OdxXhtmlPackage.eINSTANCE.getPHYSICALDATATYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPHYSICALDATATYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertPHYSICALDATATYPEToString(OdxXhtmlPackage.eINSTANCE.getPHYSICALDATATYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PHYSICALLINKTYPE createPHYSICALLINKTYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createPHYSICALLINKTYPEFromString(OdxXhtmlPackage.eINSTANCE.getPHYSICALLINKTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPHYSICALLINKTYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertPHYSICALLINKTYPEToString(OdxXhtmlPackage.eINSTANCE.getPHYSICALLINKTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PINTYPE createPINTYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createPINTYPEFromString(OdxXhtmlPackage.eINSTANCE.getPINTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPINTYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertPINTYPEToString(OdxXhtmlPackage.eINSTANCE.getPINTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PROJIDENTTYPE createPROJIDENTTYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createPROJIDENTTYPEFromString(OdxXhtmlPackage.eINSTANCE.getPROJIDENTTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPROJIDENTTYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertPROJIDENTTYPEToString(OdxXhtmlPackage.eINSTANCE.getPROJIDENTTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RADIX createRADIXObjectFromString(EDataType eDataType, String initialValue) {
		return createRADIXFromString(OdxXhtmlPackage.eINSTANCE.getRADIX(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRADIXObjectToString(EDataType eDataType, Object instanceValue) {
		return convertRADIXToString(OdxXhtmlPackage.eINSTANCE.getRADIX(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROWFRAGMENT createROWFRAGMENTObjectFromString(EDataType eDataType, String initialValue) {
		return createROWFRAGMENTFromString(OdxXhtmlPackage.eINSTANCE.getROWFRAGMENT(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertROWFRAGMENTObjectToString(EDataType eDataType, Object instanceValue) {
		return convertROWFRAGMENTToString(OdxXhtmlPackage.eINSTANCE.getROWFRAGMENT(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SESSIONSUBELEMTYPE createSESSIONSUBELEMTYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createSESSIONSUBELEMTYPEFromString(OdxXhtmlPackage.eINSTANCE.getSESSIONSUBELEMTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSESSIONSUBELEMTYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertSESSIONSUBELEMTYPEToString(OdxXhtmlPackage.eINSTANCE.getSESSIONSUBELEMTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public STANDARDISATIONLEVEL createSTANDARDISATIONLEVELObjectFromString(EDataType eDataType, String initialValue) {
		return createSTANDARDISATIONLEVELFromString(OdxXhtmlPackage.eINSTANCE.getSTANDARDISATIONLEVEL(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSTANDARDISATIONLEVELObjectToString(EDataType eDataType, Object instanceValue) {
		return convertSTANDARDISATIONLEVELToString(OdxXhtmlPackage.eINSTANCE.getSTANDARDISATIONLEVEL(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TERMINATION createTERMINATIONObjectFromString(EDataType eDataType, String initialValue) {
		return createTERMINATIONFromString(OdxXhtmlPackage.eINSTANCE.getTERMINATION(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTERMINATIONObjectToString(EDataType eDataType, Object instanceValue) {
		return convertTERMINATIONToString(OdxXhtmlPackage.eINSTANCE.getTERMINATION(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UNITGROUPCATEGORY createUNITGROUPCATEGORYObjectFromString(EDataType eDataType, String initialValue) {
		return createUNITGROUPCATEGORYFromString(OdxXhtmlPackage.eINSTANCE.getUNITGROUPCATEGORY(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUNITGROUPCATEGORYObjectToString(EDataType eDataType, Object instanceValue) {
		return convertUNITGROUPCATEGORYToString(OdxXhtmlPackage.eINSTANCE.getUNITGROUPCATEGORY(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UPDSTATUS createUPDSTATUSObjectFromString(EDataType eDataType, String initialValue) {
		return createUPDSTATUSFromString(OdxXhtmlPackage.eINSTANCE.getUPDSTATUS(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUPDSTATUSObjectToString(EDataType eDataType, Object instanceValue) {
		return convertUPDSTATUSToString(OdxXhtmlPackage.eINSTANCE.getUPDSTATUS(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VALIDTYPE createVALIDTYPEObjectFromString(EDataType eDataType, String initialValue) {
		return createVALIDTYPEFromString(OdxXhtmlPackage.eINSTANCE.getVALIDTYPE(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVALIDTYPEObjectToString(EDataType eDataType, Object instanceValue) {
		return convertVALIDTYPEToString(OdxXhtmlPackage.eINSTANCE.getVALIDTYPE(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OdxXhtmlPackage getOdxXhtmlPackage() {
		return (OdxXhtmlPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OdxXhtmlPackage getPackage() {
		return OdxXhtmlPackage.eINSTANCE;
	}

} //OdxXhtmlFactoryImpl
