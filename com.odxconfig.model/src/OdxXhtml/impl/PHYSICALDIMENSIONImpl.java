/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSICALDIMENSION;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PHYSICALDIMENSION</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getLENGTHEXP <em>LENGTHEXP</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getMASSEXP <em>MASSEXP</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getTIMEEXP <em>TIMEEXP</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getCURRENTEXP <em>CURRENTEXP</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getTEMPERATUREEXP <em>TEMPERATUREEXP</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getMOLARAMOUNTEXP <em>MOLARAMOUNTEXP</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getLUMINOUSINTENSITYEXP <em>LUMINOUSINTENSITYEXP</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PHYSICALDIMENSIONImpl extends MinimalEObjectImpl.Container implements PHYSICALDIMENSION {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getLENGTHEXP() <em>LENGTHEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLENGTHEXP()
	 * @generated
	 * @ordered
	 */
	protected static final int LENGTHEXP_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLENGTHEXP() <em>LENGTHEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLENGTHEXP()
	 * @generated
	 * @ordered
	 */
	protected int lENGTHEXP = LENGTHEXP_EDEFAULT;

	/**
	 * This is true if the LENGTHEXP attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean lENGTHEXPESet;

	/**
	 * The default value of the '{@link #getMASSEXP() <em>MASSEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMASSEXP()
	 * @generated
	 * @ordered
	 */
	protected static final int MASSEXP_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMASSEXP() <em>MASSEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMASSEXP()
	 * @generated
	 * @ordered
	 */
	protected int mASSEXP = MASSEXP_EDEFAULT;

	/**
	 * This is true if the MASSEXP attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mASSEXPESet;

	/**
	 * The default value of the '{@link #getTIMEEXP() <em>TIMEEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTIMEEXP()
	 * @generated
	 * @ordered
	 */
	protected static final int TIMEEXP_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTIMEEXP() <em>TIMEEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTIMEEXP()
	 * @generated
	 * @ordered
	 */
	protected int tIMEEXP = TIMEEXP_EDEFAULT;

	/**
	 * This is true if the TIMEEXP attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tIMEEXPESet;

	/**
	 * The default value of the '{@link #getCURRENTEXP() <em>CURRENTEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCURRENTEXP()
	 * @generated
	 * @ordered
	 */
	protected static final int CURRENTEXP_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCURRENTEXP() <em>CURRENTEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCURRENTEXP()
	 * @generated
	 * @ordered
	 */
	protected int cURRENTEXP = CURRENTEXP_EDEFAULT;

	/**
	 * This is true if the CURRENTEXP attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean cURRENTEXPESet;

	/**
	 * The default value of the '{@link #getTEMPERATUREEXP() <em>TEMPERATUREEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTEMPERATUREEXP()
	 * @generated
	 * @ordered
	 */
	protected static final int TEMPERATUREEXP_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTEMPERATUREEXP() <em>TEMPERATUREEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTEMPERATUREEXP()
	 * @generated
	 * @ordered
	 */
	protected int tEMPERATUREEXP = TEMPERATUREEXP_EDEFAULT;

	/**
	 * This is true if the TEMPERATUREEXP attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tEMPERATUREEXPESet;

	/**
	 * The default value of the '{@link #getMOLARAMOUNTEXP() <em>MOLARAMOUNTEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMOLARAMOUNTEXP()
	 * @generated
	 * @ordered
	 */
	protected static final int MOLARAMOUNTEXP_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMOLARAMOUNTEXP() <em>MOLARAMOUNTEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMOLARAMOUNTEXP()
	 * @generated
	 * @ordered
	 */
	protected int mOLARAMOUNTEXP = MOLARAMOUNTEXP_EDEFAULT;

	/**
	 * This is true if the MOLARAMOUNTEXP attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mOLARAMOUNTEXPESet;

	/**
	 * The default value of the '{@link #getLUMINOUSINTENSITYEXP() <em>LUMINOUSINTENSITYEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLUMINOUSINTENSITYEXP()
	 * @generated
	 * @ordered
	 */
	protected static final int LUMINOUSINTENSITYEXP_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLUMINOUSINTENSITYEXP() <em>LUMINOUSINTENSITYEXP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLUMINOUSINTENSITYEXP()
	 * @generated
	 * @ordered
	 */
	protected int lUMINOUSINTENSITYEXP = LUMINOUSINTENSITYEXP_EDEFAULT;

	/**
	 * This is true if the LUMINOUSINTENSITYEXP attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean lUMINOUSINTENSITYEXPESet;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PHYSICALDIMENSIONImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPHYSICALDIMENSION();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALDIMENSION__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALDIMENSION__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALDIMENSION__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSICALDIMENSION__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getLENGTHEXP() {
		return lENGTHEXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLENGTHEXP(int newLENGTHEXP) {
		int oldLENGTHEXP = lENGTHEXP;
		lENGTHEXP = newLENGTHEXP;
		boolean oldLENGTHEXPESet = lENGTHEXPESet;
		lENGTHEXPESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__LENGTHEXP, oldLENGTHEXP, lENGTHEXP, !oldLENGTHEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetLENGTHEXP() {
		int oldLENGTHEXP = lENGTHEXP;
		boolean oldLENGTHEXPESet = lENGTHEXPESet;
		lENGTHEXP = LENGTHEXP_EDEFAULT;
		lENGTHEXPESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALDIMENSION__LENGTHEXP, oldLENGTHEXP, LENGTHEXP_EDEFAULT, oldLENGTHEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetLENGTHEXP() {
		return lENGTHEXPESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMASSEXP() {
		return mASSEXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMASSEXP(int newMASSEXP) {
		int oldMASSEXP = mASSEXP;
		mASSEXP = newMASSEXP;
		boolean oldMASSEXPESet = mASSEXPESet;
		mASSEXPESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__MASSEXP, oldMASSEXP, mASSEXP, !oldMASSEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetMASSEXP() {
		int oldMASSEXP = mASSEXP;
		boolean oldMASSEXPESet = mASSEXPESet;
		mASSEXP = MASSEXP_EDEFAULT;
		mASSEXPESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALDIMENSION__MASSEXP, oldMASSEXP, MASSEXP_EDEFAULT, oldMASSEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetMASSEXP() {
		return mASSEXPESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getTIMEEXP() {
		return tIMEEXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTIMEEXP(int newTIMEEXP) {
		int oldTIMEEXP = tIMEEXP;
		tIMEEXP = newTIMEEXP;
		boolean oldTIMEEXPESet = tIMEEXPESet;
		tIMEEXPESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__TIMEEXP, oldTIMEEXP, tIMEEXP, !oldTIMEEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetTIMEEXP() {
		int oldTIMEEXP = tIMEEXP;
		boolean oldTIMEEXPESet = tIMEEXPESet;
		tIMEEXP = TIMEEXP_EDEFAULT;
		tIMEEXPESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALDIMENSION__TIMEEXP, oldTIMEEXP, TIMEEXP_EDEFAULT, oldTIMEEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetTIMEEXP() {
		return tIMEEXPESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getCURRENTEXP() {
		return cURRENTEXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCURRENTEXP(int newCURRENTEXP) {
		int oldCURRENTEXP = cURRENTEXP;
		cURRENTEXP = newCURRENTEXP;
		boolean oldCURRENTEXPESet = cURRENTEXPESet;
		cURRENTEXPESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__CURRENTEXP, oldCURRENTEXP, cURRENTEXP, !oldCURRENTEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetCURRENTEXP() {
		int oldCURRENTEXP = cURRENTEXP;
		boolean oldCURRENTEXPESet = cURRENTEXPESet;
		cURRENTEXP = CURRENTEXP_EDEFAULT;
		cURRENTEXPESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALDIMENSION__CURRENTEXP, oldCURRENTEXP, CURRENTEXP_EDEFAULT, oldCURRENTEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetCURRENTEXP() {
		return cURRENTEXPESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getTEMPERATUREEXP() {
		return tEMPERATUREEXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTEMPERATUREEXP(int newTEMPERATUREEXP) {
		int oldTEMPERATUREEXP = tEMPERATUREEXP;
		tEMPERATUREEXP = newTEMPERATUREEXP;
		boolean oldTEMPERATUREEXPESet = tEMPERATUREEXPESet;
		tEMPERATUREEXPESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__TEMPERATUREEXP, oldTEMPERATUREEXP, tEMPERATUREEXP, !oldTEMPERATUREEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetTEMPERATUREEXP() {
		int oldTEMPERATUREEXP = tEMPERATUREEXP;
		boolean oldTEMPERATUREEXPESet = tEMPERATUREEXPESet;
		tEMPERATUREEXP = TEMPERATUREEXP_EDEFAULT;
		tEMPERATUREEXPESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALDIMENSION__TEMPERATUREEXP, oldTEMPERATUREEXP, TEMPERATUREEXP_EDEFAULT, oldTEMPERATUREEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetTEMPERATUREEXP() {
		return tEMPERATUREEXPESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMOLARAMOUNTEXP() {
		return mOLARAMOUNTEXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMOLARAMOUNTEXP(int newMOLARAMOUNTEXP) {
		int oldMOLARAMOUNTEXP = mOLARAMOUNTEXP;
		mOLARAMOUNTEXP = newMOLARAMOUNTEXP;
		boolean oldMOLARAMOUNTEXPESet = mOLARAMOUNTEXPESet;
		mOLARAMOUNTEXPESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__MOLARAMOUNTEXP, oldMOLARAMOUNTEXP, mOLARAMOUNTEXP, !oldMOLARAMOUNTEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetMOLARAMOUNTEXP() {
		int oldMOLARAMOUNTEXP = mOLARAMOUNTEXP;
		boolean oldMOLARAMOUNTEXPESet = mOLARAMOUNTEXPESet;
		mOLARAMOUNTEXP = MOLARAMOUNTEXP_EDEFAULT;
		mOLARAMOUNTEXPESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALDIMENSION__MOLARAMOUNTEXP, oldMOLARAMOUNTEXP, MOLARAMOUNTEXP_EDEFAULT, oldMOLARAMOUNTEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetMOLARAMOUNTEXP() {
		return mOLARAMOUNTEXPESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getLUMINOUSINTENSITYEXP() {
		return lUMINOUSINTENSITYEXP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLUMINOUSINTENSITYEXP(int newLUMINOUSINTENSITYEXP) {
		int oldLUMINOUSINTENSITYEXP = lUMINOUSINTENSITYEXP;
		lUMINOUSINTENSITYEXP = newLUMINOUSINTENSITYEXP;
		boolean oldLUMINOUSINTENSITYEXPESet = lUMINOUSINTENSITYEXPESet;
		lUMINOUSINTENSITYEXPESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__LUMINOUSINTENSITYEXP, oldLUMINOUSINTENSITYEXP, lUMINOUSINTENSITYEXP, !oldLUMINOUSINTENSITYEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetLUMINOUSINTENSITYEXP() {
		int oldLUMINOUSINTENSITYEXP = lUMINOUSINTENSITYEXP;
		boolean oldLUMINOUSINTENSITYEXPESet = lUMINOUSINTENSITYEXPESet;
		lUMINOUSINTENSITYEXP = LUMINOUSINTENSITYEXP_EDEFAULT;
		lUMINOUSINTENSITYEXPESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALDIMENSION__LUMINOUSINTENSITYEXP, oldLUMINOUSINTENSITYEXP, LUMINOUSINTENSITYEXP_EDEFAULT, oldLUMINOUSINTENSITYEXPESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetLUMINOUSINTENSITYEXP() {
		return lUMINOUSINTENSITYEXPESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALDIMENSION__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSION__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.PHYSICALDIMENSION__DESC:
				return basicSetDESC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSION__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.PHYSICALDIMENSION__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.PHYSICALDIMENSION__DESC:
				return getDESC();
			case OdxXhtmlPackage.PHYSICALDIMENSION__LENGTHEXP:
				return getLENGTHEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__MASSEXP:
				return getMASSEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__TIMEEXP:
				return getTIMEEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__CURRENTEXP:
				return getCURRENTEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__TEMPERATUREEXP:
				return getTEMPERATUREEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__MOLARAMOUNTEXP:
				return getMOLARAMOUNTEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__LUMINOUSINTENSITYEXP:
				return getLUMINOUSINTENSITYEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__ID:
				return getID();
			case OdxXhtmlPackage.PHYSICALDIMENSION__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSION__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__LENGTHEXP:
				setLENGTHEXP((Integer)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__MASSEXP:
				setMASSEXP((Integer)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__TIMEEXP:
				setTIMEEXP((Integer)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__CURRENTEXP:
				setCURRENTEXP((Integer)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__TEMPERATUREEXP:
				setTEMPERATUREEXP((Integer)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__MOLARAMOUNTEXP:
				setMOLARAMOUNTEXP((Integer)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__LUMINOUSINTENSITYEXP:
				setLUMINOUSINTENSITYEXP((Integer)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSION__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__LENGTHEXP:
				unsetLENGTHEXP();
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__MASSEXP:
				unsetMASSEXP();
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__TIMEEXP:
				unsetTIMEEXP();
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__CURRENTEXP:
				unsetCURRENTEXP();
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__TEMPERATUREEXP:
				unsetTEMPERATUREEXP();
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__MOLARAMOUNTEXP:
				unsetMOLARAMOUNTEXP();
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__LUMINOUSINTENSITYEXP:
				unsetLUMINOUSINTENSITYEXP();
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSICALDIMENSION__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSION__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.PHYSICALDIMENSION__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.PHYSICALDIMENSION__DESC:
				return dESC != null;
			case OdxXhtmlPackage.PHYSICALDIMENSION__LENGTHEXP:
				return isSetLENGTHEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__MASSEXP:
				return isSetMASSEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__TIMEEXP:
				return isSetTIMEEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__CURRENTEXP:
				return isSetCURRENTEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__TEMPERATUREEXP:
				return isSetTEMPERATUREEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__MOLARAMOUNTEXP:
				return isSetMOLARAMOUNTEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__LUMINOUSINTENSITYEXP:
				return isSetLUMINOUSINTENSITYEXP();
			case OdxXhtmlPackage.PHYSICALDIMENSION__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.PHYSICALDIMENSION__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", lENGTHEXP: ");
		if (lENGTHEXPESet) result.append(lENGTHEXP); else result.append("<unset>");
		result.append(", mASSEXP: ");
		if (mASSEXPESet) result.append(mASSEXP); else result.append("<unset>");
		result.append(", tIMEEXP: ");
		if (tIMEEXPESet) result.append(tIMEEXP); else result.append("<unset>");
		result.append(", cURRENTEXP: ");
		if (cURRENTEXPESet) result.append(cURRENTEXP); else result.append("<unset>");
		result.append(", tEMPERATUREEXP: ");
		if (tEMPERATUREEXPESet) result.append(tEMPERATUREEXP); else result.append("<unset>");
		result.append(", mOLARAMOUNTEXP: ");
		if (mOLARAMOUNTEXPESet) result.append(mOLARAMOUNTEXP); else result.append("<unset>");
		result.append(", lUMINOUSINTENSITYEXP: ");
		if (lUMINOUSINTENSITYEXPESet) result.append(lUMINOUSINTENSITYEXP); else result.append("<unset>");
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //PHYSICALDIMENSIONImpl
