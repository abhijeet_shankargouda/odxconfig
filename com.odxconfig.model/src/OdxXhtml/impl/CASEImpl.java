/**
 */
package OdxXhtml.impl;

import OdxXhtml.CASE;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.LIMIT;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CASE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.CASEImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.CASEImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.CASEImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.CASEImpl#getSTRUCTUREREF <em>STRUCTUREREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.CASEImpl#getLOWERLIMIT <em>LOWERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.impl.CASEImpl#getUPPERLIMIT <em>UPPERLIMIT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CASEImpl extends MinimalEObjectImpl.Container implements CASE {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getSTRUCTUREREF() <em>STRUCTUREREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTRUCTUREREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK sTRUCTUREREF;

	/**
	 * The cached value of the '{@link #getLOWERLIMIT() <em>LOWERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLOWERLIMIT()
	 * @generated
	 * @ordered
	 */
	protected LIMIT lOWERLIMIT;

	/**
	 * The cached value of the '{@link #getUPPERLIMIT() <em>UPPERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUPPERLIMIT()
	 * @generated
	 * @ordered
	 */
	protected LIMIT uPPERLIMIT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CASEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCASE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getSTRUCTUREREF() {
		return sTRUCTUREREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSTRUCTUREREF(ODXLINK newSTRUCTUREREF, NotificationChain msgs) {
		ODXLINK oldSTRUCTUREREF = sTRUCTUREREF;
		sTRUCTUREREF = newSTRUCTUREREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__STRUCTUREREF, oldSTRUCTUREREF, newSTRUCTUREREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSTRUCTUREREF(ODXLINK newSTRUCTUREREF) {
		if (newSTRUCTUREREF != sTRUCTUREREF) {
			NotificationChain msgs = null;
			if (sTRUCTUREREF != null)
				msgs = ((InternalEObject)sTRUCTUREREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__STRUCTUREREF, null, msgs);
			if (newSTRUCTUREREF != null)
				msgs = ((InternalEObject)newSTRUCTUREREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__STRUCTUREREF, null, msgs);
			msgs = basicSetSTRUCTUREREF(newSTRUCTUREREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__STRUCTUREREF, newSTRUCTUREREF, newSTRUCTUREREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LIMIT getLOWERLIMIT() {
		return lOWERLIMIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLOWERLIMIT(LIMIT newLOWERLIMIT, NotificationChain msgs) {
		LIMIT oldLOWERLIMIT = lOWERLIMIT;
		lOWERLIMIT = newLOWERLIMIT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__LOWERLIMIT, oldLOWERLIMIT, newLOWERLIMIT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLOWERLIMIT(LIMIT newLOWERLIMIT) {
		if (newLOWERLIMIT != lOWERLIMIT) {
			NotificationChain msgs = null;
			if (lOWERLIMIT != null)
				msgs = ((InternalEObject)lOWERLIMIT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__LOWERLIMIT, null, msgs);
			if (newLOWERLIMIT != null)
				msgs = ((InternalEObject)newLOWERLIMIT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__LOWERLIMIT, null, msgs);
			msgs = basicSetLOWERLIMIT(newLOWERLIMIT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__LOWERLIMIT, newLOWERLIMIT, newLOWERLIMIT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LIMIT getUPPERLIMIT() {
		return uPPERLIMIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUPPERLIMIT(LIMIT newUPPERLIMIT, NotificationChain msgs) {
		LIMIT oldUPPERLIMIT = uPPERLIMIT;
		uPPERLIMIT = newUPPERLIMIT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__UPPERLIMIT, oldUPPERLIMIT, newUPPERLIMIT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUPPERLIMIT(LIMIT newUPPERLIMIT) {
		if (newUPPERLIMIT != uPPERLIMIT) {
			NotificationChain msgs = null;
			if (uPPERLIMIT != null)
				msgs = ((InternalEObject)uPPERLIMIT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__UPPERLIMIT, null, msgs);
			if (newUPPERLIMIT != null)
				msgs = ((InternalEObject)newUPPERLIMIT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CASE__UPPERLIMIT, null, msgs);
			msgs = basicSetUPPERLIMIT(newUPPERLIMIT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CASE__UPPERLIMIT, newUPPERLIMIT, newUPPERLIMIT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.CASE__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.CASE__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.CASE__STRUCTUREREF:
				return basicSetSTRUCTUREREF(null, msgs);
			case OdxXhtmlPackage.CASE__LOWERLIMIT:
				return basicSetLOWERLIMIT(null, msgs);
			case OdxXhtmlPackage.CASE__UPPERLIMIT:
				return basicSetUPPERLIMIT(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.CASE__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.CASE__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.CASE__DESC:
				return getDESC();
			case OdxXhtmlPackage.CASE__STRUCTUREREF:
				return getSTRUCTUREREF();
			case OdxXhtmlPackage.CASE__LOWERLIMIT:
				return getLOWERLIMIT();
			case OdxXhtmlPackage.CASE__UPPERLIMIT:
				return getUPPERLIMIT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.CASE__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.CASE__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.CASE__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.CASE__STRUCTUREREF:
				setSTRUCTUREREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.CASE__LOWERLIMIT:
				setLOWERLIMIT((LIMIT)newValue);
				return;
			case OdxXhtmlPackage.CASE__UPPERLIMIT:
				setUPPERLIMIT((LIMIT)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CASE__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.CASE__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.CASE__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.CASE__STRUCTUREREF:
				setSTRUCTUREREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.CASE__LOWERLIMIT:
				setLOWERLIMIT((LIMIT)null);
				return;
			case OdxXhtmlPackage.CASE__UPPERLIMIT:
				setUPPERLIMIT((LIMIT)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CASE__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.CASE__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.CASE__DESC:
				return dESC != null;
			case OdxXhtmlPackage.CASE__STRUCTUREREF:
				return sTRUCTUREREF != null;
			case OdxXhtmlPackage.CASE__LOWERLIMIT:
				return lOWERLIMIT != null;
			case OdxXhtmlPackage.CASE__UPPERLIMIT:
				return uPPERLIMIT != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(')');
		return result.toString();
	}

} //CASEImpl
