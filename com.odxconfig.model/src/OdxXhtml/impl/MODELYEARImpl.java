/**
 */
package OdxXhtml.impl;

import OdxXhtml.MODELYEAR;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MODELYEAR</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MODELYEARImpl extends INFOCOMPONENTImpl implements MODELYEAR {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MODELYEARImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMODELYEAR();
	}

} //MODELYEARImpl
