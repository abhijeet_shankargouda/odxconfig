/**
 */
package OdxXhtml.impl;

import OdxXhtml.LOGICALLINKREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LOGICALLINKREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKREFSImpl#getLOGICALLINKREF <em>LOGICALLINKREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LOGICALLINKREFSImpl extends MinimalEObjectImpl.Container implements LOGICALLINKREFS {
	/**
	 * The cached value of the '{@link #getLOGICALLINKREF() <em>LOGICALLINKREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLOGICALLINKREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> lOGICALLINKREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LOGICALLINKREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLOGICALLINKREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getLOGICALLINKREF() {
		if (lOGICALLINKREF == null) {
			lOGICALLINKREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.LOGICALLINKREFS__LOGICALLINKREF);
		}
		return lOGICALLINKREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKREFS__LOGICALLINKREF:
				return ((InternalEList<?>)getLOGICALLINKREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKREFS__LOGICALLINKREF:
				return getLOGICALLINKREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKREFS__LOGICALLINKREF:
				getLOGICALLINKREF().clear();
				getLOGICALLINKREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKREFS__LOGICALLINKREF:
				getLOGICALLINKREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINKREFS__LOGICALLINKREF:
				return lOGICALLINKREF != null && !lOGICALLINKREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LOGICALLINKREFSImpl
