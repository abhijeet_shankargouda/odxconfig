/**
 */
package OdxXhtml.impl;

import OdxXhtml.NOTINHERITEDDIAGCOMM;
import OdxXhtml.NOTINHERITEDDIAGCOMMS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NOTINHERITEDDIAGCOMMS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.NOTINHERITEDDIAGCOMMSImpl#getNOTINHERITEDDIAGCOMM <em>NOTINHERITEDDIAGCOMM</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NOTINHERITEDDIAGCOMMSImpl extends MinimalEObjectImpl.Container implements NOTINHERITEDDIAGCOMMS {
	/**
	 * The cached value of the '{@link #getNOTINHERITEDDIAGCOMM() <em>NOTINHERITEDDIAGCOMM</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNOTINHERITEDDIAGCOMM()
	 * @generated
	 * @ordered
	 */
	protected EList<NOTINHERITEDDIAGCOMM> nOTINHERITEDDIAGCOMM;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NOTINHERITEDDIAGCOMMSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getNOTINHERITEDDIAGCOMMS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<NOTINHERITEDDIAGCOMM> getNOTINHERITEDDIAGCOMM() {
		if (nOTINHERITEDDIAGCOMM == null) {
			nOTINHERITEDDIAGCOMM = new EObjectContainmentEList<NOTINHERITEDDIAGCOMM>(NOTINHERITEDDIAGCOMM.class, this, OdxXhtmlPackage.NOTINHERITEDDIAGCOMMS__NOTINHERITEDDIAGCOMM);
		}
		return nOTINHERITEDDIAGCOMM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDDIAGCOMMS__NOTINHERITEDDIAGCOMM:
				return ((InternalEList<?>)getNOTINHERITEDDIAGCOMM()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDDIAGCOMMS__NOTINHERITEDDIAGCOMM:
				return getNOTINHERITEDDIAGCOMM();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDDIAGCOMMS__NOTINHERITEDDIAGCOMM:
				getNOTINHERITEDDIAGCOMM().clear();
				getNOTINHERITEDDIAGCOMM().addAll((Collection<? extends NOTINHERITEDDIAGCOMM>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDDIAGCOMMS__NOTINHERITEDDIAGCOMM:
				getNOTINHERITEDDIAGCOMM().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDDIAGCOMMS__NOTINHERITEDDIAGCOMM:
				return nOTINHERITEDDIAGCOMM != null && !nOTINHERITEDDIAGCOMM.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //NOTINHERITEDDIAGCOMMSImpl
