/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPUMETHOD;
import OdxXhtml.DATAOBJECTPROP;
import OdxXhtml.DIAGCODEDTYPE;
import OdxXhtml.INTERNALCONSTR;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSICALTYPE;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DATAOBJECTPROP</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DATAOBJECTPROPImpl#getCOMPUMETHOD <em>COMPUMETHOD</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATAOBJECTPROPImpl#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATAOBJECTPROPImpl#getPHYSICALTYPE <em>PHYSICALTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATAOBJECTPROPImpl#getINTERNALCONSTR <em>INTERNALCONSTR</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATAOBJECTPROPImpl#getUNITREF <em>UNITREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DATAOBJECTPROPImpl extends DOPBASEImpl implements DATAOBJECTPROP {
	/**
	 * The cached value of the '{@link #getCOMPUMETHOD() <em>COMPUMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUMETHOD()
	 * @generated
	 * @ordered
	 */
	protected COMPUMETHOD cOMPUMETHOD;

	/**
	 * The cached value of the '{@link #getDIAGCODEDTYPE() <em>DIAGCODEDTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCODEDTYPE()
	 * @generated
	 * @ordered
	 */
	protected DIAGCODEDTYPE dIAGCODEDTYPE;

	/**
	 * The cached value of the '{@link #getPHYSICALTYPE() <em>PHYSICALTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALTYPE()
	 * @generated
	 * @ordered
	 */
	protected PHYSICALTYPE pHYSICALTYPE;

	/**
	 * The cached value of the '{@link #getINTERNALCONSTR() <em>INTERNALCONSTR</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINTERNALCONSTR()
	 * @generated
	 * @ordered
	 */
	protected INTERNALCONSTR iNTERNALCONSTR;

	/**
	 * The cached value of the '{@link #getUNITREF() <em>UNITREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUNITREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK uNITREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DATAOBJECTPROPImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDATAOBJECTPROP();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUMETHOD getCOMPUMETHOD() {
		return cOMPUMETHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUMETHOD(COMPUMETHOD newCOMPUMETHOD, NotificationChain msgs) {
		COMPUMETHOD oldCOMPUMETHOD = cOMPUMETHOD;
		cOMPUMETHOD = newCOMPUMETHOD;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__COMPUMETHOD, oldCOMPUMETHOD, newCOMPUMETHOD);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUMETHOD(COMPUMETHOD newCOMPUMETHOD) {
		if (newCOMPUMETHOD != cOMPUMETHOD) {
			NotificationChain msgs = null;
			if (cOMPUMETHOD != null)
				msgs = ((InternalEObject)cOMPUMETHOD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__COMPUMETHOD, null, msgs);
			if (newCOMPUMETHOD != null)
				msgs = ((InternalEObject)newCOMPUMETHOD).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__COMPUMETHOD, null, msgs);
			msgs = basicSetCOMPUMETHOD(newCOMPUMETHOD, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__COMPUMETHOD, newCOMPUMETHOD, newCOMPUMETHOD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGCODEDTYPE getDIAGCODEDTYPE() {
		return dIAGCODEDTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCODEDTYPE(DIAGCODEDTYPE newDIAGCODEDTYPE, NotificationChain msgs) {
		DIAGCODEDTYPE oldDIAGCODEDTYPE = dIAGCODEDTYPE;
		dIAGCODEDTYPE = newDIAGCODEDTYPE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__DIAGCODEDTYPE, oldDIAGCODEDTYPE, newDIAGCODEDTYPE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCODEDTYPE(DIAGCODEDTYPE newDIAGCODEDTYPE) {
		if (newDIAGCODEDTYPE != dIAGCODEDTYPE) {
			NotificationChain msgs = null;
			if (dIAGCODEDTYPE != null)
				msgs = ((InternalEObject)dIAGCODEDTYPE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__DIAGCODEDTYPE, null, msgs);
			if (newDIAGCODEDTYPE != null)
				msgs = ((InternalEObject)newDIAGCODEDTYPE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__DIAGCODEDTYPE, null, msgs);
			msgs = basicSetDIAGCODEDTYPE(newDIAGCODEDTYPE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__DIAGCODEDTYPE, newDIAGCODEDTYPE, newDIAGCODEDTYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALTYPE getPHYSICALTYPE() {
		return pHYSICALTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPHYSICALTYPE(PHYSICALTYPE newPHYSICALTYPE, NotificationChain msgs) {
		PHYSICALTYPE oldPHYSICALTYPE = pHYSICALTYPE;
		pHYSICALTYPE = newPHYSICALTYPE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__PHYSICALTYPE, oldPHYSICALTYPE, newPHYSICALTYPE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHYSICALTYPE(PHYSICALTYPE newPHYSICALTYPE) {
		if (newPHYSICALTYPE != pHYSICALTYPE) {
			NotificationChain msgs = null;
			if (pHYSICALTYPE != null)
				msgs = ((InternalEObject)pHYSICALTYPE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__PHYSICALTYPE, null, msgs);
			if (newPHYSICALTYPE != null)
				msgs = ((InternalEObject)newPHYSICALTYPE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__PHYSICALTYPE, null, msgs);
			msgs = basicSetPHYSICALTYPE(newPHYSICALTYPE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__PHYSICALTYPE, newPHYSICALTYPE, newPHYSICALTYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INTERNALCONSTR getINTERNALCONSTR() {
		return iNTERNALCONSTR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetINTERNALCONSTR(INTERNALCONSTR newINTERNALCONSTR, NotificationChain msgs) {
		INTERNALCONSTR oldINTERNALCONSTR = iNTERNALCONSTR;
		iNTERNALCONSTR = newINTERNALCONSTR;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__INTERNALCONSTR, oldINTERNALCONSTR, newINTERNALCONSTR);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setINTERNALCONSTR(INTERNALCONSTR newINTERNALCONSTR) {
		if (newINTERNALCONSTR != iNTERNALCONSTR) {
			NotificationChain msgs = null;
			if (iNTERNALCONSTR != null)
				msgs = ((InternalEObject)iNTERNALCONSTR).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__INTERNALCONSTR, null, msgs);
			if (newINTERNALCONSTR != null)
				msgs = ((InternalEObject)newINTERNALCONSTR).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__INTERNALCONSTR, null, msgs);
			msgs = basicSetINTERNALCONSTR(newINTERNALCONSTR, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__INTERNALCONSTR, newINTERNALCONSTR, newINTERNALCONSTR));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getUNITREF() {
		return uNITREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUNITREF(ODXLINK newUNITREF, NotificationChain msgs) {
		ODXLINK oldUNITREF = uNITREF;
		uNITREF = newUNITREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__UNITREF, oldUNITREF, newUNITREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUNITREF(ODXLINK newUNITREF) {
		if (newUNITREF != uNITREF) {
			NotificationChain msgs = null;
			if (uNITREF != null)
				msgs = ((InternalEObject)uNITREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__UNITREF, null, msgs);
			if (newUNITREF != null)
				msgs = ((InternalEObject)newUNITREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATAOBJECTPROP__UNITREF, null, msgs);
			msgs = basicSetUNITREF(newUNITREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAOBJECTPROP__UNITREF, newUNITREF, newUNITREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROP__COMPUMETHOD:
				return basicSetCOMPUMETHOD(null, msgs);
			case OdxXhtmlPackage.DATAOBJECTPROP__DIAGCODEDTYPE:
				return basicSetDIAGCODEDTYPE(null, msgs);
			case OdxXhtmlPackage.DATAOBJECTPROP__PHYSICALTYPE:
				return basicSetPHYSICALTYPE(null, msgs);
			case OdxXhtmlPackage.DATAOBJECTPROP__INTERNALCONSTR:
				return basicSetINTERNALCONSTR(null, msgs);
			case OdxXhtmlPackage.DATAOBJECTPROP__UNITREF:
				return basicSetUNITREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROP__COMPUMETHOD:
				return getCOMPUMETHOD();
			case OdxXhtmlPackage.DATAOBJECTPROP__DIAGCODEDTYPE:
				return getDIAGCODEDTYPE();
			case OdxXhtmlPackage.DATAOBJECTPROP__PHYSICALTYPE:
				return getPHYSICALTYPE();
			case OdxXhtmlPackage.DATAOBJECTPROP__INTERNALCONSTR:
				return getINTERNALCONSTR();
			case OdxXhtmlPackage.DATAOBJECTPROP__UNITREF:
				return getUNITREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROP__COMPUMETHOD:
				setCOMPUMETHOD((COMPUMETHOD)newValue);
				return;
			case OdxXhtmlPackage.DATAOBJECTPROP__DIAGCODEDTYPE:
				setDIAGCODEDTYPE((DIAGCODEDTYPE)newValue);
				return;
			case OdxXhtmlPackage.DATAOBJECTPROP__PHYSICALTYPE:
				setPHYSICALTYPE((PHYSICALTYPE)newValue);
				return;
			case OdxXhtmlPackage.DATAOBJECTPROP__INTERNALCONSTR:
				setINTERNALCONSTR((INTERNALCONSTR)newValue);
				return;
			case OdxXhtmlPackage.DATAOBJECTPROP__UNITREF:
				setUNITREF((ODXLINK)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROP__COMPUMETHOD:
				setCOMPUMETHOD((COMPUMETHOD)null);
				return;
			case OdxXhtmlPackage.DATAOBJECTPROP__DIAGCODEDTYPE:
				setDIAGCODEDTYPE((DIAGCODEDTYPE)null);
				return;
			case OdxXhtmlPackage.DATAOBJECTPROP__PHYSICALTYPE:
				setPHYSICALTYPE((PHYSICALTYPE)null);
				return;
			case OdxXhtmlPackage.DATAOBJECTPROP__INTERNALCONSTR:
				setINTERNALCONSTR((INTERNALCONSTR)null);
				return;
			case OdxXhtmlPackage.DATAOBJECTPROP__UNITREF:
				setUNITREF((ODXLINK)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROP__COMPUMETHOD:
				return cOMPUMETHOD != null;
			case OdxXhtmlPackage.DATAOBJECTPROP__DIAGCODEDTYPE:
				return dIAGCODEDTYPE != null;
			case OdxXhtmlPackage.DATAOBJECTPROP__PHYSICALTYPE:
				return pHYSICALTYPE != null;
			case OdxXhtmlPackage.DATAOBJECTPROP__INTERNALCONSTR:
				return iNTERNALCONSTR != null;
			case OdxXhtmlPackage.DATAOBJECTPROP__UNITREF:
				return uNITREF != null;
		}
		return super.eIsSet(featureID);
	}

} //DATAOBJECTPROPImpl
