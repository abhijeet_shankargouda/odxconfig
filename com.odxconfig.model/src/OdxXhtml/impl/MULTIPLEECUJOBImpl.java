/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.AUDIENCE;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.DIAGLAYERREFS;
import OdxXhtml.FUNCTCLASSREFS;
import OdxXhtml.INPUTPARAMS;
import OdxXhtml.MULTIPLEECUJOB;
import OdxXhtml.NEGOUTPUTPARAMS;
import OdxXhtml.OUTPUTPARAMS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROGCODES;
import OdxXhtml.SDGS;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MULTIPLEECUJOB</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getFUNCTCLASSREFS <em>FUNCTCLASSREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getPROGCODES <em>PROGCODES</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getINPUTPARAMS <em>INPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getOUTPUTPARAMS <em>OUTPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getNEGOUTPUTPARAMS <em>NEGOUTPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getDIAGLAYERREFS <em>DIAGLAYERREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getAUDIENCE <em>AUDIENCE</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#isISEXECUTABLE <em>ISEXECUTABLE</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBImpl#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MULTIPLEECUJOBImpl extends MinimalEObjectImpl.Container implements MULTIPLEECUJOB {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getSDGS() <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGS()
	 * @generated
	 * @ordered
	 */
	protected SDGS sDGS;

	/**
	 * The cached value of the '{@link #getFUNCTCLASSREFS() <em>FUNCTCLASSREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFUNCTCLASSREFS()
	 * @generated
	 * @ordered
	 */
	protected FUNCTCLASSREFS fUNCTCLASSREFS;

	/**
	 * The cached value of the '{@link #getPROGCODES() <em>PROGCODES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGCODES()
	 * @generated
	 * @ordered
	 */
	protected PROGCODES pROGCODES;

	/**
	 * The cached value of the '{@link #getINPUTPARAMS() <em>INPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINPUTPARAMS()
	 * @generated
	 * @ordered
	 */
	protected INPUTPARAMS iNPUTPARAMS;

	/**
	 * The cached value of the '{@link #getOUTPUTPARAMS() <em>OUTPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOUTPUTPARAMS()
	 * @generated
	 * @ordered
	 */
	protected OUTPUTPARAMS oUTPUTPARAMS;

	/**
	 * The cached value of the '{@link #getNEGOUTPUTPARAMS() <em>NEGOUTPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNEGOUTPUTPARAMS()
	 * @generated
	 * @ordered
	 */
	protected NEGOUTPUTPARAMS nEGOUTPUTPARAMS;

	/**
	 * The cached value of the '{@link #getDIAGLAYERREFS() <em>DIAGLAYERREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGLAYERREFS()
	 * @generated
	 * @ordered
	 */
	protected DIAGLAYERREFS dIAGLAYERREFS;

	/**
	 * The cached value of the '{@link #getAUDIENCE() <em>AUDIENCE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUDIENCE()
	 * @generated
	 * @ordered
	 */
	protected AUDIENCE aUDIENCE;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isISEXECUTABLE() <em>ISEXECUTABLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISEXECUTABLE()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISEXECUTABLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isISEXECUTABLE() <em>ISEXECUTABLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISEXECUTABLE()
	 * @generated
	 * @ordered
	 */
	protected boolean iSEXECUTABLE = ISEXECUTABLE_EDEFAULT;

	/**
	 * This is true if the ISEXECUTABLE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSEXECUTABLEESet;

	/**
	 * The default value of the '{@link #isISREDUCEDRESULTENABLED() <em>ISREDUCEDRESULTENABLED</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISREDUCEDRESULTENABLED()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISREDUCEDRESULTENABLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISREDUCEDRESULTENABLED() <em>ISREDUCEDRESULTENABLED</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISREDUCEDRESULTENABLED()
	 * @generated
	 * @ordered
	 */
	protected boolean iSREDUCEDRESULTENABLED = ISREDUCEDRESULTENABLED_EDEFAULT;

	/**
	 * This is true if the ISREDUCEDRESULTENABLED attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSREDUCEDRESULTENABLEDESet;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSECURITYACCESSLEVEL() <em>SECURITYACCESSLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSECURITYACCESSLEVEL()
	 * @generated
	 * @ordered
	 */
	protected static final long SECURITYACCESSLEVEL_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getSECURITYACCESSLEVEL() <em>SECURITYACCESSLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSECURITYACCESSLEVEL()
	 * @generated
	 * @ordered
	 */
	protected long sECURITYACCESSLEVEL = SECURITYACCESSLEVEL_EDEFAULT;

	/**
	 * This is true if the SECURITYACCESSLEVEL attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sECURITYACCESSLEVELESet;

	/**
	 * The default value of the '{@link #getSEMANTIC() <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSEMANTIC()
	 * @generated
	 * @ordered
	 */
	protected static final String SEMANTIC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSEMANTIC() <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSEMANTIC()
	 * @generated
	 * @ordered
	 */
	protected String sEMANTIC = SEMANTIC_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MULTIPLEECUJOBImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMULTIPLEECUJOB();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS getSDGS() {
		return sDGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGS(SDGS newSDGS, NotificationChain msgs) {
		SDGS oldSDGS = sDGS;
		sDGS = newSDGS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__SDGS, oldSDGS, newSDGS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGS(SDGS newSDGS) {
		if (newSDGS != sDGS) {
			NotificationChain msgs = null;
			if (sDGS != null)
				msgs = ((InternalEObject)sDGS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__SDGS, null, msgs);
			if (newSDGS != null)
				msgs = ((InternalEObject)newSDGS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__SDGS, null, msgs);
			msgs = basicSetSDGS(newSDGS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__SDGS, newSDGS, newSDGS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTCLASSREFS getFUNCTCLASSREFS() {
		return fUNCTCLASSREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFUNCTCLASSREFS(FUNCTCLASSREFS newFUNCTCLASSREFS, NotificationChain msgs) {
		FUNCTCLASSREFS oldFUNCTCLASSREFS = fUNCTCLASSREFS;
		fUNCTCLASSREFS = newFUNCTCLASSREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__FUNCTCLASSREFS, oldFUNCTCLASSREFS, newFUNCTCLASSREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFUNCTCLASSREFS(FUNCTCLASSREFS newFUNCTCLASSREFS) {
		if (newFUNCTCLASSREFS != fUNCTCLASSREFS) {
			NotificationChain msgs = null;
			if (fUNCTCLASSREFS != null)
				msgs = ((InternalEObject)fUNCTCLASSREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__FUNCTCLASSREFS, null, msgs);
			if (newFUNCTCLASSREFS != null)
				msgs = ((InternalEObject)newFUNCTCLASSREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__FUNCTCLASSREFS, null, msgs);
			msgs = basicSetFUNCTCLASSREFS(newFUNCTCLASSREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__FUNCTCLASSREFS, newFUNCTCLASSREFS, newFUNCTCLASSREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROGCODES getPROGCODES() {
		return pROGCODES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROGCODES(PROGCODES newPROGCODES, NotificationChain msgs) {
		PROGCODES oldPROGCODES = pROGCODES;
		pROGCODES = newPROGCODES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__PROGCODES, oldPROGCODES, newPROGCODES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROGCODES(PROGCODES newPROGCODES) {
		if (newPROGCODES != pROGCODES) {
			NotificationChain msgs = null;
			if (pROGCODES != null)
				msgs = ((InternalEObject)pROGCODES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__PROGCODES, null, msgs);
			if (newPROGCODES != null)
				msgs = ((InternalEObject)newPROGCODES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__PROGCODES, null, msgs);
			msgs = basicSetPROGCODES(newPROGCODES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__PROGCODES, newPROGCODES, newPROGCODES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INPUTPARAMS getINPUTPARAMS() {
		return iNPUTPARAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetINPUTPARAMS(INPUTPARAMS newINPUTPARAMS, NotificationChain msgs) {
		INPUTPARAMS oldINPUTPARAMS = iNPUTPARAMS;
		iNPUTPARAMS = newINPUTPARAMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__INPUTPARAMS, oldINPUTPARAMS, newINPUTPARAMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setINPUTPARAMS(INPUTPARAMS newINPUTPARAMS) {
		if (newINPUTPARAMS != iNPUTPARAMS) {
			NotificationChain msgs = null;
			if (iNPUTPARAMS != null)
				msgs = ((InternalEObject)iNPUTPARAMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__INPUTPARAMS, null, msgs);
			if (newINPUTPARAMS != null)
				msgs = ((InternalEObject)newINPUTPARAMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__INPUTPARAMS, null, msgs);
			msgs = basicSetINPUTPARAMS(newINPUTPARAMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__INPUTPARAMS, newINPUTPARAMS, newINPUTPARAMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OUTPUTPARAMS getOUTPUTPARAMS() {
		return oUTPUTPARAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOUTPUTPARAMS(OUTPUTPARAMS newOUTPUTPARAMS, NotificationChain msgs) {
		OUTPUTPARAMS oldOUTPUTPARAMS = oUTPUTPARAMS;
		oUTPUTPARAMS = newOUTPUTPARAMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__OUTPUTPARAMS, oldOUTPUTPARAMS, newOUTPUTPARAMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOUTPUTPARAMS(OUTPUTPARAMS newOUTPUTPARAMS) {
		if (newOUTPUTPARAMS != oUTPUTPARAMS) {
			NotificationChain msgs = null;
			if (oUTPUTPARAMS != null)
				msgs = ((InternalEObject)oUTPUTPARAMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__OUTPUTPARAMS, null, msgs);
			if (newOUTPUTPARAMS != null)
				msgs = ((InternalEObject)newOUTPUTPARAMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__OUTPUTPARAMS, null, msgs);
			msgs = basicSetOUTPUTPARAMS(newOUTPUTPARAMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__OUTPUTPARAMS, newOUTPUTPARAMS, newOUTPUTPARAMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGOUTPUTPARAMS getNEGOUTPUTPARAMS() {
		return nEGOUTPUTPARAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNEGOUTPUTPARAMS(NEGOUTPUTPARAMS newNEGOUTPUTPARAMS, NotificationChain msgs) {
		NEGOUTPUTPARAMS oldNEGOUTPUTPARAMS = nEGOUTPUTPARAMS;
		nEGOUTPUTPARAMS = newNEGOUTPUTPARAMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__NEGOUTPUTPARAMS, oldNEGOUTPUTPARAMS, newNEGOUTPUTPARAMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNEGOUTPUTPARAMS(NEGOUTPUTPARAMS newNEGOUTPUTPARAMS) {
		if (newNEGOUTPUTPARAMS != nEGOUTPUTPARAMS) {
			NotificationChain msgs = null;
			if (nEGOUTPUTPARAMS != null)
				msgs = ((InternalEObject)nEGOUTPUTPARAMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__NEGOUTPUTPARAMS, null, msgs);
			if (newNEGOUTPUTPARAMS != null)
				msgs = ((InternalEObject)newNEGOUTPUTPARAMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__NEGOUTPUTPARAMS, null, msgs);
			msgs = basicSetNEGOUTPUTPARAMS(newNEGOUTPUTPARAMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__NEGOUTPUTPARAMS, newNEGOUTPUTPARAMS, newNEGOUTPUTPARAMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGLAYERREFS getDIAGLAYERREFS() {
		return dIAGLAYERREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGLAYERREFS(DIAGLAYERREFS newDIAGLAYERREFS, NotificationChain msgs) {
		DIAGLAYERREFS oldDIAGLAYERREFS = dIAGLAYERREFS;
		dIAGLAYERREFS = newDIAGLAYERREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__DIAGLAYERREFS, oldDIAGLAYERREFS, newDIAGLAYERREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGLAYERREFS(DIAGLAYERREFS newDIAGLAYERREFS) {
		if (newDIAGLAYERREFS != dIAGLAYERREFS) {
			NotificationChain msgs = null;
			if (dIAGLAYERREFS != null)
				msgs = ((InternalEObject)dIAGLAYERREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__DIAGLAYERREFS, null, msgs);
			if (newDIAGLAYERREFS != null)
				msgs = ((InternalEObject)newDIAGLAYERREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__DIAGLAYERREFS, null, msgs);
			msgs = basicSetDIAGLAYERREFS(newDIAGLAYERREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__DIAGLAYERREFS, newDIAGLAYERREFS, newDIAGLAYERREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AUDIENCE getAUDIENCE() {
		return aUDIENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAUDIENCE(AUDIENCE newAUDIENCE, NotificationChain msgs) {
		AUDIENCE oldAUDIENCE = aUDIENCE;
		aUDIENCE = newAUDIENCE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__AUDIENCE, oldAUDIENCE, newAUDIENCE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAUDIENCE(AUDIENCE newAUDIENCE) {
		if (newAUDIENCE != aUDIENCE) {
			NotificationChain msgs = null;
			if (aUDIENCE != null)
				msgs = ((InternalEObject)aUDIENCE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__AUDIENCE, null, msgs);
			if (newAUDIENCE != null)
				msgs = ((InternalEObject)newAUDIENCE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOB__AUDIENCE, null, msgs);
			msgs = basicSetAUDIENCE(newAUDIENCE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__AUDIENCE, newAUDIENCE, newAUDIENCE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISEXECUTABLE() {
		return iSEXECUTABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISEXECUTABLE(boolean newISEXECUTABLE) {
		boolean oldISEXECUTABLE = iSEXECUTABLE;
		iSEXECUTABLE = newISEXECUTABLE;
		boolean oldISEXECUTABLEESet = iSEXECUTABLEESet;
		iSEXECUTABLEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__ISEXECUTABLE, oldISEXECUTABLE, iSEXECUTABLE, !oldISEXECUTABLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISEXECUTABLE() {
		boolean oldISEXECUTABLE = iSEXECUTABLE;
		boolean oldISEXECUTABLEESet = iSEXECUTABLEESet;
		iSEXECUTABLE = ISEXECUTABLE_EDEFAULT;
		iSEXECUTABLEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MULTIPLEECUJOB__ISEXECUTABLE, oldISEXECUTABLE, ISEXECUTABLE_EDEFAULT, oldISEXECUTABLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISEXECUTABLE() {
		return iSEXECUTABLEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISREDUCEDRESULTENABLED() {
		return iSREDUCEDRESULTENABLED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISREDUCEDRESULTENABLED(boolean newISREDUCEDRESULTENABLED) {
		boolean oldISREDUCEDRESULTENABLED = iSREDUCEDRESULTENABLED;
		iSREDUCEDRESULTENABLED = newISREDUCEDRESULTENABLED;
		boolean oldISREDUCEDRESULTENABLEDESet = iSREDUCEDRESULTENABLEDESet;
		iSREDUCEDRESULTENABLEDESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__ISREDUCEDRESULTENABLED, oldISREDUCEDRESULTENABLED, iSREDUCEDRESULTENABLED, !oldISREDUCEDRESULTENABLEDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISREDUCEDRESULTENABLED() {
		boolean oldISREDUCEDRESULTENABLED = iSREDUCEDRESULTENABLED;
		boolean oldISREDUCEDRESULTENABLEDESet = iSREDUCEDRESULTENABLEDESet;
		iSREDUCEDRESULTENABLED = ISREDUCEDRESULTENABLED_EDEFAULT;
		iSREDUCEDRESULTENABLEDESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MULTIPLEECUJOB__ISREDUCEDRESULTENABLED, oldISREDUCEDRESULTENABLED, ISREDUCEDRESULTENABLED_EDEFAULT, oldISREDUCEDRESULTENABLEDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISREDUCEDRESULTENABLED() {
		return iSREDUCEDRESULTENABLEDESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getSECURITYACCESSLEVEL() {
		return sECURITYACCESSLEVEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSECURITYACCESSLEVEL(long newSECURITYACCESSLEVEL) {
		long oldSECURITYACCESSLEVEL = sECURITYACCESSLEVEL;
		sECURITYACCESSLEVEL = newSECURITYACCESSLEVEL;
		boolean oldSECURITYACCESSLEVELESet = sECURITYACCESSLEVELESet;
		sECURITYACCESSLEVELESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__SECURITYACCESSLEVEL, oldSECURITYACCESSLEVEL, sECURITYACCESSLEVEL, !oldSECURITYACCESSLEVELESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetSECURITYACCESSLEVEL() {
		long oldSECURITYACCESSLEVEL = sECURITYACCESSLEVEL;
		boolean oldSECURITYACCESSLEVELESet = sECURITYACCESSLEVELESet;
		sECURITYACCESSLEVEL = SECURITYACCESSLEVEL_EDEFAULT;
		sECURITYACCESSLEVELESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MULTIPLEECUJOB__SECURITYACCESSLEVEL, oldSECURITYACCESSLEVEL, SECURITYACCESSLEVEL_EDEFAULT, oldSECURITYACCESSLEVELESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetSECURITYACCESSLEVEL() {
		return sECURITYACCESSLEVELESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSEMANTIC() {
		return sEMANTIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSEMANTIC(String newSEMANTIC) {
		String oldSEMANTIC = sEMANTIC;
		sEMANTIC = newSEMANTIC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOB__SEMANTIC, oldSEMANTIC, sEMANTIC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOB__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__SDGS:
				return basicSetSDGS(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__FUNCTCLASSREFS:
				return basicSetFUNCTCLASSREFS(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__PROGCODES:
				return basicSetPROGCODES(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__INPUTPARAMS:
				return basicSetINPUTPARAMS(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__OUTPUTPARAMS:
				return basicSetOUTPUTPARAMS(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__NEGOUTPUTPARAMS:
				return basicSetNEGOUTPUTPARAMS(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__DIAGLAYERREFS:
				return basicSetDIAGLAYERREFS(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOB__AUDIENCE:
				return basicSetAUDIENCE(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOB__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.MULTIPLEECUJOB__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.MULTIPLEECUJOB__DESC:
				return getDESC();
			case OdxXhtmlPackage.MULTIPLEECUJOB__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.MULTIPLEECUJOB__SDGS:
				return getSDGS();
			case OdxXhtmlPackage.MULTIPLEECUJOB__FUNCTCLASSREFS:
				return getFUNCTCLASSREFS();
			case OdxXhtmlPackage.MULTIPLEECUJOB__PROGCODES:
				return getPROGCODES();
			case OdxXhtmlPackage.MULTIPLEECUJOB__INPUTPARAMS:
				return getINPUTPARAMS();
			case OdxXhtmlPackage.MULTIPLEECUJOB__OUTPUTPARAMS:
				return getOUTPUTPARAMS();
			case OdxXhtmlPackage.MULTIPLEECUJOB__NEGOUTPUTPARAMS:
				return getNEGOUTPUTPARAMS();
			case OdxXhtmlPackage.MULTIPLEECUJOB__DIAGLAYERREFS:
				return getDIAGLAYERREFS();
			case OdxXhtmlPackage.MULTIPLEECUJOB__AUDIENCE:
				return getAUDIENCE();
			case OdxXhtmlPackage.MULTIPLEECUJOB__ID:
				return getID();
			case OdxXhtmlPackage.MULTIPLEECUJOB__ISEXECUTABLE:
				return isISEXECUTABLE();
			case OdxXhtmlPackage.MULTIPLEECUJOB__ISREDUCEDRESULTENABLED:
				return isISREDUCEDRESULTENABLED();
			case OdxXhtmlPackage.MULTIPLEECUJOB__OID:
				return getOID();
			case OdxXhtmlPackage.MULTIPLEECUJOB__SECURITYACCESSLEVEL:
				return getSECURITYACCESSLEVEL();
			case OdxXhtmlPackage.MULTIPLEECUJOB__SEMANTIC:
				return getSEMANTIC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOB__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__SDGS:
				setSDGS((SDGS)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__FUNCTCLASSREFS:
				setFUNCTCLASSREFS((FUNCTCLASSREFS)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__PROGCODES:
				setPROGCODES((PROGCODES)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__INPUTPARAMS:
				setINPUTPARAMS((INPUTPARAMS)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__OUTPUTPARAMS:
				setOUTPUTPARAMS((OUTPUTPARAMS)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__NEGOUTPUTPARAMS:
				setNEGOUTPUTPARAMS((NEGOUTPUTPARAMS)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__DIAGLAYERREFS:
				setDIAGLAYERREFS((DIAGLAYERREFS)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__AUDIENCE:
				setAUDIENCE((AUDIENCE)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ISEXECUTABLE:
				setISEXECUTABLE((Boolean)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ISREDUCEDRESULTENABLED:
				setISREDUCEDRESULTENABLED((Boolean)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__OID:
				setOID((String)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__SECURITYACCESSLEVEL:
				setSECURITYACCESSLEVEL((Long)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__SEMANTIC:
				setSEMANTIC((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOB__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__SDGS:
				setSDGS((SDGS)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__FUNCTCLASSREFS:
				setFUNCTCLASSREFS((FUNCTCLASSREFS)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__PROGCODES:
				setPROGCODES((PROGCODES)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__INPUTPARAMS:
				setINPUTPARAMS((INPUTPARAMS)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__OUTPUTPARAMS:
				setOUTPUTPARAMS((OUTPUTPARAMS)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__NEGOUTPUTPARAMS:
				setNEGOUTPUTPARAMS((NEGOUTPUTPARAMS)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__DIAGLAYERREFS:
				setDIAGLAYERREFS((DIAGLAYERREFS)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__AUDIENCE:
				setAUDIENCE((AUDIENCE)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ISEXECUTABLE:
				unsetISEXECUTABLE();
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ISREDUCEDRESULTENABLED:
				unsetISREDUCEDRESULTENABLED();
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__OID:
				setOID(OID_EDEFAULT);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__SECURITYACCESSLEVEL:
				unsetSECURITYACCESSLEVEL();
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOB__SEMANTIC:
				setSEMANTIC(SEMANTIC_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOB__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.MULTIPLEECUJOB__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__DESC:
				return dESC != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__SDGS:
				return sDGS != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__FUNCTCLASSREFS:
				return fUNCTCLASSREFS != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__PROGCODES:
				return pROGCODES != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__INPUTPARAMS:
				return iNPUTPARAMS != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__OUTPUTPARAMS:
				return oUTPUTPARAMS != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__NEGOUTPUTPARAMS:
				return nEGOUTPUTPARAMS != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__DIAGLAYERREFS:
				return dIAGLAYERREFS != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__AUDIENCE:
				return aUDIENCE != null;
			case OdxXhtmlPackage.MULTIPLEECUJOB__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.MULTIPLEECUJOB__ISEXECUTABLE:
				return isSetISEXECUTABLE();
			case OdxXhtmlPackage.MULTIPLEECUJOB__ISREDUCEDRESULTENABLED:
				return isSetISREDUCEDRESULTENABLED();
			case OdxXhtmlPackage.MULTIPLEECUJOB__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
			case OdxXhtmlPackage.MULTIPLEECUJOB__SECURITYACCESSLEVEL:
				return isSetSECURITYACCESSLEVEL();
			case OdxXhtmlPackage.MULTIPLEECUJOB__SEMANTIC:
				return SEMANTIC_EDEFAULT == null ? sEMANTIC != null : !SEMANTIC_EDEFAULT.equals(sEMANTIC);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", iSEXECUTABLE: ");
		if (iSEXECUTABLEESet) result.append(iSEXECUTABLE); else result.append("<unset>");
		result.append(", iSREDUCEDRESULTENABLED: ");
		if (iSREDUCEDRESULTENABLEDESet) result.append(iSREDUCEDRESULTENABLED); else result.append("<unset>");
		result.append(", oID: ");
		result.append(oID);
		result.append(", sECURITYACCESSLEVEL: ");
		if (sECURITYACCESSLEVELESet) result.append(sECURITYACCESSLEVEL); else result.append("<unset>");
		result.append(", sEMANTIC: ");
		result.append(sEMANTIC);
		result.append(')');
		return result.toString();
	}

} //MULTIPLEECUJOBImpl
