/**
 */
package OdxXhtml.impl;

import OdxXhtml.MATCHINGCOMPONENT;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MATCHINGCOMPONENT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MATCHINGCOMPONENTImpl#getEXPECTEDVALUE <em>EXPECTEDVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.MATCHINGCOMPONENTImpl#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.MATCHINGCOMPONENTImpl#getMULTIPLEECUJOBREF <em>MULTIPLEECUJOBREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.MATCHINGCOMPONENTImpl#getDIAGCOMMREF <em>DIAGCOMMREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MATCHINGCOMPONENTImpl extends MinimalEObjectImpl.Container implements MATCHINGCOMPONENT {
	/**
	 * The default value of the '{@link #getEXPECTEDVALUE() <em>EXPECTEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEXPECTEDVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPECTEDVALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEXPECTEDVALUE() <em>EXPECTEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEXPECTEDVALUE()
	 * @generated
	 * @ordered
	 */
	protected String eXPECTEDVALUE = EXPECTEDVALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOUTPARAMIFSNREF() <em>OUTPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOUTPARAMIFSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF oUTPARAMIFSNREF;

	/**
	 * The cached value of the '{@link #getMULTIPLEECUJOBREF() <em>MULTIPLEECUJOBREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMULTIPLEECUJOBREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK mULTIPLEECUJOBREF;

	/**
	 * The cached value of the '{@link #getDIAGCOMMREF() <em>DIAGCOMMREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCOMMREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK dIAGCOMMREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MATCHINGCOMPONENTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMATCHINGCOMPONENT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getEXPECTEDVALUE() {
		return eXPECTEDVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEXPECTEDVALUE(String newEXPECTEDVALUE) {
		String oldEXPECTEDVALUE = eXPECTEDVALUE;
		eXPECTEDVALUE = newEXPECTEDVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGCOMPONENT__EXPECTEDVALUE, oldEXPECTEDVALUE, eXPECTEDVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getOUTPARAMIFSNREF() {
		return oUTPARAMIFSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOUTPARAMIFSNREF(SNREF newOUTPARAMIFSNREF, NotificationChain msgs) {
		SNREF oldOUTPARAMIFSNREF = oUTPARAMIFSNREF;
		oUTPARAMIFSNREF = newOUTPARAMIFSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGCOMPONENT__OUTPARAMIFSNREF, oldOUTPARAMIFSNREF, newOUTPARAMIFSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOUTPARAMIFSNREF(SNREF newOUTPARAMIFSNREF) {
		if (newOUTPARAMIFSNREF != oUTPARAMIFSNREF) {
			NotificationChain msgs = null;
			if (oUTPARAMIFSNREF != null)
				msgs = ((InternalEObject)oUTPARAMIFSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGCOMPONENT__OUTPARAMIFSNREF, null, msgs);
			if (newOUTPARAMIFSNREF != null)
				msgs = ((InternalEObject)newOUTPARAMIFSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGCOMPONENT__OUTPARAMIFSNREF, null, msgs);
			msgs = basicSetOUTPARAMIFSNREF(newOUTPARAMIFSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGCOMPONENT__OUTPARAMIFSNREF, newOUTPARAMIFSNREF, newOUTPARAMIFSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getMULTIPLEECUJOBREF() {
		return mULTIPLEECUJOBREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMULTIPLEECUJOBREF(ODXLINK newMULTIPLEECUJOBREF, NotificationChain msgs) {
		ODXLINK oldMULTIPLEECUJOBREF = mULTIPLEECUJOBREF;
		mULTIPLEECUJOBREF = newMULTIPLEECUJOBREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGCOMPONENT__MULTIPLEECUJOBREF, oldMULTIPLEECUJOBREF, newMULTIPLEECUJOBREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMULTIPLEECUJOBREF(ODXLINK newMULTIPLEECUJOBREF) {
		if (newMULTIPLEECUJOBREF != mULTIPLEECUJOBREF) {
			NotificationChain msgs = null;
			if (mULTIPLEECUJOBREF != null)
				msgs = ((InternalEObject)mULTIPLEECUJOBREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGCOMPONENT__MULTIPLEECUJOBREF, null, msgs);
			if (newMULTIPLEECUJOBREF != null)
				msgs = ((InternalEObject)newMULTIPLEECUJOBREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGCOMPONENT__MULTIPLEECUJOBREF, null, msgs);
			msgs = basicSetMULTIPLEECUJOBREF(newMULTIPLEECUJOBREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGCOMPONENT__MULTIPLEECUJOBREF, newMULTIPLEECUJOBREF, newMULTIPLEECUJOBREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getDIAGCOMMREF() {
		return dIAGCOMMREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCOMMREF(ODXLINK newDIAGCOMMREF, NotificationChain msgs) {
		ODXLINK oldDIAGCOMMREF = dIAGCOMMREF;
		dIAGCOMMREF = newDIAGCOMMREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGCOMPONENT__DIAGCOMMREF, oldDIAGCOMMREF, newDIAGCOMMREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCOMMREF(ODXLINK newDIAGCOMMREF) {
		if (newDIAGCOMMREF != dIAGCOMMREF) {
			NotificationChain msgs = null;
			if (dIAGCOMMREF != null)
				msgs = ((InternalEObject)dIAGCOMMREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGCOMPONENT__DIAGCOMMREF, null, msgs);
			if (newDIAGCOMMREF != null)
				msgs = ((InternalEObject)newDIAGCOMMREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MATCHINGCOMPONENT__DIAGCOMMREF, null, msgs);
			msgs = basicSetDIAGCOMMREF(newDIAGCOMMREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGCOMPONENT__DIAGCOMMREF, newDIAGCOMMREF, newDIAGCOMMREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENT__OUTPARAMIFSNREF:
				return basicSetOUTPARAMIFSNREF(null, msgs);
			case OdxXhtmlPackage.MATCHINGCOMPONENT__MULTIPLEECUJOBREF:
				return basicSetMULTIPLEECUJOBREF(null, msgs);
			case OdxXhtmlPackage.MATCHINGCOMPONENT__DIAGCOMMREF:
				return basicSetDIAGCOMMREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENT__EXPECTEDVALUE:
				return getEXPECTEDVALUE();
			case OdxXhtmlPackage.MATCHINGCOMPONENT__OUTPARAMIFSNREF:
				return getOUTPARAMIFSNREF();
			case OdxXhtmlPackage.MATCHINGCOMPONENT__MULTIPLEECUJOBREF:
				return getMULTIPLEECUJOBREF();
			case OdxXhtmlPackage.MATCHINGCOMPONENT__DIAGCOMMREF:
				return getDIAGCOMMREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENT__EXPECTEDVALUE:
				setEXPECTEDVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.MATCHINGCOMPONENT__OUTPARAMIFSNREF:
				setOUTPARAMIFSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.MATCHINGCOMPONENT__MULTIPLEECUJOBREF:
				setMULTIPLEECUJOBREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.MATCHINGCOMPONENT__DIAGCOMMREF:
				setDIAGCOMMREF((ODXLINK)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENT__EXPECTEDVALUE:
				setEXPECTEDVALUE(EXPECTEDVALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.MATCHINGCOMPONENT__OUTPARAMIFSNREF:
				setOUTPARAMIFSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.MATCHINGCOMPONENT__MULTIPLEECUJOBREF:
				setMULTIPLEECUJOBREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.MATCHINGCOMPONENT__DIAGCOMMREF:
				setDIAGCOMMREF((ODXLINK)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENT__EXPECTEDVALUE:
				return EXPECTEDVALUE_EDEFAULT == null ? eXPECTEDVALUE != null : !EXPECTEDVALUE_EDEFAULT.equals(eXPECTEDVALUE);
			case OdxXhtmlPackage.MATCHINGCOMPONENT__OUTPARAMIFSNREF:
				return oUTPARAMIFSNREF != null;
			case OdxXhtmlPackage.MATCHINGCOMPONENT__MULTIPLEECUJOBREF:
				return mULTIPLEECUJOBREF != null;
			case OdxXhtmlPackage.MATCHINGCOMPONENT__DIAGCOMMREF:
				return dIAGCOMMREF != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (eXPECTEDVALUE: ");
		result.append(eXPECTEDVALUE);
		result.append(')');
		return result.toString();
	}

} //MATCHINGCOMPONENTImpl
