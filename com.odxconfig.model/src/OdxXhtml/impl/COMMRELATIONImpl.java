/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMMRELATION;
import OdxXhtml.COMMRELATIONVALUETYPE;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMMRELATION</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMMRELATIONImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMMRELATIONImpl#getRELATIONTYPE <em>RELATIONTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMMRELATIONImpl#getDIAGCOMMREF <em>DIAGCOMMREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMMRELATIONImpl#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMMRELATIONImpl#getINPARAMIFSNREF <em>INPARAMIFSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMMRELATIONImpl#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMMRELATIONImpl#getVALUETYPE <em>VALUETYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMMRELATIONImpl extends MinimalEObjectImpl.Container implements COMMRELATION {
	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getRELATIONTYPE() <em>RELATIONTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRELATIONTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final String RELATIONTYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRELATIONTYPE() <em>RELATIONTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRELATIONTYPE()
	 * @generated
	 * @ordered
	 */
	protected String rELATIONTYPE = RELATIONTYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDIAGCOMMREF() <em>DIAGCOMMREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCOMMREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK dIAGCOMMREF;

	/**
	 * The cached value of the '{@link #getDIAGCOMMSNREF() <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dIAGCOMMSNREF;

	/**
	 * The cached value of the '{@link #getINPARAMIFSNREF() <em>INPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINPARAMIFSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF iNPARAMIFSNREF;

	/**
	 * The cached value of the '{@link #getOUTPARAMIFSNREF() <em>OUTPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOUTPARAMIFSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF oUTPARAMIFSNREF;

	/**
	 * The default value of the '{@link #getVALUETYPE() <em>VALUETYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUETYPE()
	 * @generated
	 * @ordered
	 */
	protected static final COMMRELATIONVALUETYPE VALUETYPE_EDEFAULT = COMMRELATIONVALUETYPE.CURRENT;

	/**
	 * The cached value of the '{@link #getVALUETYPE() <em>VALUETYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUETYPE()
	 * @generated
	 * @ordered
	 */
	protected COMMRELATIONVALUETYPE vALUETYPE = VALUETYPE_EDEFAULT;

	/**
	 * This is true if the VALUETYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean vALUETYPEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMMRELATIONImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMMRELATION();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRELATIONTYPE() {
		return rELATIONTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRELATIONTYPE(String newRELATIONTYPE) {
		String oldRELATIONTYPE = rELATIONTYPE;
		rELATIONTYPE = newRELATIONTYPE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__RELATIONTYPE, oldRELATIONTYPE, rELATIONTYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getDIAGCOMMREF() {
		return dIAGCOMMREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCOMMREF(ODXLINK newDIAGCOMMREF, NotificationChain msgs) {
		ODXLINK oldDIAGCOMMREF = dIAGCOMMREF;
		dIAGCOMMREF = newDIAGCOMMREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__DIAGCOMMREF, oldDIAGCOMMREF, newDIAGCOMMREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCOMMREF(ODXLINK newDIAGCOMMREF) {
		if (newDIAGCOMMREF != dIAGCOMMREF) {
			NotificationChain msgs = null;
			if (dIAGCOMMREF != null)
				msgs = ((InternalEObject)dIAGCOMMREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__DIAGCOMMREF, null, msgs);
			if (newDIAGCOMMREF != null)
				msgs = ((InternalEObject)newDIAGCOMMREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__DIAGCOMMREF, null, msgs);
			msgs = basicSetDIAGCOMMREF(newDIAGCOMMREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__DIAGCOMMREF, newDIAGCOMMREF, newDIAGCOMMREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDIAGCOMMSNREF() {
		return dIAGCOMMSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF, NotificationChain msgs) {
		SNREF oldDIAGCOMMSNREF = dIAGCOMMSNREF;
		dIAGCOMMSNREF = newDIAGCOMMSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__DIAGCOMMSNREF, oldDIAGCOMMSNREF, newDIAGCOMMSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF) {
		if (newDIAGCOMMSNREF != dIAGCOMMSNREF) {
			NotificationChain msgs = null;
			if (dIAGCOMMSNREF != null)
				msgs = ((InternalEObject)dIAGCOMMSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__DIAGCOMMSNREF, null, msgs);
			if (newDIAGCOMMSNREF != null)
				msgs = ((InternalEObject)newDIAGCOMMSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__DIAGCOMMSNREF, null, msgs);
			msgs = basicSetDIAGCOMMSNREF(newDIAGCOMMSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__DIAGCOMMSNREF, newDIAGCOMMSNREF, newDIAGCOMMSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getINPARAMIFSNREF() {
		return iNPARAMIFSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetINPARAMIFSNREF(SNREF newINPARAMIFSNREF, NotificationChain msgs) {
		SNREF oldINPARAMIFSNREF = iNPARAMIFSNREF;
		iNPARAMIFSNREF = newINPARAMIFSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__INPARAMIFSNREF, oldINPARAMIFSNREF, newINPARAMIFSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setINPARAMIFSNREF(SNREF newINPARAMIFSNREF) {
		if (newINPARAMIFSNREF != iNPARAMIFSNREF) {
			NotificationChain msgs = null;
			if (iNPARAMIFSNREF != null)
				msgs = ((InternalEObject)iNPARAMIFSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__INPARAMIFSNREF, null, msgs);
			if (newINPARAMIFSNREF != null)
				msgs = ((InternalEObject)newINPARAMIFSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__INPARAMIFSNREF, null, msgs);
			msgs = basicSetINPARAMIFSNREF(newINPARAMIFSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__INPARAMIFSNREF, newINPARAMIFSNREF, newINPARAMIFSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getOUTPARAMIFSNREF() {
		return oUTPARAMIFSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOUTPARAMIFSNREF(SNREF newOUTPARAMIFSNREF, NotificationChain msgs) {
		SNREF oldOUTPARAMIFSNREF = oUTPARAMIFSNREF;
		oUTPARAMIFSNREF = newOUTPARAMIFSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__OUTPARAMIFSNREF, oldOUTPARAMIFSNREF, newOUTPARAMIFSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOUTPARAMIFSNREF(SNREF newOUTPARAMIFSNREF) {
		if (newOUTPARAMIFSNREF != oUTPARAMIFSNREF) {
			NotificationChain msgs = null;
			if (oUTPARAMIFSNREF != null)
				msgs = ((InternalEObject)oUTPARAMIFSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__OUTPARAMIFSNREF, null, msgs);
			if (newOUTPARAMIFSNREF != null)
				msgs = ((InternalEObject)newOUTPARAMIFSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMMRELATION__OUTPARAMIFSNREF, null, msgs);
			msgs = basicSetOUTPARAMIFSNREF(newOUTPARAMIFSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__OUTPARAMIFSNREF, newOUTPARAMIFSNREF, newOUTPARAMIFSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMMRELATIONVALUETYPE getVALUETYPE() {
		return vALUETYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVALUETYPE(COMMRELATIONVALUETYPE newVALUETYPE) {
		COMMRELATIONVALUETYPE oldVALUETYPE = vALUETYPE;
		vALUETYPE = newVALUETYPE == null ? VALUETYPE_EDEFAULT : newVALUETYPE;
		boolean oldVALUETYPEESet = vALUETYPEESet;
		vALUETYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMMRELATION__VALUETYPE, oldVALUETYPE, vALUETYPE, !oldVALUETYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetVALUETYPE() {
		COMMRELATIONVALUETYPE oldVALUETYPE = vALUETYPE;
		boolean oldVALUETYPEESet = vALUETYPEESet;
		vALUETYPE = VALUETYPE_EDEFAULT;
		vALUETYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.COMMRELATION__VALUETYPE, oldVALUETYPE, VALUETYPE_EDEFAULT, oldVALUETYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetVALUETYPE() {
		return vALUETYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATION__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMREF:
				return basicSetDIAGCOMMREF(null, msgs);
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMSNREF:
				return basicSetDIAGCOMMSNREF(null, msgs);
			case OdxXhtmlPackage.COMMRELATION__INPARAMIFSNREF:
				return basicSetINPARAMIFSNREF(null, msgs);
			case OdxXhtmlPackage.COMMRELATION__OUTPARAMIFSNREF:
				return basicSetOUTPARAMIFSNREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATION__DESC:
				return getDESC();
			case OdxXhtmlPackage.COMMRELATION__RELATIONTYPE:
				return getRELATIONTYPE();
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMREF:
				return getDIAGCOMMREF();
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMSNREF:
				return getDIAGCOMMSNREF();
			case OdxXhtmlPackage.COMMRELATION__INPARAMIFSNREF:
				return getINPARAMIFSNREF();
			case OdxXhtmlPackage.COMMRELATION__OUTPARAMIFSNREF:
				return getOUTPARAMIFSNREF();
			case OdxXhtmlPackage.COMMRELATION__VALUETYPE:
				return getVALUETYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATION__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.COMMRELATION__RELATIONTYPE:
				setRELATIONTYPE((String)newValue);
				return;
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMREF:
				setDIAGCOMMREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.COMMRELATION__INPARAMIFSNREF:
				setINPARAMIFSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.COMMRELATION__OUTPARAMIFSNREF:
				setOUTPARAMIFSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.COMMRELATION__VALUETYPE:
				setVALUETYPE((COMMRELATIONVALUETYPE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATION__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.COMMRELATION__RELATIONTYPE:
				setRELATIONTYPE(RELATIONTYPE_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMREF:
				setDIAGCOMMREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.COMMRELATION__INPARAMIFSNREF:
				setINPARAMIFSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.COMMRELATION__OUTPARAMIFSNREF:
				setOUTPARAMIFSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.COMMRELATION__VALUETYPE:
				unsetVALUETYPE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATION__DESC:
				return dESC != null;
			case OdxXhtmlPackage.COMMRELATION__RELATIONTYPE:
				return RELATIONTYPE_EDEFAULT == null ? rELATIONTYPE != null : !RELATIONTYPE_EDEFAULT.equals(rELATIONTYPE);
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMREF:
				return dIAGCOMMREF != null;
			case OdxXhtmlPackage.COMMRELATION__DIAGCOMMSNREF:
				return dIAGCOMMSNREF != null;
			case OdxXhtmlPackage.COMMRELATION__INPARAMIFSNREF:
				return iNPARAMIFSNREF != null;
			case OdxXhtmlPackage.COMMRELATION__OUTPARAMIFSNREF:
				return oUTPARAMIFSNREF != null;
			case OdxXhtmlPackage.COMMRELATION__VALUETYPE:
				return isSetVALUETYPE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (rELATIONTYPE: ");
		result.append(rELATIONTYPE);
		result.append(", vALUETYPE: ");
		if (vALUETYPEESet) result.append(vALUETYPE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //COMMRELATIONImpl
