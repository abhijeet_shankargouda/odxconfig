/**
 */
package OdxXhtml.impl;

import OdxXhtml.BASEVARIANTS;
import OdxXhtml.DIAGLAYERCONTAINER;
import OdxXhtml.ECUSHAREDDATAS;
import OdxXhtml.ECUVARIANTS;
import OdxXhtml.FUNCTIONALGROUPS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROTOCOLS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGLAYERCONTAINER</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERCONTAINERImpl#getPROTOCOLS <em>PROTOCOLS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERCONTAINERImpl#getFUNCTIONALGROUPS <em>FUNCTIONALGROUPS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERCONTAINERImpl#getECUSHAREDDATAS <em>ECUSHAREDDATAS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERCONTAINERImpl#getBASEVARIANTS <em>BASEVARIANTS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERCONTAINERImpl#getECUVARIANTS <em>ECUVARIANTS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGLAYERCONTAINERImpl extends ODXCATEGORYImpl implements DIAGLAYERCONTAINER {
	/**
	 * The cached value of the '{@link #getPROTOCOLS() <em>PROTOCOLS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROTOCOLS()
	 * @generated
	 * @ordered
	 */
	protected PROTOCOLS pROTOCOLS;

	/**
	 * The cached value of the '{@link #getFUNCTIONALGROUPS() <em>FUNCTIONALGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFUNCTIONALGROUPS()
	 * @generated
	 * @ordered
	 */
	protected FUNCTIONALGROUPS fUNCTIONALGROUPS;

	/**
	 * The cached value of the '{@link #getECUSHAREDDATAS() <em>ECUSHAREDDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUSHAREDDATAS()
	 * @generated
	 * @ordered
	 */
	protected ECUSHAREDDATAS eCUSHAREDDATAS;

	/**
	 * The cached value of the '{@link #getBASEVARIANTS() <em>BASEVARIANTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASEVARIANTS()
	 * @generated
	 * @ordered
	 */
	protected BASEVARIANTS bASEVARIANTS;

	/**
	 * The cached value of the '{@link #getECUVARIANTS() <em>ECUVARIANTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUVARIANTS()
	 * @generated
	 * @ordered
	 */
	protected ECUVARIANTS eCUVARIANTS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGLAYERCONTAINERImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGLAYERCONTAINER();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROTOCOLS getPROTOCOLS() {
		return pROTOCOLS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROTOCOLS(PROTOCOLS newPROTOCOLS, NotificationChain msgs) {
		PROTOCOLS oldPROTOCOLS = pROTOCOLS;
		pROTOCOLS = newPROTOCOLS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__PROTOCOLS, oldPROTOCOLS, newPROTOCOLS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROTOCOLS(PROTOCOLS newPROTOCOLS) {
		if (newPROTOCOLS != pROTOCOLS) {
			NotificationChain msgs = null;
			if (pROTOCOLS != null)
				msgs = ((InternalEObject)pROTOCOLS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__PROTOCOLS, null, msgs);
			if (newPROTOCOLS != null)
				msgs = ((InternalEObject)newPROTOCOLS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__PROTOCOLS, null, msgs);
			msgs = basicSetPROTOCOLS(newPROTOCOLS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__PROTOCOLS, newPROTOCOLS, newPROTOCOLS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTIONALGROUPS getFUNCTIONALGROUPS() {
		return fUNCTIONALGROUPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFUNCTIONALGROUPS(FUNCTIONALGROUPS newFUNCTIONALGROUPS, NotificationChain msgs) {
		FUNCTIONALGROUPS oldFUNCTIONALGROUPS = fUNCTIONALGROUPS;
		fUNCTIONALGROUPS = newFUNCTIONALGROUPS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__FUNCTIONALGROUPS, oldFUNCTIONALGROUPS, newFUNCTIONALGROUPS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFUNCTIONALGROUPS(FUNCTIONALGROUPS newFUNCTIONALGROUPS) {
		if (newFUNCTIONALGROUPS != fUNCTIONALGROUPS) {
			NotificationChain msgs = null;
			if (fUNCTIONALGROUPS != null)
				msgs = ((InternalEObject)fUNCTIONALGROUPS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__FUNCTIONALGROUPS, null, msgs);
			if (newFUNCTIONALGROUPS != null)
				msgs = ((InternalEObject)newFUNCTIONALGROUPS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__FUNCTIONALGROUPS, null, msgs);
			msgs = basicSetFUNCTIONALGROUPS(newFUNCTIONALGROUPS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__FUNCTIONALGROUPS, newFUNCTIONALGROUPS, newFUNCTIONALGROUPS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUSHAREDDATAS getECUSHAREDDATAS() {
		return eCUSHAREDDATAS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetECUSHAREDDATAS(ECUSHAREDDATAS newECUSHAREDDATAS, NotificationChain msgs) {
		ECUSHAREDDATAS oldECUSHAREDDATAS = eCUSHAREDDATAS;
		eCUSHAREDDATAS = newECUSHAREDDATAS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUSHAREDDATAS, oldECUSHAREDDATAS, newECUSHAREDDATAS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setECUSHAREDDATAS(ECUSHAREDDATAS newECUSHAREDDATAS) {
		if (newECUSHAREDDATAS != eCUSHAREDDATAS) {
			NotificationChain msgs = null;
			if (eCUSHAREDDATAS != null)
				msgs = ((InternalEObject)eCUSHAREDDATAS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUSHAREDDATAS, null, msgs);
			if (newECUSHAREDDATAS != null)
				msgs = ((InternalEObject)newECUSHAREDDATAS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUSHAREDDATAS, null, msgs);
			msgs = basicSetECUSHAREDDATAS(newECUSHAREDDATAS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUSHAREDDATAS, newECUSHAREDDATAS, newECUSHAREDDATAS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BASEVARIANTS getBASEVARIANTS() {
		return bASEVARIANTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBASEVARIANTS(BASEVARIANTS newBASEVARIANTS, NotificationChain msgs) {
		BASEVARIANTS oldBASEVARIANTS = bASEVARIANTS;
		bASEVARIANTS = newBASEVARIANTS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__BASEVARIANTS, oldBASEVARIANTS, newBASEVARIANTS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBASEVARIANTS(BASEVARIANTS newBASEVARIANTS) {
		if (newBASEVARIANTS != bASEVARIANTS) {
			NotificationChain msgs = null;
			if (bASEVARIANTS != null)
				msgs = ((InternalEObject)bASEVARIANTS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__BASEVARIANTS, null, msgs);
			if (newBASEVARIANTS != null)
				msgs = ((InternalEObject)newBASEVARIANTS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__BASEVARIANTS, null, msgs);
			msgs = basicSetBASEVARIANTS(newBASEVARIANTS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__BASEVARIANTS, newBASEVARIANTS, newBASEVARIANTS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUVARIANTS getECUVARIANTS() {
		return eCUVARIANTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetECUVARIANTS(ECUVARIANTS newECUVARIANTS, NotificationChain msgs) {
		ECUVARIANTS oldECUVARIANTS = eCUVARIANTS;
		eCUVARIANTS = newECUVARIANTS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUVARIANTS, oldECUVARIANTS, newECUVARIANTS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setECUVARIANTS(ECUVARIANTS newECUVARIANTS) {
		if (newECUVARIANTS != eCUVARIANTS) {
			NotificationChain msgs = null;
			if (eCUVARIANTS != null)
				msgs = ((InternalEObject)eCUVARIANTS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUVARIANTS, null, msgs);
			if (newECUVARIANTS != null)
				msgs = ((InternalEObject)newECUVARIANTS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUVARIANTS, null, msgs);
			msgs = basicSetECUVARIANTS(newECUVARIANTS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUVARIANTS, newECUVARIANTS, newECUVARIANTS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__PROTOCOLS:
				return basicSetPROTOCOLS(null, msgs);
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__FUNCTIONALGROUPS:
				return basicSetFUNCTIONALGROUPS(null, msgs);
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUSHAREDDATAS:
				return basicSetECUSHAREDDATAS(null, msgs);
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__BASEVARIANTS:
				return basicSetBASEVARIANTS(null, msgs);
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUVARIANTS:
				return basicSetECUVARIANTS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__PROTOCOLS:
				return getPROTOCOLS();
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__FUNCTIONALGROUPS:
				return getFUNCTIONALGROUPS();
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUSHAREDDATAS:
				return getECUSHAREDDATAS();
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__BASEVARIANTS:
				return getBASEVARIANTS();
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUVARIANTS:
				return getECUVARIANTS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__PROTOCOLS:
				setPROTOCOLS((PROTOCOLS)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__FUNCTIONALGROUPS:
				setFUNCTIONALGROUPS((FUNCTIONALGROUPS)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUSHAREDDATAS:
				setECUSHAREDDATAS((ECUSHAREDDATAS)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__BASEVARIANTS:
				setBASEVARIANTS((BASEVARIANTS)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUVARIANTS:
				setECUVARIANTS((ECUVARIANTS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__PROTOCOLS:
				setPROTOCOLS((PROTOCOLS)null);
				return;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__FUNCTIONALGROUPS:
				setFUNCTIONALGROUPS((FUNCTIONALGROUPS)null);
				return;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUSHAREDDATAS:
				setECUSHAREDDATAS((ECUSHAREDDATAS)null);
				return;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__BASEVARIANTS:
				setBASEVARIANTS((BASEVARIANTS)null);
				return;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUVARIANTS:
				setECUVARIANTS((ECUVARIANTS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__PROTOCOLS:
				return pROTOCOLS != null;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__FUNCTIONALGROUPS:
				return fUNCTIONALGROUPS != null;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUSHAREDDATAS:
				return eCUSHAREDDATAS != null;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__BASEVARIANTS:
				return bASEVARIANTS != null;
			case OdxXhtmlPackage.DIAGLAYERCONTAINER__ECUVARIANTS:
				return eCUVARIANTS != null;
		}
		return super.eIsSet(featureID);
	}

} //DIAGLAYERCONTAINERImpl
