/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.IDENTVALUE;
import OdxXhtml.OWNIDENT;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OWNIDENT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.OWNIDENTImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.OWNIDENTImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.OWNIDENTImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.OWNIDENTImpl#getIDENTVALUE <em>IDENTVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.OWNIDENTImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.OWNIDENTImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OWNIDENTImpl extends MinimalEObjectImpl.Container implements OWNIDENT {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getIDENTVALUE() <em>IDENTVALUE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDENTVALUE()
	 * @generated
	 * @ordered
	 */
	protected IDENTVALUE iDENTVALUE;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OWNIDENTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getOWNIDENT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.OWNIDENT__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.OWNIDENT__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.OWNIDENT__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.OWNIDENT__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.OWNIDENT__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.OWNIDENT__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.OWNIDENT__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.OWNIDENT__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.OWNIDENT__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IDENTVALUE getIDENTVALUE() {
		return iDENTVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIDENTVALUE(IDENTVALUE newIDENTVALUE, NotificationChain msgs) {
		IDENTVALUE oldIDENTVALUE = iDENTVALUE;
		iDENTVALUE = newIDENTVALUE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.OWNIDENT__IDENTVALUE, oldIDENTVALUE, newIDENTVALUE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIDENTVALUE(IDENTVALUE newIDENTVALUE) {
		if (newIDENTVALUE != iDENTVALUE) {
			NotificationChain msgs = null;
			if (iDENTVALUE != null)
				msgs = ((InternalEObject)iDENTVALUE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.OWNIDENT__IDENTVALUE, null, msgs);
			if (newIDENTVALUE != null)
				msgs = ((InternalEObject)newIDENTVALUE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.OWNIDENT__IDENTVALUE, null, msgs);
			msgs = basicSetIDENTVALUE(newIDENTVALUE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.OWNIDENT__IDENTVALUE, newIDENTVALUE, newIDENTVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.OWNIDENT__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.OWNIDENT__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENT__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.OWNIDENT__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.OWNIDENT__IDENTVALUE:
				return basicSetIDENTVALUE(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENT__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.OWNIDENT__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.OWNIDENT__DESC:
				return getDESC();
			case OdxXhtmlPackage.OWNIDENT__IDENTVALUE:
				return getIDENTVALUE();
			case OdxXhtmlPackage.OWNIDENT__ID:
				return getID();
			case OdxXhtmlPackage.OWNIDENT__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENT__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.OWNIDENT__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.OWNIDENT__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.OWNIDENT__IDENTVALUE:
				setIDENTVALUE((IDENTVALUE)newValue);
				return;
			case OdxXhtmlPackage.OWNIDENT__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.OWNIDENT__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENT__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.OWNIDENT__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.OWNIDENT__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.OWNIDENT__IDENTVALUE:
				setIDENTVALUE((IDENTVALUE)null);
				return;
			case OdxXhtmlPackage.OWNIDENT__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.OWNIDENT__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENT__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.OWNIDENT__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.OWNIDENT__DESC:
				return dESC != null;
			case OdxXhtmlPackage.OWNIDENT__IDENTVALUE:
				return iDENTVALUE != null;
			case OdxXhtmlPackage.OWNIDENT__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.OWNIDENT__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //OWNIDENTImpl
