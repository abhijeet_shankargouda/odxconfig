/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATAFORMAT;
import OdxXhtml.DATAFORMATSELECTION;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DATAFORMAT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DATAFORMATImpl#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATAFORMATImpl#getSELECTION <em>SELECTION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DATAFORMATImpl extends MinimalEObjectImpl.Container implements DATAFORMAT {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSELECTION() <em>SELECTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSELECTION()
	 * @generated
	 * @ordered
	 */
	protected static final DATAFORMATSELECTION SELECTION_EDEFAULT = DATAFORMATSELECTION.INTELHEX;

	/**
	 * The cached value of the '{@link #getSELECTION() <em>SELECTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSELECTION()
	 * @generated
	 * @ordered
	 */
	protected DATAFORMATSELECTION sELECTION = SELECTION_EDEFAULT;

	/**
	 * This is true if the SELECTION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sELECTIONESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DATAFORMATImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDATAFORMAT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAFORMAT__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATAFORMATSELECTION getSELECTION() {
		return sELECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSELECTION(DATAFORMATSELECTION newSELECTION) {
		DATAFORMATSELECTION oldSELECTION = sELECTION;
		sELECTION = newSELECTION == null ? SELECTION_EDEFAULT : newSELECTION;
		boolean oldSELECTIONESet = sELECTIONESet;
		sELECTIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAFORMAT__SELECTION, oldSELECTION, sELECTION, !oldSELECTIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetSELECTION() {
		DATAFORMATSELECTION oldSELECTION = sELECTION;
		boolean oldSELECTIONESet = sELECTIONESet;
		sELECTION = SELECTION_EDEFAULT;
		sELECTIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DATAFORMAT__SELECTION, oldSELECTION, SELECTION_EDEFAULT, oldSELECTIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetSELECTION() {
		return sELECTIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAFORMAT__VALUE:
				return getValue();
			case OdxXhtmlPackage.DATAFORMAT__SELECTION:
				return getSELECTION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAFORMAT__VALUE:
				setValue((String)newValue);
				return;
			case OdxXhtmlPackage.DATAFORMAT__SELECTION:
				setSELECTION((DATAFORMATSELECTION)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAFORMAT__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.DATAFORMAT__SELECTION:
				unsetSELECTION();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAFORMAT__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case OdxXhtmlPackage.DATAFORMAT__SELECTION:
				return isSetSELECTION();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", sELECTION: ");
		if (sELECTIONESet) result.append(sELECTION); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //DATAFORMATImpl
