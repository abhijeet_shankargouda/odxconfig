/**
 */
package OdxXhtml.impl;

import OdxXhtml.MODIFICATION;
import OdxXhtml.MODIFICATIONS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MODIFICATIONS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MODIFICATIONSImpl#getMODIFICATION <em>MODIFICATION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MODIFICATIONSImpl extends MinimalEObjectImpl.Container implements MODIFICATIONS {
	/**
	 * The cached value of the '{@link #getMODIFICATION() <em>MODIFICATION</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMODIFICATION()
	 * @generated
	 * @ordered
	 */
	protected EList<MODIFICATION> mODIFICATION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MODIFICATIONSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMODIFICATIONS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<MODIFICATION> getMODIFICATION() {
		if (mODIFICATION == null) {
			mODIFICATION = new EObjectContainmentEList<MODIFICATION>(MODIFICATION.class, this, OdxXhtmlPackage.MODIFICATIONS__MODIFICATION);
		}
		return mODIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS__MODIFICATION:
				return ((InternalEList<?>)getMODIFICATION()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS__MODIFICATION:
				return getMODIFICATION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS__MODIFICATION:
				getMODIFICATION().clear();
				getMODIFICATION().addAll((Collection<? extends MODIFICATION>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS__MODIFICATION:
				getMODIFICATION().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS__MODIFICATION:
				return mODIFICATION != null && !mODIFICATION.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MODIFICATIONSImpl
