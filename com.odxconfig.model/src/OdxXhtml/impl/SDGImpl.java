/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SD;
import OdxXhtml.SDG;
import OdxXhtml.SDGCAPTION;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SDG</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SDGImpl#getSDGCAPTION <em>SDGCAPTION</em>}</li>
 *   <li>{@link OdxXhtml.impl.SDGImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link OdxXhtml.impl.SDGImpl#getSDG <em>SDG</em>}</li>
 *   <li>{@link OdxXhtml.impl.SDGImpl#getSD <em>SD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SDGImpl extends SPECIALDATAImpl implements SDG {
	/**
	 * The cached value of the '{@link #getSDGCAPTION() <em>SDGCAPTION</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGCAPTION()
	 * @generated
	 * @ordered
	 */
	protected SDGCAPTION sDGCAPTION;

	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SDGImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSDG();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGCAPTION getSDGCAPTION() {
		return sDGCAPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGCAPTION(SDGCAPTION newSDGCAPTION, NotificationChain msgs) {
		SDGCAPTION oldSDGCAPTION = sDGCAPTION;
		sDGCAPTION = newSDGCAPTION;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SDG__SDGCAPTION, oldSDGCAPTION, newSDGCAPTION);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGCAPTION(SDGCAPTION newSDGCAPTION) {
		if (newSDGCAPTION != sDGCAPTION) {
			NotificationChain msgs = null;
			if (sDGCAPTION != null)
				msgs = ((InternalEObject)sDGCAPTION).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SDG__SDGCAPTION, null, msgs);
			if (newSDGCAPTION != null)
				msgs = ((InternalEObject)newSDGCAPTION).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SDG__SDGCAPTION, null, msgs);
			msgs = basicSetSDGCAPTION(newSDGCAPTION, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SDG__SDGCAPTION, newSDGCAPTION, newSDGCAPTION));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, OdxXhtmlPackage.SDG__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SDG> getSDG() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getSDG_SDG());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SD> getSD() {
		return getGroup().list(OdxXhtmlPackage.eINSTANCE.getSDG_SD());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG__SDGCAPTION:
				return basicSetSDGCAPTION(null, msgs);
			case OdxXhtmlPackage.SDG__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.SDG__SDG:
				return ((InternalEList<?>)getSDG()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.SDG__SD:
				return ((InternalEList<?>)getSD()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG__SDGCAPTION:
				return getSDGCAPTION();
			case OdxXhtmlPackage.SDG__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case OdxXhtmlPackage.SDG__SDG:
				return getSDG();
			case OdxXhtmlPackage.SDG__SD:
				return getSD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG__SDGCAPTION:
				setSDGCAPTION((SDGCAPTION)newValue);
				return;
			case OdxXhtmlPackage.SDG__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case OdxXhtmlPackage.SDG__SDG:
				getSDG().clear();
				getSDG().addAll((Collection<? extends SDG>)newValue);
				return;
			case OdxXhtmlPackage.SDG__SD:
				getSD().clear();
				getSD().addAll((Collection<? extends SD>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG__SDGCAPTION:
				setSDGCAPTION((SDGCAPTION)null);
				return;
			case OdxXhtmlPackage.SDG__GROUP:
				getGroup().clear();
				return;
			case OdxXhtmlPackage.SDG__SDG:
				getSDG().clear();
				return;
			case OdxXhtmlPackage.SDG__SD:
				getSD().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SDG__SDGCAPTION:
				return sDGCAPTION != null;
			case OdxXhtmlPackage.SDG__GROUP:
				return group != null && !group.isEmpty();
			case OdxXhtmlPackage.SDG__SDG:
				return !getSDG().isEmpty();
			case OdxXhtmlPackage.SDG__SD:
				return !getSD().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //SDGImpl
