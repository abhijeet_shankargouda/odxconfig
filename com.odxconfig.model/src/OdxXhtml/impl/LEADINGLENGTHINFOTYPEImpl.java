/**
 */
package OdxXhtml.impl;

import OdxXhtml.LEADINGLENGTHINFOTYPE;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LEADINGLENGTHINFOTYPE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.LEADINGLENGTHINFOTYPEImpl#getBITLENGTH <em>BITLENGTH</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LEADINGLENGTHINFOTYPEImpl extends DIAGCODEDTYPEImpl implements LEADINGLENGTHINFOTYPE {
	/**
	 * The default value of the '{@link #getBITLENGTH() <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITLENGTH()
	 * @generated
	 * @ordered
	 */
	protected static final long BITLENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBITLENGTH() <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITLENGTH()
	 * @generated
	 * @ordered
	 */
	protected long bITLENGTH = BITLENGTH_EDEFAULT;

	/**
	 * This is true if the BITLENGTH attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bITLENGTHESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LEADINGLENGTHINFOTYPEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLEADINGLENGTHINFOTYPE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBITLENGTH() {
		return bITLENGTH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBITLENGTH(long newBITLENGTH) {
		long oldBITLENGTH = bITLENGTH;
		bITLENGTH = newBITLENGTH;
		boolean oldBITLENGTHESet = bITLENGTHESet;
		bITLENGTHESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LEADINGLENGTHINFOTYPE__BITLENGTH, oldBITLENGTH, bITLENGTH, !oldBITLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBITLENGTH() {
		long oldBITLENGTH = bITLENGTH;
		boolean oldBITLENGTHESet = bITLENGTHESet;
		bITLENGTH = BITLENGTH_EDEFAULT;
		bITLENGTHESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.LEADINGLENGTHINFOTYPE__BITLENGTH, oldBITLENGTH, BITLENGTH_EDEFAULT, oldBITLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBITLENGTH() {
		return bITLENGTHESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.LEADINGLENGTHINFOTYPE__BITLENGTH:
				return getBITLENGTH();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.LEADINGLENGTHINFOTYPE__BITLENGTH:
				setBITLENGTH((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LEADINGLENGTHINFOTYPE__BITLENGTH:
				unsetBITLENGTH();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LEADINGLENGTHINFOTYPE__BITLENGTH:
				return isSetBITLENGTH();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bITLENGTH: ");
		if (bITLENGTHESet) result.append(bITLENGTH); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //LEADINGLENGTHINFOTYPEImpl
