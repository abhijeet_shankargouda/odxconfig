/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPUMETHOD;
import OdxXhtml.DIAGCODEDTYPE;
import OdxXhtml.DTCDOP;
import OdxXhtml.DTCS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSICALTYPE;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DTCDOP</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DTCDOPImpl#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCDOPImpl#getPHYSICALTYPE <em>PHYSICALTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCDOPImpl#getCOMPUMETHOD <em>COMPUMETHOD</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCDOPImpl#getDTCS <em>DTCS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DTCDOPImpl#isISVISIBLE <em>ISVISIBLE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DTCDOPImpl extends DOPBASEImpl implements DTCDOP {
	/**
	 * The cached value of the '{@link #getDIAGCODEDTYPE() <em>DIAGCODEDTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCODEDTYPE()
	 * @generated
	 * @ordered
	 */
	protected DIAGCODEDTYPE dIAGCODEDTYPE;

	/**
	 * The cached value of the '{@link #getPHYSICALTYPE() <em>PHYSICALTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALTYPE()
	 * @generated
	 * @ordered
	 */
	protected PHYSICALTYPE pHYSICALTYPE;

	/**
	 * The cached value of the '{@link #getCOMPUMETHOD() <em>COMPUMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUMETHOD()
	 * @generated
	 * @ordered
	 */
	protected COMPUMETHOD cOMPUMETHOD;

	/**
	 * The cached value of the '{@link #getDTCS() <em>DTCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDTCS()
	 * @generated
	 * @ordered
	 */
	protected DTCS dTCS;

	/**
	 * The default value of the '{@link #isISVISIBLE() <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISVISIBLE()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISVISIBLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISVISIBLE() <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISVISIBLE()
	 * @generated
	 * @ordered
	 */
	protected boolean iSVISIBLE = ISVISIBLE_EDEFAULT;

	/**
	 * This is true if the ISVISIBLE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSVISIBLEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DTCDOPImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDTCDOP();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGCODEDTYPE getDIAGCODEDTYPE() {
		return dIAGCODEDTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCODEDTYPE(DIAGCODEDTYPE newDIAGCODEDTYPE, NotificationChain msgs) {
		DIAGCODEDTYPE oldDIAGCODEDTYPE = dIAGCODEDTYPE;
		dIAGCODEDTYPE = newDIAGCODEDTYPE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTCDOP__DIAGCODEDTYPE, oldDIAGCODEDTYPE, newDIAGCODEDTYPE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCODEDTYPE(DIAGCODEDTYPE newDIAGCODEDTYPE) {
		if (newDIAGCODEDTYPE != dIAGCODEDTYPE) {
			NotificationChain msgs = null;
			if (dIAGCODEDTYPE != null)
				msgs = ((InternalEObject)dIAGCODEDTYPE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTCDOP__DIAGCODEDTYPE, null, msgs);
			if (newDIAGCODEDTYPE != null)
				msgs = ((InternalEObject)newDIAGCODEDTYPE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTCDOP__DIAGCODEDTYPE, null, msgs);
			msgs = basicSetDIAGCODEDTYPE(newDIAGCODEDTYPE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTCDOP__DIAGCODEDTYPE, newDIAGCODEDTYPE, newDIAGCODEDTYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALTYPE getPHYSICALTYPE() {
		return pHYSICALTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPHYSICALTYPE(PHYSICALTYPE newPHYSICALTYPE, NotificationChain msgs) {
		PHYSICALTYPE oldPHYSICALTYPE = pHYSICALTYPE;
		pHYSICALTYPE = newPHYSICALTYPE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTCDOP__PHYSICALTYPE, oldPHYSICALTYPE, newPHYSICALTYPE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHYSICALTYPE(PHYSICALTYPE newPHYSICALTYPE) {
		if (newPHYSICALTYPE != pHYSICALTYPE) {
			NotificationChain msgs = null;
			if (pHYSICALTYPE != null)
				msgs = ((InternalEObject)pHYSICALTYPE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTCDOP__PHYSICALTYPE, null, msgs);
			if (newPHYSICALTYPE != null)
				msgs = ((InternalEObject)newPHYSICALTYPE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTCDOP__PHYSICALTYPE, null, msgs);
			msgs = basicSetPHYSICALTYPE(newPHYSICALTYPE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTCDOP__PHYSICALTYPE, newPHYSICALTYPE, newPHYSICALTYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUMETHOD getCOMPUMETHOD() {
		return cOMPUMETHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUMETHOD(COMPUMETHOD newCOMPUMETHOD, NotificationChain msgs) {
		COMPUMETHOD oldCOMPUMETHOD = cOMPUMETHOD;
		cOMPUMETHOD = newCOMPUMETHOD;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTCDOP__COMPUMETHOD, oldCOMPUMETHOD, newCOMPUMETHOD);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUMETHOD(COMPUMETHOD newCOMPUMETHOD) {
		if (newCOMPUMETHOD != cOMPUMETHOD) {
			NotificationChain msgs = null;
			if (cOMPUMETHOD != null)
				msgs = ((InternalEObject)cOMPUMETHOD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTCDOP__COMPUMETHOD, null, msgs);
			if (newCOMPUMETHOD != null)
				msgs = ((InternalEObject)newCOMPUMETHOD).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTCDOP__COMPUMETHOD, null, msgs);
			msgs = basicSetCOMPUMETHOD(newCOMPUMETHOD, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTCDOP__COMPUMETHOD, newCOMPUMETHOD, newCOMPUMETHOD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DTCS getDTCS() {
		return dTCS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDTCS(DTCS newDTCS, NotificationChain msgs) {
		DTCS oldDTCS = dTCS;
		dTCS = newDTCS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTCDOP__DTCS, oldDTCS, newDTCS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDTCS(DTCS newDTCS) {
		if (newDTCS != dTCS) {
			NotificationChain msgs = null;
			if (dTCS != null)
				msgs = ((InternalEObject)dTCS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTCDOP__DTCS, null, msgs);
			if (newDTCS != null)
				msgs = ((InternalEObject)newDTCS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DTCDOP__DTCS, null, msgs);
			msgs = basicSetDTCS(newDTCS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTCDOP__DTCS, newDTCS, newDTCS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISVISIBLE() {
		return iSVISIBLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISVISIBLE(boolean newISVISIBLE) {
		boolean oldISVISIBLE = iSVISIBLE;
		iSVISIBLE = newISVISIBLE;
		boolean oldISVISIBLEESet = iSVISIBLEESet;
		iSVISIBLEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DTCDOP__ISVISIBLE, oldISVISIBLE, iSVISIBLE, !oldISVISIBLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISVISIBLE() {
		boolean oldISVISIBLE = iSVISIBLE;
		boolean oldISVISIBLEESet = iSVISIBLEESet;
		iSVISIBLE = ISVISIBLE_EDEFAULT;
		iSVISIBLEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DTCDOP__ISVISIBLE, oldISVISIBLE, ISVISIBLE_EDEFAULT, oldISVISIBLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISVISIBLE() {
		return iSVISIBLEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCDOP__DIAGCODEDTYPE:
				return basicSetDIAGCODEDTYPE(null, msgs);
			case OdxXhtmlPackage.DTCDOP__PHYSICALTYPE:
				return basicSetPHYSICALTYPE(null, msgs);
			case OdxXhtmlPackage.DTCDOP__COMPUMETHOD:
				return basicSetCOMPUMETHOD(null, msgs);
			case OdxXhtmlPackage.DTCDOP__DTCS:
				return basicSetDTCS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCDOP__DIAGCODEDTYPE:
				return getDIAGCODEDTYPE();
			case OdxXhtmlPackage.DTCDOP__PHYSICALTYPE:
				return getPHYSICALTYPE();
			case OdxXhtmlPackage.DTCDOP__COMPUMETHOD:
				return getCOMPUMETHOD();
			case OdxXhtmlPackage.DTCDOP__DTCS:
				return getDTCS();
			case OdxXhtmlPackage.DTCDOP__ISVISIBLE:
				return isISVISIBLE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCDOP__DIAGCODEDTYPE:
				setDIAGCODEDTYPE((DIAGCODEDTYPE)newValue);
				return;
			case OdxXhtmlPackage.DTCDOP__PHYSICALTYPE:
				setPHYSICALTYPE((PHYSICALTYPE)newValue);
				return;
			case OdxXhtmlPackage.DTCDOP__COMPUMETHOD:
				setCOMPUMETHOD((COMPUMETHOD)newValue);
				return;
			case OdxXhtmlPackage.DTCDOP__DTCS:
				setDTCS((DTCS)newValue);
				return;
			case OdxXhtmlPackage.DTCDOP__ISVISIBLE:
				setISVISIBLE((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCDOP__DIAGCODEDTYPE:
				setDIAGCODEDTYPE((DIAGCODEDTYPE)null);
				return;
			case OdxXhtmlPackage.DTCDOP__PHYSICALTYPE:
				setPHYSICALTYPE((PHYSICALTYPE)null);
				return;
			case OdxXhtmlPackage.DTCDOP__COMPUMETHOD:
				setCOMPUMETHOD((COMPUMETHOD)null);
				return;
			case OdxXhtmlPackage.DTCDOP__DTCS:
				setDTCS((DTCS)null);
				return;
			case OdxXhtmlPackage.DTCDOP__ISVISIBLE:
				unsetISVISIBLE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCDOP__DIAGCODEDTYPE:
				return dIAGCODEDTYPE != null;
			case OdxXhtmlPackage.DTCDOP__PHYSICALTYPE:
				return pHYSICALTYPE != null;
			case OdxXhtmlPackage.DTCDOP__COMPUMETHOD:
				return cOMPUMETHOD != null;
			case OdxXhtmlPackage.DTCDOP__DTCS:
				return dTCS != null;
			case OdxXhtmlPackage.DTCDOP__ISVISIBLE:
				return isSetISVISIBLE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (iSVISIBLE: ");
		if (iSVISIBLEESet) result.append(iSVISIBLE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //DTCDOPImpl
