/**
 */
package OdxXhtml.impl;

import OdxXhtml.GATEWAYLOGICALLINK;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GATEWAYLOGICALLINK</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.GATEWAYLOGICALLINKImpl#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GATEWAYLOGICALLINKImpl extends LOGICALLINKImpl implements GATEWAYLOGICALLINK {
	/**
	 * The default value of the '{@link #getSEMANTIC() <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSEMANTIC()
	 * @generated
	 * @ordered
	 */
	protected static final String SEMANTIC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSEMANTIC() <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSEMANTIC()
	 * @generated
	 * @ordered
	 */
	protected String sEMANTIC = SEMANTIC_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GATEWAYLOGICALLINKImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getGATEWAYLOGICALLINK();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSEMANTIC() {
		return sEMANTIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSEMANTIC(String newSEMANTIC) {
		String oldSEMANTIC = sEMANTIC;
		sEMANTIC = newSEMANTIC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.GATEWAYLOGICALLINK__SEMANTIC, oldSEMANTIC, sEMANTIC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.GATEWAYLOGICALLINK__SEMANTIC:
				return getSEMANTIC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.GATEWAYLOGICALLINK__SEMANTIC:
				setSEMANTIC((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.GATEWAYLOGICALLINK__SEMANTIC:
				setSEMANTIC(SEMANTIC_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.GATEWAYLOGICALLINK__SEMANTIC:
				return SEMANTIC_EDEFAULT == null ? sEMANTIC != null : !SEMANTIC_EDEFAULT.equals(sEMANTIC);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sEMANTIC: ");
		result.append(sEMANTIC);
		result.append(')');
		return result.toString();
	}

} //GATEWAYLOGICALLINKImpl
