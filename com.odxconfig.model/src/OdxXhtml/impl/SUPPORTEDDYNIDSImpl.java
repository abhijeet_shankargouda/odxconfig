/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SUPPORTEDDYNID;
import OdxXhtml.SUPPORTEDDYNIDS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SUPPORTEDDYNIDS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SUPPORTEDDYNIDSImpl#getSUPPORTEDDYNID <em>SUPPORTEDDYNID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SUPPORTEDDYNIDSImpl extends MinimalEObjectImpl.Container implements SUPPORTEDDYNIDS {
	/**
	 * The cached value of the '{@link #getSUPPORTEDDYNID() <em>SUPPORTEDDYNID</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSUPPORTEDDYNID()
	 * @generated
	 * @ordered
	 */
	protected EList<SUPPORTEDDYNID> sUPPORTEDDYNID;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SUPPORTEDDYNIDSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSUPPORTEDDYNIDS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SUPPORTEDDYNID> getSUPPORTEDDYNID() {
		if (sUPPORTEDDYNID == null) {
			sUPPORTEDDYNID = new EObjectContainmentEList<SUPPORTEDDYNID>(SUPPORTEDDYNID.class, this, OdxXhtmlPackage.SUPPORTEDDYNIDS__SUPPORTEDDYNID);
		}
		return sUPPORTEDDYNID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SUPPORTEDDYNIDS__SUPPORTEDDYNID:
				return ((InternalEList<?>)getSUPPORTEDDYNID()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SUPPORTEDDYNIDS__SUPPORTEDDYNID:
				return getSUPPORTEDDYNID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SUPPORTEDDYNIDS__SUPPORTEDDYNID:
				getSUPPORTEDDYNID().clear();
				getSUPPORTEDDYNID().addAll((Collection<? extends SUPPORTEDDYNID>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SUPPORTEDDYNIDS__SUPPORTEDDYNID:
				getSUPPORTEDDYNID().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SUPPORTEDDYNIDS__SUPPORTEDDYNID:
				return sUPPORTEDDYNID != null && !sUPPORTEDDYNID.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SUPPORTEDDYNIDSImpl
