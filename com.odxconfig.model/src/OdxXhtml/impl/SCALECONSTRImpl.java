/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.LIMIT;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SCALECONSTR;
import OdxXhtml.TEXT;
import OdxXhtml.VALIDTYPE;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SCALECONSTR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SCALECONSTRImpl#getSHORTLABEL <em>SHORTLABEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.SCALECONSTRImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.SCALECONSTRImpl#getLOWERLIMIT <em>LOWERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.impl.SCALECONSTRImpl#getUPPERLIMIT <em>UPPERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.impl.SCALECONSTRImpl#getVALIDITY <em>VALIDITY</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SCALECONSTRImpl extends MinimalEObjectImpl.Container implements SCALECONSTR {
	/**
	 * The cached value of the '{@link #getSHORTLABEL() <em>SHORTLABEL</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTLABEL()
	 * @generated
	 * @ordered
	 */
	protected TEXT sHORTLABEL;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getLOWERLIMIT() <em>LOWERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLOWERLIMIT()
	 * @generated
	 * @ordered
	 */
	protected LIMIT lOWERLIMIT;

	/**
	 * The cached value of the '{@link #getUPPERLIMIT() <em>UPPERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUPPERLIMIT()
	 * @generated
	 * @ordered
	 */
	protected LIMIT uPPERLIMIT;

	/**
	 * The default value of the '{@link #getVALIDITY() <em>VALIDITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALIDITY()
	 * @generated
	 * @ordered
	 */
	protected static final VALIDTYPE VALIDITY_EDEFAULT = VALIDTYPE.VALID;

	/**
	 * The cached value of the '{@link #getVALIDITY() <em>VALIDITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALIDITY()
	 * @generated
	 * @ordered
	 */
	protected VALIDTYPE vALIDITY = VALIDITY_EDEFAULT;

	/**
	 * This is true if the VALIDITY attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean vALIDITYESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SCALECONSTRImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSCALECONSTR();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getSHORTLABEL() {
		return sHORTLABEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSHORTLABEL(TEXT newSHORTLABEL, NotificationChain msgs) {
		TEXT oldSHORTLABEL = sHORTLABEL;
		sHORTLABEL = newSHORTLABEL;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SCALECONSTR__SHORTLABEL, oldSHORTLABEL, newSHORTLABEL);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTLABEL(TEXT newSHORTLABEL) {
		if (newSHORTLABEL != sHORTLABEL) {
			NotificationChain msgs = null;
			if (sHORTLABEL != null)
				msgs = ((InternalEObject)sHORTLABEL).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SCALECONSTR__SHORTLABEL, null, msgs);
			if (newSHORTLABEL != null)
				msgs = ((InternalEObject)newSHORTLABEL).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SCALECONSTR__SHORTLABEL, null, msgs);
			msgs = basicSetSHORTLABEL(newSHORTLABEL, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SCALECONSTR__SHORTLABEL, newSHORTLABEL, newSHORTLABEL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SCALECONSTR__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SCALECONSTR__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SCALECONSTR__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SCALECONSTR__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LIMIT getLOWERLIMIT() {
		return lOWERLIMIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLOWERLIMIT(LIMIT newLOWERLIMIT, NotificationChain msgs) {
		LIMIT oldLOWERLIMIT = lOWERLIMIT;
		lOWERLIMIT = newLOWERLIMIT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SCALECONSTR__LOWERLIMIT, oldLOWERLIMIT, newLOWERLIMIT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLOWERLIMIT(LIMIT newLOWERLIMIT) {
		if (newLOWERLIMIT != lOWERLIMIT) {
			NotificationChain msgs = null;
			if (lOWERLIMIT != null)
				msgs = ((InternalEObject)lOWERLIMIT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SCALECONSTR__LOWERLIMIT, null, msgs);
			if (newLOWERLIMIT != null)
				msgs = ((InternalEObject)newLOWERLIMIT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SCALECONSTR__LOWERLIMIT, null, msgs);
			msgs = basicSetLOWERLIMIT(newLOWERLIMIT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SCALECONSTR__LOWERLIMIT, newLOWERLIMIT, newLOWERLIMIT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LIMIT getUPPERLIMIT() {
		return uPPERLIMIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUPPERLIMIT(LIMIT newUPPERLIMIT, NotificationChain msgs) {
		LIMIT oldUPPERLIMIT = uPPERLIMIT;
		uPPERLIMIT = newUPPERLIMIT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SCALECONSTR__UPPERLIMIT, oldUPPERLIMIT, newUPPERLIMIT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUPPERLIMIT(LIMIT newUPPERLIMIT) {
		if (newUPPERLIMIT != uPPERLIMIT) {
			NotificationChain msgs = null;
			if (uPPERLIMIT != null)
				msgs = ((InternalEObject)uPPERLIMIT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SCALECONSTR__UPPERLIMIT, null, msgs);
			if (newUPPERLIMIT != null)
				msgs = ((InternalEObject)newUPPERLIMIT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SCALECONSTR__UPPERLIMIT, null, msgs);
			msgs = basicSetUPPERLIMIT(newUPPERLIMIT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SCALECONSTR__UPPERLIMIT, newUPPERLIMIT, newUPPERLIMIT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VALIDTYPE getVALIDITY() {
		return vALIDITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVALIDITY(VALIDTYPE newVALIDITY) {
		VALIDTYPE oldVALIDITY = vALIDITY;
		vALIDITY = newVALIDITY == null ? VALIDITY_EDEFAULT : newVALIDITY;
		boolean oldVALIDITYESet = vALIDITYESet;
		vALIDITYESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SCALECONSTR__VALIDITY, oldVALIDITY, vALIDITY, !oldVALIDITYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetVALIDITY() {
		VALIDTYPE oldVALIDITY = vALIDITY;
		boolean oldVALIDITYESet = vALIDITYESet;
		vALIDITY = VALIDITY_EDEFAULT;
		vALIDITYESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.SCALECONSTR__VALIDITY, oldVALIDITY, VALIDITY_EDEFAULT, oldVALIDITYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetVALIDITY() {
		return vALIDITYESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTR__SHORTLABEL:
				return basicSetSHORTLABEL(null, msgs);
			case OdxXhtmlPackage.SCALECONSTR__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.SCALECONSTR__LOWERLIMIT:
				return basicSetLOWERLIMIT(null, msgs);
			case OdxXhtmlPackage.SCALECONSTR__UPPERLIMIT:
				return basicSetUPPERLIMIT(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTR__SHORTLABEL:
				return getSHORTLABEL();
			case OdxXhtmlPackage.SCALECONSTR__DESC:
				return getDESC();
			case OdxXhtmlPackage.SCALECONSTR__LOWERLIMIT:
				return getLOWERLIMIT();
			case OdxXhtmlPackage.SCALECONSTR__UPPERLIMIT:
				return getUPPERLIMIT();
			case OdxXhtmlPackage.SCALECONSTR__VALIDITY:
				return getVALIDITY();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTR__SHORTLABEL:
				setSHORTLABEL((TEXT)newValue);
				return;
			case OdxXhtmlPackage.SCALECONSTR__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.SCALECONSTR__LOWERLIMIT:
				setLOWERLIMIT((LIMIT)newValue);
				return;
			case OdxXhtmlPackage.SCALECONSTR__UPPERLIMIT:
				setUPPERLIMIT((LIMIT)newValue);
				return;
			case OdxXhtmlPackage.SCALECONSTR__VALIDITY:
				setVALIDITY((VALIDTYPE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTR__SHORTLABEL:
				setSHORTLABEL((TEXT)null);
				return;
			case OdxXhtmlPackage.SCALECONSTR__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.SCALECONSTR__LOWERLIMIT:
				setLOWERLIMIT((LIMIT)null);
				return;
			case OdxXhtmlPackage.SCALECONSTR__UPPERLIMIT:
				setUPPERLIMIT((LIMIT)null);
				return;
			case OdxXhtmlPackage.SCALECONSTR__VALIDITY:
				unsetVALIDITY();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SCALECONSTR__SHORTLABEL:
				return sHORTLABEL != null;
			case OdxXhtmlPackage.SCALECONSTR__DESC:
				return dESC != null;
			case OdxXhtmlPackage.SCALECONSTR__LOWERLIMIT:
				return lOWERLIMIT != null;
			case OdxXhtmlPackage.SCALECONSTR__UPPERLIMIT:
				return uPPERLIMIT != null;
			case OdxXhtmlPackage.SCALECONSTR__VALIDITY:
				return isSetVALIDITY();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (vALIDITY: ");
		if (vALIDITYESet) result.append(vALIDITY); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //SCALECONSTRImpl
