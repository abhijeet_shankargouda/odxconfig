/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TABLEKEY;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TABLEKEY</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.TABLEKEYImpl#getTABLEREF <em>TABLEREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEKEYImpl#getTABLEROWREF <em>TABLEROWREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEKEYImpl#getID <em>ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TABLEKEYImpl extends POSITIONABLEPARAMImpl implements TABLEKEY {
	/**
	 * The cached value of the '{@link #getTABLEREF() <em>TABLEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTABLEREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK tABLEREF;

	/**
	 * The cached value of the '{@link #getTABLEROWREF() <em>TABLEROWREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTABLEROWREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK tABLEROWREF;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TABLEKEYImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getTABLEKEY();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getTABLEREF() {
		return tABLEREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTABLEREF(ODXLINK newTABLEREF, NotificationChain msgs) {
		ODXLINK oldTABLEREF = tABLEREF;
		tABLEREF = newTABLEREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLEKEY__TABLEREF, oldTABLEREF, newTABLEREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTABLEREF(ODXLINK newTABLEREF) {
		if (newTABLEREF != tABLEREF) {
			NotificationChain msgs = null;
			if (tABLEREF != null)
				msgs = ((InternalEObject)tABLEREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLEKEY__TABLEREF, null, msgs);
			if (newTABLEREF != null)
				msgs = ((InternalEObject)newTABLEREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLEKEY__TABLEREF, null, msgs);
			msgs = basicSetTABLEREF(newTABLEREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLEKEY__TABLEREF, newTABLEREF, newTABLEREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getTABLEROWREF() {
		return tABLEROWREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTABLEROWREF(ODXLINK newTABLEROWREF, NotificationChain msgs) {
		ODXLINK oldTABLEROWREF = tABLEROWREF;
		tABLEROWREF = newTABLEROWREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLEKEY__TABLEROWREF, oldTABLEROWREF, newTABLEROWREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTABLEROWREF(ODXLINK newTABLEROWREF) {
		if (newTABLEROWREF != tABLEROWREF) {
			NotificationChain msgs = null;
			if (tABLEROWREF != null)
				msgs = ((InternalEObject)tABLEROWREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLEKEY__TABLEROWREF, null, msgs);
			if (newTABLEROWREF != null)
				msgs = ((InternalEObject)newTABLEROWREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLEKEY__TABLEROWREF, null, msgs);
			msgs = basicSetTABLEROWREF(newTABLEROWREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLEKEY__TABLEROWREF, newTABLEROWREF, newTABLEROWREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLEKEY__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEKEY__TABLEREF:
				return basicSetTABLEREF(null, msgs);
			case OdxXhtmlPackage.TABLEKEY__TABLEROWREF:
				return basicSetTABLEROWREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEKEY__TABLEREF:
				return getTABLEREF();
			case OdxXhtmlPackage.TABLEKEY__TABLEROWREF:
				return getTABLEROWREF();
			case OdxXhtmlPackage.TABLEKEY__ID:
				return getID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEKEY__TABLEREF:
				setTABLEREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.TABLEKEY__TABLEROWREF:
				setTABLEROWREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.TABLEKEY__ID:
				setID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEKEY__TABLEREF:
				setTABLEREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.TABLEKEY__TABLEROWREF:
				setTABLEROWREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.TABLEKEY__ID:
				setID(ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEKEY__TABLEREF:
				return tABLEREF != null;
			case OdxXhtmlPackage.TABLEKEY__TABLEROWREF:
				return tABLEROWREF != null;
			case OdxXhtmlPackage.TABLEKEY__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (iD: ");
		result.append(iD);
		result.append(')');
		return result.toString();
	}

} //TABLEKEYImpl
