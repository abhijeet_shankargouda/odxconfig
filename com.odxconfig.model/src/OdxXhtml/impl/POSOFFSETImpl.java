/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.POSOFFSET;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>POSOFFSET</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.POSOFFSETImpl#getPOSITIVEOFFSET <em>POSITIVEOFFSET</em>}</li>
 * </ul>
 *
 * @generated
 */
public class POSOFFSETImpl extends TARGETADDROFFSETImpl implements POSOFFSET {
	/**
	 * The default value of the '{@link #getPOSITIVEOFFSET() <em>POSITIVEOFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPOSITIVEOFFSET()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] POSITIVEOFFSET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPOSITIVEOFFSET() <em>POSITIVEOFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPOSITIVEOFFSET()
	 * @generated
	 * @ordered
	 */
	protected byte[] pOSITIVEOFFSET = POSITIVEOFFSET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected POSOFFSETImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPOSOFFSET();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getPOSITIVEOFFSET() {
		return pOSITIVEOFFSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPOSITIVEOFFSET(byte[] newPOSITIVEOFFSET) {
		byte[] oldPOSITIVEOFFSET = pOSITIVEOFFSET;
		pOSITIVEOFFSET = newPOSITIVEOFFSET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.POSOFFSET__POSITIVEOFFSET, oldPOSITIVEOFFSET, pOSITIVEOFFSET));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.POSOFFSET__POSITIVEOFFSET:
				return getPOSITIVEOFFSET();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.POSOFFSET__POSITIVEOFFSET:
				setPOSITIVEOFFSET((byte[])newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.POSOFFSET__POSITIVEOFFSET:
				setPOSITIVEOFFSET(POSITIVEOFFSET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.POSOFFSET__POSITIVEOFFSET:
				return POSITIVEOFFSET_EDEFAULT == null ? pOSITIVEOFFSET != null : !POSITIVEOFFSET_EDEFAULT.equals(pOSITIVEOFFSET);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (pOSITIVEOFFSET: ");
		result.append(pOSITIVEOFFSET);
		result.append(')');
		return result.toString();
	}

} //POSOFFSETImpl
