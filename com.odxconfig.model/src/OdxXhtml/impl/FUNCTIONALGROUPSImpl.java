/**
 */
package OdxXhtml.impl;

import OdxXhtml.FUNCTIONALGROUP;
import OdxXhtml.FUNCTIONALGROUPS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FUNCTIONALGROUPS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FUNCTIONALGROUPSImpl#getFUNCTIONALGROUP <em>FUNCTIONALGROUP</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FUNCTIONALGROUPSImpl extends MinimalEObjectImpl.Container implements FUNCTIONALGROUPS {
	/**
	 * The cached value of the '{@link #getFUNCTIONALGROUP() <em>FUNCTIONALGROUP</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFUNCTIONALGROUP()
	 * @generated
	 * @ordered
	 */
	protected EList<FUNCTIONALGROUP> fUNCTIONALGROUP;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FUNCTIONALGROUPSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFUNCTIONALGROUPS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FUNCTIONALGROUP> getFUNCTIONALGROUP() {
		if (fUNCTIONALGROUP == null) {
			fUNCTIONALGROUP = new EObjectContainmentEList<FUNCTIONALGROUP>(FUNCTIONALGROUP.class, this, OdxXhtmlPackage.FUNCTIONALGROUPS__FUNCTIONALGROUP);
		}
		return fUNCTIONALGROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTIONALGROUPS__FUNCTIONALGROUP:
				return ((InternalEList<?>)getFUNCTIONALGROUP()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTIONALGROUPS__FUNCTIONALGROUP:
				return getFUNCTIONALGROUP();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTIONALGROUPS__FUNCTIONALGROUP:
				getFUNCTIONALGROUP().clear();
				getFUNCTIONALGROUP().addAll((Collection<? extends FUNCTIONALGROUP>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTIONALGROUPS__FUNCTIONALGROUP:
				getFUNCTIONALGROUP().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTIONALGROUPS__FUNCTIONALGROUP:
				return fUNCTIONALGROUP != null && !fUNCTIONALGROUP.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FUNCTIONALGROUPSImpl
