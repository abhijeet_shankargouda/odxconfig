/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.VEHICLECONNECTORPIN;
import OdxXhtml.VEHICLECONNECTORPINS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VEHICLECONNECTORPINS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORPINSImpl#getVEHICLECONNECTORPIN <em>VEHICLECONNECTORPIN</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VEHICLECONNECTORPINSImpl extends MinimalEObjectImpl.Container implements VEHICLECONNECTORPINS {
	/**
	 * The cached value of the '{@link #getVEHICLECONNECTORPIN() <em>VEHICLECONNECTORPIN</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVEHICLECONNECTORPIN()
	 * @generated
	 * @ordered
	 */
	protected EList<VEHICLECONNECTORPIN> vEHICLECONNECTORPIN;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VEHICLECONNECTORPINSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVEHICLECONNECTORPINS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<VEHICLECONNECTORPIN> getVEHICLECONNECTORPIN() {
		if (vEHICLECONNECTORPIN == null) {
			vEHICLECONNECTORPIN = new EObjectContainmentEList<VEHICLECONNECTORPIN>(VEHICLECONNECTORPIN.class, this, OdxXhtmlPackage.VEHICLECONNECTORPINS__VEHICLECONNECTORPIN);
		}
		return vEHICLECONNECTORPIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINS__VEHICLECONNECTORPIN:
				return ((InternalEList<?>)getVEHICLECONNECTORPIN()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINS__VEHICLECONNECTORPIN:
				return getVEHICLECONNECTORPIN();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINS__VEHICLECONNECTORPIN:
				getVEHICLECONNECTORPIN().clear();
				getVEHICLECONNECTORPIN().addAll((Collection<? extends VEHICLECONNECTORPIN>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINS__VEHICLECONNECTORPIN:
				getVEHICLECONNECTORPIN().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINS__VEHICLECONNECTORPIN:
				return vEHICLECONNECTORPIN != null && !vEHICLECONNECTORPIN.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //VEHICLECONNECTORPINSImpl
