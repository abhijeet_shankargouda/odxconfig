/**
 */
package OdxXhtml.impl;

import OdxXhtml.OEM;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OEM</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OEMImpl extends INFOCOMPONENTImpl implements OEM {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OEMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getOEM();
	}

} //OEMImpl
