/**
 */
package OdxXhtml.impl;

import OdxXhtml.MODIFICATION1;
import OdxXhtml.MODIFICATIONS1;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MODIFICATIONS1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MODIFICATIONS1Impl#getMODIFICATION <em>MODIFICATION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MODIFICATIONS1Impl extends MinimalEObjectImpl.Container implements MODIFICATIONS1 {
	/**
	 * The cached value of the '{@link #getMODIFICATION() <em>MODIFICATION</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMODIFICATION()
	 * @generated
	 * @ordered
	 */
	protected EList<MODIFICATION1> mODIFICATION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MODIFICATIONS1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMODIFICATIONS1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<MODIFICATION1> getMODIFICATION() {
		if (mODIFICATION == null) {
			mODIFICATION = new EObjectContainmentEList<MODIFICATION1>(MODIFICATION1.class, this, OdxXhtmlPackage.MODIFICATIONS1__MODIFICATION);
		}
		return mODIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS1__MODIFICATION:
				return ((InternalEList<?>)getMODIFICATION()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS1__MODIFICATION:
				return getMODIFICATION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS1__MODIFICATION:
				getMODIFICATION().clear();
				getMODIFICATION().addAll((Collection<? extends MODIFICATION1>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS1__MODIFICATION:
				getMODIFICATION().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATIONS1__MODIFICATION:
				return mODIFICATION != null && !mODIFICATION.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MODIFICATIONS1Impl
