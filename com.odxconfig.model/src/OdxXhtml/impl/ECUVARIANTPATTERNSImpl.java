/**
 */
package OdxXhtml.impl;

import OdxXhtml.ECUVARIANTPATTERN;
import OdxXhtml.ECUVARIANTPATTERNS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUVARIANTPATTERNS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTPATTERNSImpl#getECUVARIANTPATTERN <em>ECUVARIANTPATTERN</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECUVARIANTPATTERNSImpl extends MinimalEObjectImpl.Container implements ECUVARIANTPATTERNS {
	/**
	 * The cached value of the '{@link #getECUVARIANTPATTERN() <em>ECUVARIANTPATTERN</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUVARIANTPATTERN()
	 * @generated
	 * @ordered
	 */
	protected EList<ECUVARIANTPATTERN> eCUVARIANTPATTERN;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUVARIANTPATTERNSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUVARIANTPATTERNS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ECUVARIANTPATTERN> getECUVARIANTPATTERN() {
		if (eCUVARIANTPATTERN == null) {
			eCUVARIANTPATTERN = new EObjectContainmentEList<ECUVARIANTPATTERN>(ECUVARIANTPATTERN.class, this, OdxXhtmlPackage.ECUVARIANTPATTERNS__ECUVARIANTPATTERN);
		}
		return eCUVARIANTPATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERNS__ECUVARIANTPATTERN:
				return ((InternalEList<?>)getECUVARIANTPATTERN()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERNS__ECUVARIANTPATTERN:
				return getECUVARIANTPATTERN();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERNS__ECUVARIANTPATTERN:
				getECUVARIANTPATTERN().clear();
				getECUVARIANTPATTERN().addAll((Collection<? extends ECUVARIANTPATTERN>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERNS__ECUVARIANTPATTERN:
				getECUVARIANTPATTERN().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTPATTERNS__ECUVARIANTPATTERN:
				return eCUVARIANTPATTERN != null && !eCUVARIANTPATTERN.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ECUVARIANTPATTERNSImpl
