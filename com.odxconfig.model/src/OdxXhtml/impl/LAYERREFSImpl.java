/**
 */
package OdxXhtml.impl;

import OdxXhtml.LAYERREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LAYERREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.LAYERREFSImpl#getLAYERREF <em>LAYERREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LAYERREFSImpl extends MinimalEObjectImpl.Container implements LAYERREFS {
	/**
	 * The cached value of the '{@link #getLAYERREF() <em>LAYERREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLAYERREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> lAYERREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LAYERREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLAYERREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getLAYERREF() {
		if (lAYERREF == null) {
			lAYERREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.LAYERREFS__LAYERREF);
		}
		return lAYERREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.LAYERREFS__LAYERREF:
				return ((InternalEList<?>)getLAYERREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.LAYERREFS__LAYERREF:
				return getLAYERREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.LAYERREFS__LAYERREF:
				getLAYERREF().clear();
				getLAYERREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LAYERREFS__LAYERREF:
				getLAYERREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LAYERREFS__LAYERREF:
				return lAYERREF != null && !lAYERREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LAYERREFSImpl
