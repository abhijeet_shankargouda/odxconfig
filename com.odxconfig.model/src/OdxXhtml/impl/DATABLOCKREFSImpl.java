/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATABLOCKREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DATABLOCKREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DATABLOCKREFSImpl#getDATABLOCKREF <em>DATABLOCKREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DATABLOCKREFSImpl extends MinimalEObjectImpl.Container implements DATABLOCKREFS {
	/**
	 * The cached value of the '{@link #getDATABLOCKREF() <em>DATABLOCKREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATABLOCKREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> dATABLOCKREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DATABLOCKREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDATABLOCKREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getDATABLOCKREF() {
		if (dATABLOCKREF == null) {
			dATABLOCKREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.DATABLOCKREFS__DATABLOCKREF);
		}
		return dATABLOCKREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKREFS__DATABLOCKREF:
				return ((InternalEList<?>)getDATABLOCKREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKREFS__DATABLOCKREF:
				return getDATABLOCKREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKREFS__DATABLOCKREF:
				getDATABLOCKREF().clear();
				getDATABLOCKREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKREFS__DATABLOCKREF:
				getDATABLOCKREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKREFS__DATABLOCKREF:
				return dATABLOCKREF != null && !dATABLOCKREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DATABLOCKREFSImpl
