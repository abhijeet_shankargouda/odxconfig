/**
 */
package OdxXhtml.impl;

import OdxXhtml.OWNIDENT;
import OdxXhtml.OWNIDENTS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OWNIDENTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.OWNIDENTSImpl#getOWNIDENT <em>OWNIDENT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OWNIDENTSImpl extends MinimalEObjectImpl.Container implements OWNIDENTS {
	/**
	 * The cached value of the '{@link #getOWNIDENT() <em>OWNIDENT</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOWNIDENT()
	 * @generated
	 * @ordered
	 */
	protected EList<OWNIDENT> oWNIDENT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OWNIDENTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getOWNIDENTS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<OWNIDENT> getOWNIDENT() {
		if (oWNIDENT == null) {
			oWNIDENT = new EObjectContainmentEList<OWNIDENT>(OWNIDENT.class, this, OdxXhtmlPackage.OWNIDENTS__OWNIDENT);
		}
		return oWNIDENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENTS__OWNIDENT:
				return ((InternalEList<?>)getOWNIDENT()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENTS__OWNIDENT:
				return getOWNIDENT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENTS__OWNIDENT:
				getOWNIDENT().clear();
				getOWNIDENT().addAll((Collection<? extends OWNIDENT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENTS__OWNIDENT:
				getOWNIDENT().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.OWNIDENTS__OWNIDENT:
				return oWNIDENT != null && !oWNIDENT.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //OWNIDENTSImpl
