/**
 */
package OdxXhtml.impl;

import OdxXhtml.MEMBERLOGICALLINK;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MEMBERLOGICALLINK</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MEMBERLOGICALLINKImpl extends LOGICALLINKImpl implements MEMBERLOGICALLINK {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MEMBERLOGICALLINKImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMEMBERLOGICALLINK();
	}

} //MEMBERLOGICALLINKImpl
