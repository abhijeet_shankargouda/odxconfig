/**
 */
package OdxXhtml.impl;

import OdxXhtml.INPUTPARAM;
import OdxXhtml.INPUTPARAMS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>INPUTPARAMS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.INPUTPARAMSImpl#getINPUTPARAM <em>INPUTPARAM</em>}</li>
 * </ul>
 *
 * @generated
 */
public class INPUTPARAMSImpl extends MinimalEObjectImpl.Container implements INPUTPARAMS {
	/**
	 * The cached value of the '{@link #getINPUTPARAM() <em>INPUTPARAM</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINPUTPARAM()
	 * @generated
	 * @ordered
	 */
	protected EList<INPUTPARAM> iNPUTPARAM;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INPUTPARAMSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getINPUTPARAMS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<INPUTPARAM> getINPUTPARAM() {
		if (iNPUTPARAM == null) {
			iNPUTPARAM = new EObjectContainmentEList<INPUTPARAM>(INPUTPARAM.class, this, OdxXhtmlPackage.INPUTPARAMS__INPUTPARAM);
		}
		return iNPUTPARAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.INPUTPARAMS__INPUTPARAM:
				return ((InternalEList<?>)getINPUTPARAM()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.INPUTPARAMS__INPUTPARAM:
				return getINPUTPARAM();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.INPUTPARAMS__INPUTPARAM:
				getINPUTPARAM().clear();
				getINPUTPARAM().addAll((Collection<? extends INPUTPARAM>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INPUTPARAMS__INPUTPARAM:
				getINPUTPARAM().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INPUTPARAMS__INPUTPARAM:
				return iNPUTPARAM != null && !iNPUTPARAM.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //INPUTPARAMSImpl
