/**
 */
package OdxXhtml.impl;

import OdxXhtml.ECUVARIANT;
import OdxXhtml.ECUVARIANTS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUVARIANTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTSImpl#getECUVARIANT <em>ECUVARIANT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECUVARIANTSImpl extends MinimalEObjectImpl.Container implements ECUVARIANTS {
	/**
	 * The cached value of the '{@link #getECUVARIANT() <em>ECUVARIANT</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUVARIANT()
	 * @generated
	 * @ordered
	 */
	protected EList<ECUVARIANT> eCUVARIANT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUVARIANTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUVARIANTS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ECUVARIANT> getECUVARIANT() {
		if (eCUVARIANT == null) {
			eCUVARIANT = new EObjectContainmentEList<ECUVARIANT>(ECUVARIANT.class, this, OdxXhtmlPackage.ECUVARIANTS__ECUVARIANT);
		}
		return eCUVARIANT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTS__ECUVARIANT:
				return ((InternalEList<?>)getECUVARIANT()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTS__ECUVARIANT:
				return getECUVARIANT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTS__ECUVARIANT:
				getECUVARIANT().clear();
				getECUVARIANT().addAll((Collection<? extends ECUVARIANT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTS__ECUVARIANT:
				getECUVARIANT().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANTS__ECUVARIANT:
				return eCUVARIANT != null && !eCUVARIANT.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ECUVARIANTSImpl
