/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT;
import OdxXhtml.UNIT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UNIT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.UNITImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITImpl#getDISPLAYNAME <em>DISPLAYNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITImpl#getFACTORSITOUNIT <em>FACTORSITOUNIT</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITImpl#getOFFSETSITOUNIT <em>OFFSETSITOUNIT</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITImpl#getPHYSICALDIMENSIONREF <em>PHYSICALDIMENSIONREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UNITImpl extends MinimalEObjectImpl.Container implements UNIT {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getDISPLAYNAME() <em>DISPLAYNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDISPLAYNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String DISPLAYNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDISPLAYNAME() <em>DISPLAYNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDISPLAYNAME()
	 * @generated
	 * @ordered
	 */
	protected String dISPLAYNAME = DISPLAYNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getFACTORSITOUNIT() <em>FACTORSITOUNIT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFACTORSITOUNIT()
	 * @generated
	 * @ordered
	 */
	protected static final double FACTORSITOUNIT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getFACTORSITOUNIT() <em>FACTORSITOUNIT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFACTORSITOUNIT()
	 * @generated
	 * @ordered
	 */
	protected double fACTORSITOUNIT = FACTORSITOUNIT_EDEFAULT;

	/**
	 * This is true if the FACTORSITOUNIT attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fACTORSITOUNITESet;

	/**
	 * The default value of the '{@link #getOFFSETSITOUNIT() <em>OFFSETSITOUNIT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOFFSETSITOUNIT()
	 * @generated
	 * @ordered
	 */
	protected static final double OFFSETSITOUNIT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getOFFSETSITOUNIT() <em>OFFSETSITOUNIT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOFFSETSITOUNIT()
	 * @generated
	 * @ordered
	 */
	protected double oFFSETSITOUNIT = OFFSETSITOUNIT_EDEFAULT;

	/**
	 * This is true if the OFFSETSITOUNIT attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean oFFSETSITOUNITESet;

	/**
	 * The cached value of the '{@link #getPHYSICALDIMENSIONREF() <em>PHYSICALDIMENSIONREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALDIMENSIONREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK pHYSICALDIMENSIONREF;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UNITImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getUNIT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNIT__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNIT__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNIT__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNIT__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDISPLAYNAME() {
		return dISPLAYNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDISPLAYNAME(String newDISPLAYNAME) {
		String oldDISPLAYNAME = dISPLAYNAME;
		dISPLAYNAME = newDISPLAYNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__DISPLAYNAME, oldDISPLAYNAME, dISPLAYNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public double getFACTORSITOUNIT() {
		return fACTORSITOUNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFACTORSITOUNIT(double newFACTORSITOUNIT) {
		double oldFACTORSITOUNIT = fACTORSITOUNIT;
		fACTORSITOUNIT = newFACTORSITOUNIT;
		boolean oldFACTORSITOUNITESet = fACTORSITOUNITESet;
		fACTORSITOUNITESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__FACTORSITOUNIT, oldFACTORSITOUNIT, fACTORSITOUNIT, !oldFACTORSITOUNITESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetFACTORSITOUNIT() {
		double oldFACTORSITOUNIT = fACTORSITOUNIT;
		boolean oldFACTORSITOUNITESet = fACTORSITOUNITESet;
		fACTORSITOUNIT = FACTORSITOUNIT_EDEFAULT;
		fACTORSITOUNITESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.UNIT__FACTORSITOUNIT, oldFACTORSITOUNIT, FACTORSITOUNIT_EDEFAULT, oldFACTORSITOUNITESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetFACTORSITOUNIT() {
		return fACTORSITOUNITESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public double getOFFSETSITOUNIT() {
		return oFFSETSITOUNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOFFSETSITOUNIT(double newOFFSETSITOUNIT) {
		double oldOFFSETSITOUNIT = oFFSETSITOUNIT;
		oFFSETSITOUNIT = newOFFSETSITOUNIT;
		boolean oldOFFSETSITOUNITESet = oFFSETSITOUNITESet;
		oFFSETSITOUNITESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__OFFSETSITOUNIT, oldOFFSETSITOUNIT, oFFSETSITOUNIT, !oldOFFSETSITOUNITESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetOFFSETSITOUNIT() {
		double oldOFFSETSITOUNIT = oFFSETSITOUNIT;
		boolean oldOFFSETSITOUNITESet = oFFSETSITOUNITESet;
		oFFSETSITOUNIT = OFFSETSITOUNIT_EDEFAULT;
		oFFSETSITOUNITESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.UNIT__OFFSETSITOUNIT, oldOFFSETSITOUNIT, OFFSETSITOUNIT_EDEFAULT, oldOFFSETSITOUNITESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetOFFSETSITOUNIT() {
		return oFFSETSITOUNITESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getPHYSICALDIMENSIONREF() {
		return pHYSICALDIMENSIONREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPHYSICALDIMENSIONREF(ODXLINK newPHYSICALDIMENSIONREF, NotificationChain msgs) {
		ODXLINK oldPHYSICALDIMENSIONREF = pHYSICALDIMENSIONREF;
		pHYSICALDIMENSIONREF = newPHYSICALDIMENSIONREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__PHYSICALDIMENSIONREF, oldPHYSICALDIMENSIONREF, newPHYSICALDIMENSIONREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHYSICALDIMENSIONREF(ODXLINK newPHYSICALDIMENSIONREF) {
		if (newPHYSICALDIMENSIONREF != pHYSICALDIMENSIONREF) {
			NotificationChain msgs = null;
			if (pHYSICALDIMENSIONREF != null)
				msgs = ((InternalEObject)pHYSICALDIMENSIONREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNIT__PHYSICALDIMENSIONREF, null, msgs);
			if (newPHYSICALDIMENSIONREF != null)
				msgs = ((InternalEObject)newPHYSICALDIMENSIONREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNIT__PHYSICALDIMENSIONREF, null, msgs);
			msgs = basicSetPHYSICALDIMENSIONREF(newPHYSICALDIMENSIONREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__PHYSICALDIMENSIONREF, newPHYSICALDIMENSIONREF, newPHYSICALDIMENSIONREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNIT__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.UNIT__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.UNIT__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.UNIT__PHYSICALDIMENSIONREF:
				return basicSetPHYSICALDIMENSIONREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.UNIT__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.UNIT__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.UNIT__DESC:
				return getDESC();
			case OdxXhtmlPackage.UNIT__DISPLAYNAME:
				return getDISPLAYNAME();
			case OdxXhtmlPackage.UNIT__FACTORSITOUNIT:
				return getFACTORSITOUNIT();
			case OdxXhtmlPackage.UNIT__OFFSETSITOUNIT:
				return getOFFSETSITOUNIT();
			case OdxXhtmlPackage.UNIT__PHYSICALDIMENSIONREF:
				return getPHYSICALDIMENSIONREF();
			case OdxXhtmlPackage.UNIT__ID:
				return getID();
			case OdxXhtmlPackage.UNIT__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.UNIT__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.UNIT__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.UNIT__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.UNIT__DISPLAYNAME:
				setDISPLAYNAME((String)newValue);
				return;
			case OdxXhtmlPackage.UNIT__FACTORSITOUNIT:
				setFACTORSITOUNIT((Double)newValue);
				return;
			case OdxXhtmlPackage.UNIT__OFFSETSITOUNIT:
				setOFFSETSITOUNIT((Double)newValue);
				return;
			case OdxXhtmlPackage.UNIT__PHYSICALDIMENSIONREF:
				setPHYSICALDIMENSIONREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.UNIT__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.UNIT__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.UNIT__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.UNIT__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.UNIT__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.UNIT__DISPLAYNAME:
				setDISPLAYNAME(DISPLAYNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.UNIT__FACTORSITOUNIT:
				unsetFACTORSITOUNIT();
				return;
			case OdxXhtmlPackage.UNIT__OFFSETSITOUNIT:
				unsetOFFSETSITOUNIT();
				return;
			case OdxXhtmlPackage.UNIT__PHYSICALDIMENSIONREF:
				setPHYSICALDIMENSIONREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.UNIT__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.UNIT__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.UNIT__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.UNIT__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.UNIT__DESC:
				return dESC != null;
			case OdxXhtmlPackage.UNIT__DISPLAYNAME:
				return DISPLAYNAME_EDEFAULT == null ? dISPLAYNAME != null : !DISPLAYNAME_EDEFAULT.equals(dISPLAYNAME);
			case OdxXhtmlPackage.UNIT__FACTORSITOUNIT:
				return isSetFACTORSITOUNIT();
			case OdxXhtmlPackage.UNIT__OFFSETSITOUNIT:
				return isSetOFFSETSITOUNIT();
			case OdxXhtmlPackage.UNIT__PHYSICALDIMENSIONREF:
				return pHYSICALDIMENSIONREF != null;
			case OdxXhtmlPackage.UNIT__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.UNIT__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", dISPLAYNAME: ");
		result.append(dISPLAYNAME);
		result.append(", fACTORSITOUNIT: ");
		if (fACTORSITOUNITESet) result.append(fACTORSITOUNIT); else result.append("<unset>");
		result.append(", oFFSETSITOUNIT: ");
		if (oFFSETSITOUNITESet) result.append(oFFSETSITOUNIT); else result.append("<unset>");
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //UNITImpl
