/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEAMMEMBER;
import OdxXhtml.TEAMMEMBERS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TEAMMEMBERS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBERSImpl#getTEAMMEMBER <em>TEAMMEMBER</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TEAMMEMBERSImpl extends MinimalEObjectImpl.Container implements TEAMMEMBERS {
	/**
	 * The cached value of the '{@link #getTEAMMEMBER() <em>TEAMMEMBER</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTEAMMEMBER()
	 * @generated
	 * @ordered
	 */
	protected EList<TEAMMEMBER> tEAMMEMBER;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TEAMMEMBERSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getTEAMMEMBERS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TEAMMEMBER> getTEAMMEMBER() {
		if (tEAMMEMBER == null) {
			tEAMMEMBER = new EObjectContainmentEList<TEAMMEMBER>(TEAMMEMBER.class, this, OdxXhtmlPackage.TEAMMEMBERS__TEAMMEMBER);
		}
		return tEAMMEMBER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS__TEAMMEMBER:
				return ((InternalEList<?>)getTEAMMEMBER()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS__TEAMMEMBER:
				return getTEAMMEMBER();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS__TEAMMEMBER:
				getTEAMMEMBER().clear();
				getTEAMMEMBER().addAll((Collection<? extends TEAMMEMBER>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS__TEAMMEMBER:
				getTEAMMEMBER().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBERS__TEAMMEMBER:
				return tEAMMEMBER != null && !tEAMMEMBER.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TEAMMEMBERSImpl
