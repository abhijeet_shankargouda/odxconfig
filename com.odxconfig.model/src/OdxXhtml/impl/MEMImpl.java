/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATABLOCKS;
import OdxXhtml.FLASHDATAS;
import OdxXhtml.MEM;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SESSIONS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MEM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MEMImpl#getSESSIONS <em>SESSIONS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MEMImpl#getDATABLOCKS <em>DATABLOCKS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MEMImpl#getFLASHDATAS <em>FLASHDATAS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MEMImpl extends MinimalEObjectImpl.Container implements MEM {
	/**
	 * The cached value of the '{@link #getSESSIONS() <em>SESSIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSESSIONS()
	 * @generated
	 * @ordered
	 */
	protected SESSIONS sESSIONS;

	/**
	 * The cached value of the '{@link #getDATABLOCKS() <em>DATABLOCKS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATABLOCKS()
	 * @generated
	 * @ordered
	 */
	protected DATABLOCKS dATABLOCKS;

	/**
	 * The cached value of the '{@link #getFLASHDATAS() <em>FLASHDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFLASHDATAS()
	 * @generated
	 * @ordered
	 */
	protected FLASHDATAS fLASHDATAS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MEMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMEM();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SESSIONS getSESSIONS() {
		return sESSIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSESSIONS(SESSIONS newSESSIONS, NotificationChain msgs) {
		SESSIONS oldSESSIONS = sESSIONS;
		sESSIONS = newSESSIONS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MEM__SESSIONS, oldSESSIONS, newSESSIONS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSESSIONS(SESSIONS newSESSIONS) {
		if (newSESSIONS != sESSIONS) {
			NotificationChain msgs = null;
			if (sESSIONS != null)
				msgs = ((InternalEObject)sESSIONS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MEM__SESSIONS, null, msgs);
			if (newSESSIONS != null)
				msgs = ((InternalEObject)newSESSIONS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MEM__SESSIONS, null, msgs);
			msgs = basicSetSESSIONS(newSESSIONS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MEM__SESSIONS, newSESSIONS, newSESSIONS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATABLOCKS getDATABLOCKS() {
		return dATABLOCKS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDATABLOCKS(DATABLOCKS newDATABLOCKS, NotificationChain msgs) {
		DATABLOCKS oldDATABLOCKS = dATABLOCKS;
		dATABLOCKS = newDATABLOCKS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MEM__DATABLOCKS, oldDATABLOCKS, newDATABLOCKS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATABLOCKS(DATABLOCKS newDATABLOCKS) {
		if (newDATABLOCKS != dATABLOCKS) {
			NotificationChain msgs = null;
			if (dATABLOCKS != null)
				msgs = ((InternalEObject)dATABLOCKS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MEM__DATABLOCKS, null, msgs);
			if (newDATABLOCKS != null)
				msgs = ((InternalEObject)newDATABLOCKS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MEM__DATABLOCKS, null, msgs);
			msgs = basicSetDATABLOCKS(newDATABLOCKS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MEM__DATABLOCKS, newDATABLOCKS, newDATABLOCKS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FLASHDATAS getFLASHDATAS() {
		return fLASHDATAS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFLASHDATAS(FLASHDATAS newFLASHDATAS, NotificationChain msgs) {
		FLASHDATAS oldFLASHDATAS = fLASHDATAS;
		fLASHDATAS = newFLASHDATAS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MEM__FLASHDATAS, oldFLASHDATAS, newFLASHDATAS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFLASHDATAS(FLASHDATAS newFLASHDATAS) {
		if (newFLASHDATAS != fLASHDATAS) {
			NotificationChain msgs = null;
			if (fLASHDATAS != null)
				msgs = ((InternalEObject)fLASHDATAS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MEM__FLASHDATAS, null, msgs);
			if (newFLASHDATAS != null)
				msgs = ((InternalEObject)newFLASHDATAS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MEM__FLASHDATAS, null, msgs);
			msgs = basicSetFLASHDATAS(newFLASHDATAS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MEM__FLASHDATAS, newFLASHDATAS, newFLASHDATAS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MEM__SESSIONS:
				return basicSetSESSIONS(null, msgs);
			case OdxXhtmlPackage.MEM__DATABLOCKS:
				return basicSetDATABLOCKS(null, msgs);
			case OdxXhtmlPackage.MEM__FLASHDATAS:
				return basicSetFLASHDATAS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MEM__SESSIONS:
				return getSESSIONS();
			case OdxXhtmlPackage.MEM__DATABLOCKS:
				return getDATABLOCKS();
			case OdxXhtmlPackage.MEM__FLASHDATAS:
				return getFLASHDATAS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MEM__SESSIONS:
				setSESSIONS((SESSIONS)newValue);
				return;
			case OdxXhtmlPackage.MEM__DATABLOCKS:
				setDATABLOCKS((DATABLOCKS)newValue);
				return;
			case OdxXhtmlPackage.MEM__FLASHDATAS:
				setFLASHDATAS((FLASHDATAS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MEM__SESSIONS:
				setSESSIONS((SESSIONS)null);
				return;
			case OdxXhtmlPackage.MEM__DATABLOCKS:
				setDATABLOCKS((DATABLOCKS)null);
				return;
			case OdxXhtmlPackage.MEM__FLASHDATAS:
				setFLASHDATAS((FLASHDATAS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MEM__SESSIONS:
				return sESSIONS != null;
			case OdxXhtmlPackage.MEM__DATABLOCKS:
				return dATABLOCKS != null;
			case OdxXhtmlPackage.MEM__FLASHDATAS:
				return fLASHDATAS != null;
		}
		return super.eIsSet(featureID);
	}

} //MEMImpl
