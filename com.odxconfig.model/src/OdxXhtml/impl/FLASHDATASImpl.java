/**
 */
package OdxXhtml.impl;

import OdxXhtml.FLASHDATA;
import OdxXhtml.FLASHDATAS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FLASHDATAS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FLASHDATASImpl#getFLASHDATA <em>FLASHDATA</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FLASHDATASImpl extends MinimalEObjectImpl.Container implements FLASHDATAS {
	/**
	 * The cached value of the '{@link #getFLASHDATA() <em>FLASHDATA</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFLASHDATA()
	 * @generated
	 * @ordered
	 */
	protected EList<FLASHDATA> fLASHDATA;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FLASHDATASImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFLASHDATAS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FLASHDATA> getFLASHDATA() {
		if (fLASHDATA == null) {
			fLASHDATA = new EObjectContainmentEList<FLASHDATA>(FLASHDATA.class, this, OdxXhtmlPackage.FLASHDATAS__FLASHDATA);
		}
		return fLASHDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATAS__FLASHDATA:
				return ((InternalEList<?>)getFLASHDATA()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATAS__FLASHDATA:
				return getFLASHDATA();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATAS__FLASHDATA:
				getFLASHDATA().clear();
				getFLASHDATA().addAll((Collection<? extends FLASHDATA>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATAS__FLASHDATA:
				getFLASHDATA().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FLASHDATAS__FLASHDATA:
				return fLASHDATA != null && !fLASHDATA.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FLASHDATASImpl
