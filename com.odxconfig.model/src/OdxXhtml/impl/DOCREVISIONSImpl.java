/**
 */
package OdxXhtml.impl;

import OdxXhtml.DOCREVISION;
import OdxXhtml.DOCREVISIONS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DOCREVISIONS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DOCREVISIONSImpl#getDOCREVISION <em>DOCREVISION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DOCREVISIONSImpl extends MinimalEObjectImpl.Container implements DOCREVISIONS {
	/**
	 * The cached value of the '{@link #getDOCREVISION() <em>DOCREVISION</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREVISION()
	 * @generated
	 * @ordered
	 */
	protected EList<DOCREVISION> dOCREVISION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DOCREVISIONSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDOCREVISIONS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DOCREVISION> getDOCREVISION() {
		if (dOCREVISION == null) {
			dOCREVISION = new EObjectContainmentEList<DOCREVISION>(DOCREVISION.class, this, OdxXhtmlPackage.DOCREVISIONS__DOCREVISION);
		}
		return dOCREVISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS__DOCREVISION:
				return ((InternalEList<?>)getDOCREVISION()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS__DOCREVISION:
				return getDOCREVISION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS__DOCREVISION:
				getDOCREVISION().clear();
				getDOCREVISION().addAll((Collection<? extends DOCREVISION>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS__DOCREVISION:
				getDOCREVISION().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS__DOCREVISION:
				return dOCREVISION != null && !dOCREVISION.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DOCREVISIONSImpl
