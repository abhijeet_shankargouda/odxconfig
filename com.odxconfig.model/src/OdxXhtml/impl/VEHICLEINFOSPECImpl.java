/**
 */
package OdxXhtml.impl;

import OdxXhtml.INFOCOMPONENTS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.VEHICLEINFORMATIONS;
import OdxXhtml.VEHICLEINFOSPEC;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VEHICLEINFOSPEC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFOSPECImpl#getINFOCOMPONENTS <em>INFOCOMPONENTS</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLEINFOSPECImpl#getVEHICLEINFORMATIONS <em>VEHICLEINFORMATIONS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VEHICLEINFOSPECImpl extends ODXCATEGORYImpl implements VEHICLEINFOSPEC {
	/**
	 * The cached value of the '{@link #getINFOCOMPONENTS() <em>INFOCOMPONENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINFOCOMPONENTS()
	 * @generated
	 * @ordered
	 */
	protected INFOCOMPONENTS iNFOCOMPONENTS;

	/**
	 * The cached value of the '{@link #getVEHICLEINFORMATIONS() <em>VEHICLEINFORMATIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVEHICLEINFORMATIONS()
	 * @generated
	 * @ordered
	 */
	protected VEHICLEINFORMATIONS vEHICLEINFORMATIONS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VEHICLEINFOSPECImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVEHICLEINFOSPEC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INFOCOMPONENTS getINFOCOMPONENTS() {
		return iNFOCOMPONENTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetINFOCOMPONENTS(INFOCOMPONENTS newINFOCOMPONENTS, NotificationChain msgs) {
		INFOCOMPONENTS oldINFOCOMPONENTS = iNFOCOMPONENTS;
		iNFOCOMPONENTS = newINFOCOMPONENTS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFOSPEC__INFOCOMPONENTS, oldINFOCOMPONENTS, newINFOCOMPONENTS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setINFOCOMPONENTS(INFOCOMPONENTS newINFOCOMPONENTS) {
		if (newINFOCOMPONENTS != iNFOCOMPONENTS) {
			NotificationChain msgs = null;
			if (iNFOCOMPONENTS != null)
				msgs = ((InternalEObject)iNFOCOMPONENTS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFOSPEC__INFOCOMPONENTS, null, msgs);
			if (newINFOCOMPONENTS != null)
				msgs = ((InternalEObject)newINFOCOMPONENTS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFOSPEC__INFOCOMPONENTS, null, msgs);
			msgs = basicSetINFOCOMPONENTS(newINFOCOMPONENTS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFOSPEC__INFOCOMPONENTS, newINFOCOMPONENTS, newINFOCOMPONENTS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLEINFORMATIONS getVEHICLEINFORMATIONS() {
		return vEHICLEINFORMATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVEHICLEINFORMATIONS(VEHICLEINFORMATIONS newVEHICLEINFORMATIONS, NotificationChain msgs) {
		VEHICLEINFORMATIONS oldVEHICLEINFORMATIONS = vEHICLEINFORMATIONS;
		vEHICLEINFORMATIONS = newVEHICLEINFORMATIONS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFOSPEC__VEHICLEINFORMATIONS, oldVEHICLEINFORMATIONS, newVEHICLEINFORMATIONS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVEHICLEINFORMATIONS(VEHICLEINFORMATIONS newVEHICLEINFORMATIONS) {
		if (newVEHICLEINFORMATIONS != vEHICLEINFORMATIONS) {
			NotificationChain msgs = null;
			if (vEHICLEINFORMATIONS != null)
				msgs = ((InternalEObject)vEHICLEINFORMATIONS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFOSPEC__VEHICLEINFORMATIONS, null, msgs);
			if (newVEHICLEINFORMATIONS != null)
				msgs = ((InternalEObject)newVEHICLEINFORMATIONS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLEINFOSPEC__VEHICLEINFORMATIONS, null, msgs);
			msgs = basicSetVEHICLEINFORMATIONS(newVEHICLEINFORMATIONS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLEINFOSPEC__VEHICLEINFORMATIONS, newVEHICLEINFORMATIONS, newVEHICLEINFORMATIONS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFOSPEC__INFOCOMPONENTS:
				return basicSetINFOCOMPONENTS(null, msgs);
			case OdxXhtmlPackage.VEHICLEINFOSPEC__VEHICLEINFORMATIONS:
				return basicSetVEHICLEINFORMATIONS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFOSPEC__INFOCOMPONENTS:
				return getINFOCOMPONENTS();
			case OdxXhtmlPackage.VEHICLEINFOSPEC__VEHICLEINFORMATIONS:
				return getVEHICLEINFORMATIONS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFOSPEC__INFOCOMPONENTS:
				setINFOCOMPONENTS((INFOCOMPONENTS)newValue);
				return;
			case OdxXhtmlPackage.VEHICLEINFOSPEC__VEHICLEINFORMATIONS:
				setVEHICLEINFORMATIONS((VEHICLEINFORMATIONS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFOSPEC__INFOCOMPONENTS:
				setINFOCOMPONENTS((INFOCOMPONENTS)null);
				return;
			case OdxXhtmlPackage.VEHICLEINFOSPEC__VEHICLEINFORMATIONS:
				setVEHICLEINFORMATIONS((VEHICLEINFORMATIONS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLEINFOSPEC__INFOCOMPONENTS:
				return iNFOCOMPONENTS != null;
			case OdxXhtmlPackage.VEHICLEINFOSPEC__VEHICLEINFORMATIONS:
				return vEHICLEINFORMATIONS != null;
		}
		return super.eIsSet(featureID);
	}

} //VEHICLEINFOSPECImpl
