/**
 */
package OdxXhtml.impl;

import OdxXhtml.DYNAMICENDMARKERFIELD;
import OdxXhtml.DYNENDDOPREF;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DYNAMICENDMARKERFIELD</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DYNAMICENDMARKERFIELDImpl#getDATAOBJECTPROPREF <em>DATAOBJECTPROPREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DYNAMICENDMARKERFIELDImpl extends FIELDImpl implements DYNAMICENDMARKERFIELD {
	/**
	 * The cached value of the '{@link #getDATAOBJECTPROPREF() <em>DATAOBJECTPROPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATAOBJECTPROPREF()
	 * @generated
	 * @ordered
	 */
	protected DYNENDDOPREF dATAOBJECTPROPREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DYNAMICENDMARKERFIELDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDYNAMICENDMARKERFIELD();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNENDDOPREF getDATAOBJECTPROPREF() {
		return dATAOBJECTPROPREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDATAOBJECTPROPREF(DYNENDDOPREF newDATAOBJECTPROPREF, NotificationChain msgs) {
		DYNENDDOPREF oldDATAOBJECTPROPREF = dATAOBJECTPROPREF;
		dATAOBJECTPROPREF = newDATAOBJECTPROPREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNAMICENDMARKERFIELD__DATAOBJECTPROPREF, oldDATAOBJECTPROPREF, newDATAOBJECTPROPREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATAOBJECTPROPREF(DYNENDDOPREF newDATAOBJECTPROPREF) {
		if (newDATAOBJECTPROPREF != dATAOBJECTPROPREF) {
			NotificationChain msgs = null;
			if (dATAOBJECTPROPREF != null)
				msgs = ((InternalEObject)dATAOBJECTPROPREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNAMICENDMARKERFIELD__DATAOBJECTPROPREF, null, msgs);
			if (newDATAOBJECTPROPREF != null)
				msgs = ((InternalEObject)newDATAOBJECTPROPREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNAMICENDMARKERFIELD__DATAOBJECTPROPREF, null, msgs);
			msgs = basicSetDATAOBJECTPROPREF(newDATAOBJECTPROPREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNAMICENDMARKERFIELD__DATAOBJECTPROPREF, newDATAOBJECTPROPREF, newDATAOBJECTPROPREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELD__DATAOBJECTPROPREF:
				return basicSetDATAOBJECTPROPREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELD__DATAOBJECTPROPREF:
				return getDATAOBJECTPROPREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELD__DATAOBJECTPROPREF:
				setDATAOBJECTPROPREF((DYNENDDOPREF)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELD__DATAOBJECTPROPREF:
				setDATAOBJECTPROPREF((DYNENDDOPREF)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELD__DATAOBJECTPROPREF:
				return dATAOBJECTPROPREF != null;
		}
		return super.eIsSet(featureID);
	}

} //DYNAMICENDMARKERFIELDImpl
