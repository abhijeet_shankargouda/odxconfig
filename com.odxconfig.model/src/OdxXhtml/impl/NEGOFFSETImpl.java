/**
 */
package OdxXhtml.impl;

import OdxXhtml.NEGOFFSET;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NEGOFFSET</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.NEGOFFSETImpl#getNEGATIVEOFFSET <em>NEGATIVEOFFSET</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NEGOFFSETImpl extends TARGETADDROFFSETImpl implements NEGOFFSET {
	/**
	 * The default value of the '{@link #getNEGATIVEOFFSET() <em>NEGATIVEOFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNEGATIVEOFFSET()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] NEGATIVEOFFSET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNEGATIVEOFFSET() <em>NEGATIVEOFFSET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNEGATIVEOFFSET()
	 * @generated
	 * @ordered
	 */
	protected byte[] nEGATIVEOFFSET = NEGATIVEOFFSET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NEGOFFSETImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getNEGOFFSET();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getNEGATIVEOFFSET() {
		return nEGATIVEOFFSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNEGATIVEOFFSET(byte[] newNEGATIVEOFFSET) {
		byte[] oldNEGATIVEOFFSET = nEGATIVEOFFSET;
		nEGATIVEOFFSET = newNEGATIVEOFFSET;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.NEGOFFSET__NEGATIVEOFFSET, oldNEGATIVEOFFSET, nEGATIVEOFFSET));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGOFFSET__NEGATIVEOFFSET:
				return getNEGATIVEOFFSET();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGOFFSET__NEGATIVEOFFSET:
				setNEGATIVEOFFSET((byte[])newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGOFFSET__NEGATIVEOFFSET:
				setNEGATIVEOFFSET(NEGATIVEOFFSET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGOFFSET__NEGATIVEOFFSET:
				return NEGATIVEOFFSET_EDEFAULT == null ? nEGATIVEOFFSET != null : !NEGATIVEOFFSET_EDEFAULT.equals(nEGATIVEOFFSET);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (nEGATIVEOFFSET: ");
		result.append(nEGATIVEOFFSET);
		result.append(')');
		return result.toString();
	}

} //NEGOFFSETImpl
