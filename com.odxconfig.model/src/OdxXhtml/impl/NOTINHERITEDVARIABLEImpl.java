/**
 */
package OdxXhtml.impl;

import OdxXhtml.NOTINHERITEDVARIABLE;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NOTINHERITEDVARIABLE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.NOTINHERITEDVARIABLEImpl#getDIAGVARIABLESNREF <em>DIAGVARIABLESNREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NOTINHERITEDVARIABLEImpl extends MinimalEObjectImpl.Container implements NOTINHERITEDVARIABLE {
	/**
	 * The cached value of the '{@link #getDIAGVARIABLESNREF() <em>DIAGVARIABLESNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGVARIABLESNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dIAGVARIABLESNREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NOTINHERITEDVARIABLEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getNOTINHERITEDVARIABLE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDIAGVARIABLESNREF() {
		return dIAGVARIABLESNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGVARIABLESNREF(SNREF newDIAGVARIABLESNREF, NotificationChain msgs) {
		SNREF oldDIAGVARIABLESNREF = dIAGVARIABLESNREF;
		dIAGVARIABLESNREF = newDIAGVARIABLESNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.NOTINHERITEDVARIABLE__DIAGVARIABLESNREF, oldDIAGVARIABLESNREF, newDIAGVARIABLESNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGVARIABLESNREF(SNREF newDIAGVARIABLESNREF) {
		if (newDIAGVARIABLESNREF != dIAGVARIABLESNREF) {
			NotificationChain msgs = null;
			if (dIAGVARIABLESNREF != null)
				msgs = ((InternalEObject)dIAGVARIABLESNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.NOTINHERITEDVARIABLE__DIAGVARIABLESNREF, null, msgs);
			if (newDIAGVARIABLESNREF != null)
				msgs = ((InternalEObject)newDIAGVARIABLESNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.NOTINHERITEDVARIABLE__DIAGVARIABLESNREF, null, msgs);
			msgs = basicSetDIAGVARIABLESNREF(newDIAGVARIABLESNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.NOTINHERITEDVARIABLE__DIAGVARIABLESNREF, newDIAGVARIABLESNREF, newDIAGVARIABLESNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLE__DIAGVARIABLESNREF:
				return basicSetDIAGVARIABLESNREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLE__DIAGVARIABLESNREF:
				return getDIAGVARIABLESNREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLE__DIAGVARIABLESNREF:
				setDIAGVARIABLESNREF((SNREF)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLE__DIAGVARIABLESNREF:
				setDIAGVARIABLESNREF((SNREF)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLE__DIAGVARIABLESNREF:
				return dIAGVARIABLESNREF != null;
		}
		return super.eIsSet(featureID);
	}

} //NOTINHERITEDVARIABLEImpl
