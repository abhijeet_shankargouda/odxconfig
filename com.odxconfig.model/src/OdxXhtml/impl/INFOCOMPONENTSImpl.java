/**
 */
package OdxXhtml.impl;

import OdxXhtml.INFOCOMPONENT;
import OdxXhtml.INFOCOMPONENTS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>INFOCOMPONENTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.INFOCOMPONENTSImpl#getINFOCOMPONENT <em>INFOCOMPONENT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class INFOCOMPONENTSImpl extends MinimalEObjectImpl.Container implements INFOCOMPONENTS {
	/**
	 * The cached value of the '{@link #getINFOCOMPONENT() <em>INFOCOMPONENT</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINFOCOMPONENT()
	 * @generated
	 * @ordered
	 */
	protected EList<INFOCOMPONENT> iNFOCOMPONENT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INFOCOMPONENTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getINFOCOMPONENTS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<INFOCOMPONENT> getINFOCOMPONENT() {
		if (iNFOCOMPONENT == null) {
			iNFOCOMPONENT = new EObjectContainmentEList<INFOCOMPONENT>(INFOCOMPONENT.class, this, OdxXhtmlPackage.INFOCOMPONENTS__INFOCOMPONENT);
		}
		return iNFOCOMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTS__INFOCOMPONENT:
				return ((InternalEList<?>)getINFOCOMPONENT()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTS__INFOCOMPONENT:
				return getINFOCOMPONENT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTS__INFOCOMPONENT:
				getINFOCOMPONENT().clear();
				getINFOCOMPONENT().addAll((Collection<? extends INFOCOMPONENT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTS__INFOCOMPONENT:
				getINFOCOMPONENT().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INFOCOMPONENTS__INFOCOMPONENT:
				return iNFOCOMPONENT != null && !iNFOCOMPONENT.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //INFOCOMPONENTSImpl
