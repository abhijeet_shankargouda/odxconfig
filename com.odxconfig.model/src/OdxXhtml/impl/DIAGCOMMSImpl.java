/**
 */
package OdxXhtml.impl;

import OdxXhtml.DIAGCOMMS;
import OdxXhtml.DIAGSERVICE;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SINGLEECUJOB;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGCOMMS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMSImpl#getDIAGCOMMPROXY <em>DIAGCOMMPROXY</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMSImpl#getDIAGSERVICE <em>DIAGSERVICE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMSImpl#getSINGLEECUJOB <em>SINGLEECUJOB</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMSImpl#getDIAGCOMMREF <em>DIAGCOMMREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGCOMMSImpl extends MinimalEObjectImpl.Container implements DIAGCOMMS {
	/**
	 * The cached value of the '{@link #getDIAGCOMMPROXY() <em>DIAGCOMMPROXY</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCOMMPROXY()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap dIAGCOMMPROXY;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGCOMMSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGCOMMS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getDIAGCOMMPROXY() {
		if (dIAGCOMMPROXY == null) {
			dIAGCOMMPROXY = new BasicFeatureMap(this, OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMPROXY);
		}
		return dIAGCOMMPROXY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DIAGSERVICE> getDIAGSERVICE() {
		return getDIAGCOMMPROXY().list(OdxXhtmlPackage.eINSTANCE.getDIAGCOMMS_DIAGSERVICE());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SINGLEECUJOB> getSINGLEECUJOB() {
		return getDIAGCOMMPROXY().list(OdxXhtmlPackage.eINSTANCE.getDIAGCOMMS_SINGLEECUJOB());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getDIAGCOMMREF() {
		return getDIAGCOMMPROXY().list(OdxXhtmlPackage.eINSTANCE.getDIAGCOMMS_DIAGCOMMREF());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMPROXY:
				return ((InternalEList<?>)getDIAGCOMMPROXY()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DIAGCOMMS__DIAGSERVICE:
				return ((InternalEList<?>)getDIAGSERVICE()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DIAGCOMMS__SINGLEECUJOB:
				return ((InternalEList<?>)getSINGLEECUJOB()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMREF:
				return ((InternalEList<?>)getDIAGCOMMREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMPROXY:
				if (coreType) return getDIAGCOMMPROXY();
				return ((FeatureMap.Internal)getDIAGCOMMPROXY()).getWrapper();
			case OdxXhtmlPackage.DIAGCOMMS__DIAGSERVICE:
				return getDIAGSERVICE();
			case OdxXhtmlPackage.DIAGCOMMS__SINGLEECUJOB:
				return getSINGLEECUJOB();
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMREF:
				return getDIAGCOMMREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMPROXY:
				((FeatureMap.Internal)getDIAGCOMMPROXY()).set(newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMMS__DIAGSERVICE:
				getDIAGSERVICE().clear();
				getDIAGSERVICE().addAll((Collection<? extends DIAGSERVICE>)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMMS__SINGLEECUJOB:
				getSINGLEECUJOB().clear();
				getSINGLEECUJOB().addAll((Collection<? extends SINGLEECUJOB>)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMREF:
				getDIAGCOMMREF().clear();
				getDIAGCOMMREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMPROXY:
				getDIAGCOMMPROXY().clear();
				return;
			case OdxXhtmlPackage.DIAGCOMMS__DIAGSERVICE:
				getDIAGSERVICE().clear();
				return;
			case OdxXhtmlPackage.DIAGCOMMS__SINGLEECUJOB:
				getSINGLEECUJOB().clear();
				return;
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMREF:
				getDIAGCOMMREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMPROXY:
				return dIAGCOMMPROXY != null && !dIAGCOMMPROXY.isEmpty();
			case OdxXhtmlPackage.DIAGCOMMS__DIAGSERVICE:
				return !getDIAGSERVICE().isEmpty();
			case OdxXhtmlPackage.DIAGCOMMS__SINGLEECUJOB:
				return !getSINGLEECUJOB().isEmpty();
			case OdxXhtmlPackage.DIAGCOMMS__DIAGCOMMREF:
				return !getDIAGCOMMREF().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (dIAGCOMMPROXY: ");
		result.append(dIAGCOMMPROXY);
		result.append(')');
		return result.toString();
	}

} //DIAGCOMMSImpl
