/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPUCONST;
import OdxXhtml.COMPUINVERSEVALUE;
import OdxXhtml.COMPURATIONALCOEFFS;
import OdxXhtml.COMPUSCALE;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.LIMIT;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPUSCALE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPUSCALEImpl#getSHORTLABEL <em>SHORTLABEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUSCALEImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUSCALEImpl#getLOWERLIMIT <em>LOWERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUSCALEImpl#getUPPERLIMIT <em>UPPERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUSCALEImpl#getCOMPUINVERSEVALUE <em>COMPUINVERSEVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUSCALEImpl#getCOMPUCONST <em>COMPUCONST</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUSCALEImpl#getCOMPURATIONALCOEFFS <em>COMPURATIONALCOEFFS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPUSCALEImpl extends MinimalEObjectImpl.Container implements COMPUSCALE {
	/**
	 * The cached value of the '{@link #getSHORTLABEL() <em>SHORTLABEL</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTLABEL()
	 * @generated
	 * @ordered
	 */
	protected TEXT sHORTLABEL;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getLOWERLIMIT() <em>LOWERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLOWERLIMIT()
	 * @generated
	 * @ordered
	 */
	protected LIMIT lOWERLIMIT;

	/**
	 * The cached value of the '{@link #getUPPERLIMIT() <em>UPPERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUPPERLIMIT()
	 * @generated
	 * @ordered
	 */
	protected LIMIT uPPERLIMIT;

	/**
	 * The cached value of the '{@link #getCOMPUINVERSEVALUE() <em>COMPUINVERSEVALUE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUINVERSEVALUE()
	 * @generated
	 * @ordered
	 */
	protected COMPUINVERSEVALUE cOMPUINVERSEVALUE;

	/**
	 * The cached value of the '{@link #getCOMPUCONST() <em>COMPUCONST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUCONST()
	 * @generated
	 * @ordered
	 */
	protected COMPUCONST cOMPUCONST;

	/**
	 * The cached value of the '{@link #getCOMPURATIONALCOEFFS() <em>COMPURATIONALCOEFFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPURATIONALCOEFFS()
	 * @generated
	 * @ordered
	 */
	protected COMPURATIONALCOEFFS cOMPURATIONALCOEFFS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPUSCALEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPUSCALE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getSHORTLABEL() {
		return sHORTLABEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSHORTLABEL(TEXT newSHORTLABEL, NotificationChain msgs) {
		TEXT oldSHORTLABEL = sHORTLABEL;
		sHORTLABEL = newSHORTLABEL;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__SHORTLABEL, oldSHORTLABEL, newSHORTLABEL);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTLABEL(TEXT newSHORTLABEL) {
		if (newSHORTLABEL != sHORTLABEL) {
			NotificationChain msgs = null;
			if (sHORTLABEL != null)
				msgs = ((InternalEObject)sHORTLABEL).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__SHORTLABEL, null, msgs);
			if (newSHORTLABEL != null)
				msgs = ((InternalEObject)newSHORTLABEL).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__SHORTLABEL, null, msgs);
			msgs = basicSetSHORTLABEL(newSHORTLABEL, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__SHORTLABEL, newSHORTLABEL, newSHORTLABEL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LIMIT getLOWERLIMIT() {
		return lOWERLIMIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLOWERLIMIT(LIMIT newLOWERLIMIT, NotificationChain msgs) {
		LIMIT oldLOWERLIMIT = lOWERLIMIT;
		lOWERLIMIT = newLOWERLIMIT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__LOWERLIMIT, oldLOWERLIMIT, newLOWERLIMIT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLOWERLIMIT(LIMIT newLOWERLIMIT) {
		if (newLOWERLIMIT != lOWERLIMIT) {
			NotificationChain msgs = null;
			if (lOWERLIMIT != null)
				msgs = ((InternalEObject)lOWERLIMIT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__LOWERLIMIT, null, msgs);
			if (newLOWERLIMIT != null)
				msgs = ((InternalEObject)newLOWERLIMIT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__LOWERLIMIT, null, msgs);
			msgs = basicSetLOWERLIMIT(newLOWERLIMIT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__LOWERLIMIT, newLOWERLIMIT, newLOWERLIMIT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LIMIT getUPPERLIMIT() {
		return uPPERLIMIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUPPERLIMIT(LIMIT newUPPERLIMIT, NotificationChain msgs) {
		LIMIT oldUPPERLIMIT = uPPERLIMIT;
		uPPERLIMIT = newUPPERLIMIT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__UPPERLIMIT, oldUPPERLIMIT, newUPPERLIMIT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUPPERLIMIT(LIMIT newUPPERLIMIT) {
		if (newUPPERLIMIT != uPPERLIMIT) {
			NotificationChain msgs = null;
			if (uPPERLIMIT != null)
				msgs = ((InternalEObject)uPPERLIMIT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__UPPERLIMIT, null, msgs);
			if (newUPPERLIMIT != null)
				msgs = ((InternalEObject)newUPPERLIMIT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__UPPERLIMIT, null, msgs);
			msgs = basicSetUPPERLIMIT(newUPPERLIMIT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__UPPERLIMIT, newUPPERLIMIT, newUPPERLIMIT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUINVERSEVALUE getCOMPUINVERSEVALUE() {
		return cOMPUINVERSEVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUINVERSEVALUE(COMPUINVERSEVALUE newCOMPUINVERSEVALUE, NotificationChain msgs) {
		COMPUINVERSEVALUE oldCOMPUINVERSEVALUE = cOMPUINVERSEVALUE;
		cOMPUINVERSEVALUE = newCOMPUINVERSEVALUE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__COMPUINVERSEVALUE, oldCOMPUINVERSEVALUE, newCOMPUINVERSEVALUE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUINVERSEVALUE(COMPUINVERSEVALUE newCOMPUINVERSEVALUE) {
		if (newCOMPUINVERSEVALUE != cOMPUINVERSEVALUE) {
			NotificationChain msgs = null;
			if (cOMPUINVERSEVALUE != null)
				msgs = ((InternalEObject)cOMPUINVERSEVALUE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__COMPUINVERSEVALUE, null, msgs);
			if (newCOMPUINVERSEVALUE != null)
				msgs = ((InternalEObject)newCOMPUINVERSEVALUE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__COMPUINVERSEVALUE, null, msgs);
			msgs = basicSetCOMPUINVERSEVALUE(newCOMPUINVERSEVALUE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__COMPUINVERSEVALUE, newCOMPUINVERSEVALUE, newCOMPUINVERSEVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUCONST getCOMPUCONST() {
		return cOMPUCONST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUCONST(COMPUCONST newCOMPUCONST, NotificationChain msgs) {
		COMPUCONST oldCOMPUCONST = cOMPUCONST;
		cOMPUCONST = newCOMPUCONST;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__COMPUCONST, oldCOMPUCONST, newCOMPUCONST);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUCONST(COMPUCONST newCOMPUCONST) {
		if (newCOMPUCONST != cOMPUCONST) {
			NotificationChain msgs = null;
			if (cOMPUCONST != null)
				msgs = ((InternalEObject)cOMPUCONST).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__COMPUCONST, null, msgs);
			if (newCOMPUCONST != null)
				msgs = ((InternalEObject)newCOMPUCONST).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__COMPUCONST, null, msgs);
			msgs = basicSetCOMPUCONST(newCOMPUCONST, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__COMPUCONST, newCOMPUCONST, newCOMPUCONST));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPURATIONALCOEFFS getCOMPURATIONALCOEFFS() {
		return cOMPURATIONALCOEFFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPURATIONALCOEFFS(COMPURATIONALCOEFFS newCOMPURATIONALCOEFFS, NotificationChain msgs) {
		COMPURATIONALCOEFFS oldCOMPURATIONALCOEFFS = cOMPURATIONALCOEFFS;
		cOMPURATIONALCOEFFS = newCOMPURATIONALCOEFFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__COMPURATIONALCOEFFS, oldCOMPURATIONALCOEFFS, newCOMPURATIONALCOEFFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPURATIONALCOEFFS(COMPURATIONALCOEFFS newCOMPURATIONALCOEFFS) {
		if (newCOMPURATIONALCOEFFS != cOMPURATIONALCOEFFS) {
			NotificationChain msgs = null;
			if (cOMPURATIONALCOEFFS != null)
				msgs = ((InternalEObject)cOMPURATIONALCOEFFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__COMPURATIONALCOEFFS, null, msgs);
			if (newCOMPURATIONALCOEFFS != null)
				msgs = ((InternalEObject)newCOMPURATIONALCOEFFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUSCALE__COMPURATIONALCOEFFS, null, msgs);
			msgs = basicSetCOMPURATIONALCOEFFS(newCOMPURATIONALCOEFFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUSCALE__COMPURATIONALCOEFFS, newCOMPURATIONALCOEFFS, newCOMPURATIONALCOEFFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALE__SHORTLABEL:
				return basicSetSHORTLABEL(null, msgs);
			case OdxXhtmlPackage.COMPUSCALE__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.COMPUSCALE__LOWERLIMIT:
				return basicSetLOWERLIMIT(null, msgs);
			case OdxXhtmlPackage.COMPUSCALE__UPPERLIMIT:
				return basicSetUPPERLIMIT(null, msgs);
			case OdxXhtmlPackage.COMPUSCALE__COMPUINVERSEVALUE:
				return basicSetCOMPUINVERSEVALUE(null, msgs);
			case OdxXhtmlPackage.COMPUSCALE__COMPUCONST:
				return basicSetCOMPUCONST(null, msgs);
			case OdxXhtmlPackage.COMPUSCALE__COMPURATIONALCOEFFS:
				return basicSetCOMPURATIONALCOEFFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALE__SHORTLABEL:
				return getSHORTLABEL();
			case OdxXhtmlPackage.COMPUSCALE__DESC:
				return getDESC();
			case OdxXhtmlPackage.COMPUSCALE__LOWERLIMIT:
				return getLOWERLIMIT();
			case OdxXhtmlPackage.COMPUSCALE__UPPERLIMIT:
				return getUPPERLIMIT();
			case OdxXhtmlPackage.COMPUSCALE__COMPUINVERSEVALUE:
				return getCOMPUINVERSEVALUE();
			case OdxXhtmlPackage.COMPUSCALE__COMPUCONST:
				return getCOMPUCONST();
			case OdxXhtmlPackage.COMPUSCALE__COMPURATIONALCOEFFS:
				return getCOMPURATIONALCOEFFS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALE__SHORTLABEL:
				setSHORTLABEL((TEXT)newValue);
				return;
			case OdxXhtmlPackage.COMPUSCALE__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.COMPUSCALE__LOWERLIMIT:
				setLOWERLIMIT((LIMIT)newValue);
				return;
			case OdxXhtmlPackage.COMPUSCALE__UPPERLIMIT:
				setUPPERLIMIT((LIMIT)newValue);
				return;
			case OdxXhtmlPackage.COMPUSCALE__COMPUINVERSEVALUE:
				setCOMPUINVERSEVALUE((COMPUINVERSEVALUE)newValue);
				return;
			case OdxXhtmlPackage.COMPUSCALE__COMPUCONST:
				setCOMPUCONST((COMPUCONST)newValue);
				return;
			case OdxXhtmlPackage.COMPUSCALE__COMPURATIONALCOEFFS:
				setCOMPURATIONALCOEFFS((COMPURATIONALCOEFFS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALE__SHORTLABEL:
				setSHORTLABEL((TEXT)null);
				return;
			case OdxXhtmlPackage.COMPUSCALE__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.COMPUSCALE__LOWERLIMIT:
				setLOWERLIMIT((LIMIT)null);
				return;
			case OdxXhtmlPackage.COMPUSCALE__UPPERLIMIT:
				setUPPERLIMIT((LIMIT)null);
				return;
			case OdxXhtmlPackage.COMPUSCALE__COMPUINVERSEVALUE:
				setCOMPUINVERSEVALUE((COMPUINVERSEVALUE)null);
				return;
			case OdxXhtmlPackage.COMPUSCALE__COMPUCONST:
				setCOMPUCONST((COMPUCONST)null);
				return;
			case OdxXhtmlPackage.COMPUSCALE__COMPURATIONALCOEFFS:
				setCOMPURATIONALCOEFFS((COMPURATIONALCOEFFS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALE__SHORTLABEL:
				return sHORTLABEL != null;
			case OdxXhtmlPackage.COMPUSCALE__DESC:
				return dESC != null;
			case OdxXhtmlPackage.COMPUSCALE__LOWERLIMIT:
				return lOWERLIMIT != null;
			case OdxXhtmlPackage.COMPUSCALE__UPPERLIMIT:
				return uPPERLIMIT != null;
			case OdxXhtmlPackage.COMPUSCALE__COMPUINVERSEVALUE:
				return cOMPUINVERSEVALUE != null;
			case OdxXhtmlPackage.COMPUSCALE__COMPUCONST:
				return cOMPUCONST != null;
			case OdxXhtmlPackage.COMPUSCALE__COMPURATIONALCOEFFS:
				return cOMPURATIONALCOEFFS != null;
		}
		return super.eIsSet(featureID);
	}

} //COMPUSCALEImpl
