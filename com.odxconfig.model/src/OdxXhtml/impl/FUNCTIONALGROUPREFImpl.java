/**
 */
package OdxXhtml.impl;

import OdxXhtml.FUNCTIONALGROUPREF;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FUNCTIONALGROUPREF</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FUNCTIONALGROUPREFImpl extends PARENTREFImpl implements FUNCTIONALGROUPREF {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FUNCTIONALGROUPREFImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFUNCTIONALGROUPREF();
	}

} //FUNCTIONALGROUPREFImpl
