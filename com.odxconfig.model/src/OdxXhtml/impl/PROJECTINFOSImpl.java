/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROJECTINFO;
import OdxXhtml.PROJECTINFOS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PROJECTINFOS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PROJECTINFOSImpl#getPROJECTINFO <em>PROJECTINFO</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PROJECTINFOSImpl extends MinimalEObjectImpl.Container implements PROJECTINFOS {
	/**
	 * The cached value of the '{@link #getPROJECTINFO() <em>PROJECTINFO</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROJECTINFO()
	 * @generated
	 * @ordered
	 */
	protected EList<PROJECTINFO> pROJECTINFO;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PROJECTINFOSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPROJECTINFOS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<PROJECTINFO> getPROJECTINFO() {
		if (pROJECTINFO == null) {
			pROJECTINFO = new EObjectContainmentEList<PROJECTINFO>(PROJECTINFO.class, this, OdxXhtmlPackage.PROJECTINFOS__PROJECTINFO);
		}
		return pROJECTINFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFOS__PROJECTINFO:
				return ((InternalEList<?>)getPROJECTINFO()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFOS__PROJECTINFO:
				return getPROJECTINFO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFOS__PROJECTINFO:
				getPROJECTINFO().clear();
				getPROJECTINFO().addAll((Collection<? extends PROJECTINFO>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFOS__PROJECTINFO:
				getPROJECTINFO().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTINFOS__PROJECTINFO:
				return pROJECTINFO != null && !pROJECTINFO.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PROJECTINFOSImpl
