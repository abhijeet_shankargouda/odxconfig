/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATATYPE;
import OdxXhtml.DIAGCODEDTYPE;
import OdxXhtml.ENCODING;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGCODEDTYPE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGCODEDTYPEImpl#getBASEDATATYPE <em>BASEDATATYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCODEDTYPEImpl#getBASETYPEENCODING <em>BASETYPEENCODING</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCODEDTYPEImpl#isISHIGHLOWBYTEORDER <em>ISHIGHLOWBYTEORDER</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGCODEDTYPEImpl extends MinimalEObjectImpl.Container implements DIAGCODEDTYPE {
	/**
	 * The default value of the '{@link #getBASEDATATYPE() <em>BASEDATATYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASEDATATYPE()
	 * @generated
	 * @ordered
	 */
	protected static final DATATYPE BASEDATATYPE_EDEFAULT = DATATYPE.AINT32;

	/**
	 * The cached value of the '{@link #getBASEDATATYPE() <em>BASEDATATYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASEDATATYPE()
	 * @generated
	 * @ordered
	 */
	protected DATATYPE bASEDATATYPE = BASEDATATYPE_EDEFAULT;

	/**
	 * This is true if the BASEDATATYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bASEDATATYPEESet;

	/**
	 * The default value of the '{@link #getBASETYPEENCODING() <em>BASETYPEENCODING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASETYPEENCODING()
	 * @generated
	 * @ordered
	 */
	protected static final ENCODING BASETYPEENCODING_EDEFAULT = ENCODING.BCDP;

	/**
	 * The cached value of the '{@link #getBASETYPEENCODING() <em>BASETYPEENCODING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASETYPEENCODING()
	 * @generated
	 * @ordered
	 */
	protected ENCODING bASETYPEENCODING = BASETYPEENCODING_EDEFAULT;

	/**
	 * This is true if the BASETYPEENCODING attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bASETYPEENCODINGESet;

	/**
	 * The default value of the '{@link #isISHIGHLOWBYTEORDER() <em>ISHIGHLOWBYTEORDER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISHIGHLOWBYTEORDER()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISHIGHLOWBYTEORDER_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isISHIGHLOWBYTEORDER() <em>ISHIGHLOWBYTEORDER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISHIGHLOWBYTEORDER()
	 * @generated
	 * @ordered
	 */
	protected boolean iSHIGHLOWBYTEORDER = ISHIGHLOWBYTEORDER_EDEFAULT;

	/**
	 * This is true if the ISHIGHLOWBYTEORDER attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSHIGHLOWBYTEORDERESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGCODEDTYPEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGCODEDTYPE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATATYPE getBASEDATATYPE() {
		return bASEDATATYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBASEDATATYPE(DATATYPE newBASEDATATYPE) {
		DATATYPE oldBASEDATATYPE = bASEDATATYPE;
		bASEDATATYPE = newBASEDATATYPE == null ? BASEDATATYPE_EDEFAULT : newBASEDATATYPE;
		boolean oldBASEDATATYPEESet = bASEDATATYPEESet;
		bASEDATATYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCODEDTYPE__BASEDATATYPE, oldBASEDATATYPE, bASEDATATYPE, !oldBASEDATATYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBASEDATATYPE() {
		DATATYPE oldBASEDATATYPE = bASEDATATYPE;
		boolean oldBASEDATATYPEESet = bASEDATATYPEESet;
		bASEDATATYPE = BASEDATATYPE_EDEFAULT;
		bASEDATATYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGCODEDTYPE__BASEDATATYPE, oldBASEDATATYPE, BASEDATATYPE_EDEFAULT, oldBASEDATATYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBASEDATATYPE() {
		return bASEDATATYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ENCODING getBASETYPEENCODING() {
		return bASETYPEENCODING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBASETYPEENCODING(ENCODING newBASETYPEENCODING) {
		ENCODING oldBASETYPEENCODING = bASETYPEENCODING;
		bASETYPEENCODING = newBASETYPEENCODING == null ? BASETYPEENCODING_EDEFAULT : newBASETYPEENCODING;
		boolean oldBASETYPEENCODINGESet = bASETYPEENCODINGESet;
		bASETYPEENCODINGESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCODEDTYPE__BASETYPEENCODING, oldBASETYPEENCODING, bASETYPEENCODING, !oldBASETYPEENCODINGESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBASETYPEENCODING() {
		ENCODING oldBASETYPEENCODING = bASETYPEENCODING;
		boolean oldBASETYPEENCODINGESet = bASETYPEENCODINGESet;
		bASETYPEENCODING = BASETYPEENCODING_EDEFAULT;
		bASETYPEENCODINGESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGCODEDTYPE__BASETYPEENCODING, oldBASETYPEENCODING, BASETYPEENCODING_EDEFAULT, oldBASETYPEENCODINGESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBASETYPEENCODING() {
		return bASETYPEENCODINGESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISHIGHLOWBYTEORDER() {
		return iSHIGHLOWBYTEORDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISHIGHLOWBYTEORDER(boolean newISHIGHLOWBYTEORDER) {
		boolean oldISHIGHLOWBYTEORDER = iSHIGHLOWBYTEORDER;
		iSHIGHLOWBYTEORDER = newISHIGHLOWBYTEORDER;
		boolean oldISHIGHLOWBYTEORDERESet = iSHIGHLOWBYTEORDERESet;
		iSHIGHLOWBYTEORDERESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCODEDTYPE__ISHIGHLOWBYTEORDER, oldISHIGHLOWBYTEORDER, iSHIGHLOWBYTEORDER, !oldISHIGHLOWBYTEORDERESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISHIGHLOWBYTEORDER() {
		boolean oldISHIGHLOWBYTEORDER = iSHIGHLOWBYTEORDER;
		boolean oldISHIGHLOWBYTEORDERESet = iSHIGHLOWBYTEORDERESet;
		iSHIGHLOWBYTEORDER = ISHIGHLOWBYTEORDER_EDEFAULT;
		iSHIGHLOWBYTEORDERESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGCODEDTYPE__ISHIGHLOWBYTEORDER, oldISHIGHLOWBYTEORDER, ISHIGHLOWBYTEORDER_EDEFAULT, oldISHIGHLOWBYTEORDERESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISHIGHLOWBYTEORDER() {
		return iSHIGHLOWBYTEORDERESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCODEDTYPE__BASEDATATYPE:
				return getBASEDATATYPE();
			case OdxXhtmlPackage.DIAGCODEDTYPE__BASETYPEENCODING:
				return getBASETYPEENCODING();
			case OdxXhtmlPackage.DIAGCODEDTYPE__ISHIGHLOWBYTEORDER:
				return isISHIGHLOWBYTEORDER();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCODEDTYPE__BASEDATATYPE:
				setBASEDATATYPE((DATATYPE)newValue);
				return;
			case OdxXhtmlPackage.DIAGCODEDTYPE__BASETYPEENCODING:
				setBASETYPEENCODING((ENCODING)newValue);
				return;
			case OdxXhtmlPackage.DIAGCODEDTYPE__ISHIGHLOWBYTEORDER:
				setISHIGHLOWBYTEORDER((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCODEDTYPE__BASEDATATYPE:
				unsetBASEDATATYPE();
				return;
			case OdxXhtmlPackage.DIAGCODEDTYPE__BASETYPEENCODING:
				unsetBASETYPEENCODING();
				return;
			case OdxXhtmlPackage.DIAGCODEDTYPE__ISHIGHLOWBYTEORDER:
				unsetISHIGHLOWBYTEORDER();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCODEDTYPE__BASEDATATYPE:
				return isSetBASEDATATYPE();
			case OdxXhtmlPackage.DIAGCODEDTYPE__BASETYPEENCODING:
				return isSetBASETYPEENCODING();
			case OdxXhtmlPackage.DIAGCODEDTYPE__ISHIGHLOWBYTEORDER:
				return isSetISHIGHLOWBYTEORDER();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bASEDATATYPE: ");
		if (bASEDATATYPEESet) result.append(bASEDATATYPE); else result.append("<unset>");
		result.append(", bASETYPEENCODING: ");
		if (bASETYPEENCODINGESet) result.append(bASETYPEENCODING); else result.append("<unset>");
		result.append(", iSHIGHLOWBYTEORDER: ");
		if (iSHIGHLOWBYTEORDERESet) result.append(iSHIGHLOWBYTEORDER); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //DIAGCODEDTYPEImpl
