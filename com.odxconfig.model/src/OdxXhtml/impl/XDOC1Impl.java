/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION1;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT1;
import OdxXhtml.XDOC1;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XDOC1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.XDOC1Impl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.XDOC1Impl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.XDOC1Impl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.XDOC1Impl#getNUMBER <em>NUMBER</em>}</li>
 *   <li>{@link OdxXhtml.impl.XDOC1Impl#getSTATE <em>STATE</em>}</li>
 *   <li>{@link OdxXhtml.impl.XDOC1Impl#getDATE <em>DATE</em>}</li>
 *   <li>{@link OdxXhtml.impl.XDOC1Impl#getPUBLISHER <em>PUBLISHER</em>}</li>
 *   <li>{@link OdxXhtml.impl.XDOC1Impl#getURL <em>URL</em>}</li>
 *   <li>{@link OdxXhtml.impl.XDOC1Impl#getPOSITION <em>POSITION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class XDOC1Impl extends MinimalEObjectImpl.Container implements XDOC1 {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT1 lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION1 dESC;

	/**
	 * The default value of the '{@link #getNUMBER() <em>NUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNUMBER()
	 * @generated
	 * @ordered
	 */
	protected static final String NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNUMBER() <em>NUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNUMBER()
	 * @generated
	 * @ordered
	 */
	protected String nUMBER = NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getSTATE() <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTATE()
	 * @generated
	 * @ordered
	 */
	protected static final String STATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSTATE() <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTATE()
	 * @generated
	 * @ordered
	 */
	protected String sTATE = STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDATE() <em>DATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATE()
	 * @generated
	 * @ordered
	 */
	protected static final XMLGregorianCalendar DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDATE() <em>DATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATE()
	 * @generated
	 * @ordered
	 */
	protected XMLGregorianCalendar dATE = DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPUBLISHER() <em>PUBLISHER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPUBLISHER()
	 * @generated
	 * @ordered
	 */
	protected static final String PUBLISHER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPUBLISHER() <em>PUBLISHER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPUBLISHER()
	 * @generated
	 * @ordered
	 */
	protected String pUBLISHER = PUBLISHER_EDEFAULT;

	/**
	 * The default value of the '{@link #getURL() <em>URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURL()
	 * @generated
	 * @ordered
	 */
	protected static final String URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getURL() <em>URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURL()
	 * @generated
	 * @ordered
	 */
	protected String uRL = URL_EDEFAULT;

	/**
	 * The default value of the '{@link #getPOSITION() <em>POSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPOSITION()
	 * @generated
	 * @ordered
	 */
	protected static final String POSITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPOSITION() <em>POSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPOSITION()
	 * @generated
	 * @ordered
	 */
	protected String pOSITION = POSITION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XDOC1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getXDOC1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT1 getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT1 newLONGNAME, NotificationChain msgs) {
		TEXT1 oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT1 newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.XDOC1__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.XDOC1__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION1 getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION1 newDESC, NotificationChain msgs) {
		DESCRIPTION1 oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION1 newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.XDOC1__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.XDOC1__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNUMBER() {
		return nUMBER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNUMBER(String newNUMBER) {
		String oldNUMBER = nUMBER;
		nUMBER = newNUMBER;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__NUMBER, oldNUMBER, nUMBER));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSTATE() {
		return sTATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSTATE(String newSTATE) {
		String oldSTATE = sTATE;
		sTATE = newSTATE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__STATE, oldSTATE, sTATE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public XMLGregorianCalendar getDATE() {
		return dATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATE(XMLGregorianCalendar newDATE) {
		XMLGregorianCalendar oldDATE = dATE;
		dATE = newDATE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__DATE, oldDATE, dATE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPUBLISHER() {
		return pUBLISHER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPUBLISHER(String newPUBLISHER) {
		String oldPUBLISHER = pUBLISHER;
		pUBLISHER = newPUBLISHER;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__PUBLISHER, oldPUBLISHER, pUBLISHER));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getURL() {
		return uRL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setURL(String newURL) {
		String oldURL = uRL;
		uRL = newURL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__URL, oldURL, uRL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPOSITION() {
		return pOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPOSITION(String newPOSITION) {
		String oldPOSITION = pOSITION;
		pOSITION = newPOSITION;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.XDOC1__POSITION, oldPOSITION, pOSITION));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.XDOC1__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.XDOC1__DESC:
				return basicSetDESC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.XDOC1__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.XDOC1__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.XDOC1__DESC:
				return getDESC();
			case OdxXhtmlPackage.XDOC1__NUMBER:
				return getNUMBER();
			case OdxXhtmlPackage.XDOC1__STATE:
				return getSTATE();
			case OdxXhtmlPackage.XDOC1__DATE:
				return getDATE();
			case OdxXhtmlPackage.XDOC1__PUBLISHER:
				return getPUBLISHER();
			case OdxXhtmlPackage.XDOC1__URL:
				return getURL();
			case OdxXhtmlPackage.XDOC1__POSITION:
				return getPOSITION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.XDOC1__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.XDOC1__LONGNAME:
				setLONGNAME((TEXT1)newValue);
				return;
			case OdxXhtmlPackage.XDOC1__DESC:
				setDESC((DESCRIPTION1)newValue);
				return;
			case OdxXhtmlPackage.XDOC1__NUMBER:
				setNUMBER((String)newValue);
				return;
			case OdxXhtmlPackage.XDOC1__STATE:
				setSTATE((String)newValue);
				return;
			case OdxXhtmlPackage.XDOC1__DATE:
				setDATE((XMLGregorianCalendar)newValue);
				return;
			case OdxXhtmlPackage.XDOC1__PUBLISHER:
				setPUBLISHER((String)newValue);
				return;
			case OdxXhtmlPackage.XDOC1__URL:
				setURL((String)newValue);
				return;
			case OdxXhtmlPackage.XDOC1__POSITION:
				setPOSITION((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.XDOC1__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.XDOC1__LONGNAME:
				setLONGNAME((TEXT1)null);
				return;
			case OdxXhtmlPackage.XDOC1__DESC:
				setDESC((DESCRIPTION1)null);
				return;
			case OdxXhtmlPackage.XDOC1__NUMBER:
				setNUMBER(NUMBER_EDEFAULT);
				return;
			case OdxXhtmlPackage.XDOC1__STATE:
				setSTATE(STATE_EDEFAULT);
				return;
			case OdxXhtmlPackage.XDOC1__DATE:
				setDATE(DATE_EDEFAULT);
				return;
			case OdxXhtmlPackage.XDOC1__PUBLISHER:
				setPUBLISHER(PUBLISHER_EDEFAULT);
				return;
			case OdxXhtmlPackage.XDOC1__URL:
				setURL(URL_EDEFAULT);
				return;
			case OdxXhtmlPackage.XDOC1__POSITION:
				setPOSITION(POSITION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.XDOC1__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.XDOC1__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.XDOC1__DESC:
				return dESC != null;
			case OdxXhtmlPackage.XDOC1__NUMBER:
				return NUMBER_EDEFAULT == null ? nUMBER != null : !NUMBER_EDEFAULT.equals(nUMBER);
			case OdxXhtmlPackage.XDOC1__STATE:
				return STATE_EDEFAULT == null ? sTATE != null : !STATE_EDEFAULT.equals(sTATE);
			case OdxXhtmlPackage.XDOC1__DATE:
				return DATE_EDEFAULT == null ? dATE != null : !DATE_EDEFAULT.equals(dATE);
			case OdxXhtmlPackage.XDOC1__PUBLISHER:
				return PUBLISHER_EDEFAULT == null ? pUBLISHER != null : !PUBLISHER_EDEFAULT.equals(pUBLISHER);
			case OdxXhtmlPackage.XDOC1__URL:
				return URL_EDEFAULT == null ? uRL != null : !URL_EDEFAULT.equals(uRL);
			case OdxXhtmlPackage.XDOC1__POSITION:
				return POSITION_EDEFAULT == null ? pOSITION != null : !POSITION_EDEFAULT.equals(pOSITION);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", nUMBER: ");
		result.append(nUMBER);
		result.append(", sTATE: ");
		result.append(sTATE);
		result.append(", dATE: ");
		result.append(dATE);
		result.append(", pUBLISHER: ");
		result.append(pUBLISHER);
		result.append(", uRL: ");
		result.append(uRL);
		result.append(", pOSITION: ");
		result.append(pOSITION);
		result.append(')');
		return result.toString();
	}

} //XDOC1Impl
