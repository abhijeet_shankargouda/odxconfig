/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPUSCALE;
import OdxXhtml.COMPUSCALES;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPUSCALES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPUSCALESImpl#getCOMPUSCALE <em>COMPUSCALE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPUSCALESImpl extends MinimalEObjectImpl.Container implements COMPUSCALES {
	/**
	 * The cached value of the '{@link #getCOMPUSCALE() <em>COMPUSCALE</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUSCALE()
	 * @generated
	 * @ordered
	 */
	protected EList<COMPUSCALE> cOMPUSCALE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPUSCALESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPUSCALES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<COMPUSCALE> getCOMPUSCALE() {
		if (cOMPUSCALE == null) {
			cOMPUSCALE = new EObjectContainmentEList<COMPUSCALE>(COMPUSCALE.class, this, OdxXhtmlPackage.COMPUSCALES__COMPUSCALE);
		}
		return cOMPUSCALE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALES__COMPUSCALE:
				return ((InternalEList<?>)getCOMPUSCALE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALES__COMPUSCALE:
				return getCOMPUSCALE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALES__COMPUSCALE:
				getCOMPUSCALE().clear();
				getCOMPUSCALE().addAll((Collection<? extends COMPUSCALE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALES__COMPUSCALE:
				getCOMPUSCALE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUSCALES__COMPUSCALE:
				return cOMPUSCALE != null && !cOMPUSCALE.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //COMPUSCALESImpl
