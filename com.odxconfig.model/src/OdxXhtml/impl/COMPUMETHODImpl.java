/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPUCATEGORY;
import OdxXhtml.COMPUINTERNALTOPHYS;
import OdxXhtml.COMPUMETHOD;
import OdxXhtml.COMPUPHYSTOINTERNAL;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPUMETHOD</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPUMETHODImpl#getCATEGORY <em>CATEGORY</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUMETHODImpl#getCOMPUINTERNALTOPHYS <em>COMPUINTERNALTOPHYS</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUMETHODImpl#getCOMPUPHYSTOINTERNAL <em>COMPUPHYSTOINTERNAL</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPUMETHODImpl extends MinimalEObjectImpl.Container implements COMPUMETHOD {
	/**
	 * The default value of the '{@link #getCATEGORY() <em>CATEGORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCATEGORY()
	 * @generated
	 * @ordered
	 */
	protected static final COMPUCATEGORY CATEGORY_EDEFAULT = COMPUCATEGORY.IDENTICAL;

	/**
	 * The cached value of the '{@link #getCATEGORY() <em>CATEGORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCATEGORY()
	 * @generated
	 * @ordered
	 */
	protected COMPUCATEGORY cATEGORY = CATEGORY_EDEFAULT;

	/**
	 * This is true if the CATEGORY attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean cATEGORYESet;

	/**
	 * The cached value of the '{@link #getCOMPUINTERNALTOPHYS() <em>COMPUINTERNALTOPHYS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUINTERNALTOPHYS()
	 * @generated
	 * @ordered
	 */
	protected COMPUINTERNALTOPHYS cOMPUINTERNALTOPHYS;

	/**
	 * The cached value of the '{@link #getCOMPUPHYSTOINTERNAL() <em>COMPUPHYSTOINTERNAL</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUPHYSTOINTERNAL()
	 * @generated
	 * @ordered
	 */
	protected COMPUPHYSTOINTERNAL cOMPUPHYSTOINTERNAL;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPUMETHODImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPUMETHOD();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUCATEGORY getCATEGORY() {
		return cATEGORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCATEGORY(COMPUCATEGORY newCATEGORY) {
		COMPUCATEGORY oldCATEGORY = cATEGORY;
		cATEGORY = newCATEGORY == null ? CATEGORY_EDEFAULT : newCATEGORY;
		boolean oldCATEGORYESet = cATEGORYESet;
		cATEGORYESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUMETHOD__CATEGORY, oldCATEGORY, cATEGORY, !oldCATEGORYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetCATEGORY() {
		COMPUCATEGORY oldCATEGORY = cATEGORY;
		boolean oldCATEGORYESet = cATEGORYESet;
		cATEGORY = CATEGORY_EDEFAULT;
		cATEGORYESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.COMPUMETHOD__CATEGORY, oldCATEGORY, CATEGORY_EDEFAULT, oldCATEGORYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetCATEGORY() {
		return cATEGORYESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUINTERNALTOPHYS getCOMPUINTERNALTOPHYS() {
		return cOMPUINTERNALTOPHYS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUINTERNALTOPHYS(COMPUINTERNALTOPHYS newCOMPUINTERNALTOPHYS, NotificationChain msgs) {
		COMPUINTERNALTOPHYS oldCOMPUINTERNALTOPHYS = cOMPUINTERNALTOPHYS;
		cOMPUINTERNALTOPHYS = newCOMPUINTERNALTOPHYS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUMETHOD__COMPUINTERNALTOPHYS, oldCOMPUINTERNALTOPHYS, newCOMPUINTERNALTOPHYS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUINTERNALTOPHYS(COMPUINTERNALTOPHYS newCOMPUINTERNALTOPHYS) {
		if (newCOMPUINTERNALTOPHYS != cOMPUINTERNALTOPHYS) {
			NotificationChain msgs = null;
			if (cOMPUINTERNALTOPHYS != null)
				msgs = ((InternalEObject)cOMPUINTERNALTOPHYS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUMETHOD__COMPUINTERNALTOPHYS, null, msgs);
			if (newCOMPUINTERNALTOPHYS != null)
				msgs = ((InternalEObject)newCOMPUINTERNALTOPHYS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUMETHOD__COMPUINTERNALTOPHYS, null, msgs);
			msgs = basicSetCOMPUINTERNALTOPHYS(newCOMPUINTERNALTOPHYS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUMETHOD__COMPUINTERNALTOPHYS, newCOMPUINTERNALTOPHYS, newCOMPUINTERNALTOPHYS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUPHYSTOINTERNAL getCOMPUPHYSTOINTERNAL() {
		return cOMPUPHYSTOINTERNAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUPHYSTOINTERNAL(COMPUPHYSTOINTERNAL newCOMPUPHYSTOINTERNAL, NotificationChain msgs) {
		COMPUPHYSTOINTERNAL oldCOMPUPHYSTOINTERNAL = cOMPUPHYSTOINTERNAL;
		cOMPUPHYSTOINTERNAL = newCOMPUPHYSTOINTERNAL;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUMETHOD__COMPUPHYSTOINTERNAL, oldCOMPUPHYSTOINTERNAL, newCOMPUPHYSTOINTERNAL);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUPHYSTOINTERNAL(COMPUPHYSTOINTERNAL newCOMPUPHYSTOINTERNAL) {
		if (newCOMPUPHYSTOINTERNAL != cOMPUPHYSTOINTERNAL) {
			NotificationChain msgs = null;
			if (cOMPUPHYSTOINTERNAL != null)
				msgs = ((InternalEObject)cOMPUPHYSTOINTERNAL).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUMETHOD__COMPUPHYSTOINTERNAL, null, msgs);
			if (newCOMPUPHYSTOINTERNAL != null)
				msgs = ((InternalEObject)newCOMPUPHYSTOINTERNAL).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUMETHOD__COMPUPHYSTOINTERNAL, null, msgs);
			msgs = basicSetCOMPUPHYSTOINTERNAL(newCOMPUPHYSTOINTERNAL, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUMETHOD__COMPUPHYSTOINTERNAL, newCOMPUPHYSTOINTERNAL, newCOMPUPHYSTOINTERNAL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUMETHOD__COMPUINTERNALTOPHYS:
				return basicSetCOMPUINTERNALTOPHYS(null, msgs);
			case OdxXhtmlPackage.COMPUMETHOD__COMPUPHYSTOINTERNAL:
				return basicSetCOMPUPHYSTOINTERNAL(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUMETHOD__CATEGORY:
				return getCATEGORY();
			case OdxXhtmlPackage.COMPUMETHOD__COMPUINTERNALTOPHYS:
				return getCOMPUINTERNALTOPHYS();
			case OdxXhtmlPackage.COMPUMETHOD__COMPUPHYSTOINTERNAL:
				return getCOMPUPHYSTOINTERNAL();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUMETHOD__CATEGORY:
				setCATEGORY((COMPUCATEGORY)newValue);
				return;
			case OdxXhtmlPackage.COMPUMETHOD__COMPUINTERNALTOPHYS:
				setCOMPUINTERNALTOPHYS((COMPUINTERNALTOPHYS)newValue);
				return;
			case OdxXhtmlPackage.COMPUMETHOD__COMPUPHYSTOINTERNAL:
				setCOMPUPHYSTOINTERNAL((COMPUPHYSTOINTERNAL)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUMETHOD__CATEGORY:
				unsetCATEGORY();
				return;
			case OdxXhtmlPackage.COMPUMETHOD__COMPUINTERNALTOPHYS:
				setCOMPUINTERNALTOPHYS((COMPUINTERNALTOPHYS)null);
				return;
			case OdxXhtmlPackage.COMPUMETHOD__COMPUPHYSTOINTERNAL:
				setCOMPUPHYSTOINTERNAL((COMPUPHYSTOINTERNAL)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUMETHOD__CATEGORY:
				return isSetCATEGORY();
			case OdxXhtmlPackage.COMPUMETHOD__COMPUINTERNALTOPHYS:
				return cOMPUINTERNALTOPHYS != null;
			case OdxXhtmlPackage.COMPUMETHOD__COMPUPHYSTOINTERNAL:
				return cOMPUPHYSTOINTERNAL != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (cATEGORY: ");
		if (cATEGORYESet) result.append(cATEGORY); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //COMPUMETHODImpl
