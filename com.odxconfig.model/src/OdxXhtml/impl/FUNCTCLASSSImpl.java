/**
 */
package OdxXhtml.impl;

import OdxXhtml.FUNCTCLASS;
import OdxXhtml.FUNCTCLASSS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FUNCTCLASSS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FUNCTCLASSSImpl#getFUNCTCLASS <em>FUNCTCLASS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FUNCTCLASSSImpl extends MinimalEObjectImpl.Container implements FUNCTCLASSS {
	/**
	 * The cached value of the '{@link #getFUNCTCLASS() <em>FUNCTCLASS</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFUNCTCLASS()
	 * @generated
	 * @ordered
	 */
	protected EList<FUNCTCLASS> fUNCTCLASS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FUNCTCLASSSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFUNCTCLASSS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FUNCTCLASS> getFUNCTCLASS() {
		if (fUNCTCLASS == null) {
			fUNCTCLASS = new EObjectContainmentEList<FUNCTCLASS>(FUNCTCLASS.class, this, OdxXhtmlPackage.FUNCTCLASSS__FUNCTCLASS);
		}
		return fUNCTCLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSS__FUNCTCLASS:
				return ((InternalEList<?>)getFUNCTCLASS()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSS__FUNCTCLASS:
				return getFUNCTCLASS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSS__FUNCTCLASS:
				getFUNCTCLASS().clear();
				getFUNCTCLASS().addAll((Collection<? extends FUNCTCLASS>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSS__FUNCTCLASS:
				getFUNCTCLASS().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FUNCTCLASSS__FUNCTCLASS:
				return fUNCTCLASS != null && !fUNCTCLASS.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FUNCTCLASSSImpl
