/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlFactory;
import OdxXhtml.OdxXhtmlPackage;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OdxXhtmlPackageImpl extends EPackageImpl implements OdxXhtmlPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected String packageFilename = "OdxXhtml.ecore";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ablockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ablocksTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accesslevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accesslevelsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addrdeffilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addrdefphyssegmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass admindataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass admindata1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass allvalueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass audienceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass autmethodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass autmethodsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basevariantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basevariantrefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basevariantsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicstructureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass casesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass catalogEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checksumEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checksumresultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checksumsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codedconstEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commrelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commrelationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companydataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companydata1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companydatasEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companydatasTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companydocinfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companydocinfo1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companydocinfosEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companydocinfos1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companyrevisioninfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companyrevisioninfo1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companyrevisioninfosEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companyrevisioninfos1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companyspecificinfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass companyspecificinfo1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass comparamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass comparamrefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass comparamrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass comparamsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass comparamspecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexdopEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compuconstEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compudefaultvalueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compudenominatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compuinternaltophysEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compuinversevalueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compumethodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compunumeratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compuphystointernalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compurationalcoeffsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compuscaleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compuscalesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass datablockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass datablockrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass datablocksEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass datafileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataformatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataobjectpropEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataobjectpropsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass defaultcaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass descriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass description1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass determinenumberofitemsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagcodedtypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagcommEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagcommsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagdatadictionaryspecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diaglayerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diaglayercontainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diaglayerrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagserviceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagvariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagvariablesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass docrevisionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass docrevision1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass docrevisionsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass docrevisions1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dopbaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dtcEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dtcdopEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dtcdopsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dtcsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dtcvalueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dtcvaluesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynamicEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynamicendmarkerfieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynamicendmarkerfieldsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynamiclengthfieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynamiclengthfieldsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dyndefinedspecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dynenddoprefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dyniddefmodeinfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dyniddefmodeinfosEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecugroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecugroupsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecumemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecumemconnectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecumemconnectorsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecumemsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecuproxyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecuproxyrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecushareddataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecushareddatarefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecushareddatasEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecuvariantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecuvariantpatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecuvariantpatternsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ecuvariantsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass encryptcompressmethodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass endofpdufieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass endofpdufieldsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass envdataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass envdatadescEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass envdatadescsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass envdatasEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expectedidentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expectedidentsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalaccessmethodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externflashdataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass filesTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass filterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass filtersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flashEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flashclassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flashclassrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flashclasssEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flashdataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flashdatasEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functclassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functclassrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functclasssEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionalgroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionalgrouprefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionalgroupsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fwchecksumEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fwsignatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gatewaylogicallinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gatewaylogicallinkrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass globalnegresponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass globalnegresponsesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hierarchyelementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identdescEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identdescsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identvalueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass identvaluesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass importrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass infocomponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass infocomponentrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass infocomponentsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inlineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputparamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputparamsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass internalconstrEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass internflashdataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass layerrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leadinglengthinfotypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lengthdescriptorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lengthkeyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass limitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linkcomparamrefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linkcomparamrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass liTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicallinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicallinkrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicallinksEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchingcomponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchingcomponentsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchingparameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchingparametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchingrequestparamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass memEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass memberlogicallinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass minmaxlengthtypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelyearEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modification1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modificationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modifications1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multipleecujobEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multipleecujobsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multipleecujobspecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass muxEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass muxsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negoffsetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negoutputparamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negoutputparamsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negresponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negresponserefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negresponsesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notinheriteddiagcommEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notinheriteddiagcommsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notinheritedvariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notinheritedvariablesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass odxEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass odxcategoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass odxlinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass odxlink1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass oemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass olTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputparamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputparamsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ownidentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ownidentsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paramEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paramlengthinfotypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paramsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parentrefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parentrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physconstEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicaldimensionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicaldimensionsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicaltypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalvehiclelinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physicalvehiclelinksEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physmemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physsegmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass physsegmentsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass positionableparamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass posoffsetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass posresponseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass posresponserefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass posresponsesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass progcodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass progcodesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectidentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectidentsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectinfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectinfosEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass protocolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass protocolrefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass protocolsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass protocolsnrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relateddiagcommrefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relateddiagcommrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relateddocEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relateddoc1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relateddocsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass relateddocs1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requestsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reservedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass responseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rolesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roles1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scaleconstrEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scaleconstrsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sdEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sd1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sdgEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sdg1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sdgcaptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sdgcaption1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sdgsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sdgs1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass securityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass securitymethodEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass securitysEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass segmentsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass selectiontablerefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessiondescEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessiondescsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singleecujobEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sizedeffilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sizedefphyssegmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass snrefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sourceendaddressEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass specialdataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass specialdata1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass standardlengthtypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass staticfieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass staticfieldsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass structuresEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass supporteddynidEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass supporteddynidsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass supTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass switchkeyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass swvariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass swvariablesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableentryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tablekeyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tablerowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tablesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tablestructEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass targetaddroffsetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass teammemberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass teammember1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass teammembersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass teammembers1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass textEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass text1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ulTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uncompressedsizeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unionvalueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitgroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitgroupsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unitspecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass validityforEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variablegroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variablegroupsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicleconnectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicleconnectorpinEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicleconnectorpinrefsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicleconnectorpinsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicleconnectorsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicleinformationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicleinformationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicleinfospecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehiclemodelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vehicletypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vtEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xdocEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xdoc1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum addressingEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum alignTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum commrelationvaluetypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum compucategoryEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataformatselectionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum datatypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum diagclasstypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum directionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum doctypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum doctype1EEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum encodingEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum encryptcompressmethodtypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum identvaluetypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum intervaltypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum physicaldatatypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum physicallinktypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum pintypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum projidenttypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum radixEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum rowfragmentEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum sessionsubelemtypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum standardisationlevelEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum terminationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum unitgroupcategoryEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum updstatusEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum validtypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType addressingObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType alignTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType commrelationvaluetypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType compucategoryObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType dataformatselectionObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType datatypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType diagclasstypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType directionObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType doctypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType doctypeObject1EDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType encodingObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType encryptcompressmethodtypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType identvaluetypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intervaltypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType physicaldatatypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType physicallinktypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType pintypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType projidenttypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType radixObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType rowfragmentObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType sessionsubelemtypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType standardisationlevelObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType terminationObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType unitgroupcategoryObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType updstatusObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType validtypeObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see OdxXhtml.OdxXhtmlPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OdxXhtmlPackageImpl() {
		super(eNS_URI, OdxXhtmlFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link OdxXhtmlPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @generated
	 */
	public static OdxXhtmlPackage init() {
		if (isInited) return (OdxXhtmlPackage)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredOdxXhtmlPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		OdxXhtmlPackageImpl theOdxXhtmlPackage = registeredOdxXhtmlPackage instanceof OdxXhtmlPackageImpl ? (OdxXhtmlPackageImpl)registeredOdxXhtmlPackage : new OdxXhtmlPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Load packages
		theOdxXhtmlPackage.loadPackage();

		// Fix loaded packages
		theOdxXhtmlPackage.fixPackageContents();

		// Mark meta-data to indicate it can't be changed
		theOdxXhtmlPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OdxXhtmlPackage.eNS_URI, theOdxXhtmlPackage);
		return theOdxXhtmlPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getABLOCK() {
		if (ablockEClass == null) {
			ablockEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(0);
		}
		return ablockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getABLOCK_SHORTNAME() {
        return (EAttribute)getABLOCK().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getABLOCK_LONGNAME() {
        return (EReference)getABLOCK().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getABLOCK_DESC() {
        return (EReference)getABLOCK().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getABLOCK_CATEGORY() {
        return (EAttribute)getABLOCK().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getABLOCK_ADMINDATA() {
        return (EReference)getABLOCK().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getABLOCK_FILES() {
        return (EReference)getABLOCK().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getABLOCK_UPD() {
        return (EAttribute)getABLOCK().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getABLOCKSType() {
		if (ablocksTypeEClass == null) {
			ablocksTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(1);
		}
		return ablocksTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getABLOCKSType_ABLOCK() {
        return (EReference)getABLOCKSType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getACCESSLEVEL() {
		if (accesslevelEClass == null) {
			accesslevelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(2);
		}
		return accesslevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getACCESSLEVEL_VALUE() {
        return (EAttribute)getACCESSLEVEL().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getACCESSLEVEL_DESC() {
        return (EReference)getACCESSLEVEL().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getACCESSLEVEL_DIAGCOMMSNREF() {
        return (EReference)getACCESSLEVEL().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getACCESSLEVEL_EXTERNALACCESSMETHOD() {
        return (EReference)getACCESSLEVEL().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getACCESSLEVELS() {
		if (accesslevelsEClass == null) {
			accesslevelsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(3);
		}
		return accesslevelsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getACCESSLEVELS_ACCESSLEVEL() {
        return (EReference)getACCESSLEVELS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getADDRDEFFILTER() {
		if (addrdeffilterEClass == null) {
			addrdeffilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(4);
		}
		return addrdeffilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getADDRDEFFILTER_FILTEREND() {
        return (EAttribute)getADDRDEFFILTER().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getADDRDEFPHYSSEGMENT() {
		if (addrdefphyssegmentEClass == null) {
			addrdefphyssegmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(5);
		}
		return addrdefphyssegmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getADDRDEFPHYSSEGMENT_ENDADDRESS() {
        return (EAttribute)getADDRDEFPHYSSEGMENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getADMINDATA() {
		if (admindataEClass == null) {
			admindataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(8);
		}
		return admindataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getADMINDATA_LANGUAGE() {
        return (EAttribute)getADMINDATA().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getADMINDATA_COMPANYDOCINFOS() {
        return (EReference)getADMINDATA().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getADMINDATA_DOCREVISIONS() {
        return (EReference)getADMINDATA().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getADMINDATA1() {
		if (admindata1EClass == null) {
			admindata1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(9);
		}
		return admindata1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getADMINDATA1_LANGUAGE() {
        return (EAttribute)getADMINDATA1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getADMINDATA1_COMPANYDOCINFOS() {
        return (EReference)getADMINDATA1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getADMINDATA1_DOCREVISIONS() {
        return (EReference)getADMINDATA1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getALLVALUE() {
		if (allvalueEClass == null) {
			allvalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(12);
		}
		return allvalueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAUDIENCE() {
		if (audienceEClass == null) {
			audienceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(13);
		}
		return audienceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAUDIENCE_ISAFTERMARKET() {
        return (EAttribute)getAUDIENCE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAUDIENCE_ISAFTERSALES() {
        return (EAttribute)getAUDIENCE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAUDIENCE_ISDEVELOPMENT() {
        return (EAttribute)getAUDIENCE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAUDIENCE_ISMANUFACTURING() {
        return (EAttribute)getAUDIENCE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAUDIENCE_ISSUPPLIER() {
        return (EAttribute)getAUDIENCE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAUTMETHOD() {
		if (autmethodEClass == null) {
			autmethodEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(14);
		}
		return autmethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAUTMETHOD_Value() {
        return (EAttribute)getAUTMETHOD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAUTMETHODS() {
		if (autmethodsEClass == null) {
			autmethodsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(15);
		}
		return autmethodsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAUTMETHODS_AUTMETHOD() {
        return (EReference)getAUTMETHODS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBASEVARIANT() {
		if (basevariantEClass == null) {
			basevariantEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(16);
		}
		return basevariantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBASEVARIANT_DIAGVARIABLES() {
        return (EReference)getBASEVARIANT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBASEVARIANT_VARIABLEGROUPS() {
        return (EReference)getBASEVARIANT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBASEVARIANT_AUTMETHODS() {
        return (EReference)getBASEVARIANT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBASEVARIANT_DYNDEFINEDSPEC() {
        return (EReference)getBASEVARIANT().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBASEVARIANT_ACCESSLEVELS() {
        return (EReference)getBASEVARIANT().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBASEVARIANT_PARENTREFS() {
        return (EReference)getBASEVARIANT().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBASEVARIANTREF() {
		if (basevariantrefEClass == null) {
			basevariantrefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(17);
		}
		return basevariantrefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBASEVARIANTS() {
		if (basevariantsEClass == null) {
			basevariantsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(18);
		}
		return basevariantsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBASEVARIANTS_BASEVARIANT() {
        return (EReference)getBASEVARIANTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBASICSTRUCTURE() {
		if (basicstructureEClass == null) {
			basicstructureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(19);
		}
		return basicstructureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBASICSTRUCTURE_BYTESIZE() {
        return (EAttribute)getBASICSTRUCTURE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBASICSTRUCTURE_PARAMS() {
        return (EReference)getBASICSTRUCTURE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBlock() {
		if (blockEClass == null) {
			blockEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(20);
		}
		return blockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBlock_Block() {
        return (EAttribute)getBlock().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlock_P() {
        return (EReference)getBlock().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlock_Ul() {
        return (EReference)getBlock().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBlock_Ol() {
        return (EReference)getBlock().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBType() {
		if (bTypeEClass == null) {
			bTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(21);
		}
		return bTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCASE() {
		if (caseEClass == null) {
			caseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(22);
		}
		return caseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCASE_SHORTNAME() {
        return (EAttribute)getCASE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCASE_LONGNAME() {
        return (EReference)getCASE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCASE_DESC() {
        return (EReference)getCASE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCASE_STRUCTUREREF() {
        return (EReference)getCASE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCASE_LOWERLIMIT() {
        return (EReference)getCASE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCASE_UPPERLIMIT() {
        return (EReference)getCASE().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCASES() {
		if (casesEClass == null) {
			casesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(23);
		}
		return casesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCASES_CASE() {
        return (EReference)getCASES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCATALOG() {
		if (catalogEClass == null) {
			catalogEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(24);
		}
		return catalogEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCATALOG_SHORTNAME() {
        return (EAttribute)getCATALOG().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCATALOG_COMPANYDATAS() {
        return (EReference)getCATALOG().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCATALOG_ADMINDATA() {
        return (EReference)getCATALOG().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCATALOG_ABLOCKS() {
        return (EReference)getCATALOG().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCHECKSUM() {
		if (checksumEClass == null) {
			checksumEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(25);
		}
		return checksumEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCHECKSUM_SHORTNAME() {
        return (EAttribute)getCHECKSUM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCHECKSUM_LONGNAME() {
        return (EReference)getCHECKSUM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCHECKSUM_DESC() {
        return (EReference)getCHECKSUM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCHECKSUM_FILLBYTE() {
        return (EAttribute)getCHECKSUM().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCHECKSUM_SOURCESTARTADDRESS() {
        return (EAttribute)getCHECKSUM().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCHECKSUM_COMPRESSEDSIZE() {
        return (EAttribute)getCHECKSUM().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCHECKSUM_CHECKSUMALG() {
        return (EAttribute)getCHECKSUM().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCHECKSUM_SOURCEENDADDRESS() {
        return (EReference)getCHECKSUM().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCHECKSUM_UNCOMPRESSEDSIZE() {
        return (EReference)getCHECKSUM().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCHECKSUM_CHECKSUMRESULT() {
        return (EReference)getCHECKSUM().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCHECKSUM_ID() {
        return (EAttribute)getCHECKSUM().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCHECKSUM_OID() {
        return (EAttribute)getCHECKSUM().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCHECKSUMRESULT() {
		if (checksumresultEClass == null) {
			checksumresultEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(26);
		}
		return checksumresultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCHECKSUMRESULT_Value() {
        return (EAttribute)getCHECKSUMRESULT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCHECKSUMRESULT_TYPE() {
        return (EAttribute)getCHECKSUMRESULT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCHECKSUMS() {
		if (checksumsEClass == null) {
			checksumsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(27);
		}
		return checksumsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCHECKSUMS_CHECKSUM() {
        return (EReference)getCHECKSUMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCODEDCONST() {
		if (codedconstEClass == null) {
			codedconstEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(28);
		}
		return codedconstEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCODEDCONST_CODEDVALUE() {
        return (EAttribute)getCODEDCONST().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCODEDCONST_DIAGCODEDTYPE() {
        return (EReference)getCODEDCONST().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMMRELATION() {
		if (commrelationEClass == null) {
			commrelationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(29);
		}
		return commrelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMMRELATION_DESC() {
        return (EReference)getCOMMRELATION().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMMRELATION_RELATIONTYPE() {
        return (EAttribute)getCOMMRELATION().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMMRELATION_DIAGCOMMREF() {
        return (EReference)getCOMMRELATION().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMMRELATION_DIAGCOMMSNREF() {
        return (EReference)getCOMMRELATION().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMMRELATION_INPARAMIFSNREF() {
        return (EReference)getCOMMRELATION().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMMRELATION_OUTPARAMIFSNREF() {
        return (EReference)getCOMMRELATION().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMMRELATION_VALUETYPE() {
        return (EAttribute)getCOMMRELATION().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMMRELATIONS() {
		if (commrelationsEClass == null) {
			commrelationsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(30);
		}
		return commrelationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMMRELATIONS_COMMRELATION() {
        return (EReference)getCOMMRELATIONS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYDATA() {
		if (companydataEClass == null) {
			companydataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(33);
		}
		return companydataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYDATA_SHORTNAME() {
        return (EAttribute)getCOMPANYDATA().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA_LONGNAME() {
        return (EReference)getCOMPANYDATA().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA_DESC() {
        return (EReference)getCOMPANYDATA().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA_ROLES() {
        return (EReference)getCOMPANYDATA().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA_TEAMMEMBERS() {
        return (EReference)getCOMPANYDATA().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA_COMPANYSPECIFICINFO() {
        return (EReference)getCOMPANYDATA().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYDATA_ID() {
        return (EAttribute)getCOMPANYDATA().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYDATA_OID() {
        return (EAttribute)getCOMPANYDATA().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYDATA1() {
		if (companydata1EClass == null) {
			companydata1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(34);
		}
		return companydata1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYDATA1_SHORTNAME() {
        return (EAttribute)getCOMPANYDATA1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA1_LONGNAME() {
        return (EReference)getCOMPANYDATA1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA1_DESC() {
        return (EReference)getCOMPANYDATA1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA1_ROLES() {
        return (EReference)getCOMPANYDATA1().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA1_TEAMMEMBERS() {
        return (EReference)getCOMPANYDATA1().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATA1_COMPANYSPECIFICINFO() {
        return (EReference)getCOMPANYDATA1().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYDATA1_ID() {
        return (EAttribute)getCOMPANYDATA1().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYDATA1_OID() {
        return (EAttribute)getCOMPANYDATA1().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYDATAS() {
		if (companydatasEClass == null) {
			companydatasEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(35);
		}
		return companydatasEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATAS_COMPANYDATA() {
        return (EReference)getCOMPANYDATAS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYDATASType() {
		if (companydatasTypeEClass == null) {
			companydatasTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(36);
		}
		return companydatasTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDATASType_COMPANYDATA() {
        return (EReference)getCOMPANYDATASType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYDOCINFO() {
		if (companydocinfoEClass == null) {
			companydocinfoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(37);
		}
		return companydocinfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDOCINFO_COMPANYDATAREF() {
        return (EReference)getCOMPANYDOCINFO().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDOCINFO_TEAMMEMBERREF() {
        return (EReference)getCOMPANYDOCINFO().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYDOCINFO_DOCLABEL() {
        return (EAttribute)getCOMPANYDOCINFO().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDOCINFO_SDGS() {
        return (EReference)getCOMPANYDOCINFO().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYDOCINFO1() {
		if (companydocinfo1EClass == null) {
			companydocinfo1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(38);
		}
		return companydocinfo1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDOCINFO1_COMPANYDATAREF() {
        return (EReference)getCOMPANYDOCINFO1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDOCINFO1_TEAMMEMBERREF() {
        return (EReference)getCOMPANYDOCINFO1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYDOCINFO1_DOCLABEL() {
        return (EAttribute)getCOMPANYDOCINFO1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDOCINFO1_SDGS() {
        return (EReference)getCOMPANYDOCINFO1().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYDOCINFOS() {
		if (companydocinfosEClass == null) {
			companydocinfosEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(39);
		}
		return companydocinfosEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDOCINFOS_COMPANYDOCINFO() {
        return (EReference)getCOMPANYDOCINFOS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYDOCINFOS1() {
		if (companydocinfos1EClass == null) {
			companydocinfos1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(40);
		}
		return companydocinfos1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYDOCINFOS1_COMPANYDOCINFO() {
        return (EReference)getCOMPANYDOCINFOS1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYREVISIONINFO() {
		if (companyrevisioninfoEClass == null) {
			companyrevisioninfoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(41);
		}
		return companyrevisioninfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYREVISIONINFO_COMPANYDATAREF() {
        return (EReference)getCOMPANYREVISIONINFO().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYREVISIONINFO_REVISIONLABEL() {
        return (EAttribute)getCOMPANYREVISIONINFO().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYREVISIONINFO_STATE() {
        return (EAttribute)getCOMPANYREVISIONINFO().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYREVISIONINFO1() {
		if (companyrevisioninfo1EClass == null) {
			companyrevisioninfo1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(42);
		}
		return companyrevisioninfo1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYREVISIONINFO1_COMPANYDATAREF() {
        return (EReference)getCOMPANYREVISIONINFO1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYREVISIONINFO1_REVISIONLABEL() {
        return (EAttribute)getCOMPANYREVISIONINFO1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPANYREVISIONINFO1_STATE() {
        return (EAttribute)getCOMPANYREVISIONINFO1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYREVISIONINFOS() {
		if (companyrevisioninfosEClass == null) {
			companyrevisioninfosEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(43);
		}
		return companyrevisioninfosEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYREVISIONINFOS_COMPANYREVISIONINFO() {
        return (EReference)getCOMPANYREVISIONINFOS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYREVISIONINFOS1() {
		if (companyrevisioninfos1EClass == null) {
			companyrevisioninfos1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(44);
		}
		return companyrevisioninfos1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYREVISIONINFOS1_COMPANYREVISIONINFO() {
        return (EReference)getCOMPANYREVISIONINFOS1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYSPECIFICINFO() {
		if (companyspecificinfoEClass == null) {
			companyspecificinfoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(45);
		}
		return companyspecificinfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYSPECIFICINFO_RELATEDDOCS() {
        return (EReference)getCOMPANYSPECIFICINFO().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYSPECIFICINFO_SDGS() {
        return (EReference)getCOMPANYSPECIFICINFO().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPANYSPECIFICINFO1() {
		if (companyspecificinfo1EClass == null) {
			companyspecificinfo1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(46);
		}
		return companyspecificinfo1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYSPECIFICINFO1_RELATEDDOCS() {
        return (EReference)getCOMPANYSPECIFICINFO1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPANYSPECIFICINFO1_SDGS() {
        return (EReference)getCOMPANYSPECIFICINFO1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPARAM() {
		if (comparamEClass == null) {
			comparamEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(47);
		}
		return comparamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAM_SHORTNAME() {
        return (EAttribute)getCOMPARAM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAM_LONGNAME() {
        return (EReference)getCOMPARAM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAM_DESC() {
        return (EReference)getCOMPARAM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAM_PHYSICALDEFAULTVALUE() {
        return (EAttribute)getCOMPARAM().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAM_DATAOBJECTPROPREF() {
        return (EReference)getCOMPARAM().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAM_CPTYPE() {
        return (EAttribute)getCOMPARAM().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAM_DISPLAYLEVEL() {
        return (EAttribute)getCOMPARAM().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAM_ID() {
        return (EAttribute)getCOMPARAM().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAM_OID() {
        return (EAttribute)getCOMPARAM().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAM_PARAMCLASS() {
        return (EAttribute)getCOMPARAM().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAM_PDUAPIINDEX() {
        return (EAttribute)getCOMPARAM().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPARAMREF() {
		if (comparamrefEClass == null) {
			comparamrefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(48);
		}
		return comparamrefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAMREF_VALUE() {
        return (EAttribute)getCOMPARAMREF().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAMREF_DESC() {
        return (EReference)getCOMPARAMREF().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAMREF_PROTOCOLSNREF() {
        return (EReference)getCOMPARAMREF().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAMREF_DOCREF() {
        return (EAttribute)getCOMPARAMREF().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAMREF_DOCTYPE() {
        return (EAttribute)getCOMPARAMREF().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAMREF_IDREF() {
        return (EAttribute)getCOMPARAMREF().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPARAMREF_REVISION() {
        return (EAttribute)getCOMPARAMREF().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPARAMREFS() {
		if (comparamrefsEClass == null) {
			comparamrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(49);
		}
		return comparamrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAMREFS_COMPARAMREF() {
        return (EReference)getCOMPARAMREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPARAMS() {
		if (comparamsEClass == null) {
			comparamsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(50);
		}
		return comparamsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAMS_COMPARAM() {
        return (EReference)getCOMPARAMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPARAMSPEC() {
		if (comparamspecEClass == null) {
			comparamspecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(51);
		}
		return comparamspecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAMSPEC_COMPARAMS() {
        return (EReference)getCOMPARAMSPEC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAMSPEC_DATAOBJECTPROPS() {
        return (EReference)getCOMPARAMSPEC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPARAMSPEC_UNITSPEC() {
        return (EReference)getCOMPARAMSPEC().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPLEXDOP() {
		if (complexdopEClass == null) {
			complexdopEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(52);
		}
		return complexdopEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUCONST() {
		if (compuconstEClass == null) {
			compuconstEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(55);
		}
		return compuconstEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUCONST_V() {
        return (EReference)getCOMPUCONST().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUCONST_VT() {
        return (EReference)getCOMPUCONST().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUDEFAULTVALUE() {
		if (compudefaultvalueEClass == null) {
			compudefaultvalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(56);
		}
		return compudefaultvalueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUDEFAULTVALUE_V() {
        return (EReference)getCOMPUDEFAULTVALUE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUDEFAULTVALUE_VT() {
        return (EReference)getCOMPUDEFAULTVALUE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUDENOMINATOR() {
		if (compudenominatorEClass == null) {
			compudenominatorEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(57);
		}
		return compudenominatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUDENOMINATOR_V() {
        return (EReference)getCOMPUDENOMINATOR().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUINTERNALTOPHYS() {
		if (compuinternaltophysEClass == null) {
			compuinternaltophysEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(58);
		}
		return compuinternaltophysEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUINTERNALTOPHYS_COMPUSCALES() {
        return (EReference)getCOMPUINTERNALTOPHYS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUINTERNALTOPHYS_PROGCODE() {
        return (EReference)getCOMPUINTERNALTOPHYS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUINTERNALTOPHYS_COMPUDEFAULTVALUE() {
        return (EReference)getCOMPUINTERNALTOPHYS().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUINVERSEVALUE() {
		if (compuinversevalueEClass == null) {
			compuinversevalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(59);
		}
		return compuinversevalueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUINVERSEVALUE_V() {
        return (EReference)getCOMPUINVERSEVALUE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUMETHOD() {
		if (compumethodEClass == null) {
			compumethodEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(60);
		}
		return compumethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCOMPUMETHOD_CATEGORY() {
        return (EAttribute)getCOMPUMETHOD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUMETHOD_COMPUINTERNALTOPHYS() {
        return (EReference)getCOMPUMETHOD().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUMETHOD_COMPUPHYSTOINTERNAL() {
        return (EReference)getCOMPUMETHOD().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUNUMERATOR() {
		if (compunumeratorEClass == null) {
			compunumeratorEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(61);
		}
		return compunumeratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUNUMERATOR_V() {
        return (EReference)getCOMPUNUMERATOR().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUPHYSTOINTERNAL() {
		if (compuphystointernalEClass == null) {
			compuphystointernalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(62);
		}
		return compuphystointernalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUPHYSTOINTERNAL_PROGCODE() {
        return (EReference)getCOMPUPHYSTOINTERNAL().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPURATIONALCOEFFS() {
		if (compurationalcoeffsEClass == null) {
			compurationalcoeffsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(63);
		}
		return compurationalcoeffsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPURATIONALCOEFFS_COMPUNUMERATOR() {
        return (EReference)getCOMPURATIONALCOEFFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPURATIONALCOEFFS_COMPUDENOMINATOR() {
        return (EReference)getCOMPURATIONALCOEFFS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUSCALE() {
		if (compuscaleEClass == null) {
			compuscaleEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(64);
		}
		return compuscaleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUSCALE_SHORTLABEL() {
        return (EReference)getCOMPUSCALE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUSCALE_DESC() {
        return (EReference)getCOMPUSCALE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUSCALE_LOWERLIMIT() {
        return (EReference)getCOMPUSCALE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUSCALE_UPPERLIMIT() {
        return (EReference)getCOMPUSCALE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUSCALE_COMPUINVERSEVALUE() {
        return (EReference)getCOMPUSCALE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUSCALE_COMPUCONST() {
        return (EReference)getCOMPUSCALE().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUSCALE_COMPURATIONALCOEFFS() {
        return (EReference)getCOMPUSCALE().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCOMPUSCALES() {
		if (compuscalesEClass == null) {
			compuscalesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(65);
		}
		return compuscalesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCOMPUSCALES_COMPUSCALE() {
        return (EReference)getCOMPUSCALES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDATABLOCK() {
		if (datablockEClass == null) {
			datablockEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(66);
		}
		return datablockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDATABLOCK_SHORTNAME() {
        return (EAttribute)getDATABLOCK().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCK_LONGNAME() {
        return (EReference)getDATABLOCK().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCK_DESC() {
        return (EReference)getDATABLOCK().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCK_FLASHDATAREF() {
        return (EReference)getDATABLOCK().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCK_FILTERS() {
        return (EReference)getDATABLOCK().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCK_SEGMENTS() {
        return (EReference)getDATABLOCK().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCK_TARGETADDROFFSET() {
        return (EReference)getDATABLOCK().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCK_OWNIDENTS() {
        return (EReference)getDATABLOCK().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCK_SECURITYS() {
        return (EReference)getDATABLOCK().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCK_SDGS() {
        return (EReference)getDATABLOCK().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDATABLOCK_ID() {
        return (EAttribute)getDATABLOCK().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDATABLOCK_OID() {
        return (EAttribute)getDATABLOCK().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDATABLOCK_TYPE() {
        return (EAttribute)getDATABLOCK().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDATABLOCKREFS() {
		if (datablockrefsEClass == null) {
			datablockrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(67);
		}
		return datablockrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCKREFS_DATABLOCKREF() {
        return (EReference)getDATABLOCKREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDATABLOCKS() {
		if (datablocksEClass == null) {
			datablocksEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(68);
		}
		return datablocksEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATABLOCKS_DATABLOCK() {
        return (EReference)getDATABLOCKS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDATAFILE() {
		if (datafileEClass == null) {
			datafileEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(69);
		}
		return datafileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDATAFILE_Value() {
        return (EAttribute)getDATAFILE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDATAFILE_LATEBOUNDDATAFILE() {
        return (EAttribute)getDATAFILE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDATAFORMAT() {
		if (dataformatEClass == null) {
			dataformatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(70);
		}
		return dataformatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDATAFORMAT_Value() {
        return (EAttribute)getDATAFORMAT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDATAFORMAT_SELECTION() {
        return (EAttribute)getDATAFORMAT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDATAOBJECTPROP() {
		if (dataobjectpropEClass == null) {
			dataobjectpropEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(73);
		}
		return dataobjectpropEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATAOBJECTPROP_COMPUMETHOD() {
        return (EReference)getDATAOBJECTPROP().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATAOBJECTPROP_DIAGCODEDTYPE() {
        return (EReference)getDATAOBJECTPROP().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATAOBJECTPROP_PHYSICALTYPE() {
        return (EReference)getDATAOBJECTPROP().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATAOBJECTPROP_INTERNALCONSTR() {
        return (EReference)getDATAOBJECTPROP().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATAOBJECTPROP_UNITREF() {
        return (EReference)getDATAOBJECTPROP().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDATAOBJECTPROPS() {
		if (dataobjectpropsEClass == null) {
			dataobjectpropsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(74);
		}
		return dataobjectpropsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDATAOBJECTPROPS_DATAOBJECTPROP() {
        return (EReference)getDATAOBJECTPROPS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDEFAULTCASE() {
		if (defaultcaseEClass == null) {
			defaultcaseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(77);
		}
		return defaultcaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDEFAULTCASE_SHORTNAME() {
        return (EAttribute)getDEFAULTCASE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDEFAULTCASE_LONGNAME() {
        return (EReference)getDEFAULTCASE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDEFAULTCASE_DESC() {
        return (EReference)getDEFAULTCASE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDEFAULTCASE_STRUCTUREREF() {
        return (EReference)getDEFAULTCASE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDESCRIPTION() {
		if (descriptionEClass == null) {
			descriptionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(78);
		}
		return descriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDESCRIPTION_P() {
        return (EReference)getDESCRIPTION().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDESCRIPTION_TI() {
        return (EAttribute)getDESCRIPTION().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDESCRIPTION1() {
		if (description1EClass == null) {
			description1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(79);
		}
		return description1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDESCRIPTION1_P() {
        return (EReference)getDESCRIPTION1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDESCRIPTION1_TI() {
        return (EAttribute)getDESCRIPTION1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDETERMINENUMBEROFITEMS() {
		if (determinenumberofitemsEClass == null) {
			determinenumberofitemsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(80);
		}
		return determinenumberofitemsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDETERMINENUMBEROFITEMS_BYTEPOSITION() {
        return (EAttribute)getDETERMINENUMBEROFITEMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDETERMINENUMBEROFITEMS_BITPOSITION() {
        return (EAttribute)getDETERMINENUMBEROFITEMS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDETERMINENUMBEROFITEMS_DATAOBJECTPROPREF() {
        return (EReference)getDETERMINENUMBEROFITEMS().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGCODEDTYPE() {
		if (diagcodedtypeEClass == null) {
			diagcodedtypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(83);
		}
		return diagcodedtypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCODEDTYPE_BASEDATATYPE() {
        return (EAttribute)getDIAGCODEDTYPE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCODEDTYPE_BASETYPEENCODING() {
        return (EAttribute)getDIAGCODEDTYPE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCODEDTYPE_ISHIGHLOWBYTEORDER() {
        return (EAttribute)getDIAGCODEDTYPE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGCOMM() {
		if (diagcommEClass == null) {
			diagcommEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(84);
		}
		return diagcommEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMM_SHORTNAME() {
        return (EAttribute)getDIAGCOMM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMM_LONGNAME() {
        return (EReference)getDIAGCOMM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMM_DESC() {
        return (EReference)getDIAGCOMM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMM_ADMINDATA() {
        return (EReference)getDIAGCOMM().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMM_SDGS() {
        return (EReference)getDIAGCOMM().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMM_COMPARAMREFS() {
        return (EReference)getDIAGCOMM().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMM_FUNCTCLASSREFS() {
        return (EReference)getDIAGCOMM().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMM_AUDIENCE() {
        return (EReference)getDIAGCOMM().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMM_PROTOCOLSNREFS() {
        return (EReference)getDIAGCOMM().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMM_RELATEDDIAGCOMMREFS() {
        return (EReference)getDIAGCOMM().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMM_DIAGNOSTICCLASS() {
        return (EAttribute)getDIAGCOMM().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMM_ID() {
        return (EAttribute)getDIAGCOMM().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMM_ISEXECUTABLE() {
        return (EAttribute)getDIAGCOMM().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMM_ISFINAL() {
        return (EAttribute)getDIAGCOMM().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMM_ISMANDATORY() {
        return (EAttribute)getDIAGCOMM().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMM_OID() {
        return (EAttribute)getDIAGCOMM().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMM_SECURITYACCESSLEVEL() {
        return (EAttribute)getDIAGCOMM().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMM_SEMANTIC() {
        return (EAttribute)getDIAGCOMM().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGCOMMS() {
		if (diagcommsEClass == null) {
			diagcommsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(85);
		}
		return diagcommsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGCOMMS_DIAGCOMMPROXY() {
        return (EAttribute)getDIAGCOMMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMMS_DIAGSERVICE() {
        return (EReference)getDIAGCOMMS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMMS_SINGLEECUJOB() {
        return (EReference)getDIAGCOMMS().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGCOMMS_DIAGCOMMREF() {
        return (EReference)getDIAGCOMMS().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGDATADICTIONARYSPEC() {
		if (diagdatadictionaryspecEClass == null) {
			diagdatadictionaryspecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(86);
		}
		return diagdatadictionaryspecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_ADMINDATA() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_DTCDOPS() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_ENVDATADESCS() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_DATAOBJECTPROPS() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_STRUCTURES() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_STATICFIELDS() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_DYNAMICLENGTHFIELDS() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_DYNAMICENDMARKERFIELDS() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_ENDOFPDUFIELDS() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_MUXS() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_ENVDATAS() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_UNITSPEC() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGDATADICTIONARYSPEC_TABLES() {
        return (EReference)getDIAGDATADICTIONARYSPEC().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGLAYER() {
		if (diaglayerEClass == null) {
			diaglayerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(87);
		}
		return diaglayerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGLAYER_SHORTNAME() {
        return (EAttribute)getDIAGLAYER().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_LONGNAME() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_DESC() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_ADMINDATA() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_COMPANYDATAS() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_FUNCTCLASSS() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_DIAGDATADICTIONARYSPEC() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_DIAGCOMMS() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_REQUESTS() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_POSRESPONSES() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_NEGRESPONSES() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYER_GLOBALNEGRESPONSES() {
        return (EReference)getDIAGLAYER().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGLAYER_ID() {
        return (EAttribute)getDIAGLAYER().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGLAYER_OID() {
        return (EAttribute)getDIAGLAYER().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGLAYERCONTAINER() {
		if (diaglayercontainerEClass == null) {
			diaglayercontainerEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(88);
		}
		return diaglayercontainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYERCONTAINER_PROTOCOLS() {
        return (EReference)getDIAGLAYERCONTAINER().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYERCONTAINER_FUNCTIONALGROUPS() {
        return (EReference)getDIAGLAYERCONTAINER().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYERCONTAINER_ECUSHAREDDATAS() {
        return (EReference)getDIAGLAYERCONTAINER().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYERCONTAINER_BASEVARIANTS() {
        return (EReference)getDIAGLAYERCONTAINER().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYERCONTAINER_ECUVARIANTS() {
        return (EReference)getDIAGLAYERCONTAINER().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGLAYERREFS() {
		if (diaglayerrefsEClass == null) {
			diaglayerrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(89);
		}
		return diaglayerrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGLAYERREFS_DIAGLAYERREF() {
        return (EReference)getDIAGLAYERREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGSERVICE() {
		if (diagserviceEClass == null) {
			diagserviceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(90);
		}
		return diagserviceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGSERVICE_REQUESTREF() {
        return (EReference)getDIAGSERVICE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGSERVICE_POSRESPONSEREFS() {
        return (EReference)getDIAGSERVICE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGSERVICE_NEGRESPONSEREFS() {
        return (EReference)getDIAGSERVICE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGSERVICE_ADDRESSING() {
        return (EAttribute)getDIAGSERVICE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGSERVICE_ISCYCLIC() {
        return (EAttribute)getDIAGSERVICE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGSERVICE_ISMULTIPLE() {
        return (EAttribute)getDIAGSERVICE().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGVARIABLE() {
		if (diagvariableEClass == null) {
			diagvariableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(91);
		}
		return diagvariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGVARIABLE_SHORTNAME() {
        return (EAttribute)getDIAGVARIABLE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGVARIABLE_LONGNAME() {
        return (EReference)getDIAGVARIABLE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGVARIABLE_DESC() {
        return (EReference)getDIAGVARIABLE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGVARIABLE_ADMINDATA() {
        return (EReference)getDIAGVARIABLE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGVARIABLE_VARIABLEGROUPREF() {
        return (EReference)getDIAGVARIABLE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGVARIABLE_SWVARIABLES() {
        return (EReference)getDIAGVARIABLE().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGVARIABLE_COMMRELATIONS() {
        return (EReference)getDIAGVARIABLE().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGVARIABLE_ID() {
        return (EAttribute)getDIAGVARIABLE().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGVARIABLE_ISREADBEFOREWRITE() {
        return (EAttribute)getDIAGVARIABLE().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGVARIABLE_OID() {
        return (EAttribute)getDIAGVARIABLE().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDIAGVARIABLES() {
		if (diagvariablesEClass == null) {
			diagvariablesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(92);
		}
		return diagvariablesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDIAGVARIABLES_DIAGVARIABLEPROXY() {
        return (EAttribute)getDIAGVARIABLES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGVARIABLES_DIAGVARIABLEREF() {
        return (EReference)getDIAGVARIABLES().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDIAGVARIABLES_DIAGVARIABLE() {
        return (EReference)getDIAGVARIABLES().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDOCREVISION() {
		if (docrevisionEClass == null) {
			docrevisionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(95);
		}
		return docrevisionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOCREVISION_TEAMMEMBERREF() {
        return (EReference)getDOCREVISION().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOCREVISION_REVISIONLABEL() {
        return (EAttribute)getDOCREVISION().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOCREVISION_STATE() {
        return (EAttribute)getDOCREVISION().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOCREVISION_DATE() {
        return (EAttribute)getDOCREVISION().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOCREVISION_TOOL() {
        return (EAttribute)getDOCREVISION().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOCREVISION_COMPANYREVISIONINFOS() {
        return (EReference)getDOCREVISION().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOCREVISION_MODIFICATIONS() {
        return (EReference)getDOCREVISION().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDOCREVISION1() {
		if (docrevision1EClass == null) {
			docrevision1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(96);
		}
		return docrevision1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOCREVISION1_TEAMMEMBERREF() {
        return (EReference)getDOCREVISION1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOCREVISION1_REVISIONLABEL() {
        return (EAttribute)getDOCREVISION1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOCREVISION1_STATE() {
        return (EAttribute)getDOCREVISION1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOCREVISION1_DATE() {
        return (EAttribute)getDOCREVISION1().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOCREVISION1_TOOL() {
        return (EAttribute)getDOCREVISION1().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOCREVISION1_COMPANYREVISIONINFOS() {
        return (EReference)getDOCREVISION1().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOCREVISION1_MODIFICATIONS() {
        return (EReference)getDOCREVISION1().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDOCREVISIONS() {
		if (docrevisionsEClass == null) {
			docrevisionsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(97);
		}
		return docrevisionsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOCREVISIONS_DOCREVISION() {
        return (EReference)getDOCREVISIONS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDOCREVISIONS1() {
		if (docrevisions1EClass == null) {
			docrevisions1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(98);
		}
		return docrevisions1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOCREVISIONS1_DOCREVISION() {
        return (EReference)getDOCREVISIONS1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDocumentRoot() {
		if (documentRootEClass == null) {
			documentRootEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(103);
		}
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDocumentRoot_Mixed() {
        return (EAttribute)getDocumentRoot().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XMLNSPrefixMap() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XSISchemaLocation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_B() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Br() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_CATALOG() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_I() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Li() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Ol() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Sub() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Sup() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_Ul() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_ODX() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDOPBASE() {
		if (dopbaseEClass == null) {
			dopbaseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(104);
		}
		return dopbaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOPBASE_SHORTNAME() {
        return (EAttribute)getDOPBASE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOPBASE_LONGNAME() {
        return (EReference)getDOPBASE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOPBASE_DESC() {
        return (EReference)getDOPBASE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDOPBASE_ADMINDATA() {
        return (EReference)getDOPBASE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOPBASE_ID() {
        return (EAttribute)getDOPBASE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDOPBASE_OID() {
        return (EAttribute)getDOPBASE().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDTC() {
		if (dtcEClass == null) {
			dtcEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(105);
		}
		return dtcEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDTC_SHORTNAME() {
        return (EAttribute)getDTC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDTC_TROUBLECODE() {
        return (EAttribute)getDTC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDTC_DISPLAYTROUBLECODE() {
        return (EAttribute)getDTC().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTC_TEXT() {
        return (EReference)getDTC().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDTC_LEVEL() {
        return (EAttribute)getDTC().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTC_SDGS() {
        return (EReference)getDTC().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDTC_ID() {
        return (EAttribute)getDTC().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDTC_OID() {
        return (EAttribute)getDTC().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDTCDOP() {
		if (dtcdopEClass == null) {
			dtcdopEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(106);
		}
		return dtcdopEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTCDOP_DIAGCODEDTYPE() {
        return (EReference)getDTCDOP().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTCDOP_PHYSICALTYPE() {
        return (EReference)getDTCDOP().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTCDOP_COMPUMETHOD() {
        return (EReference)getDTCDOP().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTCDOP_DTCS() {
        return (EReference)getDTCDOP().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDTCDOP_ISVISIBLE() {
        return (EAttribute)getDTCDOP().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDTCDOPS() {
		if (dtcdopsEClass == null) {
			dtcdopsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(107);
		}
		return dtcdopsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTCDOPS_DTCDOP() {
        return (EReference)getDTCDOPS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDTCS() {
		if (dtcsEClass == null) {
			dtcsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(108);
		}
		return dtcsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDTCS_DTCPROXY() {
        return (EAttribute)getDTCS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTCS_DTCREF() {
        return (EReference)getDTCS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTCS_DTC() {
        return (EReference)getDTCS().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDTCVALUE() {
		if (dtcvalueEClass == null) {
			dtcvalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(109);
		}
		return dtcvalueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDTCVALUE_Value() {
        return (EAttribute)getDTCVALUE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDTCVALUES() {
		if (dtcvaluesEClass == null) {
			dtcvaluesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(110);
		}
		return dtcvaluesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDTCVALUES_DTCVALUE() {
        return (EReference)getDTCVALUES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDYNAMIC() {
		if (dynamicEClass == null) {
			dynamicEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(111);
		}
		return dynamicEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDYNAMICENDMARKERFIELD() {
		if (dynamicendmarkerfieldEClass == null) {
			dynamicendmarkerfieldEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(112);
		}
		return dynamicendmarkerfieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNAMICENDMARKERFIELD_DATAOBJECTPROPREF() {
        return (EReference)getDYNAMICENDMARKERFIELD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDYNAMICENDMARKERFIELDS() {
		if (dynamicendmarkerfieldsEClass == null) {
			dynamicendmarkerfieldsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(113);
		}
		return dynamicendmarkerfieldsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNAMICENDMARKERFIELDS_DYNAMICENDMARKERFIELD() {
        return (EReference)getDYNAMICENDMARKERFIELDS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDYNAMICLENGTHFIELD() {
		if (dynamiclengthfieldEClass == null) {
			dynamiclengthfieldEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(114);
		}
		return dynamiclengthfieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDYNAMICLENGTHFIELD_OFFSET() {
        return (EAttribute)getDYNAMICLENGTHFIELD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNAMICLENGTHFIELD_DETERMINENUMBEROFITEMS() {
        return (EReference)getDYNAMICLENGTHFIELD().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDYNAMICLENGTHFIELDS() {
		if (dynamiclengthfieldsEClass == null) {
			dynamiclengthfieldsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(115);
		}
		return dynamiclengthfieldsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNAMICLENGTHFIELDS_DYNAMICLENGTHFIELD() {
        return (EReference)getDYNAMICLENGTHFIELDS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDYNDEFINEDSPEC() {
		if (dyndefinedspecEClass == null) {
			dyndefinedspecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(116);
		}
		return dyndefinedspecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNDEFINEDSPEC_DYNIDDEFMODEINFOS() {
        return (EReference)getDYNDEFINEDSPEC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDYNENDDOPREF() {
		if (dynenddoprefEClass == null) {
			dynenddoprefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(117);
		}
		return dynenddoprefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDYNENDDOPREF_TERMINATIONVALUE() {
        return (EAttribute)getDYNENDDOPREF().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDYNENDDOPREF_DOCREF() {
        return (EAttribute)getDYNENDDOPREF().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDYNENDDOPREF_DOCTYPE() {
        return (EAttribute)getDYNENDDOPREF().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDYNENDDOPREF_IDREF() {
        return (EAttribute)getDYNENDDOPREF().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDYNENDDOPREF_REVISION() {
        return (EAttribute)getDYNENDDOPREF().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDYNIDDEFMODEINFO() {
		if (dyniddefmodeinfoEClass == null) {
			dyniddefmodeinfoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(118);
		}
		return dyniddefmodeinfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDYNIDDEFMODEINFO_DEFMODE() {
        return (EAttribute)getDYNIDDEFMODEINFO().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNIDDEFMODEINFO_CLEARDYNDEFMESSAGEREF() {
        return (EReference)getDYNIDDEFMODEINFO().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNIDDEFMODEINFO_CLEARDYNDEFMESSAGESNREF() {
        return (EReference)getDYNIDDEFMODEINFO().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNIDDEFMODEINFO_READDYNDEFMESSAGEREF() {
        return (EReference)getDYNIDDEFMODEINFO().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNIDDEFMODEINFO_READDYNDEFMESSAGESNREF() {
        return (EReference)getDYNIDDEFMODEINFO().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNIDDEFMODEINFO_DYNDEFMESSAGEREF() {
        return (EReference)getDYNIDDEFMODEINFO().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNIDDEFMODEINFO_DYNDEFMESSAGESNREF() {
        return (EReference)getDYNIDDEFMODEINFO().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNIDDEFMODEINFO_SUPPORTEDDYNIDS() {
        return (EReference)getDYNIDDEFMODEINFO().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNIDDEFMODEINFO_SELECTIONTABLEREFS() {
        return (EReference)getDYNIDDEFMODEINFO().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDYNIDDEFMODEINFOS() {
		if (dyniddefmodeinfosEClass == null) {
			dyniddefmodeinfosEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(119);
		}
		return dyniddefmodeinfosEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDYNIDDEFMODEINFOS_DYNIDDEFMODEINFO() {
        return (EReference)getDYNIDDEFMODEINFOS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUGROUP() {
		if (ecugroupEClass == null) {
			ecugroupEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(120);
		}
		return ecugroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getECUGROUP_SHORTNAME() {
        return (EAttribute)getECUGROUP().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUGROUP_LONGNAME() {
        return (EReference)getECUGROUP().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUGROUP_DESC() {
        return (EReference)getECUGROUP().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUGROUP_LOGICALLINKREFS() {
        return (EReference)getECUGROUP().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getECUGROUP_OID() {
        return (EAttribute)getECUGROUP().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUGROUPS() {
		if (ecugroupsEClass == null) {
			ecugroupsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(121);
		}
		return ecugroupsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUGROUPS_ECUGROUP() {
        return (EReference)getECUGROUPS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUMEM() {
		if (ecumemEClass == null) {
			ecumemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(122);
		}
		return ecumemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getECUMEM_SHORTNAME() {
        return (EAttribute)getECUMEM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEM_LONGNAME() {
        return (EReference)getECUMEM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEM_DESC() {
        return (EReference)getECUMEM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEM_ADMINDATA() {
        return (EReference)getECUMEM().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEM_PROJECTINFOS() {
        return (EReference)getECUMEM().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEM_MEM() {
        return (EReference)getECUMEM().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEM_PHYSMEM() {
        return (EReference)getECUMEM().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getECUMEM_ID() {
        return (EAttribute)getECUMEM().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getECUMEM_OID() {
        return (EAttribute)getECUMEM().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUMEMCONNECTOR() {
		if (ecumemconnectorEClass == null) {
			ecumemconnectorEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(123);
		}
		return ecumemconnectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getECUMEMCONNECTOR_SHORTNAME() {
        return (EAttribute)getECUMEMCONNECTOR().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMCONNECTOR_LONGNAME() {
        return (EReference)getECUMEMCONNECTOR().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMCONNECTOR_DESC() {
        return (EReference)getECUMEMCONNECTOR().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMCONNECTOR_ADMINDATA() {
        return (EReference)getECUMEMCONNECTOR().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMCONNECTOR_FLASHCLASSS() {
        return (EReference)getECUMEMCONNECTOR().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMCONNECTOR_SESSIONDESCS() {
        return (EReference)getECUMEMCONNECTOR().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMCONNECTOR_IDENTDESCS() {
        return (EReference)getECUMEMCONNECTOR().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMCONNECTOR_ECUMEMREF() {
        return (EReference)getECUMEMCONNECTOR().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMCONNECTOR_LAYERREFS() {
        return (EReference)getECUMEMCONNECTOR().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getECUMEMCONNECTOR_ID() {
        return (EAttribute)getECUMEMCONNECTOR().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getECUMEMCONNECTOR_OID() {
        return (EAttribute)getECUMEMCONNECTOR().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUMEMCONNECTORS() {
		if (ecumemconnectorsEClass == null) {
			ecumemconnectorsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(124);
		}
		return ecumemconnectorsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMCONNECTORS_ECUMEMCONNECTOR() {
        return (EReference)getECUMEMCONNECTORS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUMEMS() {
		if (ecumemsEClass == null) {
			ecumemsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(125);
		}
		return ecumemsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUMEMS_ECUMEM() {
        return (EReference)getECUMEMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUPROXY() {
		if (ecuproxyEClass == null) {
			ecuproxyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(126);
		}
		return ecuproxyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUPROXYREFS() {
		if (ecuproxyrefsEClass == null) {
			ecuproxyrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(127);
		}
		return ecuproxyrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUPROXYREFS_ECUPROXYREF() {
        return (EReference)getECUPROXYREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUSHAREDDATA() {
		if (ecushareddataEClass == null) {
			ecushareddataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(128);
		}
		return ecushareddataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUSHAREDDATA_DIAGVARIABLES() {
        return (EReference)getECUSHAREDDATA().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUSHAREDDATA_VARIABLEGROUPS() {
        return (EReference)getECUSHAREDDATA().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUSHAREDDATAREF() {
		if (ecushareddatarefEClass == null) {
			ecushareddatarefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(129);
		}
		return ecushareddatarefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUSHAREDDATAS() {
		if (ecushareddatasEClass == null) {
			ecushareddatasEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(130);
		}
		return ecushareddatasEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUSHAREDDATAS_ECUSHAREDDATA() {
        return (EReference)getECUSHAREDDATAS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUVARIANT() {
		if (ecuvariantEClass == null) {
			ecuvariantEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(131);
		}
		return ecuvariantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANT_DIAGVARIABLES() {
        return (EReference)getECUVARIANT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANT_VARIABLEGROUPS() {
        return (EReference)getECUVARIANT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANT_ECUVARIANTPATTERNS() {
        return (EReference)getECUVARIANT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANT_AUTMETHODS() {
        return (EReference)getECUVARIANT().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANT_DYNDEFINEDSPEC() {
        return (EReference)getECUVARIANT().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANT_ACCESSLEVELS() {
        return (EReference)getECUVARIANT().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANT_PARENTREFS() {
        return (EReference)getECUVARIANT().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUVARIANTPATTERN() {
		if (ecuvariantpatternEClass == null) {
			ecuvariantpatternEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(132);
		}
		return ecuvariantpatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANTPATTERN_MATCHINGPARAMETERS() {
        return (EReference)getECUVARIANTPATTERN().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUVARIANTPATTERNS() {
		if (ecuvariantpatternsEClass == null) {
			ecuvariantpatternsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(133);
		}
		return ecuvariantpatternsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANTPATTERNS_ECUVARIANTPATTERN() {
        return (EReference)getECUVARIANTPATTERNS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getECUVARIANTS() {
		if (ecuvariantsEClass == null) {
			ecuvariantsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(134);
		}
		return ecuvariantsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getECUVARIANTS_ECUVARIANT() {
        return (EReference)getECUVARIANTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getENCRYPTCOMPRESSMETHOD() {
		if (encryptcompressmethodEClass == null) {
			encryptcompressmethodEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(137);
		}
		return encryptcompressmethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getENCRYPTCOMPRESSMETHOD_Value() {
        return (EAttribute)getENCRYPTCOMPRESSMETHOD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getENCRYPTCOMPRESSMETHOD_TYPE() {
        return (EAttribute)getENCRYPTCOMPRESSMETHOD().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getENDOFPDUFIELD() {
		if (endofpdufieldEClass == null) {
			endofpdufieldEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(140);
		}
		return endofpdufieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getENDOFPDUFIELD_MAXNUMBEROFITEMS() {
        return (EAttribute)getENDOFPDUFIELD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getENDOFPDUFIELD_MINNUMBEROFITEMS() {
        return (EAttribute)getENDOFPDUFIELD().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getENDOFPDUFIELDS() {
		if (endofpdufieldsEClass == null) {
			endofpdufieldsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(141);
		}
		return endofpdufieldsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getENDOFPDUFIELDS_ENDOFPDUFIELD() {
        return (EReference)getENDOFPDUFIELDS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getENVDATA() {
		if (envdataEClass == null) {
			envdataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(142);
		}
		return envdataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getENVDATA_ALLVALUE() {
        return (EReference)getENVDATA().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getENVDATA_DTCVALUES() {
        return (EReference)getENVDATA().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getENVDATADESC() {
		if (envdatadescEClass == null) {
			envdatadescEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(143);
		}
		return envdatadescEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getENVDATADESC_PARAMSNREF() {
        return (EReference)getENVDATADESC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getENVDATADESC_ENVDATAS() {
        return (EReference)getENVDATADESC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getENVDATADESCS() {
		if (envdatadescsEClass == null) {
			envdatadescsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(144);
		}
		return envdatadescsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getENVDATADESCS_ENVDATADESC() {
        return (EReference)getENVDATADESCS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getENVDATAS() {
		if (envdatasEClass == null) {
			envdatasEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(145);
		}
		return envdatasEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getENVDATAS_ENVDATAPROXY() {
        return (EAttribute)getENVDATAS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getENVDATAS_ENVDATAREF() {
        return (EReference)getENVDATAS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getENVDATAS_ENVDATA() {
        return (EReference)getENVDATAS().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEXPECTEDIDENT() {
		if (expectedidentEClass == null) {
			expectedidentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(146);
		}
		return expectedidentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEXPECTEDIDENT_SHORTNAME() {
        return (EAttribute)getEXPECTEDIDENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEXPECTEDIDENT_LONGNAME() {
        return (EReference)getEXPECTEDIDENT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEXPECTEDIDENT_DESC() {
        return (EReference)getEXPECTEDIDENT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEXPECTEDIDENT_IDENTVALUES() {
        return (EReference)getEXPECTEDIDENT().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEXPECTEDIDENT_ID() {
        return (EAttribute)getEXPECTEDIDENT().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEXPECTEDIDENT_OID() {
        return (EAttribute)getEXPECTEDIDENT().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEXPECTEDIDENTS() {
		if (expectedidentsEClass == null) {
			expectedidentsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(147);
		}
		return expectedidentsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEXPECTEDIDENTS_EXPECTEDIDENT() {
        return (EReference)getEXPECTEDIDENTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEXTERNALACCESSMETHOD() {
		if (externalaccessmethodEClass == null) {
			externalaccessmethodEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(148);
		}
		return externalaccessmethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEXTERNALACCESSMETHOD_Value() {
        return (EAttribute)getEXTERNALACCESSMETHOD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEXTERNFLASHDATA() {
		if (externflashdataEClass == null) {
			externflashdataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(149);
		}
		return externflashdataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEXTERNFLASHDATA_DATAFILE() {
        return (EReference)getEXTERNFLASHDATA().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFIELD() {
		if (fieldEClass == null) {
			fieldEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(150);
		}
		return fieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFIELD_BASICSTRUCTUREREF() {
        return (EReference)getFIELD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFIELD_ENVDATADESCREF() {
        return (EReference)getFIELD().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFIELD_ISVISIBLE() {
        return (EAttribute)getFIELD().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFILE() {
		if (fileEClass == null) {
			fileEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(151);
		}
		return fileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFILE_Value() {
        return (EAttribute)getFILE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFILE_CREATIONDATE() {
        return (EAttribute)getFILE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFILE_CREATOR() {
        return (EAttribute)getFILE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFILE_MIMETYPE() {
        return (EAttribute)getFILE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFILESType() {
		if (filesTypeEClass == null) {
			filesTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(152);
		}
		return filesTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFILESType_FILE() {
        return (EReference)getFILESType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFILTER() {
		if (filterEClass == null) {
			filterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(153);
		}
		return filterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFILTER_FILTERSTART() {
        return (EAttribute)getFILTER().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFILTERS() {
		if (filtersEClass == null) {
			filtersEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(154);
		}
		return filtersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFILTERS_FILTER() {
        return (EReference)getFILTERS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFLASH() {
		if (flashEClass == null) {
			flashEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(155);
		}
		return flashEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASH_ECUMEMS() {
        return (EReference)getFLASH().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASH_ECUMEMCONNECTORS() {
        return (EReference)getFLASH().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFLASHCLASS() {
		if (flashclassEClass == null) {
			flashclassEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(156);
		}
		return flashclassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFLASHCLASS_SHORTNAME() {
        return (EAttribute)getFLASHCLASS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASHCLASS_LONGNAME() {
        return (EReference)getFLASHCLASS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASHCLASS_DESC() {
        return (EReference)getFLASHCLASS().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFLASHCLASS_ID() {
        return (EAttribute)getFLASHCLASS().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFLASHCLASS_OID() {
        return (EAttribute)getFLASHCLASS().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFLASHCLASSREFS() {
		if (flashclassrefsEClass == null) {
			flashclassrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(157);
		}
		return flashclassrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASHCLASSREFS_FLASHCLASSREF() {
        return (EReference)getFLASHCLASSREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFLASHCLASSS() {
		if (flashclasssEClass == null) {
			flashclasssEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(158);
		}
		return flashclasssEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASHCLASSS_FLASHCLASS() {
        return (EReference)getFLASHCLASSS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFLASHDATA() {
		if (flashdataEClass == null) {
			flashdataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(159);
		}
		return flashdataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFLASHDATA_SHORTNAME() {
        return (EAttribute)getFLASHDATA().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASHDATA_LONGNAME() {
        return (EReference)getFLASHDATA().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASHDATA_DESC() {
        return (EReference)getFLASHDATA().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASHDATA_DATAFORMAT() {
        return (EReference)getFLASHDATA().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASHDATA_ENCRYPTCOMPRESSMETHOD() {
        return (EReference)getFLASHDATA().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFLASHDATA_ID() {
        return (EAttribute)getFLASHDATA().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFLASHDATA_OID() {
        return (EAttribute)getFLASHDATA().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFLASHDATAS() {
		if (flashdatasEClass == null) {
			flashdatasEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(160);
		}
		return flashdatasEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFLASHDATAS_FLASHDATA() {
        return (EReference)getFLASHDATAS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFlow() {
		if (flowEClass == null) {
			flowEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(161);
		}
		return flowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlow_Mixed() {
        return (EAttribute)getFlow().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlow_Group() {
        return (EAttribute)getFlow().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFlow_P() {
        return (EReference)getFlow().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFlow_Ul() {
        return (EReference)getFlow().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFlow_Ol() {
        return (EReference)getFlow().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFlow_Br() {
        return (EReference)getFlow().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFlow_I() {
        return (EReference)getFlow().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFlow_B() {
        return (EReference)getFlow().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFlow_Sub() {
        return (EReference)getFlow().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFlow_Sup() {
        return (EReference)getFlow().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFUNCTCLASS() {
		if (functclassEClass == null) {
			functclassEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(162);
		}
		return functclassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFUNCTCLASS_SHORTNAME() {
        return (EAttribute)getFUNCTCLASS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTCLASS_LONGNAME() {
        return (EReference)getFUNCTCLASS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTCLASS_DESC() {
        return (EReference)getFUNCTCLASS().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTCLASS_ADMINDATA() {
        return (EReference)getFUNCTCLASS().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFUNCTCLASS_ID() {
        return (EAttribute)getFUNCTCLASS().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFUNCTCLASS_OID() {
        return (EAttribute)getFUNCTCLASS().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFUNCTCLASSREFS() {
		if (functclassrefsEClass == null) {
			functclassrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(163);
		}
		return functclassrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTCLASSREFS_FUNCTCLASSREF() {
        return (EReference)getFUNCTCLASSREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFUNCTCLASSS() {
		if (functclasssEClass == null) {
			functclasssEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(164);
		}
		return functclasssEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTCLASSS_FUNCTCLASS() {
        return (EReference)getFUNCTCLASSS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFUNCTIONALGROUP() {
		if (functionalgroupEClass == null) {
			functionalgroupEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(165);
		}
		return functionalgroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTIONALGROUP_DIAGVARIABLES() {
        return (EReference)getFUNCTIONALGROUP().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTIONALGROUP_VARIABLEGROUPS() {
        return (EReference)getFUNCTIONALGROUP().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTIONALGROUP_ACCESSLEVELS() {
        return (EReference)getFUNCTIONALGROUP().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTIONALGROUP_AUTMETHODS() {
        return (EReference)getFUNCTIONALGROUP().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTIONALGROUP_PARENTREFS() {
        return (EReference)getFUNCTIONALGROUP().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFUNCTIONALGROUPREF() {
		if (functionalgrouprefEClass == null) {
			functionalgrouprefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(166);
		}
		return functionalgrouprefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFUNCTIONALGROUPS() {
		if (functionalgroupsEClass == null) {
			functionalgroupsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(167);
		}
		return functionalgroupsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFUNCTIONALGROUPS_FUNCTIONALGROUP() {
        return (EReference)getFUNCTIONALGROUPS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFWCHECKSUM() {
		if (fwchecksumEClass == null) {
			fwchecksumEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(168);
		}
		return fwchecksumEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFWCHECKSUM_Value() {
        return (EAttribute)getFWCHECKSUM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFWCHECKSUM_TYPE() {
        return (EAttribute)getFWCHECKSUM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFWSIGNATURE() {
		if (fwsignatureEClass == null) {
			fwsignatureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(169);
		}
		return fwsignatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFWSIGNATURE_Value() {
        return (EAttribute)getFWSIGNATURE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFWSIGNATURE_TYPE() {
        return (EAttribute)getFWSIGNATURE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGATEWAYLOGICALLINK() {
		if (gatewaylogicallinkEClass == null) {
			gatewaylogicallinkEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(170);
		}
		return gatewaylogicallinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGATEWAYLOGICALLINK_SEMANTIC() {
        return (EAttribute)getGATEWAYLOGICALLINK().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGATEWAYLOGICALLINKREFS() {
		if (gatewaylogicallinkrefsEClass == null) {
			gatewaylogicallinkrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(171);
		}
		return gatewaylogicallinkrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGATEWAYLOGICALLINKREFS_GATEWAYLOGICALLINKREF() {
        return (EReference)getGATEWAYLOGICALLINKREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGLOBALNEGRESPONSE() {
		if (globalnegresponseEClass == null) {
			globalnegresponseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(172);
		}
		return globalnegresponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGLOBALNEGRESPONSES() {
		if (globalnegresponsesEClass == null) {
			globalnegresponsesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(173);
		}
		return globalnegresponsesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGLOBALNEGRESPONSES_GLOBALNEGRESPONSE() {
        return (EReference)getGLOBALNEGRESPONSES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHIERARCHYELEMENT() {
		if (hierarchyelementEClass == null) {
			hierarchyelementEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(174);
		}
		return hierarchyelementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHIERARCHYELEMENT_COMPARAMREFS() {
        return (EReference)getHIERARCHYELEMENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getHIERARCHYELEMENT_IMPORTREFS() {
        return (EReference)getHIERARCHYELEMENT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIDENTDESC() {
		if (identdescEClass == null) {
			identdescEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(175);
		}
		return identdescEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIDENTDESC_DIAGCOMMSNREF() {
        return (EReference)getIDENTDESC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIDENTDESC_IDENTIFSNREF() {
        return (EReference)getIDENTDESC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIDENTDESC_OUTPARAMIFSNREF() {
        return (EReference)getIDENTDESC().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIDENTDESCS() {
		if (identdescsEClass == null) {
			identdescsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(176);
		}
		return identdescsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIDENTDESCS_IDENTDESC() {
        return (EReference)getIDENTDESCS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIDENTVALUE() {
		if (identvalueEClass == null) {
			identvalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(177);
		}
		return identvalueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIDENTVALUE_Value() {
        return (EAttribute)getIDENTVALUE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIDENTVALUE_TYPE() {
        return (EAttribute)getIDENTVALUE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIDENTVALUES() {
		if (identvaluesEClass == null) {
			identvaluesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(178);
		}
		return identvaluesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIDENTVALUES_IDENTVALUE() {
        return (EReference)getIDENTVALUES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIMPORTREFS() {
		if (importrefsEClass == null) {
			importrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(181);
		}
		return importrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIMPORTREFS_IMPORTREF() {
        return (EReference)getIMPORTREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getINFOCOMPONENT() {
		if (infocomponentEClass == null) {
			infocomponentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(182);
		}
		return infocomponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getINFOCOMPONENT_SHORTNAME() {
        return (EAttribute)getINFOCOMPONENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINFOCOMPONENT_LONGNAME() {
        return (EReference)getINFOCOMPONENT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINFOCOMPONENT_DESC() {
        return (EReference)getINFOCOMPONENT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINFOCOMPONENT_MATCHINGCOMPONENTS() {
        return (EReference)getINFOCOMPONENT().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getINFOCOMPONENT_ID() {
        return (EAttribute)getINFOCOMPONENT().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getINFOCOMPONENT_OID() {
        return (EAttribute)getINFOCOMPONENT().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getINFOCOMPONENTREFS() {
		if (infocomponentrefsEClass == null) {
			infocomponentrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(183);
		}
		return infocomponentrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINFOCOMPONENTREFS_INFOCOMPONENTREF() {
        return (EReference)getINFOCOMPONENTREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getINFOCOMPONENTS() {
		if (infocomponentsEClass == null) {
			infocomponentsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(184);
		}
		return infocomponentsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINFOCOMPONENTS_INFOCOMPONENT() {
        return (EReference)getINFOCOMPONENTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInline() {
		if (inlineEClass == null) {
			inlineEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(185);
		}
		return inlineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInline_Mixed() {
        return (EAttribute)getInline().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInline_Inline() {
        return (EAttribute)getInline().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInline_Br() {
        return (EReference)getInline().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInline_I() {
        return (EReference)getInline().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInline_B() {
        return (EReference)getInline().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInline_Sub() {
        return (EReference)getInline().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInline_Sup() {
        return (EReference)getInline().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getINPUTPARAM() {
		if (inputparamEClass == null) {
			inputparamEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(186);
		}
		return inputparamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getINPUTPARAM_SHORTNAME() {
        return (EAttribute)getINPUTPARAM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINPUTPARAM_LONGNAME() {
        return (EReference)getINPUTPARAM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINPUTPARAM_DESC() {
        return (EReference)getINPUTPARAM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getINPUTPARAM_PHYSICALDEFAULTVALUE() {
        return (EAttribute)getINPUTPARAM().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINPUTPARAM_DOPBASEREF() {
        return (EReference)getINPUTPARAM().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getINPUTPARAM_OID() {
        return (EAttribute)getINPUTPARAM().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getINPUTPARAM_SEMANTIC() {
        return (EAttribute)getINPUTPARAM().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getINPUTPARAMS() {
		if (inputparamsEClass == null) {
			inputparamsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(187);
		}
		return inputparamsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINPUTPARAMS_INPUTPARAM() {
        return (EReference)getINPUTPARAMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getINTERNALCONSTR() {
		if (internalconstrEClass == null) {
			internalconstrEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(188);
		}
		return internalconstrEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINTERNALCONSTR_LOWERLIMIT() {
        return (EReference)getINTERNALCONSTR().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINTERNALCONSTR_UPPERLIMIT() {
        return (EReference)getINTERNALCONSTR().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getINTERNALCONSTR_SCALECONSTRS() {
        return (EReference)getINTERNALCONSTR().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getINTERNFLASHDATA() {
		if (internflashdataEClass == null) {
			internflashdataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(189);
		}
		return internflashdataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getINTERNFLASHDATA_DATA() {
        return (EAttribute)getINTERNFLASHDATA().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIType() {
		if (iTypeEClass == null) {
			iTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(192);
		}
		return iTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLAYERREFS() {
		if (layerrefsEClass == null) {
			layerrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(193);
		}
		return layerrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLAYERREFS_LAYERREF() {
        return (EReference)getLAYERREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLEADINGLENGTHINFOTYPE() {
		if (leadinglengthinfotypeEClass == null) {
			leadinglengthinfotypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(194);
		}
		return leadinglengthinfotypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLEADINGLENGTHINFOTYPE_BITLENGTH() {
        return (EAttribute)getLEADINGLENGTHINFOTYPE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLENGTHDESCRIPTOR() {
		if (lengthdescriptorEClass == null) {
			lengthdescriptorEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(195);
		}
		return lengthdescriptorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLENGTHKEY() {
		if (lengthkeyEClass == null) {
			lengthkeyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(196);
		}
		return lengthkeyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLENGTHKEY_DOPREF() {
        return (EReference)getLENGTHKEY().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLENGTHKEY_DOPSNREF() {
        return (EReference)getLENGTHKEY().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLENGTHKEY_ID() {
        return (EAttribute)getLENGTHKEY().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLIMIT() {
		if (limitEClass == null) {
			limitEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(197);
		}
		return limitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLIMIT_Value() {
        return (EAttribute)getLIMIT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLIMIT_INTERVALTYPE() {
        return (EAttribute)getLIMIT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLINKCOMPARAMREF() {
		if (linkcomparamrefEClass == null) {
			linkcomparamrefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(198);
		}
		return linkcomparamrefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLINKCOMPARAMREF_VALUE() {
        return (EAttribute)getLINKCOMPARAMREF().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLINKCOMPARAMREF_DESC() {
        return (EReference)getLINKCOMPARAMREF().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLINKCOMPARAMREF_DOCREF() {
        return (EAttribute)getLINKCOMPARAMREF().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLINKCOMPARAMREF_DOCTYPE() {
        return (EAttribute)getLINKCOMPARAMREF().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLINKCOMPARAMREF_IDREF() {
        return (EAttribute)getLINKCOMPARAMREF().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLINKCOMPARAMREF_REVISION() {
        return (EAttribute)getLINKCOMPARAMREF().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLINKCOMPARAMREFS() {
		if (linkcomparamrefsEClass == null) {
			linkcomparamrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(199);
		}
		return linkcomparamrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLINKCOMPARAMREFS_LINKCOMPARAMREF() {
        return (EReference)getLINKCOMPARAMREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLiType() {
		if (liTypeEClass == null) {
			liTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(200);
		}
		return liTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLOGICALLINK() {
		if (logicallinkEClass == null) {
			logicallinkEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(201);
		}
		return logicallinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLOGICALLINK_SHORTNAME() {
        return (EAttribute)getLOGICALLINK().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINK_LONGNAME() {
        return (EReference)getLOGICALLINK().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINK_DESC() {
        return (EReference)getLOGICALLINK().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINK_GATEWAYLOGICALLINKREFS() {
        return (EReference)getLOGICALLINK().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINK_PHYSICALVEHICLELINKREF() {
        return (EReference)getLOGICALLINK().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINK_PROTOCOLREF() {
        return (EReference)getLOGICALLINK().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINK_FUNCTIONALGROUPREF() {
        return (EReference)getLOGICALLINK().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINK_BASEVARIANTREF() {
        return (EReference)getLOGICALLINK().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINK_ECUPROXYREFS() {
        return (EReference)getLOGICALLINK().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINK_LINKCOMPARAMREFS() {
        return (EReference)getLOGICALLINK().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLOGICALLINK_ID() {
        return (EAttribute)getLOGICALLINK().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLOGICALLINK_OID() {
        return (EAttribute)getLOGICALLINK().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLOGICALLINKREFS() {
		if (logicallinkrefsEClass == null) {
			logicallinkrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(202);
		}
		return logicallinkrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINKREFS_LOGICALLINKREF() {
        return (EReference)getLOGICALLINKREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLOGICALLINKS() {
		if (logicallinksEClass == null) {
			logicallinksEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(203);
		}
		return logicallinksEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLOGICALLINKS_LOGICALLINK() {
        return (EReference)getLOGICALLINKS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMATCHINGCOMPONENT() {
		if (matchingcomponentEClass == null) {
			matchingcomponentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(204);
		}
		return matchingcomponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMATCHINGCOMPONENT_EXPECTEDVALUE() {
        return (EAttribute)getMATCHINGCOMPONENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMATCHINGCOMPONENT_OUTPARAMIFSNREF() {
        return (EReference)getMATCHINGCOMPONENT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMATCHINGCOMPONENT_MULTIPLEECUJOBREF() {
        return (EReference)getMATCHINGCOMPONENT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMATCHINGCOMPONENT_DIAGCOMMREF() {
        return (EReference)getMATCHINGCOMPONENT().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMATCHINGCOMPONENTS() {
		if (matchingcomponentsEClass == null) {
			matchingcomponentsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(205);
		}
		return matchingcomponentsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMATCHINGCOMPONENTS_MATCHINGCOMPONENT() {
        return (EReference)getMATCHINGCOMPONENTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMATCHINGPARAMETER() {
		if (matchingparameterEClass == null) {
			matchingparameterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(206);
		}
		return matchingparameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMATCHINGPARAMETER_EXPECTEDVALUE() {
        return (EAttribute)getMATCHINGPARAMETER().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMATCHINGPARAMETER_DIAGCOMMSNREF() {
        return (EReference)getMATCHINGPARAMETER().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMATCHINGPARAMETER_OUTPARAMIFSNREF() {
        return (EReference)getMATCHINGPARAMETER().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMATCHINGPARAMETERS() {
		if (matchingparametersEClass == null) {
			matchingparametersEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(207);
		}
		return matchingparametersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMATCHINGPARAMETERS_MATCHINGPARAMETER() {
        return (EReference)getMATCHINGPARAMETERS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMATCHINGREQUESTPARAM() {
		if (matchingrequestparamEClass == null) {
			matchingrequestparamEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(208);
		}
		return matchingrequestparamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMATCHINGREQUESTPARAM_REQUESTBYTEPOS() {
        return (EAttribute)getMATCHINGREQUESTPARAM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMATCHINGREQUESTPARAM_BYTELENGTH() {
        return (EAttribute)getMATCHINGREQUESTPARAM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMEM() {
		if (memEClass == null) {
			memEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(209);
		}
		return memEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMEM_SESSIONS() {
        return (EReference)getMEM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMEM_DATABLOCKS() {
        return (EReference)getMEM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMEM_FLASHDATAS() {
        return (EReference)getMEM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMEMBERLOGICALLINK() {
		if (memberlogicallinkEClass == null) {
			memberlogicallinkEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(210);
		}
		return memberlogicallinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMINMAXLENGTHTYPE() {
		if (minmaxlengthtypeEClass == null) {
			minmaxlengthtypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(211);
		}
		return minmaxlengthtypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMINMAXLENGTHTYPE_MAXLENGTH() {
        return (EAttribute)getMINMAXLENGTHTYPE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMINMAXLENGTHTYPE_MINLENGTH() {
        return (EAttribute)getMINMAXLENGTHTYPE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMINMAXLENGTHTYPE_TERMINATION() {
        return (EAttribute)getMINMAXLENGTHTYPE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMODELYEAR() {
		if (modelyearEClass == null) {
			modelyearEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(212);
		}
		return modelyearEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMODIFICATION() {
		if (modificationEClass == null) {
			modificationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(213);
		}
		return modificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMODIFICATION_CHANGE() {
        return (EAttribute)getMODIFICATION().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMODIFICATION_REASON() {
        return (EAttribute)getMODIFICATION().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMODIFICATION1() {
		if (modification1EClass == null) {
			modification1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(214);
		}
		return modification1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMODIFICATION1_CHANGE() {
        return (EAttribute)getMODIFICATION1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMODIFICATION1_REASON() {
        return (EAttribute)getMODIFICATION1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMODIFICATIONS() {
		if (modificationsEClass == null) {
			modificationsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(215);
		}
		return modificationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMODIFICATIONS_MODIFICATION() {
        return (EReference)getMODIFICATIONS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMODIFICATIONS1() {
		if (modifications1EClass == null) {
			modifications1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(216);
		}
		return modifications1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMODIFICATIONS1_MODIFICATION() {
        return (EReference)getMODIFICATIONS1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMULTIPLEECUJOB() {
		if (multipleecujobEClass == null) {
			multipleecujobEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(217);
		}
		return multipleecujobEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMULTIPLEECUJOB_SHORTNAME() {
        return (EAttribute)getMULTIPLEECUJOB().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_LONGNAME() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_DESC() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_ADMINDATA() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_SDGS() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_FUNCTCLASSREFS() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_PROGCODES() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_INPUTPARAMS() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_OUTPUTPARAMS() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_NEGOUTPUTPARAMS() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_DIAGLAYERREFS() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOB_AUDIENCE() {
        return (EReference)getMULTIPLEECUJOB().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMULTIPLEECUJOB_ID() {
        return (EAttribute)getMULTIPLEECUJOB().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMULTIPLEECUJOB_ISEXECUTABLE() {
        return (EAttribute)getMULTIPLEECUJOB().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMULTIPLEECUJOB_ISREDUCEDRESULTENABLED() {
        return (EAttribute)getMULTIPLEECUJOB().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMULTIPLEECUJOB_OID() {
        return (EAttribute)getMULTIPLEECUJOB().getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMULTIPLEECUJOB_SECURITYACCESSLEVEL() {
        return (EAttribute)getMULTIPLEECUJOB().getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMULTIPLEECUJOB_SEMANTIC() {
        return (EAttribute)getMULTIPLEECUJOB().getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMULTIPLEECUJOBS() {
		if (multipleecujobsEClass == null) {
			multipleecujobsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(218);
		}
		return multipleecujobsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOBS_MULTIPLEECUJOB() {
        return (EReference)getMULTIPLEECUJOBS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMULTIPLEECUJOBSPEC() {
		if (multipleecujobspecEClass == null) {
			multipleecujobspecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(219);
		}
		return multipleecujobspecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOBSPEC_MULTIPLEECUJOBS() {
        return (EReference)getMULTIPLEECUJOBSPEC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOBSPEC_DIAGDATADICTIONARYSPEC() {
        return (EReference)getMULTIPLEECUJOBSPEC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMULTIPLEECUJOBSPEC_FUNCTCLASSS() {
        return (EReference)getMULTIPLEECUJOBSPEC().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMUX() {
		if (muxEClass == null) {
			muxEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(220);
		}
		return muxEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMUX_BYTEPOSITION() {
        return (EAttribute)getMUX().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMUX_SWITCHKEY() {
        return (EReference)getMUX().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMUX_DEFAULTCASE() {
        return (EReference)getMUX().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMUX_CASES() {
        return (EReference)getMUX().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMUX_ISVISIBLE() {
        return (EAttribute)getMUX().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMUXS() {
		if (muxsEClass == null) {
			muxsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(221);
		}
		return muxsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMUXS_MUX() {
        return (EReference)getMUXS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNEGOFFSET() {
		if (negoffsetEClass == null) {
			negoffsetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(222);
		}
		return negoffsetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getNEGOFFSET_NEGATIVEOFFSET() {
        return (EAttribute)getNEGOFFSET().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNEGOUTPUTPARAM() {
		if (negoutputparamEClass == null) {
			negoutputparamEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(223);
		}
		return negoutputparamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getNEGOUTPUTPARAM_SHORTNAME() {
        return (EAttribute)getNEGOUTPUTPARAM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNEGOUTPUTPARAM_LONGNAME() {
        return (EReference)getNEGOUTPUTPARAM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNEGOUTPUTPARAM_DESC() {
        return (EReference)getNEGOUTPUTPARAM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNEGOUTPUTPARAM_DOPBASEREF() {
        return (EReference)getNEGOUTPUTPARAM().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNEGOUTPUTPARAMS() {
		if (negoutputparamsEClass == null) {
			negoutputparamsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(224);
		}
		return negoutputparamsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNEGOUTPUTPARAMS_NEGOUTPUTPARAM() {
        return (EReference)getNEGOUTPUTPARAMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNEGRESPONSE() {
		if (negresponseEClass == null) {
			negresponseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(225);
		}
		return negresponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNEGRESPONSEREFS() {
		if (negresponserefsEClass == null) {
			negresponserefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(226);
		}
		return negresponserefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNEGRESPONSEREFS_NEGRESPONSEREF() {
        return (EReference)getNEGRESPONSEREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNEGRESPONSES() {
		if (negresponsesEClass == null) {
			negresponsesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(227);
		}
		return negresponsesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNEGRESPONSES_NEGRESPONSE() {
        return (EReference)getNEGRESPONSES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNOTINHERITEDDIAGCOMM() {
		if (notinheriteddiagcommEClass == null) {
			notinheriteddiagcommEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(228);
		}
		return notinheriteddiagcommEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNOTINHERITEDDIAGCOMM_DIAGCOMMSNREF() {
        return (EReference)getNOTINHERITEDDIAGCOMM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNOTINHERITEDDIAGCOMMS() {
		if (notinheriteddiagcommsEClass == null) {
			notinheriteddiagcommsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(229);
		}
		return notinheriteddiagcommsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNOTINHERITEDDIAGCOMMS_NOTINHERITEDDIAGCOMM() {
        return (EReference)getNOTINHERITEDDIAGCOMMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNOTINHERITEDVARIABLE() {
		if (notinheritedvariableEClass == null) {
			notinheritedvariableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(230);
		}
		return notinheritedvariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNOTINHERITEDVARIABLE_DIAGVARIABLESNREF() {
        return (EReference)getNOTINHERITEDVARIABLE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNOTINHERITEDVARIABLES() {
		if (notinheritedvariablesEClass == null) {
			notinheritedvariablesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(231);
		}
		return notinheritedvariablesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNOTINHERITEDVARIABLES_NOTINHERITEDVARIABLE() {
        return (EReference)getNOTINHERITEDVARIABLES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getODX() {
		if (odxEClass == null) {
			odxEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(232);
		}
		return odxEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getODX_DIAGLAYERCONTAINER() {
        return (EReference)getODX().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getODX_COMPARAMSPEC() {
        return (EReference)getODX().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getODX_VEHICLEINFOSPEC() {
        return (EReference)getODX().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getODX_FLASH() {
        return (EReference)getODX().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getODX_MULTIPLEECUJOBSPEC() {
        return (EReference)getODX().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODX_MODELVERSION() {
        return (EAttribute)getODX().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getODXCATEGORY() {
		if (odxcategoryEClass == null) {
			odxcategoryEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(233);
		}
		return odxcategoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXCATEGORY_SHORTNAME() {
        return (EAttribute)getODXCATEGORY().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getODXCATEGORY_LONGNAME() {
        return (EReference)getODXCATEGORY().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getODXCATEGORY_DESC() {
        return (EReference)getODXCATEGORY().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getODXCATEGORY_ADMINDATA() {
        return (EReference)getODXCATEGORY().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getODXCATEGORY_COMPANYDATAS() {
        return (EReference)getODXCATEGORY().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXCATEGORY_ID() {
        return (EAttribute)getODXCATEGORY().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXCATEGORY_OID() {
        return (EAttribute)getODXCATEGORY().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getODXLINK() {
		if (odxlinkEClass == null) {
			odxlinkEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(234);
		}
		return odxlinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXLINK_DOCREF() {
        return (EAttribute)getODXLINK().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXLINK_DOCTYPE() {
        return (EAttribute)getODXLINK().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXLINK_IDREF() {
        return (EAttribute)getODXLINK().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXLINK_REVISION() {
        return (EAttribute)getODXLINK().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getODXLINK1() {
		if (odxlink1EClass == null) {
			odxlink1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(235);
		}
		return odxlink1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXLINK1_DOCREF() {
        return (EAttribute)getODXLINK1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXLINK1_DOCTYPE() {
        return (EAttribute)getODXLINK1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXLINK1_IDREF() {
        return (EAttribute)getODXLINK1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getODXLINK1_REVISION() {
        return (EAttribute)getODXLINK1().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOEM() {
		if (oemEClass == null) {
			oemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(236);
		}
		return oemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOlType() {
		if (olTypeEClass == null) {
			olTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(237);
		}
		return olTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOlType_Li() {
        return (EReference)getOlType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOUTPUTPARAM() {
		if (outputparamEClass == null) {
			outputparamEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(238);
		}
		return outputparamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOUTPUTPARAM_SHORTNAME() {
        return (EAttribute)getOUTPUTPARAM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOUTPUTPARAM_LONGNAME() {
        return (EReference)getOUTPUTPARAM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOUTPUTPARAM_DESC() {
        return (EReference)getOUTPUTPARAM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOUTPUTPARAM_DOPBASEREF() {
        return (EReference)getOUTPUTPARAM().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOUTPUTPARAM_ID() {
        return (EAttribute)getOUTPUTPARAM().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOUTPUTPARAM_OID() {
        return (EAttribute)getOUTPUTPARAM().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOUTPUTPARAM_SEMANTIC() {
        return (EAttribute)getOUTPUTPARAM().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOUTPUTPARAMS() {
		if (outputparamsEClass == null) {
			outputparamsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(239);
		}
		return outputparamsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOUTPUTPARAMS_OUTPUTPARAM() {
        return (EReference)getOUTPUTPARAMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOWNIDENT() {
		if (ownidentEClass == null) {
			ownidentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(240);
		}
		return ownidentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOWNIDENT_SHORTNAME() {
        return (EAttribute)getOWNIDENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOWNIDENT_LONGNAME() {
        return (EReference)getOWNIDENT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOWNIDENT_DESC() {
        return (EReference)getOWNIDENT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOWNIDENT_IDENTVALUE() {
        return (EReference)getOWNIDENT().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOWNIDENT_ID() {
        return (EAttribute)getOWNIDENT().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOWNIDENT_OID() {
        return (EAttribute)getOWNIDENT().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOWNIDENTS() {
		if (ownidentsEClass == null) {
			ownidentsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(241);
		}
		return ownidentsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getOWNIDENTS_OWNIDENT() {
        return (EReference)getOWNIDENTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getP() {
		if (pEClass == null) {
			pEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(242);
		}
		return pEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getP_Align() {
        return (EAttribute)getP().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPARAM() {
		if (paramEClass == null) {
			paramEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(243);
		}
		return paramEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPARAM_SHORTNAME() {
        return (EAttribute)getPARAM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPARAM_LONGNAME() {
        return (EReference)getPARAM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPARAM_DESC() {
        return (EReference)getPARAM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPARAM_OID() {
        return (EAttribute)getPARAM().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPARAM_SEMANTIC() {
        return (EAttribute)getPARAM().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPARAMLENGTHINFOTYPE() {
		if (paramlengthinfotypeEClass == null) {
			paramlengthinfotypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(244);
		}
		return paramlengthinfotypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPARAMLENGTHINFOTYPE_LENGTHKEYREF() {
        return (EReference)getPARAMLENGTHINFOTYPE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPARAMS() {
		if (paramsEClass == null) {
			paramsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(245);
		}
		return paramsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPARAMS_PARAM() {
        return (EReference)getPARAMS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPARENTREF() {
		if (parentrefEClass == null) {
			parentrefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(246);
		}
		return parentrefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPARENTREF_NOTINHERITEDDIAGCOMMS() {
        return (EReference)getPARENTREF().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPARENTREF_NOTINHERITEDVARIABLES() {
        return (EReference)getPARENTREF().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPARENTREF_DOCREF() {
        return (EAttribute)getPARENTREF().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPARENTREF_DOCTYPE() {
        return (EAttribute)getPARENTREF().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPARENTREF_IDREF() {
        return (EAttribute)getPARENTREF().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPARENTREF_REVISION() {
        return (EAttribute)getPARENTREF().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPARENTREFS() {
		if (parentrefsEClass == null) {
			parentrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(247);
		}
		return parentrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPARENTREFS_PARENTREF() {
        return (EReference)getPARENTREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPHYSCONST() {
		if (physconstEClass == null) {
			physconstEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(248);
		}
		return physconstEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSCONST_PHYSCONSTANTVALUE() {
        return (EAttribute)getPHYSCONST().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSCONST_DOPREF() {
        return (EReference)getPHYSCONST().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSCONST_DOPSNREF() {
        return (EReference)getPHYSCONST().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPHYSICALDIMENSION() {
		if (physicaldimensionEClass == null) {
			physicaldimensionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(251);
		}
		return physicaldimensionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_SHORTNAME() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSICALDIMENSION_LONGNAME() {
        return (EReference)getPHYSICALDIMENSION().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSICALDIMENSION_DESC() {
        return (EReference)getPHYSICALDIMENSION().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_LENGTHEXP() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_MASSEXP() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_TIMEEXP() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_CURRENTEXP() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_TEMPERATUREEXP() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_MOLARAMOUNTEXP() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_LUMINOUSINTENSITYEXP() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_ID() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALDIMENSION_OID() {
        return (EAttribute)getPHYSICALDIMENSION().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPHYSICALDIMENSIONS() {
		if (physicaldimensionsEClass == null) {
			physicaldimensionsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(252);
		}
		return physicaldimensionsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSICALDIMENSIONS_PHYSICALDIMENSION() {
        return (EReference)getPHYSICALDIMENSIONS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPHYSICALTYPE() {
		if (physicaltypeEClass == null) {
			physicaltypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(255);
		}
		return physicaltypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALTYPE_PRECISION() {
        return (EAttribute)getPHYSICALTYPE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALTYPE_BASEDATATYPE() {
        return (EAttribute)getPHYSICALTYPE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALTYPE_DISPLAYRADIX() {
        return (EAttribute)getPHYSICALTYPE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPHYSICALVEHICLELINK() {
		if (physicalvehiclelinkEClass == null) {
			physicalvehiclelinkEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(256);
		}
		return physicalvehiclelinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALVEHICLELINK_SHORTNAME() {
        return (EAttribute)getPHYSICALVEHICLELINK().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSICALVEHICLELINK_LONGNAME() {
        return (EReference)getPHYSICALVEHICLELINK().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSICALVEHICLELINK_DESC() {
        return (EReference)getPHYSICALVEHICLELINK().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSICALVEHICLELINK_VEHICLECONNECTORPINREFS() {
        return (EReference)getPHYSICALVEHICLELINK().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSICALVEHICLELINK_LINKCOMPARAMREFS() {
        return (EReference)getPHYSICALVEHICLELINK().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALVEHICLELINK_ID() {
        return (EAttribute)getPHYSICALVEHICLELINK().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALVEHICLELINK_OID() {
        return (EAttribute)getPHYSICALVEHICLELINK().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSICALVEHICLELINK_TYPE() {
        return (EAttribute)getPHYSICALVEHICLELINK().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPHYSICALVEHICLELINKS() {
		if (physicalvehiclelinksEClass == null) {
			physicalvehiclelinksEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(257);
		}
		return physicalvehiclelinksEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSICALVEHICLELINKS_PHYSICALVEHICLELINK() {
        return (EReference)getPHYSICALVEHICLELINKS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPHYSMEM() {
		if (physmemEClass == null) {
			physmemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(258);
		}
		return physmemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSMEM_SHORTNAME() {
        return (EAttribute)getPHYSMEM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSMEM_LONGNAME() {
        return (EReference)getPHYSMEM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSMEM_DESC() {
        return (EReference)getPHYSMEM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSMEM_PHYSSEGMENTS() {
        return (EReference)getPHYSMEM().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSMEM_ID() {
        return (EAttribute)getPHYSMEM().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSMEM_OID() {
        return (EAttribute)getPHYSMEM().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPHYSSEGMENT() {
		if (physsegmentEClass == null) {
			physsegmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(259);
		}
		return physsegmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSSEGMENT_SHORTNAME() {
        return (EAttribute)getPHYSSEGMENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSSEGMENT_LONGNAME() {
        return (EReference)getPHYSSEGMENT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSSEGMENT_DESC() {
        return (EReference)getPHYSSEGMENT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSSEGMENT_FILLBYTE() {
        return (EAttribute)getPHYSSEGMENT().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSSEGMENT_BLOCKSIZE() {
        return (EAttribute)getPHYSSEGMENT().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSSEGMENT_STARTADDRESS() {
        return (EAttribute)getPHYSSEGMENT().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSSEGMENT_ID() {
        return (EAttribute)getPHYSSEGMENT().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPHYSSEGMENT_OID() {
        return (EAttribute)getPHYSSEGMENT().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPHYSSEGMENTS() {
		if (physsegmentsEClass == null) {
			physsegmentsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(260);
		}
		return physsegmentsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPHYSSEGMENTS_PHYSSEGMENT() {
        return (EReference)getPHYSSEGMENTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPOSITIONABLEPARAM() {
		if (positionableparamEClass == null) {
			positionableparamEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(263);
		}
		return positionableparamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPOSITIONABLEPARAM_BYTEPOSITION() {
        return (EAttribute)getPOSITIONABLEPARAM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPOSITIONABLEPARAM_BITPOSITION() {
        return (EAttribute)getPOSITIONABLEPARAM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPOSOFFSET() {
		if (posoffsetEClass == null) {
			posoffsetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(264);
		}
		return posoffsetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPOSOFFSET_POSITIVEOFFSET() {
        return (EAttribute)getPOSOFFSET().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPOSRESPONSE() {
		if (posresponseEClass == null) {
			posresponseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(265);
		}
		return posresponseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPOSRESPONSEREFS() {
		if (posresponserefsEClass == null) {
			posresponserefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(266);
		}
		return posresponserefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPOSRESPONSEREFS_POSRESPONSEREF() {
        return (EReference)getPOSRESPONSEREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPOSRESPONSES() {
		if (posresponsesEClass == null) {
			posresponsesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(267);
		}
		return posresponsesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPOSRESPONSES_POSRESPONSE() {
        return (EReference)getPOSRESPONSES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROGCODE() {
		if (progcodeEClass == null) {
			progcodeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(268);
		}
		return progcodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROGCODE_CODEFILE() {
        return (EAttribute)getPROGCODE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROGCODE_ENCRYPTION() {
        return (EAttribute)getPROGCODE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROGCODE_SYNTAX() {
        return (EAttribute)getPROGCODE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROGCODE_REVISION() {
        return (EAttribute)getPROGCODE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROGCODE_ENTRYPOINT() {
        return (EAttribute)getPROGCODE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROGCODES() {
		if (progcodesEClass == null) {
			progcodesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(269);
		}
		return progcodesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROGCODES_PROGCODE() {
        return (EReference)getPROGCODES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROJECTIDENT() {
		if (projectidentEClass == null) {
			projectidentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(270);
		}
		return projectidentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROJECTIDENT_NAME() {
        return (EAttribute)getPROJECTIDENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROJECTIDENT_VALUE() {
        return (EAttribute)getPROJECTIDENT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROJECTIDENT_TYPE() {
        return (EAttribute)getPROJECTIDENT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROJECTIDENTS() {
		if (projectidentsEClass == null) {
			projectidentsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(271);
		}
		return projectidentsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROJECTIDENTS_PROJECTIDENT() {
        return (EReference)getPROJECTIDENTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROJECTINFO() {
		if (projectinfoEClass == null) {
			projectinfoEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(272);
		}
		return projectinfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROJECTINFO_ECUFAMILYDESC() {
        return (EAttribute)getPROJECTINFO().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROJECTINFO_MODIFICATIONLETTER() {
        return (EAttribute)getPROJECTINFO().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROJECTINFO_COMPANYDATAREF() {
        return (EReference)getPROJECTINFO().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROJECTINFO_PROJECTIDENTS() {
        return (EReference)getPROJECTINFO().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROJECTINFOS() {
		if (projectinfosEClass == null) {
			projectinfosEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(273);
		}
		return projectinfosEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROJECTINFOS_PROJECTINFO() {
        return (EReference)getPROJECTINFOS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROTOCOL() {
		if (protocolEClass == null) {
			protocolEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(276);
		}
		return protocolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROTOCOL_COMPARAMSPECREF() {
        return (EReference)getPROTOCOL().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROTOCOL_ACCESSLEVELS() {
        return (EReference)getPROTOCOL().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROTOCOL_AUTMETHODS() {
        return (EReference)getPROTOCOL().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROTOCOL_PARENTREFS() {
        return (EReference)getPROTOCOL().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPROTOCOL_TYPE() {
        return (EAttribute)getPROTOCOL().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROTOCOLREF() {
		if (protocolrefEClass == null) {
			protocolrefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(277);
		}
		return protocolrefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROTOCOLS() {
		if (protocolsEClass == null) {
			protocolsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(278);
		}
		return protocolsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROTOCOLS_PROTOCOL() {
        return (EReference)getPROTOCOLS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPROTOCOLSNREFS() {
		if (protocolsnrefsEClass == null) {
			protocolsnrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(279);
		}
		return protocolsnrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPROTOCOLSNREFS_PROTOCOLSNREF() {
        return (EReference)getPROTOCOLSNREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRELATEDDIAGCOMMREF() {
		if (relateddiagcommrefEClass == null) {
			relateddiagcommrefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(282);
		}
		return relateddiagcommrefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRELATEDDIAGCOMMREF_RELATIONTYPE() {
        return (EAttribute)getRELATEDDIAGCOMMREF().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRELATEDDIAGCOMMREF_DOCREF() {
        return (EAttribute)getRELATEDDIAGCOMMREF().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRELATEDDIAGCOMMREF_DOCTYPE() {
        return (EAttribute)getRELATEDDIAGCOMMREF().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRELATEDDIAGCOMMREF_IDREF() {
        return (EAttribute)getRELATEDDIAGCOMMREF().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRELATEDDIAGCOMMREF_REVISION() {
        return (EAttribute)getRELATEDDIAGCOMMREF().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRELATEDDIAGCOMMREFS() {
		if (relateddiagcommrefsEClass == null) {
			relateddiagcommrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(283);
		}
		return relateddiagcommrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRELATEDDIAGCOMMREFS_RELATEDDIAGCOMMREF() {
        return (EReference)getRELATEDDIAGCOMMREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRELATEDDOC() {
		if (relateddocEClass == null) {
			relateddocEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(284);
		}
		return relateddocEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRELATEDDOC_XDOC() {
        return (EReference)getRELATEDDOC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRELATEDDOC_DESC() {
        return (EReference)getRELATEDDOC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRELATEDDOC1() {
		if (relateddoc1EClass == null) {
			relateddoc1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(285);
		}
		return relateddoc1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRELATEDDOC1_XDOC() {
        return (EReference)getRELATEDDOC1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRELATEDDOC1_DESC() {
        return (EReference)getRELATEDDOC1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRELATEDDOCS() {
		if (relateddocsEClass == null) {
			relateddocsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(286);
		}
		return relateddocsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRELATEDDOCS_RELATEDDOC() {
        return (EReference)getRELATEDDOCS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRELATEDDOCS1() {
		if (relateddocs1EClass == null) {
			relateddocs1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(287);
		}
		return relateddocs1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRELATEDDOCS1_RELATEDDOC() {
        return (EReference)getRELATEDDOCS1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getREQUEST() {
		if (requestEClass == null) {
			requestEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(288);
		}
		return requestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getREQUEST_SHORTNAME() {
        return (EAttribute)getREQUEST().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getREQUEST_LONGNAME() {
        return (EReference)getREQUEST().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getREQUEST_DESC() {
        return (EReference)getREQUEST().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getREQUEST_ADMINDATA() {
        return (EReference)getREQUEST().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getREQUEST_PARAMS() {
        return (EReference)getREQUEST().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getREQUEST_ID() {
        return (EAttribute)getREQUEST().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getREQUEST_OID() {
        return (EAttribute)getREQUEST().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getREQUESTS() {
		if (requestsEClass == null) {
			requestsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(289);
		}
		return requestsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getREQUESTS_REQUEST() {
        return (EReference)getREQUESTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRESERVED() {
		if (reservedEClass == null) {
			reservedEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(290);
		}
		return reservedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRESERVED_BITLENGTH() {
        return (EAttribute)getRESERVED().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRESERVED_CODEDVALUE() {
        return (EAttribute)getRESERVED().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRESERVED_DIAGCODEDTYPE() {
        return (EReference)getRESERVED().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRESPONSE() {
		if (responseEClass == null) {
			responseEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(291);
		}
		return responseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRESPONSE_SHORTNAME() {
        return (EAttribute)getRESPONSE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRESPONSE_LONGNAME() {
        return (EReference)getRESPONSE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRESPONSE_DESC() {
        return (EReference)getRESPONSE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRESPONSE_ADMINDATA() {
        return (EReference)getRESPONSE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRESPONSE_PARAMS() {
        return (EReference)getRESPONSE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRESPONSE_ID() {
        return (EAttribute)getRESPONSE().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRESPONSE_OID() {
        return (EAttribute)getRESPONSE().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getROLES() {
		if (rolesEClass == null) {
			rolesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(292);
		}
		return rolesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getROLES_ROLE() {
        return (EAttribute)getROLES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getROLES1() {
		if (roles1EClass == null) {
			roles1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(293);
		}
		return roles1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getROLES1_ROLE() {
        return (EAttribute)getROLES1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSCALECONSTR() {
		if (scaleconstrEClass == null) {
			scaleconstrEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(296);
		}
		return scaleconstrEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSCALECONSTR_SHORTLABEL() {
        return (EReference)getSCALECONSTR().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSCALECONSTR_DESC() {
        return (EReference)getSCALECONSTR().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSCALECONSTR_LOWERLIMIT() {
        return (EReference)getSCALECONSTR().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSCALECONSTR_UPPERLIMIT() {
        return (EReference)getSCALECONSTR().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSCALECONSTR_VALIDITY() {
        return (EAttribute)getSCALECONSTR().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSCALECONSTRS() {
		if (scaleconstrsEClass == null) {
			scaleconstrsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(297);
		}
		return scaleconstrsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSCALECONSTRS_SCALECONSTR() {
        return (EReference)getSCALECONSTRS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSD() {
		if (sdEClass == null) {
			sdEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(298);
		}
		return sdEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSD_Value() {
        return (EAttribute)getSD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSD_SI() {
        return (EAttribute)getSD().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSD_TI() {
        return (EAttribute)getSD().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSD1() {
		if (sd1EClass == null) {
			sd1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(299);
		}
		return sd1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSD1_Value() {
        return (EAttribute)getSD1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSD1_SI() {
        return (EAttribute)getSD1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSD1_TI() {
        return (EAttribute)getSD1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSDG() {
		if (sdgEClass == null) {
			sdgEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(300);
		}
		return sdgEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDG_SDGCAPTION() {
        return (EReference)getSDG().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSDG_Group() {
        return (EAttribute)getSDG().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDG_SDG() {
        return (EReference)getSDG().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDG_SD() {
        return (EReference)getSDG().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSDG1() {
		if (sdg1EClass == null) {
			sdg1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(301);
		}
		return sdg1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDG1_SDGCAPTION() {
        return (EReference)getSDG1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSDG1_Group() {
        return (EAttribute)getSDG1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDG1_SDG() {
        return (EReference)getSDG1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDG1_SD() {
        return (EReference)getSDG1().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSDGCAPTION() {
		if (sdgcaptionEClass == null) {
			sdgcaptionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(302);
		}
		return sdgcaptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSDGCAPTION_SHORTNAME() {
        return (EAttribute)getSDGCAPTION().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDGCAPTION_LONGNAME() {
        return (EReference)getSDGCAPTION().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDGCAPTION_DESC() {
        return (EReference)getSDGCAPTION().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSDGCAPTION_ID() {
        return (EAttribute)getSDGCAPTION().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSDGCAPTION_OID() {
        return (EAttribute)getSDGCAPTION().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSDGCAPTION1() {
		if (sdgcaption1EClass == null) {
			sdgcaption1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(303);
		}
		return sdgcaption1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSDGCAPTION1_SHORTNAME() {
        return (EAttribute)getSDGCAPTION1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDGCAPTION1_LONGNAME() {
        return (EReference)getSDGCAPTION1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDGCAPTION1_DESC() {
        return (EReference)getSDGCAPTION1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSDGCAPTION1_ID() {
        return (EAttribute)getSDGCAPTION1().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSDGCAPTION1_OID() {
        return (EAttribute)getSDGCAPTION1().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSDGS() {
		if (sdgsEClass == null) {
			sdgsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(304);
		}
		return sdgsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDGS_SDG() {
        return (EReference)getSDGS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSDGS1() {
		if (sdgs1EClass == null) {
			sdgs1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(305);
		}
		return sdgs1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSDGS1_SDG() {
        return (EReference)getSDGS1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSECURITY() {
		if (securityEClass == null) {
			securityEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(306);
		}
		return securityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSECURITY_SECURITYMETHOD() {
        return (EReference)getSECURITY().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSECURITY_FWSIGNATURE() {
        return (EReference)getSECURITY().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSECURITY_FWCHECKSUM() {
        return (EReference)getSECURITY().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSECURITY_VALIDITYFOR() {
        return (EReference)getSECURITY().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSECURITYMETHOD() {
		if (securitymethodEClass == null) {
			securitymethodEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(307);
		}
		return securitymethodEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSECURITYMETHOD_Value() {
        return (EAttribute)getSECURITYMETHOD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSECURITYMETHOD_TYPE() {
        return (EAttribute)getSECURITYMETHOD().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSECURITYS() {
		if (securitysEClass == null) {
			securitysEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(308);
		}
		return securitysEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSECURITYS_SECURITY() {
        return (EReference)getSECURITYS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSEGMENT() {
		if (segmentEClass == null) {
			segmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(309);
		}
		return segmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSEGMENT_SHORTNAME() {
        return (EAttribute)getSEGMENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSEGMENT_LONGNAME() {
        return (EReference)getSEGMENT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSEGMENT_DESC() {
        return (EReference)getSEGMENT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSEGMENT_SOURCESTARTADDRESS() {
        return (EAttribute)getSEGMENT().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSEGMENT_COMPRESSEDSIZE() {
        return (EAttribute)getSEGMENT().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSEGMENT_UNCOMPRESSEDSIZE() {
        return (EReference)getSEGMENT().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSEGMENT_SOURCEENDADDRESS() {
        return (EReference)getSEGMENT().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSEGMENT_ID() {
        return (EAttribute)getSEGMENT().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSEGMENT_OID() {
        return (EAttribute)getSEGMENT().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSEGMENTS() {
		if (segmentsEClass == null) {
			segmentsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(310);
		}
		return segmentsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSEGMENTS_SEGMENT() {
        return (EReference)getSEGMENTS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSELECTIONTABLEREFS() {
		if (selectiontablerefsEClass == null) {
			selectiontablerefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(311);
		}
		return selectiontablerefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSELECTIONTABLEREFS_Group() {
        return (EAttribute)getSELECTIONTABLEREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSELECTIONTABLEREFS_SELECTIONTABLEREF() {
        return (EReference)getSELECTIONTABLEREFS().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSELECTIONTABLEREFS_SELECTIONTABLESNREF() {
        return (EReference)getSELECTIONTABLEREFS().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSESSION() {
		if (sessionEClass == null) {
			sessionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(312);
		}
		return sessionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSESSION_SHORTNAME() {
        return (EAttribute)getSESSION().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSION_LONGNAME() {
        return (EReference)getSESSION().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSION_DESC() {
        return (EReference)getSESSION().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSION_EXPECTEDIDENTS() {
        return (EReference)getSESSION().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSION_CHECKSUMS() {
        return (EReference)getSESSION().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSION_SECURITYS() {
        return (EReference)getSESSION().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSION_DATABLOCKREFS() {
        return (EReference)getSESSION().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSION_SDGS() {
        return (EReference)getSESSION().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSESSION_ID() {
        return (EAttribute)getSESSION().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSESSION_OID() {
        return (EAttribute)getSESSION().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSESSIONDESC() {
		if (sessiondescEClass == null) {
			sessiondescEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(313);
		}
		return sessiondescEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSESSIONDESC_SHORTNAME() {
        return (EAttribute)getSESSIONDESC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSIONDESC_LONGNAME() {
        return (EReference)getSESSIONDESC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSIONDESC_DESC() {
        return (EReference)getSESSIONDESC().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSESSIONDESC_PARTNUMBER() {
        return (EAttribute)getSESSIONDESC().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSESSIONDESC_PRIORITY() {
        return (EAttribute)getSESSIONDESC().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSIONDESC_SESSIONSNREF() {
        return (EReference)getSESSIONDESC().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSIONDESC_DIAGCOMMSNREF() {
        return (EReference)getSESSIONDESC().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSIONDESC_FLASHCLASSREFS() {
        return (EReference)getSESSIONDESC().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSIONDESC_SDGS() {
        return (EReference)getSESSIONDESC().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSESSIONDESC_DIRECTION() {
        return (EAttribute)getSESSIONDESC().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSESSIONDESC_OID() {
        return (EAttribute)getSESSIONDESC().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSESSIONDESCS() {
		if (sessiondescsEClass == null) {
			sessiondescsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(314);
		}
		return sessiondescsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSIONDESCS_SESSIONDESC() {
        return (EReference)getSESSIONDESCS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSESSIONS() {
		if (sessionsEClass == null) {
			sessionsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(315);
		}
		return sessionsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSESSIONS_SESSION() {
        return (EReference)getSESSIONS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSINGLEECUJOB() {
		if (singleecujobEClass == null) {
			singleecujobEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(318);
		}
		return singleecujobEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSINGLEECUJOB_PROGCODES() {
        return (EReference)getSINGLEECUJOB().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSINGLEECUJOB_INPUTPARAMS() {
        return (EReference)getSINGLEECUJOB().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSINGLEECUJOB_OUTPUTPARAMS() {
        return (EReference)getSINGLEECUJOB().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSINGLEECUJOB_NEGOUTPUTPARAMS() {
        return (EReference)getSINGLEECUJOB().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSINGLEECUJOB_ISREDUCEDRESULTENABLED() {
        return (EAttribute)getSINGLEECUJOB().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSIZEDEFFILTER() {
		if (sizedeffilterEClass == null) {
			sizedeffilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(319);
		}
		return sizedeffilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSIZEDEFFILTER_FILTERSIZE() {
        return (EAttribute)getSIZEDEFFILTER().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSIZEDEFPHYSSEGMENT() {
		if (sizedefphyssegmentEClass == null) {
			sizedefphyssegmentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(320);
		}
		return sizedefphyssegmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSIZEDEFPHYSSEGMENT_SIZE() {
        return (EAttribute)getSIZEDEFPHYSSEGMENT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSNREF() {
		if (snrefEClass == null) {
			snrefEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(321);
		}
		return snrefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSNREF_SHORTNAME() {
        return (EAttribute)getSNREF().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSOURCEENDADDRESS() {
		if (sourceendaddressEClass == null) {
			sourceendaddressEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(322);
		}
		return sourceendaddressEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSOURCEENDADDRESS_Value() {
        return (EAttribute)getSOURCEENDADDRESS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSPECIALDATA() {
		if (specialdataEClass == null) {
			specialdataEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(323);
		}
		return specialdataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSPECIALDATA1() {
		if (specialdata1EClass == null) {
			specialdata1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(324);
		}
		return specialdata1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSTANDARDLENGTHTYPE() {
		if (standardlengthtypeEClass == null) {
			standardlengthtypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(327);
		}
		return standardlengthtypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSTANDARDLENGTHTYPE_BITLENGTH() {
        return (EAttribute)getSTANDARDLENGTHTYPE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSTANDARDLENGTHTYPE_BITMASK() {
        return (EAttribute)getSTANDARDLENGTHTYPE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSTATICFIELD() {
		if (staticfieldEClass == null) {
			staticfieldEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(328);
		}
		return staticfieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSTATICFIELD_FIXEDNUMBEROFITEMS() {
        return (EAttribute)getSTATICFIELD().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSTATICFIELD_ITEMBYTESIZE() {
        return (EAttribute)getSTATICFIELD().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSTATICFIELDS() {
		if (staticfieldsEClass == null) {
			staticfieldsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(329);
		}
		return staticfieldsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSTATICFIELDS_STATICFIELD() {
        return (EReference)getSTATICFIELDS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSTRUCTURE() {
		if (structureEClass == null) {
			structureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(330);
		}
		return structureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSTRUCTURE_ISVISIBLE() {
        return (EAttribute)getSTRUCTURE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSTRUCTURES() {
		if (structuresEClass == null) {
			structuresEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(331);
		}
		return structuresEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSTRUCTURES_STRUCTURE() {
        return (EReference)getSTRUCTURES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSubType() {
		if (subTypeEClass == null) {
			subTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(332);
		}
		return subTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSUPPORTEDDYNID() {
		if (supporteddynidEClass == null) {
			supporteddynidEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(333);
		}
		return supporteddynidEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSUPPORTEDDYNID_Value() {
        return (EAttribute)getSUPPORTEDDYNID().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSUPPORTEDDYNIDS() {
		if (supporteddynidsEClass == null) {
			supporteddynidsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(334);
		}
		return supporteddynidsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSUPPORTEDDYNIDS_SUPPORTEDDYNID() {
        return (EReference)getSUPPORTEDDYNIDS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSupType() {
		if (supTypeEClass == null) {
			supTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(335);
		}
		return supTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSWITCHKEY() {
		if (switchkeyEClass == null) {
			switchkeyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(336);
		}
		return switchkeyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSWITCHKEY_BYTEPOSITION() {
        return (EAttribute)getSWITCHKEY().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSWITCHKEY_BITPOSITION() {
        return (EAttribute)getSWITCHKEY().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSWITCHKEY_DATAOBJECTPROPREF() {
        return (EReference)getSWITCHKEY().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSWVARIABLE() {
		if (swvariableEClass == null) {
			swvariableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(337);
		}
		return swvariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSWVARIABLE_SHORTNAME() {
        return (EAttribute)getSWVARIABLE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSWVARIABLE_LONGNAME() {
        return (EReference)getSWVARIABLE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSWVARIABLE_DESC() {
        return (EReference)getSWVARIABLE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSWVARIABLE_ORIGIN() {
        return (EAttribute)getSWVARIABLE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSWVARIABLE_OID() {
        return (EAttribute)getSWVARIABLE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSWVARIABLES() {
		if (swvariablesEClass == null) {
			swvariablesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(338);
		}
		return swvariablesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSWVARIABLES_SWVARIABLE() {
        return (EReference)getSWVARIABLES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSYSTEM() {
		if (systemEClass == null) {
			systemEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(339);
		}
		return systemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSYSTEM_DOPREF() {
        return (EReference)getSYSTEM().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSYSTEM_DOPSNREF() {
        return (EReference)getSYSTEM().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSYSTEM_SYSPARAM() {
        return (EAttribute)getSYSTEM().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTABLE() {
		if (tableEClass == null) {
			tableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(340);
		}
		return tableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLE_SHORTNAME() {
        return (EAttribute)getTABLE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLE_LONGNAME() {
        return (EReference)getTABLE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLE_DESC() {
        return (EReference)getTABLE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLE_KEYLABEL() {
        return (EAttribute)getTABLE().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLE_STRUCTLABEL() {
        return (EAttribute)getTABLE().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLE_ADMINDATA() {
        return (EReference)getTABLE().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLE_KEYDOPREF() {
        return (EReference)getTABLE().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLE_ROWWRAPPER() {
        return (EAttribute)getTABLE().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLE_TABLEROWREF() {
        return (EReference)getTABLE().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLE_TABLEROW() {
        return (EReference)getTABLE().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLE_ID() {
        return (EAttribute)getTABLE().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLE_OID() {
        return (EAttribute)getTABLE().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLE_SEMANTIC() {
        return (EAttribute)getTABLE().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTABLEENTRY() {
		if (tableentryEClass == null) {
			tableentryEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(341);
		}
		return tableentryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLEENTRY_TARGET() {
        return (EAttribute)getTABLEENTRY().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLEENTRY_TABLEROWREF() {
        return (EReference)getTABLEENTRY().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTABLEKEY() {
		if (tablekeyEClass == null) {
			tablekeyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(342);
		}
		return tablekeyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLEKEY_TABLEREF() {
        return (EReference)getTABLEKEY().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLEKEY_TABLEROWREF() {
        return (EReference)getTABLEKEY().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLEKEY_ID() {
        return (EAttribute)getTABLEKEY().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTABLEROW() {
		if (tablerowEClass == null) {
			tablerowEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(343);
		}
		return tablerowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLEROW_SHORTNAME() {
        return (EAttribute)getTABLEROW().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLEROW_LONGNAME() {
        return (EReference)getTABLEROW().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLEROW_DESC() {
        return (EReference)getTABLEROW().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLEROW_KEY() {
        return (EAttribute)getTABLEROW().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLEROW_STRUCTUREREF() {
        return (EReference)getTABLEROW().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLEROW_ID() {
        return (EAttribute)getTABLEROW().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTABLEROW_OID() {
        return (EAttribute)getTABLEROW().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTABLES() {
		if (tablesEClass == null) {
			tablesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(344);
		}
		return tablesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLES_TABLE() {
        return (EReference)getTABLES().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTABLESTRUCT() {
		if (tablestructEClass == null) {
			tablestructEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(345);
		}
		return tablestructEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTABLESTRUCT_TABLEKEYREF() {
        return (EReference)getTABLESTRUCT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTARGETADDROFFSET() {
		if (targetaddroffsetEClass == null) {
			targetaddroffsetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(346);
		}
		return targetaddroffsetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTEAMMEMBER() {
		if (teammemberEClass == null) {
			teammemberEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(347);
		}
		return teammemberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_SHORTNAME() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTEAMMEMBER_LONGNAME() {
        return (EReference)getTEAMMEMBER().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTEAMMEMBER_DESC() {
        return (EReference)getTEAMMEMBER().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTEAMMEMBER_ROLES() {
        return (EReference)getTEAMMEMBER().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_DEPARTMENT() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_ADDRESS() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_ZIP() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_CITY() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_PHONE() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_FAX() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_EMAIL() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_ID() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER_OID() {
        return (EAttribute)getTEAMMEMBER().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTEAMMEMBER1() {
		if (teammember1EClass == null) {
			teammember1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(348);
		}
		return teammember1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_SHORTNAME() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTEAMMEMBER1_LONGNAME() {
        return (EReference)getTEAMMEMBER1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTEAMMEMBER1_DESC() {
        return (EReference)getTEAMMEMBER1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTEAMMEMBER1_ROLES() {
        return (EReference)getTEAMMEMBER1().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_DEPARTMENT() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_ADDRESS() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_ZIP() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_CITY() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_PHONE() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_FAX() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_EMAIL() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_ID() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEAMMEMBER1_OID() {
        return (EAttribute)getTEAMMEMBER1().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTEAMMEMBERS() {
		if (teammembersEClass == null) {
			teammembersEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(349);
		}
		return teammembersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTEAMMEMBERS_TEAMMEMBER() {
        return (EReference)getTEAMMEMBERS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTEAMMEMBERS1() {
		if (teammembers1EClass == null) {
			teammembers1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(350);
		}
		return teammembers1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTEAMMEMBERS1_TEAMMEMBER() {
        return (EReference)getTEAMMEMBERS1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTEXT() {
		if (textEClass == null) {
			textEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(353);
		}
		return textEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEXT_Value() {
        return (EAttribute)getTEXT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEXT_TI() {
        return (EAttribute)getTEXT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTEXT1() {
		if (text1EClass == null) {
			text1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(354);
		}
		return text1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEXT1_Value() {
        return (EAttribute)getTEXT1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTEXT1_TI() {
        return (EAttribute)getTEXT1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUlType() {
		if (ulTypeEClass == null) {
			ulTypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(355);
		}
		return ulTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUlType_Li() {
        return (EReference)getUlType().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUNCOMPRESSEDSIZE() {
		if (uncompressedsizeEClass == null) {
			uncompressedsizeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(356);
		}
		return uncompressedsizeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNCOMPRESSEDSIZE_Value() {
        return (EAttribute)getUNCOMPRESSEDSIZE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUNIONVALUE() {
		if (unionvalueEClass == null) {
			unionvalueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(357);
		}
		return unionvalueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUNIT() {
		if (unitEClass == null) {
			unitEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(358);
		}
		return unitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNIT_SHORTNAME() {
        return (EAttribute)getUNIT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNIT_LONGNAME() {
        return (EReference)getUNIT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNIT_DESC() {
        return (EReference)getUNIT().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNIT_DISPLAYNAME() {
        return (EAttribute)getUNIT().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNIT_FACTORSITOUNIT() {
        return (EAttribute)getUNIT().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNIT_OFFSETSITOUNIT() {
        return (EAttribute)getUNIT().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNIT_PHYSICALDIMENSIONREF() {
        return (EReference)getUNIT().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNIT_ID() {
        return (EAttribute)getUNIT().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNIT_OID() {
        return (EAttribute)getUNIT().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUNITGROUP() {
		if (unitgroupEClass == null) {
			unitgroupEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(359);
		}
		return unitgroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNITGROUP_SHORTNAME() {
        return (EAttribute)getUNITGROUP().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITGROUP_LONGNAME() {
        return (EReference)getUNITGROUP().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITGROUP_DESC() {
        return (EReference)getUNITGROUP().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNITGROUP_CATEGORY() {
        return (EAttribute)getUNITGROUP().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITGROUP_UNITREFS() {
        return (EReference)getUNITGROUP().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUNITGROUP_OID() {
        return (EAttribute)getUNITGROUP().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUNITGROUPS() {
		if (unitgroupsEClass == null) {
			unitgroupsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(362);
		}
		return unitgroupsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITGROUPS_UNITGROUP() {
        return (EReference)getUNITGROUPS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUNITREFS() {
		if (unitrefsEClass == null) {
			unitrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(363);
		}
		return unitrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITREFS_UNITREF() {
        return (EReference)getUNITREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUNITS() {
		if (unitsEClass == null) {
			unitsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(364);
		}
		return unitsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITS_UNIT() {
        return (EReference)getUNITS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUNITSPEC() {
		if (unitspecEClass == null) {
			unitspecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(365);
		}
		return unitspecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITSPEC_ADMINDATA() {
        return (EReference)getUNITSPEC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITSPEC_UNITGROUPS() {
        return (EReference)getUNITSPEC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITSPEC_UNITS() {
        return (EReference)getUNITSPEC().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUNITSPEC_PHYSICALDIMENSIONS() {
        return (EReference)getUNITSPEC().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getV() {
		if (vEClass == null) {
			vEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(368);
		}
		return vEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getV_Value() {
        return (EAttribute)getV().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVALIDITYFOR() {
		if (validityforEClass == null) {
			validityforEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(369);
		}
		return validityforEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVALIDITYFOR_Value() {
        return (EAttribute)getVALIDITYFOR().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVALIDITYFOR_TYPE() {
        return (EAttribute)getVALIDITYFOR().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVALUE() {
		if (valueEClass == null) {
			valueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(372);
		}
		return valueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVALUE_PHYSICALDEFAULTVALUE() {
        return (EAttribute)getVALUE().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVALUE_DOPREF() {
        return (EReference)getVALUE().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVALUE_DOPSNREF() {
        return (EReference)getVALUE().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVARIABLEGROUP() {
		if (variablegroupEClass == null) {
			variablegroupEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(373);
		}
		return variablegroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVARIABLEGROUP_SHORTNAME() {
        return (EAttribute)getVARIABLEGROUP().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVARIABLEGROUP_LONGNAME() {
        return (EReference)getVARIABLEGROUP().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVARIABLEGROUP_DESC() {
        return (EReference)getVARIABLEGROUP().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVARIABLEGROUP_ID() {
        return (EAttribute)getVARIABLEGROUP().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVARIABLEGROUP_OID() {
        return (EAttribute)getVARIABLEGROUP().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVARIABLEGROUPS() {
		if (variablegroupsEClass == null) {
			variablegroupsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(374);
		}
		return variablegroupsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVARIABLEGROUPS_VARIABLEGROUP() {
        return (EReference)getVARIABLEGROUPS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLECONNECTOR() {
		if (vehicleconnectorEClass == null) {
			vehicleconnectorEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(375);
		}
		return vehicleconnectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVEHICLECONNECTOR_SHORTNAME() {
        return (EAttribute)getVEHICLECONNECTOR().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLECONNECTOR_LONGNAME() {
        return (EReference)getVEHICLECONNECTOR().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLECONNECTOR_DESC() {
        return (EReference)getVEHICLECONNECTOR().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLECONNECTOR_VEHICLECONNECTORPINS() {
        return (EReference)getVEHICLECONNECTOR().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVEHICLECONNECTOR_OID() {
        return (EAttribute)getVEHICLECONNECTOR().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLECONNECTORPIN() {
		if (vehicleconnectorpinEClass == null) {
			vehicleconnectorpinEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(376);
		}
		return vehicleconnectorpinEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVEHICLECONNECTORPIN_SHORTNAME() {
        return (EAttribute)getVEHICLECONNECTORPIN().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLECONNECTORPIN_LONGNAME() {
        return (EReference)getVEHICLECONNECTORPIN().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLECONNECTORPIN_DESC() {
        return (EReference)getVEHICLECONNECTORPIN().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVEHICLECONNECTORPIN_PINNUMBER() {
        return (EAttribute)getVEHICLECONNECTORPIN().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVEHICLECONNECTORPIN_ID() {
        return (EAttribute)getVEHICLECONNECTORPIN().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVEHICLECONNECTORPIN_OID() {
        return (EAttribute)getVEHICLECONNECTORPIN().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVEHICLECONNECTORPIN_TYPE() {
        return (EAttribute)getVEHICLECONNECTORPIN().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLECONNECTORPINREFS() {
		if (vehicleconnectorpinrefsEClass == null) {
			vehicleconnectorpinrefsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(377);
		}
		return vehicleconnectorpinrefsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLECONNECTORPINREFS_VEHICLECONNECTORPINREF() {
        return (EReference)getVEHICLECONNECTORPINREFS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLECONNECTORPINS() {
		if (vehicleconnectorpinsEClass == null) {
			vehicleconnectorpinsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(378);
		}
		return vehicleconnectorpinsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLECONNECTORPINS_VEHICLECONNECTORPIN() {
        return (EReference)getVEHICLECONNECTORPINS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLECONNECTORS() {
		if (vehicleconnectorsEClass == null) {
			vehicleconnectorsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(379);
		}
		return vehicleconnectorsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLECONNECTORS_VEHICLECONNECTOR() {
        return (EReference)getVEHICLECONNECTORS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLEINFORMATION() {
		if (vehicleinformationEClass == null) {
			vehicleinformationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(380);
		}
		return vehicleinformationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVEHICLEINFORMATION_SHORTNAME() {
        return (EAttribute)getVEHICLEINFORMATION().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFORMATION_LONGNAME() {
        return (EReference)getVEHICLEINFORMATION().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFORMATION_DESC() {
        return (EReference)getVEHICLEINFORMATION().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFORMATION_INFOCOMPONENTREFS() {
        return (EReference)getVEHICLEINFORMATION().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFORMATION_VEHICLECONNECTORS() {
        return (EReference)getVEHICLEINFORMATION().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFORMATION_LOGICALLINKS() {
        return (EReference)getVEHICLEINFORMATION().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFORMATION_ECUGROUPS() {
        return (EReference)getVEHICLEINFORMATION().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFORMATION_PHYSICALVEHICLELINKS() {
        return (EReference)getVEHICLEINFORMATION().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVEHICLEINFORMATION_OID() {
        return (EAttribute)getVEHICLEINFORMATION().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLEINFORMATIONS() {
		if (vehicleinformationsEClass == null) {
			vehicleinformationsEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(381);
		}
		return vehicleinformationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFORMATIONS_VEHICLEINFORMATION() {
        return (EReference)getVEHICLEINFORMATIONS().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLEINFOSPEC() {
		if (vehicleinfospecEClass == null) {
			vehicleinfospecEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(382);
		}
		return vehicleinfospecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFOSPEC_INFOCOMPONENTS() {
        return (EReference)getVEHICLEINFOSPEC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVEHICLEINFOSPEC_VEHICLEINFORMATIONS() {
        return (EReference)getVEHICLEINFOSPEC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLEMODEL() {
		if (vehiclemodelEClass == null) {
			vehiclemodelEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(383);
		}
		return vehiclemodelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVEHICLETYPE() {
		if (vehicletypeEClass == null) {
			vehicletypeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(384);
		}
		return vehicletypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVT() {
		if (vtEClass == null) {
			vtEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(385);
		}
		return vtEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVT_Value() {
        return (EAttribute)getVT().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVT_TI() {
        return (EAttribute)getVT().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getXDOC() {
		if (xdocEClass == null) {
			xdocEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(386);
		}
		return xdocEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC_SHORTNAME() {
        return (EAttribute)getXDOC().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getXDOC_LONGNAME() {
        return (EReference)getXDOC().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getXDOC_DESC() {
        return (EReference)getXDOC().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC_NUMBER() {
        return (EAttribute)getXDOC().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC_STATE() {
        return (EAttribute)getXDOC().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC_DATE() {
        return (EAttribute)getXDOC().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC_PUBLISHER() {
        return (EAttribute)getXDOC().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC_URL() {
        return (EAttribute)getXDOC().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC_POSITION() {
        return (EAttribute)getXDOC().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getXDOC1() {
		if (xdoc1EClass == null) {
			xdoc1EClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(387);
		}
		return xdoc1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC1_SHORTNAME() {
        return (EAttribute)getXDOC1().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getXDOC1_LONGNAME() {
        return (EReference)getXDOC1().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getXDOC1_DESC() {
        return (EReference)getXDOC1().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC1_NUMBER() {
        return (EAttribute)getXDOC1().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC1_STATE() {
        return (EAttribute)getXDOC1().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC1_DATE() {
        return (EAttribute)getXDOC1().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC1_PUBLISHER() {
        return (EAttribute)getXDOC1().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC1_URL() {
        return (EAttribute)getXDOC1().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getXDOC1_POSITION() {
        return (EAttribute)getXDOC1().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getADDRESSING() {
		if (addressingEEnum == null) {
			addressingEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(6);
		}
		return addressingEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getAlignType() {
		if (alignTypeEEnum == null) {
			alignTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(10);
		}
		return alignTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getCOMMRELATIONVALUETYPE() {
		if (commrelationvaluetypeEEnum == null) {
			commrelationvaluetypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(31);
		}
		return commrelationvaluetypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getCOMPUCATEGORY() {
		if (compucategoryEEnum == null) {
			compucategoryEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(53);
		}
		return compucategoryEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDATAFORMATSELECTION() {
		if (dataformatselectionEEnum == null) {
			dataformatselectionEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(71);
		}
		return dataformatselectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDATATYPE() {
		if (datatypeEEnum == null) {
			datatypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(75);
		}
		return datatypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDIAGCLASSTYPE() {
		if (diagclasstypeEEnum == null) {
			diagclasstypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(81);
		}
		return diagclasstypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDIRECTION() {
		if (directionEEnum == null) {
			directionEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(93);
		}
		return directionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDOCTYPE() {
		if (doctypeEEnum == null) {
			doctypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(99);
		}
		return doctypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDOCTYPE1() {
		if (doctype1EEnum == null) {
			doctype1EEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(100);
		}
		return doctype1EEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getENCODING() {
		if (encodingEEnum == null) {
			encodingEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(135);
		}
		return encodingEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getENCRYPTCOMPRESSMETHODTYPE() {
		if (encryptcompressmethodtypeEEnum == null) {
			encryptcompressmethodtypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(138);
		}
		return encryptcompressmethodtypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getIDENTVALUETYPE() {
		if (identvaluetypeEEnum == null) {
			identvaluetypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(179);
		}
		return identvaluetypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getINTERVALTYPE() {
		if (intervaltypeEEnum == null) {
			intervaltypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(190);
		}
		return intervaltypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getPHYSICALDATATYPE() {
		if (physicaldatatypeEEnum == null) {
			physicaldatatypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(249);
		}
		return physicaldatatypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getPHYSICALLINKTYPE() {
		if (physicallinktypeEEnum == null) {
			physicallinktypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(253);
		}
		return physicallinktypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getPINTYPE() {
		if (pintypeEEnum == null) {
			pintypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(261);
		}
		return pintypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getPROJIDENTTYPE() {
		if (projidenttypeEEnum == null) {
			projidenttypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(274);
		}
		return projidenttypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getRADIX() {
		if (radixEEnum == null) {
			radixEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(280);
		}
		return radixEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getROWFRAGMENT() {
		if (rowfragmentEEnum == null) {
			rowfragmentEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(294);
		}
		return rowfragmentEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSESSIONSUBELEMTYPE() {
		if (sessionsubelemtypeEEnum == null) {
			sessionsubelemtypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(316);
		}
		return sessionsubelemtypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSTANDARDISATIONLEVEL() {
		if (standardisationlevelEEnum == null) {
			standardisationlevelEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(325);
		}
		return standardisationlevelEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getTERMINATION() {
		if (terminationEEnum == null) {
			terminationEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(351);
		}
		return terminationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getUNITGROUPCATEGORY() {
		if (unitgroupcategoryEEnum == null) {
			unitgroupcategoryEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(360);
		}
		return unitgroupcategoryEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getUPDSTATUS() {
		if (updstatusEEnum == null) {
			updstatusEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(366);
		}
		return updstatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getVALIDTYPE() {
		if (validtypeEEnum == null) {
			validtypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(370);
		}
		return validtypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getADDRESSINGObject() {
		if (addressingObjectEDataType == null) {
			addressingObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(7);
		}
		return addressingObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getAlignTypeObject() {
		if (alignTypeObjectEDataType == null) {
			alignTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(11);
		}
		return alignTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getCOMMRELATIONVALUETYPEObject() {
		if (commrelationvaluetypeObjectEDataType == null) {
			commrelationvaluetypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(32);
		}
		return commrelationvaluetypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getCOMPUCATEGORYObject() {
		if (compucategoryObjectEDataType == null) {
			compucategoryObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(54);
		}
		return compucategoryObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDATAFORMATSELECTIONObject() {
		if (dataformatselectionObjectEDataType == null) {
			dataformatselectionObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(72);
		}
		return dataformatselectionObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDATATYPEObject() {
		if (datatypeObjectEDataType == null) {
			datatypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(76);
		}
		return datatypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDIAGCLASSTYPEObject() {
		if (diagclasstypeObjectEDataType == null) {
			diagclasstypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(82);
		}
		return diagclasstypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDIRECTIONObject() {
		if (directionObjectEDataType == null) {
			directionObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(94);
		}
		return directionObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDOCTYPEObject() {
		if (doctypeObjectEDataType == null) {
			doctypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(101);
		}
		return doctypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDOCTYPEObject1() {
		if (doctypeObject1EDataType == null) {
			doctypeObject1EDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(102);
		}
		return doctypeObject1EDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getENCODINGObject() {
		if (encodingObjectEDataType == null) {
			encodingObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(136);
		}
		return encodingObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getENCRYPTCOMPRESSMETHODTYPEObject() {
		if (encryptcompressmethodtypeObjectEDataType == null) {
			encryptcompressmethodtypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(139);
		}
		return encryptcompressmethodtypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getIDENTVALUETYPEObject() {
		if (identvaluetypeObjectEDataType == null) {
			identvaluetypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(180);
		}
		return identvaluetypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getINTERVALTYPEObject() {
		if (intervaltypeObjectEDataType == null) {
			intervaltypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(191);
		}
		return intervaltypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getPHYSICALDATATYPEObject() {
		if (physicaldatatypeObjectEDataType == null) {
			physicaldatatypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(250);
		}
		return physicaldatatypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getPHYSICALLINKTYPEObject() {
		if (physicallinktypeObjectEDataType == null) {
			physicallinktypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(254);
		}
		return physicallinktypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getPINTYPEObject() {
		if (pintypeObjectEDataType == null) {
			pintypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(262);
		}
		return pintypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getPROJIDENTTYPEObject() {
		if (projidenttypeObjectEDataType == null) {
			projidenttypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(275);
		}
		return projidenttypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getRADIXObject() {
		if (radixObjectEDataType == null) {
			radixObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(281);
		}
		return radixObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getROWFRAGMENTObject() {
		if (rowfragmentObjectEDataType == null) {
			rowfragmentObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(295);
		}
		return rowfragmentObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getSESSIONSUBELEMTYPEObject() {
		if (sessionsubelemtypeObjectEDataType == null) {
			sessionsubelemtypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(317);
		}
		return sessionsubelemtypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getSTANDARDISATIONLEVELObject() {
		if (standardisationlevelObjectEDataType == null) {
			standardisationlevelObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(326);
		}
		return standardisationlevelObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getTERMINATIONObject() {
		if (terminationObjectEDataType == null) {
			terminationObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(352);
		}
		return terminationObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getUNITGROUPCATEGORYObject() {
		if (unitgroupcategoryObjectEDataType == null) {
			unitgroupcategoryObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(361);
		}
		return unitgroupcategoryObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getUPDSTATUSObject() {
		if (updstatusObjectEDataType == null) {
			updstatusObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(367);
		}
		return updstatusObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getVALIDTYPEObject() {
		if (validtypeObjectEDataType == null) {
			validtypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(OdxXhtmlPackage.eNS_URI).getEClassifiers().get(371);
		}
		return validtypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OdxXhtmlFactory getOdxXhtmlFactory() {
		return (OdxXhtmlFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isLoaded = false;

	/**
	 * Laods the package and any sub-packages from their serialized form.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void loadPackage() {
		if (isLoaded) return;
		isLoaded = true;

		URL url = getClass().getResource(packageFilename);
		if (url == null) {
			throw new RuntimeException("Missing serialized package: " + packageFilename);
		}
		URI uri = URI.createURI(url.toString());
		Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
		try {
			resource.load(null);
		}
		catch (IOException exception) {
			throw new WrappedException(exception);
		}
		initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
		createResource(eNS_URI);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isFixed = false;

	/**
	 * Fixes up the loaded package, to make it appear as if it had been programmatically built.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fixPackageContents() {
		if (isFixed) return;
		isFixed = true;
		fixEClassifiers();
	}

	/**
	 * Sets the instance class on the given classifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void fixInstanceClass(EClassifier eClassifier) {
		if (eClassifier.getInstanceClassName() == null) {
			eClassifier.setInstanceClassName("OdxXhtml." + eClassifier.getName());
			setGeneratedClassName(eClassifier);
		}
	}

} //OdxXhtmlPackageImpl
