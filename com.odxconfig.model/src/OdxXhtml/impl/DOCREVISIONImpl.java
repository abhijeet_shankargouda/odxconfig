/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYREVISIONINFOS;
import OdxXhtml.DOCREVISION;
import OdxXhtml.MODIFICATIONS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DOCREVISION</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DOCREVISIONImpl#getTEAMMEMBERREF <em>TEAMMEMBERREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DOCREVISIONImpl#getREVISIONLABEL <em>REVISIONLABEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.DOCREVISIONImpl#getSTATE <em>STATE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DOCREVISIONImpl#getDATE <em>DATE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DOCREVISIONImpl#getTOOL <em>TOOL</em>}</li>
 *   <li>{@link OdxXhtml.impl.DOCREVISIONImpl#getCOMPANYREVISIONINFOS <em>COMPANYREVISIONINFOS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DOCREVISIONImpl#getMODIFICATIONS <em>MODIFICATIONS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DOCREVISIONImpl extends MinimalEObjectImpl.Container implements DOCREVISION {
	/**
	 * The cached value of the '{@link #getTEAMMEMBERREF() <em>TEAMMEMBERREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTEAMMEMBERREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK tEAMMEMBERREF;

	/**
	 * The default value of the '{@link #getREVISIONLABEL() <em>REVISIONLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISIONLABEL()
	 * @generated
	 * @ordered
	 */
	protected static final String REVISIONLABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getREVISIONLABEL() <em>REVISIONLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISIONLABEL()
	 * @generated
	 * @ordered
	 */
	protected String rEVISIONLABEL = REVISIONLABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getSTATE() <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTATE()
	 * @generated
	 * @ordered
	 */
	protected static final String STATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSTATE() <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTATE()
	 * @generated
	 * @ordered
	 */
	protected String sTATE = STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDATE() <em>DATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATE()
	 * @generated
	 * @ordered
	 */
	protected static final XMLGregorianCalendar DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDATE() <em>DATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATE()
	 * @generated
	 * @ordered
	 */
	protected XMLGregorianCalendar dATE = DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTOOL() <em>TOOL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTOOL()
	 * @generated
	 * @ordered
	 */
	protected static final String TOOL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTOOL() <em>TOOL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTOOL()
	 * @generated
	 * @ordered
	 */
	protected String tOOL = TOOL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCOMPANYREVISIONINFOS() <em>COMPANYREVISIONINFOS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYREVISIONINFOS()
	 * @generated
	 * @ordered
	 */
	protected COMPANYREVISIONINFOS cOMPANYREVISIONINFOS;

	/**
	 * The cached value of the '{@link #getMODIFICATIONS() <em>MODIFICATIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMODIFICATIONS()
	 * @generated
	 * @ordered
	 */
	protected MODIFICATIONS mODIFICATIONS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DOCREVISIONImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDOCREVISION();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getTEAMMEMBERREF() {
		return tEAMMEMBERREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTEAMMEMBERREF(ODXLINK newTEAMMEMBERREF, NotificationChain msgs) {
		ODXLINK oldTEAMMEMBERREF = tEAMMEMBERREF;
		tEAMMEMBERREF = newTEAMMEMBERREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__TEAMMEMBERREF, oldTEAMMEMBERREF, newTEAMMEMBERREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTEAMMEMBERREF(ODXLINK newTEAMMEMBERREF) {
		if (newTEAMMEMBERREF != tEAMMEMBERREF) {
			NotificationChain msgs = null;
			if (tEAMMEMBERREF != null)
				msgs = ((InternalEObject)tEAMMEMBERREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DOCREVISION__TEAMMEMBERREF, null, msgs);
			if (newTEAMMEMBERREF != null)
				msgs = ((InternalEObject)newTEAMMEMBERREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DOCREVISION__TEAMMEMBERREF, null, msgs);
			msgs = basicSetTEAMMEMBERREF(newTEAMMEMBERREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__TEAMMEMBERREF, newTEAMMEMBERREF, newTEAMMEMBERREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getREVISIONLABEL() {
		return rEVISIONLABEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREVISIONLABEL(String newREVISIONLABEL) {
		String oldREVISIONLABEL = rEVISIONLABEL;
		rEVISIONLABEL = newREVISIONLABEL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__REVISIONLABEL, oldREVISIONLABEL, rEVISIONLABEL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSTATE() {
		return sTATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSTATE(String newSTATE) {
		String oldSTATE = sTATE;
		sTATE = newSTATE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__STATE, oldSTATE, sTATE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public XMLGregorianCalendar getDATE() {
		return dATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATE(XMLGregorianCalendar newDATE) {
		XMLGregorianCalendar oldDATE = dATE;
		dATE = newDATE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__DATE, oldDATE, dATE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTOOL() {
		return tOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTOOL(String newTOOL) {
		String oldTOOL = tOOL;
		tOOL = newTOOL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__TOOL, oldTOOL, tOOL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYREVISIONINFOS getCOMPANYREVISIONINFOS() {
		return cOMPANYREVISIONINFOS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYREVISIONINFOS(COMPANYREVISIONINFOS newCOMPANYREVISIONINFOS, NotificationChain msgs) {
		COMPANYREVISIONINFOS oldCOMPANYREVISIONINFOS = cOMPANYREVISIONINFOS;
		cOMPANYREVISIONINFOS = newCOMPANYREVISIONINFOS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__COMPANYREVISIONINFOS, oldCOMPANYREVISIONINFOS, newCOMPANYREVISIONINFOS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYREVISIONINFOS(COMPANYREVISIONINFOS newCOMPANYREVISIONINFOS) {
		if (newCOMPANYREVISIONINFOS != cOMPANYREVISIONINFOS) {
			NotificationChain msgs = null;
			if (cOMPANYREVISIONINFOS != null)
				msgs = ((InternalEObject)cOMPANYREVISIONINFOS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DOCREVISION__COMPANYREVISIONINFOS, null, msgs);
			if (newCOMPANYREVISIONINFOS != null)
				msgs = ((InternalEObject)newCOMPANYREVISIONINFOS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DOCREVISION__COMPANYREVISIONINFOS, null, msgs);
			msgs = basicSetCOMPANYREVISIONINFOS(newCOMPANYREVISIONINFOS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__COMPANYREVISIONINFOS, newCOMPANYREVISIONINFOS, newCOMPANYREVISIONINFOS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MODIFICATIONS getMODIFICATIONS() {
		return mODIFICATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMODIFICATIONS(MODIFICATIONS newMODIFICATIONS, NotificationChain msgs) {
		MODIFICATIONS oldMODIFICATIONS = mODIFICATIONS;
		mODIFICATIONS = newMODIFICATIONS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__MODIFICATIONS, oldMODIFICATIONS, newMODIFICATIONS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMODIFICATIONS(MODIFICATIONS newMODIFICATIONS) {
		if (newMODIFICATIONS != mODIFICATIONS) {
			NotificationChain msgs = null;
			if (mODIFICATIONS != null)
				msgs = ((InternalEObject)mODIFICATIONS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DOCREVISION__MODIFICATIONS, null, msgs);
			if (newMODIFICATIONS != null)
				msgs = ((InternalEObject)newMODIFICATIONS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DOCREVISION__MODIFICATIONS, null, msgs);
			msgs = basicSetMODIFICATIONS(newMODIFICATIONS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DOCREVISION__MODIFICATIONS, newMODIFICATIONS, newMODIFICATIONS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISION__TEAMMEMBERREF:
				return basicSetTEAMMEMBERREF(null, msgs);
			case OdxXhtmlPackage.DOCREVISION__COMPANYREVISIONINFOS:
				return basicSetCOMPANYREVISIONINFOS(null, msgs);
			case OdxXhtmlPackage.DOCREVISION__MODIFICATIONS:
				return basicSetMODIFICATIONS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISION__TEAMMEMBERREF:
				return getTEAMMEMBERREF();
			case OdxXhtmlPackage.DOCREVISION__REVISIONLABEL:
				return getREVISIONLABEL();
			case OdxXhtmlPackage.DOCREVISION__STATE:
				return getSTATE();
			case OdxXhtmlPackage.DOCREVISION__DATE:
				return getDATE();
			case OdxXhtmlPackage.DOCREVISION__TOOL:
				return getTOOL();
			case OdxXhtmlPackage.DOCREVISION__COMPANYREVISIONINFOS:
				return getCOMPANYREVISIONINFOS();
			case OdxXhtmlPackage.DOCREVISION__MODIFICATIONS:
				return getMODIFICATIONS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISION__TEAMMEMBERREF:
				setTEAMMEMBERREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.DOCREVISION__REVISIONLABEL:
				setREVISIONLABEL((String)newValue);
				return;
			case OdxXhtmlPackage.DOCREVISION__STATE:
				setSTATE((String)newValue);
				return;
			case OdxXhtmlPackage.DOCREVISION__DATE:
				setDATE((XMLGregorianCalendar)newValue);
				return;
			case OdxXhtmlPackage.DOCREVISION__TOOL:
				setTOOL((String)newValue);
				return;
			case OdxXhtmlPackage.DOCREVISION__COMPANYREVISIONINFOS:
				setCOMPANYREVISIONINFOS((COMPANYREVISIONINFOS)newValue);
				return;
			case OdxXhtmlPackage.DOCREVISION__MODIFICATIONS:
				setMODIFICATIONS((MODIFICATIONS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISION__TEAMMEMBERREF:
				setTEAMMEMBERREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.DOCREVISION__REVISIONLABEL:
				setREVISIONLABEL(REVISIONLABEL_EDEFAULT);
				return;
			case OdxXhtmlPackage.DOCREVISION__STATE:
				setSTATE(STATE_EDEFAULT);
				return;
			case OdxXhtmlPackage.DOCREVISION__DATE:
				setDATE(DATE_EDEFAULT);
				return;
			case OdxXhtmlPackage.DOCREVISION__TOOL:
				setTOOL(TOOL_EDEFAULT);
				return;
			case OdxXhtmlPackage.DOCREVISION__COMPANYREVISIONINFOS:
				setCOMPANYREVISIONINFOS((COMPANYREVISIONINFOS)null);
				return;
			case OdxXhtmlPackage.DOCREVISION__MODIFICATIONS:
				setMODIFICATIONS((MODIFICATIONS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISION__TEAMMEMBERREF:
				return tEAMMEMBERREF != null;
			case OdxXhtmlPackage.DOCREVISION__REVISIONLABEL:
				return REVISIONLABEL_EDEFAULT == null ? rEVISIONLABEL != null : !REVISIONLABEL_EDEFAULT.equals(rEVISIONLABEL);
			case OdxXhtmlPackage.DOCREVISION__STATE:
				return STATE_EDEFAULT == null ? sTATE != null : !STATE_EDEFAULT.equals(sTATE);
			case OdxXhtmlPackage.DOCREVISION__DATE:
				return DATE_EDEFAULT == null ? dATE != null : !DATE_EDEFAULT.equals(dATE);
			case OdxXhtmlPackage.DOCREVISION__TOOL:
				return TOOL_EDEFAULT == null ? tOOL != null : !TOOL_EDEFAULT.equals(tOOL);
			case OdxXhtmlPackage.DOCREVISION__COMPANYREVISIONINFOS:
				return cOMPANYREVISIONINFOS != null;
			case OdxXhtmlPackage.DOCREVISION__MODIFICATIONS:
				return mODIFICATIONS != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (rEVISIONLABEL: ");
		result.append(rEVISIONLABEL);
		result.append(", sTATE: ");
		result.append(sTATE);
		result.append(", dATE: ");
		result.append(dATE);
		result.append(", tOOL: ");
		result.append(tOOL);
		result.append(')');
		return result.toString();
	}

} //DOCREVISIONImpl
