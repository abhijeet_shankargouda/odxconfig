/**
 */
package OdxXhtml.impl;

import OdxXhtml.ACCESSLEVEL;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.EXTERNALACCESSMETHOD;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ACCESSLEVEL</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ACCESSLEVELImpl#getVALUE <em>VALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.ACCESSLEVELImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.ACCESSLEVELImpl#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.ACCESSLEVELImpl#getEXTERNALACCESSMETHOD <em>EXTERNALACCESSMETHOD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ACCESSLEVELImpl extends MinimalEObjectImpl.Container implements ACCESSLEVEL {
	/**
	 * The default value of the '{@link #getVALUE() <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final long VALUE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getVALUE() <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUE()
	 * @generated
	 * @ordered
	 */
	protected long vALUE = VALUE_EDEFAULT;

	/**
	 * This is true if the VALUE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean vALUEESet;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getDIAGCOMMSNREF() <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dIAGCOMMSNREF;

	/**
	 * The cached value of the '{@link #getEXTERNALACCESSMETHOD() <em>EXTERNALACCESSMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEXTERNALACCESSMETHOD()
	 * @generated
	 * @ordered
	 */
	protected EXTERNALACCESSMETHOD eXTERNALACCESSMETHOD;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ACCESSLEVELImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getACCESSLEVEL();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getVALUE() {
		return vALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVALUE(long newVALUE) {
		long oldVALUE = vALUE;
		vALUE = newVALUE;
		boolean oldVALUEESet = vALUEESet;
		vALUEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ACCESSLEVEL__VALUE, oldVALUE, vALUE, !oldVALUEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetVALUE() {
		long oldVALUE = vALUE;
		boolean oldVALUEESet = vALUEESet;
		vALUE = VALUE_EDEFAULT;
		vALUEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.ACCESSLEVEL__VALUE, oldVALUE, VALUE_EDEFAULT, oldVALUEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetVALUE() {
		return vALUEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ACCESSLEVEL__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ACCESSLEVEL__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ACCESSLEVEL__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ACCESSLEVEL__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDIAGCOMMSNREF() {
		return dIAGCOMMSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF, NotificationChain msgs) {
		SNREF oldDIAGCOMMSNREF = dIAGCOMMSNREF;
		dIAGCOMMSNREF = newDIAGCOMMSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ACCESSLEVEL__DIAGCOMMSNREF, oldDIAGCOMMSNREF, newDIAGCOMMSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF) {
		if (newDIAGCOMMSNREF != dIAGCOMMSNREF) {
			NotificationChain msgs = null;
			if (dIAGCOMMSNREF != null)
				msgs = ((InternalEObject)dIAGCOMMSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ACCESSLEVEL__DIAGCOMMSNREF, null, msgs);
			if (newDIAGCOMMSNREF != null)
				msgs = ((InternalEObject)newDIAGCOMMSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ACCESSLEVEL__DIAGCOMMSNREF, null, msgs);
			msgs = basicSetDIAGCOMMSNREF(newDIAGCOMMSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ACCESSLEVEL__DIAGCOMMSNREF, newDIAGCOMMSNREF, newDIAGCOMMSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EXTERNALACCESSMETHOD getEXTERNALACCESSMETHOD() {
		return eXTERNALACCESSMETHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEXTERNALACCESSMETHOD(EXTERNALACCESSMETHOD newEXTERNALACCESSMETHOD, NotificationChain msgs) {
		EXTERNALACCESSMETHOD oldEXTERNALACCESSMETHOD = eXTERNALACCESSMETHOD;
		eXTERNALACCESSMETHOD = newEXTERNALACCESSMETHOD;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ACCESSLEVEL__EXTERNALACCESSMETHOD, oldEXTERNALACCESSMETHOD, newEXTERNALACCESSMETHOD);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEXTERNALACCESSMETHOD(EXTERNALACCESSMETHOD newEXTERNALACCESSMETHOD) {
		if (newEXTERNALACCESSMETHOD != eXTERNALACCESSMETHOD) {
			NotificationChain msgs = null;
			if (eXTERNALACCESSMETHOD != null)
				msgs = ((InternalEObject)eXTERNALACCESSMETHOD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ACCESSLEVEL__EXTERNALACCESSMETHOD, null, msgs);
			if (newEXTERNALACCESSMETHOD != null)
				msgs = ((InternalEObject)newEXTERNALACCESSMETHOD).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ACCESSLEVEL__EXTERNALACCESSMETHOD, null, msgs);
			msgs = basicSetEXTERNALACCESSMETHOD(newEXTERNALACCESSMETHOD, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ACCESSLEVEL__EXTERNALACCESSMETHOD, newEXTERNALACCESSMETHOD, newEXTERNALACCESSMETHOD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVEL__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.ACCESSLEVEL__DIAGCOMMSNREF:
				return basicSetDIAGCOMMSNREF(null, msgs);
			case OdxXhtmlPackage.ACCESSLEVEL__EXTERNALACCESSMETHOD:
				return basicSetEXTERNALACCESSMETHOD(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVEL__VALUE:
				return getVALUE();
			case OdxXhtmlPackage.ACCESSLEVEL__DESC:
				return getDESC();
			case OdxXhtmlPackage.ACCESSLEVEL__DIAGCOMMSNREF:
				return getDIAGCOMMSNREF();
			case OdxXhtmlPackage.ACCESSLEVEL__EXTERNALACCESSMETHOD:
				return getEXTERNALACCESSMETHOD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVEL__VALUE:
				setVALUE((Long)newValue);
				return;
			case OdxXhtmlPackage.ACCESSLEVEL__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.ACCESSLEVEL__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.ACCESSLEVEL__EXTERNALACCESSMETHOD:
				setEXTERNALACCESSMETHOD((EXTERNALACCESSMETHOD)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVEL__VALUE:
				unsetVALUE();
				return;
			case OdxXhtmlPackage.ACCESSLEVEL__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.ACCESSLEVEL__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.ACCESSLEVEL__EXTERNALACCESSMETHOD:
				setEXTERNALACCESSMETHOD((EXTERNALACCESSMETHOD)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVEL__VALUE:
				return isSetVALUE();
			case OdxXhtmlPackage.ACCESSLEVEL__DESC:
				return dESC != null;
			case OdxXhtmlPackage.ACCESSLEVEL__DIAGCOMMSNREF:
				return dIAGCOMMSNREF != null;
			case OdxXhtmlPackage.ACCESSLEVEL__EXTERNALACCESSMETHOD:
				return eXTERNALACCESSMETHOD != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (vALUE: ");
		if (vALUEESet) result.append(vALUE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ACCESSLEVELImpl
