/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TABLESTRUCT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TABLESTRUCT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.TABLESTRUCTImpl#getTABLEKEYREF <em>TABLEKEYREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TABLESTRUCTImpl extends POSITIONABLEPARAMImpl implements TABLESTRUCT {
	/**
	 * The cached value of the '{@link #getTABLEKEYREF() <em>TABLEKEYREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTABLEKEYREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK tABLEKEYREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TABLESTRUCTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getTABLESTRUCT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getTABLEKEYREF() {
		return tABLEKEYREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTABLEKEYREF(ODXLINK newTABLEKEYREF, NotificationChain msgs) {
		ODXLINK oldTABLEKEYREF = tABLEKEYREF;
		tABLEKEYREF = newTABLEKEYREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLESTRUCT__TABLEKEYREF, oldTABLEKEYREF, newTABLEKEYREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTABLEKEYREF(ODXLINK newTABLEKEYREF) {
		if (newTABLEKEYREF != tABLEKEYREF) {
			NotificationChain msgs = null;
			if (tABLEKEYREF != null)
				msgs = ((InternalEObject)tABLEKEYREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLESTRUCT__TABLEKEYREF, null, msgs);
			if (newTABLEKEYREF != null)
				msgs = ((InternalEObject)newTABLEKEYREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLESTRUCT__TABLEKEYREF, null, msgs);
			msgs = basicSetTABLEKEYREF(newTABLEKEYREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLESTRUCT__TABLEKEYREF, newTABLEKEYREF, newTABLEKEYREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLESTRUCT__TABLEKEYREF:
				return basicSetTABLEKEYREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLESTRUCT__TABLEKEYREF:
				return getTABLEKEYREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLESTRUCT__TABLEKEYREF:
				setTABLEKEYREF((ODXLINK)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLESTRUCT__TABLEKEYREF:
				setTABLEKEYREF((ODXLINK)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLESTRUCT__TABLEKEYREF:
				return tABLEKEYREF != null;
		}
		return super.eIsSet(featureID);
	}

} //TABLESTRUCTImpl
