/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT;
import OdxXhtml.VEHICLECONNECTOR;
import OdxXhtml.VEHICLECONNECTORPINS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VEHICLECONNECTOR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORImpl#getVEHICLECONNECTORPINS <em>VEHICLECONNECTORPINS</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VEHICLECONNECTORImpl extends MinimalEObjectImpl.Container implements VEHICLECONNECTOR {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getVEHICLECONNECTORPINS() <em>VEHICLECONNECTORPINS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVEHICLECONNECTORPINS()
	 * @generated
	 * @ordered
	 */
	protected VEHICLECONNECTORPINS vEHICLECONNECTORPINS;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VEHICLECONNECTORImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVEHICLECONNECTOR();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTOR__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTOR__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTOR__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTOR__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTOR__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTOR__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTOR__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTOR__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTOR__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VEHICLECONNECTORPINS getVEHICLECONNECTORPINS() {
		return vEHICLECONNECTORPINS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVEHICLECONNECTORPINS(VEHICLECONNECTORPINS newVEHICLECONNECTORPINS, NotificationChain msgs) {
		VEHICLECONNECTORPINS oldVEHICLECONNECTORPINS = vEHICLECONNECTORPINS;
		vEHICLECONNECTORPINS = newVEHICLECONNECTORPINS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTOR__VEHICLECONNECTORPINS, oldVEHICLECONNECTORPINS, newVEHICLECONNECTORPINS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVEHICLECONNECTORPINS(VEHICLECONNECTORPINS newVEHICLECONNECTORPINS) {
		if (newVEHICLECONNECTORPINS != vEHICLECONNECTORPINS) {
			NotificationChain msgs = null;
			if (vEHICLECONNECTORPINS != null)
				msgs = ((InternalEObject)vEHICLECONNECTORPINS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTOR__VEHICLECONNECTORPINS, null, msgs);
			if (newVEHICLECONNECTORPINS != null)
				msgs = ((InternalEObject)newVEHICLECONNECTORPINS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTOR__VEHICLECONNECTORPINS, null, msgs);
			msgs = basicSetVEHICLECONNECTORPINS(newVEHICLECONNECTORPINS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTOR__VEHICLECONNECTORPINS, newVEHICLECONNECTORPINS, newVEHICLECONNECTORPINS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTOR__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTOR__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.VEHICLECONNECTOR__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.VEHICLECONNECTOR__VEHICLECONNECTORPINS:
				return basicSetVEHICLECONNECTORPINS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTOR__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.VEHICLECONNECTOR__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.VEHICLECONNECTOR__DESC:
				return getDESC();
			case OdxXhtmlPackage.VEHICLECONNECTOR__VEHICLECONNECTORPINS:
				return getVEHICLECONNECTORPINS();
			case OdxXhtmlPackage.VEHICLECONNECTOR__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTOR__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTOR__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTOR__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTOR__VEHICLECONNECTORPINS:
				setVEHICLECONNECTORPINS((VEHICLECONNECTORPINS)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTOR__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTOR__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTOR__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTOR__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTOR__VEHICLECONNECTORPINS:
				setVEHICLECONNECTORPINS((VEHICLECONNECTORPINS)null);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTOR__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTOR__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.VEHICLECONNECTOR__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.VEHICLECONNECTOR__DESC:
				return dESC != null;
			case OdxXhtmlPackage.VEHICLECONNECTOR__VEHICLECONNECTORPINS:
				return vEHICLECONNECTORPINS != null;
			case OdxXhtmlPackage.VEHICLECONNECTOR__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //VEHICLECONNECTORImpl
