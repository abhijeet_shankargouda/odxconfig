/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSCONST;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PHYSCONST</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PHYSCONSTImpl#getPHYSCONSTANTVALUE <em>PHYSCONSTANTVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSCONSTImpl#getDOPREF <em>DOPREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSCONSTImpl#getDOPSNREF <em>DOPSNREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PHYSCONSTImpl extends POSITIONABLEPARAMImpl implements PHYSCONST {
	/**
	 * The default value of the '{@link #getPHYSCONSTANTVALUE() <em>PHYSCONSTANTVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSCONSTANTVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String PHYSCONSTANTVALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPHYSCONSTANTVALUE() <em>PHYSCONSTANTVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSCONSTANTVALUE()
	 * @generated
	 * @ordered
	 */
	protected String pHYSCONSTANTVALUE = PHYSCONSTANTVALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDOPREF() <em>DOPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOPREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK dOPREF;

	/**
	 * The cached value of the '{@link #getDOPSNREF() <em>DOPSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOPSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dOPSNREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PHYSCONSTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPHYSCONST();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPHYSCONSTANTVALUE() {
		return pHYSCONSTANTVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHYSCONSTANTVALUE(String newPHYSCONSTANTVALUE) {
		String oldPHYSCONSTANTVALUE = pHYSCONSTANTVALUE;
		pHYSCONSTANTVALUE = newPHYSCONSTANTVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSCONST__PHYSCONSTANTVALUE, oldPHYSCONSTANTVALUE, pHYSCONSTANTVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getDOPREF() {
		return dOPREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDOPREF(ODXLINK newDOPREF, NotificationChain msgs) {
		ODXLINK oldDOPREF = dOPREF;
		dOPREF = newDOPREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSCONST__DOPREF, oldDOPREF, newDOPREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOPREF(ODXLINK newDOPREF) {
		if (newDOPREF != dOPREF) {
			NotificationChain msgs = null;
			if (dOPREF != null)
				msgs = ((InternalEObject)dOPREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSCONST__DOPREF, null, msgs);
			if (newDOPREF != null)
				msgs = ((InternalEObject)newDOPREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSCONST__DOPREF, null, msgs);
			msgs = basicSetDOPREF(newDOPREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSCONST__DOPREF, newDOPREF, newDOPREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDOPSNREF() {
		return dOPSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDOPSNREF(SNREF newDOPSNREF, NotificationChain msgs) {
		SNREF oldDOPSNREF = dOPSNREF;
		dOPSNREF = newDOPSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSCONST__DOPSNREF, oldDOPSNREF, newDOPSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOPSNREF(SNREF newDOPSNREF) {
		if (newDOPSNREF != dOPSNREF) {
			NotificationChain msgs = null;
			if (dOPSNREF != null)
				msgs = ((InternalEObject)dOPSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSCONST__DOPSNREF, null, msgs);
			if (newDOPSNREF != null)
				msgs = ((InternalEObject)newDOPSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSCONST__DOPSNREF, null, msgs);
			msgs = basicSetDOPSNREF(newDOPSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSCONST__DOPSNREF, newDOPSNREF, newDOPSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSCONST__DOPREF:
				return basicSetDOPREF(null, msgs);
			case OdxXhtmlPackage.PHYSCONST__DOPSNREF:
				return basicSetDOPSNREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSCONST__PHYSCONSTANTVALUE:
				return getPHYSCONSTANTVALUE();
			case OdxXhtmlPackage.PHYSCONST__DOPREF:
				return getDOPREF();
			case OdxXhtmlPackage.PHYSCONST__DOPSNREF:
				return getDOPSNREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSCONST__PHYSCONSTANTVALUE:
				setPHYSCONSTANTVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.PHYSCONST__DOPREF:
				setDOPREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.PHYSCONST__DOPSNREF:
				setDOPSNREF((SNREF)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSCONST__PHYSCONSTANTVALUE:
				setPHYSCONSTANTVALUE(PHYSCONSTANTVALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSCONST__DOPREF:
				setDOPREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.PHYSCONST__DOPSNREF:
				setDOPSNREF((SNREF)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSCONST__PHYSCONSTANTVALUE:
				return PHYSCONSTANTVALUE_EDEFAULT == null ? pHYSCONSTANTVALUE != null : !PHYSCONSTANTVALUE_EDEFAULT.equals(pHYSCONSTANTVALUE);
			case OdxXhtmlPackage.PHYSCONST__DOPREF:
				return dOPREF != null;
			case OdxXhtmlPackage.PHYSCONST__DOPSNREF:
				return dOPSNREF != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (pHYSCONSTANTVALUE: ");
		result.append(pHYSCONSTANTVALUE);
		result.append(')');
		return result.toString();
	}

} //PHYSCONSTImpl
