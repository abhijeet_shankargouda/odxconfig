/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.COMPANYDATAS;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.ODXCATEGORY;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ODXCATEGORY</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ODXCATEGORYImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXCATEGORYImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXCATEGORYImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXCATEGORYImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXCATEGORYImpl#getCOMPANYDATAS <em>COMPANYDATAS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXCATEGORYImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.ODXCATEGORYImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ODXCATEGORYImpl extends MinimalEObjectImpl.Container implements ODXCATEGORY {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getCOMPANYDATAS() <em>COMPANYDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDATAS()
	 * @generated
	 * @ordered
	 */
	protected COMPANYDATAS cOMPANYDATAS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ODXCATEGORYImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getODXCATEGORY();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODXCATEGORY__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODXCATEGORY__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODXCATEGORY__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODXCATEGORY__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODXCATEGORY__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODXCATEGORY__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDATAS getCOMPANYDATAS() {
		return cOMPANYDATAS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYDATAS(COMPANYDATAS newCOMPANYDATAS, NotificationChain msgs) {
		COMPANYDATAS oldCOMPANYDATAS = cOMPANYDATAS;
		cOMPANYDATAS = newCOMPANYDATAS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__COMPANYDATAS, oldCOMPANYDATAS, newCOMPANYDATAS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYDATAS(COMPANYDATAS newCOMPANYDATAS) {
		if (newCOMPANYDATAS != cOMPANYDATAS) {
			NotificationChain msgs = null;
			if (cOMPANYDATAS != null)
				msgs = ((InternalEObject)cOMPANYDATAS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODXCATEGORY__COMPANYDATAS, null, msgs);
			if (newCOMPANYDATAS != null)
				msgs = ((InternalEObject)newCOMPANYDATAS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ODXCATEGORY__COMPANYDATAS, null, msgs);
			msgs = basicSetCOMPANYDATAS(newCOMPANYDATAS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__COMPANYDATAS, newCOMPANYDATAS, newCOMPANYDATAS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ODXCATEGORY__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ODXCATEGORY__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.ODXCATEGORY__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.ODXCATEGORY__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.ODXCATEGORY__COMPANYDATAS:
				return basicSetCOMPANYDATAS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ODXCATEGORY__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.ODXCATEGORY__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.ODXCATEGORY__DESC:
				return getDESC();
			case OdxXhtmlPackage.ODXCATEGORY__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.ODXCATEGORY__COMPANYDATAS:
				return getCOMPANYDATAS();
			case OdxXhtmlPackage.ODXCATEGORY__ID:
				return getID();
			case OdxXhtmlPackage.ODXCATEGORY__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ODXCATEGORY__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__COMPANYDATAS:
				setCOMPANYDATAS((COMPANYDATAS)newValue);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ODXCATEGORY__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__COMPANYDATAS:
				setCOMPANYDATAS((COMPANYDATAS)null);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.ODXCATEGORY__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ODXCATEGORY__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.ODXCATEGORY__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.ODXCATEGORY__DESC:
				return dESC != null;
			case OdxXhtmlPackage.ODXCATEGORY__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.ODXCATEGORY__COMPANYDATAS:
				return cOMPANYDATAS != null;
			case OdxXhtmlPackage.ODXCATEGORY__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.ODXCATEGORY__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //ODXCATEGORYImpl
