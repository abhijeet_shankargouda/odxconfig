/**
 */
package OdxXhtml.impl;

import OdxXhtml.MATCHINGPARAMETER;
import OdxXhtml.MATCHINGPARAMETERS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MATCHINGPARAMETERS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MATCHINGPARAMETERSImpl#getMATCHINGPARAMETER <em>MATCHINGPARAMETER</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MATCHINGPARAMETERSImpl extends MinimalEObjectImpl.Container implements MATCHINGPARAMETERS {
	/**
	 * The cached value of the '{@link #getMATCHINGPARAMETER() <em>MATCHINGPARAMETER</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMATCHINGPARAMETER()
	 * @generated
	 * @ordered
	 */
	protected EList<MATCHINGPARAMETER> mATCHINGPARAMETER;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MATCHINGPARAMETERSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMATCHINGPARAMETERS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<MATCHINGPARAMETER> getMATCHINGPARAMETER() {
		if (mATCHINGPARAMETER == null) {
			mATCHINGPARAMETER = new EObjectContainmentEList<MATCHINGPARAMETER>(MATCHINGPARAMETER.class, this, OdxXhtmlPackage.MATCHINGPARAMETERS__MATCHINGPARAMETER);
		}
		return mATCHINGPARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETERS__MATCHINGPARAMETER:
				return ((InternalEList<?>)getMATCHINGPARAMETER()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETERS__MATCHINGPARAMETER:
				return getMATCHINGPARAMETER();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETERS__MATCHINGPARAMETER:
				getMATCHINGPARAMETER().clear();
				getMATCHINGPARAMETER().addAll((Collection<? extends MATCHINGPARAMETER>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETERS__MATCHINGPARAMETER:
				getMATCHINGPARAMETER().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGPARAMETERS__MATCHINGPARAMETER:
				return mATCHINGPARAMETER != null && !mATCHINGPARAMETER.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MATCHINGPARAMETERSImpl
