/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSSEGMENT;
import OdxXhtml.PHYSSEGMENTS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PHYSSEGMENTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PHYSSEGMENTSImpl#getPHYSSEGMENT <em>PHYSSEGMENT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PHYSSEGMENTSImpl extends MinimalEObjectImpl.Container implements PHYSSEGMENTS {
	/**
	 * The cached value of the '{@link #getPHYSSEGMENT() <em>PHYSSEGMENT</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSSEGMENT()
	 * @generated
	 * @ordered
	 */
	protected EList<PHYSSEGMENT> pHYSSEGMENT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PHYSSEGMENTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPHYSSEGMENTS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<PHYSSEGMENT> getPHYSSEGMENT() {
		if (pHYSSEGMENT == null) {
			pHYSSEGMENT = new EObjectContainmentEList<PHYSSEGMENT>(PHYSSEGMENT.class, this, OdxXhtmlPackage.PHYSSEGMENTS__PHYSSEGMENT);
		}
		return pHYSSEGMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENTS__PHYSSEGMENT:
				return ((InternalEList<?>)getPHYSSEGMENT()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENTS__PHYSSEGMENT:
				return getPHYSSEGMENT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENTS__PHYSSEGMENT:
				getPHYSSEGMENT().clear();
				getPHYSSEGMENT().addAll((Collection<? extends PHYSSEGMENT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENTS__PHYSSEGMENT:
				getPHYSSEGMENT().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENTS__PHYSSEGMENT:
				return pHYSSEGMENT != null && !pHYSSEGMENT.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PHYSSEGMENTSImpl
