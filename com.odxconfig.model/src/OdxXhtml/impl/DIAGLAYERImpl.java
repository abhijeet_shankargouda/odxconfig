/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.COMPANYDATAS;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.DIAGCOMMS;
import OdxXhtml.DIAGDATADICTIONARYSPEC;
import OdxXhtml.DIAGLAYER;
import OdxXhtml.FUNCTCLASSS;
import OdxXhtml.GLOBALNEGRESPONSES;
import OdxXhtml.NEGRESPONSES;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.POSRESPONSES;
import OdxXhtml.REQUESTS;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGLAYER</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getCOMPANYDATAS <em>COMPANYDATAS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getFUNCTCLASSS <em>FUNCTCLASSS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getDIAGDATADICTIONARYSPEC <em>DIAGDATADICTIONARYSPEC</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getDIAGCOMMS <em>DIAGCOMMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getREQUESTS <em>REQUESTS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getPOSRESPONSES <em>POSRESPONSES</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getNEGRESPONSES <em>NEGRESPONSES</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getGLOBALNEGRESPONSES <em>GLOBALNEGRESPONSES</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGLAYERImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGLAYERImpl extends MinimalEObjectImpl.Container implements DIAGLAYER {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getCOMPANYDATAS() <em>COMPANYDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDATAS()
	 * @generated
	 * @ordered
	 */
	protected COMPANYDATAS cOMPANYDATAS;

	/**
	 * The cached value of the '{@link #getFUNCTCLASSS() <em>FUNCTCLASSS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFUNCTCLASSS()
	 * @generated
	 * @ordered
	 */
	protected FUNCTCLASSS fUNCTCLASSS;

	/**
	 * The cached value of the '{@link #getDIAGDATADICTIONARYSPEC() <em>DIAGDATADICTIONARYSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGDATADICTIONARYSPEC()
	 * @generated
	 * @ordered
	 */
	protected DIAGDATADICTIONARYSPEC dIAGDATADICTIONARYSPEC;

	/**
	 * The cached value of the '{@link #getDIAGCOMMS() <em>DIAGCOMMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCOMMS()
	 * @generated
	 * @ordered
	 */
	protected DIAGCOMMS dIAGCOMMS;

	/**
	 * The cached value of the '{@link #getREQUESTS() <em>REQUESTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREQUESTS()
	 * @generated
	 * @ordered
	 */
	protected REQUESTS rEQUESTS;

	/**
	 * The cached value of the '{@link #getPOSRESPONSES() <em>POSRESPONSES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPOSRESPONSES()
	 * @generated
	 * @ordered
	 */
	protected POSRESPONSES pOSRESPONSES;

	/**
	 * The cached value of the '{@link #getNEGRESPONSES() <em>NEGRESPONSES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNEGRESPONSES()
	 * @generated
	 * @ordered
	 */
	protected NEGRESPONSES nEGRESPONSES;

	/**
	 * The cached value of the '{@link #getGLOBALNEGRESPONSES() <em>GLOBALNEGRESPONSES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGLOBALNEGRESPONSES()
	 * @generated
	 * @ordered
	 */
	protected GLOBALNEGRESPONSES gLOBALNEGRESPONSES;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGLAYERImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGLAYER();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDATAS getCOMPANYDATAS() {
		return cOMPANYDATAS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYDATAS(COMPANYDATAS newCOMPANYDATAS, NotificationChain msgs) {
		COMPANYDATAS oldCOMPANYDATAS = cOMPANYDATAS;
		cOMPANYDATAS = newCOMPANYDATAS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__COMPANYDATAS, oldCOMPANYDATAS, newCOMPANYDATAS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYDATAS(COMPANYDATAS newCOMPANYDATAS) {
		if (newCOMPANYDATAS != cOMPANYDATAS) {
			NotificationChain msgs = null;
			if (cOMPANYDATAS != null)
				msgs = ((InternalEObject)cOMPANYDATAS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__COMPANYDATAS, null, msgs);
			if (newCOMPANYDATAS != null)
				msgs = ((InternalEObject)newCOMPANYDATAS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__COMPANYDATAS, null, msgs);
			msgs = basicSetCOMPANYDATAS(newCOMPANYDATAS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__COMPANYDATAS, newCOMPANYDATAS, newCOMPANYDATAS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTCLASSS getFUNCTCLASSS() {
		return fUNCTCLASSS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFUNCTCLASSS(FUNCTCLASSS newFUNCTCLASSS, NotificationChain msgs) {
		FUNCTCLASSS oldFUNCTCLASSS = fUNCTCLASSS;
		fUNCTCLASSS = newFUNCTCLASSS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__FUNCTCLASSS, oldFUNCTCLASSS, newFUNCTCLASSS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFUNCTCLASSS(FUNCTCLASSS newFUNCTCLASSS) {
		if (newFUNCTCLASSS != fUNCTCLASSS) {
			NotificationChain msgs = null;
			if (fUNCTCLASSS != null)
				msgs = ((InternalEObject)fUNCTCLASSS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__FUNCTCLASSS, null, msgs);
			if (newFUNCTCLASSS != null)
				msgs = ((InternalEObject)newFUNCTCLASSS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__FUNCTCLASSS, null, msgs);
			msgs = basicSetFUNCTCLASSS(newFUNCTCLASSS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__FUNCTCLASSS, newFUNCTCLASSS, newFUNCTCLASSS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGDATADICTIONARYSPEC getDIAGDATADICTIONARYSPEC() {
		return dIAGDATADICTIONARYSPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC newDIAGDATADICTIONARYSPEC, NotificationChain msgs) {
		DIAGDATADICTIONARYSPEC oldDIAGDATADICTIONARYSPEC = dIAGDATADICTIONARYSPEC;
		dIAGDATADICTIONARYSPEC = newDIAGDATADICTIONARYSPEC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__DIAGDATADICTIONARYSPEC, oldDIAGDATADICTIONARYSPEC, newDIAGDATADICTIONARYSPEC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC newDIAGDATADICTIONARYSPEC) {
		if (newDIAGDATADICTIONARYSPEC != dIAGDATADICTIONARYSPEC) {
			NotificationChain msgs = null;
			if (dIAGDATADICTIONARYSPEC != null)
				msgs = ((InternalEObject)dIAGDATADICTIONARYSPEC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__DIAGDATADICTIONARYSPEC, null, msgs);
			if (newDIAGDATADICTIONARYSPEC != null)
				msgs = ((InternalEObject)newDIAGDATADICTIONARYSPEC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__DIAGDATADICTIONARYSPEC, null, msgs);
			msgs = basicSetDIAGDATADICTIONARYSPEC(newDIAGDATADICTIONARYSPEC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__DIAGDATADICTIONARYSPEC, newDIAGDATADICTIONARYSPEC, newDIAGDATADICTIONARYSPEC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGCOMMS getDIAGCOMMS() {
		return dIAGCOMMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCOMMS(DIAGCOMMS newDIAGCOMMS, NotificationChain msgs) {
		DIAGCOMMS oldDIAGCOMMS = dIAGCOMMS;
		dIAGCOMMS = newDIAGCOMMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__DIAGCOMMS, oldDIAGCOMMS, newDIAGCOMMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCOMMS(DIAGCOMMS newDIAGCOMMS) {
		if (newDIAGCOMMS != dIAGCOMMS) {
			NotificationChain msgs = null;
			if (dIAGCOMMS != null)
				msgs = ((InternalEObject)dIAGCOMMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__DIAGCOMMS, null, msgs);
			if (newDIAGCOMMS != null)
				msgs = ((InternalEObject)newDIAGCOMMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__DIAGCOMMS, null, msgs);
			msgs = basicSetDIAGCOMMS(newDIAGCOMMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__DIAGCOMMS, newDIAGCOMMS, newDIAGCOMMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public REQUESTS getREQUESTS() {
		return rEQUESTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetREQUESTS(REQUESTS newREQUESTS, NotificationChain msgs) {
		REQUESTS oldREQUESTS = rEQUESTS;
		rEQUESTS = newREQUESTS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__REQUESTS, oldREQUESTS, newREQUESTS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREQUESTS(REQUESTS newREQUESTS) {
		if (newREQUESTS != rEQUESTS) {
			NotificationChain msgs = null;
			if (rEQUESTS != null)
				msgs = ((InternalEObject)rEQUESTS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__REQUESTS, null, msgs);
			if (newREQUESTS != null)
				msgs = ((InternalEObject)newREQUESTS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__REQUESTS, null, msgs);
			msgs = basicSetREQUESTS(newREQUESTS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__REQUESTS, newREQUESTS, newREQUESTS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public POSRESPONSES getPOSRESPONSES() {
		return pOSRESPONSES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPOSRESPONSES(POSRESPONSES newPOSRESPONSES, NotificationChain msgs) {
		POSRESPONSES oldPOSRESPONSES = pOSRESPONSES;
		pOSRESPONSES = newPOSRESPONSES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__POSRESPONSES, oldPOSRESPONSES, newPOSRESPONSES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPOSRESPONSES(POSRESPONSES newPOSRESPONSES) {
		if (newPOSRESPONSES != pOSRESPONSES) {
			NotificationChain msgs = null;
			if (pOSRESPONSES != null)
				msgs = ((InternalEObject)pOSRESPONSES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__POSRESPONSES, null, msgs);
			if (newPOSRESPONSES != null)
				msgs = ((InternalEObject)newPOSRESPONSES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__POSRESPONSES, null, msgs);
			msgs = basicSetPOSRESPONSES(newPOSRESPONSES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__POSRESPONSES, newPOSRESPONSES, newPOSRESPONSES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGRESPONSES getNEGRESPONSES() {
		return nEGRESPONSES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNEGRESPONSES(NEGRESPONSES newNEGRESPONSES, NotificationChain msgs) {
		NEGRESPONSES oldNEGRESPONSES = nEGRESPONSES;
		nEGRESPONSES = newNEGRESPONSES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__NEGRESPONSES, oldNEGRESPONSES, newNEGRESPONSES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNEGRESPONSES(NEGRESPONSES newNEGRESPONSES) {
		if (newNEGRESPONSES != nEGRESPONSES) {
			NotificationChain msgs = null;
			if (nEGRESPONSES != null)
				msgs = ((InternalEObject)nEGRESPONSES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__NEGRESPONSES, null, msgs);
			if (newNEGRESPONSES != null)
				msgs = ((InternalEObject)newNEGRESPONSES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__NEGRESPONSES, null, msgs);
			msgs = basicSetNEGRESPONSES(newNEGRESPONSES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__NEGRESPONSES, newNEGRESPONSES, newNEGRESPONSES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GLOBALNEGRESPONSES getGLOBALNEGRESPONSES() {
		return gLOBALNEGRESPONSES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGLOBALNEGRESPONSES(GLOBALNEGRESPONSES newGLOBALNEGRESPONSES, NotificationChain msgs) {
		GLOBALNEGRESPONSES oldGLOBALNEGRESPONSES = gLOBALNEGRESPONSES;
		gLOBALNEGRESPONSES = newGLOBALNEGRESPONSES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__GLOBALNEGRESPONSES, oldGLOBALNEGRESPONSES, newGLOBALNEGRESPONSES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGLOBALNEGRESPONSES(GLOBALNEGRESPONSES newGLOBALNEGRESPONSES) {
		if (newGLOBALNEGRESPONSES != gLOBALNEGRESPONSES) {
			NotificationChain msgs = null;
			if (gLOBALNEGRESPONSES != null)
				msgs = ((InternalEObject)gLOBALNEGRESPONSES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__GLOBALNEGRESPONSES, null, msgs);
			if (newGLOBALNEGRESPONSES != null)
				msgs = ((InternalEObject)newGLOBALNEGRESPONSES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGLAYER__GLOBALNEGRESPONSES, null, msgs);
			msgs = basicSetGLOBALNEGRESPONSES(newGLOBALNEGRESPONSES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__GLOBALNEGRESPONSES, newGLOBALNEGRESPONSES, newGLOBALNEGRESPONSES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGLAYER__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYER__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__COMPANYDATAS:
				return basicSetCOMPANYDATAS(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__FUNCTCLASSS:
				return basicSetFUNCTCLASSS(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__DIAGDATADICTIONARYSPEC:
				return basicSetDIAGDATADICTIONARYSPEC(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__DIAGCOMMS:
				return basicSetDIAGCOMMS(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__REQUESTS:
				return basicSetREQUESTS(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__POSRESPONSES:
				return basicSetPOSRESPONSES(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__NEGRESPONSES:
				return basicSetNEGRESPONSES(null, msgs);
			case OdxXhtmlPackage.DIAGLAYER__GLOBALNEGRESPONSES:
				return basicSetGLOBALNEGRESPONSES(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYER__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.DIAGLAYER__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.DIAGLAYER__DESC:
				return getDESC();
			case OdxXhtmlPackage.DIAGLAYER__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.DIAGLAYER__COMPANYDATAS:
				return getCOMPANYDATAS();
			case OdxXhtmlPackage.DIAGLAYER__FUNCTCLASSS:
				return getFUNCTCLASSS();
			case OdxXhtmlPackage.DIAGLAYER__DIAGDATADICTIONARYSPEC:
				return getDIAGDATADICTIONARYSPEC();
			case OdxXhtmlPackage.DIAGLAYER__DIAGCOMMS:
				return getDIAGCOMMS();
			case OdxXhtmlPackage.DIAGLAYER__REQUESTS:
				return getREQUESTS();
			case OdxXhtmlPackage.DIAGLAYER__POSRESPONSES:
				return getPOSRESPONSES();
			case OdxXhtmlPackage.DIAGLAYER__NEGRESPONSES:
				return getNEGRESPONSES();
			case OdxXhtmlPackage.DIAGLAYER__GLOBALNEGRESPONSES:
				return getGLOBALNEGRESPONSES();
			case OdxXhtmlPackage.DIAGLAYER__ID:
				return getID();
			case OdxXhtmlPackage.DIAGLAYER__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYER__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__COMPANYDATAS:
				setCOMPANYDATAS((COMPANYDATAS)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__FUNCTCLASSS:
				setFUNCTCLASSS((FUNCTCLASSS)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__DIAGDATADICTIONARYSPEC:
				setDIAGDATADICTIONARYSPEC((DIAGDATADICTIONARYSPEC)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__DIAGCOMMS:
				setDIAGCOMMS((DIAGCOMMS)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__REQUESTS:
				setREQUESTS((REQUESTS)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__POSRESPONSES:
				setPOSRESPONSES((POSRESPONSES)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__NEGRESPONSES:
				setNEGRESPONSES((NEGRESPONSES)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__GLOBALNEGRESPONSES:
				setGLOBALNEGRESPONSES((GLOBALNEGRESPONSES)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.DIAGLAYER__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYER__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.DIAGLAYER__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__COMPANYDATAS:
				setCOMPANYDATAS((COMPANYDATAS)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__FUNCTCLASSS:
				setFUNCTCLASSS((FUNCTCLASSS)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__DIAGDATADICTIONARYSPEC:
				setDIAGDATADICTIONARYSPEC((DIAGDATADICTIONARYSPEC)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__DIAGCOMMS:
				setDIAGCOMMS((DIAGCOMMS)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__REQUESTS:
				setREQUESTS((REQUESTS)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__POSRESPONSES:
				setPOSRESPONSES((POSRESPONSES)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__NEGRESPONSES:
				setNEGRESPONSES((NEGRESPONSES)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__GLOBALNEGRESPONSES:
				setGLOBALNEGRESPONSES((GLOBALNEGRESPONSES)null);
				return;
			case OdxXhtmlPackage.DIAGLAYER__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.DIAGLAYER__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGLAYER__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.DIAGLAYER__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.DIAGLAYER__DESC:
				return dESC != null;
			case OdxXhtmlPackage.DIAGLAYER__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.DIAGLAYER__COMPANYDATAS:
				return cOMPANYDATAS != null;
			case OdxXhtmlPackage.DIAGLAYER__FUNCTCLASSS:
				return fUNCTCLASSS != null;
			case OdxXhtmlPackage.DIAGLAYER__DIAGDATADICTIONARYSPEC:
				return dIAGDATADICTIONARYSPEC != null;
			case OdxXhtmlPackage.DIAGLAYER__DIAGCOMMS:
				return dIAGCOMMS != null;
			case OdxXhtmlPackage.DIAGLAYER__REQUESTS:
				return rEQUESTS != null;
			case OdxXhtmlPackage.DIAGLAYER__POSRESPONSES:
				return pOSRESPONSES != null;
			case OdxXhtmlPackage.DIAGLAYER__NEGRESPONSES:
				return nEGRESPONSES != null;
			case OdxXhtmlPackage.DIAGLAYER__GLOBALNEGRESPONSES:
				return gLOBALNEGRESPONSES != null;
			case OdxXhtmlPackage.DIAGLAYER__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.DIAGLAYER__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //DIAGLAYERImpl
