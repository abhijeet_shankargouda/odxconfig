/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.STATICFIELD;
import OdxXhtml.STATICFIELDS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>STATICFIELDS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.STATICFIELDSImpl#getSTATICFIELD <em>STATICFIELD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class STATICFIELDSImpl extends MinimalEObjectImpl.Container implements STATICFIELDS {
	/**
	 * The cached value of the '{@link #getSTATICFIELD() <em>STATICFIELD</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTATICFIELD()
	 * @generated
	 * @ordered
	 */
	protected EList<STATICFIELD> sTATICFIELD;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected STATICFIELDSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSTATICFIELDS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<STATICFIELD> getSTATICFIELD() {
		if (sTATICFIELD == null) {
			sTATICFIELD = new EObjectContainmentEList<STATICFIELD>(STATICFIELD.class, this, OdxXhtmlPackage.STATICFIELDS__STATICFIELD);
		}
		return sTATICFIELD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.STATICFIELDS__STATICFIELD:
				return ((InternalEList<?>)getSTATICFIELD()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.STATICFIELDS__STATICFIELD:
				return getSTATICFIELD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.STATICFIELDS__STATICFIELD:
				getSTATICFIELD().clear();
				getSTATICFIELD().addAll((Collection<? extends STATICFIELD>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STATICFIELDS__STATICFIELD:
				getSTATICFIELD().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STATICFIELDS__STATICFIELD:
				return sTATICFIELD != null && !sTATICFIELD.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //STATICFIELDSImpl
