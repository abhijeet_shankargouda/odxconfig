/**
 */
package OdxXhtml.impl;

import OdxXhtml.LENGTHDESCRIPTOR;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LENGTHDESCRIPTOR</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LENGTHDESCRIPTORImpl extends MinimalEObjectImpl.Container implements LENGTHDESCRIPTOR {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LENGTHDESCRIPTORImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLENGTHDESCRIPTOR();
	}

} //LENGTHDESCRIPTORImpl
