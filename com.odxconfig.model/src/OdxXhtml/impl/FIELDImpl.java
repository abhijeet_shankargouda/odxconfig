/**
 */
package OdxXhtml.impl;

import OdxXhtml.FIELD;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FIELD</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FIELDImpl#getBASICSTRUCTUREREF <em>BASICSTRUCTUREREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.FIELDImpl#getENVDATADESCREF <em>ENVDATADESCREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.FIELDImpl#isISVISIBLE <em>ISVISIBLE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FIELDImpl extends COMPLEXDOPImpl implements FIELD {
	/**
	 * The cached value of the '{@link #getBASICSTRUCTUREREF() <em>BASICSTRUCTUREREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASICSTRUCTUREREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK bASICSTRUCTUREREF;

	/**
	 * The cached value of the '{@link #getENVDATADESCREF() <em>ENVDATADESCREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENVDATADESCREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK eNVDATADESCREF;

	/**
	 * The default value of the '{@link #isISVISIBLE() <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISVISIBLE()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISVISIBLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isISVISIBLE() <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISVISIBLE()
	 * @generated
	 * @ordered
	 */
	protected boolean iSVISIBLE = ISVISIBLE_EDEFAULT;

	/**
	 * This is true if the ISVISIBLE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSVISIBLEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FIELDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFIELD();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getBASICSTRUCTUREREF() {
		return bASICSTRUCTUREREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBASICSTRUCTUREREF(ODXLINK newBASICSTRUCTUREREF, NotificationChain msgs) {
		ODXLINK oldBASICSTRUCTUREREF = bASICSTRUCTUREREF;
		bASICSTRUCTUREREF = newBASICSTRUCTUREREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FIELD__BASICSTRUCTUREREF, oldBASICSTRUCTUREREF, newBASICSTRUCTUREREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBASICSTRUCTUREREF(ODXLINK newBASICSTRUCTUREREF) {
		if (newBASICSTRUCTUREREF != bASICSTRUCTUREREF) {
			NotificationChain msgs = null;
			if (bASICSTRUCTUREREF != null)
				msgs = ((InternalEObject)bASICSTRUCTUREREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FIELD__BASICSTRUCTUREREF, null, msgs);
			if (newBASICSTRUCTUREREF != null)
				msgs = ((InternalEObject)newBASICSTRUCTUREREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FIELD__BASICSTRUCTUREREF, null, msgs);
			msgs = basicSetBASICSTRUCTUREREF(newBASICSTRUCTUREREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FIELD__BASICSTRUCTUREREF, newBASICSTRUCTUREREF, newBASICSTRUCTUREREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getENVDATADESCREF() {
		return eNVDATADESCREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetENVDATADESCREF(ODXLINK newENVDATADESCREF, NotificationChain msgs) {
		ODXLINK oldENVDATADESCREF = eNVDATADESCREF;
		eNVDATADESCREF = newENVDATADESCREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FIELD__ENVDATADESCREF, oldENVDATADESCREF, newENVDATADESCREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setENVDATADESCREF(ODXLINK newENVDATADESCREF) {
		if (newENVDATADESCREF != eNVDATADESCREF) {
			NotificationChain msgs = null;
			if (eNVDATADESCREF != null)
				msgs = ((InternalEObject)eNVDATADESCREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FIELD__ENVDATADESCREF, null, msgs);
			if (newENVDATADESCREF != null)
				msgs = ((InternalEObject)newENVDATADESCREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.FIELD__ENVDATADESCREF, null, msgs);
			msgs = basicSetENVDATADESCREF(newENVDATADESCREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FIELD__ENVDATADESCREF, newENVDATADESCREF, newENVDATADESCREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISVISIBLE() {
		return iSVISIBLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISVISIBLE(boolean newISVISIBLE) {
		boolean oldISVISIBLE = iSVISIBLE;
		iSVISIBLE = newISVISIBLE;
		boolean oldISVISIBLEESet = iSVISIBLEESet;
		iSVISIBLEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FIELD__ISVISIBLE, oldISVISIBLE, iSVISIBLE, !oldISVISIBLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISVISIBLE() {
		boolean oldISVISIBLE = iSVISIBLE;
		boolean oldISVISIBLEESet = iSVISIBLEESet;
		iSVISIBLE = ISVISIBLE_EDEFAULT;
		iSVISIBLEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.FIELD__ISVISIBLE, oldISVISIBLE, ISVISIBLE_EDEFAULT, oldISVISIBLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISVISIBLE() {
		return iSVISIBLEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.FIELD__BASICSTRUCTUREREF:
				return basicSetBASICSTRUCTUREREF(null, msgs);
			case OdxXhtmlPackage.FIELD__ENVDATADESCREF:
				return basicSetENVDATADESCREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FIELD__BASICSTRUCTUREREF:
				return getBASICSTRUCTUREREF();
			case OdxXhtmlPackage.FIELD__ENVDATADESCREF:
				return getENVDATADESCREF();
			case OdxXhtmlPackage.FIELD__ISVISIBLE:
				return isISVISIBLE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FIELD__BASICSTRUCTUREREF:
				setBASICSTRUCTUREREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.FIELD__ENVDATADESCREF:
				setENVDATADESCREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.FIELD__ISVISIBLE:
				setISVISIBLE((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FIELD__BASICSTRUCTUREREF:
				setBASICSTRUCTUREREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.FIELD__ENVDATADESCREF:
				setENVDATADESCREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.FIELD__ISVISIBLE:
				unsetISVISIBLE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FIELD__BASICSTRUCTUREREF:
				return bASICSTRUCTUREREF != null;
			case OdxXhtmlPackage.FIELD__ENVDATADESCREF:
				return eNVDATADESCREF != null;
			case OdxXhtmlPackage.FIELD__ISVISIBLE:
				return isSetISVISIBLE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (iSVISIBLE: ");
		if (iSVISIBLEESet) result.append(iSVISIBLE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //FIELDImpl
