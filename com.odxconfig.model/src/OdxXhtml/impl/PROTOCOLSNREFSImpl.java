/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROTOCOLSNREFS;
import OdxXhtml.SNREF;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PROTOCOLSNREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PROTOCOLSNREFSImpl#getPROTOCOLSNREF <em>PROTOCOLSNREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PROTOCOLSNREFSImpl extends MinimalEObjectImpl.Container implements PROTOCOLSNREFS {
	/**
	 * The cached value of the '{@link #getPROTOCOLSNREF() <em>PROTOCOLSNREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROTOCOLSNREF()
	 * @generated
	 * @ordered
	 */
	protected EList<SNREF> pROTOCOLSNREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PROTOCOLSNREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPROTOCOLSNREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SNREF> getPROTOCOLSNREF() {
		if (pROTOCOLSNREF == null) {
			pROTOCOLSNREF = new EObjectContainmentEList<SNREF>(SNREF.class, this, OdxXhtmlPackage.PROTOCOLSNREFS__PROTOCOLSNREF);
		}
		return pROTOCOLSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOLSNREFS__PROTOCOLSNREF:
				return ((InternalEList<?>)getPROTOCOLSNREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOLSNREFS__PROTOCOLSNREF:
				return getPROTOCOLSNREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOLSNREFS__PROTOCOLSNREF:
				getPROTOCOLSNREF().clear();
				getPROTOCOLSNREF().addAll((Collection<? extends SNREF>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOLSNREFS__PROTOCOLSNREF:
				getPROTOCOLSNREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROTOCOLSNREFS__PROTOCOLSNREF:
				return pROTOCOLSNREF != null && !pROTOCOLSNREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PROTOCOLSNREFSImpl
