/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SEGMENT;
import OdxXhtml.SOURCEENDADDRESS;
import OdxXhtml.TEXT;
import OdxXhtml.UNCOMPRESSEDSIZE;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SEGMENT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SEGMENTImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.SEGMENTImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.SEGMENTImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.SEGMENTImpl#getSOURCESTARTADDRESS <em>SOURCESTARTADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SEGMENTImpl#getCOMPRESSEDSIZE <em>COMPRESSEDSIZE</em>}</li>
 *   <li>{@link OdxXhtml.impl.SEGMENTImpl#getUNCOMPRESSEDSIZE <em>UNCOMPRESSEDSIZE</em>}</li>
 *   <li>{@link OdxXhtml.impl.SEGMENTImpl#getSOURCEENDADDRESS <em>SOURCEENDADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SEGMENTImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.SEGMENTImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SEGMENTImpl extends MinimalEObjectImpl.Container implements SEGMENT {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getSOURCESTARTADDRESS() <em>SOURCESTARTADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSOURCESTARTADDRESS()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] SOURCESTARTADDRESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSOURCESTARTADDRESS() <em>SOURCESTARTADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSOURCESTARTADDRESS()
	 * @generated
	 * @ordered
	 */
	protected byte[] sOURCESTARTADDRESS = SOURCESTARTADDRESS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCOMPRESSEDSIZE() <em>COMPRESSEDSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPRESSEDSIZE()
	 * @generated
	 * @ordered
	 */
	protected static final long COMPRESSEDSIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getCOMPRESSEDSIZE() <em>COMPRESSEDSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPRESSEDSIZE()
	 * @generated
	 * @ordered
	 */
	protected long cOMPRESSEDSIZE = COMPRESSEDSIZE_EDEFAULT;

	/**
	 * This is true if the COMPRESSEDSIZE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean cOMPRESSEDSIZEESet;

	/**
	 * The cached value of the '{@link #getUNCOMPRESSEDSIZE() <em>UNCOMPRESSEDSIZE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUNCOMPRESSEDSIZE()
	 * @generated
	 * @ordered
	 */
	protected UNCOMPRESSEDSIZE uNCOMPRESSEDSIZE;

	/**
	 * The cached value of the '{@link #getSOURCEENDADDRESS() <em>SOURCEENDADDRESS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSOURCEENDADDRESS()
	 * @generated
	 * @ordered
	 */
	protected SOURCEENDADDRESS sOURCEENDADDRESS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SEGMENTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSEGMENT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SEGMENT__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SEGMENT__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SEGMENT__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SEGMENT__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getSOURCESTARTADDRESS() {
		return sOURCESTARTADDRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSOURCESTARTADDRESS(byte[] newSOURCESTARTADDRESS) {
		byte[] oldSOURCESTARTADDRESS = sOURCESTARTADDRESS;
		sOURCESTARTADDRESS = newSOURCESTARTADDRESS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__SOURCESTARTADDRESS, oldSOURCESTARTADDRESS, sOURCESTARTADDRESS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getCOMPRESSEDSIZE() {
		return cOMPRESSEDSIZE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPRESSEDSIZE(long newCOMPRESSEDSIZE) {
		long oldCOMPRESSEDSIZE = cOMPRESSEDSIZE;
		cOMPRESSEDSIZE = newCOMPRESSEDSIZE;
		boolean oldCOMPRESSEDSIZEESet = cOMPRESSEDSIZEESet;
		cOMPRESSEDSIZEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__COMPRESSEDSIZE, oldCOMPRESSEDSIZE, cOMPRESSEDSIZE, !oldCOMPRESSEDSIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetCOMPRESSEDSIZE() {
		long oldCOMPRESSEDSIZE = cOMPRESSEDSIZE;
		boolean oldCOMPRESSEDSIZEESet = cOMPRESSEDSIZEESet;
		cOMPRESSEDSIZE = COMPRESSEDSIZE_EDEFAULT;
		cOMPRESSEDSIZEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.SEGMENT__COMPRESSEDSIZE, oldCOMPRESSEDSIZE, COMPRESSEDSIZE_EDEFAULT, oldCOMPRESSEDSIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetCOMPRESSEDSIZE() {
		return cOMPRESSEDSIZEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNCOMPRESSEDSIZE getUNCOMPRESSEDSIZE() {
		return uNCOMPRESSEDSIZE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUNCOMPRESSEDSIZE(UNCOMPRESSEDSIZE newUNCOMPRESSEDSIZE, NotificationChain msgs) {
		UNCOMPRESSEDSIZE oldUNCOMPRESSEDSIZE = uNCOMPRESSEDSIZE;
		uNCOMPRESSEDSIZE = newUNCOMPRESSEDSIZE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__UNCOMPRESSEDSIZE, oldUNCOMPRESSEDSIZE, newUNCOMPRESSEDSIZE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUNCOMPRESSEDSIZE(UNCOMPRESSEDSIZE newUNCOMPRESSEDSIZE) {
		if (newUNCOMPRESSEDSIZE != uNCOMPRESSEDSIZE) {
			NotificationChain msgs = null;
			if (uNCOMPRESSEDSIZE != null)
				msgs = ((InternalEObject)uNCOMPRESSEDSIZE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SEGMENT__UNCOMPRESSEDSIZE, null, msgs);
			if (newUNCOMPRESSEDSIZE != null)
				msgs = ((InternalEObject)newUNCOMPRESSEDSIZE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SEGMENT__UNCOMPRESSEDSIZE, null, msgs);
			msgs = basicSetUNCOMPRESSEDSIZE(newUNCOMPRESSEDSIZE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__UNCOMPRESSEDSIZE, newUNCOMPRESSEDSIZE, newUNCOMPRESSEDSIZE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SOURCEENDADDRESS getSOURCEENDADDRESS() {
		return sOURCEENDADDRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSOURCEENDADDRESS(SOURCEENDADDRESS newSOURCEENDADDRESS, NotificationChain msgs) {
		SOURCEENDADDRESS oldSOURCEENDADDRESS = sOURCEENDADDRESS;
		sOURCEENDADDRESS = newSOURCEENDADDRESS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__SOURCEENDADDRESS, oldSOURCEENDADDRESS, newSOURCEENDADDRESS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSOURCEENDADDRESS(SOURCEENDADDRESS newSOURCEENDADDRESS) {
		if (newSOURCEENDADDRESS != sOURCEENDADDRESS) {
			NotificationChain msgs = null;
			if (sOURCEENDADDRESS != null)
				msgs = ((InternalEObject)sOURCEENDADDRESS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SEGMENT__SOURCEENDADDRESS, null, msgs);
			if (newSOURCEENDADDRESS != null)
				msgs = ((InternalEObject)newSOURCEENDADDRESS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SEGMENT__SOURCEENDADDRESS, null, msgs);
			msgs = basicSetSOURCEENDADDRESS(newSOURCEENDADDRESS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__SOURCEENDADDRESS, newSOURCEENDADDRESS, newSOURCEENDADDRESS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SEGMENT__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SEGMENT__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.SEGMENT__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.SEGMENT__UNCOMPRESSEDSIZE:
				return basicSetUNCOMPRESSEDSIZE(null, msgs);
			case OdxXhtmlPackage.SEGMENT__SOURCEENDADDRESS:
				return basicSetSOURCEENDADDRESS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SEGMENT__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.SEGMENT__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.SEGMENT__DESC:
				return getDESC();
			case OdxXhtmlPackage.SEGMENT__SOURCESTARTADDRESS:
				return getSOURCESTARTADDRESS();
			case OdxXhtmlPackage.SEGMENT__COMPRESSEDSIZE:
				return getCOMPRESSEDSIZE();
			case OdxXhtmlPackage.SEGMENT__UNCOMPRESSEDSIZE:
				return getUNCOMPRESSEDSIZE();
			case OdxXhtmlPackage.SEGMENT__SOURCEENDADDRESS:
				return getSOURCEENDADDRESS();
			case OdxXhtmlPackage.SEGMENT__ID:
				return getID();
			case OdxXhtmlPackage.SEGMENT__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SEGMENT__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.SEGMENT__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.SEGMENT__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.SEGMENT__SOURCESTARTADDRESS:
				setSOURCESTARTADDRESS((byte[])newValue);
				return;
			case OdxXhtmlPackage.SEGMENT__COMPRESSEDSIZE:
				setCOMPRESSEDSIZE((Long)newValue);
				return;
			case OdxXhtmlPackage.SEGMENT__UNCOMPRESSEDSIZE:
				setUNCOMPRESSEDSIZE((UNCOMPRESSEDSIZE)newValue);
				return;
			case OdxXhtmlPackage.SEGMENT__SOURCEENDADDRESS:
				setSOURCEENDADDRESS((SOURCEENDADDRESS)newValue);
				return;
			case OdxXhtmlPackage.SEGMENT__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.SEGMENT__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SEGMENT__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.SEGMENT__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.SEGMENT__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.SEGMENT__SOURCESTARTADDRESS:
				setSOURCESTARTADDRESS(SOURCESTARTADDRESS_EDEFAULT);
				return;
			case OdxXhtmlPackage.SEGMENT__COMPRESSEDSIZE:
				unsetCOMPRESSEDSIZE();
				return;
			case OdxXhtmlPackage.SEGMENT__UNCOMPRESSEDSIZE:
				setUNCOMPRESSEDSIZE((UNCOMPRESSEDSIZE)null);
				return;
			case OdxXhtmlPackage.SEGMENT__SOURCEENDADDRESS:
				setSOURCEENDADDRESS((SOURCEENDADDRESS)null);
				return;
			case OdxXhtmlPackage.SEGMENT__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.SEGMENT__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SEGMENT__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.SEGMENT__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.SEGMENT__DESC:
				return dESC != null;
			case OdxXhtmlPackage.SEGMENT__SOURCESTARTADDRESS:
				return SOURCESTARTADDRESS_EDEFAULT == null ? sOURCESTARTADDRESS != null : !SOURCESTARTADDRESS_EDEFAULT.equals(sOURCESTARTADDRESS);
			case OdxXhtmlPackage.SEGMENT__COMPRESSEDSIZE:
				return isSetCOMPRESSEDSIZE();
			case OdxXhtmlPackage.SEGMENT__UNCOMPRESSEDSIZE:
				return uNCOMPRESSEDSIZE != null;
			case OdxXhtmlPackage.SEGMENT__SOURCEENDADDRESS:
				return sOURCEENDADDRESS != null;
			case OdxXhtmlPackage.SEGMENT__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.SEGMENT__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", sOURCESTARTADDRESS: ");
		result.append(sOURCESTARTADDRESS);
		result.append(", cOMPRESSEDSIZE: ");
		if (cOMPRESSEDSIZEESet) result.append(cOMPRESSEDSIZE); else result.append("<unset>");
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //SEGMENTImpl
