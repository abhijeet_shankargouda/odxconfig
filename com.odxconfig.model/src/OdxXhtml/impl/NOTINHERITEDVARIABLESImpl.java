/**
 */
package OdxXhtml.impl;

import OdxXhtml.NOTINHERITEDVARIABLE;
import OdxXhtml.NOTINHERITEDVARIABLES;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NOTINHERITEDVARIABLES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.NOTINHERITEDVARIABLESImpl#getNOTINHERITEDVARIABLE <em>NOTINHERITEDVARIABLE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NOTINHERITEDVARIABLESImpl extends MinimalEObjectImpl.Container implements NOTINHERITEDVARIABLES {
	/**
	 * The cached value of the '{@link #getNOTINHERITEDVARIABLE() <em>NOTINHERITEDVARIABLE</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNOTINHERITEDVARIABLE()
	 * @generated
	 * @ordered
	 */
	protected EList<NOTINHERITEDVARIABLE> nOTINHERITEDVARIABLE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NOTINHERITEDVARIABLESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getNOTINHERITEDVARIABLES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<NOTINHERITEDVARIABLE> getNOTINHERITEDVARIABLE() {
		if (nOTINHERITEDVARIABLE == null) {
			nOTINHERITEDVARIABLE = new EObjectContainmentEList<NOTINHERITEDVARIABLE>(NOTINHERITEDVARIABLE.class, this, OdxXhtmlPackage.NOTINHERITEDVARIABLES__NOTINHERITEDVARIABLE);
		}
		return nOTINHERITEDVARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLES__NOTINHERITEDVARIABLE:
				return ((InternalEList<?>)getNOTINHERITEDVARIABLE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLES__NOTINHERITEDVARIABLE:
				return getNOTINHERITEDVARIABLE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLES__NOTINHERITEDVARIABLE:
				getNOTINHERITEDVARIABLE().clear();
				getNOTINHERITEDVARIABLE().addAll((Collection<? extends NOTINHERITEDVARIABLE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLES__NOTINHERITEDVARIABLE:
				getNOTINHERITEDVARIABLE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NOTINHERITEDVARIABLES__NOTINHERITEDVARIABLE:
				return nOTINHERITEDVARIABLE != null && !nOTINHERITEDVARIABLE.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //NOTINHERITEDVARIABLESImpl
