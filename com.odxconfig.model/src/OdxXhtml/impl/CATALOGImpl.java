/**
 */
package OdxXhtml.impl;

import OdxXhtml.ABLOCKSType;
import OdxXhtml.ADMINDATA1;
import OdxXhtml.CATALOG;
import OdxXhtml.COMPANYDATASType;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CATALOG</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.CATALOGImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.CATALOGImpl#getCOMPANYDATAS <em>COMPANYDATAS</em>}</li>
 *   <li>{@link OdxXhtml.impl.CATALOGImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.CATALOGImpl#getABLOCKS <em>ABLOCKS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CATALOGImpl extends MinimalEObjectImpl.Container implements CATALOG {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCOMPANYDATAS() <em>COMPANYDATAS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDATAS()
	 * @generated
	 * @ordered
	 */
	protected COMPANYDATASType cOMPANYDATAS;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA1 aDMINDATA;

	/**
	 * The cached value of the '{@link #getABLOCKS() <em>ABLOCKS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABLOCKS()
	 * @generated
	 * @ordered
	 */
	protected ABLOCKSType aBLOCKS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CATALOGImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCATALOG();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CATALOG__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYDATASType getCOMPANYDATAS() {
		return cOMPANYDATAS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYDATAS(COMPANYDATASType newCOMPANYDATAS, NotificationChain msgs) {
		COMPANYDATASType oldCOMPANYDATAS = cOMPANYDATAS;
		cOMPANYDATAS = newCOMPANYDATAS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CATALOG__COMPANYDATAS, oldCOMPANYDATAS, newCOMPANYDATAS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYDATAS(COMPANYDATASType newCOMPANYDATAS) {
		if (newCOMPANYDATAS != cOMPANYDATAS) {
			NotificationChain msgs = null;
			if (cOMPANYDATAS != null)
				msgs = ((InternalEObject)cOMPANYDATAS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CATALOG__COMPANYDATAS, null, msgs);
			if (newCOMPANYDATAS != null)
				msgs = ((InternalEObject)newCOMPANYDATAS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CATALOG__COMPANYDATAS, null, msgs);
			msgs = basicSetCOMPANYDATAS(newCOMPANYDATAS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CATALOG__COMPANYDATAS, newCOMPANYDATAS, newCOMPANYDATAS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA1 getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA1 newADMINDATA, NotificationChain msgs) {
		ADMINDATA1 oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CATALOG__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA1 newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CATALOG__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CATALOG__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CATALOG__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ABLOCKSType getABLOCKS() {
		return aBLOCKS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetABLOCKS(ABLOCKSType newABLOCKS, NotificationChain msgs) {
		ABLOCKSType oldABLOCKS = aBLOCKS;
		aBLOCKS = newABLOCKS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CATALOG__ABLOCKS, oldABLOCKS, newABLOCKS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setABLOCKS(ABLOCKSType newABLOCKS) {
		if (newABLOCKS != aBLOCKS) {
			NotificationChain msgs = null;
			if (aBLOCKS != null)
				msgs = ((InternalEObject)aBLOCKS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CATALOG__ABLOCKS, null, msgs);
			if (newABLOCKS != null)
				msgs = ((InternalEObject)newABLOCKS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.CATALOG__ABLOCKS, null, msgs);
			msgs = basicSetABLOCKS(newABLOCKS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.CATALOG__ABLOCKS, newABLOCKS, newABLOCKS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.CATALOG__COMPANYDATAS:
				return basicSetCOMPANYDATAS(null, msgs);
			case OdxXhtmlPackage.CATALOG__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.CATALOG__ABLOCKS:
				return basicSetABLOCKS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.CATALOG__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.CATALOG__COMPANYDATAS:
				return getCOMPANYDATAS();
			case OdxXhtmlPackage.CATALOG__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.CATALOG__ABLOCKS:
				return getABLOCKS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.CATALOG__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.CATALOG__COMPANYDATAS:
				setCOMPANYDATAS((COMPANYDATASType)newValue);
				return;
			case OdxXhtmlPackage.CATALOG__ADMINDATA:
				setADMINDATA((ADMINDATA1)newValue);
				return;
			case OdxXhtmlPackage.CATALOG__ABLOCKS:
				setABLOCKS((ABLOCKSType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CATALOG__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.CATALOG__COMPANYDATAS:
				setCOMPANYDATAS((COMPANYDATASType)null);
				return;
			case OdxXhtmlPackage.CATALOG__ADMINDATA:
				setADMINDATA((ADMINDATA1)null);
				return;
			case OdxXhtmlPackage.CATALOG__ABLOCKS:
				setABLOCKS((ABLOCKSType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CATALOG__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.CATALOG__COMPANYDATAS:
				return cOMPANYDATAS != null;
			case OdxXhtmlPackage.CATALOG__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.CATALOG__ABLOCKS:
				return aBLOCKS != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(')');
		return result.toString();
	}

} //CATALOGImpl
