/**
 */
package OdxXhtml.impl;

import OdxXhtml.BASEVARIANTREF;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BASEVARIANTREF</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BASEVARIANTREFImpl extends PARENTREFImpl implements BASEVARIANTREF {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BASEVARIANTREFImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getBASEVARIANTREF();
	}

} //BASEVARIANTREFImpl
