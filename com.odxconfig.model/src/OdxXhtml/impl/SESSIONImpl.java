/**
 */
package OdxXhtml.impl;

import OdxXhtml.CHECKSUMS;
import OdxXhtml.DATABLOCKREFS;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.EXPECTEDIDENTS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SDGS;
import OdxXhtml.SECURITYS;
import OdxXhtml.SESSION;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SESSION</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getEXPECTEDIDENTS <em>EXPECTEDIDENTS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getCHECKSUMS <em>CHECKSUMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getSECURITYS <em>SECURITYS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getDATABLOCKREFS <em>DATABLOCKREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.SESSIONImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SESSIONImpl extends MinimalEObjectImpl.Container implements SESSION {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getEXPECTEDIDENTS() <em>EXPECTEDIDENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEXPECTEDIDENTS()
	 * @generated
	 * @ordered
	 */
	protected EXPECTEDIDENTS eXPECTEDIDENTS;

	/**
	 * The cached value of the '{@link #getCHECKSUMS() <em>CHECKSUMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCHECKSUMS()
	 * @generated
	 * @ordered
	 */
	protected CHECKSUMS cHECKSUMS;

	/**
	 * The cached value of the '{@link #getSECURITYS() <em>SECURITYS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSECURITYS()
	 * @generated
	 * @ordered
	 */
	protected SECURITYS sECURITYS;

	/**
	 * The cached value of the '{@link #getDATABLOCKREFS() <em>DATABLOCKREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATABLOCKREFS()
	 * @generated
	 * @ordered
	 */
	protected DATABLOCKREFS dATABLOCKREFS;

	/**
	 * The cached value of the '{@link #getSDGS() <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGS()
	 * @generated
	 * @ordered
	 */
	protected SDGS sDGS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SESSIONImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSESSION();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EXPECTEDIDENTS getEXPECTEDIDENTS() {
		return eXPECTEDIDENTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEXPECTEDIDENTS(EXPECTEDIDENTS newEXPECTEDIDENTS, NotificationChain msgs) {
		EXPECTEDIDENTS oldEXPECTEDIDENTS = eXPECTEDIDENTS;
		eXPECTEDIDENTS = newEXPECTEDIDENTS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__EXPECTEDIDENTS, oldEXPECTEDIDENTS, newEXPECTEDIDENTS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEXPECTEDIDENTS(EXPECTEDIDENTS newEXPECTEDIDENTS) {
		if (newEXPECTEDIDENTS != eXPECTEDIDENTS) {
			NotificationChain msgs = null;
			if (eXPECTEDIDENTS != null)
				msgs = ((InternalEObject)eXPECTEDIDENTS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__EXPECTEDIDENTS, null, msgs);
			if (newEXPECTEDIDENTS != null)
				msgs = ((InternalEObject)newEXPECTEDIDENTS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__EXPECTEDIDENTS, null, msgs);
			msgs = basicSetEXPECTEDIDENTS(newEXPECTEDIDENTS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__EXPECTEDIDENTS, newEXPECTEDIDENTS, newEXPECTEDIDENTS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CHECKSUMS getCHECKSUMS() {
		return cHECKSUMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCHECKSUMS(CHECKSUMS newCHECKSUMS, NotificationChain msgs) {
		CHECKSUMS oldCHECKSUMS = cHECKSUMS;
		cHECKSUMS = newCHECKSUMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__CHECKSUMS, oldCHECKSUMS, newCHECKSUMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCHECKSUMS(CHECKSUMS newCHECKSUMS) {
		if (newCHECKSUMS != cHECKSUMS) {
			NotificationChain msgs = null;
			if (cHECKSUMS != null)
				msgs = ((InternalEObject)cHECKSUMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__CHECKSUMS, null, msgs);
			if (newCHECKSUMS != null)
				msgs = ((InternalEObject)newCHECKSUMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__CHECKSUMS, null, msgs);
			msgs = basicSetCHECKSUMS(newCHECKSUMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__CHECKSUMS, newCHECKSUMS, newCHECKSUMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SECURITYS getSECURITYS() {
		return sECURITYS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSECURITYS(SECURITYS newSECURITYS, NotificationChain msgs) {
		SECURITYS oldSECURITYS = sECURITYS;
		sECURITYS = newSECURITYS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__SECURITYS, oldSECURITYS, newSECURITYS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSECURITYS(SECURITYS newSECURITYS) {
		if (newSECURITYS != sECURITYS) {
			NotificationChain msgs = null;
			if (sECURITYS != null)
				msgs = ((InternalEObject)sECURITYS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__SECURITYS, null, msgs);
			if (newSECURITYS != null)
				msgs = ((InternalEObject)newSECURITYS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__SECURITYS, null, msgs);
			msgs = basicSetSECURITYS(newSECURITYS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__SECURITYS, newSECURITYS, newSECURITYS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATABLOCKREFS getDATABLOCKREFS() {
		return dATABLOCKREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDATABLOCKREFS(DATABLOCKREFS newDATABLOCKREFS, NotificationChain msgs) {
		DATABLOCKREFS oldDATABLOCKREFS = dATABLOCKREFS;
		dATABLOCKREFS = newDATABLOCKREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__DATABLOCKREFS, oldDATABLOCKREFS, newDATABLOCKREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATABLOCKREFS(DATABLOCKREFS newDATABLOCKREFS) {
		if (newDATABLOCKREFS != dATABLOCKREFS) {
			NotificationChain msgs = null;
			if (dATABLOCKREFS != null)
				msgs = ((InternalEObject)dATABLOCKREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__DATABLOCKREFS, null, msgs);
			if (newDATABLOCKREFS != null)
				msgs = ((InternalEObject)newDATABLOCKREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__DATABLOCKREFS, null, msgs);
			msgs = basicSetDATABLOCKREFS(newDATABLOCKREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__DATABLOCKREFS, newDATABLOCKREFS, newDATABLOCKREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS getSDGS() {
		return sDGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGS(SDGS newSDGS, NotificationChain msgs) {
		SDGS oldSDGS = sDGS;
		sDGS = newSDGS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__SDGS, oldSDGS, newSDGS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGS(SDGS newSDGS) {
		if (newSDGS != sDGS) {
			NotificationChain msgs = null;
			if (sDGS != null)
				msgs = ((InternalEObject)sDGS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__SDGS, null, msgs);
			if (newSDGS != null)
				msgs = ((InternalEObject)newSDGS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SESSION__SDGS, null, msgs);
			msgs = basicSetSDGS(newSDGS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__SDGS, newSDGS, newSDGS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SESSION__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSION__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.SESSION__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.SESSION__EXPECTEDIDENTS:
				return basicSetEXPECTEDIDENTS(null, msgs);
			case OdxXhtmlPackage.SESSION__CHECKSUMS:
				return basicSetCHECKSUMS(null, msgs);
			case OdxXhtmlPackage.SESSION__SECURITYS:
				return basicSetSECURITYS(null, msgs);
			case OdxXhtmlPackage.SESSION__DATABLOCKREFS:
				return basicSetDATABLOCKREFS(null, msgs);
			case OdxXhtmlPackage.SESSION__SDGS:
				return basicSetSDGS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSION__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.SESSION__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.SESSION__DESC:
				return getDESC();
			case OdxXhtmlPackage.SESSION__EXPECTEDIDENTS:
				return getEXPECTEDIDENTS();
			case OdxXhtmlPackage.SESSION__CHECKSUMS:
				return getCHECKSUMS();
			case OdxXhtmlPackage.SESSION__SECURITYS:
				return getSECURITYS();
			case OdxXhtmlPackage.SESSION__DATABLOCKREFS:
				return getDATABLOCKREFS();
			case OdxXhtmlPackage.SESSION__SDGS:
				return getSDGS();
			case OdxXhtmlPackage.SESSION__ID:
				return getID();
			case OdxXhtmlPackage.SESSION__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSION__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.SESSION__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.SESSION__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.SESSION__EXPECTEDIDENTS:
				setEXPECTEDIDENTS((EXPECTEDIDENTS)newValue);
				return;
			case OdxXhtmlPackage.SESSION__CHECKSUMS:
				setCHECKSUMS((CHECKSUMS)newValue);
				return;
			case OdxXhtmlPackage.SESSION__SECURITYS:
				setSECURITYS((SECURITYS)newValue);
				return;
			case OdxXhtmlPackage.SESSION__DATABLOCKREFS:
				setDATABLOCKREFS((DATABLOCKREFS)newValue);
				return;
			case OdxXhtmlPackage.SESSION__SDGS:
				setSDGS((SDGS)newValue);
				return;
			case OdxXhtmlPackage.SESSION__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.SESSION__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSION__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.SESSION__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.SESSION__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.SESSION__EXPECTEDIDENTS:
				setEXPECTEDIDENTS((EXPECTEDIDENTS)null);
				return;
			case OdxXhtmlPackage.SESSION__CHECKSUMS:
				setCHECKSUMS((CHECKSUMS)null);
				return;
			case OdxXhtmlPackage.SESSION__SECURITYS:
				setSECURITYS((SECURITYS)null);
				return;
			case OdxXhtmlPackage.SESSION__DATABLOCKREFS:
				setDATABLOCKREFS((DATABLOCKREFS)null);
				return;
			case OdxXhtmlPackage.SESSION__SDGS:
				setSDGS((SDGS)null);
				return;
			case OdxXhtmlPackage.SESSION__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.SESSION__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SESSION__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.SESSION__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.SESSION__DESC:
				return dESC != null;
			case OdxXhtmlPackage.SESSION__EXPECTEDIDENTS:
				return eXPECTEDIDENTS != null;
			case OdxXhtmlPackage.SESSION__CHECKSUMS:
				return cHECKSUMS != null;
			case OdxXhtmlPackage.SESSION__SECURITYS:
				return sECURITYS != null;
			case OdxXhtmlPackage.SESSION__DATABLOCKREFS:
				return dATABLOCKREFS != null;
			case OdxXhtmlPackage.SESSION__SDGS:
				return sDGS != null;
			case OdxXhtmlPackage.SESSION__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.SESSION__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //SESSIONImpl
