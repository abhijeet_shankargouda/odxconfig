/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION1;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.RELATEDDOC1;
import OdxXhtml.XDOC1;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RELATEDDOC1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.RELATEDDOC1Impl#getXDOC <em>XDOC</em>}</li>
 *   <li>{@link OdxXhtml.impl.RELATEDDOC1Impl#getDESC <em>DESC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RELATEDDOC1Impl extends MinimalEObjectImpl.Container implements RELATEDDOC1 {
	/**
	 * The cached value of the '{@link #getXDOC() <em>XDOC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXDOC()
	 * @generated
	 * @ordered
	 */
	protected XDOC1 xDOC;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION1 dESC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RELATEDDOC1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getRELATEDDOC1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public XDOC1 getXDOC() {
		return xDOC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetXDOC(XDOC1 newXDOC, NotificationChain msgs) {
		XDOC1 oldXDOC = xDOC;
		xDOC = newXDOC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.RELATEDDOC1__XDOC, oldXDOC, newXDOC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setXDOC(XDOC1 newXDOC) {
		if (newXDOC != xDOC) {
			NotificationChain msgs = null;
			if (xDOC != null)
				msgs = ((InternalEObject)xDOC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.RELATEDDOC1__XDOC, null, msgs);
			if (newXDOC != null)
				msgs = ((InternalEObject)newXDOC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.RELATEDDOC1__XDOC, null, msgs);
			msgs = basicSetXDOC(newXDOC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.RELATEDDOC1__XDOC, newXDOC, newXDOC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION1 getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION1 newDESC, NotificationChain msgs) {
		DESCRIPTION1 oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.RELATEDDOC1__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION1 newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.RELATEDDOC1__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.RELATEDDOC1__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.RELATEDDOC1__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOC1__XDOC:
				return basicSetXDOC(null, msgs);
			case OdxXhtmlPackage.RELATEDDOC1__DESC:
				return basicSetDESC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOC1__XDOC:
				return getXDOC();
			case OdxXhtmlPackage.RELATEDDOC1__DESC:
				return getDESC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOC1__XDOC:
				setXDOC((XDOC1)newValue);
				return;
			case OdxXhtmlPackage.RELATEDDOC1__DESC:
				setDESC((DESCRIPTION1)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOC1__XDOC:
				setXDOC((XDOC1)null);
				return;
			case OdxXhtmlPackage.RELATEDDOC1__DESC:
				setDESC((DESCRIPTION1)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOC1__XDOC:
				return xDOC != null;
			case OdxXhtmlPackage.RELATEDDOC1__DESC:
				return dESC != null;
		}
		return super.eIsSet(featureID);
	}

} //RELATEDDOC1Impl
