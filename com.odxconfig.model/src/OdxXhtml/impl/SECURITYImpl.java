/**
 */
package OdxXhtml.impl;

import OdxXhtml.FWCHECKSUM;
import OdxXhtml.FWSIGNATURE;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SECURITY;
import OdxXhtml.SECURITYMETHOD;
import OdxXhtml.VALIDITYFOR;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SECURITY</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SECURITYImpl#getSECURITYMETHOD <em>SECURITYMETHOD</em>}</li>
 *   <li>{@link OdxXhtml.impl.SECURITYImpl#getFWSIGNATURE <em>FWSIGNATURE</em>}</li>
 *   <li>{@link OdxXhtml.impl.SECURITYImpl#getFWCHECKSUM <em>FWCHECKSUM</em>}</li>
 *   <li>{@link OdxXhtml.impl.SECURITYImpl#getVALIDITYFOR <em>VALIDITYFOR</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SECURITYImpl extends MinimalEObjectImpl.Container implements SECURITY {
	/**
	 * The cached value of the '{@link #getSECURITYMETHOD() <em>SECURITYMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSECURITYMETHOD()
	 * @generated
	 * @ordered
	 */
	protected SECURITYMETHOD sECURITYMETHOD;

	/**
	 * The cached value of the '{@link #getFWSIGNATURE() <em>FWSIGNATURE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFWSIGNATURE()
	 * @generated
	 * @ordered
	 */
	protected FWSIGNATURE fWSIGNATURE;

	/**
	 * The cached value of the '{@link #getFWCHECKSUM() <em>FWCHECKSUM</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFWCHECKSUM()
	 * @generated
	 * @ordered
	 */
	protected FWCHECKSUM fWCHECKSUM;

	/**
	 * The cached value of the '{@link #getVALIDITYFOR() <em>VALIDITYFOR</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALIDITYFOR()
	 * @generated
	 * @ordered
	 */
	protected VALIDITYFOR vALIDITYFOR;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SECURITYImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSECURITY();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SECURITYMETHOD getSECURITYMETHOD() {
		return sECURITYMETHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSECURITYMETHOD(SECURITYMETHOD newSECURITYMETHOD, NotificationChain msgs) {
		SECURITYMETHOD oldSECURITYMETHOD = sECURITYMETHOD;
		sECURITYMETHOD = newSECURITYMETHOD;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SECURITY__SECURITYMETHOD, oldSECURITYMETHOD, newSECURITYMETHOD);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSECURITYMETHOD(SECURITYMETHOD newSECURITYMETHOD) {
		if (newSECURITYMETHOD != sECURITYMETHOD) {
			NotificationChain msgs = null;
			if (sECURITYMETHOD != null)
				msgs = ((InternalEObject)sECURITYMETHOD).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SECURITY__SECURITYMETHOD, null, msgs);
			if (newSECURITYMETHOD != null)
				msgs = ((InternalEObject)newSECURITYMETHOD).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SECURITY__SECURITYMETHOD, null, msgs);
			msgs = basicSetSECURITYMETHOD(newSECURITYMETHOD, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SECURITY__SECURITYMETHOD, newSECURITYMETHOD, newSECURITYMETHOD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FWSIGNATURE getFWSIGNATURE() {
		return fWSIGNATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFWSIGNATURE(FWSIGNATURE newFWSIGNATURE, NotificationChain msgs) {
		FWSIGNATURE oldFWSIGNATURE = fWSIGNATURE;
		fWSIGNATURE = newFWSIGNATURE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SECURITY__FWSIGNATURE, oldFWSIGNATURE, newFWSIGNATURE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFWSIGNATURE(FWSIGNATURE newFWSIGNATURE) {
		if (newFWSIGNATURE != fWSIGNATURE) {
			NotificationChain msgs = null;
			if (fWSIGNATURE != null)
				msgs = ((InternalEObject)fWSIGNATURE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SECURITY__FWSIGNATURE, null, msgs);
			if (newFWSIGNATURE != null)
				msgs = ((InternalEObject)newFWSIGNATURE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SECURITY__FWSIGNATURE, null, msgs);
			msgs = basicSetFWSIGNATURE(newFWSIGNATURE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SECURITY__FWSIGNATURE, newFWSIGNATURE, newFWSIGNATURE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FWCHECKSUM getFWCHECKSUM() {
		return fWCHECKSUM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFWCHECKSUM(FWCHECKSUM newFWCHECKSUM, NotificationChain msgs) {
		FWCHECKSUM oldFWCHECKSUM = fWCHECKSUM;
		fWCHECKSUM = newFWCHECKSUM;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SECURITY__FWCHECKSUM, oldFWCHECKSUM, newFWCHECKSUM);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFWCHECKSUM(FWCHECKSUM newFWCHECKSUM) {
		if (newFWCHECKSUM != fWCHECKSUM) {
			NotificationChain msgs = null;
			if (fWCHECKSUM != null)
				msgs = ((InternalEObject)fWCHECKSUM).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SECURITY__FWCHECKSUM, null, msgs);
			if (newFWCHECKSUM != null)
				msgs = ((InternalEObject)newFWCHECKSUM).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SECURITY__FWCHECKSUM, null, msgs);
			msgs = basicSetFWCHECKSUM(newFWCHECKSUM, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SECURITY__FWCHECKSUM, newFWCHECKSUM, newFWCHECKSUM));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VALIDITYFOR getVALIDITYFOR() {
		return vALIDITYFOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVALIDITYFOR(VALIDITYFOR newVALIDITYFOR, NotificationChain msgs) {
		VALIDITYFOR oldVALIDITYFOR = vALIDITYFOR;
		vALIDITYFOR = newVALIDITYFOR;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SECURITY__VALIDITYFOR, oldVALIDITYFOR, newVALIDITYFOR);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVALIDITYFOR(VALIDITYFOR newVALIDITYFOR) {
		if (newVALIDITYFOR != vALIDITYFOR) {
			NotificationChain msgs = null;
			if (vALIDITYFOR != null)
				msgs = ((InternalEObject)vALIDITYFOR).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SECURITY__VALIDITYFOR, null, msgs);
			if (newVALIDITYFOR != null)
				msgs = ((InternalEObject)newVALIDITYFOR).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SECURITY__VALIDITYFOR, null, msgs);
			msgs = basicSetVALIDITYFOR(newVALIDITYFOR, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SECURITY__VALIDITYFOR, newVALIDITYFOR, newVALIDITYFOR));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SECURITY__SECURITYMETHOD:
				return basicSetSECURITYMETHOD(null, msgs);
			case OdxXhtmlPackage.SECURITY__FWSIGNATURE:
				return basicSetFWSIGNATURE(null, msgs);
			case OdxXhtmlPackage.SECURITY__FWCHECKSUM:
				return basicSetFWCHECKSUM(null, msgs);
			case OdxXhtmlPackage.SECURITY__VALIDITYFOR:
				return basicSetVALIDITYFOR(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SECURITY__SECURITYMETHOD:
				return getSECURITYMETHOD();
			case OdxXhtmlPackage.SECURITY__FWSIGNATURE:
				return getFWSIGNATURE();
			case OdxXhtmlPackage.SECURITY__FWCHECKSUM:
				return getFWCHECKSUM();
			case OdxXhtmlPackage.SECURITY__VALIDITYFOR:
				return getVALIDITYFOR();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SECURITY__SECURITYMETHOD:
				setSECURITYMETHOD((SECURITYMETHOD)newValue);
				return;
			case OdxXhtmlPackage.SECURITY__FWSIGNATURE:
				setFWSIGNATURE((FWSIGNATURE)newValue);
				return;
			case OdxXhtmlPackage.SECURITY__FWCHECKSUM:
				setFWCHECKSUM((FWCHECKSUM)newValue);
				return;
			case OdxXhtmlPackage.SECURITY__VALIDITYFOR:
				setVALIDITYFOR((VALIDITYFOR)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SECURITY__SECURITYMETHOD:
				setSECURITYMETHOD((SECURITYMETHOD)null);
				return;
			case OdxXhtmlPackage.SECURITY__FWSIGNATURE:
				setFWSIGNATURE((FWSIGNATURE)null);
				return;
			case OdxXhtmlPackage.SECURITY__FWCHECKSUM:
				setFWCHECKSUM((FWCHECKSUM)null);
				return;
			case OdxXhtmlPackage.SECURITY__VALIDITYFOR:
				setVALIDITYFOR((VALIDITYFOR)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SECURITY__SECURITYMETHOD:
				return sECURITYMETHOD != null;
			case OdxXhtmlPackage.SECURITY__FWSIGNATURE:
				return fWSIGNATURE != null;
			case OdxXhtmlPackage.SECURITY__FWCHECKSUM:
				return fWCHECKSUM != null;
			case OdxXhtmlPackage.SECURITY__VALIDITYFOR:
				return vALIDITYFOR != null;
		}
		return super.eIsSet(featureID);
	}

} //SECURITYImpl
