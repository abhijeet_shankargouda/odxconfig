/**
 */
package OdxXhtml.impl;

import OdxXhtml.DYNIDDEFMODEINFO;
import OdxXhtml.DYNIDDEFMODEINFOS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DYNIDDEFMODEINFOS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DYNIDDEFMODEINFOSImpl#getDYNIDDEFMODEINFO <em>DYNIDDEFMODEINFO</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DYNIDDEFMODEINFOSImpl extends MinimalEObjectImpl.Container implements DYNIDDEFMODEINFOS {
	/**
	 * The cached value of the '{@link #getDYNIDDEFMODEINFO() <em>DYNIDDEFMODEINFO</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNIDDEFMODEINFO()
	 * @generated
	 * @ordered
	 */
	protected EList<DYNIDDEFMODEINFO> dYNIDDEFMODEINFO;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DYNIDDEFMODEINFOSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDYNIDDEFMODEINFOS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DYNIDDEFMODEINFO> getDYNIDDEFMODEINFO() {
		if (dYNIDDEFMODEINFO == null) {
			dYNIDDEFMODEINFO = new EObjectContainmentEList<DYNIDDEFMODEINFO>(DYNIDDEFMODEINFO.class, this, OdxXhtmlPackage.DYNIDDEFMODEINFOS__DYNIDDEFMODEINFO);
		}
		return dYNIDDEFMODEINFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFOS__DYNIDDEFMODEINFO:
				return ((InternalEList<?>)getDYNIDDEFMODEINFO()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFOS__DYNIDDEFMODEINFO:
				return getDYNIDDEFMODEINFO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFOS__DYNIDDEFMODEINFO:
				getDYNIDDEFMODEINFO().clear();
				getDYNIDDEFMODEINFO().addAll((Collection<? extends DYNIDDEFMODEINFO>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFOS__DYNIDDEFMODEINFO:
				getDYNIDDEFMODEINFO().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNIDDEFMODEINFOS__DYNIDDEFMODEINFO:
				return dYNIDDEFMODEINFO != null && !dYNIDDEFMODEINFO.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DYNIDDEFMODEINFOSImpl
