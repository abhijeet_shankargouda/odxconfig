/**
 */
package OdxXhtml.impl;

import OdxXhtml.DOCREVISION1;
import OdxXhtml.DOCREVISIONS1;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DOCREVISIONS1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DOCREVISIONS1Impl#getDOCREVISION <em>DOCREVISION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DOCREVISIONS1Impl extends MinimalEObjectImpl.Container implements DOCREVISIONS1 {
	/**
	 * The cached value of the '{@link #getDOCREVISION() <em>DOCREVISION</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREVISION()
	 * @generated
	 * @ordered
	 */
	protected EList<DOCREVISION1> dOCREVISION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DOCREVISIONS1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDOCREVISIONS1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DOCREVISION1> getDOCREVISION() {
		if (dOCREVISION == null) {
			dOCREVISION = new EObjectContainmentEList<DOCREVISION1>(DOCREVISION1.class, this, OdxXhtmlPackage.DOCREVISIONS1__DOCREVISION);
		}
		return dOCREVISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS1__DOCREVISION:
				return ((InternalEList<?>)getDOCREVISION()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS1__DOCREVISION:
				return getDOCREVISION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS1__DOCREVISION:
				getDOCREVISION().clear();
				getDOCREVISION().addAll((Collection<? extends DOCREVISION1>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS1__DOCREVISION:
				getDOCREVISION().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DOCREVISIONS1__DOCREVISION:
				return dOCREVISION != null && !dOCREVISION.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DOCREVISIONS1Impl
