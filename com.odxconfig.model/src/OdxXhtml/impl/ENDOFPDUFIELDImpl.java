/**
 */
package OdxXhtml.impl;

import OdxXhtml.ENDOFPDUFIELD;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ENDOFPDUFIELD</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ENDOFPDUFIELDImpl#getMAXNUMBEROFITEMS <em>MAXNUMBEROFITEMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ENDOFPDUFIELDImpl#getMINNUMBEROFITEMS <em>MINNUMBEROFITEMS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ENDOFPDUFIELDImpl extends FIELDImpl implements ENDOFPDUFIELD {
	/**
	 * The default value of the '{@link #getMAXNUMBEROFITEMS() <em>MAXNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMAXNUMBEROFITEMS()
	 * @generated
	 * @ordered
	 */
	protected static final long MAXNUMBEROFITEMS_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMAXNUMBEROFITEMS() <em>MAXNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMAXNUMBEROFITEMS()
	 * @generated
	 * @ordered
	 */
	protected long mAXNUMBEROFITEMS = MAXNUMBEROFITEMS_EDEFAULT;

	/**
	 * This is true if the MAXNUMBEROFITEMS attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mAXNUMBEROFITEMSESet;

	/**
	 * The default value of the '{@link #getMINNUMBEROFITEMS() <em>MINNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMINNUMBEROFITEMS()
	 * @generated
	 * @ordered
	 */
	protected static final long MINNUMBEROFITEMS_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMINNUMBEROFITEMS() <em>MINNUMBEROFITEMS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMINNUMBEROFITEMS()
	 * @generated
	 * @ordered
	 */
	protected long mINNUMBEROFITEMS = MINNUMBEROFITEMS_EDEFAULT;

	/**
	 * This is true if the MINNUMBEROFITEMS attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mINNUMBEROFITEMSESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ENDOFPDUFIELDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getENDOFPDUFIELD();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getMAXNUMBEROFITEMS() {
		return mAXNUMBEROFITEMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMAXNUMBEROFITEMS(long newMAXNUMBEROFITEMS) {
		long oldMAXNUMBEROFITEMS = mAXNUMBEROFITEMS;
		mAXNUMBEROFITEMS = newMAXNUMBEROFITEMS;
		boolean oldMAXNUMBEROFITEMSESet = mAXNUMBEROFITEMSESet;
		mAXNUMBEROFITEMSESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENDOFPDUFIELD__MAXNUMBEROFITEMS, oldMAXNUMBEROFITEMS, mAXNUMBEROFITEMS, !oldMAXNUMBEROFITEMSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetMAXNUMBEROFITEMS() {
		long oldMAXNUMBEROFITEMS = mAXNUMBEROFITEMS;
		boolean oldMAXNUMBEROFITEMSESet = mAXNUMBEROFITEMSESet;
		mAXNUMBEROFITEMS = MAXNUMBEROFITEMS_EDEFAULT;
		mAXNUMBEROFITEMSESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.ENDOFPDUFIELD__MAXNUMBEROFITEMS, oldMAXNUMBEROFITEMS, MAXNUMBEROFITEMS_EDEFAULT, oldMAXNUMBEROFITEMSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetMAXNUMBEROFITEMS() {
		return mAXNUMBEROFITEMSESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getMINNUMBEROFITEMS() {
		return mINNUMBEROFITEMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMINNUMBEROFITEMS(long newMINNUMBEROFITEMS) {
		long oldMINNUMBEROFITEMS = mINNUMBEROFITEMS;
		mINNUMBEROFITEMS = newMINNUMBEROFITEMS;
		boolean oldMINNUMBEROFITEMSESet = mINNUMBEROFITEMSESet;
		mINNUMBEROFITEMSESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ENDOFPDUFIELD__MINNUMBEROFITEMS, oldMINNUMBEROFITEMS, mINNUMBEROFITEMS, !oldMINNUMBEROFITEMSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetMINNUMBEROFITEMS() {
		long oldMINNUMBEROFITEMS = mINNUMBEROFITEMS;
		boolean oldMINNUMBEROFITEMSESet = mINNUMBEROFITEMSESet;
		mINNUMBEROFITEMS = MINNUMBEROFITEMS_EDEFAULT;
		mINNUMBEROFITEMSESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.ENDOFPDUFIELD__MINNUMBEROFITEMS, oldMINNUMBEROFITEMS, MINNUMBEROFITEMS_EDEFAULT, oldMINNUMBEROFITEMSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetMINNUMBEROFITEMS() {
		return mINNUMBEROFITEMSESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ENDOFPDUFIELD__MAXNUMBEROFITEMS:
				return getMAXNUMBEROFITEMS();
			case OdxXhtmlPackage.ENDOFPDUFIELD__MINNUMBEROFITEMS:
				return getMINNUMBEROFITEMS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ENDOFPDUFIELD__MAXNUMBEROFITEMS:
				setMAXNUMBEROFITEMS((Long)newValue);
				return;
			case OdxXhtmlPackage.ENDOFPDUFIELD__MINNUMBEROFITEMS:
				setMINNUMBEROFITEMS((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENDOFPDUFIELD__MAXNUMBEROFITEMS:
				unsetMAXNUMBEROFITEMS();
				return;
			case OdxXhtmlPackage.ENDOFPDUFIELD__MINNUMBEROFITEMS:
				unsetMINNUMBEROFITEMS();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ENDOFPDUFIELD__MAXNUMBEROFITEMS:
				return isSetMAXNUMBEROFITEMS();
			case OdxXhtmlPackage.ENDOFPDUFIELD__MINNUMBEROFITEMS:
				return isSetMINNUMBEROFITEMS();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mAXNUMBEROFITEMS: ");
		if (mAXNUMBEROFITEMSESet) result.append(mAXNUMBEROFITEMS); else result.append("<unset>");
		result.append(", mINNUMBEROFITEMS: ");
		if (mINNUMBEROFITEMSESet) result.append(mINNUMBEROFITEMS); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ENDOFPDUFIELDImpl
