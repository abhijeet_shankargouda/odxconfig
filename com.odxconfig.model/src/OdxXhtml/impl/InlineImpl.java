/**
 */
package OdxXhtml.impl;

import OdxXhtml.BType;
import OdxXhtml.IType;
import OdxXhtml.Inline;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SubType;
import OdxXhtml.SupType;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inline</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.InlineImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link OdxXhtml.impl.InlineImpl#getInline <em>Inline</em>}</li>
 *   <li>{@link OdxXhtml.impl.InlineImpl#getBr <em>Br</em>}</li>
 *   <li>{@link OdxXhtml.impl.InlineImpl#getI <em>I</em>}</li>
 *   <li>{@link OdxXhtml.impl.InlineImpl#getB <em>B</em>}</li>
 *   <li>{@link OdxXhtml.impl.InlineImpl#getSub <em>Sub</em>}</li>
 *   <li>{@link OdxXhtml.impl.InlineImpl#getSup <em>Sup</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InlineImpl extends MinimalEObjectImpl.Container implements Inline {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InlineImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getInline();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, OdxXhtmlPackage.INLINE__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getInline() {
		return (FeatureMap)getMixed().<FeatureMap.Entry>list(OdxXhtmlPackage.eINSTANCE.getInline_Inline());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<EObject> getBr() {
		return getInline().list(OdxXhtmlPackage.eINSTANCE.getInline_Br());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IType> getI() {
		return getInline().list(OdxXhtmlPackage.eINSTANCE.getInline_I());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BType> getB() {
		return getInline().list(OdxXhtmlPackage.eINSTANCE.getInline_B());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SubType> getSub() {
		return getInline().list(OdxXhtmlPackage.eINSTANCE.getInline_Sub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SupType> getSup() {
		return getInline().list(OdxXhtmlPackage.eINSTANCE.getInline_Sup());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.INLINE__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.INLINE__INLINE:
				return ((InternalEList<?>)getInline()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.INLINE__BR:
				return ((InternalEList<?>)getBr()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.INLINE__I:
				return ((InternalEList<?>)getI()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.INLINE__B:
				return ((InternalEList<?>)getB()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.INLINE__SUB:
				return ((InternalEList<?>)getSub()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.INLINE__SUP:
				return ((InternalEList<?>)getSup()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.INLINE__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case OdxXhtmlPackage.INLINE__INLINE:
				if (coreType) return getInline();
				return ((FeatureMap.Internal)getInline()).getWrapper();
			case OdxXhtmlPackage.INLINE__BR:
				return getBr();
			case OdxXhtmlPackage.INLINE__I:
				return getI();
			case OdxXhtmlPackage.INLINE__B:
				return getB();
			case OdxXhtmlPackage.INLINE__SUB:
				return getSub();
			case OdxXhtmlPackage.INLINE__SUP:
				return getSup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.INLINE__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case OdxXhtmlPackage.INLINE__INLINE:
				((FeatureMap.Internal)getInline()).set(newValue);
				return;
			case OdxXhtmlPackage.INLINE__BR:
				getBr().clear();
				getBr().addAll((Collection<? extends EObject>)newValue);
				return;
			case OdxXhtmlPackage.INLINE__I:
				getI().clear();
				getI().addAll((Collection<? extends IType>)newValue);
				return;
			case OdxXhtmlPackage.INLINE__B:
				getB().clear();
				getB().addAll((Collection<? extends BType>)newValue);
				return;
			case OdxXhtmlPackage.INLINE__SUB:
				getSub().clear();
				getSub().addAll((Collection<? extends SubType>)newValue);
				return;
			case OdxXhtmlPackage.INLINE__SUP:
				getSup().clear();
				getSup().addAll((Collection<? extends SupType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INLINE__MIXED:
				getMixed().clear();
				return;
			case OdxXhtmlPackage.INLINE__INLINE:
				getInline().clear();
				return;
			case OdxXhtmlPackage.INLINE__BR:
				getBr().clear();
				return;
			case OdxXhtmlPackage.INLINE__I:
				getI().clear();
				return;
			case OdxXhtmlPackage.INLINE__B:
				getB().clear();
				return;
			case OdxXhtmlPackage.INLINE__SUB:
				getSub().clear();
				return;
			case OdxXhtmlPackage.INLINE__SUP:
				getSup().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INLINE__MIXED:
				return mixed != null && !mixed.isEmpty();
			case OdxXhtmlPackage.INLINE__INLINE:
				return !getInline().isEmpty();
			case OdxXhtmlPackage.INLINE__BR:
				return !getBr().isEmpty();
			case OdxXhtmlPackage.INLINE__I:
				return !getI().isEmpty();
			case OdxXhtmlPackage.INLINE__B:
				return !getB().isEmpty();
			case OdxXhtmlPackage.INLINE__SUB:
				return !getSub().isEmpty();
			case OdxXhtmlPackage.INLINE__SUP:
				return !getSup().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //InlineImpl
