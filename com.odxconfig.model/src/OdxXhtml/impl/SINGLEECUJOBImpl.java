/**
 */
package OdxXhtml.impl;

import OdxXhtml.INPUTPARAMS;
import OdxXhtml.NEGOUTPUTPARAMS;
import OdxXhtml.OUTPUTPARAMS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROGCODES;
import OdxXhtml.SINGLEECUJOB;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SINGLEECUJOB</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SINGLEECUJOBImpl#getPROGCODES <em>PROGCODES</em>}</li>
 *   <li>{@link OdxXhtml.impl.SINGLEECUJOBImpl#getINPUTPARAMS <em>INPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SINGLEECUJOBImpl#getOUTPUTPARAMS <em>OUTPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SINGLEECUJOBImpl#getNEGOUTPUTPARAMS <em>NEGOUTPUTPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.SINGLEECUJOBImpl#isISREDUCEDRESULTENABLED <em>ISREDUCEDRESULTENABLED</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SINGLEECUJOBImpl extends DIAGCOMMImpl implements SINGLEECUJOB {
	/**
	 * The cached value of the '{@link #getPROGCODES() <em>PROGCODES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGCODES()
	 * @generated
	 * @ordered
	 */
	protected PROGCODES pROGCODES;

	/**
	 * The cached value of the '{@link #getINPUTPARAMS() <em>INPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getINPUTPARAMS()
	 * @generated
	 * @ordered
	 */
	protected INPUTPARAMS iNPUTPARAMS;

	/**
	 * The cached value of the '{@link #getOUTPUTPARAMS() <em>OUTPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOUTPUTPARAMS()
	 * @generated
	 * @ordered
	 */
	protected OUTPUTPARAMS oUTPUTPARAMS;

	/**
	 * The cached value of the '{@link #getNEGOUTPUTPARAMS() <em>NEGOUTPUTPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNEGOUTPUTPARAMS()
	 * @generated
	 * @ordered
	 */
	protected NEGOUTPUTPARAMS nEGOUTPUTPARAMS;

	/**
	 * The default value of the '{@link #isISREDUCEDRESULTENABLED() <em>ISREDUCEDRESULTENABLED</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISREDUCEDRESULTENABLED()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISREDUCEDRESULTENABLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISREDUCEDRESULTENABLED() <em>ISREDUCEDRESULTENABLED</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISREDUCEDRESULTENABLED()
	 * @generated
	 * @ordered
	 */
	protected boolean iSREDUCEDRESULTENABLED = ISREDUCEDRESULTENABLED_EDEFAULT;

	/**
	 * This is true if the ISREDUCEDRESULTENABLED attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSREDUCEDRESULTENABLEDESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SINGLEECUJOBImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSINGLEECUJOB();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROGCODES getPROGCODES() {
		return pROGCODES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROGCODES(PROGCODES newPROGCODES, NotificationChain msgs) {
		PROGCODES oldPROGCODES = pROGCODES;
		pROGCODES = newPROGCODES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SINGLEECUJOB__PROGCODES, oldPROGCODES, newPROGCODES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROGCODES(PROGCODES newPROGCODES) {
		if (newPROGCODES != pROGCODES) {
			NotificationChain msgs = null;
			if (pROGCODES != null)
				msgs = ((InternalEObject)pROGCODES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SINGLEECUJOB__PROGCODES, null, msgs);
			if (newPROGCODES != null)
				msgs = ((InternalEObject)newPROGCODES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SINGLEECUJOB__PROGCODES, null, msgs);
			msgs = basicSetPROGCODES(newPROGCODES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SINGLEECUJOB__PROGCODES, newPROGCODES, newPROGCODES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INPUTPARAMS getINPUTPARAMS() {
		return iNPUTPARAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetINPUTPARAMS(INPUTPARAMS newINPUTPARAMS, NotificationChain msgs) {
		INPUTPARAMS oldINPUTPARAMS = iNPUTPARAMS;
		iNPUTPARAMS = newINPUTPARAMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SINGLEECUJOB__INPUTPARAMS, oldINPUTPARAMS, newINPUTPARAMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setINPUTPARAMS(INPUTPARAMS newINPUTPARAMS) {
		if (newINPUTPARAMS != iNPUTPARAMS) {
			NotificationChain msgs = null;
			if (iNPUTPARAMS != null)
				msgs = ((InternalEObject)iNPUTPARAMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SINGLEECUJOB__INPUTPARAMS, null, msgs);
			if (newINPUTPARAMS != null)
				msgs = ((InternalEObject)newINPUTPARAMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SINGLEECUJOB__INPUTPARAMS, null, msgs);
			msgs = basicSetINPUTPARAMS(newINPUTPARAMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SINGLEECUJOB__INPUTPARAMS, newINPUTPARAMS, newINPUTPARAMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OUTPUTPARAMS getOUTPUTPARAMS() {
		return oUTPUTPARAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOUTPUTPARAMS(OUTPUTPARAMS newOUTPUTPARAMS, NotificationChain msgs) {
		OUTPUTPARAMS oldOUTPUTPARAMS = oUTPUTPARAMS;
		oUTPUTPARAMS = newOUTPUTPARAMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SINGLEECUJOB__OUTPUTPARAMS, oldOUTPUTPARAMS, newOUTPUTPARAMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOUTPUTPARAMS(OUTPUTPARAMS newOUTPUTPARAMS) {
		if (newOUTPUTPARAMS != oUTPUTPARAMS) {
			NotificationChain msgs = null;
			if (oUTPUTPARAMS != null)
				msgs = ((InternalEObject)oUTPUTPARAMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SINGLEECUJOB__OUTPUTPARAMS, null, msgs);
			if (newOUTPUTPARAMS != null)
				msgs = ((InternalEObject)newOUTPUTPARAMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SINGLEECUJOB__OUTPUTPARAMS, null, msgs);
			msgs = basicSetOUTPUTPARAMS(newOUTPUTPARAMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SINGLEECUJOB__OUTPUTPARAMS, newOUTPUTPARAMS, newOUTPUTPARAMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGOUTPUTPARAMS getNEGOUTPUTPARAMS() {
		return nEGOUTPUTPARAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNEGOUTPUTPARAMS(NEGOUTPUTPARAMS newNEGOUTPUTPARAMS, NotificationChain msgs) {
		NEGOUTPUTPARAMS oldNEGOUTPUTPARAMS = nEGOUTPUTPARAMS;
		nEGOUTPUTPARAMS = newNEGOUTPUTPARAMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SINGLEECUJOB__NEGOUTPUTPARAMS, oldNEGOUTPUTPARAMS, newNEGOUTPUTPARAMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNEGOUTPUTPARAMS(NEGOUTPUTPARAMS newNEGOUTPUTPARAMS) {
		if (newNEGOUTPUTPARAMS != nEGOUTPUTPARAMS) {
			NotificationChain msgs = null;
			if (nEGOUTPUTPARAMS != null)
				msgs = ((InternalEObject)nEGOUTPUTPARAMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SINGLEECUJOB__NEGOUTPUTPARAMS, null, msgs);
			if (newNEGOUTPUTPARAMS != null)
				msgs = ((InternalEObject)newNEGOUTPUTPARAMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.SINGLEECUJOB__NEGOUTPUTPARAMS, null, msgs);
			msgs = basicSetNEGOUTPUTPARAMS(newNEGOUTPUTPARAMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SINGLEECUJOB__NEGOUTPUTPARAMS, newNEGOUTPUTPARAMS, newNEGOUTPUTPARAMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISREDUCEDRESULTENABLED() {
		return iSREDUCEDRESULTENABLED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISREDUCEDRESULTENABLED(boolean newISREDUCEDRESULTENABLED) {
		boolean oldISREDUCEDRESULTENABLED = iSREDUCEDRESULTENABLED;
		iSREDUCEDRESULTENABLED = newISREDUCEDRESULTENABLED;
		boolean oldISREDUCEDRESULTENABLEDESet = iSREDUCEDRESULTENABLEDESet;
		iSREDUCEDRESULTENABLEDESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.SINGLEECUJOB__ISREDUCEDRESULTENABLED, oldISREDUCEDRESULTENABLED, iSREDUCEDRESULTENABLED, !oldISREDUCEDRESULTENABLEDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISREDUCEDRESULTENABLED() {
		boolean oldISREDUCEDRESULTENABLED = iSREDUCEDRESULTENABLED;
		boolean oldISREDUCEDRESULTENABLEDESet = iSREDUCEDRESULTENABLEDESet;
		iSREDUCEDRESULTENABLED = ISREDUCEDRESULTENABLED_EDEFAULT;
		iSREDUCEDRESULTENABLEDESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.SINGLEECUJOB__ISREDUCEDRESULTENABLED, oldISREDUCEDRESULTENABLED, ISREDUCEDRESULTENABLED_EDEFAULT, oldISREDUCEDRESULTENABLEDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISREDUCEDRESULTENABLED() {
		return iSREDUCEDRESULTENABLEDESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SINGLEECUJOB__PROGCODES:
				return basicSetPROGCODES(null, msgs);
			case OdxXhtmlPackage.SINGLEECUJOB__INPUTPARAMS:
				return basicSetINPUTPARAMS(null, msgs);
			case OdxXhtmlPackage.SINGLEECUJOB__OUTPUTPARAMS:
				return basicSetOUTPUTPARAMS(null, msgs);
			case OdxXhtmlPackage.SINGLEECUJOB__NEGOUTPUTPARAMS:
				return basicSetNEGOUTPUTPARAMS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SINGLEECUJOB__PROGCODES:
				return getPROGCODES();
			case OdxXhtmlPackage.SINGLEECUJOB__INPUTPARAMS:
				return getINPUTPARAMS();
			case OdxXhtmlPackage.SINGLEECUJOB__OUTPUTPARAMS:
				return getOUTPUTPARAMS();
			case OdxXhtmlPackage.SINGLEECUJOB__NEGOUTPUTPARAMS:
				return getNEGOUTPUTPARAMS();
			case OdxXhtmlPackage.SINGLEECUJOB__ISREDUCEDRESULTENABLED:
				return isISREDUCEDRESULTENABLED();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SINGLEECUJOB__PROGCODES:
				setPROGCODES((PROGCODES)newValue);
				return;
			case OdxXhtmlPackage.SINGLEECUJOB__INPUTPARAMS:
				setINPUTPARAMS((INPUTPARAMS)newValue);
				return;
			case OdxXhtmlPackage.SINGLEECUJOB__OUTPUTPARAMS:
				setOUTPUTPARAMS((OUTPUTPARAMS)newValue);
				return;
			case OdxXhtmlPackage.SINGLEECUJOB__NEGOUTPUTPARAMS:
				setNEGOUTPUTPARAMS((NEGOUTPUTPARAMS)newValue);
				return;
			case OdxXhtmlPackage.SINGLEECUJOB__ISREDUCEDRESULTENABLED:
				setISREDUCEDRESULTENABLED((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SINGLEECUJOB__PROGCODES:
				setPROGCODES((PROGCODES)null);
				return;
			case OdxXhtmlPackage.SINGLEECUJOB__INPUTPARAMS:
				setINPUTPARAMS((INPUTPARAMS)null);
				return;
			case OdxXhtmlPackage.SINGLEECUJOB__OUTPUTPARAMS:
				setOUTPUTPARAMS((OUTPUTPARAMS)null);
				return;
			case OdxXhtmlPackage.SINGLEECUJOB__NEGOUTPUTPARAMS:
				setNEGOUTPUTPARAMS((NEGOUTPUTPARAMS)null);
				return;
			case OdxXhtmlPackage.SINGLEECUJOB__ISREDUCEDRESULTENABLED:
				unsetISREDUCEDRESULTENABLED();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SINGLEECUJOB__PROGCODES:
				return pROGCODES != null;
			case OdxXhtmlPackage.SINGLEECUJOB__INPUTPARAMS:
				return iNPUTPARAMS != null;
			case OdxXhtmlPackage.SINGLEECUJOB__OUTPUTPARAMS:
				return oUTPUTPARAMS != null;
			case OdxXhtmlPackage.SINGLEECUJOB__NEGOUTPUTPARAMS:
				return nEGOUTPUTPARAMS != null;
			case OdxXhtmlPackage.SINGLEECUJOB__ISREDUCEDRESULTENABLED:
				return isSetISREDUCEDRESULTENABLED();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (iSREDUCEDRESULTENABLED: ");
		if (iSREDUCEDRESULTENABLEDESet) result.append(iSREDUCEDRESULTENABLED); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //SINGLEECUJOBImpl
