/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.VEHICLECONNECTORPINREFS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VEHICLECONNECTORPINREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORPINREFSImpl#getVEHICLECONNECTORPINREF <em>VEHICLECONNECTORPINREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VEHICLECONNECTORPINREFSImpl extends MinimalEObjectImpl.Container implements VEHICLECONNECTORPINREFS {
	/**
	 * The cached value of the '{@link #getVEHICLECONNECTORPINREF() <em>VEHICLECONNECTORPINREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVEHICLECONNECTORPINREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> vEHICLECONNECTORPINREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VEHICLECONNECTORPINREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVEHICLECONNECTORPINREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getVEHICLECONNECTORPINREF() {
		if (vEHICLECONNECTORPINREF == null) {
			vEHICLECONNECTORPINREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.VEHICLECONNECTORPINREFS__VEHICLECONNECTORPINREF);
		}
		return vEHICLECONNECTORPINREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINREFS__VEHICLECONNECTORPINREF:
				return ((InternalEList<?>)getVEHICLECONNECTORPINREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINREFS__VEHICLECONNECTORPINREF:
				return getVEHICLECONNECTORPINREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINREFS__VEHICLECONNECTORPINREF:
				getVEHICLECONNECTORPINREF().clear();
				getVEHICLECONNECTORPINREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINREFS__VEHICLECONNECTORPINREF:
				getVEHICLECONNECTORPINREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPINREFS__VEHICLECONNECTORPINREF:
				return vEHICLECONNECTORPINREF != null && !vEHICLECONNECTORPINREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //VEHICLECONNECTORPINREFSImpl
