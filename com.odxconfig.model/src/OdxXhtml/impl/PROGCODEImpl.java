/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROGCODE;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PROGCODE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PROGCODEImpl#getCODEFILE <em>CODEFILE</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROGCODEImpl#getENCRYPTION <em>ENCRYPTION</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROGCODEImpl#getSYNTAX <em>SYNTAX</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROGCODEImpl#getREVISION <em>REVISION</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROGCODEImpl#getENTRYPOINT <em>ENTRYPOINT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PROGCODEImpl extends MinimalEObjectImpl.Container implements PROGCODE {
	/**
	 * The default value of the '{@link #getCODEFILE() <em>CODEFILE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCODEFILE()
	 * @generated
	 * @ordered
	 */
	protected static final String CODEFILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCODEFILE() <em>CODEFILE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCODEFILE()
	 * @generated
	 * @ordered
	 */
	protected String cODEFILE = CODEFILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getENCRYPTION() <em>ENCRYPTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENCRYPTION()
	 * @generated
	 * @ordered
	 */
	protected static final String ENCRYPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getENCRYPTION() <em>ENCRYPTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENCRYPTION()
	 * @generated
	 * @ordered
	 */
	protected String eNCRYPTION = ENCRYPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSYNTAX() <em>SYNTAX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSYNTAX()
	 * @generated
	 * @ordered
	 */
	protected static final String SYNTAX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSYNTAX() <em>SYNTAX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSYNTAX()
	 * @generated
	 * @ordered
	 */
	protected String sYNTAX = SYNTAX_EDEFAULT;

	/**
	 * The default value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected static final String REVISION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected String rEVISION = REVISION_EDEFAULT;

	/**
	 * The default value of the '{@link #getENTRYPOINT() <em>ENTRYPOINT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENTRYPOINT()
	 * @generated
	 * @ordered
	 */
	protected static final String ENTRYPOINT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getENTRYPOINT() <em>ENTRYPOINT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENTRYPOINT()
	 * @generated
	 * @ordered
	 */
	protected String eNTRYPOINT = ENTRYPOINT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PROGCODEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPROGCODE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCODEFILE() {
		return cODEFILE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCODEFILE(String newCODEFILE) {
		String oldCODEFILE = cODEFILE;
		cODEFILE = newCODEFILE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROGCODE__CODEFILE, oldCODEFILE, cODEFILE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getENCRYPTION() {
		return eNCRYPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setENCRYPTION(String newENCRYPTION) {
		String oldENCRYPTION = eNCRYPTION;
		eNCRYPTION = newENCRYPTION;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROGCODE__ENCRYPTION, oldENCRYPTION, eNCRYPTION));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSYNTAX() {
		return sYNTAX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSYNTAX(String newSYNTAX) {
		String oldSYNTAX = sYNTAX;
		sYNTAX = newSYNTAX;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROGCODE__SYNTAX, oldSYNTAX, sYNTAX));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getREVISION() {
		return rEVISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREVISION(String newREVISION) {
		String oldREVISION = rEVISION;
		rEVISION = newREVISION;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROGCODE__REVISION, oldREVISION, rEVISION));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getENTRYPOINT() {
		return eNTRYPOINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setENTRYPOINT(String newENTRYPOINT) {
		String oldENTRYPOINT = eNTRYPOINT;
		eNTRYPOINT = newENTRYPOINT;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROGCODE__ENTRYPOINT, oldENTRYPOINT, eNTRYPOINT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PROGCODE__CODEFILE:
				return getCODEFILE();
			case OdxXhtmlPackage.PROGCODE__ENCRYPTION:
				return getENCRYPTION();
			case OdxXhtmlPackage.PROGCODE__SYNTAX:
				return getSYNTAX();
			case OdxXhtmlPackage.PROGCODE__REVISION:
				return getREVISION();
			case OdxXhtmlPackage.PROGCODE__ENTRYPOINT:
				return getENTRYPOINT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PROGCODE__CODEFILE:
				setCODEFILE((String)newValue);
				return;
			case OdxXhtmlPackage.PROGCODE__ENCRYPTION:
				setENCRYPTION((String)newValue);
				return;
			case OdxXhtmlPackage.PROGCODE__SYNTAX:
				setSYNTAX((String)newValue);
				return;
			case OdxXhtmlPackage.PROGCODE__REVISION:
				setREVISION((String)newValue);
				return;
			case OdxXhtmlPackage.PROGCODE__ENTRYPOINT:
				setENTRYPOINT((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROGCODE__CODEFILE:
				setCODEFILE(CODEFILE_EDEFAULT);
				return;
			case OdxXhtmlPackage.PROGCODE__ENCRYPTION:
				setENCRYPTION(ENCRYPTION_EDEFAULT);
				return;
			case OdxXhtmlPackage.PROGCODE__SYNTAX:
				setSYNTAX(SYNTAX_EDEFAULT);
				return;
			case OdxXhtmlPackage.PROGCODE__REVISION:
				setREVISION(REVISION_EDEFAULT);
				return;
			case OdxXhtmlPackage.PROGCODE__ENTRYPOINT:
				setENTRYPOINT(ENTRYPOINT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROGCODE__CODEFILE:
				return CODEFILE_EDEFAULT == null ? cODEFILE != null : !CODEFILE_EDEFAULT.equals(cODEFILE);
			case OdxXhtmlPackage.PROGCODE__ENCRYPTION:
				return ENCRYPTION_EDEFAULT == null ? eNCRYPTION != null : !ENCRYPTION_EDEFAULT.equals(eNCRYPTION);
			case OdxXhtmlPackage.PROGCODE__SYNTAX:
				return SYNTAX_EDEFAULT == null ? sYNTAX != null : !SYNTAX_EDEFAULT.equals(sYNTAX);
			case OdxXhtmlPackage.PROGCODE__REVISION:
				return REVISION_EDEFAULT == null ? rEVISION != null : !REVISION_EDEFAULT.equals(rEVISION);
			case OdxXhtmlPackage.PROGCODE__ENTRYPOINT:
				return ENTRYPOINT_EDEFAULT == null ? eNTRYPOINT != null : !ENTRYPOINT_EDEFAULT.equals(eNTRYPOINT);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (cODEFILE: ");
		result.append(cODEFILE);
		result.append(", eNCRYPTION: ");
		result.append(eNCRYPTION);
		result.append(", sYNTAX: ");
		result.append(sYNTAX);
		result.append(", rEVISION: ");
		result.append(rEVISION);
		result.append(", eNTRYPOINT: ");
		result.append(eNTRYPOINT);
		result.append(')');
		return result.toString();
	}

} //PROGCODEImpl
