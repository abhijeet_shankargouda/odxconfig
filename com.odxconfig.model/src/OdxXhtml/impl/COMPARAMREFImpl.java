/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPARAMREF;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.DOCTYPE;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPARAMREF</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPARAMREFImpl#getVALUE <em>VALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMREFImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMREFImpl#getPROTOCOLSNREF <em>PROTOCOLSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMREFImpl#getDOCREF <em>DOCREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMREFImpl#getDOCTYPE <em>DOCTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMREFImpl#getIDREF <em>IDREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMREFImpl#getREVISION <em>REVISION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPARAMREFImpl extends MinimalEObjectImpl.Container implements COMPARAMREF {
	/**
	 * The default value of the '{@link #getVALUE() <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVALUE() <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUE()
	 * @generated
	 * @ordered
	 */
	protected String vALUE = VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getPROTOCOLSNREF() <em>PROTOCOLSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROTOCOLSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF pROTOCOLSNREF;

	/**
	 * The default value of the '{@link #getDOCREF() <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREF()
	 * @generated
	 * @ordered
	 */
	protected static final String DOCREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDOCREF() <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREF()
	 * @generated
	 * @ordered
	 */
	protected String dOCREF = DOCREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getDOCTYPE() <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final DOCTYPE DOCTYPE_EDEFAULT = DOCTYPE.FLASH;

	/**
	 * The cached value of the '{@link #getDOCTYPE() <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCTYPE()
	 * @generated
	 * @ordered
	 */
	protected DOCTYPE dOCTYPE = DOCTYPE_EDEFAULT;

	/**
	 * This is true if the DOCTYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dOCTYPEESet;

	/**
	 * The default value of the '{@link #getIDREF() <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDREF()
	 * @generated
	 * @ordered
	 */
	protected static final String IDREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIDREF() <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDREF()
	 * @generated
	 * @ordered
	 */
	protected String iDREF = IDREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected static final String REVISION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected String rEVISION = REVISION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPARAMREFImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPARAMREF();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVALUE() {
		return vALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVALUE(String newVALUE) {
		String oldVALUE = vALUE;
		vALUE = newVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMREF__VALUE, oldVALUE, vALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMREF__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMREF__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMREF__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMREF__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getPROTOCOLSNREF() {
		return pROTOCOLSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROTOCOLSNREF(SNREF newPROTOCOLSNREF, NotificationChain msgs) {
		SNREF oldPROTOCOLSNREF = pROTOCOLSNREF;
		pROTOCOLSNREF = newPROTOCOLSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMREF__PROTOCOLSNREF, oldPROTOCOLSNREF, newPROTOCOLSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROTOCOLSNREF(SNREF newPROTOCOLSNREF) {
		if (newPROTOCOLSNREF != pROTOCOLSNREF) {
			NotificationChain msgs = null;
			if (pROTOCOLSNREF != null)
				msgs = ((InternalEObject)pROTOCOLSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMREF__PROTOCOLSNREF, null, msgs);
			if (newPROTOCOLSNREF != null)
				msgs = ((InternalEObject)newPROTOCOLSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMREF__PROTOCOLSNREF, null, msgs);
			msgs = basicSetPROTOCOLSNREF(newPROTOCOLSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMREF__PROTOCOLSNREF, newPROTOCOLSNREF, newPROTOCOLSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDOCREF() {
		return dOCREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCREF(String newDOCREF) {
		String oldDOCREF = dOCREF;
		dOCREF = newDOCREF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMREF__DOCREF, oldDOCREF, dOCREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOCTYPE getDOCTYPE() {
		return dOCTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCTYPE(DOCTYPE newDOCTYPE) {
		DOCTYPE oldDOCTYPE = dOCTYPE;
		dOCTYPE = newDOCTYPE == null ? DOCTYPE_EDEFAULT : newDOCTYPE;
		boolean oldDOCTYPEESet = dOCTYPEESet;
		dOCTYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMREF__DOCTYPE, oldDOCTYPE, dOCTYPE, !oldDOCTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDOCTYPE() {
		DOCTYPE oldDOCTYPE = dOCTYPE;
		boolean oldDOCTYPEESet = dOCTYPEESet;
		dOCTYPE = DOCTYPE_EDEFAULT;
		dOCTYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.COMPARAMREF__DOCTYPE, oldDOCTYPE, DOCTYPE_EDEFAULT, oldDOCTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDOCTYPE() {
		return dOCTYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIDREF() {
		return iDREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIDREF(String newIDREF) {
		String oldIDREF = iDREF;
		iDREF = newIDREF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMREF__IDREF, oldIDREF, iDREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getREVISION() {
		return rEVISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREVISION(String newREVISION) {
		String oldREVISION = rEVISION;
		rEVISION = newREVISION;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMREF__REVISION, oldREVISION, rEVISION));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREF__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.COMPARAMREF__PROTOCOLSNREF:
				return basicSetPROTOCOLSNREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREF__VALUE:
				return getVALUE();
			case OdxXhtmlPackage.COMPARAMREF__DESC:
				return getDESC();
			case OdxXhtmlPackage.COMPARAMREF__PROTOCOLSNREF:
				return getPROTOCOLSNREF();
			case OdxXhtmlPackage.COMPARAMREF__DOCREF:
				return getDOCREF();
			case OdxXhtmlPackage.COMPARAMREF__DOCTYPE:
				return getDOCTYPE();
			case OdxXhtmlPackage.COMPARAMREF__IDREF:
				return getIDREF();
			case OdxXhtmlPackage.COMPARAMREF__REVISION:
				return getREVISION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREF__VALUE:
				setVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.COMPARAMREF__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.COMPARAMREF__PROTOCOLSNREF:
				setPROTOCOLSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.COMPARAMREF__DOCREF:
				setDOCREF((String)newValue);
				return;
			case OdxXhtmlPackage.COMPARAMREF__DOCTYPE:
				setDOCTYPE((DOCTYPE)newValue);
				return;
			case OdxXhtmlPackage.COMPARAMREF__IDREF:
				setIDREF((String)newValue);
				return;
			case OdxXhtmlPackage.COMPARAMREF__REVISION:
				setREVISION((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREF__VALUE:
				setVALUE(VALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPARAMREF__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.COMPARAMREF__PROTOCOLSNREF:
				setPROTOCOLSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.COMPARAMREF__DOCREF:
				setDOCREF(DOCREF_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPARAMREF__DOCTYPE:
				unsetDOCTYPE();
				return;
			case OdxXhtmlPackage.COMPARAMREF__IDREF:
				setIDREF(IDREF_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPARAMREF__REVISION:
				setREVISION(REVISION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMREF__VALUE:
				return VALUE_EDEFAULT == null ? vALUE != null : !VALUE_EDEFAULT.equals(vALUE);
			case OdxXhtmlPackage.COMPARAMREF__DESC:
				return dESC != null;
			case OdxXhtmlPackage.COMPARAMREF__PROTOCOLSNREF:
				return pROTOCOLSNREF != null;
			case OdxXhtmlPackage.COMPARAMREF__DOCREF:
				return DOCREF_EDEFAULT == null ? dOCREF != null : !DOCREF_EDEFAULT.equals(dOCREF);
			case OdxXhtmlPackage.COMPARAMREF__DOCTYPE:
				return isSetDOCTYPE();
			case OdxXhtmlPackage.COMPARAMREF__IDREF:
				return IDREF_EDEFAULT == null ? iDREF != null : !IDREF_EDEFAULT.equals(iDREF);
			case OdxXhtmlPackage.COMPARAMREF__REVISION:
				return REVISION_EDEFAULT == null ? rEVISION != null : !REVISION_EDEFAULT.equals(rEVISION);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (vALUE: ");
		result.append(vALUE);
		result.append(", dOCREF: ");
		result.append(dOCREF);
		result.append(", dOCTYPE: ");
		if (dOCTYPEESet) result.append(dOCTYPE); else result.append("<unset>");
		result.append(", iDREF: ");
		result.append(iDREF);
		result.append(", rEVISION: ");
		result.append(rEVISION);
		result.append(')');
		return result.toString();
	}

} //COMPARAMREFImpl
