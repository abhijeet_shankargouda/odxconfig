/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATAFILE;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DATAFILE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DATAFILEImpl#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATAFILEImpl#isLATEBOUNDDATAFILE <em>LATEBOUNDDATAFILE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DATAFILEImpl extends MinimalEObjectImpl.Container implements DATAFILE {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #isLATEBOUNDDATAFILE() <em>LATEBOUNDDATAFILE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLATEBOUNDDATAFILE()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LATEBOUNDDATAFILE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isLATEBOUNDDATAFILE() <em>LATEBOUNDDATAFILE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLATEBOUNDDATAFILE()
	 * @generated
	 * @ordered
	 */
	protected boolean lATEBOUNDDATAFILE = LATEBOUNDDATAFILE_EDEFAULT;

	/**
	 * This is true if the LATEBOUNDDATAFILE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean lATEBOUNDDATAFILEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DATAFILEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDATAFILE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAFILE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isLATEBOUNDDATAFILE() {
		return lATEBOUNDDATAFILE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLATEBOUNDDATAFILE(boolean newLATEBOUNDDATAFILE) {
		boolean oldLATEBOUNDDATAFILE = lATEBOUNDDATAFILE;
		lATEBOUNDDATAFILE = newLATEBOUNDDATAFILE;
		boolean oldLATEBOUNDDATAFILEESet = lATEBOUNDDATAFILEESet;
		lATEBOUNDDATAFILEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATAFILE__LATEBOUNDDATAFILE, oldLATEBOUNDDATAFILE, lATEBOUNDDATAFILE, !oldLATEBOUNDDATAFILEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetLATEBOUNDDATAFILE() {
		boolean oldLATEBOUNDDATAFILE = lATEBOUNDDATAFILE;
		boolean oldLATEBOUNDDATAFILEESet = lATEBOUNDDATAFILEESet;
		lATEBOUNDDATAFILE = LATEBOUNDDATAFILE_EDEFAULT;
		lATEBOUNDDATAFILEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DATAFILE__LATEBOUNDDATAFILE, oldLATEBOUNDDATAFILE, LATEBOUNDDATAFILE_EDEFAULT, oldLATEBOUNDDATAFILEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetLATEBOUNDDATAFILE() {
		return lATEBOUNDDATAFILEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAFILE__VALUE:
				return getValue();
			case OdxXhtmlPackage.DATAFILE__LATEBOUNDDATAFILE:
				return isLATEBOUNDDATAFILE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAFILE__VALUE:
				setValue((String)newValue);
				return;
			case OdxXhtmlPackage.DATAFILE__LATEBOUNDDATAFILE:
				setLATEBOUNDDATAFILE((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAFILE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.DATAFILE__LATEBOUNDDATAFILE:
				unsetLATEBOUNDDATAFILE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAFILE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case OdxXhtmlPackage.DATAFILE__LATEBOUNDDATAFILE:
				return isSetLATEBOUNDDATAFILE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", lATEBOUNDDATAFILE: ");
		if (lATEBOUNDDATAFILEESet) result.append(lATEBOUNDDATAFILE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //DATAFILEImpl
