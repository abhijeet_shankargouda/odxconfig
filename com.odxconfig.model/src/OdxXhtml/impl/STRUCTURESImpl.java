/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.STRUCTURE;
import OdxXhtml.STRUCTURES;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>STRUCTURES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.STRUCTURESImpl#getSTRUCTURE <em>STRUCTURE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class STRUCTURESImpl extends MinimalEObjectImpl.Container implements STRUCTURES {
	/**
	 * The cached value of the '{@link #getSTRUCTURE() <em>STRUCTURE</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTRUCTURE()
	 * @generated
	 * @ordered
	 */
	protected EList<STRUCTURE> sTRUCTURE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected STRUCTURESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSTRUCTURES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<STRUCTURE> getSTRUCTURE() {
		if (sTRUCTURE == null) {
			sTRUCTURE = new EObjectContainmentEList<STRUCTURE>(STRUCTURE.class, this, OdxXhtmlPackage.STRUCTURES__STRUCTURE);
		}
		return sTRUCTURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.STRUCTURES__STRUCTURE:
				return ((InternalEList<?>)getSTRUCTURE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.STRUCTURES__STRUCTURE:
				return getSTRUCTURE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.STRUCTURES__STRUCTURE:
				getSTRUCTURE().clear();
				getSTRUCTURE().addAll((Collection<? extends STRUCTURE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STRUCTURES__STRUCTURE:
				getSTRUCTURE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.STRUCTURES__STRUCTURE:
				return sTRUCTURE != null && !sTRUCTURE.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //STRUCTURESImpl
