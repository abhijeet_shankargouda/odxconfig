/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSICALDIMENSION;
import OdxXhtml.PHYSICALDIMENSIONS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PHYSICALDIMENSIONS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PHYSICALDIMENSIONSImpl#getPHYSICALDIMENSION <em>PHYSICALDIMENSION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PHYSICALDIMENSIONSImpl extends MinimalEObjectImpl.Container implements PHYSICALDIMENSIONS {
	/**
	 * The cached value of the '{@link #getPHYSICALDIMENSION() <em>PHYSICALDIMENSION</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALDIMENSION()
	 * @generated
	 * @ordered
	 */
	protected EList<PHYSICALDIMENSION> pHYSICALDIMENSION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PHYSICALDIMENSIONSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPHYSICALDIMENSIONS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<PHYSICALDIMENSION> getPHYSICALDIMENSION() {
		if (pHYSICALDIMENSION == null) {
			pHYSICALDIMENSION = new EObjectContainmentEList<PHYSICALDIMENSION>(PHYSICALDIMENSION.class, this, OdxXhtmlPackage.PHYSICALDIMENSIONS__PHYSICALDIMENSION);
		}
		return pHYSICALDIMENSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSIONS__PHYSICALDIMENSION:
				return ((InternalEList<?>)getPHYSICALDIMENSION()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSIONS__PHYSICALDIMENSION:
				return getPHYSICALDIMENSION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSIONS__PHYSICALDIMENSION:
				getPHYSICALDIMENSION().clear();
				getPHYSICALDIMENSION().addAll((Collection<? extends PHYSICALDIMENSION>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSIONS__PHYSICALDIMENSION:
				getPHYSICALDIMENSION().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALDIMENSIONS__PHYSICALDIMENSION:
				return pHYSICALDIMENSION != null && !pHYSICALDIMENSION.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PHYSICALDIMENSIONSImpl
