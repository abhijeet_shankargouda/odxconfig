/**
 */
package OdxXhtml.impl;

import OdxXhtml.DOCTYPE;
import OdxXhtml.DYNENDDOPREF;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DYNENDDOPREF</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DYNENDDOPREFImpl#getTERMINATIONVALUE <em>TERMINATIONVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNENDDOPREFImpl#getDOCREF <em>DOCREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNENDDOPREFImpl#getDOCTYPE <em>DOCTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNENDDOPREFImpl#getIDREF <em>IDREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DYNENDDOPREFImpl#getREVISION <em>REVISION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DYNENDDOPREFImpl extends MinimalEObjectImpl.Container implements DYNENDDOPREF {
	/**
	 * The default value of the '{@link #getTERMINATIONVALUE() <em>TERMINATIONVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTERMINATIONVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String TERMINATIONVALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTERMINATIONVALUE() <em>TERMINATIONVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTERMINATIONVALUE()
	 * @generated
	 * @ordered
	 */
	protected String tERMINATIONVALUE = TERMINATIONVALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDOCREF() <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREF()
	 * @generated
	 * @ordered
	 */
	protected static final String DOCREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDOCREF() <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREF()
	 * @generated
	 * @ordered
	 */
	protected String dOCREF = DOCREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getDOCTYPE() <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final DOCTYPE DOCTYPE_EDEFAULT = DOCTYPE.FLASH;

	/**
	 * The cached value of the '{@link #getDOCTYPE() <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCTYPE()
	 * @generated
	 * @ordered
	 */
	protected DOCTYPE dOCTYPE = DOCTYPE_EDEFAULT;

	/**
	 * This is true if the DOCTYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dOCTYPEESet;

	/**
	 * The default value of the '{@link #getIDREF() <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDREF()
	 * @generated
	 * @ordered
	 */
	protected static final String IDREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIDREF() <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDREF()
	 * @generated
	 * @ordered
	 */
	protected String iDREF = IDREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected static final String REVISION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected String rEVISION = REVISION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DYNENDDOPREFImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDYNENDDOPREF();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTERMINATIONVALUE() {
		return tERMINATIONVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTERMINATIONVALUE(String newTERMINATIONVALUE) {
		String oldTERMINATIONVALUE = tERMINATIONVALUE;
		tERMINATIONVALUE = newTERMINATIONVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNENDDOPREF__TERMINATIONVALUE, oldTERMINATIONVALUE, tERMINATIONVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDOCREF() {
		return dOCREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCREF(String newDOCREF) {
		String oldDOCREF = dOCREF;
		dOCREF = newDOCREF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNENDDOPREF__DOCREF, oldDOCREF, dOCREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOCTYPE getDOCTYPE() {
		return dOCTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCTYPE(DOCTYPE newDOCTYPE) {
		DOCTYPE oldDOCTYPE = dOCTYPE;
		dOCTYPE = newDOCTYPE == null ? DOCTYPE_EDEFAULT : newDOCTYPE;
		boolean oldDOCTYPEESet = dOCTYPEESet;
		dOCTYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNENDDOPREF__DOCTYPE, oldDOCTYPE, dOCTYPE, !oldDOCTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDOCTYPE() {
		DOCTYPE oldDOCTYPE = dOCTYPE;
		boolean oldDOCTYPEESet = dOCTYPEESet;
		dOCTYPE = DOCTYPE_EDEFAULT;
		dOCTYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DYNENDDOPREF__DOCTYPE, oldDOCTYPE, DOCTYPE_EDEFAULT, oldDOCTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDOCTYPE() {
		return dOCTYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIDREF() {
		return iDREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIDREF(String newIDREF) {
		String oldIDREF = iDREF;
		iDREF = newIDREF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNENDDOPREF__IDREF, oldIDREF, iDREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getREVISION() {
		return rEVISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREVISION(String newREVISION) {
		String oldREVISION = rEVISION;
		rEVISION = newREVISION;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNENDDOPREF__REVISION, oldREVISION, rEVISION));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNENDDOPREF__TERMINATIONVALUE:
				return getTERMINATIONVALUE();
			case OdxXhtmlPackage.DYNENDDOPREF__DOCREF:
				return getDOCREF();
			case OdxXhtmlPackage.DYNENDDOPREF__DOCTYPE:
				return getDOCTYPE();
			case OdxXhtmlPackage.DYNENDDOPREF__IDREF:
				return getIDREF();
			case OdxXhtmlPackage.DYNENDDOPREF__REVISION:
				return getREVISION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNENDDOPREF__TERMINATIONVALUE:
				setTERMINATIONVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.DYNENDDOPREF__DOCREF:
				setDOCREF((String)newValue);
				return;
			case OdxXhtmlPackage.DYNENDDOPREF__DOCTYPE:
				setDOCTYPE((DOCTYPE)newValue);
				return;
			case OdxXhtmlPackage.DYNENDDOPREF__IDREF:
				setIDREF((String)newValue);
				return;
			case OdxXhtmlPackage.DYNENDDOPREF__REVISION:
				setREVISION((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNENDDOPREF__TERMINATIONVALUE:
				setTERMINATIONVALUE(TERMINATIONVALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.DYNENDDOPREF__DOCREF:
				setDOCREF(DOCREF_EDEFAULT);
				return;
			case OdxXhtmlPackage.DYNENDDOPREF__DOCTYPE:
				unsetDOCTYPE();
				return;
			case OdxXhtmlPackage.DYNENDDOPREF__IDREF:
				setIDREF(IDREF_EDEFAULT);
				return;
			case OdxXhtmlPackage.DYNENDDOPREF__REVISION:
				setREVISION(REVISION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNENDDOPREF__TERMINATIONVALUE:
				return TERMINATIONVALUE_EDEFAULT == null ? tERMINATIONVALUE != null : !TERMINATIONVALUE_EDEFAULT.equals(tERMINATIONVALUE);
			case OdxXhtmlPackage.DYNENDDOPREF__DOCREF:
				return DOCREF_EDEFAULT == null ? dOCREF != null : !DOCREF_EDEFAULT.equals(dOCREF);
			case OdxXhtmlPackage.DYNENDDOPREF__DOCTYPE:
				return isSetDOCTYPE();
			case OdxXhtmlPackage.DYNENDDOPREF__IDREF:
				return IDREF_EDEFAULT == null ? iDREF != null : !IDREF_EDEFAULT.equals(iDREF);
			case OdxXhtmlPackage.DYNENDDOPREF__REVISION:
				return REVISION_EDEFAULT == null ? rEVISION != null : !REVISION_EDEFAULT.equals(rEVISION);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (tERMINATIONVALUE: ");
		result.append(tERMINATIONVALUE);
		result.append(", dOCREF: ");
		result.append(dOCREF);
		result.append(", dOCTYPE: ");
		if (dOCTYPEESet) result.append(dOCTYPE); else result.append("<unset>");
		result.append(", iDREF: ");
		result.append(iDREF);
		result.append(", rEVISION: ");
		result.append(rEVISION);
		result.append(')');
		return result.toString();
	}

} //DYNENDDOPREFImpl
