/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYDOCINFO;
import OdxXhtml.COMPANYDOCINFOS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYDOCINFOS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYDOCINFOSImpl#getCOMPANYDOCINFO <em>COMPANYDOCINFO</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYDOCINFOSImpl extends MinimalEObjectImpl.Container implements COMPANYDOCINFOS {
	/**
	 * The cached value of the '{@link #getCOMPANYDOCINFO() <em>COMPANYDOCINFO</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDOCINFO()
	 * @generated
	 * @ordered
	 */
	protected EList<COMPANYDOCINFO> cOMPANYDOCINFO;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYDOCINFOSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYDOCINFOS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<COMPANYDOCINFO> getCOMPANYDOCINFO() {
		if (cOMPANYDOCINFO == null) {
			cOMPANYDOCINFO = new EObjectContainmentEList<COMPANYDOCINFO>(COMPANYDOCINFO.class, this, OdxXhtmlPackage.COMPANYDOCINFOS__COMPANYDOCINFO);
		}
		return cOMPANYDOCINFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS__COMPANYDOCINFO:
				return ((InternalEList<?>)getCOMPANYDOCINFO()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS__COMPANYDOCINFO:
				return getCOMPANYDOCINFO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS__COMPANYDOCINFO:
				getCOMPANYDOCINFO().clear();
				getCOMPANYDOCINFO().addAll((Collection<? extends COMPANYDOCINFO>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS__COMPANYDOCINFO:
				getCOMPANYDOCINFO().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFOS__COMPANYDOCINFO:
				return cOMPANYDOCINFO != null && !cOMPANYDOCINFO.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //COMPANYDOCINFOSImpl
