/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADDRESSING;
import OdxXhtml.DIAGSERVICE;
import OdxXhtml.NEGRESPONSEREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.POSRESPONSEREFS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGSERVICE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGSERVICEImpl#getREQUESTREF <em>REQUESTREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGSERVICEImpl#getPOSRESPONSEREFS <em>POSRESPONSEREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGSERVICEImpl#getNEGRESPONSEREFS <em>NEGRESPONSEREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGSERVICEImpl#getADDRESSING <em>ADDRESSING</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGSERVICEImpl#isISCYCLIC <em>ISCYCLIC</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGSERVICEImpl#isISMULTIPLE <em>ISMULTIPLE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGSERVICEImpl extends DIAGCOMMImpl implements DIAGSERVICE {
	/**
	 * The cached value of the '{@link #getREQUESTREF() <em>REQUESTREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREQUESTREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK rEQUESTREF;

	/**
	 * The cached value of the '{@link #getPOSRESPONSEREFS() <em>POSRESPONSEREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPOSRESPONSEREFS()
	 * @generated
	 * @ordered
	 */
	protected POSRESPONSEREFS pOSRESPONSEREFS;

	/**
	 * The cached value of the '{@link #getNEGRESPONSEREFS() <em>NEGRESPONSEREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNEGRESPONSEREFS()
	 * @generated
	 * @ordered
	 */
	protected NEGRESPONSEREFS nEGRESPONSEREFS;

	/**
	 * The default value of the '{@link #getADDRESSING() <em>ADDRESSING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADDRESSING()
	 * @generated
	 * @ordered
	 */
	protected static final ADDRESSING ADDRESSING_EDEFAULT = ADDRESSING.PHYSICAL;

	/**
	 * The cached value of the '{@link #getADDRESSING() <em>ADDRESSING</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADDRESSING()
	 * @generated
	 * @ordered
	 */
	protected ADDRESSING aDDRESSING = ADDRESSING_EDEFAULT;

	/**
	 * This is true if the ADDRESSING attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aDDRESSINGESet;

	/**
	 * The default value of the '{@link #isISCYCLIC() <em>ISCYCLIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISCYCLIC()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISCYCLIC_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISCYCLIC() <em>ISCYCLIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISCYCLIC()
	 * @generated
	 * @ordered
	 */
	protected boolean iSCYCLIC = ISCYCLIC_EDEFAULT;

	/**
	 * This is true if the ISCYCLIC attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSCYCLICESet;

	/**
	 * The default value of the '{@link #isISMULTIPLE() <em>ISMULTIPLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISMULTIPLE()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISMULTIPLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISMULTIPLE() <em>ISMULTIPLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISMULTIPLE()
	 * @generated
	 * @ordered
	 */
	protected boolean iSMULTIPLE = ISMULTIPLE_EDEFAULT;

	/**
	 * This is true if the ISMULTIPLE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSMULTIPLEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGSERVICEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGSERVICE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getREQUESTREF() {
		return rEQUESTREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetREQUESTREF(ODXLINK newREQUESTREF, NotificationChain msgs) {
		ODXLINK oldREQUESTREF = rEQUESTREF;
		rEQUESTREF = newREQUESTREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGSERVICE__REQUESTREF, oldREQUESTREF, newREQUESTREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREQUESTREF(ODXLINK newREQUESTREF) {
		if (newREQUESTREF != rEQUESTREF) {
			NotificationChain msgs = null;
			if (rEQUESTREF != null)
				msgs = ((InternalEObject)rEQUESTREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGSERVICE__REQUESTREF, null, msgs);
			if (newREQUESTREF != null)
				msgs = ((InternalEObject)newREQUESTREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGSERVICE__REQUESTREF, null, msgs);
			msgs = basicSetREQUESTREF(newREQUESTREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGSERVICE__REQUESTREF, newREQUESTREF, newREQUESTREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public POSRESPONSEREFS getPOSRESPONSEREFS() {
		return pOSRESPONSEREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPOSRESPONSEREFS(POSRESPONSEREFS newPOSRESPONSEREFS, NotificationChain msgs) {
		POSRESPONSEREFS oldPOSRESPONSEREFS = pOSRESPONSEREFS;
		pOSRESPONSEREFS = newPOSRESPONSEREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGSERVICE__POSRESPONSEREFS, oldPOSRESPONSEREFS, newPOSRESPONSEREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPOSRESPONSEREFS(POSRESPONSEREFS newPOSRESPONSEREFS) {
		if (newPOSRESPONSEREFS != pOSRESPONSEREFS) {
			NotificationChain msgs = null;
			if (pOSRESPONSEREFS != null)
				msgs = ((InternalEObject)pOSRESPONSEREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGSERVICE__POSRESPONSEREFS, null, msgs);
			if (newPOSRESPONSEREFS != null)
				msgs = ((InternalEObject)newPOSRESPONSEREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGSERVICE__POSRESPONSEREFS, null, msgs);
			msgs = basicSetPOSRESPONSEREFS(newPOSRESPONSEREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGSERVICE__POSRESPONSEREFS, newPOSRESPONSEREFS, newPOSRESPONSEREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NEGRESPONSEREFS getNEGRESPONSEREFS() {
		return nEGRESPONSEREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNEGRESPONSEREFS(NEGRESPONSEREFS newNEGRESPONSEREFS, NotificationChain msgs) {
		NEGRESPONSEREFS oldNEGRESPONSEREFS = nEGRESPONSEREFS;
		nEGRESPONSEREFS = newNEGRESPONSEREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGSERVICE__NEGRESPONSEREFS, oldNEGRESPONSEREFS, newNEGRESPONSEREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNEGRESPONSEREFS(NEGRESPONSEREFS newNEGRESPONSEREFS) {
		if (newNEGRESPONSEREFS != nEGRESPONSEREFS) {
			NotificationChain msgs = null;
			if (nEGRESPONSEREFS != null)
				msgs = ((InternalEObject)nEGRESPONSEREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGSERVICE__NEGRESPONSEREFS, null, msgs);
			if (newNEGRESPONSEREFS != null)
				msgs = ((InternalEObject)newNEGRESPONSEREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGSERVICE__NEGRESPONSEREFS, null, msgs);
			msgs = basicSetNEGRESPONSEREFS(newNEGRESPONSEREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGSERVICE__NEGRESPONSEREFS, newNEGRESPONSEREFS, newNEGRESPONSEREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADDRESSING getADDRESSING() {
		return aDDRESSING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADDRESSING(ADDRESSING newADDRESSING) {
		ADDRESSING oldADDRESSING = aDDRESSING;
		aDDRESSING = newADDRESSING == null ? ADDRESSING_EDEFAULT : newADDRESSING;
		boolean oldADDRESSINGESet = aDDRESSINGESet;
		aDDRESSINGESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGSERVICE__ADDRESSING, oldADDRESSING, aDDRESSING, !oldADDRESSINGESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetADDRESSING() {
		ADDRESSING oldADDRESSING = aDDRESSING;
		boolean oldADDRESSINGESet = aDDRESSINGESet;
		aDDRESSING = ADDRESSING_EDEFAULT;
		aDDRESSINGESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGSERVICE__ADDRESSING, oldADDRESSING, ADDRESSING_EDEFAULT, oldADDRESSINGESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetADDRESSING() {
		return aDDRESSINGESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISCYCLIC() {
		return iSCYCLIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISCYCLIC(boolean newISCYCLIC) {
		boolean oldISCYCLIC = iSCYCLIC;
		iSCYCLIC = newISCYCLIC;
		boolean oldISCYCLICESet = iSCYCLICESet;
		iSCYCLICESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGSERVICE__ISCYCLIC, oldISCYCLIC, iSCYCLIC, !oldISCYCLICESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISCYCLIC() {
		boolean oldISCYCLIC = iSCYCLIC;
		boolean oldISCYCLICESet = iSCYCLICESet;
		iSCYCLIC = ISCYCLIC_EDEFAULT;
		iSCYCLICESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGSERVICE__ISCYCLIC, oldISCYCLIC, ISCYCLIC_EDEFAULT, oldISCYCLICESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISCYCLIC() {
		return iSCYCLICESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISMULTIPLE() {
		return iSMULTIPLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISMULTIPLE(boolean newISMULTIPLE) {
		boolean oldISMULTIPLE = iSMULTIPLE;
		iSMULTIPLE = newISMULTIPLE;
		boolean oldISMULTIPLEESet = iSMULTIPLEESet;
		iSMULTIPLEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGSERVICE__ISMULTIPLE, oldISMULTIPLE, iSMULTIPLE, !oldISMULTIPLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISMULTIPLE() {
		boolean oldISMULTIPLE = iSMULTIPLE;
		boolean oldISMULTIPLEESet = iSMULTIPLEESet;
		iSMULTIPLE = ISMULTIPLE_EDEFAULT;
		iSMULTIPLEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGSERVICE__ISMULTIPLE, oldISMULTIPLE, ISMULTIPLE_EDEFAULT, oldISMULTIPLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISMULTIPLE() {
		return iSMULTIPLEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGSERVICE__REQUESTREF:
				return basicSetREQUESTREF(null, msgs);
			case OdxXhtmlPackage.DIAGSERVICE__POSRESPONSEREFS:
				return basicSetPOSRESPONSEREFS(null, msgs);
			case OdxXhtmlPackage.DIAGSERVICE__NEGRESPONSEREFS:
				return basicSetNEGRESPONSEREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGSERVICE__REQUESTREF:
				return getREQUESTREF();
			case OdxXhtmlPackage.DIAGSERVICE__POSRESPONSEREFS:
				return getPOSRESPONSEREFS();
			case OdxXhtmlPackage.DIAGSERVICE__NEGRESPONSEREFS:
				return getNEGRESPONSEREFS();
			case OdxXhtmlPackage.DIAGSERVICE__ADDRESSING:
				return getADDRESSING();
			case OdxXhtmlPackage.DIAGSERVICE__ISCYCLIC:
				return isISCYCLIC();
			case OdxXhtmlPackage.DIAGSERVICE__ISMULTIPLE:
				return isISMULTIPLE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGSERVICE__REQUESTREF:
				setREQUESTREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.DIAGSERVICE__POSRESPONSEREFS:
				setPOSRESPONSEREFS((POSRESPONSEREFS)newValue);
				return;
			case OdxXhtmlPackage.DIAGSERVICE__NEGRESPONSEREFS:
				setNEGRESPONSEREFS((NEGRESPONSEREFS)newValue);
				return;
			case OdxXhtmlPackage.DIAGSERVICE__ADDRESSING:
				setADDRESSING((ADDRESSING)newValue);
				return;
			case OdxXhtmlPackage.DIAGSERVICE__ISCYCLIC:
				setISCYCLIC((Boolean)newValue);
				return;
			case OdxXhtmlPackage.DIAGSERVICE__ISMULTIPLE:
				setISMULTIPLE((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGSERVICE__REQUESTREF:
				setREQUESTREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.DIAGSERVICE__POSRESPONSEREFS:
				setPOSRESPONSEREFS((POSRESPONSEREFS)null);
				return;
			case OdxXhtmlPackage.DIAGSERVICE__NEGRESPONSEREFS:
				setNEGRESPONSEREFS((NEGRESPONSEREFS)null);
				return;
			case OdxXhtmlPackage.DIAGSERVICE__ADDRESSING:
				unsetADDRESSING();
				return;
			case OdxXhtmlPackage.DIAGSERVICE__ISCYCLIC:
				unsetISCYCLIC();
				return;
			case OdxXhtmlPackage.DIAGSERVICE__ISMULTIPLE:
				unsetISMULTIPLE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGSERVICE__REQUESTREF:
				return rEQUESTREF != null;
			case OdxXhtmlPackage.DIAGSERVICE__POSRESPONSEREFS:
				return pOSRESPONSEREFS != null;
			case OdxXhtmlPackage.DIAGSERVICE__NEGRESPONSEREFS:
				return nEGRESPONSEREFS != null;
			case OdxXhtmlPackage.DIAGSERVICE__ADDRESSING:
				return isSetADDRESSING();
			case OdxXhtmlPackage.DIAGSERVICE__ISCYCLIC:
				return isSetISCYCLIC();
			case OdxXhtmlPackage.DIAGSERVICE__ISMULTIPLE:
				return isSetISMULTIPLE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (aDDRESSING: ");
		if (aDDRESSINGESet) result.append(aDDRESSING); else result.append("<unset>");
		result.append(", iSCYCLIC: ");
		if (iSCYCLICESet) result.append(iSCYCLIC); else result.append("<unset>");
		result.append(", iSMULTIPLE: ");
		if (iSMULTIPLEESet) result.append(iSMULTIPLE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //DIAGSERVICEImpl
