/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSICALVEHICLELINK;
import OdxXhtml.PHYSICALVEHICLELINKS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PHYSICALVEHICLELINKS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PHYSICALVEHICLELINKSImpl#getPHYSICALVEHICLELINK <em>PHYSICALVEHICLELINK</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PHYSICALVEHICLELINKSImpl extends MinimalEObjectImpl.Container implements PHYSICALVEHICLELINKS {
	/**
	 * The cached value of the '{@link #getPHYSICALVEHICLELINK() <em>PHYSICALVEHICLELINK</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALVEHICLELINK()
	 * @generated
	 * @ordered
	 */
	protected EList<PHYSICALVEHICLELINK> pHYSICALVEHICLELINK;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PHYSICALVEHICLELINKSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPHYSICALVEHICLELINKS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<PHYSICALVEHICLELINK> getPHYSICALVEHICLELINK() {
		if (pHYSICALVEHICLELINK == null) {
			pHYSICALVEHICLELINK = new EObjectContainmentEList<PHYSICALVEHICLELINK>(PHYSICALVEHICLELINK.class, this, OdxXhtmlPackage.PHYSICALVEHICLELINKS__PHYSICALVEHICLELINK);
		}
		return pHYSICALVEHICLELINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINKS__PHYSICALVEHICLELINK:
				return ((InternalEList<?>)getPHYSICALVEHICLELINK()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINKS__PHYSICALVEHICLELINK:
				return getPHYSICALVEHICLELINK();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINKS__PHYSICALVEHICLELINK:
				getPHYSICALVEHICLELINK().clear();
				getPHYSICALVEHICLELINK().addAll((Collection<? extends PHYSICALVEHICLELINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINKS__PHYSICALVEHICLELINK:
				getPHYSICALVEHICLELINK().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALVEHICLELINKS__PHYSICALVEHICLELINK:
				return pHYSICALVEHICLELINK != null && !pHYSICALVEHICLELINK.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PHYSICALVEHICLELINKSImpl
