/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.RELATEDDOC1;
import OdxXhtml.RELATEDDOCS1;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RELATEDDOCS1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.RELATEDDOCS1Impl#getRELATEDDOC <em>RELATEDDOC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RELATEDDOCS1Impl extends MinimalEObjectImpl.Container implements RELATEDDOCS1 {
	/**
	 * The cached value of the '{@link #getRELATEDDOC() <em>RELATEDDOC</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRELATEDDOC()
	 * @generated
	 * @ordered
	 */
	protected EList<RELATEDDOC1> rELATEDDOC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RELATEDDOCS1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getRELATEDDOCS1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RELATEDDOC1> getRELATEDDOC() {
		if (rELATEDDOC == null) {
			rELATEDDOC = new EObjectContainmentEList<RELATEDDOC1>(RELATEDDOC1.class, this, OdxXhtmlPackage.RELATEDDOCS1__RELATEDDOC);
		}
		return rELATEDDOC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS1__RELATEDDOC:
				return ((InternalEList<?>)getRELATEDDOC()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS1__RELATEDDOC:
				return getRELATEDDOC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS1__RELATEDDOC:
				getRELATEDDOC().clear();
				getRELATEDDOC().addAll((Collection<? extends RELATEDDOC1>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS1__RELATEDDOC:
				getRELATEDDOC().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RELATEDDOCS1__RELATEDDOC:
				return rELATEDDOC != null && !rELATEDDOC.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RELATEDDOCS1Impl
