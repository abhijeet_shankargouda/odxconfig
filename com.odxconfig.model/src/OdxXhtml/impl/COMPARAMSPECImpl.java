/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPARAMS;
import OdxXhtml.COMPARAMSPEC;
import OdxXhtml.DATAOBJECTPROPS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.UNITSPEC;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPARAMSPEC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPARAMSPECImpl#getCOMPARAMS <em>COMPARAMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMSPECImpl#getDATAOBJECTPROPS <em>DATAOBJECTPROPS</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMSPECImpl#getUNITSPEC <em>UNITSPEC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPARAMSPECImpl extends ODXCATEGORYImpl implements COMPARAMSPEC {
	/**
	 * The cached value of the '{@link #getCOMPARAMS() <em>COMPARAMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPARAMS()
	 * @generated
	 * @ordered
	 */
	protected COMPARAMS cOMPARAMS;

	/**
	 * The cached value of the '{@link #getDATAOBJECTPROPS() <em>DATAOBJECTPROPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATAOBJECTPROPS()
	 * @generated
	 * @ordered
	 */
	protected DATAOBJECTPROPS dATAOBJECTPROPS;

	/**
	 * The cached value of the '{@link #getUNITSPEC() <em>UNITSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUNITSPEC()
	 * @generated
	 * @ordered
	 */
	protected UNITSPEC uNITSPEC;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPARAMSPECImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPARAMSPEC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPARAMS getCOMPARAMS() {
		return cOMPARAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPARAMS(COMPARAMS newCOMPARAMS, NotificationChain msgs) {
		COMPARAMS oldCOMPARAMS = cOMPARAMS;
		cOMPARAMS = newCOMPARAMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMSPEC__COMPARAMS, oldCOMPARAMS, newCOMPARAMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPARAMS(COMPARAMS newCOMPARAMS) {
		if (newCOMPARAMS != cOMPARAMS) {
			NotificationChain msgs = null;
			if (cOMPARAMS != null)
				msgs = ((InternalEObject)cOMPARAMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMSPEC__COMPARAMS, null, msgs);
			if (newCOMPARAMS != null)
				msgs = ((InternalEObject)newCOMPARAMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMSPEC__COMPARAMS, null, msgs);
			msgs = basicSetCOMPARAMS(newCOMPARAMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMSPEC__COMPARAMS, newCOMPARAMS, newCOMPARAMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DATAOBJECTPROPS getDATAOBJECTPROPS() {
		return dATAOBJECTPROPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDATAOBJECTPROPS(DATAOBJECTPROPS newDATAOBJECTPROPS, NotificationChain msgs) {
		DATAOBJECTPROPS oldDATAOBJECTPROPS = dATAOBJECTPROPS;
		dATAOBJECTPROPS = newDATAOBJECTPROPS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMSPEC__DATAOBJECTPROPS, oldDATAOBJECTPROPS, newDATAOBJECTPROPS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATAOBJECTPROPS(DATAOBJECTPROPS newDATAOBJECTPROPS) {
		if (newDATAOBJECTPROPS != dATAOBJECTPROPS) {
			NotificationChain msgs = null;
			if (dATAOBJECTPROPS != null)
				msgs = ((InternalEObject)dATAOBJECTPROPS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMSPEC__DATAOBJECTPROPS, null, msgs);
			if (newDATAOBJECTPROPS != null)
				msgs = ((InternalEObject)newDATAOBJECTPROPS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMSPEC__DATAOBJECTPROPS, null, msgs);
			msgs = basicSetDATAOBJECTPROPS(newDATAOBJECTPROPS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMSPEC__DATAOBJECTPROPS, newDATAOBJECTPROPS, newDATAOBJECTPROPS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNITSPEC getUNITSPEC() {
		return uNITSPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUNITSPEC(UNITSPEC newUNITSPEC, NotificationChain msgs) {
		UNITSPEC oldUNITSPEC = uNITSPEC;
		uNITSPEC = newUNITSPEC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMSPEC__UNITSPEC, oldUNITSPEC, newUNITSPEC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUNITSPEC(UNITSPEC newUNITSPEC) {
		if (newUNITSPEC != uNITSPEC) {
			NotificationChain msgs = null;
			if (uNITSPEC != null)
				msgs = ((InternalEObject)uNITSPEC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMSPEC__UNITSPEC, null, msgs);
			if (newUNITSPEC != null)
				msgs = ((InternalEObject)newUNITSPEC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAMSPEC__UNITSPEC, null, msgs);
			msgs = basicSetUNITSPEC(newUNITSPEC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAMSPEC__UNITSPEC, newUNITSPEC, newUNITSPEC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMSPEC__COMPARAMS:
				return basicSetCOMPARAMS(null, msgs);
			case OdxXhtmlPackage.COMPARAMSPEC__DATAOBJECTPROPS:
				return basicSetDATAOBJECTPROPS(null, msgs);
			case OdxXhtmlPackage.COMPARAMSPEC__UNITSPEC:
				return basicSetUNITSPEC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMSPEC__COMPARAMS:
				return getCOMPARAMS();
			case OdxXhtmlPackage.COMPARAMSPEC__DATAOBJECTPROPS:
				return getDATAOBJECTPROPS();
			case OdxXhtmlPackage.COMPARAMSPEC__UNITSPEC:
				return getUNITSPEC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMSPEC__COMPARAMS:
				setCOMPARAMS((COMPARAMS)newValue);
				return;
			case OdxXhtmlPackage.COMPARAMSPEC__DATAOBJECTPROPS:
				setDATAOBJECTPROPS((DATAOBJECTPROPS)newValue);
				return;
			case OdxXhtmlPackage.COMPARAMSPEC__UNITSPEC:
				setUNITSPEC((UNITSPEC)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMSPEC__COMPARAMS:
				setCOMPARAMS((COMPARAMS)null);
				return;
			case OdxXhtmlPackage.COMPARAMSPEC__DATAOBJECTPROPS:
				setDATAOBJECTPROPS((DATAOBJECTPROPS)null);
				return;
			case OdxXhtmlPackage.COMPARAMSPEC__UNITSPEC:
				setUNITSPEC((UNITSPEC)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAMSPEC__COMPARAMS:
				return cOMPARAMS != null;
			case OdxXhtmlPackage.COMPARAMSPEC__DATAOBJECTPROPS:
				return dATAOBJECTPROPS != null;
			case OdxXhtmlPackage.COMPARAMSPEC__UNITSPEC:
				return uNITSPEC != null;
		}
		return super.eIsSet(featureID);
	}

} //COMPARAMSPECImpl
