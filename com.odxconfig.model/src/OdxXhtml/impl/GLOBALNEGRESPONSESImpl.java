/**
 */
package OdxXhtml.impl;

import OdxXhtml.GLOBALNEGRESPONSE;
import OdxXhtml.GLOBALNEGRESPONSES;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GLOBALNEGRESPONSES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.GLOBALNEGRESPONSESImpl#getGLOBALNEGRESPONSE <em>GLOBALNEGRESPONSE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GLOBALNEGRESPONSESImpl extends MinimalEObjectImpl.Container implements GLOBALNEGRESPONSES {
	/**
	 * The cached value of the '{@link #getGLOBALNEGRESPONSE() <em>GLOBALNEGRESPONSE</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGLOBALNEGRESPONSE()
	 * @generated
	 * @ordered
	 */
	protected EList<GLOBALNEGRESPONSE> gLOBALNEGRESPONSE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GLOBALNEGRESPONSESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getGLOBALNEGRESPONSES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<GLOBALNEGRESPONSE> getGLOBALNEGRESPONSE() {
		if (gLOBALNEGRESPONSE == null) {
			gLOBALNEGRESPONSE = new EObjectContainmentEList<GLOBALNEGRESPONSE>(GLOBALNEGRESPONSE.class, this, OdxXhtmlPackage.GLOBALNEGRESPONSES__GLOBALNEGRESPONSE);
		}
		return gLOBALNEGRESPONSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.GLOBALNEGRESPONSES__GLOBALNEGRESPONSE:
				return ((InternalEList<?>)getGLOBALNEGRESPONSE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.GLOBALNEGRESPONSES__GLOBALNEGRESPONSE:
				return getGLOBALNEGRESPONSE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.GLOBALNEGRESPONSES__GLOBALNEGRESPONSE:
				getGLOBALNEGRESPONSE().clear();
				getGLOBALNEGRESPONSE().addAll((Collection<? extends GLOBALNEGRESPONSE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.GLOBALNEGRESPONSES__GLOBALNEGRESPONSE:
				getGLOBALNEGRESPONSE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.GLOBALNEGRESPONSES__GLOBALNEGRESPONSE:
				return gLOBALNEGRESPONSE != null && !gLOBALNEGRESPONSE.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //GLOBALNEGRESPONSESImpl
