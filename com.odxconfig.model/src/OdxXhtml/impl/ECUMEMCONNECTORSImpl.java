/**
 */
package OdxXhtml.impl;

import OdxXhtml.ECUMEMCONNECTOR;
import OdxXhtml.ECUMEMCONNECTORS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUMEMCONNECTORS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ECUMEMCONNECTORSImpl#getECUMEMCONNECTOR <em>ECUMEMCONNECTOR</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECUMEMCONNECTORSImpl extends MinimalEObjectImpl.Container implements ECUMEMCONNECTORS {
	/**
	 * The cached value of the '{@link #getECUMEMCONNECTOR() <em>ECUMEMCONNECTOR</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUMEMCONNECTOR()
	 * @generated
	 * @ordered
	 */
	protected EList<ECUMEMCONNECTOR> eCUMEMCONNECTOR;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUMEMCONNECTORSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUMEMCONNECTORS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ECUMEMCONNECTOR> getECUMEMCONNECTOR() {
		if (eCUMEMCONNECTOR == null) {
			eCUMEMCONNECTOR = new EObjectContainmentEList<ECUMEMCONNECTOR>(ECUMEMCONNECTOR.class, this, OdxXhtmlPackage.ECUMEMCONNECTORS__ECUMEMCONNECTOR);
		}
		return eCUMEMCONNECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTORS__ECUMEMCONNECTOR:
				return ((InternalEList<?>)getECUMEMCONNECTOR()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTORS__ECUMEMCONNECTOR:
				return getECUMEMCONNECTOR();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTORS__ECUMEMCONNECTOR:
				getECUMEMCONNECTOR().clear();
				getECUMEMCONNECTOR().addAll((Collection<? extends ECUMEMCONNECTOR>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTORS__ECUMEMCONNECTOR:
				getECUMEMCONNECTOR().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUMEMCONNECTORS__ECUMEMCONNECTOR:
				return eCUMEMCONNECTOR != null && !eCUMEMCONNECTOR.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ECUMEMCONNECTORSImpl
