/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSSEGMENT;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PHYSSEGMENT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PHYSSEGMENTImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSSEGMENTImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSSEGMENTImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSSEGMENTImpl#getFILLBYTE <em>FILLBYTE</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSSEGMENTImpl#getBLOCKSIZE <em>BLOCKSIZE</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSSEGMENTImpl#getSTARTADDRESS <em>STARTADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSSEGMENTImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSSEGMENTImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PHYSSEGMENTImpl extends MinimalEObjectImpl.Container implements PHYSSEGMENT {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getFILLBYTE() <em>FILLBYTE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILLBYTE()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] FILLBYTE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFILLBYTE() <em>FILLBYTE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILLBYTE()
	 * @generated
	 * @ordered
	 */
	protected byte[] fILLBYTE = FILLBYTE_EDEFAULT;

	/**
	 * The default value of the '{@link #getBLOCKSIZE() <em>BLOCKSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBLOCKSIZE()
	 * @generated
	 * @ordered
	 */
	protected static final long BLOCKSIZE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBLOCKSIZE() <em>BLOCKSIZE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBLOCKSIZE()
	 * @generated
	 * @ordered
	 */
	protected long bLOCKSIZE = BLOCKSIZE_EDEFAULT;

	/**
	 * This is true if the BLOCKSIZE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bLOCKSIZEESet;

	/**
	 * The default value of the '{@link #getSTARTADDRESS() <em>STARTADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTARTADDRESS()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] STARTADDRESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSTARTADDRESS() <em>STARTADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTARTADDRESS()
	 * @generated
	 * @ordered
	 */
	protected byte[] sTARTADDRESS = STARTADDRESS_EDEFAULT;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PHYSSEGMENTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPHYSSEGMENT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSSEGMENT__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSSEGMENT__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSSEGMENT__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PHYSSEGMENT__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getFILLBYTE() {
		return fILLBYTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFILLBYTE(byte[] newFILLBYTE) {
		byte[] oldFILLBYTE = fILLBYTE;
		fILLBYTE = newFILLBYTE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__FILLBYTE, oldFILLBYTE, fILLBYTE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBLOCKSIZE() {
		return bLOCKSIZE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBLOCKSIZE(long newBLOCKSIZE) {
		long oldBLOCKSIZE = bLOCKSIZE;
		bLOCKSIZE = newBLOCKSIZE;
		boolean oldBLOCKSIZEESet = bLOCKSIZEESet;
		bLOCKSIZEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__BLOCKSIZE, oldBLOCKSIZE, bLOCKSIZE, !oldBLOCKSIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBLOCKSIZE() {
		long oldBLOCKSIZE = bLOCKSIZE;
		boolean oldBLOCKSIZEESet = bLOCKSIZEESet;
		bLOCKSIZE = BLOCKSIZE_EDEFAULT;
		bLOCKSIZEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSSEGMENT__BLOCKSIZE, oldBLOCKSIZE, BLOCKSIZE_EDEFAULT, oldBLOCKSIZEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBLOCKSIZE() {
		return bLOCKSIZEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getSTARTADDRESS() {
		return sTARTADDRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSTARTADDRESS(byte[] newSTARTADDRESS) {
		byte[] oldSTARTADDRESS = sTARTADDRESS;
		sTARTADDRESS = newSTARTADDRESS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__STARTADDRESS, oldSTARTADDRESS, sTARTADDRESS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSSEGMENT__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENT__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.PHYSSEGMENT__DESC:
				return basicSetDESC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENT__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.PHYSSEGMENT__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.PHYSSEGMENT__DESC:
				return getDESC();
			case OdxXhtmlPackage.PHYSSEGMENT__FILLBYTE:
				return getFILLBYTE();
			case OdxXhtmlPackage.PHYSSEGMENT__BLOCKSIZE:
				return getBLOCKSIZE();
			case OdxXhtmlPackage.PHYSSEGMENT__STARTADDRESS:
				return getSTARTADDRESS();
			case OdxXhtmlPackage.PHYSSEGMENT__ID:
				return getID();
			case OdxXhtmlPackage.PHYSSEGMENT__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENT__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__FILLBYTE:
				setFILLBYTE((byte[])newValue);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__BLOCKSIZE:
				setBLOCKSIZE((Long)newValue);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__STARTADDRESS:
				setSTARTADDRESS((byte[])newValue);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENT__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__FILLBYTE:
				setFILLBYTE(FILLBYTE_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__BLOCKSIZE:
				unsetBLOCKSIZE();
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__STARTADDRESS:
				setSTARTADDRESS(STARTADDRESS_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.PHYSSEGMENT__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSSEGMENT__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.PHYSSEGMENT__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.PHYSSEGMENT__DESC:
				return dESC != null;
			case OdxXhtmlPackage.PHYSSEGMENT__FILLBYTE:
				return FILLBYTE_EDEFAULT == null ? fILLBYTE != null : !FILLBYTE_EDEFAULT.equals(fILLBYTE);
			case OdxXhtmlPackage.PHYSSEGMENT__BLOCKSIZE:
				return isSetBLOCKSIZE();
			case OdxXhtmlPackage.PHYSSEGMENT__STARTADDRESS:
				return STARTADDRESS_EDEFAULT == null ? sTARTADDRESS != null : !STARTADDRESS_EDEFAULT.equals(sTARTADDRESS);
			case OdxXhtmlPackage.PHYSSEGMENT__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.PHYSSEGMENT__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", fILLBYTE: ");
		result.append(fILLBYTE);
		result.append(", bLOCKSIZE: ");
		if (bLOCKSIZEESet) result.append(bLOCKSIZE); else result.append("<unset>");
		result.append(", sTARTADDRESS: ");
		result.append(sTARTADDRESS);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //PHYSSEGMENTImpl
