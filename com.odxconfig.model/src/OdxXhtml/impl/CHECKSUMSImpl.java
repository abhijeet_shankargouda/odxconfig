/**
 */
package OdxXhtml.impl;

import OdxXhtml.CHECKSUM;
import OdxXhtml.CHECKSUMS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CHECKSUMS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.CHECKSUMSImpl#getCHECKSUM <em>CHECKSUM</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CHECKSUMSImpl extends MinimalEObjectImpl.Container implements CHECKSUMS {
	/**
	 * The cached value of the '{@link #getCHECKSUM() <em>CHECKSUM</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCHECKSUM()
	 * @generated
	 * @ordered
	 */
	protected EList<CHECKSUM> cHECKSUM;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CHECKSUMSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCHECKSUMS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<CHECKSUM> getCHECKSUM() {
		if (cHECKSUM == null) {
			cHECKSUM = new EObjectContainmentEList<CHECKSUM>(CHECKSUM.class, this, OdxXhtmlPackage.CHECKSUMS__CHECKSUM);
		}
		return cHECKSUM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUMS__CHECKSUM:
				return ((InternalEList<?>)getCHECKSUM()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUMS__CHECKSUM:
				return getCHECKSUM();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUMS__CHECKSUM:
				getCHECKSUM().clear();
				getCHECKSUM().addAll((Collection<? extends CHECKSUM>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUMS__CHECKSUM:
				getCHECKSUM().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.CHECKSUMS__CHECKSUM:
				return cHECKSUM != null && !cHECKSUM.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CHECKSUMSImpl
