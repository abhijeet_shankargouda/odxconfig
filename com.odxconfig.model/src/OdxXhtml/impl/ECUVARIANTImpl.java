/**
 */
package OdxXhtml.impl;

import OdxXhtml.ACCESSLEVELS;
import OdxXhtml.AUTMETHODS;
import OdxXhtml.DIAGVARIABLES;
import OdxXhtml.DYNDEFINEDSPEC;
import OdxXhtml.ECUVARIANT;
import OdxXhtml.ECUVARIANTPATTERNS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PARENTREFS;
import OdxXhtml.VARIABLEGROUPS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUVARIANT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTImpl#getDIAGVARIABLES <em>DIAGVARIABLES</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTImpl#getVARIABLEGROUPS <em>VARIABLEGROUPS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTImpl#getECUVARIANTPATTERNS <em>ECUVARIANTPATTERNS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTImpl#getAUTMETHODS <em>AUTMETHODS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTImpl#getDYNDEFINEDSPEC <em>DYNDEFINEDSPEC</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTImpl#getACCESSLEVELS <em>ACCESSLEVELS</em>}</li>
 *   <li>{@link OdxXhtml.impl.ECUVARIANTImpl#getPARENTREFS <em>PARENTREFS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ECUVARIANTImpl extends HIERARCHYELEMENTImpl implements ECUVARIANT {
	/**
	 * The cached value of the '{@link #getDIAGVARIABLES() <em>DIAGVARIABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGVARIABLES()
	 * @generated
	 * @ordered
	 */
	protected DIAGVARIABLES dIAGVARIABLES;

	/**
	 * The cached value of the '{@link #getVARIABLEGROUPS() <em>VARIABLEGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVARIABLEGROUPS()
	 * @generated
	 * @ordered
	 */
	protected VARIABLEGROUPS vARIABLEGROUPS;

	/**
	 * The cached value of the '{@link #getECUVARIANTPATTERNS() <em>ECUVARIANTPATTERNS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUVARIANTPATTERNS()
	 * @generated
	 * @ordered
	 */
	protected ECUVARIANTPATTERNS eCUVARIANTPATTERNS;

	/**
	 * The cached value of the '{@link #getAUTMETHODS() <em>AUTMETHODS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUTMETHODS()
	 * @generated
	 * @ordered
	 */
	protected AUTMETHODS aUTMETHODS;

	/**
	 * The cached value of the '{@link #getDYNDEFINEDSPEC() <em>DYNDEFINEDSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNDEFINEDSPEC()
	 * @generated
	 * @ordered
	 */
	protected DYNDEFINEDSPEC dYNDEFINEDSPEC;

	/**
	 * The cached value of the '{@link #getACCESSLEVELS() <em>ACCESSLEVELS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACCESSLEVELS()
	 * @generated
	 * @ordered
	 */
	protected ACCESSLEVELS aCCESSLEVELS;

	/**
	 * The cached value of the '{@link #getPARENTREFS() <em>PARENTREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARENTREFS()
	 * @generated
	 * @ordered
	 */
	protected PARENTREFS pARENTREFS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUVARIANTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUVARIANT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGVARIABLES getDIAGVARIABLES() {
		return dIAGVARIABLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGVARIABLES(DIAGVARIABLES newDIAGVARIABLES, NotificationChain msgs) {
		DIAGVARIABLES oldDIAGVARIABLES = dIAGVARIABLES;
		dIAGVARIABLES = newDIAGVARIABLES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__DIAGVARIABLES, oldDIAGVARIABLES, newDIAGVARIABLES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGVARIABLES(DIAGVARIABLES newDIAGVARIABLES) {
		if (newDIAGVARIABLES != dIAGVARIABLES) {
			NotificationChain msgs = null;
			if (dIAGVARIABLES != null)
				msgs = ((InternalEObject)dIAGVARIABLES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__DIAGVARIABLES, null, msgs);
			if (newDIAGVARIABLES != null)
				msgs = ((InternalEObject)newDIAGVARIABLES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__DIAGVARIABLES, null, msgs);
			msgs = basicSetDIAGVARIABLES(newDIAGVARIABLES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__DIAGVARIABLES, newDIAGVARIABLES, newDIAGVARIABLES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VARIABLEGROUPS getVARIABLEGROUPS() {
		return vARIABLEGROUPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVARIABLEGROUPS(VARIABLEGROUPS newVARIABLEGROUPS, NotificationChain msgs) {
		VARIABLEGROUPS oldVARIABLEGROUPS = vARIABLEGROUPS;
		vARIABLEGROUPS = newVARIABLEGROUPS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__VARIABLEGROUPS, oldVARIABLEGROUPS, newVARIABLEGROUPS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVARIABLEGROUPS(VARIABLEGROUPS newVARIABLEGROUPS) {
		if (newVARIABLEGROUPS != vARIABLEGROUPS) {
			NotificationChain msgs = null;
			if (vARIABLEGROUPS != null)
				msgs = ((InternalEObject)vARIABLEGROUPS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__VARIABLEGROUPS, null, msgs);
			if (newVARIABLEGROUPS != null)
				msgs = ((InternalEObject)newVARIABLEGROUPS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__VARIABLEGROUPS, null, msgs);
			msgs = basicSetVARIABLEGROUPS(newVARIABLEGROUPS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__VARIABLEGROUPS, newVARIABLEGROUPS, newVARIABLEGROUPS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUVARIANTPATTERNS getECUVARIANTPATTERNS() {
		return eCUVARIANTPATTERNS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetECUVARIANTPATTERNS(ECUVARIANTPATTERNS newECUVARIANTPATTERNS, NotificationChain msgs) {
		ECUVARIANTPATTERNS oldECUVARIANTPATTERNS = eCUVARIANTPATTERNS;
		eCUVARIANTPATTERNS = newECUVARIANTPATTERNS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__ECUVARIANTPATTERNS, oldECUVARIANTPATTERNS, newECUVARIANTPATTERNS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setECUVARIANTPATTERNS(ECUVARIANTPATTERNS newECUVARIANTPATTERNS) {
		if (newECUVARIANTPATTERNS != eCUVARIANTPATTERNS) {
			NotificationChain msgs = null;
			if (eCUVARIANTPATTERNS != null)
				msgs = ((InternalEObject)eCUVARIANTPATTERNS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__ECUVARIANTPATTERNS, null, msgs);
			if (newECUVARIANTPATTERNS != null)
				msgs = ((InternalEObject)newECUVARIANTPATTERNS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__ECUVARIANTPATTERNS, null, msgs);
			msgs = basicSetECUVARIANTPATTERNS(newECUVARIANTPATTERNS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__ECUVARIANTPATTERNS, newECUVARIANTPATTERNS, newECUVARIANTPATTERNS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AUTMETHODS getAUTMETHODS() {
		return aUTMETHODS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAUTMETHODS(AUTMETHODS newAUTMETHODS, NotificationChain msgs) {
		AUTMETHODS oldAUTMETHODS = aUTMETHODS;
		aUTMETHODS = newAUTMETHODS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__AUTMETHODS, oldAUTMETHODS, newAUTMETHODS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAUTMETHODS(AUTMETHODS newAUTMETHODS) {
		if (newAUTMETHODS != aUTMETHODS) {
			NotificationChain msgs = null;
			if (aUTMETHODS != null)
				msgs = ((InternalEObject)aUTMETHODS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__AUTMETHODS, null, msgs);
			if (newAUTMETHODS != null)
				msgs = ((InternalEObject)newAUTMETHODS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__AUTMETHODS, null, msgs);
			msgs = basicSetAUTMETHODS(newAUTMETHODS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__AUTMETHODS, newAUTMETHODS, newAUTMETHODS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNDEFINEDSPEC getDYNDEFINEDSPEC() {
		return dYNDEFINEDSPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDYNDEFINEDSPEC(DYNDEFINEDSPEC newDYNDEFINEDSPEC, NotificationChain msgs) {
		DYNDEFINEDSPEC oldDYNDEFINEDSPEC = dYNDEFINEDSPEC;
		dYNDEFINEDSPEC = newDYNDEFINEDSPEC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__DYNDEFINEDSPEC, oldDYNDEFINEDSPEC, newDYNDEFINEDSPEC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDYNDEFINEDSPEC(DYNDEFINEDSPEC newDYNDEFINEDSPEC) {
		if (newDYNDEFINEDSPEC != dYNDEFINEDSPEC) {
			NotificationChain msgs = null;
			if (dYNDEFINEDSPEC != null)
				msgs = ((InternalEObject)dYNDEFINEDSPEC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__DYNDEFINEDSPEC, null, msgs);
			if (newDYNDEFINEDSPEC != null)
				msgs = ((InternalEObject)newDYNDEFINEDSPEC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__DYNDEFINEDSPEC, null, msgs);
			msgs = basicSetDYNDEFINEDSPEC(newDYNDEFINEDSPEC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__DYNDEFINEDSPEC, newDYNDEFINEDSPEC, newDYNDEFINEDSPEC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ACCESSLEVELS getACCESSLEVELS() {
		return aCCESSLEVELS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetACCESSLEVELS(ACCESSLEVELS newACCESSLEVELS, NotificationChain msgs) {
		ACCESSLEVELS oldACCESSLEVELS = aCCESSLEVELS;
		aCCESSLEVELS = newACCESSLEVELS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__ACCESSLEVELS, oldACCESSLEVELS, newACCESSLEVELS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setACCESSLEVELS(ACCESSLEVELS newACCESSLEVELS) {
		if (newACCESSLEVELS != aCCESSLEVELS) {
			NotificationChain msgs = null;
			if (aCCESSLEVELS != null)
				msgs = ((InternalEObject)aCCESSLEVELS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__ACCESSLEVELS, null, msgs);
			if (newACCESSLEVELS != null)
				msgs = ((InternalEObject)newACCESSLEVELS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__ACCESSLEVELS, null, msgs);
			msgs = basicSetACCESSLEVELS(newACCESSLEVELS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__ACCESSLEVELS, newACCESSLEVELS, newACCESSLEVELS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PARENTREFS getPARENTREFS() {
		return pARENTREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPARENTREFS(PARENTREFS newPARENTREFS, NotificationChain msgs) {
		PARENTREFS oldPARENTREFS = pARENTREFS;
		pARENTREFS = newPARENTREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__PARENTREFS, oldPARENTREFS, newPARENTREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPARENTREFS(PARENTREFS newPARENTREFS) {
		if (newPARENTREFS != pARENTREFS) {
			NotificationChain msgs = null;
			if (pARENTREFS != null)
				msgs = ((InternalEObject)pARENTREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__PARENTREFS, null, msgs);
			if (newPARENTREFS != null)
				msgs = ((InternalEObject)newPARENTREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ECUVARIANT__PARENTREFS, null, msgs);
			msgs = basicSetPARENTREFS(newPARENTREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ECUVARIANT__PARENTREFS, newPARENTREFS, newPARENTREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANT__DIAGVARIABLES:
				return basicSetDIAGVARIABLES(null, msgs);
			case OdxXhtmlPackage.ECUVARIANT__VARIABLEGROUPS:
				return basicSetVARIABLEGROUPS(null, msgs);
			case OdxXhtmlPackage.ECUVARIANT__ECUVARIANTPATTERNS:
				return basicSetECUVARIANTPATTERNS(null, msgs);
			case OdxXhtmlPackage.ECUVARIANT__AUTMETHODS:
				return basicSetAUTMETHODS(null, msgs);
			case OdxXhtmlPackage.ECUVARIANT__DYNDEFINEDSPEC:
				return basicSetDYNDEFINEDSPEC(null, msgs);
			case OdxXhtmlPackage.ECUVARIANT__ACCESSLEVELS:
				return basicSetACCESSLEVELS(null, msgs);
			case OdxXhtmlPackage.ECUVARIANT__PARENTREFS:
				return basicSetPARENTREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANT__DIAGVARIABLES:
				return getDIAGVARIABLES();
			case OdxXhtmlPackage.ECUVARIANT__VARIABLEGROUPS:
				return getVARIABLEGROUPS();
			case OdxXhtmlPackage.ECUVARIANT__ECUVARIANTPATTERNS:
				return getECUVARIANTPATTERNS();
			case OdxXhtmlPackage.ECUVARIANT__AUTMETHODS:
				return getAUTMETHODS();
			case OdxXhtmlPackage.ECUVARIANT__DYNDEFINEDSPEC:
				return getDYNDEFINEDSPEC();
			case OdxXhtmlPackage.ECUVARIANT__ACCESSLEVELS:
				return getACCESSLEVELS();
			case OdxXhtmlPackage.ECUVARIANT__PARENTREFS:
				return getPARENTREFS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANT__DIAGVARIABLES:
				setDIAGVARIABLES((DIAGVARIABLES)newValue);
				return;
			case OdxXhtmlPackage.ECUVARIANT__VARIABLEGROUPS:
				setVARIABLEGROUPS((VARIABLEGROUPS)newValue);
				return;
			case OdxXhtmlPackage.ECUVARIANT__ECUVARIANTPATTERNS:
				setECUVARIANTPATTERNS((ECUVARIANTPATTERNS)newValue);
				return;
			case OdxXhtmlPackage.ECUVARIANT__AUTMETHODS:
				setAUTMETHODS((AUTMETHODS)newValue);
				return;
			case OdxXhtmlPackage.ECUVARIANT__DYNDEFINEDSPEC:
				setDYNDEFINEDSPEC((DYNDEFINEDSPEC)newValue);
				return;
			case OdxXhtmlPackage.ECUVARIANT__ACCESSLEVELS:
				setACCESSLEVELS((ACCESSLEVELS)newValue);
				return;
			case OdxXhtmlPackage.ECUVARIANT__PARENTREFS:
				setPARENTREFS((PARENTREFS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANT__DIAGVARIABLES:
				setDIAGVARIABLES((DIAGVARIABLES)null);
				return;
			case OdxXhtmlPackage.ECUVARIANT__VARIABLEGROUPS:
				setVARIABLEGROUPS((VARIABLEGROUPS)null);
				return;
			case OdxXhtmlPackage.ECUVARIANT__ECUVARIANTPATTERNS:
				setECUVARIANTPATTERNS((ECUVARIANTPATTERNS)null);
				return;
			case OdxXhtmlPackage.ECUVARIANT__AUTMETHODS:
				setAUTMETHODS((AUTMETHODS)null);
				return;
			case OdxXhtmlPackage.ECUVARIANT__DYNDEFINEDSPEC:
				setDYNDEFINEDSPEC((DYNDEFINEDSPEC)null);
				return;
			case OdxXhtmlPackage.ECUVARIANT__ACCESSLEVELS:
				setACCESSLEVELS((ACCESSLEVELS)null);
				return;
			case OdxXhtmlPackage.ECUVARIANT__PARENTREFS:
				setPARENTREFS((PARENTREFS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ECUVARIANT__DIAGVARIABLES:
				return dIAGVARIABLES != null;
			case OdxXhtmlPackage.ECUVARIANT__VARIABLEGROUPS:
				return vARIABLEGROUPS != null;
			case OdxXhtmlPackage.ECUVARIANT__ECUVARIANTPATTERNS:
				return eCUVARIANTPATTERNS != null;
			case OdxXhtmlPackage.ECUVARIANT__AUTMETHODS:
				return aUTMETHODS != null;
			case OdxXhtmlPackage.ECUVARIANT__DYNDEFINEDSPEC:
				return dYNDEFINEDSPEC != null;
			case OdxXhtmlPackage.ECUVARIANT__ACCESSLEVELS:
				return aCCESSLEVELS != null;
			case OdxXhtmlPackage.ECUVARIANT__PARENTREFS:
				return pARENTREFS != null;
		}
		return super.eIsSet(featureID);
	}

} //ECUVARIANTImpl
