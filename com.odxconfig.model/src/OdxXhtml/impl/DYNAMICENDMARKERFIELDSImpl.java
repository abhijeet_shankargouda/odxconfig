/**
 */
package OdxXhtml.impl;

import OdxXhtml.DYNAMICENDMARKERFIELD;
import OdxXhtml.DYNAMICENDMARKERFIELDS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DYNAMICENDMARKERFIELDS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DYNAMICENDMARKERFIELDSImpl#getDYNAMICENDMARKERFIELD <em>DYNAMICENDMARKERFIELD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DYNAMICENDMARKERFIELDSImpl extends MinimalEObjectImpl.Container implements DYNAMICENDMARKERFIELDS {
	/**
	 * The cached value of the '{@link #getDYNAMICENDMARKERFIELD() <em>DYNAMICENDMARKERFIELD</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNAMICENDMARKERFIELD()
	 * @generated
	 * @ordered
	 */
	protected EList<DYNAMICENDMARKERFIELD> dYNAMICENDMARKERFIELD;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DYNAMICENDMARKERFIELDSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDYNAMICENDMARKERFIELDS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DYNAMICENDMARKERFIELD> getDYNAMICENDMARKERFIELD() {
		if (dYNAMICENDMARKERFIELD == null) {
			dYNAMICENDMARKERFIELD = new EObjectContainmentEList<DYNAMICENDMARKERFIELD>(DYNAMICENDMARKERFIELD.class, this, OdxXhtmlPackage.DYNAMICENDMARKERFIELDS__DYNAMICENDMARKERFIELD);
		}
		return dYNAMICENDMARKERFIELD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELDS__DYNAMICENDMARKERFIELD:
				return ((InternalEList<?>)getDYNAMICENDMARKERFIELD()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELDS__DYNAMICENDMARKERFIELD:
				return getDYNAMICENDMARKERFIELD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELDS__DYNAMICENDMARKERFIELD:
				getDYNAMICENDMARKERFIELD().clear();
				getDYNAMICENDMARKERFIELD().addAll((Collection<? extends DYNAMICENDMARKERFIELD>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELDS__DYNAMICENDMARKERFIELD:
				getDYNAMICENDMARKERFIELD().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICENDMARKERFIELDS__DYNAMICENDMARKERFIELD:
				return dYNAMICENDMARKERFIELD != null && !dYNAMICENDMARKERFIELD.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DYNAMICENDMARKERFIELDSImpl
