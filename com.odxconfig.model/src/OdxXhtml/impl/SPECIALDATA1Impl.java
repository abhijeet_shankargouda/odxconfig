/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SPECIALDATA1;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SPECIALDATA1</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SPECIALDATA1Impl extends MinimalEObjectImpl.Container implements SPECIALDATA1 {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SPECIALDATA1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSPECIALDATA1();
	}

} //SPECIALDATA1Impl
