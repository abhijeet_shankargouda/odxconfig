/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.ECUPROXYREFS;
import OdxXhtml.GATEWAYLOGICALLINKREFS;
import OdxXhtml.LINKCOMPARAMREFS;
import OdxXhtml.LOGICALLINK;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LOGICALLINK</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getGATEWAYLOGICALLINKREFS <em>GATEWAYLOGICALLINKREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getPHYSICALVEHICLELINKREF <em>PHYSICALVEHICLELINKREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getPROTOCOLREF <em>PROTOCOLREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getFUNCTIONALGROUPREF <em>FUNCTIONALGROUPREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getBASEVARIANTREF <em>BASEVARIANTREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getECUPROXYREFS <em>ECUPROXYREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getLINKCOMPARAMREFS <em>LINKCOMPARAMREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.LOGICALLINKImpl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LOGICALLINKImpl extends MinimalEObjectImpl.Container implements LOGICALLINK {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getGATEWAYLOGICALLINKREFS() <em>GATEWAYLOGICALLINKREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGATEWAYLOGICALLINKREFS()
	 * @generated
	 * @ordered
	 */
	protected GATEWAYLOGICALLINKREFS gATEWAYLOGICALLINKREFS;

	/**
	 * The cached value of the '{@link #getPHYSICALVEHICLELINKREF() <em>PHYSICALVEHICLELINKREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALVEHICLELINKREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK pHYSICALVEHICLELINKREF;

	/**
	 * The cached value of the '{@link #getPROTOCOLREF() <em>PROTOCOLREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROTOCOLREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK pROTOCOLREF;

	/**
	 * The cached value of the '{@link #getFUNCTIONALGROUPREF() <em>FUNCTIONALGROUPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFUNCTIONALGROUPREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK fUNCTIONALGROUPREF;

	/**
	 * The cached value of the '{@link #getBASEVARIANTREF() <em>BASEVARIANTREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASEVARIANTREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK bASEVARIANTREF;

	/**
	 * The cached value of the '{@link #getECUPROXYREFS() <em>ECUPROXYREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getECUPROXYREFS()
	 * @generated
	 * @ordered
	 */
	protected ECUPROXYREFS eCUPROXYREFS;

	/**
	 * The cached value of the '{@link #getLINKCOMPARAMREFS() <em>LINKCOMPARAMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLINKCOMPARAMREFS()
	 * @generated
	 * @ordered
	 */
	protected LINKCOMPARAMREFS lINKCOMPARAMREFS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LOGICALLINKImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLOGICALLINK();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GATEWAYLOGICALLINKREFS getGATEWAYLOGICALLINKREFS() {
		return gATEWAYLOGICALLINKREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGATEWAYLOGICALLINKREFS(GATEWAYLOGICALLINKREFS newGATEWAYLOGICALLINKREFS, NotificationChain msgs) {
		GATEWAYLOGICALLINKREFS oldGATEWAYLOGICALLINKREFS = gATEWAYLOGICALLINKREFS;
		gATEWAYLOGICALLINKREFS = newGATEWAYLOGICALLINKREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__GATEWAYLOGICALLINKREFS, oldGATEWAYLOGICALLINKREFS, newGATEWAYLOGICALLINKREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGATEWAYLOGICALLINKREFS(GATEWAYLOGICALLINKREFS newGATEWAYLOGICALLINKREFS) {
		if (newGATEWAYLOGICALLINKREFS != gATEWAYLOGICALLINKREFS) {
			NotificationChain msgs = null;
			if (gATEWAYLOGICALLINKREFS != null)
				msgs = ((InternalEObject)gATEWAYLOGICALLINKREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__GATEWAYLOGICALLINKREFS, null, msgs);
			if (newGATEWAYLOGICALLINKREFS != null)
				msgs = ((InternalEObject)newGATEWAYLOGICALLINKREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__GATEWAYLOGICALLINKREFS, null, msgs);
			msgs = basicSetGATEWAYLOGICALLINKREFS(newGATEWAYLOGICALLINKREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__GATEWAYLOGICALLINKREFS, newGATEWAYLOGICALLINKREFS, newGATEWAYLOGICALLINKREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getPHYSICALVEHICLELINKREF() {
		return pHYSICALVEHICLELINKREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPHYSICALVEHICLELINKREF(ODXLINK newPHYSICALVEHICLELINKREF, NotificationChain msgs) {
		ODXLINK oldPHYSICALVEHICLELINKREF = pHYSICALVEHICLELINKREF;
		pHYSICALVEHICLELINKREF = newPHYSICALVEHICLELINKREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__PHYSICALVEHICLELINKREF, oldPHYSICALVEHICLELINKREF, newPHYSICALVEHICLELINKREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHYSICALVEHICLELINKREF(ODXLINK newPHYSICALVEHICLELINKREF) {
		if (newPHYSICALVEHICLELINKREF != pHYSICALVEHICLELINKREF) {
			NotificationChain msgs = null;
			if (pHYSICALVEHICLELINKREF != null)
				msgs = ((InternalEObject)pHYSICALVEHICLELINKREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__PHYSICALVEHICLELINKREF, null, msgs);
			if (newPHYSICALVEHICLELINKREF != null)
				msgs = ((InternalEObject)newPHYSICALVEHICLELINKREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__PHYSICALVEHICLELINKREF, null, msgs);
			msgs = basicSetPHYSICALVEHICLELINKREF(newPHYSICALVEHICLELINKREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__PHYSICALVEHICLELINKREF, newPHYSICALVEHICLELINKREF, newPHYSICALVEHICLELINKREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getPROTOCOLREF() {
		return pROTOCOLREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROTOCOLREF(ODXLINK newPROTOCOLREF, NotificationChain msgs) {
		ODXLINK oldPROTOCOLREF = pROTOCOLREF;
		pROTOCOLREF = newPROTOCOLREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__PROTOCOLREF, oldPROTOCOLREF, newPROTOCOLREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROTOCOLREF(ODXLINK newPROTOCOLREF) {
		if (newPROTOCOLREF != pROTOCOLREF) {
			NotificationChain msgs = null;
			if (pROTOCOLREF != null)
				msgs = ((InternalEObject)pROTOCOLREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__PROTOCOLREF, null, msgs);
			if (newPROTOCOLREF != null)
				msgs = ((InternalEObject)newPROTOCOLREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__PROTOCOLREF, null, msgs);
			msgs = basicSetPROTOCOLREF(newPROTOCOLREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__PROTOCOLREF, newPROTOCOLREF, newPROTOCOLREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getFUNCTIONALGROUPREF() {
		return fUNCTIONALGROUPREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFUNCTIONALGROUPREF(ODXLINK newFUNCTIONALGROUPREF, NotificationChain msgs) {
		ODXLINK oldFUNCTIONALGROUPREF = fUNCTIONALGROUPREF;
		fUNCTIONALGROUPREF = newFUNCTIONALGROUPREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__FUNCTIONALGROUPREF, oldFUNCTIONALGROUPREF, newFUNCTIONALGROUPREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFUNCTIONALGROUPREF(ODXLINK newFUNCTIONALGROUPREF) {
		if (newFUNCTIONALGROUPREF != fUNCTIONALGROUPREF) {
			NotificationChain msgs = null;
			if (fUNCTIONALGROUPREF != null)
				msgs = ((InternalEObject)fUNCTIONALGROUPREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__FUNCTIONALGROUPREF, null, msgs);
			if (newFUNCTIONALGROUPREF != null)
				msgs = ((InternalEObject)newFUNCTIONALGROUPREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__FUNCTIONALGROUPREF, null, msgs);
			msgs = basicSetFUNCTIONALGROUPREF(newFUNCTIONALGROUPREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__FUNCTIONALGROUPREF, newFUNCTIONALGROUPREF, newFUNCTIONALGROUPREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getBASEVARIANTREF() {
		return bASEVARIANTREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBASEVARIANTREF(ODXLINK newBASEVARIANTREF, NotificationChain msgs) {
		ODXLINK oldBASEVARIANTREF = bASEVARIANTREF;
		bASEVARIANTREF = newBASEVARIANTREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__BASEVARIANTREF, oldBASEVARIANTREF, newBASEVARIANTREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBASEVARIANTREF(ODXLINK newBASEVARIANTREF) {
		if (newBASEVARIANTREF != bASEVARIANTREF) {
			NotificationChain msgs = null;
			if (bASEVARIANTREF != null)
				msgs = ((InternalEObject)bASEVARIANTREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__BASEVARIANTREF, null, msgs);
			if (newBASEVARIANTREF != null)
				msgs = ((InternalEObject)newBASEVARIANTREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__BASEVARIANTREF, null, msgs);
			msgs = basicSetBASEVARIANTREF(newBASEVARIANTREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__BASEVARIANTREF, newBASEVARIANTREF, newBASEVARIANTREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ECUPROXYREFS getECUPROXYREFS() {
		return eCUPROXYREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetECUPROXYREFS(ECUPROXYREFS newECUPROXYREFS, NotificationChain msgs) {
		ECUPROXYREFS oldECUPROXYREFS = eCUPROXYREFS;
		eCUPROXYREFS = newECUPROXYREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__ECUPROXYREFS, oldECUPROXYREFS, newECUPROXYREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setECUPROXYREFS(ECUPROXYREFS newECUPROXYREFS) {
		if (newECUPROXYREFS != eCUPROXYREFS) {
			NotificationChain msgs = null;
			if (eCUPROXYREFS != null)
				msgs = ((InternalEObject)eCUPROXYREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__ECUPROXYREFS, null, msgs);
			if (newECUPROXYREFS != null)
				msgs = ((InternalEObject)newECUPROXYREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__ECUPROXYREFS, null, msgs);
			msgs = basicSetECUPROXYREFS(newECUPROXYREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__ECUPROXYREFS, newECUPROXYREFS, newECUPROXYREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LINKCOMPARAMREFS getLINKCOMPARAMREFS() {
		return lINKCOMPARAMREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLINKCOMPARAMREFS(LINKCOMPARAMREFS newLINKCOMPARAMREFS, NotificationChain msgs) {
		LINKCOMPARAMREFS oldLINKCOMPARAMREFS = lINKCOMPARAMREFS;
		lINKCOMPARAMREFS = newLINKCOMPARAMREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__LINKCOMPARAMREFS, oldLINKCOMPARAMREFS, newLINKCOMPARAMREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLINKCOMPARAMREFS(LINKCOMPARAMREFS newLINKCOMPARAMREFS) {
		if (newLINKCOMPARAMREFS != lINKCOMPARAMREFS) {
			NotificationChain msgs = null;
			if (lINKCOMPARAMREFS != null)
				msgs = ((InternalEObject)lINKCOMPARAMREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__LINKCOMPARAMREFS, null, msgs);
			if (newLINKCOMPARAMREFS != null)
				msgs = ((InternalEObject)newLINKCOMPARAMREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.LOGICALLINK__LINKCOMPARAMREFS, null, msgs);
			msgs = basicSetLINKCOMPARAMREFS(newLINKCOMPARAMREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__LINKCOMPARAMREFS, newLINKCOMPARAMREFS, newLINKCOMPARAMREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.LOGICALLINK__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINK__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.LOGICALLINK__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.LOGICALLINK__GATEWAYLOGICALLINKREFS:
				return basicSetGATEWAYLOGICALLINKREFS(null, msgs);
			case OdxXhtmlPackage.LOGICALLINK__PHYSICALVEHICLELINKREF:
				return basicSetPHYSICALVEHICLELINKREF(null, msgs);
			case OdxXhtmlPackage.LOGICALLINK__PROTOCOLREF:
				return basicSetPROTOCOLREF(null, msgs);
			case OdxXhtmlPackage.LOGICALLINK__FUNCTIONALGROUPREF:
				return basicSetFUNCTIONALGROUPREF(null, msgs);
			case OdxXhtmlPackage.LOGICALLINK__BASEVARIANTREF:
				return basicSetBASEVARIANTREF(null, msgs);
			case OdxXhtmlPackage.LOGICALLINK__ECUPROXYREFS:
				return basicSetECUPROXYREFS(null, msgs);
			case OdxXhtmlPackage.LOGICALLINK__LINKCOMPARAMREFS:
				return basicSetLINKCOMPARAMREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINK__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.LOGICALLINK__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.LOGICALLINK__DESC:
				return getDESC();
			case OdxXhtmlPackage.LOGICALLINK__GATEWAYLOGICALLINKREFS:
				return getGATEWAYLOGICALLINKREFS();
			case OdxXhtmlPackage.LOGICALLINK__PHYSICALVEHICLELINKREF:
				return getPHYSICALVEHICLELINKREF();
			case OdxXhtmlPackage.LOGICALLINK__PROTOCOLREF:
				return getPROTOCOLREF();
			case OdxXhtmlPackage.LOGICALLINK__FUNCTIONALGROUPREF:
				return getFUNCTIONALGROUPREF();
			case OdxXhtmlPackage.LOGICALLINK__BASEVARIANTREF:
				return getBASEVARIANTREF();
			case OdxXhtmlPackage.LOGICALLINK__ECUPROXYREFS:
				return getECUPROXYREFS();
			case OdxXhtmlPackage.LOGICALLINK__LINKCOMPARAMREFS:
				return getLINKCOMPARAMREFS();
			case OdxXhtmlPackage.LOGICALLINK__ID:
				return getID();
			case OdxXhtmlPackage.LOGICALLINK__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINK__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__GATEWAYLOGICALLINKREFS:
				setGATEWAYLOGICALLINKREFS((GATEWAYLOGICALLINKREFS)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__PHYSICALVEHICLELINKREF:
				setPHYSICALVEHICLELINKREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__PROTOCOLREF:
				setPROTOCOLREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__FUNCTIONALGROUPREF:
				setFUNCTIONALGROUPREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__BASEVARIANTREF:
				setBASEVARIANTREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__ECUPROXYREFS:
				setECUPROXYREFS((ECUPROXYREFS)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__LINKCOMPARAMREFS:
				setLINKCOMPARAMREFS((LINKCOMPARAMREFS)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.LOGICALLINK__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINK__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.LOGICALLINK__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.LOGICALLINK__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.LOGICALLINK__GATEWAYLOGICALLINKREFS:
				setGATEWAYLOGICALLINKREFS((GATEWAYLOGICALLINKREFS)null);
				return;
			case OdxXhtmlPackage.LOGICALLINK__PHYSICALVEHICLELINKREF:
				setPHYSICALVEHICLELINKREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.LOGICALLINK__PROTOCOLREF:
				setPROTOCOLREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.LOGICALLINK__FUNCTIONALGROUPREF:
				setFUNCTIONALGROUPREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.LOGICALLINK__BASEVARIANTREF:
				setBASEVARIANTREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.LOGICALLINK__ECUPROXYREFS:
				setECUPROXYREFS((ECUPROXYREFS)null);
				return;
			case OdxXhtmlPackage.LOGICALLINK__LINKCOMPARAMREFS:
				setLINKCOMPARAMREFS((LINKCOMPARAMREFS)null);
				return;
			case OdxXhtmlPackage.LOGICALLINK__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.LOGICALLINK__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LOGICALLINK__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.LOGICALLINK__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.LOGICALLINK__DESC:
				return dESC != null;
			case OdxXhtmlPackage.LOGICALLINK__GATEWAYLOGICALLINKREFS:
				return gATEWAYLOGICALLINKREFS != null;
			case OdxXhtmlPackage.LOGICALLINK__PHYSICALVEHICLELINKREF:
				return pHYSICALVEHICLELINKREF != null;
			case OdxXhtmlPackage.LOGICALLINK__PROTOCOLREF:
				return pROTOCOLREF != null;
			case OdxXhtmlPackage.LOGICALLINK__FUNCTIONALGROUPREF:
				return fUNCTIONALGROUPREF != null;
			case OdxXhtmlPackage.LOGICALLINK__BASEVARIANTREF:
				return bASEVARIANTREF != null;
			case OdxXhtmlPackage.LOGICALLINK__ECUPROXYREFS:
				return eCUPROXYREFS != null;
			case OdxXhtmlPackage.LOGICALLINK__LINKCOMPARAMREFS:
				return lINKCOMPARAMREFS != null;
			case OdxXhtmlPackage.LOGICALLINK__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.LOGICALLINK__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //LOGICALLINKImpl
