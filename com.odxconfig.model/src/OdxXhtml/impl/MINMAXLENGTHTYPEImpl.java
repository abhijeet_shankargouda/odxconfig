/**
 */
package OdxXhtml.impl;

import OdxXhtml.MINMAXLENGTHTYPE;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TERMINATION;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MINMAXLENGTHTYPE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MINMAXLENGTHTYPEImpl#getMAXLENGTH <em>MAXLENGTH</em>}</li>
 *   <li>{@link OdxXhtml.impl.MINMAXLENGTHTYPEImpl#getMINLENGTH <em>MINLENGTH</em>}</li>
 *   <li>{@link OdxXhtml.impl.MINMAXLENGTHTYPEImpl#getTERMINATION <em>TERMINATION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MINMAXLENGTHTYPEImpl extends DIAGCODEDTYPEImpl implements MINMAXLENGTHTYPE {
	/**
	 * The default value of the '{@link #getMAXLENGTH() <em>MAXLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMAXLENGTH()
	 * @generated
	 * @ordered
	 */
	protected static final long MAXLENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMAXLENGTH() <em>MAXLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMAXLENGTH()
	 * @generated
	 * @ordered
	 */
	protected long mAXLENGTH = MAXLENGTH_EDEFAULT;

	/**
	 * This is true if the MAXLENGTH attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mAXLENGTHESet;

	/**
	 * The default value of the '{@link #getMINLENGTH() <em>MINLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMINLENGTH()
	 * @generated
	 * @ordered
	 */
	protected static final long MINLENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getMINLENGTH() <em>MINLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMINLENGTH()
	 * @generated
	 * @ordered
	 */
	protected long mINLENGTH = MINLENGTH_EDEFAULT;

	/**
	 * This is true if the MINLENGTH attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mINLENGTHESet;

	/**
	 * The default value of the '{@link #getTERMINATION() <em>TERMINATION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTERMINATION()
	 * @generated
	 * @ordered
	 */
	protected static final TERMINATION TERMINATION_EDEFAULT = TERMINATION.ENDOFPDU;

	/**
	 * The cached value of the '{@link #getTERMINATION() <em>TERMINATION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTERMINATION()
	 * @generated
	 * @ordered
	 */
	protected TERMINATION tERMINATION = TERMINATION_EDEFAULT;

	/**
	 * This is true if the TERMINATION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tERMINATIONESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MINMAXLENGTHTYPEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMINMAXLENGTHTYPE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getMAXLENGTH() {
		return mAXLENGTH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMAXLENGTH(long newMAXLENGTH) {
		long oldMAXLENGTH = mAXLENGTH;
		mAXLENGTH = newMAXLENGTH;
		boolean oldMAXLENGTHESet = mAXLENGTHESet;
		mAXLENGTHESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MINMAXLENGTHTYPE__MAXLENGTH, oldMAXLENGTH, mAXLENGTH, !oldMAXLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetMAXLENGTH() {
		long oldMAXLENGTH = mAXLENGTH;
		boolean oldMAXLENGTHESet = mAXLENGTHESet;
		mAXLENGTH = MAXLENGTH_EDEFAULT;
		mAXLENGTHESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MINMAXLENGTHTYPE__MAXLENGTH, oldMAXLENGTH, MAXLENGTH_EDEFAULT, oldMAXLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetMAXLENGTH() {
		return mAXLENGTHESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getMINLENGTH() {
		return mINLENGTH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMINLENGTH(long newMINLENGTH) {
		long oldMINLENGTH = mINLENGTH;
		mINLENGTH = newMINLENGTH;
		boolean oldMINLENGTHESet = mINLENGTHESet;
		mINLENGTHESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MINMAXLENGTHTYPE__MINLENGTH, oldMINLENGTH, mINLENGTH, !oldMINLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetMINLENGTH() {
		long oldMINLENGTH = mINLENGTH;
		boolean oldMINLENGTHESet = mINLENGTHESet;
		mINLENGTH = MINLENGTH_EDEFAULT;
		mINLENGTHESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MINMAXLENGTHTYPE__MINLENGTH, oldMINLENGTH, MINLENGTH_EDEFAULT, oldMINLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetMINLENGTH() {
		return mINLENGTHESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TERMINATION getTERMINATION() {
		return tERMINATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTERMINATION(TERMINATION newTERMINATION) {
		TERMINATION oldTERMINATION = tERMINATION;
		tERMINATION = newTERMINATION == null ? TERMINATION_EDEFAULT : newTERMINATION;
		boolean oldTERMINATIONESet = tERMINATIONESet;
		tERMINATIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MINMAXLENGTHTYPE__TERMINATION, oldTERMINATION, tERMINATION, !oldTERMINATIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetTERMINATION() {
		TERMINATION oldTERMINATION = tERMINATION;
		boolean oldTERMINATIONESet = tERMINATIONESet;
		tERMINATION = TERMINATION_EDEFAULT;
		tERMINATIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MINMAXLENGTHTYPE__TERMINATION, oldTERMINATION, TERMINATION_EDEFAULT, oldTERMINATIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetTERMINATION() {
		return tERMINATIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__MAXLENGTH:
				return getMAXLENGTH();
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__MINLENGTH:
				return getMINLENGTH();
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__TERMINATION:
				return getTERMINATION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__MAXLENGTH:
				setMAXLENGTH((Long)newValue);
				return;
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__MINLENGTH:
				setMINLENGTH((Long)newValue);
				return;
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__TERMINATION:
				setTERMINATION((TERMINATION)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__MAXLENGTH:
				unsetMAXLENGTH();
				return;
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__MINLENGTH:
				unsetMINLENGTH();
				return;
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__TERMINATION:
				unsetTERMINATION();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__MAXLENGTH:
				return isSetMAXLENGTH();
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__MINLENGTH:
				return isSetMINLENGTH();
			case OdxXhtmlPackage.MINMAXLENGTHTYPE__TERMINATION:
				return isSetTERMINATION();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mAXLENGTH: ");
		if (mAXLENGTHESet) result.append(mAXLENGTH); else result.append("<unset>");
		result.append(", mINLENGTH: ");
		if (mINLENGTHESet) result.append(mINLENGTH); else result.append("<unset>");
		result.append(", tERMINATION: ");
		if (tERMINATIONESet) result.append(tERMINATION); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MINMAXLENGTHTYPEImpl
