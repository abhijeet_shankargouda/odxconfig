/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYREVISIONINFO;
import OdxXhtml.COMPANYREVISIONINFOS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYREVISIONINFOS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYREVISIONINFOSImpl#getCOMPANYREVISIONINFO <em>COMPANYREVISIONINFO</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYREVISIONINFOSImpl extends MinimalEObjectImpl.Container implements COMPANYREVISIONINFOS {
	/**
	 * The cached value of the '{@link #getCOMPANYREVISIONINFO() <em>COMPANYREVISIONINFO</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYREVISIONINFO()
	 * @generated
	 * @ordered
	 */
	protected EList<COMPANYREVISIONINFO> cOMPANYREVISIONINFO;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYREVISIONINFOSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYREVISIONINFOS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<COMPANYREVISIONINFO> getCOMPANYREVISIONINFO() {
		if (cOMPANYREVISIONINFO == null) {
			cOMPANYREVISIONINFO = new EObjectContainmentEList<COMPANYREVISIONINFO>(COMPANYREVISIONINFO.class, this, OdxXhtmlPackage.COMPANYREVISIONINFOS__COMPANYREVISIONINFO);
		}
		return cOMPANYREVISIONINFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS__COMPANYREVISIONINFO:
				return ((InternalEList<?>)getCOMPANYREVISIONINFO()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS__COMPANYREVISIONINFO:
				return getCOMPANYREVISIONINFO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS__COMPANYREVISIONINFO:
				getCOMPANYREVISIONINFO().clear();
				getCOMPANYREVISIONINFO().addAll((Collection<? extends COMPANYREVISIONINFO>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS__COMPANYREVISIONINFO:
				getCOMPANYREVISIONINFO().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS__COMPANYREVISIONINFO:
				return cOMPANYREVISIONINFO != null && !cOMPANYREVISIONINFO.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //COMPANYREVISIONINFOSImpl
