/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYDOCINFO;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SDGS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYDOCINFO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYDOCINFOImpl#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDOCINFOImpl#getTEAMMEMBERREF <em>TEAMMEMBERREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDOCINFOImpl#getDOCLABEL <em>DOCLABEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDOCINFOImpl#getSDGS <em>SDGS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYDOCINFOImpl extends MinimalEObjectImpl.Container implements COMPANYDOCINFO {
	/**
	 * The cached value of the '{@link #getCOMPANYDATAREF() <em>COMPANYDATAREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDATAREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK cOMPANYDATAREF;

	/**
	 * The cached value of the '{@link #getTEAMMEMBERREF() <em>TEAMMEMBERREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTEAMMEMBERREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK tEAMMEMBERREF;

	/**
	 * The default value of the '{@link #getDOCLABEL() <em>DOCLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCLABEL()
	 * @generated
	 * @ordered
	 */
	protected static final String DOCLABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDOCLABEL() <em>DOCLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCLABEL()
	 * @generated
	 * @ordered
	 */
	protected String dOCLABEL = DOCLABEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSDGS() <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGS()
	 * @generated
	 * @ordered
	 */
	protected SDGS sDGS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYDOCINFOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYDOCINFO();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getCOMPANYDATAREF() {
		return cOMPANYDATAREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYDATAREF(ODXLINK newCOMPANYDATAREF, NotificationChain msgs) {
		ODXLINK oldCOMPANYDATAREF = cOMPANYDATAREF;
		cOMPANYDATAREF = newCOMPANYDATAREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDOCINFO__COMPANYDATAREF, oldCOMPANYDATAREF, newCOMPANYDATAREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYDATAREF(ODXLINK newCOMPANYDATAREF) {
		if (newCOMPANYDATAREF != cOMPANYDATAREF) {
			NotificationChain msgs = null;
			if (cOMPANYDATAREF != null)
				msgs = ((InternalEObject)cOMPANYDATAREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDOCINFO__COMPANYDATAREF, null, msgs);
			if (newCOMPANYDATAREF != null)
				msgs = ((InternalEObject)newCOMPANYDATAREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDOCINFO__COMPANYDATAREF, null, msgs);
			msgs = basicSetCOMPANYDATAREF(newCOMPANYDATAREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDOCINFO__COMPANYDATAREF, newCOMPANYDATAREF, newCOMPANYDATAREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getTEAMMEMBERREF() {
		return tEAMMEMBERREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTEAMMEMBERREF(ODXLINK newTEAMMEMBERREF, NotificationChain msgs) {
		ODXLINK oldTEAMMEMBERREF = tEAMMEMBERREF;
		tEAMMEMBERREF = newTEAMMEMBERREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDOCINFO__TEAMMEMBERREF, oldTEAMMEMBERREF, newTEAMMEMBERREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTEAMMEMBERREF(ODXLINK newTEAMMEMBERREF) {
		if (newTEAMMEMBERREF != tEAMMEMBERREF) {
			NotificationChain msgs = null;
			if (tEAMMEMBERREF != null)
				msgs = ((InternalEObject)tEAMMEMBERREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDOCINFO__TEAMMEMBERREF, null, msgs);
			if (newTEAMMEMBERREF != null)
				msgs = ((InternalEObject)newTEAMMEMBERREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDOCINFO__TEAMMEMBERREF, null, msgs);
			msgs = basicSetTEAMMEMBERREF(newTEAMMEMBERREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDOCINFO__TEAMMEMBERREF, newTEAMMEMBERREF, newTEAMMEMBERREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDOCLABEL() {
		return dOCLABEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCLABEL(String newDOCLABEL) {
		String oldDOCLABEL = dOCLABEL;
		dOCLABEL = newDOCLABEL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDOCINFO__DOCLABEL, oldDOCLABEL, dOCLABEL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS getSDGS() {
		return sDGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGS(SDGS newSDGS, NotificationChain msgs) {
		SDGS oldSDGS = sDGS;
		sDGS = newSDGS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDOCINFO__SDGS, oldSDGS, newSDGS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGS(SDGS newSDGS) {
		if (newSDGS != sDGS) {
			NotificationChain msgs = null;
			if (sDGS != null)
				msgs = ((InternalEObject)sDGS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDOCINFO__SDGS, null, msgs);
			if (newSDGS != null)
				msgs = ((InternalEObject)newSDGS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDOCINFO__SDGS, null, msgs);
			msgs = basicSetSDGS(newSDGS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDOCINFO__SDGS, newSDGS, newSDGS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFO__COMPANYDATAREF:
				return basicSetCOMPANYDATAREF(null, msgs);
			case OdxXhtmlPackage.COMPANYDOCINFO__TEAMMEMBERREF:
				return basicSetTEAMMEMBERREF(null, msgs);
			case OdxXhtmlPackage.COMPANYDOCINFO__SDGS:
				return basicSetSDGS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFO__COMPANYDATAREF:
				return getCOMPANYDATAREF();
			case OdxXhtmlPackage.COMPANYDOCINFO__TEAMMEMBERREF:
				return getTEAMMEMBERREF();
			case OdxXhtmlPackage.COMPANYDOCINFO__DOCLABEL:
				return getDOCLABEL();
			case OdxXhtmlPackage.COMPANYDOCINFO__SDGS:
				return getSDGS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFO__COMPANYDATAREF:
				setCOMPANYDATAREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDOCINFO__TEAMMEMBERREF:
				setTEAMMEMBERREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDOCINFO__DOCLABEL:
				setDOCLABEL((String)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDOCINFO__SDGS:
				setSDGS((SDGS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFO__COMPANYDATAREF:
				setCOMPANYDATAREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.COMPANYDOCINFO__TEAMMEMBERREF:
				setTEAMMEMBERREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.COMPANYDOCINFO__DOCLABEL:
				setDOCLABEL(DOCLABEL_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPANYDOCINFO__SDGS:
				setSDGS((SDGS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDOCINFO__COMPANYDATAREF:
				return cOMPANYDATAREF != null;
			case OdxXhtmlPackage.COMPANYDOCINFO__TEAMMEMBERREF:
				return tEAMMEMBERREF != null;
			case OdxXhtmlPackage.COMPANYDOCINFO__DOCLABEL:
				return DOCLABEL_EDEFAULT == null ? dOCLABEL != null : !DOCLABEL_EDEFAULT.equals(dOCLABEL);
			case OdxXhtmlPackage.COMPANYDOCINFO__SDGS:
				return sDGS != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (dOCLABEL: ");
		result.append(dOCLABEL);
		result.append(')');
		return result.toString();
	}

} //COMPANYDOCINFOImpl
