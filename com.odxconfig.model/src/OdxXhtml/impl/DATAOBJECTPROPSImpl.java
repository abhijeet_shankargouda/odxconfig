/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATAOBJECTPROP;
import OdxXhtml.DATAOBJECTPROPS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DATAOBJECTPROPS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DATAOBJECTPROPSImpl#getDATAOBJECTPROP <em>DATAOBJECTPROP</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DATAOBJECTPROPSImpl extends MinimalEObjectImpl.Container implements DATAOBJECTPROPS {
	/**
	 * The cached value of the '{@link #getDATAOBJECTPROP() <em>DATAOBJECTPROP</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATAOBJECTPROP()
	 * @generated
	 * @ordered
	 */
	protected EList<DATAOBJECTPROP> dATAOBJECTPROP;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DATAOBJECTPROPSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDATAOBJECTPROPS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DATAOBJECTPROP> getDATAOBJECTPROP() {
		if (dATAOBJECTPROP == null) {
			dATAOBJECTPROP = new EObjectContainmentEList<DATAOBJECTPROP>(DATAOBJECTPROP.class, this, OdxXhtmlPackage.DATAOBJECTPROPS__DATAOBJECTPROP);
		}
		return dATAOBJECTPROP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROPS__DATAOBJECTPROP:
				return ((InternalEList<?>)getDATAOBJECTPROP()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROPS__DATAOBJECTPROP:
				return getDATAOBJECTPROP();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROPS__DATAOBJECTPROP:
				getDATAOBJECTPROP().clear();
				getDATAOBJECTPROP().addAll((Collection<? extends DATAOBJECTPROP>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROPS__DATAOBJECTPROP:
				getDATAOBJECTPROP().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATAOBJECTPROPS__DATAOBJECTPROP:
				return dATAOBJECTPROP != null && !dATAOBJECTPROP.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DATAOBJECTPROPSImpl
