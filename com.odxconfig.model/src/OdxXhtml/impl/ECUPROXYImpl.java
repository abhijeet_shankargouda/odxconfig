/**
 */
package OdxXhtml.impl;

import OdxXhtml.ECUPROXY;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ECUPROXY</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ECUPROXYImpl extends INFOCOMPONENTImpl implements ECUPROXY {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECUPROXYImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getECUPROXY();
	}

} //ECUPROXYImpl
