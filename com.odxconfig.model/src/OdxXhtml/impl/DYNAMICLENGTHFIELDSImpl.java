/**
 */
package OdxXhtml.impl;

import OdxXhtml.DYNAMICLENGTHFIELD;
import OdxXhtml.DYNAMICLENGTHFIELDS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DYNAMICLENGTHFIELDS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DYNAMICLENGTHFIELDSImpl#getDYNAMICLENGTHFIELD <em>DYNAMICLENGTHFIELD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DYNAMICLENGTHFIELDSImpl extends MinimalEObjectImpl.Container implements DYNAMICLENGTHFIELDS {
	/**
	 * The cached value of the '{@link #getDYNAMICLENGTHFIELD() <em>DYNAMICLENGTHFIELD</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNAMICLENGTHFIELD()
	 * @generated
	 * @ordered
	 */
	protected EList<DYNAMICLENGTHFIELD> dYNAMICLENGTHFIELD;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DYNAMICLENGTHFIELDSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDYNAMICLENGTHFIELDS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DYNAMICLENGTHFIELD> getDYNAMICLENGTHFIELD() {
		if (dYNAMICLENGTHFIELD == null) {
			dYNAMICLENGTHFIELD = new EObjectContainmentEList<DYNAMICLENGTHFIELD>(DYNAMICLENGTHFIELD.class, this, OdxXhtmlPackage.DYNAMICLENGTHFIELDS__DYNAMICLENGTHFIELD);
		}
		return dYNAMICLENGTHFIELD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELDS__DYNAMICLENGTHFIELD:
				return ((InternalEList<?>)getDYNAMICLENGTHFIELD()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELDS__DYNAMICLENGTHFIELD:
				return getDYNAMICLENGTHFIELD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELDS__DYNAMICLENGTHFIELD:
				getDYNAMICLENGTHFIELD().clear();
				getDYNAMICLENGTHFIELD().addAll((Collection<? extends DYNAMICLENGTHFIELD>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELDS__DYNAMICLENGTHFIELD:
				getDYNAMICLENGTHFIELD().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNAMICLENGTHFIELDS__DYNAMICLENGTHFIELD:
				return dYNAMICLENGTHFIELD != null && !dYNAMICLENGTHFIELD.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DYNAMICLENGTHFIELDSImpl
