/**
 */
package OdxXhtml.impl;

import OdxXhtml.GATEWAYLOGICALLINKREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GATEWAYLOGICALLINKREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.GATEWAYLOGICALLINKREFSImpl#getGATEWAYLOGICALLINKREF <em>GATEWAYLOGICALLINKREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GATEWAYLOGICALLINKREFSImpl extends MinimalEObjectImpl.Container implements GATEWAYLOGICALLINKREFS {
	/**
	 * The cached value of the '{@link #getGATEWAYLOGICALLINKREF() <em>GATEWAYLOGICALLINKREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGATEWAYLOGICALLINKREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> gATEWAYLOGICALLINKREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GATEWAYLOGICALLINKREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getGATEWAYLOGICALLINKREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getGATEWAYLOGICALLINKREF() {
		if (gATEWAYLOGICALLINKREF == null) {
			gATEWAYLOGICALLINKREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.GATEWAYLOGICALLINKREFS__GATEWAYLOGICALLINKREF);
		}
		return gATEWAYLOGICALLINKREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.GATEWAYLOGICALLINKREFS__GATEWAYLOGICALLINKREF:
				return ((InternalEList<?>)getGATEWAYLOGICALLINKREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.GATEWAYLOGICALLINKREFS__GATEWAYLOGICALLINKREF:
				return getGATEWAYLOGICALLINKREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.GATEWAYLOGICALLINKREFS__GATEWAYLOGICALLINKREF:
				getGATEWAYLOGICALLINKREF().clear();
				getGATEWAYLOGICALLINKREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.GATEWAYLOGICALLINKREFS__GATEWAYLOGICALLINKREF:
				getGATEWAYLOGICALLINKREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.GATEWAYLOGICALLINKREFS__GATEWAYLOGICALLINKREF:
				return gATEWAYLOGICALLINKREF != null && !gATEWAYLOGICALLINKREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //GATEWAYLOGICALLINKREFSImpl
