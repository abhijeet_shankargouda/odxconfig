/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMMRELATION;
import OdxXhtml.COMMRELATIONS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMMRELATIONS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMMRELATIONSImpl#getCOMMRELATION <em>COMMRELATION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMMRELATIONSImpl extends MinimalEObjectImpl.Container implements COMMRELATIONS {
	/**
	 * The cached value of the '{@link #getCOMMRELATION() <em>COMMRELATION</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMMRELATION()
	 * @generated
	 * @ordered
	 */
	protected EList<COMMRELATION> cOMMRELATION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMMRELATIONSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMMRELATIONS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<COMMRELATION> getCOMMRELATION() {
		if (cOMMRELATION == null) {
			cOMMRELATION = new EObjectContainmentEList<COMMRELATION>(COMMRELATION.class, this, OdxXhtmlPackage.COMMRELATIONS__COMMRELATION);
		}
		return cOMMRELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATIONS__COMMRELATION:
				return ((InternalEList<?>)getCOMMRELATION()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATIONS__COMMRELATION:
				return getCOMMRELATION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATIONS__COMMRELATION:
				getCOMMRELATION().clear();
				getCOMMRELATION().addAll((Collection<? extends COMMRELATION>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATIONS__COMMRELATION:
				getCOMMRELATION().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMMRELATIONS__COMMRELATION:
				return cOMMRELATION != null && !cOMMRELATION.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //COMMRELATIONSImpl
