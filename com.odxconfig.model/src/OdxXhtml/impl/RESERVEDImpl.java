/**
 */
package OdxXhtml.impl;

import OdxXhtml.DIAGCODEDTYPE;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.RESERVED;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RESERVED</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.RESERVEDImpl#getBITLENGTH <em>BITLENGTH</em>}</li>
 *   <li>{@link OdxXhtml.impl.RESERVEDImpl#getCODEDVALUE <em>CODEDVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.RESERVEDImpl#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RESERVEDImpl extends POSITIONABLEPARAMImpl implements RESERVED {
	/**
	 * The default value of the '{@link #getBITLENGTH() <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITLENGTH()
	 * @generated
	 * @ordered
	 */
	protected static final long BITLENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBITLENGTH() <em>BITLENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBITLENGTH()
	 * @generated
	 * @ordered
	 */
	protected long bITLENGTH = BITLENGTH_EDEFAULT;

	/**
	 * This is true if the BITLENGTH attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bITLENGTHESet;

	/**
	 * The default value of the '{@link #getCODEDVALUE() <em>CODEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCODEDVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String CODEDVALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCODEDVALUE() <em>CODEDVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCODEDVALUE()
	 * @generated
	 * @ordered
	 */
	protected String cODEDVALUE = CODEDVALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDIAGCODEDTYPE() <em>DIAGCODEDTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCODEDTYPE()
	 * @generated
	 * @ordered
	 */
	protected DIAGCODEDTYPE dIAGCODEDTYPE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RESERVEDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getRESERVED();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBITLENGTH() {
		return bITLENGTH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBITLENGTH(long newBITLENGTH) {
		long oldBITLENGTH = bITLENGTH;
		bITLENGTH = newBITLENGTH;
		boolean oldBITLENGTHESet = bITLENGTHESet;
		bITLENGTHESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.RESERVED__BITLENGTH, oldBITLENGTH, bITLENGTH, !oldBITLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBITLENGTH() {
		long oldBITLENGTH = bITLENGTH;
		boolean oldBITLENGTHESet = bITLENGTHESet;
		bITLENGTH = BITLENGTH_EDEFAULT;
		bITLENGTHESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.RESERVED__BITLENGTH, oldBITLENGTH, BITLENGTH_EDEFAULT, oldBITLENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBITLENGTH() {
		return bITLENGTHESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCODEDVALUE() {
		return cODEDVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCODEDVALUE(String newCODEDVALUE) {
		String oldCODEDVALUE = cODEDVALUE;
		cODEDVALUE = newCODEDVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.RESERVED__CODEDVALUE, oldCODEDVALUE, cODEDVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGCODEDTYPE getDIAGCODEDTYPE() {
		return dIAGCODEDTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCODEDTYPE(DIAGCODEDTYPE newDIAGCODEDTYPE, NotificationChain msgs) {
		DIAGCODEDTYPE oldDIAGCODEDTYPE = dIAGCODEDTYPE;
		dIAGCODEDTYPE = newDIAGCODEDTYPE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.RESERVED__DIAGCODEDTYPE, oldDIAGCODEDTYPE, newDIAGCODEDTYPE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCODEDTYPE(DIAGCODEDTYPE newDIAGCODEDTYPE) {
		if (newDIAGCODEDTYPE != dIAGCODEDTYPE) {
			NotificationChain msgs = null;
			if (dIAGCODEDTYPE != null)
				msgs = ((InternalEObject)dIAGCODEDTYPE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.RESERVED__DIAGCODEDTYPE, null, msgs);
			if (newDIAGCODEDTYPE != null)
				msgs = ((InternalEObject)newDIAGCODEDTYPE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.RESERVED__DIAGCODEDTYPE, null, msgs);
			msgs = basicSetDIAGCODEDTYPE(newDIAGCODEDTYPE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.RESERVED__DIAGCODEDTYPE, newDIAGCODEDTYPE, newDIAGCODEDTYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.RESERVED__DIAGCODEDTYPE:
				return basicSetDIAGCODEDTYPE(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.RESERVED__BITLENGTH:
				return getBITLENGTH();
			case OdxXhtmlPackage.RESERVED__CODEDVALUE:
				return getCODEDVALUE();
			case OdxXhtmlPackage.RESERVED__DIAGCODEDTYPE:
				return getDIAGCODEDTYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.RESERVED__BITLENGTH:
				setBITLENGTH((Long)newValue);
				return;
			case OdxXhtmlPackage.RESERVED__CODEDVALUE:
				setCODEDVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.RESERVED__DIAGCODEDTYPE:
				setDIAGCODEDTYPE((DIAGCODEDTYPE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RESERVED__BITLENGTH:
				unsetBITLENGTH();
				return;
			case OdxXhtmlPackage.RESERVED__CODEDVALUE:
				setCODEDVALUE(CODEDVALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.RESERVED__DIAGCODEDTYPE:
				setDIAGCODEDTYPE((DIAGCODEDTYPE)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.RESERVED__BITLENGTH:
				return isSetBITLENGTH();
			case OdxXhtmlPackage.RESERVED__CODEDVALUE:
				return CODEDVALUE_EDEFAULT == null ? cODEDVALUE != null : !CODEDVALUE_EDEFAULT.equals(cODEDVALUE);
			case OdxXhtmlPackage.RESERVED__DIAGCODEDTYPE:
				return dIAGCODEDTYPE != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (bITLENGTH: ");
		if (bITLENGTHESet) result.append(bITLENGTH); else result.append("<unset>");
		result.append(", cODEDVALUE: ");
		result.append(cODEDVALUE);
		result.append(')');
		return result.toString();
	}

} //RESERVEDImpl
