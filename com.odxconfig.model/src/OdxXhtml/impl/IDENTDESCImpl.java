/**
 */
package OdxXhtml.impl;

import OdxXhtml.IDENTDESC;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SNREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IDENTDESC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.IDENTDESCImpl#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.IDENTDESCImpl#getIDENTIFSNREF <em>IDENTIFSNREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.IDENTDESCImpl#getOUTPARAMIFSNREF <em>OUTPARAMIFSNREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IDENTDESCImpl extends MinimalEObjectImpl.Container implements IDENTDESC {
	/**
	 * The cached value of the '{@link #getDIAGCOMMSNREF() <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF dIAGCOMMSNREF;

	/**
	 * The cached value of the '{@link #getIDENTIFSNREF() <em>IDENTIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDENTIFSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF iDENTIFSNREF;

	/**
	 * The cached value of the '{@link #getOUTPARAMIFSNREF() <em>OUTPARAMIFSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOUTPARAMIFSNREF()
	 * @generated
	 * @ordered
	 */
	protected SNREF oUTPARAMIFSNREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IDENTDESCImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getIDENTDESC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getDIAGCOMMSNREF() {
		return dIAGCOMMSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF, NotificationChain msgs) {
		SNREF oldDIAGCOMMSNREF = dIAGCOMMSNREF;
		dIAGCOMMSNREF = newDIAGCOMMSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.IDENTDESC__DIAGCOMMSNREF, oldDIAGCOMMSNREF, newDIAGCOMMSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGCOMMSNREF(SNREF newDIAGCOMMSNREF) {
		if (newDIAGCOMMSNREF != dIAGCOMMSNREF) {
			NotificationChain msgs = null;
			if (dIAGCOMMSNREF != null)
				msgs = ((InternalEObject)dIAGCOMMSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.IDENTDESC__DIAGCOMMSNREF, null, msgs);
			if (newDIAGCOMMSNREF != null)
				msgs = ((InternalEObject)newDIAGCOMMSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.IDENTDESC__DIAGCOMMSNREF, null, msgs);
			msgs = basicSetDIAGCOMMSNREF(newDIAGCOMMSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.IDENTDESC__DIAGCOMMSNREF, newDIAGCOMMSNREF, newDIAGCOMMSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getIDENTIFSNREF() {
		return iDENTIFSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIDENTIFSNREF(SNREF newIDENTIFSNREF, NotificationChain msgs) {
		SNREF oldIDENTIFSNREF = iDENTIFSNREF;
		iDENTIFSNREF = newIDENTIFSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.IDENTDESC__IDENTIFSNREF, oldIDENTIFSNREF, newIDENTIFSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIDENTIFSNREF(SNREF newIDENTIFSNREF) {
		if (newIDENTIFSNREF != iDENTIFSNREF) {
			NotificationChain msgs = null;
			if (iDENTIFSNREF != null)
				msgs = ((InternalEObject)iDENTIFSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.IDENTDESC__IDENTIFSNREF, null, msgs);
			if (newIDENTIFSNREF != null)
				msgs = ((InternalEObject)newIDENTIFSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.IDENTDESC__IDENTIFSNREF, null, msgs);
			msgs = basicSetIDENTIFSNREF(newIDENTIFSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.IDENTDESC__IDENTIFSNREF, newIDENTIFSNREF, newIDENTIFSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SNREF getOUTPARAMIFSNREF() {
		return oUTPARAMIFSNREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOUTPARAMIFSNREF(SNREF newOUTPARAMIFSNREF, NotificationChain msgs) {
		SNREF oldOUTPARAMIFSNREF = oUTPARAMIFSNREF;
		oUTPARAMIFSNREF = newOUTPARAMIFSNREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.IDENTDESC__OUTPARAMIFSNREF, oldOUTPARAMIFSNREF, newOUTPARAMIFSNREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOUTPARAMIFSNREF(SNREF newOUTPARAMIFSNREF) {
		if (newOUTPARAMIFSNREF != oUTPARAMIFSNREF) {
			NotificationChain msgs = null;
			if (oUTPARAMIFSNREF != null)
				msgs = ((InternalEObject)oUTPARAMIFSNREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.IDENTDESC__OUTPARAMIFSNREF, null, msgs);
			if (newOUTPARAMIFSNREF != null)
				msgs = ((InternalEObject)newOUTPARAMIFSNREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.IDENTDESC__OUTPARAMIFSNREF, null, msgs);
			msgs = basicSetOUTPARAMIFSNREF(newOUTPARAMIFSNREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.IDENTDESC__OUTPARAMIFSNREF, newOUTPARAMIFSNREF, newOUTPARAMIFSNREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESC__DIAGCOMMSNREF:
				return basicSetDIAGCOMMSNREF(null, msgs);
			case OdxXhtmlPackage.IDENTDESC__IDENTIFSNREF:
				return basicSetIDENTIFSNREF(null, msgs);
			case OdxXhtmlPackage.IDENTDESC__OUTPARAMIFSNREF:
				return basicSetOUTPARAMIFSNREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESC__DIAGCOMMSNREF:
				return getDIAGCOMMSNREF();
			case OdxXhtmlPackage.IDENTDESC__IDENTIFSNREF:
				return getIDENTIFSNREF();
			case OdxXhtmlPackage.IDENTDESC__OUTPARAMIFSNREF:
				return getOUTPARAMIFSNREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESC__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.IDENTDESC__IDENTIFSNREF:
				setIDENTIFSNREF((SNREF)newValue);
				return;
			case OdxXhtmlPackage.IDENTDESC__OUTPARAMIFSNREF:
				setOUTPARAMIFSNREF((SNREF)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESC__DIAGCOMMSNREF:
				setDIAGCOMMSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.IDENTDESC__IDENTIFSNREF:
				setIDENTIFSNREF((SNREF)null);
				return;
			case OdxXhtmlPackage.IDENTDESC__OUTPARAMIFSNREF:
				setOUTPARAMIFSNREF((SNREF)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.IDENTDESC__DIAGCOMMSNREF:
				return dIAGCOMMSNREF != null;
			case OdxXhtmlPackage.IDENTDESC__IDENTIFSNREF:
				return iDENTIFSNREF != null;
			case OdxXhtmlPackage.IDENTDESC__OUTPARAMIFSNREF:
				return oUTPARAMIFSNREF != null;
		}
		return super.eIsSet(featureID);
	}

} //IDENTDESCImpl
