/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYSPECIFICINFO;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.RELATEDDOCS;
import OdxXhtml.SDGS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYSPECIFICINFO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYSPECIFICINFOImpl#getRELATEDDOCS <em>RELATEDDOCS</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYSPECIFICINFOImpl#getSDGS <em>SDGS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYSPECIFICINFOImpl extends MinimalEObjectImpl.Container implements COMPANYSPECIFICINFO {
	/**
	 * The cached value of the '{@link #getRELATEDDOCS() <em>RELATEDDOCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRELATEDDOCS()
	 * @generated
	 * @ordered
	 */
	protected RELATEDDOCS rELATEDDOCS;

	/**
	 * The cached value of the '{@link #getSDGS() <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGS()
	 * @generated
	 * @ordered
	 */
	protected SDGS sDGS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYSPECIFICINFOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYSPECIFICINFO();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RELATEDDOCS getRELATEDDOCS() {
		return rELATEDDOCS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRELATEDDOCS(RELATEDDOCS newRELATEDDOCS, NotificationChain msgs) {
		RELATEDDOCS oldRELATEDDOCS = rELATEDDOCS;
		rELATEDDOCS = newRELATEDDOCS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYSPECIFICINFO__RELATEDDOCS, oldRELATEDDOCS, newRELATEDDOCS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRELATEDDOCS(RELATEDDOCS newRELATEDDOCS) {
		if (newRELATEDDOCS != rELATEDDOCS) {
			NotificationChain msgs = null;
			if (rELATEDDOCS != null)
				msgs = ((InternalEObject)rELATEDDOCS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYSPECIFICINFO__RELATEDDOCS, null, msgs);
			if (newRELATEDDOCS != null)
				msgs = ((InternalEObject)newRELATEDDOCS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYSPECIFICINFO__RELATEDDOCS, null, msgs);
			msgs = basicSetRELATEDDOCS(newRELATEDDOCS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYSPECIFICINFO__RELATEDDOCS, newRELATEDDOCS, newRELATEDDOCS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS getSDGS() {
		return sDGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGS(SDGS newSDGS, NotificationChain msgs) {
		SDGS oldSDGS = sDGS;
		sDGS = newSDGS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYSPECIFICINFO__SDGS, oldSDGS, newSDGS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGS(SDGS newSDGS) {
		if (newSDGS != sDGS) {
			NotificationChain msgs = null;
			if (sDGS != null)
				msgs = ((InternalEObject)sDGS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYSPECIFICINFO__SDGS, null, msgs);
			if (newSDGS != null)
				msgs = ((InternalEObject)newSDGS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYSPECIFICINFO__SDGS, null, msgs);
			msgs = basicSetSDGS(newSDGS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYSPECIFICINFO__SDGS, newSDGS, newSDGS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__RELATEDDOCS:
				return basicSetRELATEDDOCS(null, msgs);
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__SDGS:
				return basicSetSDGS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__RELATEDDOCS:
				return getRELATEDDOCS();
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__SDGS:
				return getSDGS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__RELATEDDOCS:
				setRELATEDDOCS((RELATEDDOCS)newValue);
				return;
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__SDGS:
				setSDGS((SDGS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__RELATEDDOCS:
				setRELATEDDOCS((RELATEDDOCS)null);
				return;
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__SDGS:
				setSDGS((SDGS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__RELATEDDOCS:
				return rELATEDDOCS != null;
			case OdxXhtmlPackage.COMPANYSPECIFICINFO__SDGS:
				return sDGS != null;
		}
		return super.eIsSet(featureID);
	}

} //COMPANYSPECIFICINFOImpl
