/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SWVARIABLE;
import OdxXhtml.SWVARIABLES;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SWVARIABLES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.SWVARIABLESImpl#getSWVARIABLE <em>SWVARIABLE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SWVARIABLESImpl extends MinimalEObjectImpl.Container implements SWVARIABLES {
	/**
	 * The cached value of the '{@link #getSWVARIABLE() <em>SWVARIABLE</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSWVARIABLE()
	 * @generated
	 * @ordered
	 */
	protected EList<SWVARIABLE> sWVARIABLE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SWVARIABLESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getSWVARIABLES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SWVARIABLE> getSWVARIABLE() {
		if (sWVARIABLE == null) {
			sWVARIABLE = new EObjectContainmentEList<SWVARIABLE>(SWVARIABLE.class, this, OdxXhtmlPackage.SWVARIABLES__SWVARIABLE);
		}
		return sWVARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.SWVARIABLES__SWVARIABLE:
				return ((InternalEList<?>)getSWVARIABLE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.SWVARIABLES__SWVARIABLE:
				return getSWVARIABLE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.SWVARIABLES__SWVARIABLE:
				getSWVARIABLE().clear();
				getSWVARIABLE().addAll((Collection<? extends SWVARIABLE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SWVARIABLES__SWVARIABLE:
				getSWVARIABLE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.SWVARIABLES__SWVARIABLE:
				return sWVARIABLE != null && !sWVARIABLE.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SWVARIABLESImpl
