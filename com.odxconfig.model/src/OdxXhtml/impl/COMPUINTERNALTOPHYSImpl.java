/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPUDEFAULTVALUE;
import OdxXhtml.COMPUINTERNALTOPHYS;
import OdxXhtml.COMPUSCALES;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROGCODE;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPUINTERNALTOPHYS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPUINTERNALTOPHYSImpl#getCOMPUSCALES <em>COMPUSCALES</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUINTERNALTOPHYSImpl#getPROGCODE <em>PROGCODE</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPUINTERNALTOPHYSImpl#getCOMPUDEFAULTVALUE <em>COMPUDEFAULTVALUE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPUINTERNALTOPHYSImpl extends MinimalEObjectImpl.Container implements COMPUINTERNALTOPHYS {
	/**
	 * The cached value of the '{@link #getCOMPUSCALES() <em>COMPUSCALES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUSCALES()
	 * @generated
	 * @ordered
	 */
	protected COMPUSCALES cOMPUSCALES;

	/**
	 * The cached value of the '{@link #getPROGCODE() <em>PROGCODE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGCODE()
	 * @generated
	 * @ordered
	 */
	protected PROGCODE pROGCODE;

	/**
	 * The cached value of the '{@link #getCOMPUDEFAULTVALUE() <em>COMPUDEFAULTVALUE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPUDEFAULTVALUE()
	 * @generated
	 * @ordered
	 */
	protected COMPUDEFAULTVALUE cOMPUDEFAULTVALUE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPUINTERNALTOPHYSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPUINTERNALTOPHYS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUSCALES getCOMPUSCALES() {
		return cOMPUSCALES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUSCALES(COMPUSCALES newCOMPUSCALES, NotificationChain msgs) {
		COMPUSCALES oldCOMPUSCALES = cOMPUSCALES;
		cOMPUSCALES = newCOMPUSCALES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUSCALES, oldCOMPUSCALES, newCOMPUSCALES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUSCALES(COMPUSCALES newCOMPUSCALES) {
		if (newCOMPUSCALES != cOMPUSCALES) {
			NotificationChain msgs = null;
			if (cOMPUSCALES != null)
				msgs = ((InternalEObject)cOMPUSCALES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUSCALES, null, msgs);
			if (newCOMPUSCALES != null)
				msgs = ((InternalEObject)newCOMPUSCALES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUSCALES, null, msgs);
			msgs = basicSetCOMPUSCALES(newCOMPUSCALES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUSCALES, newCOMPUSCALES, newCOMPUSCALES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROGCODE getPROGCODE() {
		return pROGCODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROGCODE(PROGCODE newPROGCODE, NotificationChain msgs) {
		PROGCODE oldPROGCODE = pROGCODE;
		pROGCODE = newPROGCODE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUINTERNALTOPHYS__PROGCODE, oldPROGCODE, newPROGCODE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROGCODE(PROGCODE newPROGCODE) {
		if (newPROGCODE != pROGCODE) {
			NotificationChain msgs = null;
			if (pROGCODE != null)
				msgs = ((InternalEObject)pROGCODE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUINTERNALTOPHYS__PROGCODE, null, msgs);
			if (newPROGCODE != null)
				msgs = ((InternalEObject)newPROGCODE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUINTERNALTOPHYS__PROGCODE, null, msgs);
			msgs = basicSetPROGCODE(newPROGCODE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUINTERNALTOPHYS__PROGCODE, newPROGCODE, newPROGCODE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPUDEFAULTVALUE getCOMPUDEFAULTVALUE() {
		return cOMPUDEFAULTVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPUDEFAULTVALUE(COMPUDEFAULTVALUE newCOMPUDEFAULTVALUE, NotificationChain msgs) {
		COMPUDEFAULTVALUE oldCOMPUDEFAULTVALUE = cOMPUDEFAULTVALUE;
		cOMPUDEFAULTVALUE = newCOMPUDEFAULTVALUE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUDEFAULTVALUE, oldCOMPUDEFAULTVALUE, newCOMPUDEFAULTVALUE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPUDEFAULTVALUE(COMPUDEFAULTVALUE newCOMPUDEFAULTVALUE) {
		if (newCOMPUDEFAULTVALUE != cOMPUDEFAULTVALUE) {
			NotificationChain msgs = null;
			if (cOMPUDEFAULTVALUE != null)
				msgs = ((InternalEObject)cOMPUDEFAULTVALUE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUDEFAULTVALUE, null, msgs);
			if (newCOMPUDEFAULTVALUE != null)
				msgs = ((InternalEObject)newCOMPUDEFAULTVALUE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUDEFAULTVALUE, null, msgs);
			msgs = basicSetCOMPUDEFAULTVALUE(newCOMPUDEFAULTVALUE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUDEFAULTVALUE, newCOMPUDEFAULTVALUE, newCOMPUDEFAULTVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUSCALES:
				return basicSetCOMPUSCALES(null, msgs);
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__PROGCODE:
				return basicSetPROGCODE(null, msgs);
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUDEFAULTVALUE:
				return basicSetCOMPUDEFAULTVALUE(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUSCALES:
				return getCOMPUSCALES();
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__PROGCODE:
				return getPROGCODE();
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUDEFAULTVALUE:
				return getCOMPUDEFAULTVALUE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUSCALES:
				setCOMPUSCALES((COMPUSCALES)newValue);
				return;
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__PROGCODE:
				setPROGCODE((PROGCODE)newValue);
				return;
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUDEFAULTVALUE:
				setCOMPUDEFAULTVALUE((COMPUDEFAULTVALUE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUSCALES:
				setCOMPUSCALES((COMPUSCALES)null);
				return;
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__PROGCODE:
				setPROGCODE((PROGCODE)null);
				return;
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUDEFAULTVALUE:
				setCOMPUDEFAULTVALUE((COMPUDEFAULTVALUE)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUSCALES:
				return cOMPUSCALES != null;
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__PROGCODE:
				return pROGCODE != null;
			case OdxXhtmlPackage.COMPUINTERNALTOPHYS__COMPUDEFAULTVALUE:
				return cOMPUDEFAULTVALUE != null;
		}
		return super.eIsSet(featureID);
	}

} //COMPUINTERNALTOPHYSImpl
