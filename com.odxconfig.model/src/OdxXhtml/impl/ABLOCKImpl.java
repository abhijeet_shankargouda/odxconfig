/**
 */
package OdxXhtml.impl;

import OdxXhtml.ABLOCK;
import OdxXhtml.ADMINDATA1;
import OdxXhtml.DESCRIPTION1;
import OdxXhtml.FILESType;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TEXT1;
import OdxXhtml.UPDSTATUS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ABLOCK</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ABLOCKImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.ABLOCKImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.ABLOCKImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.ABLOCKImpl#getCATEGORY <em>CATEGORY</em>}</li>
 *   <li>{@link OdxXhtml.impl.ABLOCKImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.ABLOCKImpl#getFILES <em>FILES</em>}</li>
 *   <li>{@link OdxXhtml.impl.ABLOCKImpl#getUPD <em>UPD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ABLOCKImpl extends MinimalEObjectImpl.Container implements ABLOCK {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT1 lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION1 dESC;

	/**
	 * The default value of the '{@link #getCATEGORY() <em>CATEGORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCATEGORY()
	 * @generated
	 * @ordered
	 */
	protected static final String CATEGORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCATEGORY() <em>CATEGORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCATEGORY()
	 * @generated
	 * @ordered
	 */
	protected String cATEGORY = CATEGORY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA1 aDMINDATA;

	/**
	 * The cached value of the '{@link #getFILES() <em>FILES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILES()
	 * @generated
	 * @ordered
	 */
	protected FILESType fILES;

	/**
	 * The default value of the '{@link #getUPD() <em>UPD</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUPD()
	 * @generated
	 * @ordered
	 */
	protected static final UPDSTATUS UPD_EDEFAULT = UPDSTATUS.UNDEFINED;

	/**
	 * The cached value of the '{@link #getUPD() <em>UPD</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUPD()
	 * @generated
	 * @ordered
	 */
	protected UPDSTATUS uPD = UPD_EDEFAULT;

	/**
	 * This is true if the UPD attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean uPDESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ABLOCKImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getABLOCK();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT1 getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT1 newLONGNAME, NotificationChain msgs) {
		TEXT1 oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT1 newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ABLOCK__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ABLOCK__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION1 getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION1 newDESC, NotificationChain msgs) {
		DESCRIPTION1 oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION1 newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ABLOCK__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ABLOCK__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCATEGORY() {
		return cATEGORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCATEGORY(String newCATEGORY) {
		String oldCATEGORY = cATEGORY;
		cATEGORY = newCATEGORY;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__CATEGORY, oldCATEGORY, cATEGORY));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA1 getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA1 newADMINDATA, NotificationChain msgs) {
		ADMINDATA1 oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA1 newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ABLOCK__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ABLOCK__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FILESType getFILES() {
		return fILES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFILES(FILESType newFILES, NotificationChain msgs) {
		FILESType oldFILES = fILES;
		fILES = newFILES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__FILES, oldFILES, newFILES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFILES(FILESType newFILES) {
		if (newFILES != fILES) {
			NotificationChain msgs = null;
			if (fILES != null)
				msgs = ((InternalEObject)fILES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ABLOCK__FILES, null, msgs);
			if (newFILES != null)
				msgs = ((InternalEObject)newFILES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.ABLOCK__FILES, null, msgs);
			msgs = basicSetFILES(newFILES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__FILES, newFILES, newFILES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UPDSTATUS getUPD() {
		return uPD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUPD(UPDSTATUS newUPD) {
		UPDSTATUS oldUPD = uPD;
		uPD = newUPD == null ? UPD_EDEFAULT : newUPD;
		boolean oldUPDESet = uPDESet;
		uPDESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ABLOCK__UPD, oldUPD, uPD, !oldUPDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetUPD() {
		UPDSTATUS oldUPD = uPD;
		boolean oldUPDESet = uPDESet;
		uPD = UPD_EDEFAULT;
		uPDESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.ABLOCK__UPD, oldUPD, UPD_EDEFAULT, oldUPDESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetUPD() {
		return uPDESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCK__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.ABLOCK__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.ABLOCK__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.ABLOCK__FILES:
				return basicSetFILES(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCK__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.ABLOCK__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.ABLOCK__DESC:
				return getDESC();
			case OdxXhtmlPackage.ABLOCK__CATEGORY:
				return getCATEGORY();
			case OdxXhtmlPackage.ABLOCK__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.ABLOCK__FILES:
				return getFILES();
			case OdxXhtmlPackage.ABLOCK__UPD:
				return getUPD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCK__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.ABLOCK__LONGNAME:
				setLONGNAME((TEXT1)newValue);
				return;
			case OdxXhtmlPackage.ABLOCK__DESC:
				setDESC((DESCRIPTION1)newValue);
				return;
			case OdxXhtmlPackage.ABLOCK__CATEGORY:
				setCATEGORY((String)newValue);
				return;
			case OdxXhtmlPackage.ABLOCK__ADMINDATA:
				setADMINDATA((ADMINDATA1)newValue);
				return;
			case OdxXhtmlPackage.ABLOCK__FILES:
				setFILES((FILESType)newValue);
				return;
			case OdxXhtmlPackage.ABLOCK__UPD:
				setUPD((UPDSTATUS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCK__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.ABLOCK__LONGNAME:
				setLONGNAME((TEXT1)null);
				return;
			case OdxXhtmlPackage.ABLOCK__DESC:
				setDESC((DESCRIPTION1)null);
				return;
			case OdxXhtmlPackage.ABLOCK__CATEGORY:
				setCATEGORY(CATEGORY_EDEFAULT);
				return;
			case OdxXhtmlPackage.ABLOCK__ADMINDATA:
				setADMINDATA((ADMINDATA1)null);
				return;
			case OdxXhtmlPackage.ABLOCK__FILES:
				setFILES((FILESType)null);
				return;
			case OdxXhtmlPackage.ABLOCK__UPD:
				unsetUPD();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ABLOCK__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.ABLOCK__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.ABLOCK__DESC:
				return dESC != null;
			case OdxXhtmlPackage.ABLOCK__CATEGORY:
				return CATEGORY_EDEFAULT == null ? cATEGORY != null : !CATEGORY_EDEFAULT.equals(cATEGORY);
			case OdxXhtmlPackage.ABLOCK__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.ABLOCK__FILES:
				return fILES != null;
			case OdxXhtmlPackage.ABLOCK__UPD:
				return isSetUPD();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", cATEGORY: ");
		result.append(cATEGORY);
		result.append(", uPD: ");
		if (uPDESet) result.append(uPD); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //ABLOCKImpl
