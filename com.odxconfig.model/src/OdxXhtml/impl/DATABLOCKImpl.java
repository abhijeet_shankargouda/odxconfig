/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATABLOCK;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.FILTERS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OWNIDENTS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SDGS;
import OdxXhtml.SECURITYS;
import OdxXhtml.SEGMENTS;
import OdxXhtml.TARGETADDROFFSET;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DATABLOCK</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getFLASHDATAREF <em>FLASHDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getFILTERS <em>FILTERS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getSEGMENTS <em>SEGMENTS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getTARGETADDROFFSET <em>TARGETADDROFFSET</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getOWNIDENTS <em>OWNIDENTS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getSECURITYS <em>SECURITYS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.impl.DATABLOCKImpl#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DATABLOCKImpl extends MinimalEObjectImpl.Container implements DATABLOCK {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getFLASHDATAREF() <em>FLASHDATAREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFLASHDATAREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK fLASHDATAREF;

	/**
	 * The cached value of the '{@link #getFILTERS() <em>FILTERS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILTERS()
	 * @generated
	 * @ordered
	 */
	protected FILTERS fILTERS;

	/**
	 * The cached value of the '{@link #getSEGMENTS() <em>SEGMENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSEGMENTS()
	 * @generated
	 * @ordered
	 */
	protected SEGMENTS sEGMENTS;

	/**
	 * The cached value of the '{@link #getTARGETADDROFFSET() <em>TARGETADDROFFSET</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTARGETADDROFFSET()
	 * @generated
	 * @ordered
	 */
	protected TARGETADDROFFSET tARGETADDROFFSET;

	/**
	 * The cached value of the '{@link #getOWNIDENTS() <em>OWNIDENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOWNIDENTS()
	 * @generated
	 * @ordered
	 */
	protected OWNIDENTS oWNIDENTS;

	/**
	 * The cached value of the '{@link #getSECURITYS() <em>SECURITYS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSECURITYS()
	 * @generated
	 * @ordered
	 */
	protected SECURITYS sECURITYS;

	/**
	 * The cached value of the '{@link #getSDGS() <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGS()
	 * @generated
	 * @ordered
	 */
	protected SDGS sDGS;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * The default value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected String tYPE = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DATABLOCKImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDATABLOCK();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getFLASHDATAREF() {
		return fLASHDATAREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFLASHDATAREF(ODXLINK newFLASHDATAREF, NotificationChain msgs) {
		ODXLINK oldFLASHDATAREF = fLASHDATAREF;
		fLASHDATAREF = newFLASHDATAREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__FLASHDATAREF, oldFLASHDATAREF, newFLASHDATAREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFLASHDATAREF(ODXLINK newFLASHDATAREF) {
		if (newFLASHDATAREF != fLASHDATAREF) {
			NotificationChain msgs = null;
			if (fLASHDATAREF != null)
				msgs = ((InternalEObject)fLASHDATAREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__FLASHDATAREF, null, msgs);
			if (newFLASHDATAREF != null)
				msgs = ((InternalEObject)newFLASHDATAREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__FLASHDATAREF, null, msgs);
			msgs = basicSetFLASHDATAREF(newFLASHDATAREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__FLASHDATAREF, newFLASHDATAREF, newFLASHDATAREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FILTERS getFILTERS() {
		return fILTERS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFILTERS(FILTERS newFILTERS, NotificationChain msgs) {
		FILTERS oldFILTERS = fILTERS;
		fILTERS = newFILTERS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__FILTERS, oldFILTERS, newFILTERS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFILTERS(FILTERS newFILTERS) {
		if (newFILTERS != fILTERS) {
			NotificationChain msgs = null;
			if (fILTERS != null)
				msgs = ((InternalEObject)fILTERS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__FILTERS, null, msgs);
			if (newFILTERS != null)
				msgs = ((InternalEObject)newFILTERS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__FILTERS, null, msgs);
			msgs = basicSetFILTERS(newFILTERS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__FILTERS, newFILTERS, newFILTERS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SEGMENTS getSEGMENTS() {
		return sEGMENTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSEGMENTS(SEGMENTS newSEGMENTS, NotificationChain msgs) {
		SEGMENTS oldSEGMENTS = sEGMENTS;
		sEGMENTS = newSEGMENTS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__SEGMENTS, oldSEGMENTS, newSEGMENTS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSEGMENTS(SEGMENTS newSEGMENTS) {
		if (newSEGMENTS != sEGMENTS) {
			NotificationChain msgs = null;
			if (sEGMENTS != null)
				msgs = ((InternalEObject)sEGMENTS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__SEGMENTS, null, msgs);
			if (newSEGMENTS != null)
				msgs = ((InternalEObject)newSEGMENTS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__SEGMENTS, null, msgs);
			msgs = basicSetSEGMENTS(newSEGMENTS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__SEGMENTS, newSEGMENTS, newSEGMENTS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TARGETADDROFFSET getTARGETADDROFFSET() {
		return tARGETADDROFFSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTARGETADDROFFSET(TARGETADDROFFSET newTARGETADDROFFSET, NotificationChain msgs) {
		TARGETADDROFFSET oldTARGETADDROFFSET = tARGETADDROFFSET;
		tARGETADDROFFSET = newTARGETADDROFFSET;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__TARGETADDROFFSET, oldTARGETADDROFFSET, newTARGETADDROFFSET);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTARGETADDROFFSET(TARGETADDROFFSET newTARGETADDROFFSET) {
		if (newTARGETADDROFFSET != tARGETADDROFFSET) {
			NotificationChain msgs = null;
			if (tARGETADDROFFSET != null)
				msgs = ((InternalEObject)tARGETADDROFFSET).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__TARGETADDROFFSET, null, msgs);
			if (newTARGETADDROFFSET != null)
				msgs = ((InternalEObject)newTARGETADDROFFSET).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__TARGETADDROFFSET, null, msgs);
			msgs = basicSetTARGETADDROFFSET(newTARGETADDROFFSET, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__TARGETADDROFFSET, newTARGETADDROFFSET, newTARGETADDROFFSET));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OWNIDENTS getOWNIDENTS() {
		return oWNIDENTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOWNIDENTS(OWNIDENTS newOWNIDENTS, NotificationChain msgs) {
		OWNIDENTS oldOWNIDENTS = oWNIDENTS;
		oWNIDENTS = newOWNIDENTS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__OWNIDENTS, oldOWNIDENTS, newOWNIDENTS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOWNIDENTS(OWNIDENTS newOWNIDENTS) {
		if (newOWNIDENTS != oWNIDENTS) {
			NotificationChain msgs = null;
			if (oWNIDENTS != null)
				msgs = ((InternalEObject)oWNIDENTS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__OWNIDENTS, null, msgs);
			if (newOWNIDENTS != null)
				msgs = ((InternalEObject)newOWNIDENTS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__OWNIDENTS, null, msgs);
			msgs = basicSetOWNIDENTS(newOWNIDENTS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__OWNIDENTS, newOWNIDENTS, newOWNIDENTS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SECURITYS getSECURITYS() {
		return sECURITYS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSECURITYS(SECURITYS newSECURITYS, NotificationChain msgs) {
		SECURITYS oldSECURITYS = sECURITYS;
		sECURITYS = newSECURITYS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__SECURITYS, oldSECURITYS, newSECURITYS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSECURITYS(SECURITYS newSECURITYS) {
		if (newSECURITYS != sECURITYS) {
			NotificationChain msgs = null;
			if (sECURITYS != null)
				msgs = ((InternalEObject)sECURITYS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__SECURITYS, null, msgs);
			if (newSECURITYS != null)
				msgs = ((InternalEObject)newSECURITYS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__SECURITYS, null, msgs);
			msgs = basicSetSECURITYS(newSECURITYS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__SECURITYS, newSECURITYS, newSECURITYS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS getSDGS() {
		return sDGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGS(SDGS newSDGS, NotificationChain msgs) {
		SDGS oldSDGS = sDGS;
		sDGS = newSDGS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__SDGS, oldSDGS, newSDGS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGS(SDGS newSDGS) {
		if (newSDGS != sDGS) {
			NotificationChain msgs = null;
			if (sDGS != null)
				msgs = ((InternalEObject)sDGS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__SDGS, null, msgs);
			if (newSDGS != null)
				msgs = ((InternalEObject)newSDGS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DATABLOCK__SDGS, null, msgs);
			msgs = basicSetSDGS(newSDGS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__SDGS, newSDGS, newSDGS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTYPE() {
		return tYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTYPE(String newTYPE) {
		String oldTYPE = tYPE;
		tYPE = newTYPE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DATABLOCK__TYPE, oldTYPE, tYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCK__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.DATABLOCK__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.DATABLOCK__FLASHDATAREF:
				return basicSetFLASHDATAREF(null, msgs);
			case OdxXhtmlPackage.DATABLOCK__FILTERS:
				return basicSetFILTERS(null, msgs);
			case OdxXhtmlPackage.DATABLOCK__SEGMENTS:
				return basicSetSEGMENTS(null, msgs);
			case OdxXhtmlPackage.DATABLOCK__TARGETADDROFFSET:
				return basicSetTARGETADDROFFSET(null, msgs);
			case OdxXhtmlPackage.DATABLOCK__OWNIDENTS:
				return basicSetOWNIDENTS(null, msgs);
			case OdxXhtmlPackage.DATABLOCK__SECURITYS:
				return basicSetSECURITYS(null, msgs);
			case OdxXhtmlPackage.DATABLOCK__SDGS:
				return basicSetSDGS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCK__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.DATABLOCK__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.DATABLOCK__DESC:
				return getDESC();
			case OdxXhtmlPackage.DATABLOCK__FLASHDATAREF:
				return getFLASHDATAREF();
			case OdxXhtmlPackage.DATABLOCK__FILTERS:
				return getFILTERS();
			case OdxXhtmlPackage.DATABLOCK__SEGMENTS:
				return getSEGMENTS();
			case OdxXhtmlPackage.DATABLOCK__TARGETADDROFFSET:
				return getTARGETADDROFFSET();
			case OdxXhtmlPackage.DATABLOCK__OWNIDENTS:
				return getOWNIDENTS();
			case OdxXhtmlPackage.DATABLOCK__SECURITYS:
				return getSECURITYS();
			case OdxXhtmlPackage.DATABLOCK__SDGS:
				return getSDGS();
			case OdxXhtmlPackage.DATABLOCK__ID:
				return getID();
			case OdxXhtmlPackage.DATABLOCK__OID:
				return getOID();
			case OdxXhtmlPackage.DATABLOCK__TYPE:
				return getTYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCK__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__FLASHDATAREF:
				setFLASHDATAREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__FILTERS:
				setFILTERS((FILTERS)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__SEGMENTS:
				setSEGMENTS((SEGMENTS)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__TARGETADDROFFSET:
				setTARGETADDROFFSET((TARGETADDROFFSET)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__OWNIDENTS:
				setOWNIDENTS((OWNIDENTS)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__SECURITYS:
				setSECURITYS((SECURITYS)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__SDGS:
				setSDGS((SDGS)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__OID:
				setOID((String)newValue);
				return;
			case OdxXhtmlPackage.DATABLOCK__TYPE:
				setTYPE((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCK__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.DATABLOCK__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.DATABLOCK__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.DATABLOCK__FLASHDATAREF:
				setFLASHDATAREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.DATABLOCK__FILTERS:
				setFILTERS((FILTERS)null);
				return;
			case OdxXhtmlPackage.DATABLOCK__SEGMENTS:
				setSEGMENTS((SEGMENTS)null);
				return;
			case OdxXhtmlPackage.DATABLOCK__TARGETADDROFFSET:
				setTARGETADDROFFSET((TARGETADDROFFSET)null);
				return;
			case OdxXhtmlPackage.DATABLOCK__OWNIDENTS:
				setOWNIDENTS((OWNIDENTS)null);
				return;
			case OdxXhtmlPackage.DATABLOCK__SECURITYS:
				setSECURITYS((SECURITYS)null);
				return;
			case OdxXhtmlPackage.DATABLOCK__SDGS:
				setSDGS((SDGS)null);
				return;
			case OdxXhtmlPackage.DATABLOCK__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.DATABLOCK__OID:
				setOID(OID_EDEFAULT);
				return;
			case OdxXhtmlPackage.DATABLOCK__TYPE:
				setTYPE(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCK__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.DATABLOCK__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.DATABLOCK__DESC:
				return dESC != null;
			case OdxXhtmlPackage.DATABLOCK__FLASHDATAREF:
				return fLASHDATAREF != null;
			case OdxXhtmlPackage.DATABLOCK__FILTERS:
				return fILTERS != null;
			case OdxXhtmlPackage.DATABLOCK__SEGMENTS:
				return sEGMENTS != null;
			case OdxXhtmlPackage.DATABLOCK__TARGETADDROFFSET:
				return tARGETADDROFFSET != null;
			case OdxXhtmlPackage.DATABLOCK__OWNIDENTS:
				return oWNIDENTS != null;
			case OdxXhtmlPackage.DATABLOCK__SECURITYS:
				return sECURITYS != null;
			case OdxXhtmlPackage.DATABLOCK__SDGS:
				return sDGS != null;
			case OdxXhtmlPackage.DATABLOCK__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.DATABLOCK__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
			case OdxXhtmlPackage.DATABLOCK__TYPE:
				return TYPE_EDEFAULT == null ? tYPE != null : !TYPE_EDEFAULT.equals(tYPE);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(", tYPE: ");
		result.append(tYPE);
		result.append(')');
		return result.toString();
	}

} //DATABLOCKImpl
