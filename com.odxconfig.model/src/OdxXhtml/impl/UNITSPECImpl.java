/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSICALDIMENSIONS;
import OdxXhtml.UNITGROUPS;
import OdxXhtml.UNITS;
import OdxXhtml.UNITSPEC;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UNITSPEC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.UNITSPECImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITSPECImpl#getUNITGROUPS <em>UNITGROUPS</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITSPECImpl#getUNITS <em>UNITS</em>}</li>
 *   <li>{@link OdxXhtml.impl.UNITSPECImpl#getPHYSICALDIMENSIONS <em>PHYSICALDIMENSIONS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UNITSPECImpl extends MinimalEObjectImpl.Container implements UNITSPEC {
	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getUNITGROUPS() <em>UNITGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUNITGROUPS()
	 * @generated
	 * @ordered
	 */
	protected UNITGROUPS uNITGROUPS;

	/**
	 * The cached value of the '{@link #getUNITS() <em>UNITS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUNITS()
	 * @generated
	 * @ordered
	 */
	protected UNITS uNITS;

	/**
	 * The cached value of the '{@link #getPHYSICALDIMENSIONS() <em>PHYSICALDIMENSIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALDIMENSIONS()
	 * @generated
	 * @ordered
	 */
	protected PHYSICALDIMENSIONS pHYSICALDIMENSIONS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UNITSPECImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getUNITSPEC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNITSPEC__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNITSPEC__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNITSPEC__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNITSPEC__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNITGROUPS getUNITGROUPS() {
		return uNITGROUPS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUNITGROUPS(UNITGROUPS newUNITGROUPS, NotificationChain msgs) {
		UNITGROUPS oldUNITGROUPS = uNITGROUPS;
		uNITGROUPS = newUNITGROUPS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNITSPEC__UNITGROUPS, oldUNITGROUPS, newUNITGROUPS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUNITGROUPS(UNITGROUPS newUNITGROUPS) {
		if (newUNITGROUPS != uNITGROUPS) {
			NotificationChain msgs = null;
			if (uNITGROUPS != null)
				msgs = ((InternalEObject)uNITGROUPS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNITSPEC__UNITGROUPS, null, msgs);
			if (newUNITGROUPS != null)
				msgs = ((InternalEObject)newUNITGROUPS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNITSPEC__UNITGROUPS, null, msgs);
			msgs = basicSetUNITGROUPS(newUNITGROUPS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNITSPEC__UNITGROUPS, newUNITGROUPS, newUNITGROUPS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UNITS getUNITS() {
		return uNITS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUNITS(UNITS newUNITS, NotificationChain msgs) {
		UNITS oldUNITS = uNITS;
		uNITS = newUNITS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNITSPEC__UNITS, oldUNITS, newUNITS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUNITS(UNITS newUNITS) {
		if (newUNITS != uNITS) {
			NotificationChain msgs = null;
			if (uNITS != null)
				msgs = ((InternalEObject)uNITS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNITSPEC__UNITS, null, msgs);
			if (newUNITS != null)
				msgs = ((InternalEObject)newUNITS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNITSPEC__UNITS, null, msgs);
			msgs = basicSetUNITS(newUNITS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNITSPEC__UNITS, newUNITS, newUNITS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALDIMENSIONS getPHYSICALDIMENSIONS() {
		return pHYSICALDIMENSIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPHYSICALDIMENSIONS(PHYSICALDIMENSIONS newPHYSICALDIMENSIONS, NotificationChain msgs) {
		PHYSICALDIMENSIONS oldPHYSICALDIMENSIONS = pHYSICALDIMENSIONS;
		pHYSICALDIMENSIONS = newPHYSICALDIMENSIONS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNITSPEC__PHYSICALDIMENSIONS, oldPHYSICALDIMENSIONS, newPHYSICALDIMENSIONS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHYSICALDIMENSIONS(PHYSICALDIMENSIONS newPHYSICALDIMENSIONS) {
		if (newPHYSICALDIMENSIONS != pHYSICALDIMENSIONS) {
			NotificationChain msgs = null;
			if (pHYSICALDIMENSIONS != null)
				msgs = ((InternalEObject)pHYSICALDIMENSIONS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNITSPEC__PHYSICALDIMENSIONS, null, msgs);
			if (newPHYSICALDIMENSIONS != null)
				msgs = ((InternalEObject)newPHYSICALDIMENSIONS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.UNITSPEC__PHYSICALDIMENSIONS, null, msgs);
			msgs = basicSetPHYSICALDIMENSIONS(newPHYSICALDIMENSIONS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.UNITSPEC__PHYSICALDIMENSIONS, newPHYSICALDIMENSIONS, newPHYSICALDIMENSIONS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.UNITSPEC__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.UNITSPEC__UNITGROUPS:
				return basicSetUNITGROUPS(null, msgs);
			case OdxXhtmlPackage.UNITSPEC__UNITS:
				return basicSetUNITS(null, msgs);
			case OdxXhtmlPackage.UNITSPEC__PHYSICALDIMENSIONS:
				return basicSetPHYSICALDIMENSIONS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.UNITSPEC__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.UNITSPEC__UNITGROUPS:
				return getUNITGROUPS();
			case OdxXhtmlPackage.UNITSPEC__UNITS:
				return getUNITS();
			case OdxXhtmlPackage.UNITSPEC__PHYSICALDIMENSIONS:
				return getPHYSICALDIMENSIONS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.UNITSPEC__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.UNITSPEC__UNITGROUPS:
				setUNITGROUPS((UNITGROUPS)newValue);
				return;
			case OdxXhtmlPackage.UNITSPEC__UNITS:
				setUNITS((UNITS)newValue);
				return;
			case OdxXhtmlPackage.UNITSPEC__PHYSICALDIMENSIONS:
				setPHYSICALDIMENSIONS((PHYSICALDIMENSIONS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.UNITSPEC__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.UNITSPEC__UNITGROUPS:
				setUNITGROUPS((UNITGROUPS)null);
				return;
			case OdxXhtmlPackage.UNITSPEC__UNITS:
				setUNITS((UNITS)null);
				return;
			case OdxXhtmlPackage.UNITSPEC__PHYSICALDIMENSIONS:
				setPHYSICALDIMENSIONS((PHYSICALDIMENSIONS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.UNITSPEC__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.UNITSPEC__UNITGROUPS:
				return uNITGROUPS != null;
			case OdxXhtmlPackage.UNITSPEC__UNITS:
				return uNITS != null;
			case OdxXhtmlPackage.UNITSPEC__PHYSICALDIMENSIONS:
				return pHYSICALDIMENSIONS != null;
		}
		return super.eIsSet(featureID);
	}

} //UNITSPECImpl
