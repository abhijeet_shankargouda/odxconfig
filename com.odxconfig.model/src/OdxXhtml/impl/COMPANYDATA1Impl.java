/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYDATA1;
import OdxXhtml.COMPANYSPECIFICINFO1;
import OdxXhtml.DESCRIPTION1;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.ROLES1;
import OdxXhtml.TEAMMEMBERS1;
import OdxXhtml.TEXT1;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYDATA1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYDATA1Impl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATA1Impl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATA1Impl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATA1Impl#getROLES <em>ROLES</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATA1Impl#getTEAMMEMBERS <em>TEAMMEMBERS</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATA1Impl#getCOMPANYSPECIFICINFO <em>COMPANYSPECIFICINFO</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATA1Impl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYDATA1Impl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYDATA1Impl extends MinimalEObjectImpl.Container implements COMPANYDATA1 {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT1 lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION1 dESC;

	/**
	 * The cached value of the '{@link #getROLES() <em>ROLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getROLES()
	 * @generated
	 * @ordered
	 */
	protected ROLES1 rOLES;

	/**
	 * The cached value of the '{@link #getTEAMMEMBERS() <em>TEAMMEMBERS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTEAMMEMBERS()
	 * @generated
	 * @ordered
	 */
	protected TEAMMEMBERS1 tEAMMEMBERS;

	/**
	 * The cached value of the '{@link #getCOMPANYSPECIFICINFO() <em>COMPANYSPECIFICINFO</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYSPECIFICINFO()
	 * @generated
	 * @ordered
	 */
	protected COMPANYSPECIFICINFO1 cOMPANYSPECIFICINFO;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYDATA1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYDATA1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT1 getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT1 newLONGNAME, NotificationChain msgs) {
		TEXT1 oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT1 newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION1 getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION1 newDESC, NotificationChain msgs) {
		DESCRIPTION1 oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION1 newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ROLES1 getROLES() {
		return rOLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetROLES(ROLES1 newROLES, NotificationChain msgs) {
		ROLES1 oldROLES = rOLES;
		rOLES = newROLES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__ROLES, oldROLES, newROLES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setROLES(ROLES1 newROLES) {
		if (newROLES != rOLES) {
			NotificationChain msgs = null;
			if (rOLES != null)
				msgs = ((InternalEObject)rOLES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__ROLES, null, msgs);
			if (newROLES != null)
				msgs = ((InternalEObject)newROLES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__ROLES, null, msgs);
			msgs = basicSetROLES(newROLES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__ROLES, newROLES, newROLES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEAMMEMBERS1 getTEAMMEMBERS() {
		return tEAMMEMBERS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTEAMMEMBERS(TEAMMEMBERS1 newTEAMMEMBERS, NotificationChain msgs) {
		TEAMMEMBERS1 oldTEAMMEMBERS = tEAMMEMBERS;
		tEAMMEMBERS = newTEAMMEMBERS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__TEAMMEMBERS, oldTEAMMEMBERS, newTEAMMEMBERS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTEAMMEMBERS(TEAMMEMBERS1 newTEAMMEMBERS) {
		if (newTEAMMEMBERS != tEAMMEMBERS) {
			NotificationChain msgs = null;
			if (tEAMMEMBERS != null)
				msgs = ((InternalEObject)tEAMMEMBERS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__TEAMMEMBERS, null, msgs);
			if (newTEAMMEMBERS != null)
				msgs = ((InternalEObject)newTEAMMEMBERS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__TEAMMEMBERS, null, msgs);
			msgs = basicSetTEAMMEMBERS(newTEAMMEMBERS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__TEAMMEMBERS, newTEAMMEMBERS, newTEAMMEMBERS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPANYSPECIFICINFO1 getCOMPANYSPECIFICINFO() {
		return cOMPANYSPECIFICINFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO1 newCOMPANYSPECIFICINFO, NotificationChain msgs) {
		COMPANYSPECIFICINFO1 oldCOMPANYSPECIFICINFO = cOMPANYSPECIFICINFO;
		cOMPANYSPECIFICINFO = newCOMPANYSPECIFICINFO;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__COMPANYSPECIFICINFO, oldCOMPANYSPECIFICINFO, newCOMPANYSPECIFICINFO);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO1 newCOMPANYSPECIFICINFO) {
		if (newCOMPANYSPECIFICINFO != cOMPANYSPECIFICINFO) {
			NotificationChain msgs = null;
			if (cOMPANYSPECIFICINFO != null)
				msgs = ((InternalEObject)cOMPANYSPECIFICINFO).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__COMPANYSPECIFICINFO, null, msgs);
			if (newCOMPANYSPECIFICINFO != null)
				msgs = ((InternalEObject)newCOMPANYSPECIFICINFO).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYDATA1__COMPANYSPECIFICINFO, null, msgs);
			msgs = basicSetCOMPANYSPECIFICINFO(newCOMPANYSPECIFICINFO, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__COMPANYSPECIFICINFO, newCOMPANYSPECIFICINFO, newCOMPANYSPECIFICINFO));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYDATA1__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA1__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.COMPANYDATA1__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.COMPANYDATA1__ROLES:
				return basicSetROLES(null, msgs);
			case OdxXhtmlPackage.COMPANYDATA1__TEAMMEMBERS:
				return basicSetTEAMMEMBERS(null, msgs);
			case OdxXhtmlPackage.COMPANYDATA1__COMPANYSPECIFICINFO:
				return basicSetCOMPANYSPECIFICINFO(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA1__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.COMPANYDATA1__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.COMPANYDATA1__DESC:
				return getDESC();
			case OdxXhtmlPackage.COMPANYDATA1__ROLES:
				return getROLES();
			case OdxXhtmlPackage.COMPANYDATA1__TEAMMEMBERS:
				return getTEAMMEMBERS();
			case OdxXhtmlPackage.COMPANYDATA1__COMPANYSPECIFICINFO:
				return getCOMPANYSPECIFICINFO();
			case OdxXhtmlPackage.COMPANYDATA1__ID:
				return getID();
			case OdxXhtmlPackage.COMPANYDATA1__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA1__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__LONGNAME:
				setLONGNAME((TEXT1)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__DESC:
				setDESC((DESCRIPTION1)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__ROLES:
				setROLES((ROLES1)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__TEAMMEMBERS:
				setTEAMMEMBERS((TEAMMEMBERS1)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__COMPANYSPECIFICINFO:
				setCOMPANYSPECIFICINFO((COMPANYSPECIFICINFO1)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA1__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__LONGNAME:
				setLONGNAME((TEXT1)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__DESC:
				setDESC((DESCRIPTION1)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__ROLES:
				setROLES((ROLES1)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__TEAMMEMBERS:
				setTEAMMEMBERS((TEAMMEMBERS1)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__COMPANYSPECIFICINFO:
				setCOMPANYSPECIFICINFO((COMPANYSPECIFICINFO1)null);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPANYDATA1__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYDATA1__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.COMPANYDATA1__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.COMPANYDATA1__DESC:
				return dESC != null;
			case OdxXhtmlPackage.COMPANYDATA1__ROLES:
				return rOLES != null;
			case OdxXhtmlPackage.COMPANYDATA1__TEAMMEMBERS:
				return tEAMMEMBERS != null;
			case OdxXhtmlPackage.COMPANYDATA1__COMPANYSPECIFICINFO:
				return cOMPANYSPECIFICINFO != null;
			case OdxXhtmlPackage.COMPANYDATA1__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.COMPANYDATA1__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //COMPANYDATA1Impl
