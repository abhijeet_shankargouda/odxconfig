/**
 */
package OdxXhtml.impl;

import OdxXhtml.MATCHINGCOMPONENT;
import OdxXhtml.MATCHINGCOMPONENTS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MATCHINGCOMPONENTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MATCHINGCOMPONENTSImpl#getMATCHINGCOMPONENT <em>MATCHINGCOMPONENT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MATCHINGCOMPONENTSImpl extends MinimalEObjectImpl.Container implements MATCHINGCOMPONENTS {
	/**
	 * The cached value of the '{@link #getMATCHINGCOMPONENT() <em>MATCHINGCOMPONENT</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMATCHINGCOMPONENT()
	 * @generated
	 * @ordered
	 */
	protected EList<MATCHINGCOMPONENT> mATCHINGCOMPONENT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MATCHINGCOMPONENTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMATCHINGCOMPONENTS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<MATCHINGCOMPONENT> getMATCHINGCOMPONENT() {
		if (mATCHINGCOMPONENT == null) {
			mATCHINGCOMPONENT = new EObjectContainmentEList<MATCHINGCOMPONENT>(MATCHINGCOMPONENT.class, this, OdxXhtmlPackage.MATCHINGCOMPONENTS__MATCHINGCOMPONENT);
		}
		return mATCHINGCOMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENTS__MATCHINGCOMPONENT:
				return ((InternalEList<?>)getMATCHINGCOMPONENT()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENTS__MATCHINGCOMPONENT:
				return getMATCHINGCOMPONENT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENTS__MATCHINGCOMPONENT:
				getMATCHINGCOMPONENT().clear();
				getMATCHINGCOMPONENT().addAll((Collection<? extends MATCHINGCOMPONENT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENTS__MATCHINGCOMPONENT:
				getMATCHINGCOMPONENT().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGCOMPONENTS__MATCHINGCOMPONENT:
				return mATCHINGCOMPONENT != null && !mATCHINGCOMPONENT.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //MATCHINGCOMPONENTSImpl
