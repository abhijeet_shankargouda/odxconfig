/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION1;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.ROLES1;
import OdxXhtml.TEAMMEMBER1;
import OdxXhtml.TEXT1;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TEAMMEMBER1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getROLES <em>ROLES</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getDEPARTMENT <em>DEPARTMENT</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getADDRESS <em>ADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getZIP <em>ZIP</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getCITY <em>CITY</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getPHONE <em>PHONE</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getFAX <em>FAX</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getEMAIL <em>EMAIL</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.TEAMMEMBER1Impl#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TEAMMEMBER1Impl extends MinimalEObjectImpl.Container implements TEAMMEMBER1 {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT1 lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION1 dESC;

	/**
	 * The cached value of the '{@link #getROLES() <em>ROLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getROLES()
	 * @generated
	 * @ordered
	 */
	protected ROLES1 rOLES;

	/**
	 * The default value of the '{@link #getDEPARTMENT() <em>DEPARTMENT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDEPARTMENT()
	 * @generated
	 * @ordered
	 */
	protected static final String DEPARTMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDEPARTMENT() <em>DEPARTMENT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDEPARTMENT()
	 * @generated
	 * @ordered
	 */
	protected String dEPARTMENT = DEPARTMENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getADDRESS() <em>ADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADDRESS()
	 * @generated
	 * @ordered
	 */
	protected static final String ADDRESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getADDRESS() <em>ADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADDRESS()
	 * @generated
	 * @ordered
	 */
	protected String aDDRESS = ADDRESS_EDEFAULT;

	/**
	 * The default value of the '{@link #getZIP() <em>ZIP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZIP()
	 * @generated
	 * @ordered
	 */
	protected static final String ZIP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getZIP() <em>ZIP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZIP()
	 * @generated
	 * @ordered
	 */
	protected String zIP = ZIP_EDEFAULT;

	/**
	 * The default value of the '{@link #getCITY() <em>CITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCITY()
	 * @generated
	 * @ordered
	 */
	protected static final String CITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCITY() <em>CITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCITY()
	 * @generated
	 * @ordered
	 */
	protected String cITY = CITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPHONE() <em>PHONE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHONE()
	 * @generated
	 * @ordered
	 */
	protected static final String PHONE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPHONE() <em>PHONE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHONE()
	 * @generated
	 * @ordered
	 */
	protected String pHONE = PHONE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFAX() <em>FAX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFAX()
	 * @generated
	 * @ordered
	 */
	protected static final String FAX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFAX() <em>FAX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFAX()
	 * @generated
	 * @ordered
	 */
	protected String fAX = FAX_EDEFAULT;

	/**
	 * The default value of the '{@link #getEMAIL() <em>EMAIL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEMAIL()
	 * @generated
	 * @ordered
	 */
	protected static final String EMAIL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEMAIL() <em>EMAIL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEMAIL()
	 * @generated
	 * @ordered
	 */
	protected String eMAIL = EMAIL_EDEFAULT;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TEAMMEMBER1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getTEAMMEMBER1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT1 getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT1 newLONGNAME, NotificationChain msgs) {
		TEXT1 oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT1 newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TEAMMEMBER1__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TEAMMEMBER1__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION1 getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION1 newDESC, NotificationChain msgs) {
		DESCRIPTION1 oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION1 newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TEAMMEMBER1__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TEAMMEMBER1__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ROLES1 getROLES() {
		return rOLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetROLES(ROLES1 newROLES, NotificationChain msgs) {
		ROLES1 oldROLES = rOLES;
		rOLES = newROLES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__ROLES, oldROLES, newROLES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setROLES(ROLES1 newROLES) {
		if (newROLES != rOLES) {
			NotificationChain msgs = null;
			if (rOLES != null)
				msgs = ((InternalEObject)rOLES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TEAMMEMBER1__ROLES, null, msgs);
			if (newROLES != null)
				msgs = ((InternalEObject)newROLES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TEAMMEMBER1__ROLES, null, msgs);
			msgs = basicSetROLES(newROLES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__ROLES, newROLES, newROLES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDEPARTMENT() {
		return dEPARTMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDEPARTMENT(String newDEPARTMENT) {
		String oldDEPARTMENT = dEPARTMENT;
		dEPARTMENT = newDEPARTMENT;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__DEPARTMENT, oldDEPARTMENT, dEPARTMENT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getADDRESS() {
		return aDDRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADDRESS(String newADDRESS) {
		String oldADDRESS = aDDRESS;
		aDDRESS = newADDRESS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__ADDRESS, oldADDRESS, aDDRESS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getZIP() {
		return zIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setZIP(String newZIP) {
		String oldZIP = zIP;
		zIP = newZIP;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__ZIP, oldZIP, zIP));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCITY() {
		return cITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCITY(String newCITY) {
		String oldCITY = cITY;
		cITY = newCITY;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__CITY, oldCITY, cITY));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPHONE() {
		return pHONE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHONE(String newPHONE) {
		String oldPHONE = pHONE;
		pHONE = newPHONE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__PHONE, oldPHONE, pHONE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFAX() {
		return fAX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFAX(String newFAX) {
		String oldFAX = fAX;
		fAX = newFAX;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__FAX, oldFAX, fAX));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getEMAIL() {
		return eMAIL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEMAIL(String newEMAIL) {
		String oldEMAIL = eMAIL;
		eMAIL = newEMAIL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__EMAIL, oldEMAIL, eMAIL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TEAMMEMBER1__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBER1__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.TEAMMEMBER1__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.TEAMMEMBER1__ROLES:
				return basicSetROLES(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBER1__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.TEAMMEMBER1__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.TEAMMEMBER1__DESC:
				return getDESC();
			case OdxXhtmlPackage.TEAMMEMBER1__ROLES:
				return getROLES();
			case OdxXhtmlPackage.TEAMMEMBER1__DEPARTMENT:
				return getDEPARTMENT();
			case OdxXhtmlPackage.TEAMMEMBER1__ADDRESS:
				return getADDRESS();
			case OdxXhtmlPackage.TEAMMEMBER1__ZIP:
				return getZIP();
			case OdxXhtmlPackage.TEAMMEMBER1__CITY:
				return getCITY();
			case OdxXhtmlPackage.TEAMMEMBER1__PHONE:
				return getPHONE();
			case OdxXhtmlPackage.TEAMMEMBER1__FAX:
				return getFAX();
			case OdxXhtmlPackage.TEAMMEMBER1__EMAIL:
				return getEMAIL();
			case OdxXhtmlPackage.TEAMMEMBER1__ID:
				return getID();
			case OdxXhtmlPackage.TEAMMEMBER1__OID:
				return getOID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBER1__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__LONGNAME:
				setLONGNAME((TEXT1)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__DESC:
				setDESC((DESCRIPTION1)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__ROLES:
				setROLES((ROLES1)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__DEPARTMENT:
				setDEPARTMENT((String)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__ADDRESS:
				setADDRESS((String)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__ZIP:
				setZIP((String)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__CITY:
				setCITY((String)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__PHONE:
				setPHONE((String)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__FAX:
				setFAX((String)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__EMAIL:
				setEMAIL((String)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__OID:
				setOID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBER1__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__LONGNAME:
				setLONGNAME((TEXT1)null);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__DESC:
				setDESC((DESCRIPTION1)null);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__ROLES:
				setROLES((ROLES1)null);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__DEPARTMENT:
				setDEPARTMENT(DEPARTMENT_EDEFAULT);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__ADDRESS:
				setADDRESS(ADDRESS_EDEFAULT);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__ZIP:
				setZIP(ZIP_EDEFAULT);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__CITY:
				setCITY(CITY_EDEFAULT);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__PHONE:
				setPHONE(PHONE_EDEFAULT);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__FAX:
				setFAX(FAX_EDEFAULT);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__EMAIL:
				setEMAIL(EMAIL_EDEFAULT);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.TEAMMEMBER1__OID:
				setOID(OID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TEAMMEMBER1__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.TEAMMEMBER1__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.TEAMMEMBER1__DESC:
				return dESC != null;
			case OdxXhtmlPackage.TEAMMEMBER1__ROLES:
				return rOLES != null;
			case OdxXhtmlPackage.TEAMMEMBER1__DEPARTMENT:
				return DEPARTMENT_EDEFAULT == null ? dEPARTMENT != null : !DEPARTMENT_EDEFAULT.equals(dEPARTMENT);
			case OdxXhtmlPackage.TEAMMEMBER1__ADDRESS:
				return ADDRESS_EDEFAULT == null ? aDDRESS != null : !ADDRESS_EDEFAULT.equals(aDDRESS);
			case OdxXhtmlPackage.TEAMMEMBER1__ZIP:
				return ZIP_EDEFAULT == null ? zIP != null : !ZIP_EDEFAULT.equals(zIP);
			case OdxXhtmlPackage.TEAMMEMBER1__CITY:
				return CITY_EDEFAULT == null ? cITY != null : !CITY_EDEFAULT.equals(cITY);
			case OdxXhtmlPackage.TEAMMEMBER1__PHONE:
				return PHONE_EDEFAULT == null ? pHONE != null : !PHONE_EDEFAULT.equals(pHONE);
			case OdxXhtmlPackage.TEAMMEMBER1__FAX:
				return FAX_EDEFAULT == null ? fAX != null : !FAX_EDEFAULT.equals(fAX);
			case OdxXhtmlPackage.TEAMMEMBER1__EMAIL:
				return EMAIL_EDEFAULT == null ? eMAIL != null : !EMAIL_EDEFAULT.equals(eMAIL);
			case OdxXhtmlPackage.TEAMMEMBER1__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.TEAMMEMBER1__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", dEPARTMENT: ");
		result.append(dEPARTMENT);
		result.append(", aDDRESS: ");
		result.append(aDDRESS);
		result.append(", zIP: ");
		result.append(zIP);
		result.append(", cITY: ");
		result.append(cITY);
		result.append(", pHONE: ");
		result.append(pHONE);
		result.append(", fAX: ");
		result.append(fAX);
		result.append(", eMAIL: ");
		result.append(eMAIL);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(')');
		return result.toString();
	}

} //TEAMMEMBER1Impl
