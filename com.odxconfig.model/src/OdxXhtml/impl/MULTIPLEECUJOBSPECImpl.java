/**
 */
package OdxXhtml.impl;

import OdxXhtml.DIAGDATADICTIONARYSPEC;
import OdxXhtml.FUNCTCLASSS;
import OdxXhtml.MULTIPLEECUJOBS;
import OdxXhtml.MULTIPLEECUJOBSPEC;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MULTIPLEECUJOBSPEC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBSPECImpl#getMULTIPLEECUJOBS <em>MULTIPLEECUJOBS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBSPECImpl#getDIAGDATADICTIONARYSPEC <em>DIAGDATADICTIONARYSPEC</em>}</li>
 *   <li>{@link OdxXhtml.impl.MULTIPLEECUJOBSPECImpl#getFUNCTCLASSS <em>FUNCTCLASSS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MULTIPLEECUJOBSPECImpl extends ODXCATEGORYImpl implements MULTIPLEECUJOBSPEC {
	/**
	 * The cached value of the '{@link #getMULTIPLEECUJOBS() <em>MULTIPLEECUJOBS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMULTIPLEECUJOBS()
	 * @generated
	 * @ordered
	 */
	protected MULTIPLEECUJOBS mULTIPLEECUJOBS;

	/**
	 * The cached value of the '{@link #getDIAGDATADICTIONARYSPEC() <em>DIAGDATADICTIONARYSPEC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGDATADICTIONARYSPEC()
	 * @generated
	 * @ordered
	 */
	protected DIAGDATADICTIONARYSPEC dIAGDATADICTIONARYSPEC;

	/**
	 * The cached value of the '{@link #getFUNCTCLASSS() <em>FUNCTCLASSS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFUNCTCLASSS()
	 * @generated
	 * @ordered
	 */
	protected FUNCTCLASSS fUNCTCLASSS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MULTIPLEECUJOBSPECImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMULTIPLEECUJOBSPEC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MULTIPLEECUJOBS getMULTIPLEECUJOBS() {
		return mULTIPLEECUJOBS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMULTIPLEECUJOBS(MULTIPLEECUJOBS newMULTIPLEECUJOBS, NotificationChain msgs) {
		MULTIPLEECUJOBS oldMULTIPLEECUJOBS = mULTIPLEECUJOBS;
		mULTIPLEECUJOBS = newMULTIPLEECUJOBS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOBSPEC__MULTIPLEECUJOBS, oldMULTIPLEECUJOBS, newMULTIPLEECUJOBS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMULTIPLEECUJOBS(MULTIPLEECUJOBS newMULTIPLEECUJOBS) {
		if (newMULTIPLEECUJOBS != mULTIPLEECUJOBS) {
			NotificationChain msgs = null;
			if (mULTIPLEECUJOBS != null)
				msgs = ((InternalEObject)mULTIPLEECUJOBS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOBSPEC__MULTIPLEECUJOBS, null, msgs);
			if (newMULTIPLEECUJOBS != null)
				msgs = ((InternalEObject)newMULTIPLEECUJOBS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOBSPEC__MULTIPLEECUJOBS, null, msgs);
			msgs = basicSetMULTIPLEECUJOBS(newMULTIPLEECUJOBS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOBSPEC__MULTIPLEECUJOBS, newMULTIPLEECUJOBS, newMULTIPLEECUJOBS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGDATADICTIONARYSPEC getDIAGDATADICTIONARYSPEC() {
		return dIAGDATADICTIONARYSPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC newDIAGDATADICTIONARYSPEC, NotificationChain msgs) {
		DIAGDATADICTIONARYSPEC oldDIAGDATADICTIONARYSPEC = dIAGDATADICTIONARYSPEC;
		dIAGDATADICTIONARYSPEC = newDIAGDATADICTIONARYSPEC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOBSPEC__DIAGDATADICTIONARYSPEC, oldDIAGDATADICTIONARYSPEC, newDIAGDATADICTIONARYSPEC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGDATADICTIONARYSPEC(DIAGDATADICTIONARYSPEC newDIAGDATADICTIONARYSPEC) {
		if (newDIAGDATADICTIONARYSPEC != dIAGDATADICTIONARYSPEC) {
			NotificationChain msgs = null;
			if (dIAGDATADICTIONARYSPEC != null)
				msgs = ((InternalEObject)dIAGDATADICTIONARYSPEC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOBSPEC__DIAGDATADICTIONARYSPEC, null, msgs);
			if (newDIAGDATADICTIONARYSPEC != null)
				msgs = ((InternalEObject)newDIAGDATADICTIONARYSPEC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOBSPEC__DIAGDATADICTIONARYSPEC, null, msgs);
			msgs = basicSetDIAGDATADICTIONARYSPEC(newDIAGDATADICTIONARYSPEC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOBSPEC__DIAGDATADICTIONARYSPEC, newDIAGDATADICTIONARYSPEC, newDIAGDATADICTIONARYSPEC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTCLASSS getFUNCTCLASSS() {
		return fUNCTCLASSS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFUNCTCLASSS(FUNCTCLASSS newFUNCTCLASSS, NotificationChain msgs) {
		FUNCTCLASSS oldFUNCTCLASSS = fUNCTCLASSS;
		fUNCTCLASSS = newFUNCTCLASSS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOBSPEC__FUNCTCLASSS, oldFUNCTCLASSS, newFUNCTCLASSS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFUNCTCLASSS(FUNCTCLASSS newFUNCTCLASSS) {
		if (newFUNCTCLASSS != fUNCTCLASSS) {
			NotificationChain msgs = null;
			if (fUNCTCLASSS != null)
				msgs = ((InternalEObject)fUNCTCLASSS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOBSPEC__FUNCTCLASSS, null, msgs);
			if (newFUNCTCLASSS != null)
				msgs = ((InternalEObject)newFUNCTCLASSS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.MULTIPLEECUJOBSPEC__FUNCTCLASSS, null, msgs);
			msgs = basicSetFUNCTCLASSS(newFUNCTCLASSS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MULTIPLEECUJOBSPEC__FUNCTCLASSS, newFUNCTCLASSS, newFUNCTCLASSS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__MULTIPLEECUJOBS:
				return basicSetMULTIPLEECUJOBS(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__DIAGDATADICTIONARYSPEC:
				return basicSetDIAGDATADICTIONARYSPEC(null, msgs);
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__FUNCTCLASSS:
				return basicSetFUNCTCLASSS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__MULTIPLEECUJOBS:
				return getMULTIPLEECUJOBS();
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__DIAGDATADICTIONARYSPEC:
				return getDIAGDATADICTIONARYSPEC();
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__FUNCTCLASSS:
				return getFUNCTCLASSS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__MULTIPLEECUJOBS:
				setMULTIPLEECUJOBS((MULTIPLEECUJOBS)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__DIAGDATADICTIONARYSPEC:
				setDIAGDATADICTIONARYSPEC((DIAGDATADICTIONARYSPEC)newValue);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__FUNCTCLASSS:
				setFUNCTCLASSS((FUNCTCLASSS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__MULTIPLEECUJOBS:
				setMULTIPLEECUJOBS((MULTIPLEECUJOBS)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__DIAGDATADICTIONARYSPEC:
				setDIAGDATADICTIONARYSPEC((DIAGDATADICTIONARYSPEC)null);
				return;
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__FUNCTCLASSS:
				setFUNCTCLASSS((FUNCTCLASSS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__MULTIPLEECUJOBS:
				return mULTIPLEECUJOBS != null;
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__DIAGDATADICTIONARYSPEC:
				return dIAGDATADICTIONARYSPEC != null;
			case OdxXhtmlPackage.MULTIPLEECUJOBSPEC__FUNCTCLASSS:
				return fUNCTCLASSS != null;
		}
		return super.eIsSet(featureID);
	}

} //MULTIPLEECUJOBSPECImpl
