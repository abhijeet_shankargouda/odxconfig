/**
 */
package OdxXhtml.impl;

import OdxXhtml.FILTER;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FILTER</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FILTERImpl#getFILTERSTART <em>FILTERSTART</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FILTERImpl extends MinimalEObjectImpl.Container implements FILTER {
	/**
	 * The default value of the '{@link #getFILTERSTART() <em>FILTERSTART</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILTERSTART()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] FILTERSTART_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFILTERSTART() <em>FILTERSTART</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFILTERSTART()
	 * @generated
	 * @ordered
	 */
	protected byte[] fILTERSTART = FILTERSTART_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FILTERImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFILTER();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getFILTERSTART() {
		return fILTERSTART;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFILTERSTART(byte[] newFILTERSTART) {
		byte[] oldFILTERSTART = fILTERSTART;
		fILTERSTART = newFILTERSTART;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FILTER__FILTERSTART, oldFILTERSTART, fILTERSTART));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FILTER__FILTERSTART:
				return getFILTERSTART();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FILTER__FILTERSTART:
				setFILTERSTART((byte[])newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FILTER__FILTERSTART:
				setFILTERSTART(FILTERSTART_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FILTER__FILTERSTART:
				return FILTERSTART_EDEFAULT == null ? fILTERSTART != null : !FILTERSTART_EDEFAULT.equals(fILTERSTART);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (fILTERSTART: ");
		result.append(fILTERSTART);
		result.append(')');
		return result.toString();
	}

} //FILTERImpl
