/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADDRDEFPHYSSEGMENT;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ADDRDEFPHYSSEGMENT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ADDRDEFPHYSSEGMENTImpl#getENDADDRESS <em>ENDADDRESS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ADDRDEFPHYSSEGMENTImpl extends PHYSSEGMENTImpl implements ADDRDEFPHYSSEGMENT {
	/**
	 * The default value of the '{@link #getENDADDRESS() <em>ENDADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENDADDRESS()
	 * @generated
	 * @ordered
	 */
	protected static final byte[] ENDADDRESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getENDADDRESS() <em>ENDADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENDADDRESS()
	 * @generated
	 * @ordered
	 */
	protected byte[] eNDADDRESS = ENDADDRESS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ADDRDEFPHYSSEGMENTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getADDRDEFPHYSSEGMENT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public byte[] getENDADDRESS() {
		return eNDADDRESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setENDADDRESS(byte[] newENDADDRESS) {
		byte[] oldENDADDRESS = eNDADDRESS;
		eNDADDRESS = newENDADDRESS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.ADDRDEFPHYSSEGMENT__ENDADDRESS, oldENDADDRESS, eNDADDRESS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ADDRDEFPHYSSEGMENT__ENDADDRESS:
				return getENDADDRESS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ADDRDEFPHYSSEGMENT__ENDADDRESS:
				setENDADDRESS((byte[])newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ADDRDEFPHYSSEGMENT__ENDADDRESS:
				setENDADDRESS(ENDADDRESS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ADDRDEFPHYSSEGMENT__ENDADDRESS:
				return ENDADDRESS_EDEFAULT == null ? eNDADDRESS != null : !ENDADDRESS_EDEFAULT.equals(eNDADDRESS);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (eNDADDRESS: ");
		result.append(eNDADDRESS);
		result.append(')');
		return result.toString();
	}

} //ADDRDEFPHYSSEGMENTImpl
