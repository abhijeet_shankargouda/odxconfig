/**
 */
package OdxXhtml.impl;

import OdxXhtml.BASEVARIANT;
import OdxXhtml.BASEVARIANTS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BASEVARIANTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.BASEVARIANTSImpl#getBASEVARIANT <em>BASEVARIANT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BASEVARIANTSImpl extends MinimalEObjectImpl.Container implements BASEVARIANTS {
	/**
	 * The cached value of the '{@link #getBASEVARIANT() <em>BASEVARIANT</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASEVARIANT()
	 * @generated
	 * @ordered
	 */
	protected EList<BASEVARIANT> bASEVARIANT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BASEVARIANTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getBASEVARIANTS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BASEVARIANT> getBASEVARIANT() {
		if (bASEVARIANT == null) {
			bASEVARIANT = new EObjectContainmentEList<BASEVARIANT>(BASEVARIANT.class, this, OdxXhtmlPackage.BASEVARIANTS__BASEVARIANT);
		}
		return bASEVARIANT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANTS__BASEVARIANT:
				return ((InternalEList<?>)getBASEVARIANT()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANTS__BASEVARIANT:
				return getBASEVARIANT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANTS__BASEVARIANT:
				getBASEVARIANT().clear();
				getBASEVARIANT().addAll((Collection<? extends BASEVARIANT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANTS__BASEVARIANT:
				getBASEVARIANT().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.BASEVARIANTS__BASEVARIANT:
				return bASEVARIANT != null && !bASEVARIANT.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BASEVARIANTSImpl
