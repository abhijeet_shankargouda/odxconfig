/**
 */
package OdxXhtml.impl;

import OdxXhtml.NEGRESPONSEREFS;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NEGRESPONSEREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.NEGRESPONSEREFSImpl#getNEGRESPONSEREF <em>NEGRESPONSEREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NEGRESPONSEREFSImpl extends MinimalEObjectImpl.Container implements NEGRESPONSEREFS {
	/**
	 * The cached value of the '{@link #getNEGRESPONSEREF() <em>NEGRESPONSEREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNEGRESPONSEREF()
	 * @generated
	 * @ordered
	 */
	protected EList<ODXLINK> nEGRESPONSEREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NEGRESPONSEREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getNEGRESPONSEREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getNEGRESPONSEREF() {
		if (nEGRESPONSEREF == null) {
			nEGRESPONSEREF = new EObjectContainmentEList<ODXLINK>(ODXLINK.class, this, OdxXhtmlPackage.NEGRESPONSEREFS__NEGRESPONSEREF);
		}
		return nEGRESPONSEREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSEREFS__NEGRESPONSEREF:
				return ((InternalEList<?>)getNEGRESPONSEREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSEREFS__NEGRESPONSEREF:
				return getNEGRESPONSEREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSEREFS__NEGRESPONSEREF:
				getNEGRESPONSEREF().clear();
				getNEGRESPONSEREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSEREFS__NEGRESPONSEREF:
				getNEGRESPONSEREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.NEGRESPONSEREFS__NEGRESPONSEREF:
				return nEGRESPONSEREF != null && !nEGRESPONSEREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //NEGRESPONSEREFSImpl
