/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPARAM;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.STANDARDISATIONLEVEL;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPARAM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getPHYSICALDEFAULTVALUE <em>PHYSICALDEFAULTVALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getDATAOBJECTPROPREF <em>DATAOBJECTPROPREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getCPTYPE <em>CPTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getDISPLAYLEVEL <em>DISPLAYLEVEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getPARAMCLASS <em>PARAMCLASS</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPARAMImpl#getPDUAPIINDEX <em>PDUAPIINDEX</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPARAMImpl extends MinimalEObjectImpl.Container implements COMPARAM {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getPHYSICALDEFAULTVALUE() <em>PHYSICALDEFAULTVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALDEFAULTVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String PHYSICALDEFAULTVALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPHYSICALDEFAULTVALUE() <em>PHYSICALDEFAULTVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPHYSICALDEFAULTVALUE()
	 * @generated
	 * @ordered
	 */
	protected String pHYSICALDEFAULTVALUE = PHYSICALDEFAULTVALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDATAOBJECTPROPREF() <em>DATAOBJECTPROPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATAOBJECTPROPREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK dATAOBJECTPROPREF;

	/**
	 * The default value of the '{@link #getCPTYPE() <em>CPTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCPTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final STANDARDISATIONLEVEL CPTYPE_EDEFAULT = STANDARDISATIONLEVEL.STANDARD;

	/**
	 * The cached value of the '{@link #getCPTYPE() <em>CPTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCPTYPE()
	 * @generated
	 * @ordered
	 */
	protected STANDARDISATIONLEVEL cPTYPE = CPTYPE_EDEFAULT;

	/**
	 * This is true if the CPTYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean cPTYPEESet;

	/**
	 * The default value of the '{@link #getDISPLAYLEVEL() <em>DISPLAYLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDISPLAYLEVEL()
	 * @generated
	 * @ordered
	 */
	protected static final long DISPLAYLEVEL_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getDISPLAYLEVEL() <em>DISPLAYLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDISPLAYLEVEL()
	 * @generated
	 * @ordered
	 */
	protected long dISPLAYLEVEL = DISPLAYLEVEL_EDEFAULT;

	/**
	 * This is true if the DISPLAYLEVEL attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dISPLAYLEVELESet;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * The default value of the '{@link #getPARAMCLASS() <em>PARAMCLASS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARAMCLASS()
	 * @generated
	 * @ordered
	 */
	protected static final String PARAMCLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPARAMCLASS() <em>PARAMCLASS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPARAMCLASS()
	 * @generated
	 * @ordered
	 */
	protected String pARAMCLASS = PARAMCLASS_EDEFAULT;

	/**
	 * The default value of the '{@link #getPDUAPIINDEX() <em>PDUAPIINDEX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPDUAPIINDEX()
	 * @generated
	 * @ordered
	 */
	protected static final long PDUAPIINDEX_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getPDUAPIINDEX() <em>PDUAPIINDEX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPDUAPIINDEX()
	 * @generated
	 * @ordered
	 */
	protected long pDUAPIINDEX = PDUAPIINDEX_EDEFAULT;

	/**
	 * This is true if the PDUAPIINDEX attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean pDUAPIINDEXESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPARAMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPARAM();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAM__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAM__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAM__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAM__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPHYSICALDEFAULTVALUE() {
		return pHYSICALDEFAULTVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPHYSICALDEFAULTVALUE(String newPHYSICALDEFAULTVALUE) {
		String oldPHYSICALDEFAULTVALUE = pHYSICALDEFAULTVALUE;
		pHYSICALDEFAULTVALUE = newPHYSICALDEFAULTVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__PHYSICALDEFAULTVALUE, oldPHYSICALDEFAULTVALUE, pHYSICALDEFAULTVALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getDATAOBJECTPROPREF() {
		return dATAOBJECTPROPREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDATAOBJECTPROPREF(ODXLINK newDATAOBJECTPROPREF, NotificationChain msgs) {
		ODXLINK oldDATAOBJECTPROPREF = dATAOBJECTPROPREF;
		dATAOBJECTPROPREF = newDATAOBJECTPROPREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__DATAOBJECTPROPREF, oldDATAOBJECTPROPREF, newDATAOBJECTPROPREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDATAOBJECTPROPREF(ODXLINK newDATAOBJECTPROPREF) {
		if (newDATAOBJECTPROPREF != dATAOBJECTPROPREF) {
			NotificationChain msgs = null;
			if (dATAOBJECTPROPREF != null)
				msgs = ((InternalEObject)dATAOBJECTPROPREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAM__DATAOBJECTPROPREF, null, msgs);
			if (newDATAOBJECTPROPREF != null)
				msgs = ((InternalEObject)newDATAOBJECTPROPREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPARAM__DATAOBJECTPROPREF, null, msgs);
			msgs = basicSetDATAOBJECTPROPREF(newDATAOBJECTPROPREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__DATAOBJECTPROPREF, newDATAOBJECTPROPREF, newDATAOBJECTPROPREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public STANDARDISATIONLEVEL getCPTYPE() {
		return cPTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCPTYPE(STANDARDISATIONLEVEL newCPTYPE) {
		STANDARDISATIONLEVEL oldCPTYPE = cPTYPE;
		cPTYPE = newCPTYPE == null ? CPTYPE_EDEFAULT : newCPTYPE;
		boolean oldCPTYPEESet = cPTYPEESet;
		cPTYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__CPTYPE, oldCPTYPE, cPTYPE, !oldCPTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetCPTYPE() {
		STANDARDISATIONLEVEL oldCPTYPE = cPTYPE;
		boolean oldCPTYPEESet = cPTYPEESet;
		cPTYPE = CPTYPE_EDEFAULT;
		cPTYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.COMPARAM__CPTYPE, oldCPTYPE, CPTYPE_EDEFAULT, oldCPTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetCPTYPE() {
		return cPTYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getDISPLAYLEVEL() {
		return dISPLAYLEVEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDISPLAYLEVEL(long newDISPLAYLEVEL) {
		long oldDISPLAYLEVEL = dISPLAYLEVEL;
		dISPLAYLEVEL = newDISPLAYLEVEL;
		boolean oldDISPLAYLEVELESet = dISPLAYLEVELESet;
		dISPLAYLEVELESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__DISPLAYLEVEL, oldDISPLAYLEVEL, dISPLAYLEVEL, !oldDISPLAYLEVELESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDISPLAYLEVEL() {
		long oldDISPLAYLEVEL = dISPLAYLEVEL;
		boolean oldDISPLAYLEVELESet = dISPLAYLEVELESet;
		dISPLAYLEVEL = DISPLAYLEVEL_EDEFAULT;
		dISPLAYLEVELESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.COMPARAM__DISPLAYLEVEL, oldDISPLAYLEVEL, DISPLAYLEVEL_EDEFAULT, oldDISPLAYLEVELESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDISPLAYLEVEL() {
		return dISPLAYLEVELESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPARAMCLASS() {
		return pARAMCLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPARAMCLASS(String newPARAMCLASS) {
		String oldPARAMCLASS = pARAMCLASS;
		pARAMCLASS = newPARAMCLASS;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__PARAMCLASS, oldPARAMCLASS, pARAMCLASS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getPDUAPIINDEX() {
		return pDUAPIINDEX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPDUAPIINDEX(long newPDUAPIINDEX) {
		long oldPDUAPIINDEX = pDUAPIINDEX;
		pDUAPIINDEX = newPDUAPIINDEX;
		boolean oldPDUAPIINDEXESet = pDUAPIINDEXESet;
		pDUAPIINDEXESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPARAM__PDUAPIINDEX, oldPDUAPIINDEX, pDUAPIINDEX, !oldPDUAPIINDEXESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetPDUAPIINDEX() {
		long oldPDUAPIINDEX = pDUAPIINDEX;
		boolean oldPDUAPIINDEXESet = pDUAPIINDEXESet;
		pDUAPIINDEX = PDUAPIINDEX_EDEFAULT;
		pDUAPIINDEXESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.COMPARAM__PDUAPIINDEX, oldPDUAPIINDEX, PDUAPIINDEX_EDEFAULT, oldPDUAPIINDEXESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetPDUAPIINDEX() {
		return pDUAPIINDEXESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAM__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.COMPARAM__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.COMPARAM__DATAOBJECTPROPREF:
				return basicSetDATAOBJECTPROPREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAM__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.COMPARAM__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.COMPARAM__DESC:
				return getDESC();
			case OdxXhtmlPackage.COMPARAM__PHYSICALDEFAULTVALUE:
				return getPHYSICALDEFAULTVALUE();
			case OdxXhtmlPackage.COMPARAM__DATAOBJECTPROPREF:
				return getDATAOBJECTPROPREF();
			case OdxXhtmlPackage.COMPARAM__CPTYPE:
				return getCPTYPE();
			case OdxXhtmlPackage.COMPARAM__DISPLAYLEVEL:
				return getDISPLAYLEVEL();
			case OdxXhtmlPackage.COMPARAM__ID:
				return getID();
			case OdxXhtmlPackage.COMPARAM__OID:
				return getOID();
			case OdxXhtmlPackage.COMPARAM__PARAMCLASS:
				return getPARAMCLASS();
			case OdxXhtmlPackage.COMPARAM__PDUAPIINDEX:
				return getPDUAPIINDEX();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAM__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__PHYSICALDEFAULTVALUE:
				setPHYSICALDEFAULTVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__DATAOBJECTPROPREF:
				setDATAOBJECTPROPREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__CPTYPE:
				setCPTYPE((STANDARDISATIONLEVEL)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__DISPLAYLEVEL:
				setDISPLAYLEVEL((Long)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__OID:
				setOID((String)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__PARAMCLASS:
				setPARAMCLASS((String)newValue);
				return;
			case OdxXhtmlPackage.COMPARAM__PDUAPIINDEX:
				setPDUAPIINDEX((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAM__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPARAM__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.COMPARAM__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.COMPARAM__PHYSICALDEFAULTVALUE:
				setPHYSICALDEFAULTVALUE(PHYSICALDEFAULTVALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPARAM__DATAOBJECTPROPREF:
				setDATAOBJECTPROPREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.COMPARAM__CPTYPE:
				unsetCPTYPE();
				return;
			case OdxXhtmlPackage.COMPARAM__DISPLAYLEVEL:
				unsetDISPLAYLEVEL();
				return;
			case OdxXhtmlPackage.COMPARAM__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPARAM__OID:
				setOID(OID_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPARAM__PARAMCLASS:
				setPARAMCLASS(PARAMCLASS_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPARAM__PDUAPIINDEX:
				unsetPDUAPIINDEX();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPARAM__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.COMPARAM__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.COMPARAM__DESC:
				return dESC != null;
			case OdxXhtmlPackage.COMPARAM__PHYSICALDEFAULTVALUE:
				return PHYSICALDEFAULTVALUE_EDEFAULT == null ? pHYSICALDEFAULTVALUE != null : !PHYSICALDEFAULTVALUE_EDEFAULT.equals(pHYSICALDEFAULTVALUE);
			case OdxXhtmlPackage.COMPARAM__DATAOBJECTPROPREF:
				return dATAOBJECTPROPREF != null;
			case OdxXhtmlPackage.COMPARAM__CPTYPE:
				return isSetCPTYPE();
			case OdxXhtmlPackage.COMPARAM__DISPLAYLEVEL:
				return isSetDISPLAYLEVEL();
			case OdxXhtmlPackage.COMPARAM__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.COMPARAM__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
			case OdxXhtmlPackage.COMPARAM__PARAMCLASS:
				return PARAMCLASS_EDEFAULT == null ? pARAMCLASS != null : !PARAMCLASS_EDEFAULT.equals(pARAMCLASS);
			case OdxXhtmlPackage.COMPARAM__PDUAPIINDEX:
				return isSetPDUAPIINDEX();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", pHYSICALDEFAULTVALUE: ");
		result.append(pHYSICALDEFAULTVALUE);
		result.append(", cPTYPE: ");
		if (cPTYPEESet) result.append(cPTYPE); else result.append("<unset>");
		result.append(", dISPLAYLEVEL: ");
		if (dISPLAYLEVELESet) result.append(dISPLAYLEVEL); else result.append("<unset>");
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(", pARAMCLASS: ");
		result.append(pARAMCLASS);
		result.append(", pDUAPIINDEX: ");
		if (pDUAPIINDEXESet) result.append(pDUAPIINDEX); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //COMPARAMImpl
