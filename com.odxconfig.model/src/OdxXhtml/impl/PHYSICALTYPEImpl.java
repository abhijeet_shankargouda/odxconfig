/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PHYSICALDATATYPE;
import OdxXhtml.PHYSICALTYPE;
import OdxXhtml.RADIX;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PHYSICALTYPE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PHYSICALTYPEImpl#getPRECISION <em>PRECISION</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALTYPEImpl#getBASEDATATYPE <em>BASEDATATYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.PHYSICALTYPEImpl#getDISPLAYRADIX <em>DISPLAYRADIX</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PHYSICALTYPEImpl extends MinimalEObjectImpl.Container implements PHYSICALTYPE {
	/**
	 * The default value of the '{@link #getPRECISION() <em>PRECISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPRECISION()
	 * @generated
	 * @ordered
	 */
	protected static final long PRECISION_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getPRECISION() <em>PRECISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPRECISION()
	 * @generated
	 * @ordered
	 */
	protected long pRECISION = PRECISION_EDEFAULT;

	/**
	 * This is true if the PRECISION attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean pRECISIONESet;

	/**
	 * The default value of the '{@link #getBASEDATATYPE() <em>BASEDATATYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASEDATATYPE()
	 * @generated
	 * @ordered
	 */
	protected static final PHYSICALDATATYPE BASEDATATYPE_EDEFAULT = PHYSICALDATATYPE.AINT32;

	/**
	 * The cached value of the '{@link #getBASEDATATYPE() <em>BASEDATATYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBASEDATATYPE()
	 * @generated
	 * @ordered
	 */
	protected PHYSICALDATATYPE bASEDATATYPE = BASEDATATYPE_EDEFAULT;

	/**
	 * This is true if the BASEDATATYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bASEDATATYPEESet;

	/**
	 * The default value of the '{@link #getDISPLAYRADIX() <em>DISPLAYRADIX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDISPLAYRADIX()
	 * @generated
	 * @ordered
	 */
	protected static final RADIX DISPLAYRADIX_EDEFAULT = RADIX.DEC;

	/**
	 * The cached value of the '{@link #getDISPLAYRADIX() <em>DISPLAYRADIX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDISPLAYRADIX()
	 * @generated
	 * @ordered
	 */
	protected RADIX dISPLAYRADIX = DISPLAYRADIX_EDEFAULT;

	/**
	 * This is true if the DISPLAYRADIX attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dISPLAYRADIXESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PHYSICALTYPEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPHYSICALTYPE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getPRECISION() {
		return pRECISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPRECISION(long newPRECISION) {
		long oldPRECISION = pRECISION;
		pRECISION = newPRECISION;
		boolean oldPRECISIONESet = pRECISIONESet;
		pRECISIONESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALTYPE__PRECISION, oldPRECISION, pRECISION, !oldPRECISIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetPRECISION() {
		long oldPRECISION = pRECISION;
		boolean oldPRECISIONESet = pRECISIONESet;
		pRECISION = PRECISION_EDEFAULT;
		pRECISIONESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALTYPE__PRECISION, oldPRECISION, PRECISION_EDEFAULT, oldPRECISIONESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetPRECISION() {
		return pRECISIONESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PHYSICALDATATYPE getBASEDATATYPE() {
		return bASEDATATYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBASEDATATYPE(PHYSICALDATATYPE newBASEDATATYPE) {
		PHYSICALDATATYPE oldBASEDATATYPE = bASEDATATYPE;
		bASEDATATYPE = newBASEDATATYPE == null ? BASEDATATYPE_EDEFAULT : newBASEDATATYPE;
		boolean oldBASEDATATYPEESet = bASEDATATYPEESet;
		bASEDATATYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALTYPE__BASEDATATYPE, oldBASEDATATYPE, bASEDATATYPE, !oldBASEDATATYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBASEDATATYPE() {
		PHYSICALDATATYPE oldBASEDATATYPE = bASEDATATYPE;
		boolean oldBASEDATATYPEESet = bASEDATATYPEESet;
		bASEDATATYPE = BASEDATATYPE_EDEFAULT;
		bASEDATATYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALTYPE__BASEDATATYPE, oldBASEDATATYPE, BASEDATATYPE_EDEFAULT, oldBASEDATATYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBASEDATATYPE() {
		return bASEDATATYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RADIX getDISPLAYRADIX() {
		return dISPLAYRADIX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDISPLAYRADIX(RADIX newDISPLAYRADIX) {
		RADIX oldDISPLAYRADIX = dISPLAYRADIX;
		dISPLAYRADIX = newDISPLAYRADIX == null ? DISPLAYRADIX_EDEFAULT : newDISPLAYRADIX;
		boolean oldDISPLAYRADIXESet = dISPLAYRADIXESet;
		dISPLAYRADIXESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PHYSICALTYPE__DISPLAYRADIX, oldDISPLAYRADIX, dISPLAYRADIX, !oldDISPLAYRADIXESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDISPLAYRADIX() {
		RADIX oldDISPLAYRADIX = dISPLAYRADIX;
		boolean oldDISPLAYRADIXESet = dISPLAYRADIXESet;
		dISPLAYRADIX = DISPLAYRADIX_EDEFAULT;
		dISPLAYRADIXESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PHYSICALTYPE__DISPLAYRADIX, oldDISPLAYRADIX, DISPLAYRADIX_EDEFAULT, oldDISPLAYRADIXESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDISPLAYRADIX() {
		return dISPLAYRADIXESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALTYPE__PRECISION:
				return getPRECISION();
			case OdxXhtmlPackage.PHYSICALTYPE__BASEDATATYPE:
				return getBASEDATATYPE();
			case OdxXhtmlPackage.PHYSICALTYPE__DISPLAYRADIX:
				return getDISPLAYRADIX();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALTYPE__PRECISION:
				setPRECISION((Long)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALTYPE__BASEDATATYPE:
				setBASEDATATYPE((PHYSICALDATATYPE)newValue);
				return;
			case OdxXhtmlPackage.PHYSICALTYPE__DISPLAYRADIX:
				setDISPLAYRADIX((RADIX)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALTYPE__PRECISION:
				unsetPRECISION();
				return;
			case OdxXhtmlPackage.PHYSICALTYPE__BASEDATATYPE:
				unsetBASEDATATYPE();
				return;
			case OdxXhtmlPackage.PHYSICALTYPE__DISPLAYRADIX:
				unsetDISPLAYRADIX();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PHYSICALTYPE__PRECISION:
				return isSetPRECISION();
			case OdxXhtmlPackage.PHYSICALTYPE__BASEDATATYPE:
				return isSetBASEDATATYPE();
			case OdxXhtmlPackage.PHYSICALTYPE__DISPLAYRADIX:
				return isSetDISPLAYRADIX();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (pRECISION: ");
		if (pRECISIONESet) result.append(pRECISION); else result.append("<unset>");
		result.append(", bASEDATATYPE: ");
		if (bASEDATATYPEESet) result.append(bASEDATATYPE); else result.append("<unset>");
		result.append(", dISPLAYRADIX: ");
		if (dISPLAYRADIXESet) result.append(dISPLAYRADIX); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //PHYSICALTYPEImpl
