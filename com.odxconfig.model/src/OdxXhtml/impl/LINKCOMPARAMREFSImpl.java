/**
 */
package OdxXhtml.impl;

import OdxXhtml.LINKCOMPARAMREF;
import OdxXhtml.LINKCOMPARAMREFS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LINKCOMPARAMREFS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.LINKCOMPARAMREFSImpl#getLINKCOMPARAMREF <em>LINKCOMPARAMREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LINKCOMPARAMREFSImpl extends MinimalEObjectImpl.Container implements LINKCOMPARAMREFS {
	/**
	 * The cached value of the '{@link #getLINKCOMPARAMREF() <em>LINKCOMPARAMREF</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLINKCOMPARAMREF()
	 * @generated
	 * @ordered
	 */
	protected EList<LINKCOMPARAMREF> lINKCOMPARAMREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LINKCOMPARAMREFSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getLINKCOMPARAMREFS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LINKCOMPARAMREF> getLINKCOMPARAMREF() {
		if (lINKCOMPARAMREF == null) {
			lINKCOMPARAMREF = new EObjectContainmentEList<LINKCOMPARAMREF>(LINKCOMPARAMREF.class, this, OdxXhtmlPackage.LINKCOMPARAMREFS__LINKCOMPARAMREF);
		}
		return lINKCOMPARAMREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREFS__LINKCOMPARAMREF:
				return ((InternalEList<?>)getLINKCOMPARAMREF()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREFS__LINKCOMPARAMREF:
				return getLINKCOMPARAMREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREFS__LINKCOMPARAMREF:
				getLINKCOMPARAMREF().clear();
				getLINKCOMPARAMREF().addAll((Collection<? extends LINKCOMPARAMREF>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREFS__LINKCOMPARAMREF:
				getLINKCOMPARAMREF().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.LINKCOMPARAMREFS__LINKCOMPARAMREF:
				return lINKCOMPARAMREF != null && !lINKCOMPARAMREF.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LINKCOMPARAMREFSImpl
