/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROJECTIDENT;
import OdxXhtml.PROJIDENTTYPE;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PROJECTIDENT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PROJECTIDENTImpl#getNAME <em>NAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROJECTIDENTImpl#getVALUE <em>VALUE</em>}</li>
 *   <li>{@link OdxXhtml.impl.PROJECTIDENTImpl#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PROJECTIDENTImpl extends MinimalEObjectImpl.Container implements PROJECTIDENT {
	/**
	 * The default value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNAME() <em>NAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNAME()
	 * @generated
	 * @ordered
	 */
	protected String nAME = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVALUE() <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUE()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVALUE() <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVALUE()
	 * @generated
	 * @ordered
	 */
	protected String vALUE = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final PROJIDENTTYPE TYPE_EDEFAULT = PROJIDENTTYPE.DOC;

	/**
	 * The cached value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected PROJIDENTTYPE tYPE = TYPE_EDEFAULT;

	/**
	 * This is true if the TYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tYPEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PROJECTIDENTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPROJECTIDENT();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNAME() {
		return nAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNAME(String newNAME) {
		String oldNAME = nAME;
		nAME = newNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROJECTIDENT__NAME, oldNAME, nAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVALUE() {
		return vALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVALUE(String newVALUE) {
		String oldVALUE = vALUE;
		vALUE = newVALUE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROJECTIDENT__VALUE, oldVALUE, vALUE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROJIDENTTYPE getTYPE() {
		return tYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTYPE(PROJIDENTTYPE newTYPE) {
		PROJIDENTTYPE oldTYPE = tYPE;
		tYPE = newTYPE == null ? TYPE_EDEFAULT : newTYPE;
		boolean oldTYPEESet = tYPEESet;
		tYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PROJECTIDENT__TYPE, oldTYPE, tYPE, !oldTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetTYPE() {
		PROJIDENTTYPE oldTYPE = tYPE;
		boolean oldTYPEESet = tYPEESet;
		tYPE = TYPE_EDEFAULT;
		tYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PROJECTIDENT__TYPE, oldTYPE, TYPE_EDEFAULT, oldTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetTYPE() {
		return tYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTIDENT__NAME:
				return getNAME();
			case OdxXhtmlPackage.PROJECTIDENT__VALUE:
				return getVALUE();
			case OdxXhtmlPackage.PROJECTIDENT__TYPE:
				return getTYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTIDENT__NAME:
				setNAME((String)newValue);
				return;
			case OdxXhtmlPackage.PROJECTIDENT__VALUE:
				setVALUE((String)newValue);
				return;
			case OdxXhtmlPackage.PROJECTIDENT__TYPE:
				setTYPE((PROJIDENTTYPE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTIDENT__NAME:
				setNAME(NAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.PROJECTIDENT__VALUE:
				setVALUE(VALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.PROJECTIDENT__TYPE:
				unsetTYPE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTIDENT__NAME:
				return NAME_EDEFAULT == null ? nAME != null : !NAME_EDEFAULT.equals(nAME);
			case OdxXhtmlPackage.PROJECTIDENT__VALUE:
				return VALUE_EDEFAULT == null ? vALUE != null : !VALUE_EDEFAULT.equals(vALUE);
			case OdxXhtmlPackage.PROJECTIDENT__TYPE:
				return isSetTYPE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (nAME: ");
		result.append(nAME);
		result.append(", vALUE: ");
		result.append(vALUE);
		result.append(", tYPE: ");
		if (tYPEESet) result.append(tYPE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //PROJECTIDENTImpl
