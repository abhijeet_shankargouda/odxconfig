/**
 */
package OdxXhtml.impl;

import OdxXhtml.INTERNALCONSTR;
import OdxXhtml.LIMIT;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.SCALECONSTRS;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>INTERNALCONSTR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.INTERNALCONSTRImpl#getLOWERLIMIT <em>LOWERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.impl.INTERNALCONSTRImpl#getUPPERLIMIT <em>UPPERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.impl.INTERNALCONSTRImpl#getSCALECONSTRS <em>SCALECONSTRS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class INTERNALCONSTRImpl extends MinimalEObjectImpl.Container implements INTERNALCONSTR {
	/**
	 * The cached value of the '{@link #getLOWERLIMIT() <em>LOWERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLOWERLIMIT()
	 * @generated
	 * @ordered
	 */
	protected LIMIT lOWERLIMIT;

	/**
	 * The cached value of the '{@link #getUPPERLIMIT() <em>UPPERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUPPERLIMIT()
	 * @generated
	 * @ordered
	 */
	protected LIMIT uPPERLIMIT;

	/**
	 * The cached value of the '{@link #getSCALECONSTRS() <em>SCALECONSTRS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSCALECONSTRS()
	 * @generated
	 * @ordered
	 */
	protected SCALECONSTRS sCALECONSTRS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INTERNALCONSTRImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getINTERNALCONSTR();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LIMIT getLOWERLIMIT() {
		return lOWERLIMIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLOWERLIMIT(LIMIT newLOWERLIMIT, NotificationChain msgs) {
		LIMIT oldLOWERLIMIT = lOWERLIMIT;
		lOWERLIMIT = newLOWERLIMIT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.INTERNALCONSTR__LOWERLIMIT, oldLOWERLIMIT, newLOWERLIMIT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLOWERLIMIT(LIMIT newLOWERLIMIT) {
		if (newLOWERLIMIT != lOWERLIMIT) {
			NotificationChain msgs = null;
			if (lOWERLIMIT != null)
				msgs = ((InternalEObject)lOWERLIMIT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.INTERNALCONSTR__LOWERLIMIT, null, msgs);
			if (newLOWERLIMIT != null)
				msgs = ((InternalEObject)newLOWERLIMIT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.INTERNALCONSTR__LOWERLIMIT, null, msgs);
			msgs = basicSetLOWERLIMIT(newLOWERLIMIT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.INTERNALCONSTR__LOWERLIMIT, newLOWERLIMIT, newLOWERLIMIT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LIMIT getUPPERLIMIT() {
		return uPPERLIMIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUPPERLIMIT(LIMIT newUPPERLIMIT, NotificationChain msgs) {
		LIMIT oldUPPERLIMIT = uPPERLIMIT;
		uPPERLIMIT = newUPPERLIMIT;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.INTERNALCONSTR__UPPERLIMIT, oldUPPERLIMIT, newUPPERLIMIT);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUPPERLIMIT(LIMIT newUPPERLIMIT) {
		if (newUPPERLIMIT != uPPERLIMIT) {
			NotificationChain msgs = null;
			if (uPPERLIMIT != null)
				msgs = ((InternalEObject)uPPERLIMIT).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.INTERNALCONSTR__UPPERLIMIT, null, msgs);
			if (newUPPERLIMIT != null)
				msgs = ((InternalEObject)newUPPERLIMIT).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.INTERNALCONSTR__UPPERLIMIT, null, msgs);
			msgs = basicSetUPPERLIMIT(newUPPERLIMIT, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.INTERNALCONSTR__UPPERLIMIT, newUPPERLIMIT, newUPPERLIMIT));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SCALECONSTRS getSCALECONSTRS() {
		return sCALECONSTRS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSCALECONSTRS(SCALECONSTRS newSCALECONSTRS, NotificationChain msgs) {
		SCALECONSTRS oldSCALECONSTRS = sCALECONSTRS;
		sCALECONSTRS = newSCALECONSTRS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.INTERNALCONSTR__SCALECONSTRS, oldSCALECONSTRS, newSCALECONSTRS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSCALECONSTRS(SCALECONSTRS newSCALECONSTRS) {
		if (newSCALECONSTRS != sCALECONSTRS) {
			NotificationChain msgs = null;
			if (sCALECONSTRS != null)
				msgs = ((InternalEObject)sCALECONSTRS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.INTERNALCONSTR__SCALECONSTRS, null, msgs);
			if (newSCALECONSTRS != null)
				msgs = ((InternalEObject)newSCALECONSTRS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.INTERNALCONSTR__SCALECONSTRS, null, msgs);
			msgs = basicSetSCALECONSTRS(newSCALECONSTRS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.INTERNALCONSTR__SCALECONSTRS, newSCALECONSTRS, newSCALECONSTRS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.INTERNALCONSTR__LOWERLIMIT:
				return basicSetLOWERLIMIT(null, msgs);
			case OdxXhtmlPackage.INTERNALCONSTR__UPPERLIMIT:
				return basicSetUPPERLIMIT(null, msgs);
			case OdxXhtmlPackage.INTERNALCONSTR__SCALECONSTRS:
				return basicSetSCALECONSTRS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.INTERNALCONSTR__LOWERLIMIT:
				return getLOWERLIMIT();
			case OdxXhtmlPackage.INTERNALCONSTR__UPPERLIMIT:
				return getUPPERLIMIT();
			case OdxXhtmlPackage.INTERNALCONSTR__SCALECONSTRS:
				return getSCALECONSTRS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.INTERNALCONSTR__LOWERLIMIT:
				setLOWERLIMIT((LIMIT)newValue);
				return;
			case OdxXhtmlPackage.INTERNALCONSTR__UPPERLIMIT:
				setUPPERLIMIT((LIMIT)newValue);
				return;
			case OdxXhtmlPackage.INTERNALCONSTR__SCALECONSTRS:
				setSCALECONSTRS((SCALECONSTRS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INTERNALCONSTR__LOWERLIMIT:
				setLOWERLIMIT((LIMIT)null);
				return;
			case OdxXhtmlPackage.INTERNALCONSTR__UPPERLIMIT:
				setUPPERLIMIT((LIMIT)null);
				return;
			case OdxXhtmlPackage.INTERNALCONSTR__SCALECONSTRS:
				setSCALECONSTRS((SCALECONSTRS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.INTERNALCONSTR__LOWERLIMIT:
				return lOWERLIMIT != null;
			case OdxXhtmlPackage.INTERNALCONSTR__UPPERLIMIT:
				return uPPERLIMIT != null;
			case OdxXhtmlPackage.INTERNALCONSTR__SCALECONSTRS:
				return sCALECONSTRS != null;
		}
		return super.eIsSet(featureID);
	}

} //INTERNALCONSTRImpl
