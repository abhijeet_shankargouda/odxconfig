/**
 */
package OdxXhtml.impl;

import OdxXhtml.MATCHINGREQUESTPARAM;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MATCHINGREQUESTPARAM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MATCHINGREQUESTPARAMImpl#getREQUESTBYTEPOS <em>REQUESTBYTEPOS</em>}</li>
 *   <li>{@link OdxXhtml.impl.MATCHINGREQUESTPARAMImpl#getBYTELENGTH <em>BYTELENGTH</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MATCHINGREQUESTPARAMImpl extends POSITIONABLEPARAMImpl implements MATCHINGREQUESTPARAM {
	/**
	 * The default value of the '{@link #getREQUESTBYTEPOS() <em>REQUESTBYTEPOS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREQUESTBYTEPOS()
	 * @generated
	 * @ordered
	 */
	protected static final int REQUESTBYTEPOS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getREQUESTBYTEPOS() <em>REQUESTBYTEPOS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREQUESTBYTEPOS()
	 * @generated
	 * @ordered
	 */
	protected int rEQUESTBYTEPOS = REQUESTBYTEPOS_EDEFAULT;

	/**
	 * This is true if the REQUESTBYTEPOS attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean rEQUESTBYTEPOSESet;

	/**
	 * The default value of the '{@link #getBYTELENGTH() <em>BYTELENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTELENGTH()
	 * @generated
	 * @ordered
	 */
	protected static final long BYTELENGTH_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getBYTELENGTH() <em>BYTELENGTH</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBYTELENGTH()
	 * @generated
	 * @ordered
	 */
	protected long bYTELENGTH = BYTELENGTH_EDEFAULT;

	/**
	 * This is true if the BYTELENGTH attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean bYTELENGTHESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MATCHINGREQUESTPARAMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMATCHINGREQUESTPARAM();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getREQUESTBYTEPOS() {
		return rEQUESTBYTEPOS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREQUESTBYTEPOS(int newREQUESTBYTEPOS) {
		int oldREQUESTBYTEPOS = rEQUESTBYTEPOS;
		rEQUESTBYTEPOS = newREQUESTBYTEPOS;
		boolean oldREQUESTBYTEPOSESet = rEQUESTBYTEPOSESet;
		rEQUESTBYTEPOSESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGREQUESTPARAM__REQUESTBYTEPOS, oldREQUESTBYTEPOS, rEQUESTBYTEPOS, !oldREQUESTBYTEPOSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetREQUESTBYTEPOS() {
		int oldREQUESTBYTEPOS = rEQUESTBYTEPOS;
		boolean oldREQUESTBYTEPOSESet = rEQUESTBYTEPOSESet;
		rEQUESTBYTEPOS = REQUESTBYTEPOS_EDEFAULT;
		rEQUESTBYTEPOSESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MATCHINGREQUESTPARAM__REQUESTBYTEPOS, oldREQUESTBYTEPOS, REQUESTBYTEPOS_EDEFAULT, oldREQUESTBYTEPOSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetREQUESTBYTEPOS() {
		return rEQUESTBYTEPOSESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getBYTELENGTH() {
		return bYTELENGTH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBYTELENGTH(long newBYTELENGTH) {
		long oldBYTELENGTH = bYTELENGTH;
		bYTELENGTH = newBYTELENGTH;
		boolean oldBYTELENGTHESet = bYTELENGTHESet;
		bYTELENGTHESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MATCHINGREQUESTPARAM__BYTELENGTH, oldBYTELENGTH, bYTELENGTH, !oldBYTELENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBYTELENGTH() {
		long oldBYTELENGTH = bYTELENGTH;
		boolean oldBYTELENGTHESet = bYTELENGTHESet;
		bYTELENGTH = BYTELENGTH_EDEFAULT;
		bYTELENGTHESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.MATCHINGREQUESTPARAM__BYTELENGTH, oldBYTELENGTH, BYTELENGTH_EDEFAULT, oldBYTELENGTHESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBYTELENGTH() {
		return bYTELENGTHESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM__REQUESTBYTEPOS:
				return getREQUESTBYTEPOS();
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM__BYTELENGTH:
				return getBYTELENGTH();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM__REQUESTBYTEPOS:
				setREQUESTBYTEPOS((Integer)newValue);
				return;
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM__BYTELENGTH:
				setBYTELENGTH((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM__REQUESTBYTEPOS:
				unsetREQUESTBYTEPOS();
				return;
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM__BYTELENGTH:
				unsetBYTELENGTH();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM__REQUESTBYTEPOS:
				return isSetREQUESTBYTEPOS();
			case OdxXhtmlPackage.MATCHINGREQUESTPARAM__BYTELENGTH:
				return isSetBYTELENGTH();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (rEQUESTBYTEPOS: ");
		if (rEQUESTBYTEPOSESet) result.append(rEQUESTBYTEPOS); else result.append("<unset>");
		result.append(", bYTELENGTH: ");
		if (bYTELENGTHESet) result.append(bYTELENGTH); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MATCHINGREQUESTPARAMImpl
