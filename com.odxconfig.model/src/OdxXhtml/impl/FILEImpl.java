/**
 */
package OdxXhtml.impl;

import OdxXhtml.FILE;
import OdxXhtml.OdxXhtmlPackage;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FILE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.FILEImpl#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.impl.FILEImpl#getCREATIONDATE <em>CREATIONDATE</em>}</li>
 *   <li>{@link OdxXhtml.impl.FILEImpl#getCREATOR <em>CREATOR</em>}</li>
 *   <li>{@link OdxXhtml.impl.FILEImpl#getMIMETYPE <em>MIMETYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FILEImpl extends MinimalEObjectImpl.Container implements FILE {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCREATIONDATE() <em>CREATIONDATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATIONDATE()
	 * @generated
	 * @ordered
	 */
	protected static final XMLGregorianCalendar CREATIONDATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCREATIONDATE() <em>CREATIONDATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATIONDATE()
	 * @generated
	 * @ordered
	 */
	protected XMLGregorianCalendar cREATIONDATE = CREATIONDATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCREATOR() <em>CREATOR</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATOR()
	 * @generated
	 * @ordered
	 */
	protected static final String CREATOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCREATOR() <em>CREATOR</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCREATOR()
	 * @generated
	 * @ordered
	 */
	protected String cREATOR = CREATOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getMIMETYPE() <em>MIMETYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMIMETYPE()
	 * @generated
	 * @ordered
	 */
	protected static final String MIMETYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMIMETYPE() <em>MIMETYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMIMETYPE()
	 * @generated
	 * @ordered
	 */
	protected String mIMETYPE = MIMETYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FILEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getFILE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FILE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public XMLGregorianCalendar getCREATIONDATE() {
		return cREATIONDATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCREATIONDATE(XMLGregorianCalendar newCREATIONDATE) {
		XMLGregorianCalendar oldCREATIONDATE = cREATIONDATE;
		cREATIONDATE = newCREATIONDATE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FILE__CREATIONDATE, oldCREATIONDATE, cREATIONDATE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCREATOR() {
		return cREATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCREATOR(String newCREATOR) {
		String oldCREATOR = cREATOR;
		cREATOR = newCREATOR;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FILE__CREATOR, oldCREATOR, cREATOR));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMIMETYPE() {
		return mIMETYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMIMETYPE(String newMIMETYPE) {
		String oldMIMETYPE = mIMETYPE;
		mIMETYPE = newMIMETYPE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.FILE__MIMETYPE, oldMIMETYPE, mIMETYPE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.FILE__VALUE:
				return getValue();
			case OdxXhtmlPackage.FILE__CREATIONDATE:
				return getCREATIONDATE();
			case OdxXhtmlPackage.FILE__CREATOR:
				return getCREATOR();
			case OdxXhtmlPackage.FILE__MIMETYPE:
				return getMIMETYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.FILE__VALUE:
				setValue((String)newValue);
				return;
			case OdxXhtmlPackage.FILE__CREATIONDATE:
				setCREATIONDATE((XMLGregorianCalendar)newValue);
				return;
			case OdxXhtmlPackage.FILE__CREATOR:
				setCREATOR((String)newValue);
				return;
			case OdxXhtmlPackage.FILE__MIMETYPE:
				setMIMETYPE((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FILE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case OdxXhtmlPackage.FILE__CREATIONDATE:
				setCREATIONDATE(CREATIONDATE_EDEFAULT);
				return;
			case OdxXhtmlPackage.FILE__CREATOR:
				setCREATOR(CREATOR_EDEFAULT);
				return;
			case OdxXhtmlPackage.FILE__MIMETYPE:
				setMIMETYPE(MIMETYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.FILE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case OdxXhtmlPackage.FILE__CREATIONDATE:
				return CREATIONDATE_EDEFAULT == null ? cREATIONDATE != null : !CREATIONDATE_EDEFAULT.equals(cREATIONDATE);
			case OdxXhtmlPackage.FILE__CREATOR:
				return CREATOR_EDEFAULT == null ? cREATOR != null : !CREATOR_EDEFAULT.equals(cREATOR);
			case OdxXhtmlPackage.FILE__MIMETYPE:
				return MIMETYPE_EDEFAULT == null ? mIMETYPE != null : !MIMETYPE_EDEFAULT.equals(mIMETYPE);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", cREATIONDATE: ");
		result.append(cREATIONDATE);
		result.append(", cREATOR: ");
		result.append(cREATOR);
		result.append(", mIMETYPE: ");
		result.append(mIMETYPE);
		result.append(')');
		return result.toString();
	}

} //FILEImpl
