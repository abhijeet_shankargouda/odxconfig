/**
 */
package OdxXhtml.impl;

import OdxXhtml.DYNDEFINEDSPEC;
import OdxXhtml.DYNIDDEFMODEINFOS;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DYNDEFINEDSPEC</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DYNDEFINEDSPECImpl#getDYNIDDEFMODEINFOS <em>DYNIDDEFMODEINFOS</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DYNDEFINEDSPECImpl extends MinimalEObjectImpl.Container implements DYNDEFINEDSPEC {
	/**
	 * The cached value of the '{@link #getDYNIDDEFMODEINFOS() <em>DYNIDDEFMODEINFOS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDYNIDDEFMODEINFOS()
	 * @generated
	 * @ordered
	 */
	protected DYNIDDEFMODEINFOS dYNIDDEFMODEINFOS;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DYNDEFINEDSPECImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDYNDEFINEDSPEC();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DYNIDDEFMODEINFOS getDYNIDDEFMODEINFOS() {
		return dYNIDDEFMODEINFOS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDYNIDDEFMODEINFOS(DYNIDDEFMODEINFOS newDYNIDDEFMODEINFOS, NotificationChain msgs) {
		DYNIDDEFMODEINFOS oldDYNIDDEFMODEINFOS = dYNIDDEFMODEINFOS;
		dYNIDDEFMODEINFOS = newDYNIDDEFMODEINFOS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNDEFINEDSPEC__DYNIDDEFMODEINFOS, oldDYNIDDEFMODEINFOS, newDYNIDDEFMODEINFOS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDYNIDDEFMODEINFOS(DYNIDDEFMODEINFOS newDYNIDDEFMODEINFOS) {
		if (newDYNIDDEFMODEINFOS != dYNIDDEFMODEINFOS) {
			NotificationChain msgs = null;
			if (dYNIDDEFMODEINFOS != null)
				msgs = ((InternalEObject)dYNIDDEFMODEINFOS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNDEFINEDSPEC__DYNIDDEFMODEINFOS, null, msgs);
			if (newDYNIDDEFMODEINFOS != null)
				msgs = ((InternalEObject)newDYNIDDEFMODEINFOS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DYNDEFINEDSPEC__DYNIDDEFMODEINFOS, null, msgs);
			msgs = basicSetDYNIDDEFMODEINFOS(newDYNIDDEFMODEINFOS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DYNDEFINEDSPEC__DYNIDDEFMODEINFOS, newDYNIDDEFMODEINFOS, newDYNIDDEFMODEINFOS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNDEFINEDSPEC__DYNIDDEFMODEINFOS:
				return basicSetDYNIDDEFMODEINFOS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNDEFINEDSPEC__DYNIDDEFMODEINFOS:
				return getDYNIDDEFMODEINFOS();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNDEFINEDSPEC__DYNIDDEFMODEINFOS:
				setDYNIDDEFMODEINFOS((DYNIDDEFMODEINFOS)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNDEFINEDSPEC__DYNIDDEFMODEINFOS:
				setDYNIDDEFMODEINFOS((DYNIDDEFMODEINFOS)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DYNDEFINEDSPEC__DYNIDDEFMODEINFOS:
				return dYNIDDEFMODEINFOS != null;
		}
		return super.eIsSet(featureID);
	}

} //DYNDEFINEDSPECImpl
