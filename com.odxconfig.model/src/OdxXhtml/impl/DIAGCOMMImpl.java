/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.AUDIENCE;
import OdxXhtml.COMPARAMREFS;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.DIAGCLASSTYPE;
import OdxXhtml.DIAGCOMM;
import OdxXhtml.FUNCTCLASSREFS;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROTOCOLSNREFS;
import OdxXhtml.RELATEDDIAGCOMMREFS;
import OdxXhtml.SDGS;
import OdxXhtml.TEXT;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DIAGCOMM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getCOMPARAMREFS <em>COMPARAMREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getFUNCTCLASSREFS <em>FUNCTCLASSREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getAUDIENCE <em>AUDIENCE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getPROTOCOLSNREFS <em>PROTOCOLSNREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getRELATEDDIAGCOMMREFS <em>RELATEDDIAGCOMMREFS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getDIAGNOSTICCLASS <em>DIAGNOSTICCLASS</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#isISEXECUTABLE <em>ISEXECUTABLE</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#isISFINAL <em>ISFINAL</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#isISMANDATORY <em>ISMANDATORY</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getSECURITYACCESSLEVEL <em>SECURITYACCESSLEVEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.DIAGCOMMImpl#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DIAGCOMMImpl extends MinimalEObjectImpl.Container implements DIAGCOMM {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getSDGS() <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSDGS()
	 * @generated
	 * @ordered
	 */
	protected SDGS sDGS;

	/**
	 * The cached value of the '{@link #getCOMPARAMREFS() <em>COMPARAMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPARAMREFS()
	 * @generated
	 * @ordered
	 */
	protected COMPARAMREFS cOMPARAMREFS;

	/**
	 * The cached value of the '{@link #getFUNCTCLASSREFS() <em>FUNCTCLASSREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFUNCTCLASSREFS()
	 * @generated
	 * @ordered
	 */
	protected FUNCTCLASSREFS fUNCTCLASSREFS;

	/**
	 * The cached value of the '{@link #getAUDIENCE() <em>AUDIENCE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUDIENCE()
	 * @generated
	 * @ordered
	 */
	protected AUDIENCE aUDIENCE;

	/**
	 * The cached value of the '{@link #getPROTOCOLSNREFS() <em>PROTOCOLSNREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROTOCOLSNREFS()
	 * @generated
	 * @ordered
	 */
	protected PROTOCOLSNREFS pROTOCOLSNREFS;

	/**
	 * The cached value of the '{@link #getRELATEDDIAGCOMMREFS() <em>RELATEDDIAGCOMMREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRELATEDDIAGCOMMREFS()
	 * @generated
	 * @ordered
	 */
	protected RELATEDDIAGCOMMREFS rELATEDDIAGCOMMREFS;

	/**
	 * The default value of the '{@link #getDIAGNOSTICCLASS() <em>DIAGNOSTICCLASS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGNOSTICCLASS()
	 * @generated
	 * @ordered
	 */
	protected static final DIAGCLASSTYPE DIAGNOSTICCLASS_EDEFAULT = DIAGCLASSTYPE.STARTCOMM;

	/**
	 * The cached value of the '{@link #getDIAGNOSTICCLASS() <em>DIAGNOSTICCLASS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDIAGNOSTICCLASS()
	 * @generated
	 * @ordered
	 */
	protected DIAGCLASSTYPE dIAGNOSTICCLASS = DIAGNOSTICCLASS_EDEFAULT;

	/**
	 * This is true if the DIAGNOSTICCLASS attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dIAGNOSTICCLASSESet;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #isISEXECUTABLE() <em>ISEXECUTABLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISEXECUTABLE()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISEXECUTABLE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isISEXECUTABLE() <em>ISEXECUTABLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISEXECUTABLE()
	 * @generated
	 * @ordered
	 */
	protected boolean iSEXECUTABLE = ISEXECUTABLE_EDEFAULT;

	/**
	 * This is true if the ISEXECUTABLE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSEXECUTABLEESet;

	/**
	 * The default value of the '{@link #isISFINAL() <em>ISFINAL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISFINAL()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISFINAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISFINAL() <em>ISFINAL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISFINAL()
	 * @generated
	 * @ordered
	 */
	protected boolean iSFINAL = ISFINAL_EDEFAULT;

	/**
	 * This is true if the ISFINAL attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSFINALESet;

	/**
	 * The default value of the '{@link #isISMANDATORY() <em>ISMANDATORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISMANDATORY()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ISMANDATORY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isISMANDATORY() <em>ISMANDATORY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isISMANDATORY()
	 * @generated
	 * @ordered
	 */
	protected boolean iSMANDATORY = ISMANDATORY_EDEFAULT;

	/**
	 * This is true if the ISMANDATORY attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iSMANDATORYESet;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSECURITYACCESSLEVEL() <em>SECURITYACCESSLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSECURITYACCESSLEVEL()
	 * @generated
	 * @ordered
	 */
	protected static final long SECURITYACCESSLEVEL_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getSECURITYACCESSLEVEL() <em>SECURITYACCESSLEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSECURITYACCESSLEVEL()
	 * @generated
	 * @ordered
	 */
	protected long sECURITYACCESSLEVEL = SECURITYACCESSLEVEL_EDEFAULT;

	/**
	 * This is true if the SECURITYACCESSLEVEL attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sECURITYACCESSLEVELESet;

	/**
	 * The default value of the '{@link #getSEMANTIC() <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSEMANTIC()
	 * @generated
	 * @ordered
	 */
	protected static final String SEMANTIC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSEMANTIC() <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSEMANTIC()
	 * @generated
	 * @ordered
	 */
	protected String sEMANTIC = SEMANTIC_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DIAGCOMMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDIAGCOMM();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SDGS getSDGS() {
		return sDGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSDGS(SDGS newSDGS, NotificationChain msgs) {
		SDGS oldSDGS = sDGS;
		sDGS = newSDGS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__SDGS, oldSDGS, newSDGS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSDGS(SDGS newSDGS) {
		if (newSDGS != sDGS) {
			NotificationChain msgs = null;
			if (sDGS != null)
				msgs = ((InternalEObject)sDGS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__SDGS, null, msgs);
			if (newSDGS != null)
				msgs = ((InternalEObject)newSDGS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__SDGS, null, msgs);
			msgs = basicSetSDGS(newSDGS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__SDGS, newSDGS, newSDGS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public COMPARAMREFS getCOMPARAMREFS() {
		return cOMPARAMREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPARAMREFS(COMPARAMREFS newCOMPARAMREFS, NotificationChain msgs) {
		COMPARAMREFS oldCOMPARAMREFS = cOMPARAMREFS;
		cOMPARAMREFS = newCOMPARAMREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__COMPARAMREFS, oldCOMPARAMREFS, newCOMPARAMREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPARAMREFS(COMPARAMREFS newCOMPARAMREFS) {
		if (newCOMPARAMREFS != cOMPARAMREFS) {
			NotificationChain msgs = null;
			if (cOMPARAMREFS != null)
				msgs = ((InternalEObject)cOMPARAMREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__COMPARAMREFS, null, msgs);
			if (newCOMPARAMREFS != null)
				msgs = ((InternalEObject)newCOMPARAMREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__COMPARAMREFS, null, msgs);
			msgs = basicSetCOMPARAMREFS(newCOMPARAMREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__COMPARAMREFS, newCOMPARAMREFS, newCOMPARAMREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FUNCTCLASSREFS getFUNCTCLASSREFS() {
		return fUNCTCLASSREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFUNCTCLASSREFS(FUNCTCLASSREFS newFUNCTCLASSREFS, NotificationChain msgs) {
		FUNCTCLASSREFS oldFUNCTCLASSREFS = fUNCTCLASSREFS;
		fUNCTCLASSREFS = newFUNCTCLASSREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__FUNCTCLASSREFS, oldFUNCTCLASSREFS, newFUNCTCLASSREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFUNCTCLASSREFS(FUNCTCLASSREFS newFUNCTCLASSREFS) {
		if (newFUNCTCLASSREFS != fUNCTCLASSREFS) {
			NotificationChain msgs = null;
			if (fUNCTCLASSREFS != null)
				msgs = ((InternalEObject)fUNCTCLASSREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__FUNCTCLASSREFS, null, msgs);
			if (newFUNCTCLASSREFS != null)
				msgs = ((InternalEObject)newFUNCTCLASSREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__FUNCTCLASSREFS, null, msgs);
			msgs = basicSetFUNCTCLASSREFS(newFUNCTCLASSREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__FUNCTCLASSREFS, newFUNCTCLASSREFS, newFUNCTCLASSREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AUDIENCE getAUDIENCE() {
		return aUDIENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAUDIENCE(AUDIENCE newAUDIENCE, NotificationChain msgs) {
		AUDIENCE oldAUDIENCE = aUDIENCE;
		aUDIENCE = newAUDIENCE;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__AUDIENCE, oldAUDIENCE, newAUDIENCE);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAUDIENCE(AUDIENCE newAUDIENCE) {
		if (newAUDIENCE != aUDIENCE) {
			NotificationChain msgs = null;
			if (aUDIENCE != null)
				msgs = ((InternalEObject)aUDIENCE).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__AUDIENCE, null, msgs);
			if (newAUDIENCE != null)
				msgs = ((InternalEObject)newAUDIENCE).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__AUDIENCE, null, msgs);
			msgs = basicSetAUDIENCE(newAUDIENCE, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__AUDIENCE, newAUDIENCE, newAUDIENCE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PROTOCOLSNREFS getPROTOCOLSNREFS() {
		return pROTOCOLSNREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPROTOCOLSNREFS(PROTOCOLSNREFS newPROTOCOLSNREFS, NotificationChain msgs) {
		PROTOCOLSNREFS oldPROTOCOLSNREFS = pROTOCOLSNREFS;
		pROTOCOLSNREFS = newPROTOCOLSNREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__PROTOCOLSNREFS, oldPROTOCOLSNREFS, newPROTOCOLSNREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPROTOCOLSNREFS(PROTOCOLSNREFS newPROTOCOLSNREFS) {
		if (newPROTOCOLSNREFS != pROTOCOLSNREFS) {
			NotificationChain msgs = null;
			if (pROTOCOLSNREFS != null)
				msgs = ((InternalEObject)pROTOCOLSNREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__PROTOCOLSNREFS, null, msgs);
			if (newPROTOCOLSNREFS != null)
				msgs = ((InternalEObject)newPROTOCOLSNREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__PROTOCOLSNREFS, null, msgs);
			msgs = basicSetPROTOCOLSNREFS(newPROTOCOLSNREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__PROTOCOLSNREFS, newPROTOCOLSNREFS, newPROTOCOLSNREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RELATEDDIAGCOMMREFS getRELATEDDIAGCOMMREFS() {
		return rELATEDDIAGCOMMREFS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRELATEDDIAGCOMMREFS(RELATEDDIAGCOMMREFS newRELATEDDIAGCOMMREFS, NotificationChain msgs) {
		RELATEDDIAGCOMMREFS oldRELATEDDIAGCOMMREFS = rELATEDDIAGCOMMREFS;
		rELATEDDIAGCOMMREFS = newRELATEDDIAGCOMMREFS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__RELATEDDIAGCOMMREFS, oldRELATEDDIAGCOMMREFS, newRELATEDDIAGCOMMREFS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRELATEDDIAGCOMMREFS(RELATEDDIAGCOMMREFS newRELATEDDIAGCOMMREFS) {
		if (newRELATEDDIAGCOMMREFS != rELATEDDIAGCOMMREFS) {
			NotificationChain msgs = null;
			if (rELATEDDIAGCOMMREFS != null)
				msgs = ((InternalEObject)rELATEDDIAGCOMMREFS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__RELATEDDIAGCOMMREFS, null, msgs);
			if (newRELATEDDIAGCOMMREFS != null)
				msgs = ((InternalEObject)newRELATEDDIAGCOMMREFS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.DIAGCOMM__RELATEDDIAGCOMMREFS, null, msgs);
			msgs = basicSetRELATEDDIAGCOMMREFS(newRELATEDDIAGCOMMREFS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__RELATEDDIAGCOMMREFS, newRELATEDDIAGCOMMREFS, newRELATEDDIAGCOMMREFS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DIAGCLASSTYPE getDIAGNOSTICCLASS() {
		return dIAGNOSTICCLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDIAGNOSTICCLASS(DIAGCLASSTYPE newDIAGNOSTICCLASS) {
		DIAGCLASSTYPE oldDIAGNOSTICCLASS = dIAGNOSTICCLASS;
		dIAGNOSTICCLASS = newDIAGNOSTICCLASS == null ? DIAGNOSTICCLASS_EDEFAULT : newDIAGNOSTICCLASS;
		boolean oldDIAGNOSTICCLASSESet = dIAGNOSTICCLASSESet;
		dIAGNOSTICCLASSESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__DIAGNOSTICCLASS, oldDIAGNOSTICCLASS, dIAGNOSTICCLASS, !oldDIAGNOSTICCLASSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDIAGNOSTICCLASS() {
		DIAGCLASSTYPE oldDIAGNOSTICCLASS = dIAGNOSTICCLASS;
		boolean oldDIAGNOSTICCLASSESet = dIAGNOSTICCLASSESet;
		dIAGNOSTICCLASS = DIAGNOSTICCLASS_EDEFAULT;
		dIAGNOSTICCLASSESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGCOMM__DIAGNOSTICCLASS, oldDIAGNOSTICCLASS, DIAGNOSTICCLASS_EDEFAULT, oldDIAGNOSTICCLASSESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDIAGNOSTICCLASS() {
		return dIAGNOSTICCLASSESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISEXECUTABLE() {
		return iSEXECUTABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISEXECUTABLE(boolean newISEXECUTABLE) {
		boolean oldISEXECUTABLE = iSEXECUTABLE;
		iSEXECUTABLE = newISEXECUTABLE;
		boolean oldISEXECUTABLEESet = iSEXECUTABLEESet;
		iSEXECUTABLEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__ISEXECUTABLE, oldISEXECUTABLE, iSEXECUTABLE, !oldISEXECUTABLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISEXECUTABLE() {
		boolean oldISEXECUTABLE = iSEXECUTABLE;
		boolean oldISEXECUTABLEESet = iSEXECUTABLEESet;
		iSEXECUTABLE = ISEXECUTABLE_EDEFAULT;
		iSEXECUTABLEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGCOMM__ISEXECUTABLE, oldISEXECUTABLE, ISEXECUTABLE_EDEFAULT, oldISEXECUTABLEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISEXECUTABLE() {
		return iSEXECUTABLEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISFINAL() {
		return iSFINAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISFINAL(boolean newISFINAL) {
		boolean oldISFINAL = iSFINAL;
		iSFINAL = newISFINAL;
		boolean oldISFINALESet = iSFINALESet;
		iSFINALESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__ISFINAL, oldISFINAL, iSFINAL, !oldISFINALESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISFINAL() {
		boolean oldISFINAL = iSFINAL;
		boolean oldISFINALESet = iSFINALESet;
		iSFINAL = ISFINAL_EDEFAULT;
		iSFINALESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGCOMM__ISFINAL, oldISFINAL, ISFINAL_EDEFAULT, oldISFINALESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISFINAL() {
		return iSFINALESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isISMANDATORY() {
		return iSMANDATORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setISMANDATORY(boolean newISMANDATORY) {
		boolean oldISMANDATORY = iSMANDATORY;
		iSMANDATORY = newISMANDATORY;
		boolean oldISMANDATORYESet = iSMANDATORYESet;
		iSMANDATORYESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__ISMANDATORY, oldISMANDATORY, iSMANDATORY, !oldISMANDATORYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetISMANDATORY() {
		boolean oldISMANDATORY = iSMANDATORY;
		boolean oldISMANDATORYESet = iSMANDATORYESet;
		iSMANDATORY = ISMANDATORY_EDEFAULT;
		iSMANDATORYESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGCOMM__ISMANDATORY, oldISMANDATORY, ISMANDATORY_EDEFAULT, oldISMANDATORYESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetISMANDATORY() {
		return iSMANDATORYESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getSECURITYACCESSLEVEL() {
		return sECURITYACCESSLEVEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSECURITYACCESSLEVEL(long newSECURITYACCESSLEVEL) {
		long oldSECURITYACCESSLEVEL = sECURITYACCESSLEVEL;
		sECURITYACCESSLEVEL = newSECURITYACCESSLEVEL;
		boolean oldSECURITYACCESSLEVELESet = sECURITYACCESSLEVELESet;
		sECURITYACCESSLEVELESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__SECURITYACCESSLEVEL, oldSECURITYACCESSLEVEL, sECURITYACCESSLEVEL, !oldSECURITYACCESSLEVELESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetSECURITYACCESSLEVEL() {
		long oldSECURITYACCESSLEVEL = sECURITYACCESSLEVEL;
		boolean oldSECURITYACCESSLEVELESet = sECURITYACCESSLEVELESet;
		sECURITYACCESSLEVEL = SECURITYACCESSLEVEL_EDEFAULT;
		sECURITYACCESSLEVELESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.DIAGCOMM__SECURITYACCESSLEVEL, oldSECURITYACCESSLEVEL, SECURITYACCESSLEVEL_EDEFAULT, oldSECURITYACCESSLEVELESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetSECURITYACCESSLEVEL() {
		return sECURITYACCESSLEVELESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSEMANTIC() {
		return sEMANTIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSEMANTIC(String newSEMANTIC) {
		String oldSEMANTIC = sEMANTIC;
		sEMANTIC = newSEMANTIC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.DIAGCOMM__SEMANTIC, oldSEMANTIC, sEMANTIC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMM__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.DIAGCOMM__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.DIAGCOMM__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.DIAGCOMM__SDGS:
				return basicSetSDGS(null, msgs);
			case OdxXhtmlPackage.DIAGCOMM__COMPARAMREFS:
				return basicSetCOMPARAMREFS(null, msgs);
			case OdxXhtmlPackage.DIAGCOMM__FUNCTCLASSREFS:
				return basicSetFUNCTCLASSREFS(null, msgs);
			case OdxXhtmlPackage.DIAGCOMM__AUDIENCE:
				return basicSetAUDIENCE(null, msgs);
			case OdxXhtmlPackage.DIAGCOMM__PROTOCOLSNREFS:
				return basicSetPROTOCOLSNREFS(null, msgs);
			case OdxXhtmlPackage.DIAGCOMM__RELATEDDIAGCOMMREFS:
				return basicSetRELATEDDIAGCOMMREFS(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMM__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.DIAGCOMM__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.DIAGCOMM__DESC:
				return getDESC();
			case OdxXhtmlPackage.DIAGCOMM__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.DIAGCOMM__SDGS:
				return getSDGS();
			case OdxXhtmlPackage.DIAGCOMM__COMPARAMREFS:
				return getCOMPARAMREFS();
			case OdxXhtmlPackage.DIAGCOMM__FUNCTCLASSREFS:
				return getFUNCTCLASSREFS();
			case OdxXhtmlPackage.DIAGCOMM__AUDIENCE:
				return getAUDIENCE();
			case OdxXhtmlPackage.DIAGCOMM__PROTOCOLSNREFS:
				return getPROTOCOLSNREFS();
			case OdxXhtmlPackage.DIAGCOMM__RELATEDDIAGCOMMREFS:
				return getRELATEDDIAGCOMMREFS();
			case OdxXhtmlPackage.DIAGCOMM__DIAGNOSTICCLASS:
				return getDIAGNOSTICCLASS();
			case OdxXhtmlPackage.DIAGCOMM__ID:
				return getID();
			case OdxXhtmlPackage.DIAGCOMM__ISEXECUTABLE:
				return isISEXECUTABLE();
			case OdxXhtmlPackage.DIAGCOMM__ISFINAL:
				return isISFINAL();
			case OdxXhtmlPackage.DIAGCOMM__ISMANDATORY:
				return isISMANDATORY();
			case OdxXhtmlPackage.DIAGCOMM__OID:
				return getOID();
			case OdxXhtmlPackage.DIAGCOMM__SECURITYACCESSLEVEL:
				return getSECURITYACCESSLEVEL();
			case OdxXhtmlPackage.DIAGCOMM__SEMANTIC:
				return getSEMANTIC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMM__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__SDGS:
				setSDGS((SDGS)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__COMPARAMREFS:
				setCOMPARAMREFS((COMPARAMREFS)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__FUNCTCLASSREFS:
				setFUNCTCLASSREFS((FUNCTCLASSREFS)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__AUDIENCE:
				setAUDIENCE((AUDIENCE)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__PROTOCOLSNREFS:
				setPROTOCOLSNREFS((PROTOCOLSNREFS)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__RELATEDDIAGCOMMREFS:
				setRELATEDDIAGCOMMREFS((RELATEDDIAGCOMMREFS)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__DIAGNOSTICCLASS:
				setDIAGNOSTICCLASS((DIAGCLASSTYPE)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__ISEXECUTABLE:
				setISEXECUTABLE((Boolean)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__ISFINAL:
				setISFINAL((Boolean)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__ISMANDATORY:
				setISMANDATORY((Boolean)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__OID:
				setOID((String)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__SECURITYACCESSLEVEL:
				setSECURITYACCESSLEVEL((Long)newValue);
				return;
			case OdxXhtmlPackage.DIAGCOMM__SEMANTIC:
				setSEMANTIC((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMM__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.DIAGCOMM__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.DIAGCOMM__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.DIAGCOMM__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.DIAGCOMM__SDGS:
				setSDGS((SDGS)null);
				return;
			case OdxXhtmlPackage.DIAGCOMM__COMPARAMREFS:
				setCOMPARAMREFS((COMPARAMREFS)null);
				return;
			case OdxXhtmlPackage.DIAGCOMM__FUNCTCLASSREFS:
				setFUNCTCLASSREFS((FUNCTCLASSREFS)null);
				return;
			case OdxXhtmlPackage.DIAGCOMM__AUDIENCE:
				setAUDIENCE((AUDIENCE)null);
				return;
			case OdxXhtmlPackage.DIAGCOMM__PROTOCOLSNREFS:
				setPROTOCOLSNREFS((PROTOCOLSNREFS)null);
				return;
			case OdxXhtmlPackage.DIAGCOMM__RELATEDDIAGCOMMREFS:
				setRELATEDDIAGCOMMREFS((RELATEDDIAGCOMMREFS)null);
				return;
			case OdxXhtmlPackage.DIAGCOMM__DIAGNOSTICCLASS:
				unsetDIAGNOSTICCLASS();
				return;
			case OdxXhtmlPackage.DIAGCOMM__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.DIAGCOMM__ISEXECUTABLE:
				unsetISEXECUTABLE();
				return;
			case OdxXhtmlPackage.DIAGCOMM__ISFINAL:
				unsetISFINAL();
				return;
			case OdxXhtmlPackage.DIAGCOMM__ISMANDATORY:
				unsetISMANDATORY();
				return;
			case OdxXhtmlPackage.DIAGCOMM__OID:
				setOID(OID_EDEFAULT);
				return;
			case OdxXhtmlPackage.DIAGCOMM__SECURITYACCESSLEVEL:
				unsetSECURITYACCESSLEVEL();
				return;
			case OdxXhtmlPackage.DIAGCOMM__SEMANTIC:
				setSEMANTIC(SEMANTIC_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DIAGCOMM__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.DIAGCOMM__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.DIAGCOMM__DESC:
				return dESC != null;
			case OdxXhtmlPackage.DIAGCOMM__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.DIAGCOMM__SDGS:
				return sDGS != null;
			case OdxXhtmlPackage.DIAGCOMM__COMPARAMREFS:
				return cOMPARAMREFS != null;
			case OdxXhtmlPackage.DIAGCOMM__FUNCTCLASSREFS:
				return fUNCTCLASSREFS != null;
			case OdxXhtmlPackage.DIAGCOMM__AUDIENCE:
				return aUDIENCE != null;
			case OdxXhtmlPackage.DIAGCOMM__PROTOCOLSNREFS:
				return pROTOCOLSNREFS != null;
			case OdxXhtmlPackage.DIAGCOMM__RELATEDDIAGCOMMREFS:
				return rELATEDDIAGCOMMREFS != null;
			case OdxXhtmlPackage.DIAGCOMM__DIAGNOSTICCLASS:
				return isSetDIAGNOSTICCLASS();
			case OdxXhtmlPackage.DIAGCOMM__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.DIAGCOMM__ISEXECUTABLE:
				return isSetISEXECUTABLE();
			case OdxXhtmlPackage.DIAGCOMM__ISFINAL:
				return isSetISFINAL();
			case OdxXhtmlPackage.DIAGCOMM__ISMANDATORY:
				return isSetISMANDATORY();
			case OdxXhtmlPackage.DIAGCOMM__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
			case OdxXhtmlPackage.DIAGCOMM__SECURITYACCESSLEVEL:
				return isSetSECURITYACCESSLEVEL();
			case OdxXhtmlPackage.DIAGCOMM__SEMANTIC:
				return SEMANTIC_EDEFAULT == null ? sEMANTIC != null : !SEMANTIC_EDEFAULT.equals(sEMANTIC);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", dIAGNOSTICCLASS: ");
		if (dIAGNOSTICCLASSESet) result.append(dIAGNOSTICCLASS); else result.append("<unset>");
		result.append(", iD: ");
		result.append(iD);
		result.append(", iSEXECUTABLE: ");
		if (iSEXECUTABLEESet) result.append(iSEXECUTABLE); else result.append("<unset>");
		result.append(", iSFINAL: ");
		if (iSFINALESet) result.append(iSFINAL); else result.append("<unset>");
		result.append(", iSMANDATORY: ");
		if (iSMANDATORYESet) result.append(iSMANDATORY); else result.append("<unset>");
		result.append(", oID: ");
		result.append(oID);
		result.append(", sECURITYACCESSLEVEL: ");
		if (sECURITYACCESSLEVELESet) result.append(sECURITYACCESSLEVEL); else result.append("<unset>");
		result.append(", sEMANTIC: ");
		result.append(sEMANTIC);
		result.append(')');
		return result.toString();
	}

} //DIAGCOMMImpl
