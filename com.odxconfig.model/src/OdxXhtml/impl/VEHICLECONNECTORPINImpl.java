/**
 */
package OdxXhtml.impl;

import OdxXhtml.DESCRIPTION;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PINTYPE;
import OdxXhtml.TEXT;
import OdxXhtml.VEHICLECONNECTORPIN;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VEHICLECONNECTORPIN</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORPINImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORPINImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORPINImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORPINImpl#getPINNUMBER <em>PINNUMBER</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORPINImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORPINImpl#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.impl.VEHICLECONNECTORPINImpl#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VEHICLECONNECTORPINImpl extends MinimalEObjectImpl.Container implements VEHICLECONNECTORPIN {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getPINNUMBER() <em>PINNUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPINNUMBER()
	 * @generated
	 * @ordered
	 */
	protected static final long PINNUMBER_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getPINNUMBER() <em>PINNUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPINNUMBER()
	 * @generated
	 * @ordered
	 */
	protected long pINNUMBER = PINNUMBER_EDEFAULT;

	/**
	 * This is true if the PINNUMBER attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean pINNUMBERESet;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * The default value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final PINTYPE TYPE_EDEFAULT = PINTYPE.HI;

	/**
	 * The cached value of the '{@link #getTYPE() <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTYPE()
	 * @generated
	 * @ordered
	 */
	protected PINTYPE tYPE = TYPE_EDEFAULT;

	/**
	 * This is true if the TYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tYPEESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VEHICLECONNECTORPINImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVEHICLECONNECTORPIN();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTORPIN__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTORPIN__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTORPIN__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTORPIN__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTORPIN__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTORPIN__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTORPIN__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.VEHICLECONNECTORPIN__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTORPIN__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public long getPINNUMBER() {
		return pINNUMBER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPINNUMBER(long newPINNUMBER) {
		long oldPINNUMBER = pINNUMBER;
		pINNUMBER = newPINNUMBER;
		boolean oldPINNUMBERESet = pINNUMBERESet;
		pINNUMBERESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTORPIN__PINNUMBER, oldPINNUMBER, pINNUMBER, !oldPINNUMBERESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetPINNUMBER() {
		long oldPINNUMBER = pINNUMBER;
		boolean oldPINNUMBERESet = pINNUMBERESet;
		pINNUMBER = PINNUMBER_EDEFAULT;
		pINNUMBERESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.VEHICLECONNECTORPIN__PINNUMBER, oldPINNUMBER, PINNUMBER_EDEFAULT, oldPINNUMBERESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetPINNUMBER() {
		return pINNUMBERESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTORPIN__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTORPIN__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PINTYPE getTYPE() {
		return tYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTYPE(PINTYPE newTYPE) {
		PINTYPE oldTYPE = tYPE;
		tYPE = newTYPE == null ? TYPE_EDEFAULT : newTYPE;
		boolean oldTYPEESet = tYPEESet;
		tYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.VEHICLECONNECTORPIN__TYPE, oldTYPE, tYPE, !oldTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetTYPE() {
		PINTYPE oldTYPE = tYPE;
		boolean oldTYPEESet = tYPEESet;
		tYPE = TYPE_EDEFAULT;
		tYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.VEHICLECONNECTORPIN__TYPE, oldTYPE, TYPE_EDEFAULT, oldTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetTYPE() {
		return tYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__DESC:
				return basicSetDESC(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__DESC:
				return getDESC();
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__PINNUMBER:
				return getPINNUMBER();
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__ID:
				return getID();
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__OID:
				return getOID();
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__TYPE:
				return getTYPE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__PINNUMBER:
				setPINNUMBER((Long)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__OID:
				setOID((String)newValue);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__TYPE:
				setTYPE((PINTYPE)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__PINNUMBER:
				unsetPINNUMBER();
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__OID:
				setOID(OID_EDEFAULT);
				return;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__TYPE:
				unsetTYPE();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__DESC:
				return dESC != null;
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__PINNUMBER:
				return isSetPINNUMBER();
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
			case OdxXhtmlPackage.VEHICLECONNECTORPIN__TYPE:
				return isSetTYPE();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", pINNUMBER: ");
		if (pINNUMBERESet) result.append(pINNUMBER); else result.append("<unset>");
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(", tYPE: ");
		if (tYPEESet) result.append(tYPE); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //VEHICLECONNECTORPINImpl
