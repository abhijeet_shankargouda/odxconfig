/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYREVISIONINFO;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYREVISIONINFO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYREVISIONINFOImpl#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYREVISIONINFOImpl#getREVISIONLABEL <em>REVISIONLABEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.COMPANYREVISIONINFOImpl#getSTATE <em>STATE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYREVISIONINFOImpl extends MinimalEObjectImpl.Container implements COMPANYREVISIONINFO {
	/**
	 * The cached value of the '{@link #getCOMPANYDATAREF() <em>COMPANYDATAREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYDATAREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK cOMPANYDATAREF;

	/**
	 * The default value of the '{@link #getREVISIONLABEL() <em>REVISIONLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISIONLABEL()
	 * @generated
	 * @ordered
	 */
	protected static final String REVISIONLABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getREVISIONLABEL() <em>REVISIONLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISIONLABEL()
	 * @generated
	 * @ordered
	 */
	protected String rEVISIONLABEL = REVISIONLABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getSTATE() <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTATE()
	 * @generated
	 * @ordered
	 */
	protected static final String STATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSTATE() <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTATE()
	 * @generated
	 * @ordered
	 */
	protected String sTATE = STATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYREVISIONINFOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYREVISIONINFO();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getCOMPANYDATAREF() {
		return cOMPANYDATAREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCOMPANYDATAREF(ODXLINK newCOMPANYDATAREF, NotificationChain msgs) {
		ODXLINK oldCOMPANYDATAREF = cOMPANYDATAREF;
		cOMPANYDATAREF = newCOMPANYDATAREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYREVISIONINFO__COMPANYDATAREF, oldCOMPANYDATAREF, newCOMPANYDATAREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCOMPANYDATAREF(ODXLINK newCOMPANYDATAREF) {
		if (newCOMPANYDATAREF != cOMPANYDATAREF) {
			NotificationChain msgs = null;
			if (cOMPANYDATAREF != null)
				msgs = ((InternalEObject)cOMPANYDATAREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYREVISIONINFO__COMPANYDATAREF, null, msgs);
			if (newCOMPANYDATAREF != null)
				msgs = ((InternalEObject)newCOMPANYDATAREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.COMPANYREVISIONINFO__COMPANYDATAREF, null, msgs);
			msgs = basicSetCOMPANYDATAREF(newCOMPANYDATAREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYREVISIONINFO__COMPANYDATAREF, newCOMPANYDATAREF, newCOMPANYDATAREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getREVISIONLABEL() {
		return rEVISIONLABEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREVISIONLABEL(String newREVISIONLABEL) {
		String oldREVISIONLABEL = rEVISIONLABEL;
		rEVISIONLABEL = newREVISIONLABEL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYREVISIONINFO__REVISIONLABEL, oldREVISIONLABEL, rEVISIONLABEL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSTATE() {
		return sTATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSTATE(String newSTATE) {
		String oldSTATE = sTATE;
		sTATE = newSTATE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.COMPANYREVISIONINFO__STATE, oldSTATE, sTATE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFO__COMPANYDATAREF:
				return basicSetCOMPANYDATAREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFO__COMPANYDATAREF:
				return getCOMPANYDATAREF();
			case OdxXhtmlPackage.COMPANYREVISIONINFO__REVISIONLABEL:
				return getREVISIONLABEL();
			case OdxXhtmlPackage.COMPANYREVISIONINFO__STATE:
				return getSTATE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFO__COMPANYDATAREF:
				setCOMPANYDATAREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.COMPANYREVISIONINFO__REVISIONLABEL:
				setREVISIONLABEL((String)newValue);
				return;
			case OdxXhtmlPackage.COMPANYREVISIONINFO__STATE:
				setSTATE((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFO__COMPANYDATAREF:
				setCOMPANYDATAREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.COMPANYREVISIONINFO__REVISIONLABEL:
				setREVISIONLABEL(REVISIONLABEL_EDEFAULT);
				return;
			case OdxXhtmlPackage.COMPANYREVISIONINFO__STATE:
				setSTATE(STATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFO__COMPANYDATAREF:
				return cOMPANYDATAREF != null;
			case OdxXhtmlPackage.COMPANYREVISIONINFO__REVISIONLABEL:
				return REVISIONLABEL_EDEFAULT == null ? rEVISIONLABEL != null : !REVISIONLABEL_EDEFAULT.equals(rEVISIONLABEL);
			case OdxXhtmlPackage.COMPANYREVISIONINFO__STATE:
				return STATE_EDEFAULT == null ? sTATE != null : !STATE_EDEFAULT.equals(sTATE);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (rEVISIONLABEL: ");
		result.append(rEVISIONLABEL);
		result.append(", sTATE: ");
		result.append(sTATE);
		result.append(')');
		return result.toString();
	}

} //COMPANYREVISIONINFOImpl
