/**
 */
package OdxXhtml.impl;

import OdxXhtml.COMPANYREVISIONINFO1;
import OdxXhtml.COMPANYREVISIONINFOS1;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COMPANYREVISIONINFOS1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.COMPANYREVISIONINFOS1Impl#getCOMPANYREVISIONINFO <em>COMPANYREVISIONINFO</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COMPANYREVISIONINFOS1Impl extends MinimalEObjectImpl.Container implements COMPANYREVISIONINFOS1 {
	/**
	 * The cached value of the '{@link #getCOMPANYREVISIONINFO() <em>COMPANYREVISIONINFO</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCOMPANYREVISIONINFO()
	 * @generated
	 * @ordered
	 */
	protected EList<COMPANYREVISIONINFO1> cOMPANYREVISIONINFO;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COMPANYREVISIONINFOS1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getCOMPANYREVISIONINFOS1();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<COMPANYREVISIONINFO1> getCOMPANYREVISIONINFO() {
		if (cOMPANYREVISIONINFO == null) {
			cOMPANYREVISIONINFO = new EObjectContainmentEList<COMPANYREVISIONINFO1>(COMPANYREVISIONINFO1.class, this, OdxXhtmlPackage.COMPANYREVISIONINFOS1__COMPANYREVISIONINFO);
		}
		return cOMPANYREVISIONINFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS1__COMPANYREVISIONINFO:
				return ((InternalEList<?>)getCOMPANYREVISIONINFO()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS1__COMPANYREVISIONINFO:
				return getCOMPANYREVISIONINFO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS1__COMPANYREVISIONINFO:
				getCOMPANYREVISIONINFO().clear();
				getCOMPANYREVISIONINFO().addAll((Collection<? extends COMPANYREVISIONINFO1>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS1__COMPANYREVISIONINFO:
				getCOMPANYREVISIONINFO().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.COMPANYREVISIONINFOS1__COMPANYREVISIONINFO:
				return cOMPANYREVISIONINFO != null && !cOMPANYREVISIONINFO.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //COMPANYREVISIONINFOS1Impl
