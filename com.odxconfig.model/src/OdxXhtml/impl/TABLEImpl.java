/**
 */
package OdxXhtml.impl;

import OdxXhtml.ADMINDATA;
import OdxXhtml.DESCRIPTION;
import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.TABLE;
import OdxXhtml.TABLEROW;
import OdxXhtml.TEXT;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TABLE</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getKEYLABEL <em>KEYLABEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getSTRUCTLABEL <em>STRUCTLABEL</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getKEYDOPREF <em>KEYDOPREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getROWWRAPPER <em>ROWWRAPPER</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getTABLEROWREF <em>TABLEROWREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getTABLEROW <em>TABLEROW</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEImpl#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TABLEImpl extends MinimalEObjectImpl.Container implements TABLE {
	/**
	 * The default value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected static final String SHORTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSHORTNAME() <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSHORTNAME()
	 * @generated
	 * @ordered
	 */
	protected String sHORTNAME = SHORTNAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLONGNAME() <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLONGNAME()
	 * @generated
	 * @ordered
	 */
	protected TEXT lONGNAME;

	/**
	 * The cached value of the '{@link #getDESC() <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDESC()
	 * @generated
	 * @ordered
	 */
	protected DESCRIPTION dESC;

	/**
	 * The default value of the '{@link #getKEYLABEL() <em>KEYLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKEYLABEL()
	 * @generated
	 * @ordered
	 */
	protected static final String KEYLABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getKEYLABEL() <em>KEYLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKEYLABEL()
	 * @generated
	 * @ordered
	 */
	protected String kEYLABEL = KEYLABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getSTRUCTLABEL() <em>STRUCTLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTRUCTLABEL()
	 * @generated
	 * @ordered
	 */
	protected static final String STRUCTLABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSTRUCTLABEL() <em>STRUCTLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSTRUCTLABEL()
	 * @generated
	 * @ordered
	 */
	protected String sTRUCTLABEL = STRUCTLABEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getADMINDATA() <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADMINDATA()
	 * @generated
	 * @ordered
	 */
	protected ADMINDATA aDMINDATA;

	/**
	 * The cached value of the '{@link #getKEYDOPREF() <em>KEYDOPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKEYDOPREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK kEYDOPREF;

	/**
	 * The cached value of the '{@link #getROWWRAPPER() <em>ROWWRAPPER</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getROWWRAPPER()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap rOWWRAPPER;

	/**
	 * The default value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getID() <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getID()
	 * @generated
	 * @ordered
	 */
	protected String iD = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected static final String OID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOID() <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOID()
	 * @generated
	 * @ordered
	 */
	protected String oID = OID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSEMANTIC() <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSEMANTIC()
	 * @generated
	 * @ordered
	 */
	protected static final String SEMANTIC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSEMANTIC() <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSEMANTIC()
	 * @generated
	 * @ordered
	 */
	protected String sEMANTIC = SEMANTIC_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TABLEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getTABLE();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSHORTNAME() {
		return sHORTNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSHORTNAME(String newSHORTNAME) {
		String oldSHORTNAME = sHORTNAME;
		sHORTNAME = newSHORTNAME;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__SHORTNAME, oldSHORTNAME, sHORTNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TEXT getLONGNAME() {
		return lONGNAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLONGNAME(TEXT newLONGNAME, NotificationChain msgs) {
		TEXT oldLONGNAME = lONGNAME;
		lONGNAME = newLONGNAME;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__LONGNAME, oldLONGNAME, newLONGNAME);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLONGNAME(TEXT newLONGNAME) {
		if (newLONGNAME != lONGNAME) {
			NotificationChain msgs = null;
			if (lONGNAME != null)
				msgs = ((InternalEObject)lONGNAME).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLE__LONGNAME, null, msgs);
			if (newLONGNAME != null)
				msgs = ((InternalEObject)newLONGNAME).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLE__LONGNAME, null, msgs);
			msgs = basicSetLONGNAME(newLONGNAME, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__LONGNAME, newLONGNAME, newLONGNAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DESCRIPTION getDESC() {
		return dESC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDESC(DESCRIPTION newDESC, NotificationChain msgs) {
		DESCRIPTION oldDESC = dESC;
		dESC = newDESC;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__DESC, oldDESC, newDESC);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDESC(DESCRIPTION newDESC) {
		if (newDESC != dESC) {
			NotificationChain msgs = null;
			if (dESC != null)
				msgs = ((InternalEObject)dESC).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLE__DESC, null, msgs);
			if (newDESC != null)
				msgs = ((InternalEObject)newDESC).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLE__DESC, null, msgs);
			msgs = basicSetDESC(newDESC, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__DESC, newDESC, newDESC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getKEYLABEL() {
		return kEYLABEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKEYLABEL(String newKEYLABEL) {
		String oldKEYLABEL = kEYLABEL;
		kEYLABEL = newKEYLABEL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__KEYLABEL, oldKEYLABEL, kEYLABEL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSTRUCTLABEL() {
		return sTRUCTLABEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSTRUCTLABEL(String newSTRUCTLABEL) {
		String oldSTRUCTLABEL = sTRUCTLABEL;
		sTRUCTLABEL = newSTRUCTLABEL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__STRUCTLABEL, oldSTRUCTLABEL, sTRUCTLABEL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ADMINDATA getADMINDATA() {
		return aDMINDATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetADMINDATA(ADMINDATA newADMINDATA, NotificationChain msgs) {
		ADMINDATA oldADMINDATA = aDMINDATA;
		aDMINDATA = newADMINDATA;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__ADMINDATA, oldADMINDATA, newADMINDATA);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setADMINDATA(ADMINDATA newADMINDATA) {
		if (newADMINDATA != aDMINDATA) {
			NotificationChain msgs = null;
			if (aDMINDATA != null)
				msgs = ((InternalEObject)aDMINDATA).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLE__ADMINDATA, null, msgs);
			if (newADMINDATA != null)
				msgs = ((InternalEObject)newADMINDATA).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLE__ADMINDATA, null, msgs);
			msgs = basicSetADMINDATA(newADMINDATA, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__ADMINDATA, newADMINDATA, newADMINDATA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getKEYDOPREF() {
		return kEYDOPREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKEYDOPREF(ODXLINK newKEYDOPREF, NotificationChain msgs) {
		ODXLINK oldKEYDOPREF = kEYDOPREF;
		kEYDOPREF = newKEYDOPREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__KEYDOPREF, oldKEYDOPREF, newKEYDOPREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setKEYDOPREF(ODXLINK newKEYDOPREF) {
		if (newKEYDOPREF != kEYDOPREF) {
			NotificationChain msgs = null;
			if (kEYDOPREF != null)
				msgs = ((InternalEObject)kEYDOPREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLE__KEYDOPREF, null, msgs);
			if (newKEYDOPREF != null)
				msgs = ((InternalEObject)newKEYDOPREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLE__KEYDOPREF, null, msgs);
			msgs = basicSetKEYDOPREF(newKEYDOPREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__KEYDOPREF, newKEYDOPREF, newKEYDOPREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getROWWRAPPER() {
		if (rOWWRAPPER == null) {
			rOWWRAPPER = new BasicFeatureMap(this, OdxXhtmlPackage.TABLE__ROWWRAPPER);
		}
		return rOWWRAPPER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ODXLINK> getTABLEROWREF() {
		return getROWWRAPPER().list(OdxXhtmlPackage.eINSTANCE.getTABLE_TABLEROWREF());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TABLEROW> getTABLEROW() {
		return getROWWRAPPER().list(OdxXhtmlPackage.eINSTANCE.getTABLE_TABLEROW());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getID() {
		return iD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setID(String newID) {
		String oldID = iD;
		iD = newID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__ID, oldID, iD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOID() {
		return oID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOID(String newOID) {
		String oldOID = oID;
		oID = newOID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__OID, oldOID, oID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSEMANTIC() {
		return sEMANTIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSEMANTIC(String newSEMANTIC) {
		String oldSEMANTIC = sEMANTIC;
		sEMANTIC = newSEMANTIC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLE__SEMANTIC, oldSEMANTIC, sEMANTIC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLE__LONGNAME:
				return basicSetLONGNAME(null, msgs);
			case OdxXhtmlPackage.TABLE__DESC:
				return basicSetDESC(null, msgs);
			case OdxXhtmlPackage.TABLE__ADMINDATA:
				return basicSetADMINDATA(null, msgs);
			case OdxXhtmlPackage.TABLE__KEYDOPREF:
				return basicSetKEYDOPREF(null, msgs);
			case OdxXhtmlPackage.TABLE__ROWWRAPPER:
				return ((InternalEList<?>)getROWWRAPPER()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.TABLE__TABLEROWREF:
				return ((InternalEList<?>)getTABLEROWREF()).basicRemove(otherEnd, msgs);
			case OdxXhtmlPackage.TABLE__TABLEROW:
				return ((InternalEList<?>)getTABLEROW()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLE__SHORTNAME:
				return getSHORTNAME();
			case OdxXhtmlPackage.TABLE__LONGNAME:
				return getLONGNAME();
			case OdxXhtmlPackage.TABLE__DESC:
				return getDESC();
			case OdxXhtmlPackage.TABLE__KEYLABEL:
				return getKEYLABEL();
			case OdxXhtmlPackage.TABLE__STRUCTLABEL:
				return getSTRUCTLABEL();
			case OdxXhtmlPackage.TABLE__ADMINDATA:
				return getADMINDATA();
			case OdxXhtmlPackage.TABLE__KEYDOPREF:
				return getKEYDOPREF();
			case OdxXhtmlPackage.TABLE__ROWWRAPPER:
				if (coreType) return getROWWRAPPER();
				return ((FeatureMap.Internal)getROWWRAPPER()).getWrapper();
			case OdxXhtmlPackage.TABLE__TABLEROWREF:
				return getTABLEROWREF();
			case OdxXhtmlPackage.TABLE__TABLEROW:
				return getTABLEROW();
			case OdxXhtmlPackage.TABLE__ID:
				return getID();
			case OdxXhtmlPackage.TABLE__OID:
				return getOID();
			case OdxXhtmlPackage.TABLE__SEMANTIC:
				return getSEMANTIC();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLE__SHORTNAME:
				setSHORTNAME((String)newValue);
				return;
			case OdxXhtmlPackage.TABLE__LONGNAME:
				setLONGNAME((TEXT)newValue);
				return;
			case OdxXhtmlPackage.TABLE__DESC:
				setDESC((DESCRIPTION)newValue);
				return;
			case OdxXhtmlPackage.TABLE__KEYLABEL:
				setKEYLABEL((String)newValue);
				return;
			case OdxXhtmlPackage.TABLE__STRUCTLABEL:
				setSTRUCTLABEL((String)newValue);
				return;
			case OdxXhtmlPackage.TABLE__ADMINDATA:
				setADMINDATA((ADMINDATA)newValue);
				return;
			case OdxXhtmlPackage.TABLE__KEYDOPREF:
				setKEYDOPREF((ODXLINK)newValue);
				return;
			case OdxXhtmlPackage.TABLE__ROWWRAPPER:
				((FeatureMap.Internal)getROWWRAPPER()).set(newValue);
				return;
			case OdxXhtmlPackage.TABLE__TABLEROWREF:
				getTABLEROWREF().clear();
				getTABLEROWREF().addAll((Collection<? extends ODXLINK>)newValue);
				return;
			case OdxXhtmlPackage.TABLE__TABLEROW:
				getTABLEROW().clear();
				getTABLEROW().addAll((Collection<? extends TABLEROW>)newValue);
				return;
			case OdxXhtmlPackage.TABLE__ID:
				setID((String)newValue);
				return;
			case OdxXhtmlPackage.TABLE__OID:
				setOID((String)newValue);
				return;
			case OdxXhtmlPackage.TABLE__SEMANTIC:
				setSEMANTIC((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLE__SHORTNAME:
				setSHORTNAME(SHORTNAME_EDEFAULT);
				return;
			case OdxXhtmlPackage.TABLE__LONGNAME:
				setLONGNAME((TEXT)null);
				return;
			case OdxXhtmlPackage.TABLE__DESC:
				setDESC((DESCRIPTION)null);
				return;
			case OdxXhtmlPackage.TABLE__KEYLABEL:
				setKEYLABEL(KEYLABEL_EDEFAULT);
				return;
			case OdxXhtmlPackage.TABLE__STRUCTLABEL:
				setSTRUCTLABEL(STRUCTLABEL_EDEFAULT);
				return;
			case OdxXhtmlPackage.TABLE__ADMINDATA:
				setADMINDATA((ADMINDATA)null);
				return;
			case OdxXhtmlPackage.TABLE__KEYDOPREF:
				setKEYDOPREF((ODXLINK)null);
				return;
			case OdxXhtmlPackage.TABLE__ROWWRAPPER:
				getROWWRAPPER().clear();
				return;
			case OdxXhtmlPackage.TABLE__TABLEROWREF:
				getTABLEROWREF().clear();
				return;
			case OdxXhtmlPackage.TABLE__TABLEROW:
				getTABLEROW().clear();
				return;
			case OdxXhtmlPackage.TABLE__ID:
				setID(ID_EDEFAULT);
				return;
			case OdxXhtmlPackage.TABLE__OID:
				setOID(OID_EDEFAULT);
				return;
			case OdxXhtmlPackage.TABLE__SEMANTIC:
				setSEMANTIC(SEMANTIC_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLE__SHORTNAME:
				return SHORTNAME_EDEFAULT == null ? sHORTNAME != null : !SHORTNAME_EDEFAULT.equals(sHORTNAME);
			case OdxXhtmlPackage.TABLE__LONGNAME:
				return lONGNAME != null;
			case OdxXhtmlPackage.TABLE__DESC:
				return dESC != null;
			case OdxXhtmlPackage.TABLE__KEYLABEL:
				return KEYLABEL_EDEFAULT == null ? kEYLABEL != null : !KEYLABEL_EDEFAULT.equals(kEYLABEL);
			case OdxXhtmlPackage.TABLE__STRUCTLABEL:
				return STRUCTLABEL_EDEFAULT == null ? sTRUCTLABEL != null : !STRUCTLABEL_EDEFAULT.equals(sTRUCTLABEL);
			case OdxXhtmlPackage.TABLE__ADMINDATA:
				return aDMINDATA != null;
			case OdxXhtmlPackage.TABLE__KEYDOPREF:
				return kEYDOPREF != null;
			case OdxXhtmlPackage.TABLE__ROWWRAPPER:
				return rOWWRAPPER != null && !rOWWRAPPER.isEmpty();
			case OdxXhtmlPackage.TABLE__TABLEROWREF:
				return !getTABLEROWREF().isEmpty();
			case OdxXhtmlPackage.TABLE__TABLEROW:
				return !getTABLEROW().isEmpty();
			case OdxXhtmlPackage.TABLE__ID:
				return ID_EDEFAULT == null ? iD != null : !ID_EDEFAULT.equals(iD);
			case OdxXhtmlPackage.TABLE__OID:
				return OID_EDEFAULT == null ? oID != null : !OID_EDEFAULT.equals(oID);
			case OdxXhtmlPackage.TABLE__SEMANTIC:
				return SEMANTIC_EDEFAULT == null ? sEMANTIC != null : !SEMANTIC_EDEFAULT.equals(sEMANTIC);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sHORTNAME: ");
		result.append(sHORTNAME);
		result.append(", kEYLABEL: ");
		result.append(kEYLABEL);
		result.append(", sTRUCTLABEL: ");
		result.append(sTRUCTLABEL);
		result.append(", rOWWRAPPER: ");
		result.append(rOWWRAPPER);
		result.append(", iD: ");
		result.append(iD);
		result.append(", oID: ");
		result.append(oID);
		result.append(", sEMANTIC: ");
		result.append(sEMANTIC);
		result.append(')');
		return result.toString();
	}

} //TABLEImpl
