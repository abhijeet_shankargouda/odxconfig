/**
 */
package OdxXhtml.impl;

import OdxXhtml.DTCVALUE;
import OdxXhtml.DTCVALUES;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DTCVALUES</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DTCVALUESImpl#getDTCVALUE <em>DTCVALUE</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DTCVALUESImpl extends MinimalEObjectImpl.Container implements DTCVALUES {
	/**
	 * The cached value of the '{@link #getDTCVALUE() <em>DTCVALUE</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDTCVALUE()
	 * @generated
	 * @ordered
	 */
	protected EList<DTCVALUE> dTCVALUE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DTCVALUESImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDTCVALUES();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DTCVALUE> getDTCVALUE() {
		if (dTCVALUE == null) {
			dTCVALUE = new EObjectContainmentEList<DTCVALUE>(DTCVALUE.class, this, OdxXhtmlPackage.DTCVALUES__DTCVALUE);
		}
		return dTCVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCVALUES__DTCVALUE:
				return ((InternalEList<?>)getDTCVALUE()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCVALUES__DTCVALUE:
				return getDTCVALUE();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCVALUES__DTCVALUE:
				getDTCVALUE().clear();
				getDTCVALUE().addAll((Collection<? extends DTCVALUE>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCVALUES__DTCVALUE:
				getDTCVALUE().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DTCVALUES__DTCVALUE:
				return dTCVALUE != null && !dTCVALUE.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DTCVALUESImpl
