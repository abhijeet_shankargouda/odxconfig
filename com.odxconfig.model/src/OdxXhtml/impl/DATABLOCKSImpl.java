/**
 */
package OdxXhtml.impl;

import OdxXhtml.DATABLOCK;
import OdxXhtml.DATABLOCKS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DATABLOCKS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.DATABLOCKSImpl#getDATABLOCK <em>DATABLOCK</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DATABLOCKSImpl extends MinimalEObjectImpl.Container implements DATABLOCKS {
	/**
	 * The cached value of the '{@link #getDATABLOCK() <em>DATABLOCK</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDATABLOCK()
	 * @generated
	 * @ordered
	 */
	protected EList<DATABLOCK> dATABLOCK;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DATABLOCKSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getDATABLOCKS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<DATABLOCK> getDATABLOCK() {
		if (dATABLOCK == null) {
			dATABLOCK = new EObjectContainmentEList<DATABLOCK>(DATABLOCK.class, this, OdxXhtmlPackage.DATABLOCKS__DATABLOCK);
		}
		return dATABLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKS__DATABLOCK:
				return ((InternalEList<?>)getDATABLOCK()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKS__DATABLOCK:
				return getDATABLOCK();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKS__DATABLOCK:
				getDATABLOCK().clear();
				getDATABLOCK().addAll((Collection<? extends DATABLOCK>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKS__DATABLOCK:
				getDATABLOCK().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.DATABLOCKS__DATABLOCK:
				return dATABLOCK != null && !dATABLOCK.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DATABLOCKSImpl
