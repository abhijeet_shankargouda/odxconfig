/**
 */
package OdxXhtml.impl;

import OdxXhtml.ACCESSLEVEL;
import OdxXhtml.ACCESSLEVELS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ACCESSLEVELS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.ACCESSLEVELSImpl#getACCESSLEVEL <em>ACCESSLEVEL</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ACCESSLEVELSImpl extends MinimalEObjectImpl.Container implements ACCESSLEVELS {
	/**
	 * The cached value of the '{@link #getACCESSLEVEL() <em>ACCESSLEVEL</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACCESSLEVEL()
	 * @generated
	 * @ordered
	 */
	protected EList<ACCESSLEVEL> aCCESSLEVEL;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ACCESSLEVELSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getACCESSLEVELS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ACCESSLEVEL> getACCESSLEVEL() {
		if (aCCESSLEVEL == null) {
			aCCESSLEVEL = new EObjectContainmentEList<ACCESSLEVEL>(ACCESSLEVEL.class, this, OdxXhtmlPackage.ACCESSLEVELS__ACCESSLEVEL);
		}
		return aCCESSLEVEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVELS__ACCESSLEVEL:
				return ((InternalEList<?>)getACCESSLEVEL()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVELS__ACCESSLEVEL:
				return getACCESSLEVEL();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVELS__ACCESSLEVEL:
				getACCESSLEVEL().clear();
				getACCESSLEVEL().addAll((Collection<? extends ACCESSLEVEL>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVELS__ACCESSLEVEL:
				getACCESSLEVEL().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.ACCESSLEVELS__ACCESSLEVEL:
				return aCCESSLEVEL != null && !aCCESSLEVEL.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ACCESSLEVELSImpl
