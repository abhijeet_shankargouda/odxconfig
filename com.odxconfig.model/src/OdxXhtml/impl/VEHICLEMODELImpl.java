/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.VEHICLEMODEL;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VEHICLEMODEL</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VEHICLEMODELImpl extends INFOCOMPONENTImpl implements VEHICLEMODEL {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VEHICLEMODELImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getVEHICLEMODEL();
	}

} //VEHICLEMODELImpl
