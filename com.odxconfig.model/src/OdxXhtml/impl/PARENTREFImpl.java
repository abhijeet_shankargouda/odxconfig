/**
 */
package OdxXhtml.impl;

import OdxXhtml.DOCTYPE;
import OdxXhtml.NOTINHERITEDDIAGCOMMS;
import OdxXhtml.NOTINHERITEDVARIABLES;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PARENTREF;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PARENTREF</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PARENTREFImpl#getNOTINHERITEDDIAGCOMMS <em>NOTINHERITEDDIAGCOMMS</em>}</li>
 *   <li>{@link OdxXhtml.impl.PARENTREFImpl#getNOTINHERITEDVARIABLES <em>NOTINHERITEDVARIABLES</em>}</li>
 *   <li>{@link OdxXhtml.impl.PARENTREFImpl#getDOCREF <em>DOCREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.PARENTREFImpl#getDOCTYPE <em>DOCTYPE</em>}</li>
 *   <li>{@link OdxXhtml.impl.PARENTREFImpl#getIDREF <em>IDREF</em>}</li>
 *   <li>{@link OdxXhtml.impl.PARENTREFImpl#getREVISION <em>REVISION</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PARENTREFImpl extends MinimalEObjectImpl.Container implements PARENTREF {
	/**
	 * The cached value of the '{@link #getNOTINHERITEDDIAGCOMMS() <em>NOTINHERITEDDIAGCOMMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNOTINHERITEDDIAGCOMMS()
	 * @generated
	 * @ordered
	 */
	protected NOTINHERITEDDIAGCOMMS nOTINHERITEDDIAGCOMMS;

	/**
	 * The cached value of the '{@link #getNOTINHERITEDVARIABLES() <em>NOTINHERITEDVARIABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNOTINHERITEDVARIABLES()
	 * @generated
	 * @ordered
	 */
	protected NOTINHERITEDVARIABLES nOTINHERITEDVARIABLES;

	/**
	 * The default value of the '{@link #getDOCREF() <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREF()
	 * @generated
	 * @ordered
	 */
	protected static final String DOCREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDOCREF() <em>DOCREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCREF()
	 * @generated
	 * @ordered
	 */
	protected String dOCREF = DOCREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getDOCTYPE() <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCTYPE()
	 * @generated
	 * @ordered
	 */
	protected static final DOCTYPE DOCTYPE_EDEFAULT = DOCTYPE.FLASH;

	/**
	 * The cached value of the '{@link #getDOCTYPE() <em>DOCTYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOCTYPE()
	 * @generated
	 * @ordered
	 */
	protected DOCTYPE dOCTYPE = DOCTYPE_EDEFAULT;

	/**
	 * This is true if the DOCTYPE attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean dOCTYPEESet;

	/**
	 * The default value of the '{@link #getIDREF() <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDREF()
	 * @generated
	 * @ordered
	 */
	protected static final String IDREF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIDREF() <em>IDREF</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIDREF()
	 * @generated
	 * @ordered
	 */
	protected String iDREF = IDREF_EDEFAULT;

	/**
	 * The default value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected static final String REVISION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getREVISION() <em>REVISION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREVISION()
	 * @generated
	 * @ordered
	 */
	protected String rEVISION = REVISION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PARENTREFImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPARENTREF();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NOTINHERITEDDIAGCOMMS getNOTINHERITEDDIAGCOMMS() {
		return nOTINHERITEDDIAGCOMMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNOTINHERITEDDIAGCOMMS(NOTINHERITEDDIAGCOMMS newNOTINHERITEDDIAGCOMMS, NotificationChain msgs) {
		NOTINHERITEDDIAGCOMMS oldNOTINHERITEDDIAGCOMMS = nOTINHERITEDDIAGCOMMS;
		nOTINHERITEDDIAGCOMMS = newNOTINHERITEDDIAGCOMMS;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARENTREF__NOTINHERITEDDIAGCOMMS, oldNOTINHERITEDDIAGCOMMS, newNOTINHERITEDDIAGCOMMS);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNOTINHERITEDDIAGCOMMS(NOTINHERITEDDIAGCOMMS newNOTINHERITEDDIAGCOMMS) {
		if (newNOTINHERITEDDIAGCOMMS != nOTINHERITEDDIAGCOMMS) {
			NotificationChain msgs = null;
			if (nOTINHERITEDDIAGCOMMS != null)
				msgs = ((InternalEObject)nOTINHERITEDDIAGCOMMS).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PARENTREF__NOTINHERITEDDIAGCOMMS, null, msgs);
			if (newNOTINHERITEDDIAGCOMMS != null)
				msgs = ((InternalEObject)newNOTINHERITEDDIAGCOMMS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PARENTREF__NOTINHERITEDDIAGCOMMS, null, msgs);
			msgs = basicSetNOTINHERITEDDIAGCOMMS(newNOTINHERITEDDIAGCOMMS, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARENTREF__NOTINHERITEDDIAGCOMMS, newNOTINHERITEDDIAGCOMMS, newNOTINHERITEDDIAGCOMMS));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NOTINHERITEDVARIABLES getNOTINHERITEDVARIABLES() {
		return nOTINHERITEDVARIABLES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNOTINHERITEDVARIABLES(NOTINHERITEDVARIABLES newNOTINHERITEDVARIABLES, NotificationChain msgs) {
		NOTINHERITEDVARIABLES oldNOTINHERITEDVARIABLES = nOTINHERITEDVARIABLES;
		nOTINHERITEDVARIABLES = newNOTINHERITEDVARIABLES;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARENTREF__NOTINHERITEDVARIABLES, oldNOTINHERITEDVARIABLES, newNOTINHERITEDVARIABLES);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNOTINHERITEDVARIABLES(NOTINHERITEDVARIABLES newNOTINHERITEDVARIABLES) {
		if (newNOTINHERITEDVARIABLES != nOTINHERITEDVARIABLES) {
			NotificationChain msgs = null;
			if (nOTINHERITEDVARIABLES != null)
				msgs = ((InternalEObject)nOTINHERITEDVARIABLES).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PARENTREF__NOTINHERITEDVARIABLES, null, msgs);
			if (newNOTINHERITEDVARIABLES != null)
				msgs = ((InternalEObject)newNOTINHERITEDVARIABLES).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.PARENTREF__NOTINHERITEDVARIABLES, null, msgs);
			msgs = basicSetNOTINHERITEDVARIABLES(newNOTINHERITEDVARIABLES, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARENTREF__NOTINHERITEDVARIABLES, newNOTINHERITEDVARIABLES, newNOTINHERITEDVARIABLES));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDOCREF() {
		return dOCREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCREF(String newDOCREF) {
		String oldDOCREF = dOCREF;
		dOCREF = newDOCREF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARENTREF__DOCREF, oldDOCREF, dOCREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DOCTYPE getDOCTYPE() {
		return dOCTYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOCTYPE(DOCTYPE newDOCTYPE) {
		DOCTYPE oldDOCTYPE = dOCTYPE;
		dOCTYPE = newDOCTYPE == null ? DOCTYPE_EDEFAULT : newDOCTYPE;
		boolean oldDOCTYPEESet = dOCTYPEESet;
		dOCTYPEESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARENTREF__DOCTYPE, oldDOCTYPE, dOCTYPE, !oldDOCTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetDOCTYPE() {
		DOCTYPE oldDOCTYPE = dOCTYPE;
		boolean oldDOCTYPEESet = dOCTYPEESet;
		dOCTYPE = DOCTYPE_EDEFAULT;
		dOCTYPEESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.PARENTREF__DOCTYPE, oldDOCTYPE, DOCTYPE_EDEFAULT, oldDOCTYPEESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetDOCTYPE() {
		return dOCTYPEESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIDREF() {
		return iDREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIDREF(String newIDREF) {
		String oldIDREF = iDREF;
		iDREF = newIDREF;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARENTREF__IDREF, oldIDREF, iDREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getREVISION() {
		return rEVISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREVISION(String newREVISION) {
		String oldREVISION = rEVISION;
		rEVISION = newREVISION;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.PARENTREF__REVISION, oldREVISION, rEVISION));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDDIAGCOMMS:
				return basicSetNOTINHERITEDDIAGCOMMS(null, msgs);
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDVARIABLES:
				return basicSetNOTINHERITEDVARIABLES(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDDIAGCOMMS:
				return getNOTINHERITEDDIAGCOMMS();
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDVARIABLES:
				return getNOTINHERITEDVARIABLES();
			case OdxXhtmlPackage.PARENTREF__DOCREF:
				return getDOCREF();
			case OdxXhtmlPackage.PARENTREF__DOCTYPE:
				return getDOCTYPE();
			case OdxXhtmlPackage.PARENTREF__IDREF:
				return getIDREF();
			case OdxXhtmlPackage.PARENTREF__REVISION:
				return getREVISION();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDDIAGCOMMS:
				setNOTINHERITEDDIAGCOMMS((NOTINHERITEDDIAGCOMMS)newValue);
				return;
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDVARIABLES:
				setNOTINHERITEDVARIABLES((NOTINHERITEDVARIABLES)newValue);
				return;
			case OdxXhtmlPackage.PARENTREF__DOCREF:
				setDOCREF((String)newValue);
				return;
			case OdxXhtmlPackage.PARENTREF__DOCTYPE:
				setDOCTYPE((DOCTYPE)newValue);
				return;
			case OdxXhtmlPackage.PARENTREF__IDREF:
				setIDREF((String)newValue);
				return;
			case OdxXhtmlPackage.PARENTREF__REVISION:
				setREVISION((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDDIAGCOMMS:
				setNOTINHERITEDDIAGCOMMS((NOTINHERITEDDIAGCOMMS)null);
				return;
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDVARIABLES:
				setNOTINHERITEDVARIABLES((NOTINHERITEDVARIABLES)null);
				return;
			case OdxXhtmlPackage.PARENTREF__DOCREF:
				setDOCREF(DOCREF_EDEFAULT);
				return;
			case OdxXhtmlPackage.PARENTREF__DOCTYPE:
				unsetDOCTYPE();
				return;
			case OdxXhtmlPackage.PARENTREF__IDREF:
				setIDREF(IDREF_EDEFAULT);
				return;
			case OdxXhtmlPackage.PARENTREF__REVISION:
				setREVISION(REVISION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDDIAGCOMMS:
				return nOTINHERITEDDIAGCOMMS != null;
			case OdxXhtmlPackage.PARENTREF__NOTINHERITEDVARIABLES:
				return nOTINHERITEDVARIABLES != null;
			case OdxXhtmlPackage.PARENTREF__DOCREF:
				return DOCREF_EDEFAULT == null ? dOCREF != null : !DOCREF_EDEFAULT.equals(dOCREF);
			case OdxXhtmlPackage.PARENTREF__DOCTYPE:
				return isSetDOCTYPE();
			case OdxXhtmlPackage.PARENTREF__IDREF:
				return IDREF_EDEFAULT == null ? iDREF != null : !IDREF_EDEFAULT.equals(iDREF);
			case OdxXhtmlPackage.PARENTREF__REVISION:
				return REVISION_EDEFAULT == null ? rEVISION != null : !REVISION_EDEFAULT.equals(rEVISION);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (dOCREF: ");
		result.append(dOCREF);
		result.append(", dOCTYPE: ");
		if (dOCTYPEESet) result.append(dOCTYPE); else result.append("<unset>");
		result.append(", iDREF: ");
		result.append(iDREF);
		result.append(", rEVISION: ");
		result.append(rEVISION);
		result.append(')');
		return result.toString();
	}

} //PARENTREFImpl
