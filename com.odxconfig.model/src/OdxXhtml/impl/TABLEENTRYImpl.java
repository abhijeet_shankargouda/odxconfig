/**
 */
package OdxXhtml.impl;

import OdxXhtml.ODXLINK;
import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.ROWFRAGMENT;
import OdxXhtml.TABLEENTRY;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TABLEENTRY</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.TABLEENTRYImpl#getTARGET <em>TARGET</em>}</li>
 *   <li>{@link OdxXhtml.impl.TABLEENTRYImpl#getTABLEROWREF <em>TABLEROWREF</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TABLEENTRYImpl extends PARAMImpl implements TABLEENTRY {
	/**
	 * The default value of the '{@link #getTARGET() <em>TARGET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTARGET()
	 * @generated
	 * @ordered
	 */
	protected static final ROWFRAGMENT TARGET_EDEFAULT = ROWFRAGMENT.KEY;

	/**
	 * The cached value of the '{@link #getTARGET() <em>TARGET</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTARGET()
	 * @generated
	 * @ordered
	 */
	protected ROWFRAGMENT tARGET = TARGET_EDEFAULT;

	/**
	 * This is true if the TARGET attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tARGETESet;

	/**
	 * The cached value of the '{@link #getTABLEROWREF() <em>TABLEROWREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTABLEROWREF()
	 * @generated
	 * @ordered
	 */
	protected ODXLINK tABLEROWREF;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TABLEENTRYImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getTABLEENTRY();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ROWFRAGMENT getTARGET() {
		return tARGET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTARGET(ROWFRAGMENT newTARGET) {
		ROWFRAGMENT oldTARGET = tARGET;
		tARGET = newTARGET == null ? TARGET_EDEFAULT : newTARGET;
		boolean oldTARGETESet = tARGETESet;
		tARGETESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLEENTRY__TARGET, oldTARGET, tARGET, !oldTARGETESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetTARGET() {
		ROWFRAGMENT oldTARGET = tARGET;
		boolean oldTARGETESet = tARGETESet;
		tARGET = TARGET_EDEFAULT;
		tARGETESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, OdxXhtmlPackage.TABLEENTRY__TARGET, oldTARGET, TARGET_EDEFAULT, oldTARGETESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetTARGET() {
		return tARGETESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ODXLINK getTABLEROWREF() {
		return tABLEROWREF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTABLEROWREF(ODXLINK newTABLEROWREF, NotificationChain msgs) {
		ODXLINK oldTABLEROWREF = tABLEROWREF;
		tABLEROWREF = newTABLEROWREF;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLEENTRY__TABLEROWREF, oldTABLEROWREF, newTABLEROWREF);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTABLEROWREF(ODXLINK newTABLEROWREF) {
		if (newTABLEROWREF != tABLEROWREF) {
			NotificationChain msgs = null;
			if (tABLEROWREF != null)
				msgs = ((InternalEObject)tABLEROWREF).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLEENTRY__TABLEROWREF, null, msgs);
			if (newTABLEROWREF != null)
				msgs = ((InternalEObject)newTABLEROWREF).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OdxXhtmlPackage.TABLEENTRY__TABLEROWREF, null, msgs);
			msgs = basicSetTABLEROWREF(newTABLEROWREF, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.TABLEENTRY__TABLEROWREF, newTABLEROWREF, newTABLEROWREF));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEENTRY__TABLEROWREF:
				return basicSetTABLEROWREF(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEENTRY__TARGET:
				return getTARGET();
			case OdxXhtmlPackage.TABLEENTRY__TABLEROWREF:
				return getTABLEROWREF();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEENTRY__TARGET:
				setTARGET((ROWFRAGMENT)newValue);
				return;
			case OdxXhtmlPackage.TABLEENTRY__TABLEROWREF:
				setTABLEROWREF((ODXLINK)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEENTRY__TARGET:
				unsetTARGET();
				return;
			case OdxXhtmlPackage.TABLEENTRY__TABLEROWREF:
				setTABLEROWREF((ODXLINK)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.TABLEENTRY__TARGET:
				return isSetTARGET();
			case OdxXhtmlPackage.TABLEENTRY__TABLEROWREF:
				return tABLEROWREF != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (tARGET: ");
		if (tARGETESet) result.append(tARGET); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //TABLEENTRYImpl
