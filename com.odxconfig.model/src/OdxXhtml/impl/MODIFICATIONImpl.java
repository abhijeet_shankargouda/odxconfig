/**
 */
package OdxXhtml.impl;

import OdxXhtml.MODIFICATION;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MODIFICATION</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.MODIFICATIONImpl#getCHANGE <em>CHANGE</em>}</li>
 *   <li>{@link OdxXhtml.impl.MODIFICATIONImpl#getREASON <em>REASON</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MODIFICATIONImpl extends MinimalEObjectImpl.Container implements MODIFICATION {
	/**
	 * The default value of the '{@link #getCHANGE() <em>CHANGE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCHANGE()
	 * @generated
	 * @ordered
	 */
	protected static final String CHANGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCHANGE() <em>CHANGE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCHANGE()
	 * @generated
	 * @ordered
	 */
	protected String cHANGE = CHANGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getREASON() <em>REASON</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREASON()
	 * @generated
	 * @ordered
	 */
	protected static final String REASON_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getREASON() <em>REASON</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getREASON()
	 * @generated
	 * @ordered
	 */
	protected String rEASON = REASON_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MODIFICATIONImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getMODIFICATION();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCHANGE() {
		return cHANGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCHANGE(String newCHANGE) {
		String oldCHANGE = cHANGE;
		cHANGE = newCHANGE;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MODIFICATION__CHANGE, oldCHANGE, cHANGE));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getREASON() {
		return rEASON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setREASON(String newREASON) {
		String oldREASON = rEASON;
		rEASON = newREASON;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OdxXhtmlPackage.MODIFICATION__REASON, oldREASON, rEASON));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATION__CHANGE:
				return getCHANGE();
			case OdxXhtmlPackage.MODIFICATION__REASON:
				return getREASON();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATION__CHANGE:
				setCHANGE((String)newValue);
				return;
			case OdxXhtmlPackage.MODIFICATION__REASON:
				setREASON((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATION__CHANGE:
				setCHANGE(CHANGE_EDEFAULT);
				return;
			case OdxXhtmlPackage.MODIFICATION__REASON:
				setREASON(REASON_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.MODIFICATION__CHANGE:
				return CHANGE_EDEFAULT == null ? cHANGE != null : !CHANGE_EDEFAULT.equals(cHANGE);
			case OdxXhtmlPackage.MODIFICATION__REASON:
				return REASON_EDEFAULT == null ? rEASON != null : !REASON_EDEFAULT.equals(rEASON);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (cHANGE: ");
		result.append(cHANGE);
		result.append(", rEASON: ");
		result.append(rEASON);
		result.append(')');
		return result.toString();
	}

} //MODIFICATIONImpl
