/**
 */
package OdxXhtml.impl;

import OdxXhtml.OdxXhtmlPackage;
import OdxXhtml.PROJECTIDENT;
import OdxXhtml.PROJECTIDENTS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PROJECTIDENTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.PROJECTIDENTSImpl#getPROJECTIDENT <em>PROJECTIDENT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PROJECTIDENTSImpl extends MinimalEObjectImpl.Container implements PROJECTIDENTS {
	/**
	 * The cached value of the '{@link #getPROJECTIDENT() <em>PROJECTIDENT</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROJECTIDENT()
	 * @generated
	 * @ordered
	 */
	protected EList<PROJECTIDENT> pROJECTIDENT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PROJECTIDENTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getPROJECTIDENTS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<PROJECTIDENT> getPROJECTIDENT() {
		if (pROJECTIDENT == null) {
			pROJECTIDENT = new EObjectContainmentEList<PROJECTIDENT>(PROJECTIDENT.class, this, OdxXhtmlPackage.PROJECTIDENTS__PROJECTIDENT);
		}
		return pROJECTIDENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTIDENTS__PROJECTIDENT:
				return ((InternalEList<?>)getPROJECTIDENT()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTIDENTS__PROJECTIDENT:
				return getPROJECTIDENT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTIDENTS__PROJECTIDENT:
				getPROJECTIDENT().clear();
				getPROJECTIDENT().addAll((Collection<? extends PROJECTIDENT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTIDENTS__PROJECTIDENT:
				getPROJECTIDENT().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.PROJECTIDENTS__PROJECTIDENT:
				return pROJECTIDENT != null && !pROJECTIDENT.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //PROJECTIDENTSImpl
