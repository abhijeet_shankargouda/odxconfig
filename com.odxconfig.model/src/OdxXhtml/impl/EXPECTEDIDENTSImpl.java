/**
 */
package OdxXhtml.impl;

import OdxXhtml.EXPECTEDIDENT;
import OdxXhtml.EXPECTEDIDENTS;
import OdxXhtml.OdxXhtmlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EXPECTEDIDENTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.impl.EXPECTEDIDENTSImpl#getEXPECTEDIDENT <em>EXPECTEDIDENT</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EXPECTEDIDENTSImpl extends MinimalEObjectImpl.Container implements EXPECTEDIDENTS {
	/**
	 * The cached value of the '{@link #getEXPECTEDIDENT() <em>EXPECTEDIDENT</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEXPECTEDIDENT()
	 * @generated
	 * @ordered
	 */
	protected EList<EXPECTEDIDENT> eXPECTEDIDENT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EXPECTEDIDENTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getEXPECTEDIDENTS();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<EXPECTEDIDENT> getEXPECTEDIDENT() {
		if (eXPECTEDIDENT == null) {
			eXPECTEDIDENT = new EObjectContainmentEList<EXPECTEDIDENT>(EXPECTEDIDENT.class, this, OdxXhtmlPackage.EXPECTEDIDENTS__EXPECTEDIDENT);
		}
		return eXPECTEDIDENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OdxXhtmlPackage.EXPECTEDIDENTS__EXPECTEDIDENT:
				return ((InternalEList<?>)getEXPECTEDIDENT()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OdxXhtmlPackage.EXPECTEDIDENTS__EXPECTEDIDENT:
				return getEXPECTEDIDENT();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OdxXhtmlPackage.EXPECTEDIDENTS__EXPECTEDIDENT:
				getEXPECTEDIDENT().clear();
				getEXPECTEDIDENT().addAll((Collection<? extends EXPECTEDIDENT>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.EXPECTEDIDENTS__EXPECTEDIDENT:
				getEXPECTEDIDENT().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OdxXhtmlPackage.EXPECTEDIDENTS__EXPECTEDIDENT:
				return eXPECTEDIDENT != null && !eXPECTEDIDENT.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EXPECTEDIDENTSImpl
