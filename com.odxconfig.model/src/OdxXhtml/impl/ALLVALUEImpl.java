/**
 */
package OdxXhtml.impl;

import OdxXhtml.ALLVALUE;
import OdxXhtml.OdxXhtmlPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ALLVALUE</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ALLVALUEImpl extends MinimalEObjectImpl.Container implements ALLVALUE {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ALLVALUEImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OdxXhtmlPackage.eINSTANCE.getALLVALUE();
	}

} //ALLVALUEImpl
