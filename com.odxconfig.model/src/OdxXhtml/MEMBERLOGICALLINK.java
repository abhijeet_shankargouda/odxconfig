/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MEMBERLOGICALLINK</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMEMBERLOGICALLINK()
 * @model extendedMetaData="name='MEMBER-LOGICAL-LINK' kind='elementOnly'"
 * @generated
 */
public interface MEMBERLOGICALLINK extends LOGICALLINK {
} // MEMBERLOGICALLINK
