/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DATAOBJECTPROP</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DATAOBJECTPROP#getCOMPUMETHOD <em>COMPUMETHOD</em>}</li>
 *   <li>{@link OdxXhtml.DATAOBJECTPROP#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}</li>
 *   <li>{@link OdxXhtml.DATAOBJECTPROP#getPHYSICALTYPE <em>PHYSICALTYPE</em>}</li>
 *   <li>{@link OdxXhtml.DATAOBJECTPROP#getINTERNALCONSTR <em>INTERNALCONSTR</em>}</li>
 *   <li>{@link OdxXhtml.DATAOBJECTPROP#getUNITREF <em>UNITREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDATAOBJECTPROP()
 * @model extendedMetaData="name='DATA-OBJECT-PROP' kind='elementOnly'"
 * @generated
 */
public interface DATAOBJECTPROP extends DOPBASE {
	/**
	 * Returns the value of the '<em><b>COMPUMETHOD</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUMETHOD</em>' containment reference.
	 * @see #setCOMPUMETHOD(COMPUMETHOD)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAOBJECTPROP_COMPUMETHOD()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPU-METHOD' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUMETHOD getCOMPUMETHOD();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATAOBJECTPROP#getCOMPUMETHOD <em>COMPUMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUMETHOD</em>' containment reference.
	 * @see #getCOMPUMETHOD()
	 * @generated
	 */
	void setCOMPUMETHOD(COMPUMETHOD value);

	/**
	 * Returns the value of the '<em><b>DIAGCODEDTYPE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCODEDTYPE</em>' containment reference.
	 * @see #setDIAGCODEDTYPE(DIAGCODEDTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAOBJECTPROP_DIAGCODEDTYPE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-CODED-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGCODEDTYPE getDIAGCODEDTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATAOBJECTPROP#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCODEDTYPE</em>' containment reference.
	 * @see #getDIAGCODEDTYPE()
	 * @generated
	 */
	void setDIAGCODEDTYPE(DIAGCODEDTYPE value);

	/**
	 * Returns the value of the '<em><b>PHYSICALTYPE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALTYPE</em>' containment reference.
	 * @see #setPHYSICALTYPE(PHYSICALTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAOBJECTPROP_PHYSICALTYPE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PHYSICAL-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	PHYSICALTYPE getPHYSICALTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATAOBJECTPROP#getPHYSICALTYPE <em>PHYSICALTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSICALTYPE</em>' containment reference.
	 * @see #getPHYSICALTYPE()
	 * @generated
	 */
	void setPHYSICALTYPE(PHYSICALTYPE value);

	/**
	 * Returns the value of the '<em><b>INTERNALCONSTR</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INTERNALCONSTR</em>' containment reference.
	 * @see #setINTERNALCONSTR(INTERNALCONSTR)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAOBJECTPROP_INTERNALCONSTR()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='INTERNAL-CONSTR' namespace='##targetNamespace'"
	 * @generated
	 */
	INTERNALCONSTR getINTERNALCONSTR();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATAOBJECTPROP#getINTERNALCONSTR <em>INTERNALCONSTR</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>INTERNALCONSTR</em>' containment reference.
	 * @see #getINTERNALCONSTR()
	 * @generated
	 */
	void setINTERNALCONSTR(INTERNALCONSTR value);

	/**
	 * Returns the value of the '<em><b>UNITREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UNITREF</em>' containment reference.
	 * @see #setUNITREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAOBJECTPROP_UNITREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UNIT-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getUNITREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATAOBJECTPROP#getUNITREF <em>UNITREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UNITREF</em>' containment reference.
	 * @see #getUNITREF()
	 * @generated
	 */
	void setUNITREF(ODXLINK value);

} // DATAOBJECTPROP
