/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NEGRESPONSE</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getNEGRESPONSE()
 * @model extendedMetaData="name='NEG-RESPONSE' kind='elementOnly'"
 * @generated
 */
public interface NEGRESPONSE extends RESPONSE {
} // NEGRESPONSE
