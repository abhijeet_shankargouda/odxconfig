/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUSHAREDDATA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUSHAREDDATA#getDIAGVARIABLES <em>DIAGVARIABLES</em>}</li>
 *   <li>{@link OdxXhtml.ECUSHAREDDATA#getVARIABLEGROUPS <em>VARIABLEGROUPS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUSHAREDDATA()
 * @model extendedMetaData="name='ECU-SHARED-DATA' kind='elementOnly'"
 * @generated
 */
public interface ECUSHAREDDATA extends DIAGLAYER {
	/**
	 * Returns the value of the '<em><b>DIAGVARIABLES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGVARIABLES</em>' containment reference.
	 * @see #setDIAGVARIABLES(DIAGVARIABLES)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUSHAREDDATA_DIAGVARIABLES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-VARIABLES' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGVARIABLES getDIAGVARIABLES();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUSHAREDDATA#getDIAGVARIABLES <em>DIAGVARIABLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGVARIABLES</em>' containment reference.
	 * @see #getDIAGVARIABLES()
	 * @generated
	 */
	void setDIAGVARIABLES(DIAGVARIABLES value);

	/**
	 * Returns the value of the '<em><b>VARIABLEGROUPS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VARIABLEGROUPS</em>' containment reference.
	 * @see #setVARIABLEGROUPS(VARIABLEGROUPS)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUSHAREDDATA_VARIABLEGROUPS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='VARIABLE-GROUPS' namespace='##targetNamespace'"
	 * @generated
	 */
	VARIABLEGROUPS getVARIABLEGROUPS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUSHAREDDATA#getVARIABLEGROUPS <em>VARIABLEGROUPS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VARIABLEGROUPS</em>' containment reference.
	 * @see #getVARIABLEGROUPS()
	 * @generated
	 */
	void setVARIABLEGROUPS(VARIABLEGROUPS value);

} // ECUSHAREDDATA
