/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TABLES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.TABLES#getTABLE <em>TABLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getTABLES()
 * @model extendedMetaData="name='TABLES' kind='elementOnly'"
 * @generated
 */
public interface TABLES extends EObject {
	/**
	 * Returns the value of the '<em><b>TABLE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.TABLE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TABLE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLES_TABLE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='TABLE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<TABLE> getTABLE();

} // TABLES
