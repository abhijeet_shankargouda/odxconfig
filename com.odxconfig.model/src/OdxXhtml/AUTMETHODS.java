/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AUTMETHODS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.AUTMETHODS#getAUTMETHOD <em>AUTMETHOD</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getAUTMETHODS()
 * @model extendedMetaData="name='AUT-METHODS' kind='elementOnly'"
 * @generated
 */
public interface AUTMETHODS extends EObject {
	/**
	 * Returns the value of the '<em><b>AUTMETHOD</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.AUTMETHOD}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUTMETHOD</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getAUTMETHODS_AUTMETHOD()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='AUT-METHOD' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<AUTMETHOD> getAUTMETHOD();

} // AUTMETHODS
