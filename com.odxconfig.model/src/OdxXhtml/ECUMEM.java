/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUMEM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUMEM#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEM#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEM#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEM#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEM#getPROJECTINFOS <em>PROJECTINFOS</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEM#getMEM <em>MEM</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEM#getPHYSMEM <em>PHYSMEM</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEM#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.ECUMEM#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM()
 * @model extendedMetaData="name='ECU-MEM' kind='elementOnly'"
 * @generated
 */
public interface ECUMEM extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEM#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEM#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEM#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEM#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA value);

	/**
	 * Returns the value of the '<em><b>PROJECTINFOS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROJECTINFOS</em>' containment reference.
	 * @see #setPROJECTINFOS(PROJECTINFOS)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM_PROJECTINFOS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PROJECT-INFOS' namespace='##targetNamespace'"
	 * @generated
	 */
	PROJECTINFOS getPROJECTINFOS();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEM#getPROJECTINFOS <em>PROJECTINFOS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PROJECTINFOS</em>' containment reference.
	 * @see #getPROJECTINFOS()
	 * @generated
	 */
	void setPROJECTINFOS(PROJECTINFOS value);

	/**
	 * Returns the value of the '<em><b>MEM</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MEM</em>' containment reference.
	 * @see #setMEM(MEM)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM_MEM()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='MEM' namespace='##targetNamespace'"
	 * @generated
	 */
	MEM getMEM();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEM#getMEM <em>MEM</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MEM</em>' containment reference.
	 * @see #getMEM()
	 * @generated
	 */
	void setMEM(MEM value);

	/**
	 * Returns the value of the '<em><b>PHYSMEM</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSMEM</em>' containment reference.
	 * @see #setPHYSMEM(PHYSMEM)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM_PHYSMEM()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PHYS-MEM' namespace='##targetNamespace'"
	 * @generated
	 */
	PHYSMEM getPHYSMEM();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEM#getPHYSMEM <em>PHYSMEM</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSMEM</em>' containment reference.
	 * @see #getPHYSMEM()
	 * @generated
	 */
	void setPHYSMEM(PHYSMEM value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEM#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEM_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.ECUMEM#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // ECUMEM
