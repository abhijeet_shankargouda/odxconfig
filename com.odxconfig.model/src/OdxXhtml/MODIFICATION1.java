/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MODIFICATION1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.MODIFICATION1#getCHANGE <em>CHANGE</em>}</li>
 *   <li>{@link OdxXhtml.MODIFICATION1#getREASON <em>REASON</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getMODIFICATION1()
 * @model extendedMetaData="name='MODIFICATION' kind='elementOnly'"
 * @generated
 */
public interface MODIFICATION1 extends EObject {
	/**
	 * Returns the value of the '<em><b>CHANGE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CHANGE</em>' attribute.
	 * @see #setCHANGE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getMODIFICATION1_CHANGE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='CHANGE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCHANGE();

	/**
	 * Sets the value of the '{@link OdxXhtml.MODIFICATION1#getCHANGE <em>CHANGE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CHANGE</em>' attribute.
	 * @see #getCHANGE()
	 * @generated
	 */
	void setCHANGE(String value);

	/**
	 * Returns the value of the '<em><b>REASON</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REASON</em>' attribute.
	 * @see #setREASON(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getMODIFICATION1_REASON()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='REASON' namespace='##targetNamespace'"
	 * @generated
	 */
	String getREASON();

	/**
	 * Sets the value of the '{@link OdxXhtml.MODIFICATION1#getREASON <em>REASON</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REASON</em>' attribute.
	 * @see #getREASON()
	 * @generated
	 */
	void setREASON(String value);

} // MODIFICATION1
