/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ACCESSLEVEL</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ACCESSLEVEL#getVALUE <em>VALUE</em>}</li>
 *   <li>{@link OdxXhtml.ACCESSLEVEL#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.ACCESSLEVEL#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}</li>
 *   <li>{@link OdxXhtml.ACCESSLEVEL#getEXTERNALACCESSMETHOD <em>EXTERNALACCESSMETHOD</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getACCESSLEVEL()
 * @model extendedMetaData="name='ACCESS-LEVEL' kind='elementOnly'"
 * @generated
 */
public interface ACCESSLEVEL extends EObject {
	/**
	 * Returns the value of the '<em><b>VALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VALUE</em>' attribute.
	 * @see #isSetVALUE()
	 * @see #unsetVALUE()
	 * @see #setVALUE(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getACCESSLEVEL_VALUE()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	long getVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.ACCESSLEVEL#getVALUE <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VALUE</em>' attribute.
	 * @see #isSetVALUE()
	 * @see #unsetVALUE()
	 * @see #getVALUE()
	 * @generated
	 */
	void setVALUE(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.ACCESSLEVEL#getVALUE <em>VALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVALUE()
	 * @see #getVALUE()
	 * @see #setVALUE(long)
	 * @generated
	 */
	void unsetVALUE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.ACCESSLEVEL#getVALUE <em>VALUE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>VALUE</em>' attribute is set.
	 * @see #unsetVALUE()
	 * @see #getVALUE()
	 * @see #setVALUE(long)
	 * @generated
	 */
	boolean isSetVALUE();

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getACCESSLEVEL_DESC()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.ACCESSLEVEL#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>DIAGCOMMSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #setDIAGCOMMSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getACCESSLEVEL_DIAGCOMMSNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DIAG-COMM-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDIAGCOMMSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.ACCESSLEVEL#getDIAGCOMMSNREF <em>DIAGCOMMSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCOMMSNREF</em>' containment reference.
	 * @see #getDIAGCOMMSNREF()
	 * @generated
	 */
	void setDIAGCOMMSNREF(SNREF value);

	/**
	 * Returns the value of the '<em><b>EXTERNALACCESSMETHOD</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EXTERNALACCESSMETHOD</em>' containment reference.
	 * @see #setEXTERNALACCESSMETHOD(EXTERNALACCESSMETHOD)
	 * @see OdxXhtml.OdxXhtmlPackage#getACCESSLEVEL_EXTERNALACCESSMETHOD()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EXTERNAL-ACCESS-METHOD' namespace='##targetNamespace'"
	 * @generated
	 */
	EXTERNALACCESSMETHOD getEXTERNALACCESSMETHOD();

	/**
	 * Sets the value of the '{@link OdxXhtml.ACCESSLEVEL#getEXTERNALACCESSMETHOD <em>EXTERNALACCESSMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EXTERNALACCESSMETHOD</em>' containment reference.
	 * @see #getEXTERNALACCESSMETHOD()
	 * @generated
	 */
	void setEXTERNALACCESSMETHOD(EXTERNALACCESSMETHOD value);

} // ACCESSLEVEL
