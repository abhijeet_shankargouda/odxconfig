/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DATAFILE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DATAFILE#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.DATAFILE#isLATEBOUNDDATAFILE <em>LATEBOUNDDATAFILE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDATAFILE()
 * @model extendedMetaData="name='DATAFILE' kind='simple'"
 * @generated
 */
public interface DATAFILE extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAFILE_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATAFILE#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>LATEBOUNDDATAFILE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LATEBOUNDDATAFILE</em>' attribute.
	 * @see #isSetLATEBOUNDDATAFILE()
	 * @see #unsetLATEBOUNDDATAFILE()
	 * @see #setLATEBOUNDDATAFILE(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAFILE_LATEBOUNDDATAFILE()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
	 *        extendedMetaData="kind='attribute' name='LATEBOUND-DATAFILE' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isLATEBOUNDDATAFILE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATAFILE#isLATEBOUNDDATAFILE <em>LATEBOUNDDATAFILE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LATEBOUNDDATAFILE</em>' attribute.
	 * @see #isSetLATEBOUNDDATAFILE()
	 * @see #unsetLATEBOUNDDATAFILE()
	 * @see #isLATEBOUNDDATAFILE()
	 * @generated
	 */
	void setLATEBOUNDDATAFILE(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DATAFILE#isLATEBOUNDDATAFILE <em>LATEBOUNDDATAFILE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLATEBOUNDDATAFILE()
	 * @see #isLATEBOUNDDATAFILE()
	 * @see #setLATEBOUNDDATAFILE(boolean)
	 * @generated
	 */
	void unsetLATEBOUNDDATAFILE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DATAFILE#isLATEBOUNDDATAFILE <em>LATEBOUNDDATAFILE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>LATEBOUNDDATAFILE</em>' attribute is set.
	 * @see #unsetLATEBOUNDDATAFILE()
	 * @see #isLATEBOUNDDATAFILE()
	 * @see #setLATEBOUNDDATAFILE(boolean)
	 * @generated
	 */
	boolean isSetLATEBOUNDDATAFILE();

} // DATAFILE
