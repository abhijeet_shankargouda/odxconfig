/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SECURITYS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SECURITYS#getSECURITY <em>SECURITY</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSECURITYS()
 * @model extendedMetaData="name='SECURITYS' kind='elementOnly'"
 * @generated
 */
public interface SECURITYS extends EObject {
	/**
	 * Returns the value of the '<em><b>SECURITY</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.SECURITY}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SECURITY</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSECURITYS_SECURITY()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='SECURITY' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<SECURITY> getSECURITY();

} // SECURITYS
