/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DYNAMICENDMARKERFIELDS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DYNAMICENDMARKERFIELDS#getDYNAMICENDMARKERFIELD <em>DYNAMICENDMARKERFIELD</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMICENDMARKERFIELDS()
 * @model extendedMetaData="name='DYNAMIC-ENDMARKER-FIELDS' kind='elementOnly'"
 * @generated
 */
public interface DYNAMICENDMARKERFIELDS extends EObject {
	/**
	 * Returns the value of the '<em><b>DYNAMICENDMARKERFIELD</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DYNAMICENDMARKERFIELD}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DYNAMICENDMARKERFIELD</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMICENDMARKERFIELDS_DYNAMICENDMARKERFIELD()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DYNAMIC-ENDMARKER-FIELD' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DYNAMICENDMARKERFIELD> getDYNAMICENDMARKERFIELD();

} // DYNAMICENDMARKERFIELDS
