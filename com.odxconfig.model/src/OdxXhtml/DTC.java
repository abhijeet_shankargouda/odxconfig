/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DTC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DTC#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.DTC#getTROUBLECODE <em>TROUBLECODE</em>}</li>
 *   <li>{@link OdxXhtml.DTC#getDISPLAYTROUBLECODE <em>DISPLAYTROUBLECODE</em>}</li>
 *   <li>{@link OdxXhtml.DTC#getTEXT <em>TEXT</em>}</li>
 *   <li>{@link OdxXhtml.DTC#getLEVEL <em>LEVEL</em>}</li>
 *   <li>{@link OdxXhtml.DTC#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.DTC#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.DTC#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDTC()
 * @model extendedMetaData="name='DTC' kind='elementOnly'"
 * @generated
 */
public interface DTC extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTC_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTC#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>TROUBLECODE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TROUBLECODE</em>' attribute.
	 * @see #isSetTROUBLECODE()
	 * @see #unsetTROUBLECODE()
	 * @see #setTROUBLECODE(int)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTC_TROUBLECODE()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int" required="true"
	 *        extendedMetaData="kind='element' name='TROUBLE-CODE' namespace='##targetNamespace'"
	 * @generated
	 */
	int getTROUBLECODE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTC#getTROUBLECODE <em>TROUBLECODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TROUBLECODE</em>' attribute.
	 * @see #isSetTROUBLECODE()
	 * @see #unsetTROUBLECODE()
	 * @see #getTROUBLECODE()
	 * @generated
	 */
	void setTROUBLECODE(int value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DTC#getTROUBLECODE <em>TROUBLECODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTROUBLECODE()
	 * @see #getTROUBLECODE()
	 * @see #setTROUBLECODE(int)
	 * @generated
	 */
	void unsetTROUBLECODE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DTC#getTROUBLECODE <em>TROUBLECODE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TROUBLECODE</em>' attribute is set.
	 * @see #unsetTROUBLECODE()
	 * @see #getTROUBLECODE()
	 * @see #setTROUBLECODE(int)
	 * @generated
	 */
	boolean isSetTROUBLECODE();

	/**
	 * Returns the value of the '<em><b>DISPLAYTROUBLECODE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DISPLAYTROUBLECODE</em>' attribute.
	 * @see #setDISPLAYTROUBLECODE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTC_DISPLAYTROUBLECODE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='DISPLAY-TROUBLE-CODE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDISPLAYTROUBLECODE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTC#getDISPLAYTROUBLECODE <em>DISPLAYTROUBLECODE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DISPLAYTROUBLECODE</em>' attribute.
	 * @see #getDISPLAYTROUBLECODE()
	 * @generated
	 */
	void setDISPLAYTROUBLECODE(String value);

	/**
	 * Returns the value of the '<em><b>TEXT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TEXT</em>' containment reference.
	 * @see #setTEXT(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTC_TEXT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='TEXT' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getTEXT();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTC#getTEXT <em>TEXT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TEXT</em>' containment reference.
	 * @see #getTEXT()
	 * @generated
	 */
	void setTEXT(TEXT value);

	/**
	 * Returns the value of the '<em><b>LEVEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LEVEL</em>' attribute.
	 * @see #isSetLEVEL()
	 * @see #unsetLEVEL()
	 * @see #setLEVEL(short)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTC_LEVEL()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedByte"
	 *        extendedMetaData="kind='element' name='LEVEL' namespace='##targetNamespace'"
	 * @generated
	 */
	short getLEVEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTC#getLEVEL <em>LEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LEVEL</em>' attribute.
	 * @see #isSetLEVEL()
	 * @see #unsetLEVEL()
	 * @see #getLEVEL()
	 * @generated
	 */
	void setLEVEL(short value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DTC#getLEVEL <em>LEVEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLEVEL()
	 * @see #getLEVEL()
	 * @see #setLEVEL(short)
	 * @generated
	 */
	void unsetLEVEL();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DTC#getLEVEL <em>LEVEL</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>LEVEL</em>' attribute is set.
	 * @see #unsetLEVEL()
	 * @see #getLEVEL()
	 * @see #setLEVEL(short)
	 * @generated
	 */
	boolean isSetLEVEL();

	/**
	 * Returns the value of the '<em><b>SDGS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGS</em>' containment reference.
	 * @see #setSDGS(SDGS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTC_SDGS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDGS' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGS getSDGS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTC#getSDGS <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGS</em>' containment reference.
	 * @see #getSDGS()
	 * @generated
	 */
	void setSDGS(SDGS value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTC_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTC#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTC_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTC#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // DTC
