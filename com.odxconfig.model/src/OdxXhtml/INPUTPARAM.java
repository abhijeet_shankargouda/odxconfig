/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INPUTPARAM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.INPUTPARAM#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.INPUTPARAM#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.INPUTPARAM#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.INPUTPARAM#getPHYSICALDEFAULTVALUE <em>PHYSICALDEFAULTVALUE</em>}</li>
 *   <li>{@link OdxXhtml.INPUTPARAM#getDOPBASEREF <em>DOPBASEREF</em>}</li>
 *   <li>{@link OdxXhtml.INPUTPARAM#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.INPUTPARAM#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAM()
 * @model extendedMetaData="name='INPUT-PARAM' kind='elementOnly'"
 * @generated
 */
public interface INPUTPARAM extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAM_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.INPUTPARAM#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAM_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.INPUTPARAM#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAM_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.INPUTPARAM#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>PHYSICALDEFAULTVALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALDEFAULTVALUE</em>' attribute.
	 * @see #setPHYSICALDEFAULTVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAM_PHYSICALDEFAULTVALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='PHYSICAL-DEFAULT-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPHYSICALDEFAULTVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.INPUTPARAM#getPHYSICALDEFAULTVALUE <em>PHYSICALDEFAULTVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSICALDEFAULTVALUE</em>' attribute.
	 * @see #getPHYSICALDEFAULTVALUE()
	 * @generated
	 */
	void setPHYSICALDEFAULTVALUE(String value);

	/**
	 * Returns the value of the '<em><b>DOPBASEREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPBASEREF</em>' containment reference.
	 * @see #setDOPBASEREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAM_DOPBASEREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DOP-BASE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDOPBASEREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.INPUTPARAM#getDOPBASEREF <em>DOPBASEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPBASEREF</em>' containment reference.
	 * @see #getDOPBASEREF()
	 * @generated
	 */
	void setDOPBASEREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAM_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.INPUTPARAM#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

	/**
	 * Returns the value of the '<em><b>SEMANTIC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SEMANTIC</em>' attribute.
	 * @see #setSEMANTIC(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getINPUTPARAM_SEMANTIC()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='SEMANTIC' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSEMANTIC();

	/**
	 * Sets the value of the '{@link OdxXhtml.INPUTPARAM#getSEMANTIC <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SEMANTIC</em>' attribute.
	 * @see #getSEMANTIC()
	 * @generated
	 */
	void setSEMANTIC(String value);

} // INPUTPARAM
