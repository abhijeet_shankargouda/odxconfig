/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DTCS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DTCS#getDTCPROXY <em>DTCPROXY</em>}</li>
 *   <li>{@link OdxXhtml.DTCS#getDTCREF <em>DTCREF</em>}</li>
 *   <li>{@link OdxXhtml.DTCS#getDTC <em>DTC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDTCS()
 * @model extendedMetaData="name='DTCS' kind='elementOnly'"
 * @generated
 */
public interface DTCS extends EObject {
	/**
	 * Returns the value of the '<em><b>DTCPROXY</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DTCPROXY</em>' attribute list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCS_DTCPROXY()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='DTCPROXY:0'"
	 * @generated
	 */
	FeatureMap getDTCPROXY();

	/**
	 * Returns the value of the '<em><b>DTCREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DTCREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCS_DTCREF()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DTC-REF' namespace='##targetNamespace' group='DTCPROXY:0'"
	 * @generated
	 */
	EList<ODXLINK> getDTCREF();

	/**
	 * Returns the value of the '<em><b>DTC</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DTC}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DTC</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCS_DTC()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='DTC' namespace='##targetNamespace' group='DTCPROXY:0'"
	 * @generated
	 */
	EList<DTC> getDTC();

} // DTCS
