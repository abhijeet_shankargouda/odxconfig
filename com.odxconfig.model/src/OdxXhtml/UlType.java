/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ul Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.UlType#getLi <em>Li</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getUlType()
 * @model extendedMetaData="name='ul_._type' kind='elementOnly'"
 * @generated
 */
public interface UlType extends EObject {
	/**
	 * Returns the value of the '<em><b>Li</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.LiType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Li</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getUlType_Li()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='li' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<LiType> getLi();

} // UlType
