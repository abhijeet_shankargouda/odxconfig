/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NEGOUTPUTPARAMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.NEGOUTPUTPARAMS#getNEGOUTPUTPARAM <em>NEGOUTPUTPARAM</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getNEGOUTPUTPARAMS()
 * @model extendedMetaData="name='NEG-OUTPUT-PARAMS' kind='elementOnly'"
 * @generated
 */
public interface NEGOUTPUTPARAMS extends EObject {
	/**
	 * Returns the value of the '<em><b>NEGOUTPUTPARAM</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.NEGOUTPUTPARAM}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NEGOUTPUTPARAM</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getNEGOUTPUTPARAMS_NEGOUTPUTPARAM()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='NEG-OUTPUT-PARAM' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<NEGOUTPUTPARAM> getNEGOUTPUTPARAM();

} // NEGOUTPUTPARAMS
