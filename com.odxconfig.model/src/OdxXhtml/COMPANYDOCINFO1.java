/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYDOCINFO1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYDOCINFO1#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDOCINFO1#getTEAMMEMBERREF <em>TEAMMEMBERREF</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDOCINFO1#getDOCLABEL <em>DOCLABEL</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDOCINFO1#getSDGS <em>SDGS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDOCINFO1()
 * @model extendedMetaData="name='COMPANY-DOC-INFO' kind='elementOnly'"
 * @generated
 */
public interface COMPANYDOCINFO1 extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPANYDATAREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYDATAREF</em>' containment reference.
	 * @see #setCOMPANYDATAREF(ODXLINK1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDOCINFO1_COMPANYDATAREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPANY-DATA-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK1 getCOMPANYDATAREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDOCINFO1#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYDATAREF</em>' containment reference.
	 * @see #getCOMPANYDATAREF()
	 * @generated
	 */
	void setCOMPANYDATAREF(ODXLINK1 value);

	/**
	 * Returns the value of the '<em><b>TEAMMEMBERREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TEAMMEMBERREF</em>' containment reference.
	 * @see #setTEAMMEMBERREF(ODXLINK1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDOCINFO1_TEAMMEMBERREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TEAM-MEMBER-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK1 getTEAMMEMBERREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDOCINFO1#getTEAMMEMBERREF <em>TEAMMEMBERREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TEAMMEMBERREF</em>' containment reference.
	 * @see #getTEAMMEMBERREF()
	 * @generated
	 */
	void setTEAMMEMBERREF(ODXLINK1 value);

	/**
	 * Returns the value of the '<em><b>DOCLABEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOCLABEL</em>' attribute.
	 * @see #setDOCLABEL(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDOCINFO1_DOCLABEL()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='DOC-LABEL' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDOCLABEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDOCINFO1#getDOCLABEL <em>DOCLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOCLABEL</em>' attribute.
	 * @see #getDOCLABEL()
	 * @generated
	 */
	void setDOCLABEL(String value);

	/**
	 * Returns the value of the '<em><b>SDGS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGS</em>' containment reference.
	 * @see #setSDGS(SDGS1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDOCINFO1_SDGS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDGS' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGS1 getSDGS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDOCINFO1#getSDGS <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGS</em>' containment reference.
	 * @see #getSDGS()
	 * @generated
	 */
	void setSDGS(SDGS1 value);

} // COMPANYDOCINFO1
