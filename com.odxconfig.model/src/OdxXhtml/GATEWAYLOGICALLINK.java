/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GATEWAYLOGICALLINK</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.GATEWAYLOGICALLINK#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getGATEWAYLOGICALLINK()
 * @model extendedMetaData="name='GATEWAY-LOGICAL-LINK' kind='elementOnly'"
 * @generated
 */
public interface GATEWAYLOGICALLINK extends LOGICALLINK {
	/**
	 * Returns the value of the '<em><b>SEMANTIC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SEMANTIC</em>' attribute.
	 * @see #setSEMANTIC(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getGATEWAYLOGICALLINK_SEMANTIC()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='SEMANTIC' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSEMANTIC();

	/**
	 * Sets the value of the '{@link OdxXhtml.GATEWAYLOGICALLINK#getSEMANTIC <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SEMANTIC</em>' attribute.
	 * @see #getSEMANTIC()
	 * @generated
	 */
	void setSEMANTIC(String value);

} // GATEWAYLOGICALLINK
