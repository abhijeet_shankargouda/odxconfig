/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VALUE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VALUE#getPHYSICALDEFAULTVALUE <em>PHYSICALDEFAULTVALUE</em>}</li>
 *   <li>{@link OdxXhtml.VALUE#getDOPREF <em>DOPREF</em>}</li>
 *   <li>{@link OdxXhtml.VALUE#getDOPSNREF <em>DOPSNREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVALUE()
 * @model extendedMetaData="name='VALUE' kind='elementOnly'"
 * @generated
 */
public interface VALUE extends POSITIONABLEPARAM {
	/**
	 * Returns the value of the '<em><b>PHYSICALDEFAULTVALUE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALDEFAULTVALUE</em>' attribute.
	 * @see #setPHYSICALDEFAULTVALUE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getVALUE_PHYSICALDEFAULTVALUE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='PHYSICAL-DEFAULT-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPHYSICALDEFAULTVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.VALUE#getPHYSICALDEFAULTVALUE <em>PHYSICALDEFAULTVALUE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSICALDEFAULTVALUE</em>' attribute.
	 * @see #getPHYSICALDEFAULTVALUE()
	 * @generated
	 */
	void setPHYSICALDEFAULTVALUE(String value);

	/**
	 * Returns the value of the '<em><b>DOPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPREF</em>' containment reference.
	 * @see #setDOPREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getVALUE_DOPREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DOP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getDOPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.VALUE#getDOPREF <em>DOPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPREF</em>' containment reference.
	 * @see #getDOPREF()
	 * @generated
	 */
	void setDOPREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>DOPSNREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOPSNREF</em>' containment reference.
	 * @see #setDOPSNREF(SNREF)
	 * @see OdxXhtml.OdxXhtmlPackage#getVALUE_DOPSNREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DOP-SNREF' namespace='##targetNamespace'"
	 * @generated
	 */
	SNREF getDOPSNREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.VALUE#getDOPSNREF <em>DOPSNREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOPSNREF</em>' containment reference.
	 * @see #getDOPSNREF()
	 * @generated
	 */
	void setDOPSNREF(SNREF value);

} // VALUE
