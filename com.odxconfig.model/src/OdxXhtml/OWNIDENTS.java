/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OWNIDENTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.OWNIDENTS#getOWNIDENT <em>OWNIDENT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getOWNIDENTS()
 * @model extendedMetaData="name='OWN-IDENTS' kind='elementOnly'"
 * @generated
 */
public interface OWNIDENTS extends EObject {
	/**
	 * Returns the value of the '<em><b>OWNIDENT</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.OWNIDENT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OWNIDENT</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getOWNIDENTS_OWNIDENT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='OWN-IDENT' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<OWNIDENT> getOWNIDENT();

} // OWNIDENTS
