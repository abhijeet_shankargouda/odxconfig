/**
 */
package OdxXhtml;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XDOC1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.XDOC1#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.XDOC1#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.XDOC1#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.XDOC1#getNUMBER <em>NUMBER</em>}</li>
 *   <li>{@link OdxXhtml.XDOC1#getSTATE <em>STATE</em>}</li>
 *   <li>{@link OdxXhtml.XDOC1#getDATE <em>DATE</em>}</li>
 *   <li>{@link OdxXhtml.XDOC1#getPUBLISHER <em>PUBLISHER</em>}</li>
 *   <li>{@link OdxXhtml.XDOC1#getURL <em>URL</em>}</li>
 *   <li>{@link OdxXhtml.XDOC1#getPOSITION <em>POSITION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1()
 * @model extendedMetaData="name='XDOC' kind='elementOnly'"
 * @generated
 */
public interface XDOC1 extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.XDOC1#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT1)
	 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT1 getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.XDOC1#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT1 value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION1)
	 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION1 getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.XDOC1#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION1 value);

	/**
	 * Returns the value of the '<em><b>NUMBER</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NUMBER</em>' attribute.
	 * @see #setNUMBER(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1_NUMBER()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='NUMBER' namespace='##targetNamespace'"
	 * @generated
	 */
	String getNUMBER();

	/**
	 * Sets the value of the '{@link OdxXhtml.XDOC1#getNUMBER <em>NUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>NUMBER</em>' attribute.
	 * @see #getNUMBER()
	 * @generated
	 */
	void setNUMBER(String value);

	/**
	 * Returns the value of the '<em><b>STATE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STATE</em>' attribute.
	 * @see #setSTATE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1_STATE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='STATE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSTATE();

	/**
	 * Sets the value of the '{@link OdxXhtml.XDOC1#getSTATE <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>STATE</em>' attribute.
	 * @see #getSTATE()
	 * @generated
	 */
	void setSTATE(String value);

	/**
	 * Returns the value of the '<em><b>DATE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATE</em>' attribute.
	 * @see #setDATE(XMLGregorianCalendar)
	 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1_DATE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.DateTime"
	 *        extendedMetaData="kind='element' name='DATE' namespace='##targetNamespace'"
	 * @generated
	 */
	XMLGregorianCalendar getDATE();

	/**
	 * Sets the value of the '{@link OdxXhtml.XDOC1#getDATE <em>DATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATE</em>' attribute.
	 * @see #getDATE()
	 * @generated
	 */
	void setDATE(XMLGregorianCalendar value);

	/**
	 * Returns the value of the '<em><b>PUBLISHER</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PUBLISHER</em>' attribute.
	 * @see #setPUBLISHER(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1_PUBLISHER()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='PUBLISHER' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPUBLISHER();

	/**
	 * Sets the value of the '{@link OdxXhtml.XDOC1#getPUBLISHER <em>PUBLISHER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PUBLISHER</em>' attribute.
	 * @see #getPUBLISHER()
	 * @generated
	 */
	void setPUBLISHER(String value);

	/**
	 * Returns the value of the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URL</em>' attribute.
	 * @see #setURL(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1_URL()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='URL' namespace='##targetNamespace'"
	 * @generated
	 */
	String getURL();

	/**
	 * Sets the value of the '{@link OdxXhtml.XDOC1#getURL <em>URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>URL</em>' attribute.
	 * @see #getURL()
	 * @generated
	 */
	void setURL(String value);

	/**
	 * Returns the value of the '<em><b>POSITION</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>POSITION</em>' attribute.
	 * @see #setPOSITION(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getXDOC1_POSITION()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='POSITION' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPOSITION();

	/**
	 * Sets the value of the '{@link OdxXhtml.XDOC1#getPOSITION <em>POSITION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>POSITION</em>' attribute.
	 * @see #getPOSITION()
	 * @generated
	 */
	void setPOSITION(String value);

} // XDOC1
