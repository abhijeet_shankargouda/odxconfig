/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYREVISIONINFO1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYREVISIONINFO1#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYREVISIONINFO1#getREVISIONLABEL <em>REVISIONLABEL</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYREVISIONINFO1#getSTATE <em>STATE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFO1()
 * @model extendedMetaData="name='COMPANY-REVISION-INFO' kind='elementOnly'"
 * @generated
 */
public interface COMPANYREVISIONINFO1 extends EObject {
	/**
	 * Returns the value of the '<em><b>COMPANYDATAREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYDATAREF</em>' containment reference.
	 * @see #setCOMPANYDATAREF(ODXLINK1)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFO1_COMPANYDATAREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPANY-DATA-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK1 getCOMPANYDATAREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYREVISIONINFO1#getCOMPANYDATAREF <em>COMPANYDATAREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYDATAREF</em>' containment reference.
	 * @see #getCOMPANYDATAREF()
	 * @generated
	 */
	void setCOMPANYDATAREF(ODXLINK1 value);

	/**
	 * Returns the value of the '<em><b>REVISIONLABEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>REVISIONLABEL</em>' attribute.
	 * @see #setREVISIONLABEL(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFO1_REVISIONLABEL()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='REVISION-LABEL' namespace='##targetNamespace'"
	 * @generated
	 */
	String getREVISIONLABEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYREVISIONINFO1#getREVISIONLABEL <em>REVISIONLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>REVISIONLABEL</em>' attribute.
	 * @see #getREVISIONLABEL()
	 * @generated
	 */
	void setREVISIONLABEL(String value);

	/**
	 * Returns the value of the '<em><b>STATE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STATE</em>' attribute.
	 * @see #setSTATE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYREVISIONINFO1_STATE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='STATE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSTATE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYREVISIONINFO1#getSTATE <em>STATE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>STATE</em>' attribute.
	 * @see #getSTATE()
	 * @generated
	 */
	void setSTATE(String value);

} // COMPANYREVISIONINFO1
