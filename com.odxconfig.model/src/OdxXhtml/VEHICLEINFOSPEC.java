/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLEINFOSPEC</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VEHICLEINFOSPEC#getINFOCOMPONENTS <em>INFOCOMPONENTS</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLEINFOSPEC#getVEHICLEINFORMATIONS <em>VEHICLEINFORMATIONS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFOSPEC()
 * @model extendedMetaData="name='VEHICLE-INFO-SPEC' kind='elementOnly'"
 * @generated
 */
public interface VEHICLEINFOSPEC extends ODXCATEGORY {
	/**
	 * Returns the value of the '<em><b>INFOCOMPONENTS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>INFOCOMPONENTS</em>' containment reference.
	 * @see #setINFOCOMPONENTS(INFOCOMPONENTS)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFOSPEC_INFOCOMPONENTS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='INFO-COMPONENTS' namespace='##targetNamespace'"
	 * @generated
	 */
	INFOCOMPONENTS getINFOCOMPONENTS();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFOSPEC#getINFOCOMPONENTS <em>INFOCOMPONENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>INFOCOMPONENTS</em>' containment reference.
	 * @see #getINFOCOMPONENTS()
	 * @generated
	 */
	void setINFOCOMPONENTS(INFOCOMPONENTS value);

	/**
	 * Returns the value of the '<em><b>VEHICLEINFORMATIONS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VEHICLEINFORMATIONS</em>' containment reference.
	 * @see #setVEHICLEINFORMATIONS(VEHICLEINFORMATIONS)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLEINFOSPEC_VEHICLEINFORMATIONS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='VEHICLE-INFORMATIONS' namespace='##targetNamespace'"
	 * @generated
	 */
	VEHICLEINFORMATIONS getVEHICLEINFORMATIONS();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLEINFOSPEC#getVEHICLEINFORMATIONS <em>VEHICLEINFORMATIONS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>VEHICLEINFORMATIONS</em>' containment reference.
	 * @see #getVEHICLEINFORMATIONS()
	 * @generated
	 */
	void setVEHICLEINFORMATIONS(VEHICLEINFORMATIONS value);

} // VEHICLEINFOSPEC
