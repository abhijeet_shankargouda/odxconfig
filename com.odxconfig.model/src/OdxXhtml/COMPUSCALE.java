/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPUSCALE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPUSCALE#getSHORTLABEL <em>SHORTLABEL</em>}</li>
 *   <li>{@link OdxXhtml.COMPUSCALE#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.COMPUSCALE#getLOWERLIMIT <em>LOWERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.COMPUSCALE#getUPPERLIMIT <em>UPPERLIMIT</em>}</li>
 *   <li>{@link OdxXhtml.COMPUSCALE#getCOMPUINVERSEVALUE <em>COMPUINVERSEVALUE</em>}</li>
 *   <li>{@link OdxXhtml.COMPUSCALE#getCOMPUCONST <em>COMPUCONST</em>}</li>
 *   <li>{@link OdxXhtml.COMPUSCALE#getCOMPURATIONALCOEFFS <em>COMPURATIONALCOEFFS</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALE()
 * @model extendedMetaData="name='COMPU-SCALE' kind='elementOnly'"
 * @generated
 */
public interface COMPUSCALE extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTLABEL</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTLABEL</em>' containment reference.
	 * @see #setSHORTLABEL(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALE_SHORTLABEL()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SHORT-LABEL' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getSHORTLABEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUSCALE#getSHORTLABEL <em>SHORTLABEL</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTLABEL</em>' containment reference.
	 * @see #getSHORTLABEL()
	 * @generated
	 */
	void setSHORTLABEL(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALE_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUSCALE#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>LOWERLIMIT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LOWERLIMIT</em>' containment reference.
	 * @see #setLOWERLIMIT(LIMIT)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALE_LOWERLIMIT()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LOWER-LIMIT' namespace='##targetNamespace'"
	 * @generated
	 */
	LIMIT getLOWERLIMIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUSCALE#getLOWERLIMIT <em>LOWERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LOWERLIMIT</em>' containment reference.
	 * @see #getLOWERLIMIT()
	 * @generated
	 */
	void setLOWERLIMIT(LIMIT value);

	/**
	 * Returns the value of the '<em><b>UPPERLIMIT</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UPPERLIMIT</em>' containment reference.
	 * @see #setUPPERLIMIT(LIMIT)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALE_UPPERLIMIT()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='UPPER-LIMIT' namespace='##targetNamespace'"
	 * @generated
	 */
	LIMIT getUPPERLIMIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUSCALE#getUPPERLIMIT <em>UPPERLIMIT</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UPPERLIMIT</em>' containment reference.
	 * @see #getUPPERLIMIT()
	 * @generated
	 */
	void setUPPERLIMIT(LIMIT value);

	/**
	 * Returns the value of the '<em><b>COMPUINVERSEVALUE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUINVERSEVALUE</em>' containment reference.
	 * @see #setCOMPUINVERSEVALUE(COMPUINVERSEVALUE)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALE_COMPUINVERSEVALUE()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPU-INVERSE-VALUE' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUINVERSEVALUE getCOMPUINVERSEVALUE();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUSCALE#getCOMPUINVERSEVALUE <em>COMPUINVERSEVALUE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUINVERSEVALUE</em>' containment reference.
	 * @see #getCOMPUINVERSEVALUE()
	 * @generated
	 */
	void setCOMPUINVERSEVALUE(COMPUINVERSEVALUE value);

	/**
	 * Returns the value of the '<em><b>COMPUCONST</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUCONST</em>' containment reference.
	 * @see #setCOMPUCONST(COMPUCONST)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALE_COMPUCONST()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPU-CONST' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUCONST getCOMPUCONST();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUSCALE#getCOMPUCONST <em>COMPUCONST</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUCONST</em>' containment reference.
	 * @see #getCOMPUCONST()
	 * @generated
	 */
	void setCOMPUCONST(COMPUCONST value);

	/**
	 * Returns the value of the '<em><b>COMPURATIONALCOEFFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPURATIONALCOEFFS</em>' containment reference.
	 * @see #setCOMPURATIONALCOEFFS(COMPURATIONALCOEFFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUSCALE_COMPURATIONALCOEFFS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPU-RATIONAL-COEFFS' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPURATIONALCOEFFS getCOMPURATIONALCOEFFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPUSCALE#getCOMPURATIONALCOEFFS <em>COMPURATIONALCOEFFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPURATIONALCOEFFS</em>' containment reference.
	 * @see #getCOMPURATIONALCOEFFS()
	 * @generated
	 */
	void setCOMPURATIONALCOEFFS(COMPURATIONALCOEFFS value);

} // COMPUSCALE
