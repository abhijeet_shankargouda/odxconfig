/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUVARIANTPATTERNS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUVARIANTPATTERNS#getECUVARIANTPATTERN <em>ECUVARIANTPATTERN</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUVARIANTPATTERNS()
 * @model extendedMetaData="name='ECU-VARIANT-PATTERNS' kind='elementOnly'"
 * @generated
 */
public interface ECUVARIANTPATTERNS extends EObject {
	/**
	 * Returns the value of the '<em><b>ECUVARIANTPATTERN</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ECUVARIANTPATTERN}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUVARIANTPATTERN</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getECUVARIANTPATTERNS_ECUVARIANTPATTERN()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ECU-VARIANT-PATTERN' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ECUVARIANTPATTERN> getECUVARIANTPATTERN();

} // ECUVARIANTPATTERNS
