/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NOTINHERITEDVARIABLES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.NOTINHERITEDVARIABLES#getNOTINHERITEDVARIABLE <em>NOTINHERITEDVARIABLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getNOTINHERITEDVARIABLES()
 * @model extendedMetaData="name='NOT-INHERITED-VARIABLES' kind='elementOnly'"
 * @generated
 */
public interface NOTINHERITEDVARIABLES extends EObject {
	/**
	 * Returns the value of the '<em><b>NOTINHERITEDVARIABLE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.NOTINHERITEDVARIABLE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>NOTINHERITEDVARIABLE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getNOTINHERITEDVARIABLES_NOTINHERITEDVARIABLE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='NOT-INHERITED-VARIABLE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<NOTINHERITEDVARIABLE> getNOTINHERITEDVARIABLE();

} // NOTINHERITEDVARIABLES
