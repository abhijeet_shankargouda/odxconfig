/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPUDENOMINATOR</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPUDENOMINATOR#getV <em>V</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUDENOMINATOR()
 * @model extendedMetaData="name='COMPU-DENOMINATOR' kind='elementOnly'"
 * @generated
 */
public interface COMPUDENOMINATOR extends EObject {
	/**
	 * Returns the value of the '<em><b>V</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.V}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>V</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPUDENOMINATOR_V()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='V' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<V> getV();

} // COMPUDENOMINATOR
