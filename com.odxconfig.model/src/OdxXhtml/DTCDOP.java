/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DTCDOP</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DTCDOP#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}</li>
 *   <li>{@link OdxXhtml.DTCDOP#getPHYSICALTYPE <em>PHYSICALTYPE</em>}</li>
 *   <li>{@link OdxXhtml.DTCDOP#getCOMPUMETHOD <em>COMPUMETHOD</em>}</li>
 *   <li>{@link OdxXhtml.DTCDOP#getDTCS <em>DTCS</em>}</li>
 *   <li>{@link OdxXhtml.DTCDOP#isISVISIBLE <em>ISVISIBLE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDTCDOP()
 * @model extendedMetaData="name='DTC-DOP' kind='elementOnly'"
 * @generated
 */
public interface DTCDOP extends DOPBASE {
	/**
	 * Returns the value of the '<em><b>DIAGCODEDTYPE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DIAGCODEDTYPE</em>' containment reference.
	 * @see #setDIAGCODEDTYPE(DIAGCODEDTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCDOP_DIAGCODEDTYPE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DIAG-CODED-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	DIAGCODEDTYPE getDIAGCODEDTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTCDOP#getDIAGCODEDTYPE <em>DIAGCODEDTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DIAGCODEDTYPE</em>' containment reference.
	 * @see #getDIAGCODEDTYPE()
	 * @generated
	 */
	void setDIAGCODEDTYPE(DIAGCODEDTYPE value);

	/**
	 * Returns the value of the '<em><b>PHYSICALTYPE</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALTYPE</em>' containment reference.
	 * @see #setPHYSICALTYPE(PHYSICALTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCDOP_PHYSICALTYPE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PHYSICAL-TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	PHYSICALTYPE getPHYSICALTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTCDOP#getPHYSICALTYPE <em>PHYSICALTYPE</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSICALTYPE</em>' containment reference.
	 * @see #getPHYSICALTYPE()
	 * @generated
	 */
	void setPHYSICALTYPE(PHYSICALTYPE value);

	/**
	 * Returns the value of the '<em><b>COMPUMETHOD</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPUMETHOD</em>' containment reference.
	 * @see #setCOMPUMETHOD(COMPUMETHOD)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCDOP_COMPUMETHOD()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='COMPU-METHOD' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPUMETHOD getCOMPUMETHOD();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTCDOP#getCOMPUMETHOD <em>COMPUMETHOD</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPUMETHOD</em>' containment reference.
	 * @see #getCOMPUMETHOD()
	 * @generated
	 */
	void setCOMPUMETHOD(COMPUMETHOD value);

	/**
	 * Returns the value of the '<em><b>DTCS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DTCS</em>' containment reference.
	 * @see #setDTCS(DTCS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCDOP_DTCS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DTCS' namespace='##targetNamespace'"
	 * @generated
	 */
	DTCS getDTCS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTCDOP#getDTCS <em>DTCS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DTCS</em>' containment reference.
	 * @see #getDTCS()
	 * @generated
	 */
	void setDTCS(DTCS value);

	/**
	 * Returns the value of the '<em><b>ISVISIBLE</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISVISIBLE</em>' attribute.
	 * @see #isSetISVISIBLE()
	 * @see #unsetISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @see OdxXhtml.OdxXhtmlPackage#getDTCDOP_ISVISIBLE()
	 * @model default="false" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 *        extendedMetaData="kind='attribute' name='IS-VISIBLE' namespace='##targetNamespace'"
	 * @generated
	 */
	boolean isISVISIBLE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DTCDOP#isISVISIBLE <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ISVISIBLE</em>' attribute.
	 * @see #isSetISVISIBLE()
	 * @see #unsetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @generated
	 */
	void setISVISIBLE(boolean value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DTCDOP#isISVISIBLE <em>ISVISIBLE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @generated
	 */
	void unsetISVISIBLE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DTCDOP#isISVISIBLE <em>ISVISIBLE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ISVISIBLE</em>' attribute is set.
	 * @see #unsetISVISIBLE()
	 * @see #isISVISIBLE()
	 * @see #setISVISIBLE(boolean)
	 * @generated
	 */
	boolean isSetISVISIBLE();

} // DTCDOP
