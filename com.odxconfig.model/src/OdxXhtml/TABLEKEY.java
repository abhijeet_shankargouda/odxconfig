/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TABLEKEY</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.TABLEKEY#getTABLEREF <em>TABLEREF</em>}</li>
 *   <li>{@link OdxXhtml.TABLEKEY#getTABLEROWREF <em>TABLEROWREF</em>}</li>
 *   <li>{@link OdxXhtml.TABLEKEY#getID <em>ID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getTABLEKEY()
 * @model extendedMetaData="name='TABLE-KEY' kind='elementOnly'"
 * @generated
 */
public interface TABLEKEY extends POSITIONABLEPARAM {
	/**
	 * Returns the value of the '<em><b>TABLEREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TABLEREF</em>' containment reference.
	 * @see #setTABLEREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLEKEY_TABLEREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TABLE-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getTABLEREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLEKEY#getTABLEREF <em>TABLEREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TABLEREF</em>' containment reference.
	 * @see #getTABLEREF()
	 * @generated
	 */
	void setTABLEREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>TABLEROWREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TABLEROWREF</em>' containment reference.
	 * @see #setTABLEROWREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLEKEY_TABLEROWREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TABLE-ROW-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getTABLEROWREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLEKEY#getTABLEROWREF <em>TABLEROWREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TABLEROWREF</em>' containment reference.
	 * @see #getTABLEROWREF()
	 * @generated
	 */
	void setTABLEROWREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLEKEY_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLEKEY#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

} // TABLEKEY
