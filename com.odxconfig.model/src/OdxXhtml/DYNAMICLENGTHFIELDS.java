/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DYNAMICLENGTHFIELDS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DYNAMICLENGTHFIELDS#getDYNAMICLENGTHFIELD <em>DYNAMICLENGTHFIELD</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMICLENGTHFIELDS()
 * @model extendedMetaData="name='DYNAMIC-LENGTH-FIELDS' kind='elementOnly'"
 * @generated
 */
public interface DYNAMICLENGTHFIELDS extends EObject {
	/**
	 * Returns the value of the '<em><b>DYNAMICLENGTHFIELD</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.DYNAMICLENGTHFIELD}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DYNAMICLENGTHFIELD</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getDYNAMICLENGTHFIELDS_DYNAMICLENGTHFIELD()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DYNAMIC-LENGTH-FIELD' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<DYNAMICLENGTHFIELD> getDYNAMICLENGTHFIELD();

} // DYNAMICLENGTHFIELDS
