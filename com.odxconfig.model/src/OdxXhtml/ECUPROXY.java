/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUPROXY</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUPROXY()
 * @model extendedMetaData="name='ECU-PROXY' kind='elementOnly'"
 * @generated
 */
public interface ECUPROXY extends INFOCOMPONENT {
} // ECUPROXY
