/**
 */
package OdxXhtml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>POSRESPONSE</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPOSRESPONSE()
 * @model extendedMetaData="name='POS-RESPONSE' kind='elementOnly'"
 * @generated
 */
public interface POSRESPONSE extends RESPONSE {
} // POSRESPONSE
