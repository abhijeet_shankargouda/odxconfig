/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>STRUCTURES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.STRUCTURES#getSTRUCTURE <em>STRUCTURE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSTRUCTURES()
 * @model extendedMetaData="name='STRUCTURES' kind='elementOnly'"
 * @generated
 */
public interface STRUCTURES extends EObject {
	/**
	 * Returns the value of the '<em><b>STRUCTURE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.STRUCTURE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STRUCTURE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getSTRUCTURES_STRUCTURE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='STRUCTURE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<STRUCTURE> getSTRUCTURE();

} // STRUCTURES
