/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SESSION</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.SESSION#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.SESSION#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.SESSION#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.SESSION#getEXPECTEDIDENTS <em>EXPECTEDIDENTS</em>}</li>
 *   <li>{@link OdxXhtml.SESSION#getCHECKSUMS <em>CHECKSUMS</em>}</li>
 *   <li>{@link OdxXhtml.SESSION#getSECURITYS <em>SECURITYS</em>}</li>
 *   <li>{@link OdxXhtml.SESSION#getDATABLOCKREFS <em>DATABLOCKREFS</em>}</li>
 *   <li>{@link OdxXhtml.SESSION#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.SESSION#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.SESSION#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getSESSION()
 * @model extendedMetaData="name='SESSION' kind='elementOnly'"
 * @generated
 */
public interface SESSION extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>EXPECTEDIDENTS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EXPECTEDIDENTS</em>' containment reference.
	 * @see #setEXPECTEDIDENTS(EXPECTEDIDENTS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_EXPECTEDIDENTS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EXPECTED-IDENTS' namespace='##targetNamespace'"
	 * @generated
	 */
	EXPECTEDIDENTS getEXPECTEDIDENTS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getEXPECTEDIDENTS <em>EXPECTEDIDENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EXPECTEDIDENTS</em>' containment reference.
	 * @see #getEXPECTEDIDENTS()
	 * @generated
	 */
	void setEXPECTEDIDENTS(EXPECTEDIDENTS value);

	/**
	 * Returns the value of the '<em><b>CHECKSUMS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CHECKSUMS</em>' containment reference.
	 * @see #setCHECKSUMS(CHECKSUMS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_CHECKSUMS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='CHECKSUMS' namespace='##targetNamespace'"
	 * @generated
	 */
	CHECKSUMS getCHECKSUMS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getCHECKSUMS <em>CHECKSUMS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CHECKSUMS</em>' containment reference.
	 * @see #getCHECKSUMS()
	 * @generated
	 */
	void setCHECKSUMS(CHECKSUMS value);

	/**
	 * Returns the value of the '<em><b>SECURITYS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SECURITYS</em>' containment reference.
	 * @see #setSECURITYS(SECURITYS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_SECURITYS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SECURITYS' namespace='##targetNamespace'"
	 * @generated
	 */
	SECURITYS getSECURITYS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getSECURITYS <em>SECURITYS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SECURITYS</em>' containment reference.
	 * @see #getSECURITYS()
	 * @generated
	 */
	void setSECURITYS(SECURITYS value);

	/**
	 * Returns the value of the '<em><b>DATABLOCKREFS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DATABLOCKREFS</em>' containment reference.
	 * @see #setDATABLOCKREFS(DATABLOCKREFS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_DATABLOCKREFS()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='DATABLOCK-REFS' namespace='##targetNamespace'"
	 * @generated
	 */
	DATABLOCKREFS getDATABLOCKREFS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getDATABLOCKREFS <em>DATABLOCKREFS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DATABLOCKREFS</em>' containment reference.
	 * @see #getDATABLOCKREFS()
	 * @generated
	 */
	void setDATABLOCKREFS(DATABLOCKREFS value);

	/**
	 * Returns the value of the '<em><b>SDGS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGS</em>' containment reference.
	 * @see #setSDGS(SDGS)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_SDGS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDGS' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGS getSDGS();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getSDGS <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGS</em>' containment reference.
	 * @see #getSDGS()
	 * @generated
	 */
	void setSDGS(SDGS value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getSESSION_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.SESSION#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // SESSION
