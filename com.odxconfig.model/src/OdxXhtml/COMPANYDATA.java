/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COMPANYDATA</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.COMPANYDATA#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA#getROLES <em>ROLES</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA#getTEAMMEMBERS <em>TEAMMEMBERS</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA#getCOMPANYSPECIFICINFO <em>COMPANYSPECIFICINFO</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.COMPANYDATA#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA()
 * @model extendedMetaData="name='COMPANY-DATA' kind='elementOnly'"
 * @generated
 */
public interface COMPANYDATA extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>ROLES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ROLES</em>' containment reference.
	 * @see #setROLES(ROLES)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA_ROLES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ROLES' namespace='##targetNamespace'"
	 * @generated
	 */
	ROLES getROLES();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA#getROLES <em>ROLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ROLES</em>' containment reference.
	 * @see #getROLES()
	 * @generated
	 */
	void setROLES(ROLES value);

	/**
	 * Returns the value of the '<em><b>TEAMMEMBERS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TEAMMEMBERS</em>' containment reference.
	 * @see #setTEAMMEMBERS(TEAMMEMBERS)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA_TEAMMEMBERS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TEAM-MEMBERS' namespace='##targetNamespace'"
	 * @generated
	 */
	TEAMMEMBERS getTEAMMEMBERS();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA#getTEAMMEMBERS <em>TEAMMEMBERS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TEAMMEMBERS</em>' containment reference.
	 * @see #getTEAMMEMBERS()
	 * @generated
	 */
	void setTEAMMEMBERS(TEAMMEMBERS value);

	/**
	 * Returns the value of the '<em><b>COMPANYSPECIFICINFO</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>COMPANYSPECIFICINFO</em>' containment reference.
	 * @see #setCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA_COMPANYSPECIFICINFO()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='COMPANY-SPECIFIC-INFO' namespace='##targetNamespace'"
	 * @generated
	 */
	COMPANYSPECIFICINFO getCOMPANYSPECIFICINFO();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA#getCOMPANYSPECIFICINFO <em>COMPANYSPECIFICINFO</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>COMPANYSPECIFICINFO</em>' containment reference.
	 * @see #getCOMPANYSPECIFICINFO()
	 * @generated
	 */
	void setCOMPANYSPECIFICINFO(COMPANYSPECIFICINFO value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getCOMPANYDATA_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.COMPANYDATA#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // COMPANYDATA
