/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ECUMEMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ECUMEMS#getECUMEM <em>ECUMEM</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMS()
 * @model extendedMetaData="name='ECU-MEMS' kind='elementOnly'"
 * @generated
 */
public interface ECUMEMS extends EObject {
	/**
	 * Returns the value of the '<em><b>ECUMEM</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ECUMEM}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ECUMEM</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getECUMEMS_ECUMEM()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ECU-MEM' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ECUMEM> getECUMEM();

} // ECUMEMS
