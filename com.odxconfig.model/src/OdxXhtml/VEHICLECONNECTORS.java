/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLECONNECTORS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORS#getVEHICLECONNECTOR <em>VEHICLECONNECTOR</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORS()
 * @model extendedMetaData="name='VEHICLE-CONNECTORS' kind='elementOnly'"
 * @generated
 */
public interface VEHICLECONNECTORS extends EObject {
	/**
	 * Returns the value of the '<em><b>VEHICLECONNECTOR</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.VEHICLECONNECTOR}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>VEHICLECONNECTOR</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORS_VEHICLECONNECTOR()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='VEHICLE-CONNECTOR' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<VEHICLECONNECTOR> getVEHICLECONNECTOR();

} // VEHICLECONNECTORS
