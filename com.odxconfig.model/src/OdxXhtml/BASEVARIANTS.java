/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BASEVARIANTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.BASEVARIANTS#getBASEVARIANT <em>BASEVARIANT</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getBASEVARIANTS()
 * @model extendedMetaData="name='BASE-VARIANTS' kind='elementOnly'"
 * @generated
 */
public interface BASEVARIANTS extends EObject {
	/**
	 * Returns the value of the '<em><b>BASEVARIANT</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.BASEVARIANT}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BASEVARIANT</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getBASEVARIANTS_BASEVARIANT()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='BASE-VARIANT' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<BASEVARIANT> getBASEVARIANT();

} // BASEVARIANTS
