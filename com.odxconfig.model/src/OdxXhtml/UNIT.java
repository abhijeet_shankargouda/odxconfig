/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UNIT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.UNIT#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.UNIT#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.UNIT#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.UNIT#getDISPLAYNAME <em>DISPLAYNAME</em>}</li>
 *   <li>{@link OdxXhtml.UNIT#getFACTORSITOUNIT <em>FACTORSITOUNIT</em>}</li>
 *   <li>{@link OdxXhtml.UNIT#getOFFSETSITOUNIT <em>OFFSETSITOUNIT</em>}</li>
 *   <li>{@link OdxXhtml.UNIT#getPHYSICALDIMENSIONREF <em>PHYSICALDIMENSIONREF</em>}</li>
 *   <li>{@link OdxXhtml.UNIT#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.UNIT#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getUNIT()
 * @model extendedMetaData="name='UNIT' kind='elementOnly'"
 * @generated
 */
public interface UNIT extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNIT_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNIT#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNIT_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNIT#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNIT_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNIT#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>DISPLAYNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DISPLAYNAME</em>' attribute.
	 * @see #setDISPLAYNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNIT_DISPLAYNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='DISPLAY-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDISPLAYNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNIT#getDISPLAYNAME <em>DISPLAYNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DISPLAYNAME</em>' attribute.
	 * @see #getDISPLAYNAME()
	 * @generated
	 */
	void setDISPLAYNAME(String value);

	/**
	 * Returns the value of the '<em><b>FACTORSITOUNIT</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FACTORSITOUNIT</em>' attribute.
	 * @see #isSetFACTORSITOUNIT()
	 * @see #unsetFACTORSITOUNIT()
	 * @see #setFACTORSITOUNIT(double)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNIT_FACTORSITOUNIT()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Double"
	 *        extendedMetaData="kind='element' name='FACTOR-SI-TO-UNIT' namespace='##targetNamespace'"
	 * @generated
	 */
	double getFACTORSITOUNIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNIT#getFACTORSITOUNIT <em>FACTORSITOUNIT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FACTORSITOUNIT</em>' attribute.
	 * @see #isSetFACTORSITOUNIT()
	 * @see #unsetFACTORSITOUNIT()
	 * @see #getFACTORSITOUNIT()
	 * @generated
	 */
	void setFACTORSITOUNIT(double value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.UNIT#getFACTORSITOUNIT <em>FACTORSITOUNIT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFACTORSITOUNIT()
	 * @see #getFACTORSITOUNIT()
	 * @see #setFACTORSITOUNIT(double)
	 * @generated
	 */
	void unsetFACTORSITOUNIT();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.UNIT#getFACTORSITOUNIT <em>FACTORSITOUNIT</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>FACTORSITOUNIT</em>' attribute is set.
	 * @see #unsetFACTORSITOUNIT()
	 * @see #getFACTORSITOUNIT()
	 * @see #setFACTORSITOUNIT(double)
	 * @generated
	 */
	boolean isSetFACTORSITOUNIT();

	/**
	 * Returns the value of the '<em><b>OFFSETSITOUNIT</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OFFSETSITOUNIT</em>' attribute.
	 * @see #isSetOFFSETSITOUNIT()
	 * @see #unsetOFFSETSITOUNIT()
	 * @see #setOFFSETSITOUNIT(double)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNIT_OFFSETSITOUNIT()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Double"
	 *        extendedMetaData="kind='element' name='OFFSET-SI-TO-UNIT' namespace='##targetNamespace'"
	 * @generated
	 */
	double getOFFSETSITOUNIT();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNIT#getOFFSETSITOUNIT <em>OFFSETSITOUNIT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OFFSETSITOUNIT</em>' attribute.
	 * @see #isSetOFFSETSITOUNIT()
	 * @see #unsetOFFSETSITOUNIT()
	 * @see #getOFFSETSITOUNIT()
	 * @generated
	 */
	void setOFFSETSITOUNIT(double value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.UNIT#getOFFSETSITOUNIT <em>OFFSETSITOUNIT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOFFSETSITOUNIT()
	 * @see #getOFFSETSITOUNIT()
	 * @see #setOFFSETSITOUNIT(double)
	 * @generated
	 */
	void unsetOFFSETSITOUNIT();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.UNIT#getOFFSETSITOUNIT <em>OFFSETSITOUNIT</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>OFFSETSITOUNIT</em>' attribute is set.
	 * @see #unsetOFFSETSITOUNIT()
	 * @see #getOFFSETSITOUNIT()
	 * @see #setOFFSETSITOUNIT(double)
	 * @generated
	 */
	boolean isSetOFFSETSITOUNIT();

	/**
	 * Returns the value of the '<em><b>PHYSICALDIMENSIONREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHYSICALDIMENSIONREF</em>' containment reference.
	 * @see #setPHYSICALDIMENSIONREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNIT_PHYSICALDIMENSIONREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='PHYSICAL-DIMENSION-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getPHYSICALDIMENSIONREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNIT#getPHYSICALDIMENSIONREF <em>PHYSICALDIMENSIONREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHYSICALDIMENSIONREF</em>' containment reference.
	 * @see #getPHYSICALDIMENSIONREF()
	 * @generated
	 */
	void setPHYSICALDIMENSIONREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNIT_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNIT#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getUNIT_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.UNIT#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // UNIT
