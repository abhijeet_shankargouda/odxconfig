/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FUNCTCLASSREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.FUNCTCLASSREFS#getFUNCTCLASSREF <em>FUNCTCLASSREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTCLASSREFS()
 * @model extendedMetaData="name='FUNCT-CLASS-REFS' kind='elementOnly'"
 * @generated
 */
public interface FUNCTCLASSREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>FUNCTCLASSREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FUNCTCLASSREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getFUNCTCLASSREFS_FUNCTCLASSREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FUNCT-CLASS-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ODXLINK> getFUNCTCLASSREF();

} // FUNCTCLASSREFS
