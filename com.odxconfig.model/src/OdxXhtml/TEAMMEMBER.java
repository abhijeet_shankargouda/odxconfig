/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TEAMMEMBER</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getROLES <em>ROLES</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getDEPARTMENT <em>DEPARTMENT</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getADDRESS <em>ADDRESS</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getZIP <em>ZIP</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getCITY <em>CITY</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getPHONE <em>PHONE</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getFAX <em>FAX</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getEMAIL <em>EMAIL</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.TEAMMEMBER#getOID <em>OID</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER()
 * @model extendedMetaData="name='TEAM-MEMBER' kind='elementOnly'"
 * @generated
 */
public interface TEAMMEMBER extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>ROLES</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ROLES</em>' containment reference.
	 * @see #setROLES(ROLES)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_ROLES()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ROLES' namespace='##targetNamespace'"
	 * @generated
	 */
	ROLES getROLES();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getROLES <em>ROLES</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ROLES</em>' containment reference.
	 * @see #getROLES()
	 * @generated
	 */
	void setROLES(ROLES value);

	/**
	 * Returns the value of the '<em><b>DEPARTMENT</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DEPARTMENT</em>' attribute.
	 * @see #setDEPARTMENT(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_DEPARTMENT()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='DEPARTMENT' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDEPARTMENT();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getDEPARTMENT <em>DEPARTMENT</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DEPARTMENT</em>' attribute.
	 * @see #getDEPARTMENT()
	 * @generated
	 */
	void setDEPARTMENT(String value);

	/**
	 * Returns the value of the '<em><b>ADDRESS</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADDRESS</em>' attribute.
	 * @see #setADDRESS(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_ADDRESS()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='ADDRESS' namespace='##targetNamespace'"
	 * @generated
	 */
	String getADDRESS();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getADDRESS <em>ADDRESS</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADDRESS</em>' attribute.
	 * @see #getADDRESS()
	 * @generated
	 */
	void setADDRESS(String value);

	/**
	 * Returns the value of the '<em><b>ZIP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ZIP</em>' attribute.
	 * @see #setZIP(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_ZIP()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='ZIP' namespace='##targetNamespace'"
	 * @generated
	 */
	String getZIP();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getZIP <em>ZIP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ZIP</em>' attribute.
	 * @see #getZIP()
	 * @generated
	 */
	void setZIP(String value);

	/**
	 * Returns the value of the '<em><b>CITY</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CITY</em>' attribute.
	 * @see #setCITY(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_CITY()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='CITY' namespace='##targetNamespace'"
	 * @generated
	 */
	String getCITY();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getCITY <em>CITY</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CITY</em>' attribute.
	 * @see #getCITY()
	 * @generated
	 */
	void setCITY(String value);

	/**
	 * Returns the value of the '<em><b>PHONE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PHONE</em>' attribute.
	 * @see #setPHONE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_PHONE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='PHONE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPHONE();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getPHONE <em>PHONE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PHONE</em>' attribute.
	 * @see #getPHONE()
	 * @generated
	 */
	void setPHONE(String value);

	/**
	 * Returns the value of the '<em><b>FAX</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FAX</em>' attribute.
	 * @see #setFAX(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_FAX()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='FAX' namespace='##targetNamespace'"
	 * @generated
	 */
	String getFAX();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getFAX <em>FAX</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FAX</em>' attribute.
	 * @see #getFAX()
	 * @generated
	 */
	void setFAX(String value);

	/**
	 * Returns the value of the '<em><b>EMAIL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EMAIL</em>' attribute.
	 * @see #setEMAIL(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_EMAIL()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='EMAIL' namespace='##targetNamespace'"
	 * @generated
	 */
	String getEMAIL();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getEMAIL <em>EMAIL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EMAIL</em>' attribute.
	 * @see #getEMAIL()
	 * @generated
	 */
	void setEMAIL(String value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTEAMMEMBER_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.TEAMMEMBER#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

} // TEAMMEMBER
