/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ACCESSLEVELS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.ACCESSLEVELS#getACCESSLEVEL <em>ACCESSLEVEL</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getACCESSLEVELS()
 * @model extendedMetaData="name='ACCESS-LEVELS' kind='elementOnly'"
 * @generated
 */
public interface ACCESSLEVELS extends EObject {
	/**
	 * Returns the value of the '<em><b>ACCESSLEVEL</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ACCESSLEVEL}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ACCESSLEVEL</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getACCESSLEVELS_ACCESSLEVEL()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ACCESS-LEVEL' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ACCESSLEVEL> getACCESSLEVEL();

} // ACCESSLEVELS
