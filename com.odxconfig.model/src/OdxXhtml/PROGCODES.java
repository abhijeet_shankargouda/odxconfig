/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PROGCODES</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PROGCODES#getPROGCODE <em>PROGCODE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPROGCODES()
 * @model extendedMetaData="name='PROG-CODES' kind='elementOnly'"
 * @generated
 */
public interface PROGCODES extends EObject {
	/**
	 * Returns the value of the '<em><b>PROGCODE</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.PROGCODE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROGCODE</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPROGCODES_PROGCODE()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PROG-CODE' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PROGCODE> getPROGCODE();

} // PROGCODES
