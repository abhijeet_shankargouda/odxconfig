/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PARENTREFS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.PARENTREFS#getPARENTREF <em>PARENTREF</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getPARENTREFS()
 * @model extendedMetaData="name='PARENT-REFS' kind='elementOnly'"
 * @generated
 */
public interface PARENTREFS extends EObject {
	/**
	 * Returns the value of the '<em><b>PARENTREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.PARENTREF}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PARENTREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getPARENTREFS_PARENTREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='PARENT-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<PARENTREF> getPARENTREF();

} // PARENTREFS
