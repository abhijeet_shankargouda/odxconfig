/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VEHICLECONNECTORPIN</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORPIN#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORPIN#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORPIN#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORPIN#getPINNUMBER <em>PINNUMBER</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORPIN#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORPIN#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.VEHICLECONNECTORPIN#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPIN()
 * @model extendedMetaData="name='VEHICLE-CONNECTOR-PIN' kind='elementOnly'"
 * @generated
 */
public interface VEHICLECONNECTORPIN extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPIN_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPIN_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPIN_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>PINNUMBER</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PINNUMBER</em>' attribute.
	 * @see #isSetPINNUMBER()
	 * @see #unsetPINNUMBER()
	 * @see #setPINNUMBER(long)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPIN_PINNUMBER()
	 * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.UnsignedInt" required="true"
	 *        extendedMetaData="kind='element' name='PIN_NUMBER' namespace='##targetNamespace'"
	 * @generated
	 */
	long getPINNUMBER();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getPINNUMBER <em>PINNUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>PINNUMBER</em>' attribute.
	 * @see #isSetPINNUMBER()
	 * @see #unsetPINNUMBER()
	 * @see #getPINNUMBER()
	 * @generated
	 */
	void setPINNUMBER(long value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getPINNUMBER <em>PINNUMBER</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPINNUMBER()
	 * @see #getPINNUMBER()
	 * @see #setPINNUMBER(long)
	 * @generated
	 */
	void unsetPINNUMBER();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getPINNUMBER <em>PINNUMBER</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>PINNUMBER</em>' attribute is set.
	 * @see #unsetPINNUMBER()
	 * @see #getPINNUMBER()
	 * @see #setPINNUMBER(long)
	 * @generated
	 */
	boolean isSetPINNUMBER();

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPIN_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPIN_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

	/**
	 * Returns the value of the '<em><b>TYPE</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.PINTYPE}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TYPE</em>' attribute.
	 * @see OdxXhtml.PINTYPE
	 * @see #isSetTYPE()
	 * @see #unsetTYPE()
	 * @see #setTYPE(PINTYPE)
	 * @see OdxXhtml.OdxXhtmlPackage#getVEHICLECONNECTORPIN_TYPE()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	PINTYPE getTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TYPE</em>' attribute.
	 * @see OdxXhtml.PINTYPE
	 * @see #isSetTYPE()
	 * @see #unsetTYPE()
	 * @see #getTYPE()
	 * @generated
	 */
	void setTYPE(PINTYPE value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTYPE()
	 * @see #getTYPE()
	 * @see #setTYPE(PINTYPE)
	 * @generated
	 */
	void unsetTYPE();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.VEHICLECONNECTORPIN#getTYPE <em>TYPE</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>TYPE</em>' attribute is set.
	 * @see #unsetTYPE()
	 * @see #getTYPE()
	 * @see #setTYPE(PINTYPE)
	 * @generated
	 */
	boolean isSetTYPE();

} // VEHICLECONNECTORPIN
