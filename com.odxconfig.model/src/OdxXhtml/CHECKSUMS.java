/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CHECKSUMS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.CHECKSUMS#getCHECKSUM <em>CHECKSUM</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getCHECKSUMS()
 * @model extendedMetaData="name='CHECKSUMS' kind='elementOnly'"
 * @generated
 */
public interface CHECKSUMS extends EObject {
	/**
	 * Returns the value of the '<em><b>CHECKSUM</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.CHECKSUM}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CHECKSUM</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getCHECKSUMS_CHECKSUM()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='CHECKSUM' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<CHECKSUM> getCHECKSUM();

} // CHECKSUMS
