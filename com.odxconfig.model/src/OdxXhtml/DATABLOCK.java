/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DATABLOCK</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DATABLOCK#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getFLASHDATAREF <em>FLASHDATAREF</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getFILTERS <em>FILTERS</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getSEGMENTS <em>SEGMENTS</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getTARGETADDROFFSET <em>TARGETADDROFFSET</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getOWNIDENTS <em>OWNIDENTS</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getSECURITYS <em>SECURITYS</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getSDGS <em>SDGS</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.DATABLOCK#getTYPE <em>TYPE</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK()
 * @model extendedMetaData="name='DATABLOCK' kind='elementOnly'"
 * @generated
 */
public interface DATABLOCK extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>FLASHDATAREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FLASHDATAREF</em>' containment reference.
	 * @see #setFLASHDATAREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_FLASHDATAREF()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='FLASHDATA-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getFLASHDATAREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getFLASHDATAREF <em>FLASHDATAREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FLASHDATAREF</em>' containment reference.
	 * @see #getFLASHDATAREF()
	 * @generated
	 */
	void setFLASHDATAREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>FILTERS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FILTERS</em>' containment reference.
	 * @see #setFILTERS(FILTERS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_FILTERS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FILTERS' namespace='##targetNamespace'"
	 * @generated
	 */
	FILTERS getFILTERS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getFILTERS <em>FILTERS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FILTERS</em>' containment reference.
	 * @see #getFILTERS()
	 * @generated
	 */
	void setFILTERS(FILTERS value);

	/**
	 * Returns the value of the '<em><b>SEGMENTS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SEGMENTS</em>' containment reference.
	 * @see #setSEGMENTS(SEGMENTS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_SEGMENTS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SEGMENTS' namespace='##targetNamespace'"
	 * @generated
	 */
	SEGMENTS getSEGMENTS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getSEGMENTS <em>SEGMENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SEGMENTS</em>' containment reference.
	 * @see #getSEGMENTS()
	 * @generated
	 */
	void setSEGMENTS(SEGMENTS value);

	/**
	 * Returns the value of the '<em><b>TARGETADDROFFSET</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TARGETADDROFFSET</em>' containment reference.
	 * @see #setTARGETADDROFFSET(TARGETADDROFFSET)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_TARGETADDROFFSET()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='TARGET-ADDR-OFFSET' namespace='##targetNamespace'"
	 * @generated
	 */
	TARGETADDROFFSET getTARGETADDROFFSET();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getTARGETADDROFFSET <em>TARGETADDROFFSET</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TARGETADDROFFSET</em>' containment reference.
	 * @see #getTARGETADDROFFSET()
	 * @generated
	 */
	void setTARGETADDROFFSET(TARGETADDROFFSET value);

	/**
	 * Returns the value of the '<em><b>OWNIDENTS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OWNIDENTS</em>' containment reference.
	 * @see #setOWNIDENTS(OWNIDENTS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_OWNIDENTS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='OWN-IDENTS' namespace='##targetNamespace'"
	 * @generated
	 */
	OWNIDENTS getOWNIDENTS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getOWNIDENTS <em>OWNIDENTS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OWNIDENTS</em>' containment reference.
	 * @see #getOWNIDENTS()
	 * @generated
	 */
	void setOWNIDENTS(OWNIDENTS value);

	/**
	 * Returns the value of the '<em><b>SECURITYS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SECURITYS</em>' containment reference.
	 * @see #setSECURITYS(SECURITYS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_SECURITYS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SECURITYS' namespace='##targetNamespace'"
	 * @generated
	 */
	SECURITYS getSECURITYS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getSECURITYS <em>SECURITYS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SECURITYS</em>' containment reference.
	 * @see #getSECURITYS()
	 * @generated
	 */
	void setSECURITYS(SECURITYS value);

	/**
	 * Returns the value of the '<em><b>SDGS</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SDGS</em>' containment reference.
	 * @see #setSDGS(SDGS)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_SDGS()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SDGS' namespace='##targetNamespace'"
	 * @generated
	 */
	SDGS getSDGS();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getSDGS <em>SDGS</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SDGS</em>' containment reference.
	 * @see #getSDGS()
	 * @generated
	 */
	void setSDGS(SDGS value);

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

	/**
	 * Returns the value of the '<em><b>TYPE</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TYPE</em>' attribute.
	 * @see #setTYPE(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATABLOCK_TYPE()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='TYPE' namespace='##targetNamespace'"
	 * @generated
	 */
	String getTYPE();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATABLOCK#getTYPE <em>TYPE</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TYPE</em>' attribute.
	 * @see #getTYPE()
	 * @generated
	 */
	void setTYPE(String value);

} // DATABLOCK
