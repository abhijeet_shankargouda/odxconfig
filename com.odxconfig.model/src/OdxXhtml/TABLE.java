/**
 */
package OdxXhtml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TABLE</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.TABLE#getSHORTNAME <em>SHORTNAME</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getLONGNAME <em>LONGNAME</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getDESC <em>DESC</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getKEYLABEL <em>KEYLABEL</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getSTRUCTLABEL <em>STRUCTLABEL</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getADMINDATA <em>ADMINDATA</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getKEYDOPREF <em>KEYDOPREF</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getROWWRAPPER <em>ROWWRAPPER</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getTABLEROWREF <em>TABLEROWREF</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getTABLEROW <em>TABLEROW</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getID <em>ID</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getOID <em>OID</em>}</li>
 *   <li>{@link OdxXhtml.TABLE#getSEMANTIC <em>SEMANTIC</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getTABLE()
 * @model extendedMetaData="name='TABLE' kind='elementOnly'"
 * @generated
 */
public interface TABLE extends EObject {
	/**
	 * Returns the value of the '<em><b>SHORTNAME</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SHORTNAME</em>' attribute.
	 * @see #setSHORTNAME(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_SHORTNAME()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='element' name='SHORT-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSHORTNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getSHORTNAME <em>SHORTNAME</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SHORTNAME</em>' attribute.
	 * @see #getSHORTNAME()
	 * @generated
	 */
	void setSHORTNAME(String value);

	/**
	 * Returns the value of the '<em><b>LONGNAME</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LONGNAME</em>' containment reference.
	 * @see #setLONGNAME(TEXT)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_LONGNAME()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='LONG-NAME' namespace='##targetNamespace'"
	 * @generated
	 */
	TEXT getLONGNAME();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getLONGNAME <em>LONGNAME</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LONGNAME</em>' containment reference.
	 * @see #getLONGNAME()
	 * @generated
	 */
	void setLONGNAME(TEXT value);

	/**
	 * Returns the value of the '<em><b>DESC</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DESC</em>' containment reference.
	 * @see #setDESC(DESCRIPTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_DESC()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='DESC' namespace='##targetNamespace'"
	 * @generated
	 */
	DESCRIPTION getDESC();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getDESC <em>DESC</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DESC</em>' containment reference.
	 * @see #getDESC()
	 * @generated
	 */
	void setDESC(DESCRIPTION value);

	/**
	 * Returns the value of the '<em><b>KEYLABEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>KEYLABEL</em>' attribute.
	 * @see #setKEYLABEL(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_KEYLABEL()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='KEY-LABEL' namespace='##targetNamespace'"
	 * @generated
	 */
	String getKEYLABEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getKEYLABEL <em>KEYLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>KEYLABEL</em>' attribute.
	 * @see #getKEYLABEL()
	 * @generated
	 */
	void setKEYLABEL(String value);

	/**
	 * Returns the value of the '<em><b>STRUCTLABEL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>STRUCTLABEL</em>' attribute.
	 * @see #setSTRUCTLABEL(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_STRUCTLABEL()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='element' name='STRUCT-LABEL' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSTRUCTLABEL();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getSTRUCTLABEL <em>STRUCTLABEL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>STRUCTLABEL</em>' attribute.
	 * @see #getSTRUCTLABEL()
	 * @generated
	 */
	void setSTRUCTLABEL(String value);

	/**
	 * Returns the value of the '<em><b>ADMINDATA</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #setADMINDATA(ADMINDATA)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_ADMINDATA()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ADMIN-DATA' namespace='##targetNamespace'"
	 * @generated
	 */
	ADMINDATA getADMINDATA();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getADMINDATA <em>ADMINDATA</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADMINDATA</em>' containment reference.
	 * @see #getADMINDATA()
	 * @generated
	 */
	void setADMINDATA(ADMINDATA value);

	/**
	 * Returns the value of the '<em><b>KEYDOPREF</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>KEYDOPREF</em>' containment reference.
	 * @see #setKEYDOPREF(ODXLINK)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_KEYDOPREF()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='KEY-DOP-REF' namespace='##targetNamespace'"
	 * @generated
	 */
	ODXLINK getKEYDOPREF();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getKEYDOPREF <em>KEYDOPREF</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>KEYDOPREF</em>' containment reference.
	 * @see #getKEYDOPREF()
	 * @generated
	 */
	void setKEYDOPREF(ODXLINK value);

	/**
	 * Returns the value of the '<em><b>ROWWRAPPER</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ROWWRAPPER</em>' attribute list.
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_ROWWRAPPER()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='ROWWRAPPER:7'"
	 * @generated
	 */
	FeatureMap getROWWRAPPER();

	/**
	 * Returns the value of the '<em><b>TABLEROWREF</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.ODXLINK}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TABLEROWREF</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_TABLEROWREF()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='TABLE-ROW-REF' namespace='##targetNamespace' group='ROWWRAPPER:7'"
	 * @generated
	 */
	EList<ODXLINK> getTABLEROWREF();

	/**
	 * Returns the value of the '<em><b>TABLEROW</b></em>' containment reference list.
	 * The list contents are of type {@link OdxXhtml.TABLEROW}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TABLEROW</em>' containment reference list.
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_TABLEROW()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='TABLE-ROW' namespace='##targetNamespace' group='ROWWRAPPER:7'"
	 * @generated
	 */
	EList<TABLEROW> getTABLEROW();

	/**
	 * Returns the value of the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ID</em>' attribute.
	 * @see #setID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_ID()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID" required="true"
	 *        extendedMetaData="kind='attribute' name='ID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getID();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getID <em>ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ID</em>' attribute.
	 * @see #getID()
	 * @generated
	 */
	void setID(String value);

	/**
	 * Returns the value of the '<em><b>OID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OID</em>' attribute.
	 * @see #setOID(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_OID()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='OID' namespace='##targetNamespace'"
	 * @generated
	 */
	String getOID();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getOID <em>OID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OID</em>' attribute.
	 * @see #getOID()
	 * @generated
	 */
	void setOID(String value);

	/**
	 * Returns the value of the '<em><b>SEMANTIC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SEMANTIC</em>' attribute.
	 * @see #setSEMANTIC(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getTABLE_SEMANTIC()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='SEMANTIC' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSEMANTIC();

	/**
	 * Sets the value of the '{@link OdxXhtml.TABLE#getSEMANTIC <em>SEMANTIC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SEMANTIC</em>' attribute.
	 * @see #getSEMANTIC()
	 * @generated
	 */
	void setSEMANTIC(String value);

} // TABLE
