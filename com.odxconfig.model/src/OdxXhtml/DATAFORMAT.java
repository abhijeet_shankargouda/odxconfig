/**
 */
package OdxXhtml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DATAFORMAT</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OdxXhtml.DATAFORMAT#getValue <em>Value</em>}</li>
 *   <li>{@link OdxXhtml.DATAFORMAT#getSELECTION <em>SELECTION</em>}</li>
 * </ul>
 *
 * @see OdxXhtml.OdxXhtmlPackage#getDATAFORMAT()
 * @model extendedMetaData="name='DATAFORMAT' kind='simple'"
 * @generated
 */
public interface DATAFORMAT extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAFORMAT_Value()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="name=':0' kind='simple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATAFORMAT#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>SELECTION</b></em>' attribute.
	 * The literals are from the enumeration {@link OdxXhtml.DATAFORMATSELECTION}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SELECTION</em>' attribute.
	 * @see OdxXhtml.DATAFORMATSELECTION
	 * @see #isSetSELECTION()
	 * @see #unsetSELECTION()
	 * @see #setSELECTION(DATAFORMATSELECTION)
	 * @see OdxXhtml.OdxXhtmlPackage#getDATAFORMAT_SELECTION()
	 * @model unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='SELECTION' namespace='##targetNamespace'"
	 * @generated
	 */
	DATAFORMATSELECTION getSELECTION();

	/**
	 * Sets the value of the '{@link OdxXhtml.DATAFORMAT#getSELECTION <em>SELECTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SELECTION</em>' attribute.
	 * @see OdxXhtml.DATAFORMATSELECTION
	 * @see #isSetSELECTION()
	 * @see #unsetSELECTION()
	 * @see #getSELECTION()
	 * @generated
	 */
	void setSELECTION(DATAFORMATSELECTION value);

	/**
	 * Unsets the value of the '{@link OdxXhtml.DATAFORMAT#getSELECTION <em>SELECTION</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSELECTION()
	 * @see #getSELECTION()
	 * @see #setSELECTION(DATAFORMATSELECTION)
	 * @generated
	 */
	void unsetSELECTION();

	/**
	 * Returns whether the value of the '{@link OdxXhtml.DATAFORMAT#getSELECTION <em>SELECTION</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>SELECTION</em>' attribute is set.
	 * @see #unsetSELECTION()
	 * @see #getSELECTION()
	 * @see #setSELECTION(DATAFORMATSELECTION)
	 * @generated
	 */
	boolean isSetSELECTION();

} // DATAFORMAT
